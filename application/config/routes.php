<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login/login_controller/admin_credentials';
$route['404_override'] = 'errors/error_controller/page_not_found';
$route['translate_uri_dashes'] = FALSE;

$route['logout'] = 'login/login_controller/logout';

/*Admin Basic Template Route Starts*/
$route['dashboard'] = 'admin_base_template/admin_base_template_controller/dashboard';
/*Admin Basic Template Route Ends*/

/*Change Password*/
$route['change-password'] = 'other_operation/change_password_controller/change_password';
/*Change Password*/

/*website controlling*/
$route['manage-website-testimonial'] = 'other_operation/website_controller/manage_testimonial';
$route['del-testimonial-video/(:any)'] = 'other_operation/website_controller/delete_testimonial_video/$1';
/*website controlling*/



/* Tele Enquiry Routes Starts */
$route['view-tele-enquiry'] = 'tele_enquiry/tele_enquiry_controller/view_tele_enquiry';
$route['view-tele-enquiry/(:num)'] = 'tele_enquiry/tele_enquiry_controller/view_tele_enquiry/$1';
$route['add-tele-enquiry'] = 'tele_enquiry/tele_enquiry_controller/add_tele_enquiry';
$route['edit-tele-enquiry/(:any)'] = 'tele_enquiry/tele_enquiry_controller/edit_tele_enquiry/$1';
$route['del-tele-enquiry/(:num)'] = 'tele_enquiry/tele_enquiry_controller/del_tele_enquiry/$1';
$route['enquiry_handler'] = 'tele_enquiry/tele_enquiry_controller/enquiry_handler';
$route['sub_sources'] = 'tele_enquiry/tele_enquiry_controller/sub_sources';
$route['telEnqExp'] = 'tele_enquiry/tele_enquiry_controller/telEnqExp';


    /* Tele Enquiry Follow Up routes Starts */
    $route['tele-enquiry-follow-up/details/(:any)/(:any)'] = 'follow_up/follow_up_controller/teleEnquiryFollowUp/$1/$2';
    $route['tele-enquiry-follow-up/add-follow-up'] = 'follow_up/follow_up_controller/addTeleEnquiryFollowUp';
    $route['tele-enquiry-follow-up/centreDropDown'] = 'follow_up/follow_up_controller/centreDropDown';
    $route['tele-enquiry-follow-up/employeeNames'] = 'follow_up/follow_up_controller/employeeNames';
    $route['tele-enquiry-follow-up/updateTeleEnquiryFollowUp'] = 'follow_up/follow_up_controller/updateTeleEnquiryFollowUp';
    $route['tele-enquiry-follow-up/deleteTeleEnquiryFollowUp'] = 'follow_up/follow_up_controller/deleteTeleEnquiryFollowUp';
    $route['followUpStatus'] = 'follow_up/follow_up_controller/followUpStatusDropDown';

        /* get followups of today routes starts  by ankur  */
        $route['getCurrentEquiries'] = 'tele_enquiry/tele_enquiry_controller/getTodayFollowUps';
        $route['followup_enquiry_details'] = 'tele_enquiry/tele_enquiry_controller/showTodayFollowUps';
        /* get followups of today routes ends  by ankur  */
    /* Tele Enquiry Follow Up routes Ends */

/* Tele Enquiry Routes Ends */

/* HR route starts */
$route['hr/addCandidate'] = 'hr/HR_controller/addCandidate';
/* HR route ends */

/* HRA routes start*/
$route['hra/add-employee'] = 'hra/HRA_controller/add_employee/';
$route['hra/update-employee/(:any)'] = 'hra/HRA_controller/update_employee/$1';
$route['hra/addTiming'] = 'hra/HRA_controller/addTiming';
$route['hra/view-employee'] = 'hra/HRA_controller/view_employee';
$route['hra/export-employee-list'] = 'hra/HRA_controller/export_employee_details';
$route['hra/view-employee/(:num)'] = 'hra/HRA_controller/view_employee/$1';
$route['hra/update-staff-login'] = 'hra/HRA_controller/update_staff_login';
$route['hra/update-staff-login/(:any)'] = 'hra/HRA_controller/update_staff_login/$1';
$route['update_staff_login_details'] = 'hra/HRA_controller/update_staff_login_details';
$route['hra/post_vacancy'] = 'hra/HRA_controller/post_vacancy/';
$route['hra/close_vacancy/(:num)'] = 'hra/HRA_controller/close_vacancy/$1';
$route['employee-paid-leaves'] = 'hra/HRA_controller/employee_paid_leaves/';
$route['employee-paid-leaves/(:num)'] = 'hra/HRA_controller/employee_paid_leaves/$1';
$route['hra/hr_settings'] = 'hra/HRA_controller/hr_settings/';
$route['hr_settings_add_new_designation'] = 'hra/HRA_controller/add_new_designation';
$route['hr_settings_update_payroll_details'] = 'hra/HRA_controller/update_payroll_details';
$route['hr_settings_reinstate_exemployeesearch'] = 'hra/HRA_controller/reinstate_exemployeesearch';
$route['new_reinstate_exemployee'] = 'hra/HRA_controller/add_reinstate_employee';
$route['hra/edit_PaidLeave'] = 'hra/HRA_controller/edit_PaidLeave';
$route['hra/updatePL'] = 'hra/HRA_controller/updatePL';
$route['hra/empIds'] = 'hra/HRA_controller/empIdPdf';
$route['hra/idCard'] = 'hra/HRA_controller/empIdCard';
$route['hra/addAppraisal'] = 'hra/HRA_controller/addAppraisal';
$route['hra/performanceAppraisal/(:any)'] = 'hra/HRA_controller/performanceAppraisal/$1';
$route['hra/performanceAppraisalUpdate/(:any)'] = 'hra/HRA_controller/performanceAppraisalUpdate/$1';
$route['hra/empAppraisalSystem'] = 'hra/HRA_controller/employeeAppraisalSystem';
$route['hra/empAppraisalSystem/(:num)'] = 'hra/HRA_controller/employeeAppraisalSystem/$1';
$route['hra/addCandidate'] = 'hra/HRA_controller/addCandidate';
// routes added by ankur on 13/09/2017
$route['hra/update-controls'] = 'hra/HRA_controller/update_controls';
$route['update_control_data'] = 'hra/HRA_controller/update_control_data';
$route['hra/bank-master'] = 'hra/HRA_controller/bank_master';
$route['update_bank_name'] = 'hra/HRA_controller/update_bank_name';
$route['hra/add-bank-branch/(:any)/(:any)'] = 'hra/HRA_controller/add_bank_branch/$1/$2';
$route['update_bank_branch_name'] = 'hra/HRA_controller/update_bank_branch_name';
$route['delete_bank_branch_name'] = 'hra/HRA_controller/delete_bank_branch_name';
$route['hra/attendance'] = 'hra/HRA_controller/monthly_attendance';
$route['hra/employee_leave_approval/(:any)/(:any)'] = 'hra/HRA_controller/leave_approval/$1/$2';
$route['hra/update_attendance_timing/(:any)/(:any)'] = 'hra/HRA_controller/update_attendance/$1/$2';
$route['approve_or_disapprove_leaves'] = 'hra/HRA_controller/approve_or_disapprove_leaves';
$route['hra/holiday-list'] = 'hra/HRA_controller/holiday_list';
$route['download'] = 'hra/HRA_controller/download_xml';
/* HRA routes End*/

/* Enquiry Routes Starts */
    $route['tele-enquiry/convert-enquiry/(:any)'] = 'enquiry/enquiry_controller/convertEnquiry/$1';
    $route['enquiry/view-enquiry'] = 'enquiry/enquiry_controller/viewSearchEnquiry';
    $route['enquiry/enquiry-report'] = 'enquiry/enquiry_controller/enquiry_report';
    $route['enquiry/view-enquiry/(:num)'] = 'enquiry/enquiry_controller/viewSearchEnquiry/$1';
    $route['enquiry/add-enquiry'] = 'enquiry/enquiry_controller/addEnquiry';
    $route['enquiry/edit-enquiry/(:any)'] = 'enquiry/enquiry_controller/editEnquiry/$1';
    $route['enquiry/del-enquiry/(:any)'] = 'enquiry/enquiry_controller/delEnquiry/$1';
    $route['eod-enquiry'] = 'enquiry/enquiry_controller/eod_enquiry';
/* Enquiry Routes Ends */

/* Admission Routes Starts */
    $route['enquiry/convert-admission/(:any)'] = 'admission/admission_controller/addAdmission/$1';
    $route['discounts'] = 'admission/admission_controller/discounts';
    $route['course-fees'] = 'admission/admission_controller/courseFees';
    $route['admissions/admission-installment/(:any)'] = 'admission/admission_controller/admissionInstallment/$1';
    $route['admission/view-admission'] = 'admission/admission_controller/viewAdmission';
    $route['viewAdmission'] = 'admission/admission_controller/see_admission'; // temporary added for testing
    $route['admission/getAdmissionReport'] = 'admission/admission_controller/getAdmissionReport';
    $route['admission/view-admission/(:any)'] = 'admission/admission_controller/viewAdmission/$1';
    $route['admission/view-admission/(:num)'] = 'admission/admission_controller/viewAdmission/$1';
    $route['admission/edit-admission/(:any)'] = 'admission/admission_controller/editAdmission/$1';
    $route['admission/delete-admission/(:any)'] = 'admission/admission_controller/deleteAdmission/$1';
    $route['admission/getAdmissionReceipt'] = 'admission/admission_controller/getAdmissionReceipt';
    $route['admission/saveAdmissionReceipt'] = 'admission/admission_controller/saveAdmissionReceipt';
    $route['admission/admissionReciptPrint/(:any)'] = 'admission/admission_controller/admissionReciptPrint/$1';
    $route['admission/otherRecData'] = 'admission/admission_controller/otherRecData';
    $route['admission/addOtherReceipts'] = 'admission/admission_controller/addOtherReceipts';
    $route['admission/otherRecPrint/(:any)'] = 'admission/admission_controller/otherRecPrint/$1';
    $route['admission/editInstallment/(:any)'] = 'admission/admission_controller/editInstallment/$1';
    $route['admission/addUpdateInstallment'] = 'admission/admission_controller/addUpdateInstallment';
    $route['admission/delInstallment'] = 'admission/admission_controller/delInstallment';
/* Admission Routes Ends */

/* Batch Master Routes Starts */
    $route['batchMaster/batchcreation'] = 'batch_master/BatchMaster_controller/batchcreation';
    // $route['BatchMaster/batchupdation'] = 'batch_master/BatchMaster_controller/batchupdation';
    // $route['batchMaster/batchupdation/(:num)'] = 'batch_master/BatchMaster_controller/batchupdation/$1';
    $route['batchMaster/batchupdation/(:any)'] = 'batch_master/BatchMaster_controller/batchUpdation/$1';
    $route['batchMaster/import-existing-batch/(:any)'] = 'batch_master/BatchMaster_controller/ImportExistingBatch/$1';
    $route['batchMaster/batch-delete/(:any)'] = 'batch_master/BatchMaster_controller/batchDelete/$1';
    // $route['BatchMaster/batchsearch'] = 'batch_master/BatchMaster_controller/batchsearch';
    $route['BatchMaster/batchsearch'] = 'batch_master/BatchMaster_controller/batchsearch';
    $route['BatchMaster/batchreport'] = 'batch_master/BatchMaster_controller/batch_report';
    $route['Batchmaster/export-batch-search'] = 'batch_master/Batchmaster_controller/export_batch_search';
    $route['BatchMaster/batchsearch/(:any)'] = 'batch_master/BatchMaster_controller/batchsearch/$1';
    $route['get_empname'] = 'batch_master/BatchMaster_controller/get_empname';
    $route['get_duration'] = 'batch_master/BatchMaster_controller/get_duration';

    // $route['BatchMaster/assignment-upload'] = 'batch_master/BatchMaster_controller/assignment_upload';
    // $route['BatchMaster/assignment-upload/(:any)'] = 'batch_master/BatchMaster_controller/assignment_upload/$1';

    $route['BatchMaster/addStudentsToBatch/(:any)'] = 'batch_master/BatchMaster_controller/addStudentsToBatch/$1';
    $route['batchMaster/delSelectedStudents'] = 'batch_master/BatchMaster_controller/delSelectedStudents';
    $route['batchMaster/addSelectedStudentsToBatch'] = 'batch_master/BatchMaster_controller/addSelectedStudentsToBatch';
    $route['batchMaster/issueAttendenceSheet/(:any)'] = 'batch_master/BatchMaster_controller/issueAttendenceSheet/$1';
    //laxmi
    $route['batchMaster/attendance/(:any)'] = 'batch_master/BatchMaster_controller/attendance/$1';
    $route['batchMaster/saveAttendance'] = 'batch_master/BatchMaster_controller/saveAttendance';
    //laxmi

    $route['batchMaster/weekly-test/(:any)'] = 'batch_master/BatchMaster_controller/upload_weekly_test/$1';
    $route['batchMaster/markBatchCompletion/(:any)'] = 'batch_master/BatchMaster_controller/markBatchCompletion/$1';
    $route['sendBatchCompletionRequest/(:any)'] = 'batch_master/BatchMaster_controller/sendBatchCompletionRequest/$1';
    $route['batchMaster/upload-assignments/(:any)'] = 'batch_master/BatchMaster_controller/upload_assignments_and_projects/$1';
    /* getting assignments and projects attachments */
    $route['get_assignment_or_projects_attachments/(:any)'] = 'batch_master/BatchMaster_controller/link_assignment_or_projects_attachments/$1';
    /* getting assignments and projects attachments */

    $route['batchMaster/batchRequest'] = 'batch_master/BatchMaster_controller/batchRequest';
    $route['batchMaster/batchRequest/(:any)'] = 'batch_master/BatchMaster_controller/batchRequest/$1';


    $route['batchMaster/facultyaudit'] = 'batch_master/BatchMaster_controller/facultyaudit';
    $route['batchMaster/facultyaudit_save'] = 'batch_master/BatchMaster_controller/facultyaudit_save';
    // $route['batchMaster/facultyaudit-exists/(:any)'] = 'batch_master/BatchMaster_controller/facultyaudit_exists/$1';

    $route['batchMaster/studentNotice'] = 'batch_master/BatchMaster_controller/studentNotice';
    $route['batchMaster/studentNotice/(:any)'] = 'batch_master/BatchMaster_controller/studentNotice/$1';
    $route['batchMaster/studentNoticeDelete/(:any)'] = 'batch_master/BatchMaster_controller/studentNoticeDelete/$1';
    $route['batchMaster/student-exam-details'] = 'batch_master/BatchMaster_controller/search_student_exam_details/';

/* Batch Master Routes Ends */

/* vendor Routes Starts */
    $route['vendor/view-vendor'] = 'vendor_master/vendor/vendor_controller/getVendorList';
    $route['vendor/add-vendor'] = 'vendor_master/vendor/vendor_controller/addVendor';
    $route['vendor/add-vendor/(:num)'] = 'vendor_master/vendor/vendor_controller/addVendor/$1';
    $route['vendor/edit-vendor/(:any)'] = 'vendor_master/vendor/vendor_controller/updateVendor/$1';
	$route['vendor/del-vendor/(:any)'] = 'vendor_master/vendor/vendor_controller/deleteVendor/$1';
/* vendor Routes Ends */

/* tax Routes Starts */
    $route['tax/view-tax'] = 'vendor_master/tax/tax_controller/getTaxList';
    $route['tax/add-tax'] = 'vendor_master/tax/tax_controller/addTax';
    $route['tax/edit-tax/(:any)'] = 'vendor_master/tax/tax_controller/updateTax/$1';
    $route['tax/del-tax/(:any)'] = 'vendor_master/tax/tax_controller/deleteTax/$1';
/* tax Routes Ends */

/* unit_master Routes Starts */
    $route['unit-master/unit-master'] = 'logistics_master/unit_master/unit_controller/addUnit';
    $route['unit-master/edit-unit/(:any)'] = 'logistics_master/unit_master/unit_controller/updateUnit/$1';
	$route['unit-master/del-unit/(:any)'] = 'logistics_master/unit_master/unit_controller/deleteUnit/$1';
/* unit_master Routes Ends */

/* item Routes Starts */
    $route['item/view-item'] = 'logistics_master/item/item_controller/getItemList';
    $route['item/add-item'] = 'logistics_master/item/item_controller/addItem';
    $route['item/add-item/(:num)'] = 'logistics_master/item/item_controller/addItem/$1';
    $route['item/edit-item/(:any)'] = 'logistics_master/item/item_controller/updateItem/$1';
	$route['item/del-item/(:any)'] = 'logistics_master/item/item_controller/deleteItem/$1';
/* item Routes Ends */

/* assets Routes Starts */
    $route['assets/add-assets'] = 'logistics_master/asset/asset_controller/addAsset';
    $route['get_assets_sub_category'] = 'logistics_master/asset/asset_controller/get_assets_sub_categories_details';
/* assets Routes Ends */

/* HO Stock Routes Start */
$route['hostock/hostock-add'] = "logistics_master/hostock/hostock_controller/hostock_add";
$route['hostock/hostock-add/(:num)'] = "logistics_master/hostock/hostock_controller/hostock_add/$1";

$route['hostock/hostock-view'] = "logistics_master/hostock/hostock_controller/hostock_view";

$route['hostock/hostock-view/(:num)'] = "logistics_master/hostock/hostock_controller/hostock_view/$1";


$route['admission/book-exam/(:any)/(:any)'] = 'admission/admission_controller/bookExam/$1/$2';
$route['admission/get_exam_slots'] = 'admission/admission_controller/getexamtimingslot';
$route['admission/hallticket/(:any)/(:any)/(:any)'] = 'admission/hallticket_print/$1/$2/$3';
$route['admission/issue-certificate/(:any)'] = 'admission/admission_controller/issue_certificate/$1';
$route['merge_exam_marks'] = 'admission/admission_controller/merge_exam_marks';
$route['admission/print-certificate/(:any)/(:any)'] = 'admission/admission_controller/print_certificate_and_marksheet/$1/$2';
$route['admission/print-marksheet/(:any)/(:any)'] = 'admission/admission_controller/print_certificate_and_marksheet/$1/$2';
$route['admission/reset-password/(:any)'] = 'admission/admission_controller/reset_student_desk_password/$1';
/* HO Stock Routes Ends */

$route['purchase-order/add-purchase-order'] = 'vendor_master/purchase_order/purchase_order_controller/addPurchaseOrder';

$route['purchase-order/add-purchase-order/(:num)'] = 'vendor_master/purchase_order/purchase_order_controller/addPurchaseOrder/$1';


$route['purchase-order/purchaseOrderItem'] = 'vendor_master/purchase_order/purchase_order_controller/purchaseOrderItem';
$route['purchase-order/purchaseOrder'] = 'vendor_master/purchase_order/purchase_order_controller/purchaseOrder';
$route['purchase-order/printPurchOrder/(:any)'] = 'vendor_master/purchase_order/purchase_order_controller/printPurchOrder/$1';
// $route['purchase-order/purchaseOrder'] = 'vendor_master/purchase_order/purchase_order_controller/purchaseOrder';

    $route['employee/view-candidate'] = 'employee/Employee_controller/show';
    $route['employee/insert-candidate'] = 'employee/Employee_controller/insertCandidate';

/* Stationery Routes Starts */
$route['requisition/stationery'] = 'requisition/requisition_controller/stationery';
$route['requisition/requisiton-status'] = 'requisition/requisition_controller/requisitionStatus';
$route['requisition/stationeryRequisition'] = 'requisition/requisition_controller/stationeryRequisition';
$route['requisition/addReq'] = 'requisition/requisition_controller/addReq';
$route['requisition/viewRequisition'] = 'requisition/requisition_controller/viewRequisition';
$route['requisition/viewRequisition/(:num)'] = 'requisition/requisition_controller/viewRequisition/$1';
$route['requisition/RequisitionReport'] = 'requisition/requisition_controller/RequisitionReport';
$route['requisition/updateStatusRequisition/(:any)'] = 'requisition/requisition_controller/updateStatusRequisition/$1';
$route['requisition/challan_print/(:any)'] = 'requisition/requisition_controller/challan_print/$1';
/* Stationery Routes Ends */

/* Reports Routes Starts */
$route['reports/leadConvAnalysisRep'] = 'analysis/analysis_controller/leadConvAnalysisRep';
$route['reports/leadConvAnalysisRep/(:num)'] = 'analysis/analysis_controller/leadConvAnalysisRep/$1';
$route['reports/leadConvAnalysisRepExp'] = 'analysis/analysis_controller/leadExport';
$route['reports/leadConvAnalysisRepExp/(:num)'] = 'analysis/analysis_controller/leadExport/$1';
// added by ankur on 10/11/2017
$route['reports/leadSourceReports'] = 'analysis/analysis_controller/leadSourceReports';
$route['reports/export-to-excel-leadSourceReports'] = 'analysis/analysis_controller/export_to_excel_leadSourceReports';
$route['reports/enquirySummaryReports'] = 'analysis/analysis_controller/enquirySummaryReports';
/* Reports Routes Ends */


/*  Students Routes Starts */
$route['students/student-details/(:any)'] = 'students/student_controller/student_details/$1';
$route['students/student_attendance_upload'] = 'students/student_controller/upload_student_attendance';
$route['upload_student_attendance'] = 'students/student_controller/upload_student_attendance';
$route['get_student_attendance'] = 'students/student_controller/student_attendance';
/*  Students Routes Ends */

/*  Accounts Routes Starts */
$route['accounts/add-employee-payroll'] = 'accounts/account_controller/add_employee_payroll';
$route['import_empolyee_details'] = 'accounts/account_controller/import_empolyee_details';
// added by ankur
//receipts
$route['accounts/search-receipts'] = 'accounts/account_controller/search_receipts';
$route['accounts/search-receipts/(:num)'] = 'accounts/account_controller/search_receipts/$1';
$route['accounts/deleted-receipts'] = 'accounts/account_controller/deleted_receipts';
$route['accounts/deleted-receipts/(:num)'] = 'accounts/account_controller/deleted_receipts/$1';
$route['accounts/deleted-receipts-report'] = 'accounts/account_controller/export_to_excel_deleted_receipts';
$route['accounts/admission-receipts/(:any)/(:any)/(:any)'] = 'accounts/account_controller/admission_receipts/$1/$2/$3';
$route['accounts/print-receipts/(:any)/(:any)'] = 'accounts/account_controller/print_receipts/$1/$2';
$route['accounts/print-summary/(:any)/(:any)'] = 'accounts/account_controller/print_summary/$1/$2';
$route['accounts/transfer-receipts'] = 'accounts/account_controller/transfer_receipts';
$route['get_transfer_receipt_detail/(:any)'] = 'accounts/account_controller/get_transfer_receipt_detail/$1';
$route['transfer_selected_receipts/(:any)'] = 'accounts/account_controller/transfer_selected_receipts/$1';
$route['accounts/cash-check-deposition'] = 'accounts/account_controller/cash_check_deposition';
$route['accounts/cash-check-deposition/(:num)'] = 'accounts/account_controller/cash_check_deposition/$1';
$route['save_cash_cheque_data/(:any)'] = 'accounts/account_controller/update_cash_cheque_data/$1';
$route['get_receipt_data'] = 'accounts/account_controller/get_receipt_data';
$route['get_receipt_data'] = 'accounts/account_controller/search_receipts/$1';
//other receipts
$route['accounts/search-other-receipts'] = 'accounts/account_controller/search_other_receipts';
$route['accounts/export-to-excel-deleted-other-receipts'] = 'accounts/account_controller/export_to_excel_deleted_other_receipts';
$route['accounts/search-other-receipts/(:num)'] = 'accounts/account_controller/search_other_receipts/$1';
$route['accounts/admission-other-receipts/(:any)/(:any)/(:any)'] = 'accounts/account_controller/admission_other_receipts/$1/$2/$3';
$route['accounts/print-other-receipts/(:any)/(:any)'] = 'accounts/account_controller/print_other_receipts/$1/$2';
$route['accounts/transfer-other-receipts'] = 'accounts/account_controller/transfer_other_receipts';
$route['accounts/other-cash-check-deposition'] = 'accounts/account_controller/other_cash_check_deposition';
$route['accounts/other-cash-check-deposition/(:num)'] = 'accounts/account_controller/other_cash_check_deposition/$1';
$route['accounts/admission_reports'] = 'accounts/account_controller/admission_reports';
$route['accounts/admission_reports/(:any)'] = 'accounts/account_controller/admission_reports/$1';
$route['accounts/defaulters_list'] = 'accounts/account_controller/defaulters_list';
$route['accounts/defaulters_list/(:any)'] = 'accounts/account_controller/defaulters_list/$1';
$route['accounts/outstanding-reports'] = 'accounts/account_controller/outstanding_reports';
$route['accounts/outstanding-reports/(:any)'] = 'accounts/account_controller/outstanding_reports/$1';
$route['accounts/deleted-other-receipt-reports'] = 'accounts/account_controller/deleted_receipt_reports';
$route['accounts/deleted-other-receipt-reports/(:any)'] = 'accounts/account_controller/deleted_receipt_reports/$1';
$route['accounts/minimum-fees-reports'] = 'accounts/account_controller/minimum_fees_reports';
$route['accounts/minimum-fees-reports/(:any)'] = 'accounts/account_controller/minimum_fees_reports/$1';
$route['accounts/all-receipt-export-to-excel'] = 'accounts/account_controller/export_to_excel_all_receipts_report';
$route['accounts/all-receipt-search'] = 'accounts/account_controller/all_receipt_search';
$route['accounts/all-receipt-search/(:any)'] = 'accounts/account_controller/all_receipt_search/$1';
$route['accounts/receipts-edcr'] = 'accounts/account_controller/receipt_edcr';
$route['accounts/receipts-edcr/(:any)'] = 'accounts/account_controller/receipt_edcr/$1';
/*  Accounts Routes Ends */

$route['student_marks_upload'] = 'students/student_controller/ExcelDataAdd';

/*  Placement Routes Starts */
$route['placement/add-employer-details'] = 'placement/placement_controller/add_employer_details';
$route['update_company_job_status'] = 'placement/placement_controller/update_company_job_status';
$route['delete_company_details'] = 'placement/placement_controller/delete_company_details';
$route['placement/student-search'] = 'placement/placement_controller/student_search';
$route['placement/download-resume'] = 'placement/placement_controller/download_resume';
$route['placement/placement-remarks/(:any)'] = 'placement/placement_controller/placement_remarks/$1';
$route['placement/employer-details'] = 'placement/placement_controller/search_employer_details';
$route['placement/update-employer-details'] = 'placement/placement_controller/update_employer_details';
/*  Placement Routes Ends */

/*  sql data Routes Starts */
$route['sql-data-fetch'] = 'sql_table_updates/sql_table_updates_controller/check_now';
/*  sql data Routes Ends */

$route['send_sms_to_receipts_from_emis'] = 'tele_enquiry/tele_enquiry_controller/send_sms_to_all_type_of_receipients';
$route['tele-enquiry/convert-to-walkin/(:any)'] = 'tele_enquiry/Tele_enquiry_controller/update_convert_to_walk/$1';
