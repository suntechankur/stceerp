<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_controller extends MX_Controller {

     public function __construct()
        {
            $this->load->model('admission/admission_model');
            $this->load->model('account_model');
            $this->load->model('tele_enquiry/tele_enquiry_model');
            $this->load->model('students/student_model');
            parent::__construct();
        }


    public function add_employee_payroll() {
        $data = array();
        if($this->session->flashdata('exportData')) {

            $affectedrowcount = $this->session->flashdata('exportData');

            $data['tableheading'] = $this->account_model->get_employee_pay_details_header();
            $tablecount = $this->account_model->get_employee_pay_details_count();
            $finalcount = $tablecount - $affectedrowcount;
            // // echo $finalcount;
            $data['tabledata'] = $this->account_model->get_employee_pay_details($finalcount,$affectedrowcount);
            $data['affectrows'] = $affectedrowcount;
            // $this->load->admin_view("add_employee_payroll",$data);
        }
        $this->load->admin_view("add_employee_payroll",$data);
    }

    public function import_empolyee_details() {
        $stardate = $this->input->post('start_date');
        $enddate = $this->input->post('end_date');

        // check end date greater problem in line 38
        //also check the db errors when anyone uploads other excel file where column is not matches to db table columns

        if($enddate != "" && $stardate != "" && $_FILES['excelfile']['size'] != 0) {
            if(!strtotime($enddate) > strtotime($stardate)) {

                $date = str_replace('/', '-', $stardate);
                $sdate = date("Y-m-d H:i:s", strtotime($date));
                $date1 = str_replace('/', '-', $enddate);
                $edate = date("Y-m-d H:i:s", strtotime($date1));

                $config['upload_path'] = FCPATH.'resources/excel/';
                $config['allowed_types'] = 'xlsx|csv|xls';
                $config['file_name'] = "employee_pay_details".time();
                $this->load->library('upload', $config);
                $this->upload->do_upload('excelfile');
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                $extension=$upload_data['file_ext'];

                $this->load->library('excel');

                $objReader= PHPExcel_IOFactory::createReader('Excel2007');
                $objReader->setReadDataOnly(true);

                $objPHPExcel=$objReader->load(FCPATH.'resources/excel/'.$file_name);

                $worksheetData = $objReader->listWorksheetInfo(FCPATH.'resources/excel/'.$file_name);
                $totalColumns  = $worksheetData[0]['totalColumns'];

                $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
                $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
                for($j=0;$j<$totalColumns;$j++){
                    $heading[] = $objWorksheet->getCellByColumnAndRow($j,1)->getValue();
                }

                $result = 0;
                for($i=2;$i<=$totalrows;$i++)
                {
                  for($j=0;$j<$totalColumns;$j++){
                    $data['records']['FROM_DATE'] = $sdate;
                    $data['records']['TO_DATE'] = $edate;

                    if(($heading[$j] == "SANCTIONED_LEAVES") || ($heading[$j] == "UNSANCTIONED_LEAVES")) {
                        $data['records'][$heading[$j]]= "";
                    }
                    else if(($heading[$j] == "DATE_ADDED") || ($heading[$j] == "LAST_MODIFIED")){
                        $data['records'][$heading[$j]]= date("Y-m-d H:i:s");
                    }
                    else {
                        $data['records'][$heading[$j]]= $objWorksheet->getCellByColumnAndRow($j,$i)->getValue();
                    }

                  }

                $result += $this->account_model->employee_pay_details($data);
                }

            $this->session->set_flashdata('exportData', $result);
            redirect(base_url('accounts/add-employee-payroll'));

            }
            else {
                $msg = "1";
                $this->session->set_flashdata('flashMsg', $msg);
                redirect(base_url('accounts/add-employee-payroll'));
            }
        }
        else {
            $msg = "2";
            $this->session->set_flashdata('flashMsg', $msg);
            redirect(base_url('accounts/add-employee-payroll'));
        }

    }

    public function search_receipts($offset = 0){
        if(isset($_POST['search_receipts'])){
            $this->session->set_userdata('receipt_filter',$_POST);
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('receipt_filter');
            redirect('accounts/search-receipts');
        }
        $data['receipt_filter'] = $this->session->userdata('receipt_filter');

        $postData = $data['receipt_filter'];

        $data['centres'] = $this->tele_enquiry_model->suggested_centre();
        $data['intake'] = $this->account_model->get_intake_months();
        $data['academic_year'] = $this->account_model->get_academic_year();

        $data['get_receipt_data'] = $this->get_receipt_data($offset,$postData,'search_receipts');

        // for calculating total amount of total-cash,check,cc amount of 10 records in last row
        $data['total_paid_cash_amount']['amt_gst'] = 0;
        $data['total_paid_cash_amount']['amt_w/o_gst'] = 0;
        $data['total_paid_cash_amount']['amt_deposited'] = 0;
        $data['total_paid_cash_amount']['balance_amt_deposited'] = 0;
        $data['total_paid_cc_amount']['amt_gst'] = 0;
        $data['total_paid_cc_amount']['amt_w/o_gst'] = 0;
        $data['total_paid_cc_amount']['amt_deposited'] = 0;
        $data['total_paid_cc_amount']['balance_amt_deposited'] = 0;
        $data['total_paid_check_amount']['amt_gst'] = 0;
        $data['total_paid_check_amount']['amt_w/o_gst'] = 0;
        $data['total_paid_check_amount']['amt_deposited'] = 0;
        $data['total_paid_check_amount']['balance_amt_deposited'] = 0;

        for($i=0;$i<count($data['get_receipt_data']['receipt_data']);$i++){
            if(($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'] == 'CASH') || ($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'] == '1') || ($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'] == '') || empty($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'])){
                $data['total_paid_cash_amount']['amt_gst'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_PAID'];
                $data['total_paid_cash_amount']['amt_w/o_gst'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_PAID_FEES'];
                $data['total_paid_cash_amount']['amt_deposited'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_DEPOSITED'];
                $data['total_paid_cash_amount']['balance_amt_deposited'] += $data['get_receipt_data']['receipt_data'][$i]['BALANCE'];
            }
            else if(($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'] == 'CC') || ($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'] == '2')){
                $data['total_paid_cc_amount']['amt_gst'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_PAID'];
                $data['total_paid_cc_amount']['amt_w/o_gst'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_PAID_FEES'];
                $data['total_paid_cc_amount']['amt_deposited'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_DEPOSITED'];
                $data['total_paid_cc_amount']['balance_amt_deposited'] += $data['get_receipt_data']['receipt_data'][$i]['BALANCE'];
            }
            else if(($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'] == 'CHEQUE') || ($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'] == '3')){
                $data['total_paid_check_amount']['amt_gst'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_PAID'];
                $data['total_paid_check_amount']['amt_w/o_gst'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_PAID_FEES'];
                $data['total_paid_check_amount']['amt_deposited'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_DEPOSITED'];
                $data['total_paid_check_amount']['balance_amt_deposited'] += $data['get_receipt_data']['receipt_data'][$i]['BALANCE'];
            }
        }

        // and then pass value individually in array to show total addition
        $data['grand_total']['amt_gst'] = $data['total_paid_cash_amount']['amt_gst'] + $data['total_paid_cc_amount']['amt_gst'] + $data['total_paid_check_amount']['amt_gst'];
        $data['grand_total']['amt_w/o_gst'] = $data['total_paid_cash_amount']['amt_w/o_gst'] + $data['total_paid_cc_amount']['amt_w/o_gst'] + $data['total_paid_check_amount']['amt_w/o_gst'];
        $data['grand_total']['amt_deposited'] = $data['total_paid_cash_amount']['amt_deposited'] + $data['total_paid_cc_amount']['amt_deposited'] + $data['total_paid_check_amount']['amt_deposited'];
        $data['grand_total']['balance_amt_deposited'] = $data['total_paid_cash_amount']['balance_amt_deposited'] + $data['total_paid_cc_amount']['balance_amt_deposited'] + $data['total_paid_check_amount']['balance_amt_deposited'];

        $this->load->admin_view("search_receipts",$data);
    }

/////////// Export to Excel Recept Deleted report //////////////////
public function export_to_excel_deleted_receipts()
{

    $receipt_list['deleted_receipt_filter'] = $this->session->userdata('deleted_receipt_filter');

    $receipt = $this->account_model->export_to_excel_deleted_receipts($receipt_list['deleted_receipt_filter']);

        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);

        $col = 'A';
        for($i=0;$i<count($receipt['fields']);$i++){
            $objPHPExcel->getActiveSheet()->SetCellValue($col.'1', $receipt['fields'][$i]);
            $objPHPExcel->getActiveSheet()->getStyle("A1:X1")->getFont()->setBold(true);
            $col++;
        }

        $j = 2;
        $alphebetCol = 'A';
        foreach ($receipt['details'] as $key => $value) {

            foreach ($receipt['details'][$key] as $index => $data) {
                $objPHPExcel->getActiveSheet()->SetCellValue($alphebetCol.$j,$receipt['details'][$key][$index]);

                $alphebetCol++;
                if($alphebetCol == "AD"){
                    $alphebetCol = 'A';
                }
            }

            $j++;
        }


//die;
        $filename = "Deleted_receipt_Details-" . date("h.i.s.d-m-Y") . ".xls";
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=" . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
}


public function export_to_excel_deleted_other_receipts()
{
    $receipt_list['deleted_other_receipt_reports'] = $this->session->userdata('deleted_other_receipt_reports');

    $receipt = $this->account_model->export_to_excel_deleted_other_receipts($receipt_list['deleted_other_receipt_reports']);


        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);

        $col = 'A';
        for($i=0;$i<count($receipt['fields']);$i++){
            $objPHPExcel->getActiveSheet()->SetCellValue($col.'1', $receipt['fields'][$i]);
            $objPHPExcel->getActiveSheet()->getStyle("A1:Z1")->getFont()->setBold(true);
            $col++;
        }
        $j = 2;
        $alphebetCol = 'A';
        foreach ($receipt['details'] as $key => $value) {

            foreach ($receipt['details'][$key] as $index => $data) {
                $objPHPExcel->getActiveSheet()->SetCellValue($alphebetCol.$j,$receipt['details'][$key][$index]);

                $alphebetCol++;
                if($alphebetCol == "AA"){
                    $alphebetCol = 'A';
                }
            }

            $j++;
        }

        $filename = "Deleted_other_receipt_Details-" . date("h.i.s.d-m-Y") . ".xls";
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=" . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
}


    public function get_receipt_data($offset,$postData,$type){
        if($type == 'deleted_receipts'){
            $config['base_url'] = base_url('accounts/deleted-receipts');
            $config['total_rows'] = $this->account_model->get_deleted_receipts_details($offset,$postData,1,1);
        }
        if($type == 'search_receipts'){
            $config['base_url'] = base_url('accounts/search-receipts');
            $config['total_rows'] = $this->account_model->get_student_receipts_details($offset,$postData,1,1);
        }
        else if($type == "search_other_receipts"){
            $config['base_url'] = base_url('accounts/search-other-receipts');
            $config['total_rows'] = $this->account_model->get_student_other_receipts_details($offset,$postData,1,1);
        }
        else if($type == 'cash_check_deposition'){
            $config['base_url'] = base_url('accounts/cash-check-deposition');
            $config['total_rows'] = $this->account_model->cash_check_deposition_data($offset,$postData,1,1);
        }
        else if($type == 'other_cash_check_deposition'){
            $config['base_url'] = base_url('accounts/other-cash-check-deposition');
            $config['total_rows'] = $this->account_model->other_cash_check_deposition_data($offset,$postData,1,1);
        }
        else if($type == 'admission_report'){
            $config['base_url'] = base_url('accounts/admission_reports');
            $config['total_rows'] = $this->account_model->get_admission_report($offset,$postData,1,1);
        }
        else if($type == 'defaulters_list'){
            $config['base_url'] = base_url('accounts/defaulters_list');
            $config['total_rows'] = $this->account_model->get_defaulters_list($offset,$postData,1,1);
        }
        else if($type == 'outstanding_reports'){
            $config['base_url'] = base_url('accounts/outstanding-reports');
            $config['total_rows'] = $this->account_model->get_outstanding_student_data($offset,$postData,1,1);
        }
        else if($type == 'deleted_other_receipt_reports'){
            $config['base_url'] = base_url('accounts/deleted-other-receipt-reports');
            $config['total_rows'] = $this->account_model->get_deleted_other_receipts($offset,$postData,1,1);
        }
        // else if($type == 'all_receipts_report'){
        //     $config['base_url'] = base_url('accounts/all-receipt-search');
        //     $config['total_rows'] = $this->account_model->get_receipts_report($offset,$postData,1,1);

        // }
        // else if($type == 'all_receipts_edcr'){
        //     $config['base_url'] = base_url('accounts/receipts-edcr');
        //     $config['total_rows'] = $this->account_model->get_receipt_edcr_report($offset,$postData,1,1);
        // }
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);

        if($type == 'deleted_receipts'){
            $receipts_details['receipt_data'] = $this->account_model->get_deleted_receipts_details($offset,$postData,$config['per_page'],0);
        }
        else if($type == 'search_receipts'){
            $receipts_details['receipt_data'] = $this->account_model->get_student_receipts_details($offset,$postData,$config['per_page'],0);
        }
        else if($type == "search_other_receipts"){
            $receipts_details['receipt_data'] = $this->account_model->get_student_other_receipts_details($offset,$postData,$config['per_page'],0);
        }
        else if($type == 'cash_check_deposition'){
            $receipts_details['receipt_data'] = $this->account_model->cash_check_deposition_data($offset,$postData,'100',0);
        }
        else if($type == 'other_cash_check_deposition'){
            $receipts_details['receipt_data'] = $this->account_model->other_cash_check_deposition_data($offset,$postData,'100',0);
        }
        else if($type == 'admission_report'){
            $receipts_details['receipt_data'] = $this->account_model->get_admission_report($offset,$postData,$config['per_page'],0);
        }
        else if($type == 'defaulters_list'){
            $receipts_details['receipt_data'] = $this->account_model->get_defaulters_list($offset,$postData,$config['per_page'],0);
        }
        else if($type == 'outstanding_reports'){
            $receipts_details['receipt_data'] = $this->account_model->get_outstanding_student_data($offset,$postData,$config['per_page'],0);
        }
        else if($type == 'deleted_other_receipt_reports'){
            $receipts_details['receipt_data'] = $this->account_model->get_deleted_other_receipts($offset,$postData,$config['per_page'],0);
        }
        // else if($type == 'all_receipts_report'){
        //     $receipts_details['receipt_data'] = $this->account_model->get_receipts_report($offset,$postData,$config['per_page'],0);

        // }
        // else if($type == 'all_receipts_edcr'){
        //     $receipts_details['receipt_data'] = $this->account_model->get_receipt_edcr_report($offset,$postData,'2500',0);
        // }
        $receipts_details['pagination'] = $this->pagination->create_links();

            return $receipts_details;

    }

    public function admission_receipts($receipt_id,$admission_id,$type){
        $receiptId= $this->encrypt->decode($receipt_id);
        $admissionId= $this->encrypt->decode($admission_id);

        // get individual receipt details by receipt_id
        $data['receipt_details_for_edit'] = $this->account_model->get_receipt_details_for_edit($receiptId,"get_only_the_receipt_data_only");

        // adding receipt_id in receipt_details_for_edit array
        $data['receipt_details_for_edit'][0]['receipt_id'] = $receiptId;
        $data['receipt_details_for_edit'][0]['type'] = $type;

        // fetch all admission_receipts for this admission id
        $data['get_receipt_details_by_admission_id'] = $this->account_model->get_receipt_details_by_admission_id($admissionId);

        //for populating centre and employee for dropdown
        $data['centres'] = $this->tele_enquiry_model->suggested_centre();
        $data['received_by'] = $this->account_model->handled_by();

        // get balance amount in installment table
        $first_balance = $data['receipt_details_for_edit'][0]['TOTALFEES'] - $data['get_receipt_details_by_admission_id'][0]['AMOUNT_PAID_FEES'];
        $data['get_receipt_details_by_admission_id'][0]['BALANCE_AMOUNT'] = $first_balance;
        for($index=1;$index<count($data['get_receipt_details_by_admission_id']);$index++){
            // echo $index;
            $balance = $first_balance - $data['get_receipt_details_by_admission_id'][$index]['AMOUNT_PAID_FEES'];
            $first_balance = $balance;
            $data['get_receipt_details_by_admission_id'][$index]['BALANCE_AMOUNT'] = $first_balance;
        }

        $data['get_admission_installment'] = $this->admission_model->getAdmissionInstallmentTransaction($admissionId);

        if(isset($_POST['update_search_receipts'])) {
            if(($_POST['amount_with_gst'] != "") && ($_POST['received_by'] != "") && ($_POST['remarks'] != "")) {
                $this->session->set_userdata('admission_receipts_details',$_POST);
            }
            else {
                $this->session->set_flashdata('flashMsg', '1');
            redirect(base_url('accounts/admission-receipts/'.$this->encrypt->encode($receiptId).'/'.$this->encrypt->encode($admissionId).'/update'));
            }

            $receipts_details['update_receipts_details'] = $this->account_model->update_receipts_details($this->session->userdata('admission_receipts_details'),"update");
            if($receipts_details['update_receipts_details']){
              echo '<script language="javascript">';
              echo 'alert("Receipt updated successfully.")';
              echo '</script>';
            }
            $this->session->set_flashdata('flashMsgRemarks', $_POST['remarks']);
            redirect(base_url('accounts/admission-receipts/'.$this->encrypt->encode($receiptId).'/'.$this->encrypt->encode($admissionId).'/update'));
        }
        else if(isset($_POST['delete_search_receipts'])) {
            if(($_POST['amount_with_gst'] != "") && ($_POST['received_by'] != "") && ($_POST['remarks'] != "")) {
                $this->session->set_userdata('admission_receipts_details',$_POST);
            }
            else {
                $this->session->set_flashdata('flashMsg', '1');
                redirect(base_url('accounts/admission-receipts/'.$this->encrypt->encode($receiptId).'/'.$this->encrypt->encode($admissionId).'/delete'));
            }

            $receipts_details['update_receipts_details'] = $this->account_model->update_receipts_details($this->session->userdata('admission_receipts_details'),"delete");
            if($receipts_details['update_receipts_details']){
              echo '<script language="javascript">';
              echo 'alert("Receipt deleted successfully.")';
              echo '</script>';
            }
            redirect(base_url('accounts/search-receipts'));
        }
        else{
            $this->load->admin_view("admission_receipts",$data);
        }
    }

    public function print_receipts($receipt_id,$type){
        $receiptId= $this->encrypt->decode($receipt_id);

        $data['receipt_details_for_edit'] = $this->account_model->get_receipt_details_for_edit($receiptId,$type);
        $data['receipt_details_for_edit'][0]['AMOUNT_IN_WORDS'] = $this->convert_number($data['receipt_details_for_edit'][0]['AMOUNT_PAID']);

        $this->load->helper('currency');
        $this->load->helper('pdf_helper');
        $this->load->admin_view("print_student_receipt",$data);
    }

    public function print_summary($receipt_id,$admission_id){
        $receiptId= $this->encrypt->decode($receipt_id);

        $data['receipt_details_for_edit'] = $this->account_model->get_receipt_details_for_edit($receiptId,"only_for_gtting_receipt_details");

        // for handeling admission_id if its blank
        if($admission_id == 0){
            $admissionId = $data['receipt_details_for_edit'][0]['ADMISSION_ID'];
        }
        else{
            $admissionId= $this->encrypt->decode($admission_id);
        }

        // get course name by course id
        $course_id = explode(",", $data['receipt_details_for_edit'][0]['COURSE_TAKEN']);

        if(is_numeric($course_id[0])){
            $data['courses'] = $this->tele_enquiry_model->course_interested($course_id);

            foreach($data['courses'] as $course){
                 $course_name .= $course['COURSE_NAME'].", ";
            }

            $data['receipt_details_for_edit'][0]['COURSE_TAKEN'] = trim($course_name,", ");
        }

        $data['get_admission_installment'] = $this->admission_model->getAdmissionInstallmentTransaction($admissionId);
        $data['fees_paid_till_date'] = $this->account_model->feesPaidTillDate($admissionId,$receipt_id,$data['receipt_details_for_edit'][0]['PAYMENT_DATE']);
        $data['fees_amount_till_date'] = $this->account_model->feesAmountTillDate($admissionId,$receipt_id,$data['receipt_details_for_edit'][0]['PAYMENT_DATE']);

        $this->load->helper('currency');
        $this->load->helper('pdf_helper');
        $this->load->admin_view("print_student_summary",$data);
    }

    public function convert_number($number) {
        if (($number < 0) || ($number > 999999999)) {
            throw new Exception("Number is out of range");
        }
        $Gn = floor($number / 1000000);
        /* Millions (giga) */
        $number -= $Gn * 1000000;
        $kn = floor($number / 1000);
        /* Thousands (kilo) */
        $number -= $kn * 1000;
        $Hn = floor($number / 100);
        /* Hundreds (hecto) */
        $number -= $Hn * 100;
        $Dn = floor($number / 10);
        /* Tens (deca) */
        $n = $number % 10;
        /* Ones */
        $res = "";
        if ($Gn) {
            $res .= $this->convert_number($Gn) .  "Million";
        }
        if ($kn) {
            $res .= (empty($res) ? "" : " ") .$this->convert_number($kn) . " Thousand";
        }
        if ($Hn) {
            $res .= (empty($res) ? "" : " ") .$this->convert_number($Hn) . " Hundred";
        }
        $ones = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen");
        $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eigthy", "Ninety");
        if ($Dn || $n) {
            if (!empty($res)) {
                $res .= " and ";
            }
            if ($Dn < 2) {
                $res .= $ones[$Dn * 10 + $n];
            } else {
                $res .= $tens[$Dn];
                if ($n) {
                    $res .= "-" . $ones[$n];
                }
            }
        }
        if (empty($res)) {
            $res = "zero";
        }
        return $res;
    }

    public function cash_check_deposition($offset = 0){
        $center_id = $this->input->post('centre');

        if (!$center_id) {
            $center_id = $this->session->userdata('admin_data')[0]['CENTRE_ID'];
            if (empty($this->session->userdata('cash_check_deposition'))) {
                $this->session->set_userdata('cash_check_deposition',$_POST);
            }
            else{
                $cashCheckData = $this->session->userdata('cash_check_deposition');
                $center_id = $cashCheckData['centre'];
            }
        }
        else{
           $this->session->set_userdata('cash_check_deposition',$_POST);
        }

        $data['cash_check_deposition'] = $this->session->userdata('cash_check_deposition');
        $data['cash_check_search_data'] = $this->get_receipt_data($offset,$center_id,'cash_check_deposition');

        $data['centres'] = $this->tele_enquiry_model->suggested_centre();
        $this->load->admin_view("cash_check_deposition",$data);
    }

    public function update_cash_cheque_data($type){
        $receiptid = $this->input->post('receipt_id');
        $amountdept = $this->input->post('amount_dept');
        $depositdate = $this->input->post('deposit_date');
        $depositslipno = $this->input->post('deposit_slip_no');
        $remarks = $this->input->post('remarks');

        $receipt_exist = $this->account_model->receipt_exist($receiptid,$type);

        if($receipt_exist){
            $response = $this->account_model->update_cash_cheque_deposition_data($receiptid,$amountdept,$depositdate,$depositslipno,$remarks,$type);
        }
        echo json_encode($response);
    }

    public function transfer_receipts(){
        $this->load->admin_view("transfer_receipts");
    }

    public function get_transfer_receipt_detail($page){
        $admission_id = $this->input->post('admission_id');
        $data['studData'] = $this->student_model->getStudentData($admission_id);
        if($page == "other"){
            $data['receiptDetails'] = $this->admission_model->getOtherReceiptsData($admission_id);
        }
        else{
            $data['receiptDetails'] = $this->admission_model->getAdmissionReceipt($admission_id);
        }

        if($page != "other"){
            if(count($data['receiptDetails'])){
                $totalFees = $data['receiptDetails'][0]['TOTALFEES'];
                $deductAmount = $data['receiptDetails'][0]['AMOUNT_PAID'] - $data['receiptDetails'][0]['AMOUNT_PAID_SERVICETAX'];

                for($i=0;$i<count($data['receiptDetails']);$i++){
                    $j = $totalFees - $deductAmount;

                    $data['receiptDetails'][$i]['BALANCES'] = $j;       //adding this value in receiptDetails array
                    $totalFees = $j;

                    if($i != (count($data['receiptDetails']) - 1)){
                        $deductAmount = $data['receiptDetails'][$i + 1]['AMOUNT_PAID'] - $data['receiptDetails'][$i + 1]['AMOUNT_PAID_SERVICETAX'];
                    }

                    $j = "";
                }
            }
        }

        $data['response'] = $page;

        echo json_encode($data);
    }

    public function transfer_selected_receipts($page){
        $from_admission_id = $this->input->post('from_admission_id');
        $to_admission_id = $this->input->post('to_admission_id');
        $remarks = $this->input->post('remarks');
        $receipt_data = $this->input->post('receipts_data');

        if($page == "other"){
            $update_receipts = $this->account_model->update_transfer_other_receipts($from_admission_id,$to_admission_id,$remarks,$receipt_data);
        }
        else{
            $update_receipts = $this->account_model->update_transfer_receipts($from_admission_id,$to_admission_id,$remarks,$receipt_data);
        }

        echo json_encode($update_receipts);
    }

    public function search_other_receipts($offset = 0){
        if(isset($_POST['search_other_receipts'])){
            $this->session->set_userdata('other_receipt_filter',$_POST);
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('other_receipt_filter');
            redirect('accounts/search-other-receipts');
        }
        $data['other_receipt_filter'] = $this->session->userdata('other_receipt_filter');

        $postData = $data['other_receipt_filter'];

        $data['centres'] = $this->tele_enquiry_model->suggested_centre();
        $data['intake'] = $this->account_model->get_intake_months();
        $data['academic_year'] = $this->account_model->get_academic_year();

        $data['get_receipt_data'] = $this->get_receipt_data($offset,$postData,'search_other_receipts');

        // for calculating total amount of total-cash,check,cc amount of 10 records in last row
        $data['total_paid_cash_amount']['amt_gst'] = 0;
        $data['total_paid_cash_amount']['amt_w/o_gst'] = 0;
        $data['total_paid_cash_amount']['amt_deposited'] = 0;
        $data['total_paid_cash_amount']['balance_amt_deposited'] = 0;
        $data['total_paid_cc_amount']['amt_gst'] = 0;
        $data['total_paid_cc_amount']['amt_w/o_gst'] = 0;
        $data['total_paid_cc_amount']['amt_deposited'] = 0;
        $data['total_paid_cc_amount']['balance_amt_deposited'] = 0;
        $data['total_paid_check_amount']['amt_gst'] = 0;
        $data['total_paid_check_amount']['amt_w/o_gst'] = 0;
        $data['total_paid_check_amount']['amt_deposited'] = 0;
        $data['total_paid_check_amount']['balance_amt_deposited'] = 0;

        for($i=0;$i<count($data['get_receipt_data']['receipt_data']);$i++){
            if(($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'] == 'CASH') || ($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'] == '') || empty($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE']) || ($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'] == '1')){
                $data['total_paid_cash_amount']['amt_gst'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_PAID'];
                $data['total_paid_cash_amount']['amt_w/o_gst'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_PAID_FEES'];
                $data['total_paid_cash_amount']['amt_deposited'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_DEPOSITED'];
                $data['total_paid_cash_amount']['balance_amt_deposited'] += $data['get_receipt_data']['receipt_data'][$i]['BALANCE'];
            }
            else if(($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'] == 'CC') || ($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'] == '2')){
                $data['total_paid_cc_amount']['amt_gst'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_PAID'];
                $data['total_paid_cc_amount']['amt_w/o_gst'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_PAID_FEES'];
                $data['total_paid_cc_amount']['amt_deposited'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_DEPOSITED'];
                $data['total_paid_cc_amount']['balance_amt_deposited'] += $data['get_receipt_data']['receipt_data'][$i]['BALANCE'];
            }
            else if(($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'] == 'CHEQUE') || ($data['get_receipt_data']['receipt_data'][$i]['PAYMENT_TYPE'] == '3')){
                $data['total_paid_check_amount']['amt_gst'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_PAID'];
                $data['total_paid_check_amount']['amt_w/o_gst'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_PAID_FEES'];
                $data['total_paid_check_amount']['amt_deposited'] += $data['get_receipt_data']['receipt_data'][$i]['AMOUNT_DEPOSITED'];
                $data['total_paid_check_amount']['balance_amt_deposited'] += $data['get_receipt_data']['receipt_data'][$i]['BALANCE'];
            }
        }

        // and then pass value individually in array to show total addition
        $data['grand_total']['amt_gst'] = $data['total_paid_cash_amount']['amt_gst'] + $data['total_paid_cc_amount']['amt_gst'] + $data['total_paid_check_amount']['amt_gst'];
        $data['grand_total']['amt_w/o_gst'] = $data['total_paid_cash_amount']['amt_w/o_gst'] + $data['total_paid_cc_amount']['amt_w/o_gst'] + $data['total_paid_check_amount']['amt_w/o_gst'];
        $data['grand_total']['amt_deposited'] = $data['total_paid_cash_amount']['amt_deposited'] + $data['total_paid_cc_amount']['amt_deposited'] + $data['total_paid_check_amount']['amt_deposited'];
        $data['grand_total']['balance_amt_deposited'] = $data['total_paid_cash_amount']['balance_amt_deposited'] + $data['total_paid_cc_amount']['balance_amt_deposited'] + $data['total_paid_check_amount']['balance_amt_deposited'];
        $this->load->admin_view("search_other_receipts",$data);
    }

    public function admission_other_receipts($receipt_id,$admission_id,$type){
        $receiptId= $this->encrypt->decode($receipt_id);
        $admissionId= $this->encrypt->decode($admission_id);

        // get individual receipt details by receipt_id
        $data['receipt_details_for_edit'] = $this->account_model->get_other_receipt_details_for_edit($receiptId,"only_for_receipt_data_edit");

        // adding receipt_id in receipt_details_for_edit array
        $data['receipt_details_for_edit'][0]['receipt_id'] = $receiptId;
        $data['receipt_details_for_edit'][0]['type'] = $type;

        // fetch all admission_receipts for this admission id
        $data['get_receipt_details_by_admission_id'] = $this->account_model->get_other_receipt_details_by_admission_id($admissionId);

        //for populating centre and employee for dropdown
        $data['centres'] = $this->tele_enquiry_model->suggested_centre();
        $data['received_by'] = $this->account_model->handled_by();

        $data['payFor'] = $this->admission_model->payFor();

        // get balance amount in installment table
        $first_balance = $data['receipt_details_for_edit'][0]['TOTALFEES'] - $data['get_receipt_details_by_admission_id'][0]['AMOUNT_PAID_FEES'];
        $data['get_receipt_details_by_admission_id'][0]['BALANCE_AMOUNT'] = $first_balance;
        for($index=1;$index<count($data['get_receipt_details_by_admission_id']);$index++){
            // echo $index;
            $balance = $first_balance - $data['get_receipt_details_by_admission_id'][$index]['AMOUNT_PAID_FEES'];
            $first_balance = $balance;
            $data['get_receipt_details_by_admission_id'][$index]['BALANCE_AMOUNT'] = $first_balance;
        }

        $data['get_admission_installment'] = $this->admission_model->getAdmissionInstallmentTransaction($admissionId);

        if(isset($_POST['update_search_other_receipts'])) {
            if($_POST['amount_with_gst'] != "" && $_POST['received_by'] != "" && $_POST['remarks'] != "") {
                $this->session->set_userdata('admission_other_receipts_details',$_POST);
            }
            else {
                $this->session->set_flashdata('flashMsg', '1');
            redirect(base_url('accounts/admission-other-receipts/'.$this->encrypt->encode($receiptId).'/'.$this->encrypt->encode($admissionId).'/update'));
            }

            $receipts_details['update_receipts_details'] = $this->account_model->update_other_receipts_details($this->session->userdata('admission_other_receipts_details'),"update");
            $this->session->set_flashdata('flashMsgRemarks', $_POST['remarks']);
            redirect(base_url('accounts/admission-other-receipts/'.$this->encrypt->encode($receiptId).'/'.$this->encrypt->encode($admissionId).'/update'));
        }
        else if(isset($_POST['delete_search_other_receipts'])) {
            if($_POST['amount_with_gst'] != "" && $_POST['received_by'] != "" && $_POST['remarks'] != "") {
                $this->session->set_userdata('admission_other_receipts_details',$_POST);
            }
            else {
                $this->session->set_flashdata('flashMsg', '1');
            redirect(base_url('accounts/admission-other-receipts/'.$this->encrypt->encode($receiptId).'/'.$this->encrypt->encode($admissionId).'/delete'));
            }

            $receipts_details['update_receipts_details'] = $this->account_model->update_other_receipts_details($this->session->userdata('admission_other_receipts_details'),"delete");
            redirect(base_url('accounts/search-other-receipts'));
        }
        else{
            $this->load->admin_view("admission_other_receipts",$data);
        }
    }

    public function print_other_receipts($receipt_id,$type){
        $receiptId= $this->encrypt->decode($receipt_id);

        $data['receipt_details_for_edit'] = $this->account_model->get_other_receipt_details_for_edit($receiptId,$type);
        $data['receipt_details_for_edit'][0]['AMOUNT_IN_WORDS'] = $this->convert_number($data['receipt_details_for_edit'][0]['AMOUNT_PAID']);

        $this->load->helper('currency');
        $this->load->helper('pdf_helper');
        $this->load->admin_view("print_student_receipt",$data);
    }

    public function transfer_other_receipts(){
        $this->load->admin_view("transfer_other_receipts");
    }

    public function get_transfer_other_receipt_detail(){
        $admission_id = $this->input->post('admission_id');
        $data['studData'] = $this->student_model->getStudentData($admission_id);
        $data['receiptDetails'] = $this->admission_model->getOtherReceiptsData($admission_id);

        if(count($data['receiptDetails'])){
            $totalFees = $data['receiptDetails'][0]['TOTALFEES'];
            $deductAmount = $data['receiptDetails'][0]['AMOUNT_PAID'] - $data['receiptDetails'][0]['AMOUNT_PAID_SERVICETAX'];

            for($i=0;$i<count($data['receiptDetails']);$i++){
                $j = $totalFees - $deductAmount;

                $data['receiptDetails'][$i]['BALANCES'] = $j;       //adding this value in receiptDetails array
                $totalFees = $j;

                if($i != (count($data['receiptDetails']) - 1)){
                    $deductAmount = $data['receiptDetails'][$i + 1]['AMOUNT_PAID'] - $data['receiptDetails'][$i + 1]['AMOUNT_PAID_SERVICETAX'];
                }

                $j = "";
            }
        }
        echo json_encode($data);
    }

    public function transfer_selected_other_receipts(){
        $from_admission_id = $this->input->post('from_admission_id');
        $to_admission_id = $this->input->post('to_admission_id');
        $remarks = $this->input->post('remarks');
        $receipt_data = $this->input->post('receipts_data');

        $update_receipts = $this->account_model->update_transfer_receipts($from_admission_id,$to_admission_id,$remarks,$receipt_data);

        echo json_encode($update_receipts);
    }

    public function other_cash_check_deposition($offset = 0){
        $center_id = $this->input->post('centre');

        if (!$center_id) {
            $center_id = $this->session->userdata('admin_data')[0]['CENTRE_ID'];
            if (empty($this->session->userdata('other_cash_check_deposition'))) {
                $this->session->set_userdata('other_cash_check_deposition',$_POST);
            }
            else{
                $cashCheckData = $this->session->userdata('other_cash_check_deposition');
                $center_id = $cashCheckData['centre'];
            }
        }
        else{
           $this->session->set_userdata('other_cash_check_deposition',$_POST);
        }

        $data['other_cash_check_deposition'] = $this->session->userdata('other_cash_check_deposition');
        $data['cash_check_search_data'] = $this->get_receipt_data($offset,$center_id,'other_cash_check_deposition');

        $data['centres'] = $this->tele_enquiry_model->suggested_centre();
        $this->load->admin_view("other_cash_check_deposition",$data);
    }

    public function admission_reports($offset = 0){
        $data['centres'] = $this->tele_enquiry_model->suggested_centre();
        $data['received_by'] = $this->account_model->handled_by();

        if(isset($_POST['search_admission_data'])){
            $this->session->set_userdata('admission_reports',$_POST);
        }
        if(isset($_POST['search_admission_reset'])){
            $this->session->unset_userdata('admission_reports');
            redirect('accounts/admission_reports');
        }

        $data['search_data'] = $this->session->userdata('admission_reports');

        $data['admission_report'] = $this->get_receipt_data($offset,$data['search_data'],'admission_report');

        for($i=0;$i<count($data['admission_report']['receipt_data']);$i++){
            $receipts = $this->account_model->get_receipt_details_by_admission_id($data['admission_report']['receipt_data'][$i]['ADMISSION_ID']);

            $paid_amount=0;
            for($j=0;$j<count($receipts);$j++){
                $paid_amount += $receipts[$j]['AMOUNT_PAID'];
            }

            $data['admission_report']['receipt_data'][$i]['FEES_PAID'] = $paid_amount;

        }

        $grand_total_fees = 0;
        $grand_total_fees_paid = 0;
        foreach($data['admission_report']['receipt_data'] as $adm_rep){
            $grand_total_fees += $adm_rep['TOTALFEES'];
            $grand_total_fees_paid += $adm_rep['FEES_PAID'];
        }

        $data["total_fees"] = $grand_total_fees;
        $data["total_fees_paid"] = $grand_total_fees_paid;
        $this->load->admin_view("admission_reports",$data);
    }

    public function defaulters_list($offset = '0'){
        $data['centres'] = $this->tele_enquiry_model->suggested_centre();
        if(isset($_POST['generate_defaulters_list'])){
            $this->session->set_userdata('defaulters_list',$_POST);
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('defaulters_list');
            redirect('accounts/defaulters_list');
        }

        $data['defaulters_session_data'] = $this->session->userdata('defaulters_list');



        $data['list'] = $this->get_receipt_data($offset,$data['defaulters_session_data'],'defaulters_list');

        foreach ($data['list']['receipt_data'] as $key => $value) {


        $admId = $data['list']['receipt_data'][$key]['ADMISSION_ID'];

        $installmentPlanArrDate = explode(",", $value['DUEDATE']);

        $installmentPlanArrAmount = explode(",", $value['DUE_AMOUNT']);

        $curDate = date('Y-m-d');

        $TotalamtTobePaid =0;
        $mostRecent = array();
        for ($i=0; $i <count($installmentPlanArrDate) ; $i++) {
            $installmentPlanArr[] = array(
                    'DUEDATE' => $installmentPlanArrDate[$i],
                    'DUE_AMOUNT' => $installmentPlanArrAmount[$i]
                );

            if ($installmentPlanArrDate[$i]<$curDate)
            {
                $mostRecent = $installmentPlanArrDate[$i];
                $TotalamtTobePaid = $TotalamtTobePaid+$installmentPlanArrAmount[$i];
            }

        }

        $curMonthYear = date("m-Y");

        foreach ($installmentPlanArr as $insPlan) {

            $insDate = date("Y-m-d", strtotime($insPlan['DUEDATE']));

            $insMonthYear = date("m-Y", strtotime($insPlan['DUEDATE']));


            if ($curMonthYear == $insMonthYear) {
                $insMonthIncDays = date("Y-m-d",strtotime("+3 day", strtotime($insDate)));

                if($insMonthIncDays >= $curMonthYear){
                    $admReceipt['expDate'] = '1';
                }
                else{
                    $admReceipt['expDate'] = '0';
                }
            }
        }


        $getAdmRec = $this->admission_model->getAdmissionReceipt($admId);
        $totAmtPaid =0;
        for ($i=0; $i <count($getAdmRec) ; $i++) {
            $getAdmRec[$i]['ADMISSION_RECEIPTS_ID_ENC'] = $this->encrypt->encode($getAdmRec[$i]['ADMISSION_RECEIPTS_ID']);
                $totAmtPaid = $totAmtPaid + $getAdmRec[$i]['AMOUNT_PAID'];

        }

        if(isset($mostRecent))
        {
            $mostRecent = ($mostRecent);
            $DateAfterThreeDay = date( "Y-m-d", strtotime( "$mostRecent +3 day" ) );
            $curDate = new DateTime($curDate);
            $date2 = new DateTime($DateAfterThreeDay);
            $diff = $date2->diff($curDate)->format("%a");
        }

        if($totAmtPaid<$TotalamtTobePaid)
        {
            $penalty = 100*$diff;


            $ArrayadmId[] = $admId;
            /*$data['dfl'] = array(array(
                "ADMISSION_ID"=>$data['list']['receipt_data'][$key]['ADMISSION_ID'],
                "DUEDATE"=>$data['list']['receipt_data'][$key]['DUEDATE'],
                "DUE_AMOUNT"=>$data['list']['receipt_data'][$key]['DUE_AMOUNT'],
                "STUDENT_NAME"=>$data['list']['receipt_data'][$key]['STUDENT_NAME'],
                "ADMISSION_DATE"=>$data['list']['receipt_data'][$key]['ADMISSION_DATE'],
                "COURSE_TAKEN"=>$data['list']['receipt_data'][$key]['COURSE_TAKEN'],
                "TOTALFEES"=>$data['list']['receipt_data'][$key]['TOTALFEES'],
                "totAmtPaid"=>$totAmtPaid,
                "TotalamtTobePaid"=>$TotalamtTobePaid
                ));*/
                    //print"<pre>";



        }
        else
        {
            $penalty ='0';
        }

        }


         /* Pagination starts */
        $config['base_url'] = base_url('accounts/defaulters_list/');
        $config['total_rows'] = $this->admission_model->defaultersData_count($ArrayadmId);
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */
        $data['dfl'] = $this->admission_model->defaultersData($offset,$config['per_page'],$ArrayadmId);

        $data['pagination'] = $this->pagination->create_links();

         /*print"<pre>";
        print_r($data['dfl']);*/

        /*
        $data['admReceipt'] = $getAdmRec;
        $data['insPlan'] = $installmentPlanArr;
        $data['penalty'] = $penalty;
        */


        $this->load->admin_view("defaulters_list",$data);
    }

    function defaulter_penality($admId='')
    {
print $admId;
        $installmentPlan = $this->admission_model->getAdmissionInstallmentTransaction($admId);

        print"<pre>";
        print_r($installmentPlan);

        $installmentPlanArrDate = explode(",", $installmentPlan[0]['DUEDATE']);

        $installmentPlanArrAmount = explode(",", $installmentPlan[0]['DUE_AMOUNT']);

        $curDate = date('Y-m-d');

        $TotalamtTobePaid =0;

        for ($i=0; $i <count($installmentPlanArrDate) ; $i++) {
            $installmentPlanArr[] = array(
                    'DUEDATE' => $installmentPlanArrDate[$i],
                    'DUE_AMOUNT' => $installmentPlanArrAmount[$i]
                );

            if ($installmentPlanArrDate[$i]<$curDate)
            {
                $mostRecent[] = $installmentPlanArrDate[$i];
                $TotalamtTobePaid = $TotalamtTobePaid+$installmentPlanArrAmount[$i];
            }

        }

        $curMonthYear = date("m-Y");

        foreach ($installmentPlanArr as $insPlan) {

            $insDate = date("Y-m-d", strtotime($insPlan['DUEDATE']));

            $insMonthYear = date("m-Y", strtotime($insPlan['DUEDATE']));


            if ($curMonthYear == $insMonthYear) {
                $insMonthIncDays = date("Y-m-d",strtotime("+3 day", strtotime($insDate)));

                if($insMonthIncDays >= $curMonthYear){
                    $admReceipt['expDate'] = '1';
                }
                else{
                    $admReceipt['expDate'] = '0';
                }
            }
        }


        $getAdmRec = $this->admission_model->getAdmissionReceipt($admId);
        $totAmtPaid =0;
        for ($i=0; $i <count($getAdmRec) ; $i++) {
            $getAdmRec[$i]['ADMISSION_RECEIPTS_ID_ENC'] = $this->encrypt->encode($getAdmRec[$i]['ADMISSION_RECEIPTS_ID']);
                $totAmtPaid = $totAmtPaid + $getAdmRec[$i]['AMOUNT_PAID'];

        }

        if(isset($mostRecent))
        {
            $mostRecent = max($mostRecent);
            $DateAfterThreeDay = date( "Y-m-d", strtotime( "$mostRecent +3 day" ) );
            $curDate = new DateTime($curDate);
            $date2 = new DateTime($DateAfterThreeDay);
            $diff = $date2->diff($curDate)->format("%a");
        }

        if($totAmtPaid<$TotalamtTobePaid)
        {
            $penalty = 100*$diff;
        }
        else
        {
            $penalty ='0';
        }

        $admReceipt['admReceipt'] = $getAdmRec;
        $admReceipt['insPlan'] = $installmentPlanArr;
        $admReceipt['penalty'] = $penalty;
        return $admReceipt;
    }

    public function outstanding_reports($offset = 0){
        $data['centres'] = $this->tele_enquiry_model->suggested_centre();
        if(isset($_POST['get_outstanding_report'])){
            $this->session->set_userdata('outstanding_reports',$_POST);
        }
        if(isset($_POST['outstanding_report_reset'])){
            $this->session->unset_userdata('outstanding_reports');
            redirect('accounts/outstanding-reports');
        }

        $data['outstanding_reports'] = $this->session->userdata('outstanding_reports');


        $data['outstanding_data'] = array();

        if($this->session->userdata('outstanding_reports')){
          $courses = $this->tele_enquiry_model->course_interested();
          $modules = $this->tele_enquiry_model->get_modules();

            $data['outstanding_data'] = $this->get_receipt_data($offset,$data['outstanding_reports'],'outstanding_reports');

            $data['grand_total_fees'] = "";
            $data['grand_total_fees_paid'] = "";
            $data['grand_total_balance_fees'] = "";

            for($i=0;$i<count($data['outstanding_data']['receipt_data']);$i++){

              if(!preg_match("/[a-z]/i", $data['outstanding_data']['receipt_data'][$i]['COURSE_TAKEN'])){
                $courses_arr = explode(",",$data['outstanding_data']['receipt_data'][$i]['COURSE_TAKEN']);
                $courseNames = '';
                for($j=0;$j<count($courses);$j++){
                  if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
                      $courseNames .= $courses[$j]['COURSE_NAME'].',';
                  }
                }

                $modules_arr = explode(",",$data['outstanding_data']['receipt_data'][$i]['MODULE_TAKEN']);
                $moduleNames = '';
                for($j=0;$j<count($modules);$j++){
                  if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
                      $moduleNames .= $modules[$j]['MODULE_NAME'].',';
                  }
                }

                $data['outstanding_data']['receipt_data'][$i]['COURSE_TAKEN'] = trim((trim($moduleNames,',').','.trim($courseNames)),',');
              }

    //          for getting receipt report  by admission_id
                $data['get_receipt_data'] = $this->account_model->get_receipt_details_by_admission_id($data['outstanding_data']['receipt_data'][$i]['ADMISSION_ID']);

    //          code for taking first and last receipt details
                $total_fees = 0;
                for($j=0;$j<count($data['get_receipt_data']);$j++){
                    if(count($data['get_receipt_data']) != 0){
                        $count = count($data['get_receipt_data']) - 1;
                        $admDateRep = str_replace('-', '/', $data['get_receipt_data'][$j]['PAYMENT_DATE']);
                        $date =  date("d/m/Y", strtotime($admDateRep) );

                        if(($data['get_receipt_data'][$j]['PAYMENT_TYPE'] != "CASH") || ($data['get_receipt_data'][$j]['PAYMENT_TYPE'] != "") || ($data['get_receipt_data'][$j]['PAYMENT_TYPE'] != "1")){

                            if(is_numeric($data['get_receipt_data'][$j]['PAYMENT_TYPE'])){
                              if($data['get_receipt_data'][$j]['PAYMENT_TYPE'] != "1"){
                                $pay_type = "CASH";
                              }
                            }
                            else{
                              $pay_type = $data['get_receipt_data'][0]['PAYMENT_TYPE'];
                            }

                            $data['outstanding_data']['receipt_data'][$i]['first_receipt_data'] = $date." (".trim($data['get_receipt_data'][0]['ADMISSION_RECEIPT_NO']).") ".$pay_type;
                            if(count($data['get_receipt_data']) >= 1){
                                $data['outstanding_data']['receipt_data'][$i]['last_receipt_data'] = $date." (".trim($data['get_receipt_data'][$count]['ADMISSION_RECEIPT_NO']).") ".$pay_type;
                            }
                            else {
                                $data['outstanding_data']['receipt_data'][$i]['last_receipt_data'] = 0;
                            }
                        }
                        else{
                            if(is_numeric($data['get_receipt_data'][$j]['PAYMENT_TYPE'])){
                              if($data['get_receipt_data'][$j]['PAYMENT_TYPE'] != "2"){
                                $pay_type = "CC";
                              }
                              if($data['get_receipt_data'][$j]['PAYMENT_TYPE'] != "3"){
                                $pay_type = "CHEQUE";
                              }
                            }
                            else{
                              $pay_type = $data['get_receipt_data'][0]['PAYMENT_TYPE'];
                            }

                            $data['outstanding_data']['receipt_data'][$i]['first_receipt_data'] = $date." (".trim($data['get_receipt_data'][0]['ADMISSION_RECEIPT_NO']).") ".$pay_type;
                            if(count($data['get_receipt_data']) >= 1){
                                $data['outstanding_data']['receipt_data'][$i]['last_receipt_data'] = $date." (".trim($data['get_receipt_data'][$count]['ADMISSION_RECEIPT_NO']).") ".$pay_type;
                            }
                            else{
                                $data['outstanding_data']['receipt_data'][$i]['last_receipt_data'] = 0;
                            }
                        }

                        // Sum receipt amount as total amount paid
                        $total_fees += $data['get_receipt_data'][$j]['AMOUNT_PAID_FEES'];
                    }
                    else{
                        $data['outstanding_data']['receipt_data'][$i]['first_receipt_data'] = 0;
                        $data['outstanding_data']['receipt_data'][$i]['last_receipt_data'] = 0;
                        $total_fees = 0;
                    }
                }

    //          for calculating fees paid and balance fees
                $data['outstanding_data']['receipt_data'][$i]['FEES_PAID'] = $total_fees;
                $data['outstanding_data']['receipt_data'][$i]['BALANCE_FEES'] = $data['outstanding_data']['receipt_data'][$i]['TOTALFEES'] - $total_fees;


    //          condition for inserting zero value when student receipt not returned in array
                if(!array_key_exists("first_receipt_data",$data['outstanding_data']['receipt_data'][$i])){
                    $data['outstanding_data']['receipt_data'][$i]['first_receipt_data'] = 0;
                    $data['outstanding_data']['receipt_data'][$i]['last_receipt_data'] = 0;
                    $total_fees = 0;
                }


    //          for geting grand total of fees,fees_paid and balance fees
                $data['grand_total_fees'] += $data['outstanding_data']['receipt_data'][$i]['TOTALFEES'];
                $data['grand_total_fees_paid'] += $data['outstanding_data']['receipt_data'][$i]['FEES_PAID'];
                $data['grand_total_balance_fees'] += $data['outstanding_data']['receipt_data'][$i]['BALANCE_FEES'];

            }

        }

        $this->load->admin_view("outstanding_report",$data);
    }

    public function deleted_receipts($offset = 0){
        $data['centres'] = $this->tele_enquiry_model->suggested_centre();

        if(isset($_POST['get_deleted_receipt'])){
            $this->session->set_userdata('deleted_receipt_filter',$_POST);
        }
        if(isset($_POST['deleted_receipt_reset'])){
            $this->session->unset_userdata('deleted_receipt_filter');
            redirect('accounts/deleted-receipts');
        }
        $data['deleted_receipt_filter'] = $this->session->userdata('deleted_receipt_filter');

        $data['deleted_receipt_reports'] = array();

        if($this->session->userdata('deleted_receipt_filter')){
            if(!(array_key_exists("cheque_bounce_only",$data['deleted_receipt_filter']))){
                $data['deleted_receipt_filter']['cheque_bounce_only'] = "off";
            }

            $data['deleted_receipt_reports'] = $this->get_receipt_data($offset,$data['deleted_receipt_filter'],'deleted_receipts');

            $receipt_no = array();
            for($j=0;$j<count($data['deleted_receipt_reports']['receipt_data']);$j++){
                $receipt_no = $this->account_model->get_bounce_penalty_receipt_no($data['deleted_receipt_reports']['receipt_data'][$j]['ADMISSION_ID']);

                $receiptnos = array();
                if(!empty($receipt_no)){
                    for($k=0;$k<count($receipt_no);$k++){
                        array_push($receiptnos,$receipt_no[$k]['PENALTY_RECEIPT_NO']);
                        $penalty_receipt_no = implode(", ",$receiptnos);
                    }
                    $data['deleted_receipt_reports']['receipt_data'][$j]['bounce_receipt_no'] = $penalty_receipt_no;
                }
                else{
                    $data['deleted_receipt_reports']['receipt_data'][$j]['bounce_receipt_no'] = '';
                }

            }
        }

        $this->load->admin_view("deleted_receipts",$data);
    }

    // for deleted other receipt page normal
    public function deleted_receipt_reports($offset = 0){
        $data['centres'] = $this->tele_enquiry_model->suggested_centre();
        if(isset($_POST['get_other_deleted_receipt'])){
            $this->session->set_userdata('deleted_other_receipt_reports',$_POST);
        }
        if(isset($_POST['deleted_other_receipt_reset'])){
            $this->session->unset_userdata('deleted_other_receipt_reports');
            redirect('accounts/deleted-other-receipt-reports');
        }

        $data['deleted_other_receipt_reports'] = $this->session->userdata('deleted_other_receipt_reports');

        $data['deleted_receipt_reports'] = array();

        if($this->session->userdata('deleted_other_receipt_reports')){
            if(!(array_key_exists("cheque_bounce_only",$data['deleted_other_receipt_reports']))){
                $data['deleted_other_receipt_reports']['cheque_bounce_only'] = "off";
            }

            $data['deleted_receipt_reports'] = $this->get_receipt_data($offset,$data['deleted_other_receipt_reports'],'deleted_other_receipt_reports');
        }

        $this->load->admin_view("other_receipt_deleted_report",$data);
    }
    public function export_to_excel_all_receipts_report()
    {
        $data['all_receipts_report'] = $this->session->userdata('all_receipts_report');

    $all_receipts_report = $this->account_model->export_to_excel_all_receipts_report($data['all_receipts_report']);

        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);

        $col = 'A';

        for($i=0;$i<count($all_receipts_report['fields']);$i++){
            $objPHPExcel->getActiveSheet()->SetCellValue($col.'1', $all_receipts_report['fields'][$i]);
            $objPHPExcel->getActiveSheet()->getStyle("A1:AA1")->getFont()->setBold(true);
            $col++;
        }


        $j = 2;
        $alphebetCol = 'A';
        foreach ($all_receipts_report['details'] as $key => $value) {

            foreach ($all_receipts_report['details'][$key] as $index => $data) {
                $objPHPExcel->getActiveSheet()->SetCellValue($alphebetCol.$j,$all_receipts_report['details'][$key][$index]);

                $alphebetCol++;
                if($alphebetCol == "AA"){
                    $alphebetCol = 'A';
                }
            }

            $j++;
        }

//die;
        $filename = "All_receipts_Details-" . date("h.i.s.d-m-Y") . ".xls";
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=" . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    public function all_receipt_search($offset = 0){
        $data['centres'] = $this->tele_enquiry_model->suggested_centre();

        if(isset($_POST['all_receipt_search'])){
            $this->session->set_userdata('all_receipts_report',$_POST);
        }
        if(isset($_POST['all_receipt_reset'])){
            $this->session->unset_userdata('all_receipts_report');
            redirect('accounts/all-receipt-search');
        }

        $data['all_receipts_report'] = $this->session->userdata('all_receipts_report');

        $data['all_receipts_search'] = array();
        if($this->session->userdata('all_receipts_report')){
            if(!(array_key_exists("option",$data['all_receipts_report']))){
                $data['all_receipts_report']['option'] = "off";
            }
// $this->account_model->get_receipts_report($offset,$postData,$config['per_page'],0)
            $data['all_receipts_search'] = $this->account_model->get_receipts_report($data['all_receipts_report'],0);

            $data['all_receipts_search']['receipt_data'] = $data['all_receipts_search'];

            $b = 0;
            for($i=0;$i<count($data['all_receipts_search']['receipt_data']);$i++){
                $a = $data['all_receipts_search']['receipt_data'][$i]['AMOUNT_PAID'] - $data['all_receipts_search']['receipt_data'][$i]['AMOUNT_DEPOSITED'];
                $b = $a + $b;
                $data['all_receipts_search']['receipt_data'][$i]['balance'] = $a;
                $data['all_receipts_search']['receipt_data'][$i]['balance_to_be_deposit'] = $b;
            }


            // for calculating total amount of total-cash,check,cc amount of 10 records in last row
            $data['total_paid_cash_amount']['amt_gst'] = 0;
            $data['total_paid_cash_amount']['amt_w/o_gst'] = 0;
            $data['total_paid_cash_amount']['amt_deposited'] = 0;
            $data['total_paid_cash_amount']['balance_amt_deposited'] = 0;
            $data['total_paid_cc_amount']['amt_gst'] = 0;
            $data['total_paid_cc_amount']['amt_w/o_gst'] = 0;
            $data['total_paid_cc_amount']['amt_deposited'] = 0;
            $data['total_paid_cc_amount']['balance_amt_deposited'] = 0;
            $data['total_paid_check_amount']['amt_gst'] = 0;
            $data['total_paid_check_amount']['amt_w/o_gst'] = 0;
            $data['total_paid_check_amount']['amt_deposited'] = 0;
            $data['total_paid_check_amount']['balance_amt_deposited'] = 0;

            for($i=0;$i<count($data['all_receipts_search']['receipt_data']);$i++){
                if(($data['all_receipts_search']['receipt_data'][$i]['PAYMENT_TYPE'] == 'CASH') || ($data['all_receipts_search']['receipt_data'][$i]['PAYMENT_TYPE'] == '1') || ($data['all_receipts_search']['receipt_data'][$i]['PAYMENT_TYPE'] == '') || empty($data['all_receipts_search']['receipt_data'][$i]['PAYMENT_TYPE'])){
                    $data['total_paid_cash_amount']['amt_gst'] += $data['all_receipts_search']['receipt_data'][$i]['AMOUNT_PAID'];
                    $data['total_paid_cash_amount']['amt_w/o_gst'] += $data['all_receipts_search']['receipt_data'][$i]['AMOUNT_PAID_FEES'];
                    $data['total_paid_cash_amount']['amt_deposited'] += $data['all_receipts_search']['receipt_data'][$i]['AMOUNT_DEPOSITED'];
                    $data['total_paid_cash_amount']['balance_amt_deposited'] += $data['all_receipts_search']['receipt_data'][$i]['balance'];
                }
                else if($data['all_receipts_search']['receipt_data'][$i]['PAYMENT_TYPE'] == 'CC' || ($data['all_receipts_search']['receipt_data'][$i]['PAYMENT_TYPE'] == '2')){
                    $data['total_paid_cc_amount']['amt_gst'] += $data['all_receipts_search']['receipt_data'][$i]['AMOUNT_PAID'];
                    $data['total_paid_cc_amount']['amt_w/o_gst'] += $data['all_receipts_search']['receipt_data'][$i]['AMOUNT_PAID_FEES'];
                    $data['total_paid_cc_amount']['amt_deposited'] += $data['all_receipts_search']['receipt_data'][$i]['AMOUNT_DEPOSITED'];
                    $data['total_paid_cc_amount']['balance_amt_deposited'] += $data['all_receipts_search']['receipt_data'][$i]['balance'];
                }
                else if($data['all_receipts_search']['receipt_data'][$i]['PAYMENT_TYPE'] == 'CHEQUE' || ($data['all_receipts_search']['receipt_data'][$i]['PAYMENT_TYPE'] == '3')){
                    $data['total_paid_check_amount']['amt_gst'] += $data['all_receipts_search']['receipt_data'][$i]['AMOUNT_PAID'];
                    $data['total_paid_check_amount']['amt_w/o_gst'] += $data['all_receipts_search']['receipt_data'][$i]['AMOUNT_PAID_FEES'];
                    $data['total_paid_check_amount']['amt_deposited'] += $data['all_receipts_search']['receipt_data'][$i]['AMOUNT_DEPOSITED'];
                    $data['total_paid_check_amount']['balance_amt_deposited'] += $data['all_receipts_search']['receipt_data'][$i]['balance'];
                }
            }

            // and then pass value individually in array to show total addition
            $data['grand_total']['amt_gst'] = $data['total_paid_cash_amount']['amt_gst'] + $data['total_paid_cc_amount']['amt_gst'] + $data['total_paid_check_amount']['amt_gst'];
            $data['grand_total']['amt_w/o_gst'] = $data['total_paid_cash_amount']['amt_w/o_gst'] + $data['total_paid_cc_amount']['amt_w/o_gst'] + $data['total_paid_check_amount']['amt_w/o_gst'];
            $data['grand_total']['amt_deposited'] = $data['total_paid_cash_amount']['amt_deposited'] + $data['total_paid_cc_amount']['amt_deposited'] + $data['total_paid_check_amount']['amt_deposited'];
            $data['grand_total']['balance_amt_deposited'] = $data['total_paid_cash_amount']['balance_amt_deposited'] + $data['total_paid_cc_amount']['balance_amt_deposited'] + $data['total_paid_check_amount']['balance_amt_deposited'];

        }

        $this->load->admin_view("all_receipt_report",$data);
    }

    public function receipt_edcr($offset = 0){
        $data['centres'] = $this->tele_enquiry_model->suggested_centre();
        $data['payfor'] = $this->admission_model->payFor();

        if(isset($_POST['receipt_edcr_search'])){
            $this->session->set_userdata('receipt_edcr_report',$_POST);
        }
        if(isset($_POST['receipt_edcr_reset'])){
            $this->session->unset_userdata('receipt_edcr_report');
            redirect('accounts/receipts-edcr');
        }

        $data['receipt_edcr_report'] = $this->session->userdata('receipt_edcr_report');
        $data['courses'] = $this->tele_enquiry_model->course_interested();
        $data['modules'] = $this->tele_enquiry_model->module_interested();
        $data['all_receipts_edcr'] = array();

        $billing_amount = 0;
        $token_number = 0;
        $new_admission = 0;
        $outstanding_new_admission = 0;
        $outstanding_old_admission = 0;
        $cash = 0;
        $cheque = 0;
        $fees_without_gst = 0;
        $fees_gst = 0;

        if($this->session->userdata('receipt_edcr_report')){
            $data['all_receipts_edcr'] = $this->account_model->get_receipt_edcr_report($data['receipt_edcr_report'],0);

            // for showing modulename,coursename in course taken section of receipt_edcr
            foreach($data['all_receipts_edcr'] as $key=>$value){
              $courseData = str_replace( ',', '',$data['all_receipts_edcr'][$key]['COURSE_TAKEN']);
              $moduleData = str_replace( ',', '',$data['all_receipts_edcr'][$key]['MODULE_TAKEN']);
              $module_name = $data['all_receipts_edcr'][$key]['MODULE_TAKEN'];
              $course_name = $data['all_receipts_edcr'][$key]['COURSE_TAKEN'];
              if(is_numeric($moduleData)){
                $module_id_Array = explode(',', $data['all_receipts_edcr'][$key]['MODULE_TAKEN']);
                $module_name = $this->account_model->get_course_and_module_names_by_id_array($module_id_Array,'module');
              }
              if (is_numeric($courseData)){
                $course_id_Array = explode(',', $data['all_receipts_edcr'][$key]['COURSE_TAKEN']);
                $course_name = $this->account_model->get_course_and_module_names_by_id_array($course_id_Array,'course');
                //echo $data['all_receipts_edcr'][$key]['COURSE_TAKEN']." : yes<br>";
              }
              $final_course_name = $module_name.",".$course_name;
              $data['all_receipts_edcr'][$key]['COURSE_TAKEN'] = trim($final_course_name,",");
            }
            // end of course_name logic

            $data['all_receipts_edcr']['receipt_data'] = $data['all_receipts_edcr'];

            $admission_id_array = array();
            for($j=0;$j<count($data['all_receipts_edcr']['receipt_data']);$j++){
              $admission_count = 0;
              for($k=0;$k<count($admission_id_array);$k++){
                if(in_array($admission_id_array[$k]['admission_id'],$data['all_receipts_edcr']['receipt_data'][$j])){
                  $admission_count = $admission_id_array[$k]['receipt_count'] + 1;
                }
              }
              $receipts['receipt_count'] = $admission_count;
              $receipts['receipt_no'] = $data['all_receipts_edcr']['receipt_data'][$j]['ADMISSION_RECEIPT_NO'];
              $receipts['admission_id'] = $data['all_receipts_edcr']['receipt_data'][$j]['ADMISSION_ID'];
              $data['all_receipts_edcr']['receipt_data'][$j]['receipt_count'] = $admission_count;
              array_push($admission_id_array,$receipts);
            }

            for($i=0;$i<count($data['all_receipts_edcr']['receipt_data']);$i++){
                $data['all_receipts_edcr']['receipt_data'][$i]['ENQUIRIES'] =  $this->account_model->get_enquiries_count_for_receipt_edcr($data['all_receipts_edcr']['receipt_data'][$i]['PAYMENT_DATE'],$data['all_receipts_edcr']['receipt_data'][$i]['CENTRE_ID']);
                $data['all_receipts_edcr']['receipt_data'][$i]['ADMISSION'] =  $this->account_model->get_admission_count_for_receipt_edcr($data['all_receipts_edcr']['receipt_data'][$i]['PAYMENT_DATE'],$data['all_receipts_edcr']['receipt_data'][$i]['CENTRE_ID']);

                //added by ankur on 28 jan 2018 for adding option of new admission_receipts and biling amount section addition in edcr report
                $time=strtotime($data['all_receipts_edcr']['receipt_data'][$i]['PAYMENT_DATE']);
                $payment_date_month_n_year=date("m-Y",$time);
                $time2=strtotime($data['all_receipts_edcr']['receipt_data'][$i]['ADMISSION_DATE']);
                $admission_date_month_n_year=date("m-Y",$time2);
              //  echo "payment date : ".$payment_date_month." and admission_date : ".$admission_date_month."<br>";
                // till here

                $data['all_receipts_edcr']['receipt_data'][$i]['billing_amount'] = '';
                $data['all_receipts_edcr']['receipt_data'][$i]['new_admission'] = '';
                $data['all_receipts_edcr']['receipt_data'][$i]['new_admission_outstanding'] = '';
                $data['all_receipts_edcr']['receipt_data'][$i]['old_admission_outstanding'] = '';
                $data['all_receipts_edcr']['receipt_data'][$i]['cash_receipt_no'] = '';
                $data['all_receipts_edcr']['receipt_data'][$i]['cheque_receipt_no'] = '';
                $data['all_receipts_edcr']['receipt_data'][$i]['cash'] = '';
                $data['all_receipts_edcr']['receipt_data'][$i]['cheque'] = '';

                if($payment_date_month_n_year == $admission_date_month_n_year){
                  if($data['all_receipts_edcr']['receipt_data'][$i]['receipt_count'] != 0){
                    $data['all_receipts_edcr']['receipt_data'][$i]['new_admission_outstanding'] = $data['all_receipts_edcr']['receipt_data'][$i]['AMOUNT_PAID'];
                  }
                  else{
                    $data['all_receipts_edcr']['receipt_data'][$i]['billing_amount'] = $data['all_receipts_edcr']['receipt_data'][$i]['TOTALFEES'];
                    $data['all_receipts_edcr']['receipt_data'][$i]['new_admission'] = $data['all_receipts_edcr']['receipt_data'][$i]['AMOUNT_PAID'];
                  }
                }
                else if($payment_date_month_n_year != $admission_date_month_n_year){
                  $data['all_receipts_edcr']['receipt_data'][$i]['old_admission_outstanding'] = $data['all_receipts_edcr']['receipt_data'][$i]['AMOUNT_PAID'];
                }

                if(($data['all_receipts_edcr']['receipt_data'][$i]['PAYMENT_TYPE'] == "CASH") || ($data['all_receipts_edcr']['receipt_data'][$i]['PAYMENT_TYPE'] == "1")){
                    $data['all_receipts_edcr']['receipt_data'][$i]['cash_receipt_no'] = $data['all_receipts_edcr']['receipt_data'][$i]['ADMISSION_RECEIPT_NO'];
                    $data['all_receipts_edcr']['receipt_data'][$i]['cash'] = $data['all_receipts_edcr']['receipt_data'][$i]['AMOUNT_PAID'];
                }
                else{
                    $data['all_receipts_edcr']['receipt_data'][$i]['cheque_receipt_no'] = $data['all_receipts_edcr']['receipt_data'][$i]['ADMISSION_RECEIPT_NO'];
                    $data['all_receipts_edcr']['receipt_data'][$i]['cheque'] = $data['all_receipts_edcr']['receipt_data'][$i]['AMOUNT_PAID'];
                }
            $billing_amount += $data['all_receipts_edcr']['receipt_data'][$i]['billing_amount'];
            $token_number += 0;
            $new_admission += $data['all_receipts_edcr']['receipt_data'][$i]['new_admission'];
            $outstanding_new_admission += $data['all_receipts_edcr']['receipt_data'][$i]['new_admission_outstanding'];
            $outstanding_old_admission += $data['all_receipts_edcr']['receipt_data'][$i]['old_admission_outstanding'];
            $cash += $data['all_receipts_edcr']['receipt_data'][$i]['cash'];
            $cheque += $data['all_receipts_edcr']['receipt_data'][$i]['cheque'];
            $fees_without_gst += $data['all_receipts_edcr']['receipt_data'][$i]['AMOUNT_PAID_FEES'];
            $fees_gst += $data['all_receipts_edcr']['receipt_data'][$i]['AMOUNT_PAID_SERVICETAX'];
            }
        }

        $data['billing_amount'] = $billing_amount;
        $data['token_number'] = $token_number;
        $data['new_admission'] = $new_admission;
        $data['outstanding_new_admission'] = $outstanding_new_admission;
        $data['outstanding_old_admission'] = $outstanding_old_admission;
        $data['cash'] = $cash;
        $data['cheque'] = $cheque;
        $data['fees_without_gst'] = $fees_without_gst;
        $data['fees_gst'] = $fees_gst;

        $this->load->admin_view("receipt_edcr",$data);
    }

}
