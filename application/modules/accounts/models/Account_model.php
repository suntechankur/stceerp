<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_model extends CI_Model {

  public $sql;
  public function __construct() {
      parent::__construct();
  }

	public function employee_pay_details($data){

		$employeeid = $data['records']['EMPLOYEE_ID'];
		$fromdate = $data['records']['FROM_DATE'];
		$todate = $data['records']['TO_DATE'];

		$where = "EMPLOYEE_ID = '$employeeid' AND FROM_DATE = '$fromdate' AND TO_DATE = '$todate'";
		$select = $this->db->select('*')
	    				   ->from('employees_pay_details')
	    				   ->where($where)
	  					   ->get();
		$rowcount = $select->num_rows();

		if($rowcount == 0){
	    	$query = $this->db->insert('employees_pay_details',$data['records']);
	        if ($this->db->affected_rows() > 0) {
		        return $this->db->affected_rows();
		    }
		    else {
		    	return $this->db->error();
		    }
	  	$this->_randomsleep();
		}
	}

	public function _randomsleep()
	{
	 $sleep = rand(11, 61);
	 sleep($sleep);
	 $this->db->reconnect();
	}

	public function get_employee_pay_details_header(){
    $query = $this->db->select('*')
    				  ->from('employees_pay_details')
  					  ->get();
  	return $query->list_fields();
	}

	public function get_employee_pay_details_count(){
    $query = $this->db->select('*')
    				  ->from('employees_pay_details')
  					  ->get();
  	return $query->num_rows();
	}

	public function get_employee_pay_details($count,$limit){
    $query = $this->db->select('*')
    				  ->from('employees_pay_details')
    				  ->limit($limit,$count)
  					  ->get();
  	return $query->result_array();
	}

	public function get_intake_months(){
    $query = $this->db->select('controlfile_value')
    				  ->from('control_file')
    				  ->where('controlfile_key = "IN_TAKE"')
  					  ->get();
  	return $query->result_array();
	}

	public function get_academic_year(){
    $query = $this->db->select('controlfile_value')
    				  ->from('control_file')
    				  ->where('controlfile_key = "YEAR"')
  					  ->get();
  	return $query->result_array();
	}

public function export_to_excel_deleted_receipts($postData){

	    $query = $this->db->select('AR.ADMISSION_RECEIPTS_ID,
AR.ADMISSION_RECEIPT_NO,
AR.CENTRE_RECEIPT_ID,
AR.RECIEPT_NO AS RECEIPT_NO,
CM.CENTRE_ID,
CM.CENTRE_NAME as CENTRE_NAME,
CONCAT(EnM.ENQUIRY_FIRSTNAME, " ", EnM.ENQUIRY_LASTNAME) As STUDENT_NAME,
AM.ADMISSION_ID,
AM.ADMISSION_DATE as ADMISSION_DATE,
AR.PAYMENT_DATE AS PAYMENT_DATE ,
AR.PAYMENT_TYPE,
AR.AMOUNT_PAID,
AR.AMOUNT_PAID_FEES,
if(AR.CHEQUE_BANK_NAME IS NULL ," ",AR.CHEQUE_BANK_NAME) as BANK,
if(AR.CHEQUE_NO IS NULL ," ",AR.CHEQUE_NO) as CHEQUE_NO,
AR.CHEQUE_DATE AS CHEQUE_DATE,
if(AR.CHEQUE_BRANCH IS NULL ," ",AR.CHEQUE_BRANCH) as BRANCH,
CONCAT(IFNULL(EM.EMP_FNAME, ""), " ",IFNULL(EM.EMP_LASTNAME, "")) AS RECEIVED_BY,
if(AR.AMOUNT_DEPOSITED IS NULL,0,AR.AMOUNT_DEPOSITED)as AMOUNT_DEPOSITED,
if(AR.AMOUNT_DEPOSITED2 IS NULL,0,AR.AMOUNT_DEPOSITED2)as AMOUNT_DEPOSITED2,
AR.DATE_OF_DEPOSIT,
AR.DATE_OF_DEPOSIT2,
AR.DEPOSIT_SLIP_NO AS SLIP_NO,
AR.DEPOSIT_SLIP_NO2 AS SLIP_NO2,
if(AR.STATUS IS NULL,0,AR.STATUS) AS STATUS,
AR.REMARKS,
AR.AMOUNT_PAID-if(AR.AMOUNT_DEPOSITED IS NULL,0,AR.AMOUNT_DEPOSITED)- if(AR.AMOUNT_DEPOSITED2 IS NULL,0,AR.AMOUNT_DEPOSITED2) AS BALANCE,
if(AR.INTERNAL IS NULL,0,AR.INTERNAL) AS INTERNAL,
AR.COLLECTION_MONTH
')
	    				  ->from('admission_master AM')
	    				  ->join('admission_receipts AR', 'AM.ADMISSION_ID = AR.ADMISSION_ID')
	    				  ->join('enquiry_master EnM', 'AM.ENQUIRY_ID = EnM.ENQUIRY_ID')
	    				  ->join('employee_master EM', 'EM.EMPLOYEE_ID= AR.RECEIVED_BY ')
	    				  ->join('centre_master CM', 'CM.CENTRE_ID=AR.CENTRE_ID');

	    				  if($postData['receipt_from_date'] != '' && $postData['receipt_to_date'] != ''){


	                $date_format_change = str_replace("/","-",$postData['receipt_from_date']);
	                $from_date = date('Y-m-d', strtotime($date_format_change));

	                $to_date_format_change = str_replace("/","-",$postData['receipt_to_date']);
	                $to_date = date('Y-m-d', strtotime($to_date_format_change));

					$query = $this->db->where('AR.PAYMENT_DATE > ',$from_date)
	                                  ->where('AR.PAYMENT_DATE < ',$to_date);
	    				  		}
	    				  if($postData['centre'] != ''){
					$query = $this->db->where('AM.CENTRE_ID',$postData['centre']);
	    				  }

	    				  if (array_key_exists("chequeBounce",$postData)){
		    				  if($postData['chequeBounce']!='')
		    				  {
		    				  	$query = $this->db->like('AR.REMARKS',$postData['chequeBounce'],'both');
		    				  }
	    				  }

	    				  $query = $this->db->order_by("AR.PAYMENT_TYPE, AR.CENTRE_RECEIPT_ID");

		  				  $query = $this->db->where('AR.ISACTIVE',"0")
	  					  					->get();
	  					  					//echo $this->db->last_query();

	  	$data['details'] = $query->result_array();

	  	$data['fields'] = $query->list_fields();

        return $data;
	}


	public function get_student_receipts_details($offset,$postData,$perpage,$count){
	    $query = $this->db->select('AR.ADMISSION_RECEIPTS_ID,
AR.ADMISSION_RECEIPT_NO,
AR.CENTRE_RECEIPT_ID,
AR.RECIEPT_NO AS RECEIPT_NO,
CM.CENTRE_ID,
CM.CENTRE_NAME as CENTRE_NAME,
CONCAT(EnM.ENQUIRY_FIRSTNAME, " ", EnM.ENQUIRY_LASTNAME) As STUDENT_NAME,
AM.ADMISSION_ID,
AM.ADMISSION_DATE as ADMISSION_DATE,
AR.PAYMENT_DATE AS PAYMENT_DATE ,
AR.PAYMENT_TYPE,
AR.AMOUNT_PAID,
AR.AMOUNT_PAID_FEES,
if(AR.CHEQUE_BANK_NAME IS NULL ," ",AR.CHEQUE_BANK_NAME) as BANK,
if(AR.CHEQUE_NO IS NULL ," ",AR.CHEQUE_NO) as CHEQUE_NO,
AR.CHEQUE_DATE AS CHEQUE_DATE,
if(AR.CHEQUE_BRANCH IS NULL ," ",AR.CHEQUE_BRANCH) as BRANCH,
CONCAT(IFNULL(EM.EMP_FNAME, ""), " ",IFNULL(EM.EMP_LASTNAME, "")) AS RECEIVED_BY,
if(AR.AMOUNT_DEPOSITED IS NULL,0,AR.AMOUNT_DEPOSITED)as AMOUNT_DEPOSITED,
if(AR.AMOUNT_DEPOSITED2 IS NULL,0,AR.AMOUNT_DEPOSITED2)as AMOUNT_DEPOSITED2,
AR.DATE_OF_DEPOSIT,
AR.DATE_OF_DEPOSIT2,
AR.DEPOSIT_SLIP_NO AS SLIP_NO,
AR.DEPOSIT_SLIP_NO2 AS SLIP_NO2,
if(AR.STATUS IS NULL,0,AR.STATUS) AS STATUS,
AR.REMARKS,
AR.AMOUNT_PAID-if(AR.AMOUNT_DEPOSITED IS NULL,0,AR.AMOUNT_DEPOSITED)- if(AR.AMOUNT_DEPOSITED2 IS NULL,0,AR.AMOUNT_DEPOSITED2) AS BALANCE,
if(AR.INTERNAL IS NULL,0,AR.INTERNAL) AS INTERNAL,
AR.COLLECTION_MONTH
')
	    				  ->from('admission_master AM')
	    				  ->join('admission_receipts AR', 'AM.ADMISSION_ID = AR.ADMISSION_ID')
	    				  ->join('enquiry_master EnM', 'AM.ENQUIRY_ID = EnM.ENQUIRY_ID')
	    				  ->join('employee_master EM', 'EM.EMPLOYEE_ID= AR.RECEIVED_BY ')
	    				  ->join('centre_master CM', 'CM.CENTRE_ID=AR.CENTRE_ID');

	    				  if($postData['first_name'] != '' && $postData['last_name'] != ''){
					$query = $this->db->like('EnM.ENQUIRY_FIRSTNAME',$postData['first_name'],'after')
									->like('EnM.ENQUIRY_LASTNAME',$postData['last_name'],'after');
	    				  }
	    				  else{
		    				  if($postData['first_name'] != ''){
						$query = $this->db->like('EnM.ENQUIRY_FIRSTNAME',$postData['first_name'],'after');
		    				  }
		    				  else if($postData['last_name'] != ''){
						$query = $this->db->like('EnM.ENQUIRY_LASTNAME',$postData['last_name'],'after');
		    				  }
	    				  }

	    				  if($postData['payment_type'] != ''){

						$payment_type = "";
    				  	switch ($postData['payment_type']) {
    				  		case 'cash':
    				  			$payment_type = 1;
    				  		break;
    				  		case 'cc':
    				  			$payment_type = 2;
    				  		break;
    				  		case 'cheque':
    				  			$payment_type = 3;
    				  		break;
    				  	}

    				  	$pay_type = $postData['payment_type'];

    				  	$where = "(AR.PAYMENT_TYPE = '$payment_type' or AR.PAYMENT_TYPE = '$pay_type')";

						$this->db->where($where);

	    				  }

	    				  if($postData['receipt_from_date'] != '' && $postData['receipt_to_date'] != ''){
	                $date_format_change = str_replace("/","-",$postData['receipt_from_date']);
	                $from_date = date('Y-m-d', strtotime($date_format_change));

	                $to_date_format_change = str_replace("/","-",$postData['receipt_to_date']);
	                $to_date = date('Y-m-d', strtotime($to_date_format_change));

	                $where = "AR.PAYMENT_DATE BETWEEN '$from_date' and '$to_date'";

					$query = $this->db->where($where);
	    				  		}
	    				  if($postData['centre'] != ''){
					$query = $this->db->where('AM.CENTRE_ID',$postData['centre']);
	    				  }

	    				  if($postData['admission_from_date'] != '' && $postData['admission_to_date'] != ''){
	                $ad_date_format_change = str_replace("/","-",$postData['admission_from_date']);
	                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

	                $ad_to_date_format_change = str_replace("/","-",$postData['admission_to_date']);
	                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));

	                $where = "AM.ADMISSION_DATE BETWEEN '$from_date' and '$to_date'";

					$query = $this->db->where($where);
	    				  		}

	    				  if($postData['received_by'] != ''){
					$query = $this->db->like('EM.EMP_FNAME',$postData['received_by'],'after');
	    				  		}

	    				  if($postData['receipt_no_from'] != '' && $postData['receipt_no_to'] != ''){

	    				  	$from = $postData['receipt_no_from'];
	    				  	$to = $postData['receipt_no_to'];

 					$where = "AR.CENTRE_RECEIPT_ID BETWEEN '$from' and '$to'";

					$query = $this->db->where($where);
	    				  		}

	    				  if($postData['manual_receipt_from'] != '' && $postData['manual_receipt_to'] != ''){

	    				  	$from = $postData['manual_receipt_from'];
	    				  	$to = $postData['manual_receipt_to'];

 					$where = "AR.RECIEPT_NO BETWEEN '$from' and '$to'";

					$query = $this->db->where($where);
	    				  		}

	    				  if($postData['deposit_date_from'] != '' && $postData['deposit_date_to'] != ''){
					$date_format_change = str_replace("/","-",$postData['deposit_date_from']);
	                $from_date = date('Y-m-d', strtotime($date_format_change));

	                $to_date_format_change = str_replace("/","-",$postData['deposit_date_to']);
	                $to_date = date('Y-m-d', strtotime($to_date_format_change));

	                $where = "AR.DATE_OF_DEPOSIT BETWEEN '$from_date' and '$to_date'";

					$query = $this->db->where($where);
	    				  		}

	    				  if($postData['cheque_no'] != ''){
					$query = $this->db->where('AR.CHEQUE_NO',$postData['cheque_no']);
	    				  		}

	    				  if($postData['deposition_status'] != ''){
	    				  	if($postData['deposition_status'] == 'deposited'){
					$query = $this->db->where('AR.DEPOSIT_SLIP_NO IS NOT NULL');
	    				  	}
	    				  	else if($postData['deposition_status'] == 'undeposited'){
					$query = $this->db->where('AR.DEPOSIT_SLIP_NO IS NULL');
	    				  	}
	    				  	}
	    				  $query = $this->db->order_by("AR.PAYMENT_TYPE, AR.CENTRE_RECEIPT_ID");
	    				  // if($count != 1){
	    				  // 	$query = $this->db->limit($perpage,$offset);
	    				  // }
		  				  $query = $this->db->where('AR.ISACTIVE',"1")
	  					  					->get();
	  					  					//echo $this->db->last_query();
	  	if ($count == 1) {
	  		return $query->num_rows();
	  	}
	  	else{
	  		return $query->result_array();
	  	}
	}

	public function get_receipt_details_for_edit($receiptId,$type){
	    $query = $this->db->select('AR.ADMISSION_RECEIPTS_ID,
 AR.ADMISSION_ID,
 AR.ADMISSION_RECEIPT_NO,
 CONCAT(EnM.ENQUIRY_FIRSTNAME, " ", EnM.ENQUIRY_LASTNAME) As STUDENT_NAME,
 CONCAT(EnM.ENQUIRY_ADDRESS1, " ", EnM.ENQUIRY_CITY," ",EnM.ENQUIRY_STATE," ",EnM.ENQUIRY_ZIP," ",EnM.ENQUIRY_COUNTRY) As ADDRESS,
 AM.ADMISSION_DATE,
 AM.COURSE_TAKEN,
 AM.TOTALFEES,
 AR.PAYMENT_DATE,
 AR.PAYMENT_TYPE,
 AR.AMOUNT_PAID,
 if(AR.CHEQUE_BANK_NAME IS NULL ," ",AR.CHEQUE_BANK_NAME) as BANK,
 if(AR.CHEQUE_NO IS NULL ," ",AR.CHEQUE_NO) as CHEQUE_NO,
 if(AR.CHEQUE_DATE IS NULL ," ",AR.CHEQUE_DATE) as CHEQUE_DATE,
 if(AR.CHEQUE_BRANCH IS NULL ," ",AR.CHEQUE_BRANCH) as BRANCH,
 AR.RECEIVED_BY,
 AR.MODIFIED_BY,
 AR.MODIFIED_DATE,
 AR.AMOUNT_PAID_FEES,
 AR.AMOUNT_PAID_SERVICETAX,
 AR.RECIEPT_NO,
 AR.UPDATE_REMARKS,
 AR.CENTRE_ID,
 AR.SERVICE_TAX,
 CM.CENTRE_NAME,
 CONCAT(IFNULL(EmM.EMP_FNAME, ""), " ",IFNULL(EmM.EMP_LASTNAME, "")) As EMPLOYEE_NAME')
	    				  ->from('admission_receipts AR')
	    				  ->join('admission_master AM', 'AR.ADMISSION_ID = AM.ADMISSION_ID','left')
	    				  ->join('enquiry_master EnM', 'AM.ENQUIRY_ID = EnM.ENQUIRY_ID','left')
	    				  ->join('centre_master CM', 'AR.CENTRE_ID = CM.CENTRE_ID','left')
	    				  ->join('employee_master EmM', 'AR.RECEIVED_BY = EmM.EMPLOYEE_ID','left')
	    				  ->where('AR.ADMISSION_RECEIPTS_ID',$receiptId);
	    				  if($type == "delete"){
	    				  	$query = $this->db->where('AR.ISACTIVE',"0");
	    				  }
	    				  else{
	    				  	$query = $this->db->where('AR.ISACTIVE',"1");
	    				  }
	  					  $query = $this->db->get();
	  	return $query->result_array();
	}

	public function get_receipt_details_by_admission_id($admissionId){
	    $query = $this->db->select('AR.ADMISSION_RECEIPTS_ID,
 AR.RECIEPT_NO,
 AR.ADMISSION_ID,
 AR.ADMISSION_RECEIPT_NO,
 AR.AMOUNT_PAID,
 AR.AMOUNT_PAID_FEES,
 AR.CHEQUE_NO,
 AR.CHEQUE_DATE,
 AR.CHEQUE_BANK_NAME,
 AR.PAYMENT_TYPE,
 AR.PAYMENT_DATE')
	    				  ->from('admission_receipts AR')
	    				  ->join('admission_master AM', 'AR.ADMISSION_ID = AM.ADMISSION_ID','left')
	    				  ->where('AR.ADMISSION_ID',$admissionId)
	    				  ->order_by('AR.PAYMENT_DATE','ASC')
	  					  ->get();
	  	return $query->result_array();
	}

    public function handled_by(){
        $query = $this->db->select('EMPLOYEE_ID,CONCAT(EMP_FNAME," ",EMP_LASTNAME) as EMP_NAME,ISACTIVE')
                          ->from('employee_master')
                          ->where('CONCAT(EMP_FNAME," ",EMP_LASTNAME) IS NOT NULL')
                          ->order_by('ISACTIVE','DESC')
                          ->order_by('EMP_FNAME,EMP_LASTNAME','ASC')
                          ->get();
        return $query->result_array();
    }

    public function update_receipts_details($receipts_details,$type){
		$trimmedArray = array_map('trim', $receipts_details);   // for removing spaces from array values

		$ad_date_format_change = str_replace("/","-",$trimmedArray['payment_date']);
    $payment_date = date('Y-m-d H:i:s', strtotime($ad_date_format_change));

		$cheque_date_format_change = str_replace("/","-",$trimmedArray['cheque_date']);
    $cheque_date = date('Y-m-d H:i:s', strtotime($cheque_date_format_change));

		$data = array(
				'CENTRE_ID' => $trimmedArray['centre'],
				'PAYMENT_TYPE' => $trimmedArray['payment_type'],
				'PAYMENT_DATE' => $payment_date,
				'AMOUNT_PAID' => $trimmedArray['amount_with_gst'],
				'AMOUNT_PAID_FEES' => $trimmedArray['amount_without_gst'],
				'AMOUNT_PAID_SERVICETAX' => $trimmedArray['gst_paid'],
				'CHEQUE_DATE' => $cheque_date,
				'CHEQUE_NO' => $trimmedArray['cheque_no'],
				'CHEQUE_BANK_NAME' => $trimmedArray['bank_name'],
				'CHEQUE_BRANCH' => $trimmedArray['bank_branch'],
				'RECEIVED_BY' => $trimmedArray['received_by'],
				'UPDATE_REMARKS' => $trimmedArray['remarks'],
				'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['USER_ID'],
				'MODIFIED_DATE' => date('Y-m-d H:i:s'));

    	$data = array_filter($data, function($value) { return !empty($value) || $value === 0; });   // for avoiding selection of blank values from actual array

    	if($type == "delete"){
    		$this->db->set('ISACTIVE',"0");		// for cancel   mysql
    	}
		$this->db->set($data)					// for update   mysql
                 ->where('ADMISSION_RECEIPTS_ID',$trimmedArray['receipt_id'])
                 ->where('ADMISSION_ID',$trimmedArray['admission_id'])
                 ->update('admission_receipts');


      if($this->db->affected_rows() > 0){
        return 1;
      }
      else{
        return 0;
      }
	}

	public function feesPaidTillDate($admissionId,$receipt_id,$payment_date){
        $query = $this->db->select('SUM(AMOUNT_PAID_FEES) as TOTAL_PAID')
                          ->from('admission_receipts')
                          ->where('ADMISSION_ID',$admissionId)
                          ->where('PAYMENT_DATE <=',$payment_date)
                          ->where('ISACTIVE','1')
                          ->get();
        return $query->result_array();
	}

	public function feesAmountTillDate($admissionId,$receipt_id,$payment_date){
	    $query = $this->db->select('AMOUNT_PAID_FEES')
                          ->from('admission_receipts')
                          ->where('ADMISSION_ID',$admissionId)
                          ->where('PAYMENT_DATE <=',$payment_date)
                          ->where('ISACTIVE','1')
                          ->get();
        return $query->result_array();
    }

    public function cash_check_deposition_data($offset,$centre_id,$perpage,$count){
    	$current_date = date("Y-m-d H:i:s");
    	$where = "AR.PAYMENT_DATE BETWEEN '2015-01-01 00.00.00' and '$current_date'";
      $query = $this->db->select('AR.ADMISSION_ID,
 CONCAT(EM.ENQUIRY_FIRSTNAME, " ", EM.ENQUIRY_LASTNAME) As STUDENT_NAME,
 CM.CENTRE_NAME,
 AR.CENTRE_ID,
 if(AR.ADMISSION_RECEIPTS_ID IS NULL ," ",AR.ADMISSION_RECEIPTS_ID) as ADMISSION_RECEIPTS_ID,
 AR.CENTRE_RECEIPT_ID,
 AR.ADMISSION_RECEIPT_NO,
 AR.RECIEPT_NO AS RECEIPT_NO,
 AM.ADMISSION_DATE AS ADMISSION_DATE,
 AR.PAYMENT_DATE AS PAYMENT_DATE ,
 AR.PAYMENT_TYPE,
 AR.AMOUNT_PAID,
 AR.AMOUNT_DEPOSITED,
 (AR.AMOUNT_PAID - if(AR.AMOUNT_DEPOSITED IS NULL,0,AR.AMOUNT_DEPOSITED)) as BALANCE,
 if(AR.CHEQUE_BANK_NAME IS NULL ," ",AR.CHEQUE_BANK_NAME) as BANK,
 if(AR.CHEQUE_NO IS NULL ," ",AR.CHEQUE_NO) as CHEQUE_NO,
 if(AR.CHEQUE_DATE IS NULL ," ",AR.CHEQUE_DATE) as CHEQUE_DATE,
 if(AR.CHEQUE_BRANCH IS NULL ," ",AR.CHEQUE_BRANCH) as BRANCH,
 AR.RECEIVED_BY,
 CONCAT(IFNULL(EmM.EMP_FNAME, ""), " ",IFNULL(EmM.EMP_LASTNAME, "")) As EMPLOYEE_NAME,
 AR.AMOUNT_DEPOSITED2,
 AR.DATE_OF_DEPOSIT AS DATE_OF_DEPOSIT,
 AR.DEPOSIT_SLIP_NO AS DEPOSIT_SLIP_NO,
 AR.REMARKS AS REMARKS,
 AR.ISACTIVE')
                        ->from('admission_receipts AR')
                        ->join('admission_master AM','AR.ADMISSION_ID = AM.ADMISSION_ID','left')
                        ->join('enquiry_master EM','AM.ENQUIRY_ID = EM.ENQUIRY_ID','left')
                        ->join('centre_master CM','CM.CENTRE_ID = AM.CENTRE_ID','left')
	    				          ->join('employee_master EmM', 'AR.RECEIVED_BY = EmM.EMPLOYEE_ID','left')
                        ->where('AR.AMOUNT_DEPOSITED IS NULL')
                        ->where('AR.CENTRE_ID',$centre_id)
                        ->where('(AR.ISACTIVE = "1" OR AR.ISACTIVE IS NULL)')
                        ->where($where);
                        if($count != 1){
	    				  	$query = $this->db->limit($perpage,$offset);
	    				}
	    				$query = $this->db->order_by('AR.PAYMENT_DATE','desc');
		  				$query = $this->db->get();
					  	if ($count == 1) {
					  		return $query->num_rows();
					  	}
					  	else{
					  		return $query->result_array();
					  	}
      return $query->result_array();
    }

    public function receipt_exist($receiptid,$type){
    	if($type == "other"){
	    	$query = $this->db->select('PENALTY_RECEIPT_ID')
	    					 ->from('penalties_receipt_master')
	    					 ->where('PENALTY_RECEIPT_ID',$receiptid)
	    					 ->get();
    	}
    	else{
	    	$query = $this->db->select('ADMISSION_RECEIPTS_ID')
	    					 ->from('admission_receipts')
	    					 ->where('ADMISSION_RECEIPTS_ID',$receiptid)
	    					 ->get();
    	}

    	return $query->num_rows();
    }

    public function update_cash_cheque_deposition_data($receiptid,$amountdept,$depositdate,$depositslipno,$remarks,$type){

		$deposition_date = str_replace('/', '-', $depositdate);
		$receipt_deposit_date = date("Y-m-d H:i:s", strtotime($deposition_date));

    	if($type == "other"){
			$data = array(
					'AMOUNT_DEPOSITED' => $amountdept,
					'DATE_OF_DEPOSIT' => $receipt_deposit_date,
					'DEPOSIT_SLIP_NO' => $depositslipno,
					'REMARK' => $remarks,
					'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['USER_ID'],
					'MODIFIED_DATE' => date('Y-m-d H:i:s'));

			$trimmedArray = array_map('trim', $data);  // remove side space from values

	    	$data = array_filter($trimmedArray, function($value) { return !empty($value) || $value === 0; });   // for avoiding selection of blank values from actual array

			$this->db->set($data)					// for update  mysql
	                 ->where('PENALTY_RECEIPT_ID',$receiptid)
	                 ->update('penalties_receipt_master');

    	}
    	else{
			$data = array(
					'AMOUNT_DEPOSITED' => $amountdept,
					'DATE_OF_DEPOSIT' => $receipt_deposit_date,
					'DEPOSIT_SLIP_NO' => $depositslipno,
					'REMARKS' => $remarks,
					'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['USER_ID'],
					'MODIFIED_DATE' => date('Y-m-d H:i:s'));

			$trimmedArray = array_map('trim', $data);  // remove side space from values

	    	$data = array_filter($trimmedArray, function($value) { return !empty($value) || $value === 0; });   // for avoiding selection of blank values from actual array

			$this->db->set($data)					// for update  mysql
	                 ->where('ADMISSION_RECEIPTS_ID',$receiptid)
	                 ->update('admission_receipts');

	     // added by ankur on 17/10/2017
			unset($data['REMARKS']);
			$data['UPDATE_REMARKS'] = $remarks;
	     // till here

    	}

        if($this->db->affected_rows() > 0){
        	return 1;
        }
        else{
        	return 0;
        }
    }

    public function update_transfer_receipts($from_admission_id,$to_admission_id,$remarks,$receipt_data){
    	for($i=0;$i<count($receipt_data);$i++){
			$data = array(
				'ADMISSION_ID' => $to_admission_id,
				'REMARKS' => $remarks,
				'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['USER_ID'],
				'MODIFIED_DATE' => date('Y-m-d H:i:s'));
	    	$this->db->set($data)					// for update  mysql
                 ->where('ADMISSION_ID',$from_admission_id)
                 ->where('ADMISSION_RECEIPTS_ID',$receipt_data[$i])
                 ->update('admission_receipts');


		    // added by ankur on 17/10/2017
				unset($data['REMARKS']);
				$data['UPDATE_REMARKS'] = $remarks;
		    // till here

    	}

        if($this->db->affected_rows() > 0){
        	return 1;
        }
        else{
        	return 0;
        }
    }

    public function get_student_other_receipts_details($offset,$postData,$perpage,$count){
	    $query = $this->db->select('PRM.PENALTY_RECEIPT_ID AS ADMISSION_RECEIPTS_ID,
PRM.PENALTY_RECEIPT_NO AS ADMISSION_RECEIPT_NO,
PRM.CENTRE_RECEIPT_ID AS CENTRE_RECEIPT_ID,
PRM.RECEIPT_NO AS RECEIPT_NO,
CM.CENTRE_NAME as CENTRE_NAME,
CONCAT(EnM.ENQUIRY_FIRSTNAME, " ", EnM.ENQUIRY_LASTNAME) As STUDENT_NAME,
AM.ADMISSION_ID,
AM.ADMISSION_DATE as ADMISSION_DATE,
PRM.PAYMENT_DATE AS PAYMENT_DATE ,
PRM.PAYMENT_TYPE,
PRM.REMARK,
PRM.AMOUNT As AMOUNT_PAID,
PRM.AMOUNT_WITHOUT_ST As AMOUNT_PAID_FEES,
if(PRM.BANK_NAME IS NULL," ",PRM.BANK_NAME) as BANK,
if(PRM.CHEQUE_NO IS NULL," ",PRM.CHEQUE_NO) as CHEQUE_NO,
PRM.CHEQUE_DATE AS CHEQUE_DATE,
if(PRM.BRANCH IS NULL ," ",PRM.BRANCH) as BRANCH,
CONCAT(IFNULL(EM.EMP_FNAME, ""), " ",IFNULL(EM.EMP_LASTNAME, "")) AS RECEIVED_BY,
if(PRM.AMOUNT_DEPOSITED IS NULL,0,PRM.AMOUNT_DEPOSITED)as AMOUNT_DEPOSITED,
if(PRM.AMOUNT_DEPOSITED2 IS NULL,0,PRM.AMOUNT_DEPOSITED2)as AMOUNT_DEPOSITED2,
PRM.DATE_OF_DEPOSIT,
PRM.DATE_OF_DEPOSIT2,
PRM.DEPOSIT_SLIP_NO AS SLIP_NO,
PRM.DEPOSIT_SLIP_NO2 AS SLIP_NO2,
if(PRM.STATUS IS NULL,0,PRM.STATUS) AS STATUS,
PRM.AMOUNT-if(PRM.AMOUNT_DEPOSITED IS NULL,0,PRM.AMOUNT_DEPOSITED)- if(PRM.AMOUNT_DEPOSITED2 IS NULL,0,PRM.AMOUNT_DEPOSITED2) AS BALANCE')
	    				  ->from('admission_master AM')
	    				  ->join('penalties_receipt_master PRM', 'AM.ADMISSION_ID = PRM.ADMISSION_ID')
	    				  ->join('enquiry_master EnM', 'AM.ENQUIRY_ID = EnM.ENQUIRY_ID')
	    				  ->join('employee_master EM', 'EM.EMPLOYEE_ID= PRM.RECEIVED_BY ')
	    				  ->join('centre_master CM', 'CM.CENTRE_ID=PRM.CENTRE_ID');

	    				  if($postData['first_name'] != '' && $postData['last_name'] != ''){
					$query = $this->db->like('EnM.ENQUIRY_FIRSTNAME',$postData['first_name'],'after')
									->like('EnM.ENQUIRY_LASTNAME',$postData['last_name'],'after');
	    				  }
	    				  else{
		    				  if($postData['first_name'] != ''){
						$query = $this->db->like('EnM.ENQUIRY_FIRSTNAME',$postData['first_name'],'after');
		    				  }
		    				  else if($postData['last_name'] != ''){
						$query = $this->db->like('EnM.ENQUIRY_LASTNAME',$postData['last_name'],'after');
		    				  }
	    				  }

	    				  if($postData['payment_type'] != ''){

						$payment_type = "";
    				  	switch ($postData['payment_type']) {
    				  		case 'cash':
    				  			$payment_type = 1;
    				  		break;
    				  		case 'cc':
    				  			$payment_type = 2;
    				  		break;
    				  		case 'cheque':
    				  			$payment_type = 3;
    				  		break;
    				  	}

    				  	$pay_type = $postData['payment_type'];

    				  	$where = "(PRM.PAYMENT_TYPE = '$payment_type' or PRM.PAYMENT_TYPE = '$pay_type')";

						$this->db->where($where);

	    				  }

	    				  if($postData['receipt_from_date'] != '' && $postData['receipt_to_date'] != ''){
	                $date_format_change = str_replace("/","-",$postData['receipt_from_date']);
	                $from_date = date('Y-m-d', strtotime($date_format_change));

	                $to_date_format_change = str_replace("/","-",$postData['receipt_to_date']);
	                $to_date = date('Y-m-d', strtotime($to_date_format_change));


	                $where = "PRM.PAYMENT_DATE between '$from_date' and '$to_date'";

					$query = $this->db->where($where);
	    				  		}
	    				  if($postData['centre'] != ''){
					$query = $this->db->where('AM.CENTRE_ID',$postData['centre']);
	    				  }

	    				  if($postData['admission_from_date'] != '' && $postData['admission_to_date'] != ''){
	                $ad_date_format_change = str_replace("/","-",$postData['admission_from_date']);
	                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

	                $ad_to_date_format_change = str_replace("/","-",$postData['admission_to_date']);
	                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));

	                $where = "AM.ADMISSION_DATE between '$from_date' and '$to_date'";

					$query = $this->db->where($where);

	    				  		}

	    				  if($postData['received_by'] != ''){
					$query = $this->db->like('EM.EMP_FNAME',$postData['received_by'],'after');
	    				  		}

	    				  if($postData['receipt_no_from'] != '' && $postData['receipt_no_to'] != ''){

	    			$from = $postData['receipt_no_from'];
	    			$to = $postData['receipt_no_to'];

					$where = "PRM.CENTRE_RECEIPT_ID  between '$from' and '$to'";

					$query = $this->db->where($where);

	    				  		}

	    				  if($postData['manual_receipt_from'] != '' && $postData['manual_receipt_to'] != ''){

					$from = $postData['manual_receipt_from'];
	    			$to = $postData['manual_receipt_to'];

					$where = "PRM.RECIEPT_NO between '$from' and '$to'";

					$query = $this->db->where($where);

	    				  		}

	    				  if($postData['deposit_date_from'] != '' && $postData['deposit_date_to'] != ''){

					$ad_date_format_change = str_replace("/","-",$postData['deposit_date_from']);
	                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

	                $ad_to_date_format_change = str_replace("/","-",$postData['deposit_date_to']);
	                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));

	                $where = "PRM.DATE_OF_DEPOSIT between '$from_date' and '$to_date'";

					$query = $this->db->where($where);

	    				  		}

	    				  if($postData['cheque_no'] != ''){
					$query = $this->db->where('PRM.CHEQUE_NO',$postData['cheque_no']);
	    				  		}

	    				  if($postData['deposition_status'] != ''){
	    				  	if($postData['deposition_status'] == 'deposited'){
					$query = $this->db->where('PRM.DEPOSIT_SLIP_NO IS NOT NULL');
	    				  	}
	    				  	else if($postData['deposition_status'] == 'undeposited'){
					$query = $this->db->where('PRM.DEPOSIT_SLIP_NO IS NULL');
	    				  	}
	    				  	}
	    				  $query = $this->db->order_by("PRM.PAYMENT_TYPE, PRM.CENTRE_RECEIPT_ID");
	    				  // if($count != 1){
	    				  // 	$query = $this->db->limit($perpage,$offset);
	    				  // }
		  				  $query = $this->db->where('PRM.ISACTIVE',"1")
	  					  					->get();
	  	if ($count == 1) {
	  		return $query->num_rows();
	  	}
	  	else{
	  		return $query->result_array();
	  	}
	}

	public function get_other_receipt_details_for_edit($receiptId,$type){
	    $query = $this->db->select('PRM.ADMISSION_ID,
 PRM.PENALTY_RECEIPT_ID AS ADMISSION_RECEIPTS_ID,
 PRM.PENALTY_RECEIPT_NO AS ADMISSION_RECEIPT_NO,
 PRM.CENTRE_RECEIPT_ID AS CENTRE_RECEIPT_ID,
 PRM.RECEIPT_NO AS RECEIPT_NO,
 CONCAT(EnM.ENQUIRY_FIRSTNAME, " ", EnM.ENQUIRY_LASTNAME) As STUDENT_NAME,
 CONCAT(EnM.ENQUIRY_ADDRESS1, " ", EnM.ENQUIRY_CITY," ",EnM.ENQUIRY_STATE," ",EnM.ENQUIRY_ZIP," ",EnM.ENQUIRY_COUNTRY) As ADDRESS,
 AM.ADMISSION_DATE,
 AM.COURSE_TAKEN,
 AM.TOTALFEES,
 PRM.PAYMENT_DATE,
 PRM.PAYMENT_TYPE,
 PRM.AMOUNT As AMOUNT_PAID,
 PRM.SERVICE_TAX_PAID As AMOUNT_PAID_SERVICETAX,
 if(PRM.BANK_NAME IS NULL," ",PRM.BANK_NAME) as BANK,
 if(PRM.CHEQUE_NO IS NULL," ",PRM.CHEQUE_NO) as CHEQUE_NO,
 if(PRM.CHEQUE_DATE IS NULL ," ",PRM.CHEQUE_DATE) as CHEQUE_DATE,
 if(PRM.BRANCH IS NULL ," ",PRM.BRANCH) as BRANCH,
 PRM.RECEIVED_BY,
 PRM.SERVICE_TAX,
 PRM.MODIFIED_BY,
 PRM.MODIFIED_DATE,
 PRM.AMOUNT_WITHOUT_ST As AMOUNT_PAID_FEES,
 PRM.SERVICE_TAX_PAID,
 PRM.UPDATE_REMARKS,
 PRM.CENTRE_ID,
 PRM.PARTICULAR,
 CM.CENTRE_NAME,
 CONCAT(IFNULL(EmM.EMP_FNAME, ""), " ",IFNULL(EmM.EMP_LASTNAME, "")) As EMPLOYEE_NAME')
	    				  ->from('penalties_receipt_master PRM')
	    				  ->join('admission_master AM', 'PRM.ADMISSION_ID = AM.ADMISSION_ID','left')
	    				  ->join('enquiry_master EnM', 'AM.ENQUIRY_ID = EnM.ENQUIRY_ID','left')
	    				  ->join('centre_master CM', 'PRM.CENTRE_ID = CM.CENTRE_ID','left')
	    				  ->join('employee_master EmM', 'PRM.RECEIVED_BY = EmM.EMPLOYEE_ID','left')
	    				  ->where('PRM.PENALTY_RECEIPT_ID',$receiptId);
	    				  if($type == "delete"){
	    				  	$query = $this->db->where('PRM.ISACTIVE',"0");
	    				  }
	    				  else{
	    				  	$query = $this->db->where('PRM.ISACTIVE',"1");
	    				  }
	  					  $query = $this->db->get();
	  	return $query->result_array();
	}

	public function get_other_receipt_details_by_admission_id($admissionId){
	    $query = $this->db->select('PRM.ADMISSION_ID,
 PRM.PENALTY_RECEIPT_ID AS ADMISSION_RECEIPTS_ID,
 PRM.PENALTY_RECEIPT_NO AS ADMISSION_RECEIPT_NO,
 PRM.RECEIPT_NO AS RECEIPT_NO,
 PRM.AMOUNT As AMOUNT_PAID,
 PRM.AMOUNT_WITHOUT_ST As AMOUNT_PAID_FEES,
 if(PRM.CHEQUE_NO IS NULL," ",PRM.CHEQUE_NO) as CHEQUE_NO,
 if(PRM.CHEQUE_DATE IS NULL ," ",PRM.CHEQUE_DATE) as CHEQUE_DATE,
 if(PRM.BANK_NAME IS NULL," ",PRM.BANK_NAME) as CHEQUE_BANK_NAME,
 PRM.PAYMENT_TYPE,
 PRM.PAYMENT_DATE,
 PRM.PARTICULAR')
	    				  ->from('penalties_receipt_master PRM')
	    				  ->join('admission_master AM', 'PRM.ADMISSION_ID = AM.ADMISSION_ID','left')
	    				  ->where('PRM.ADMISSION_ID',$admissionId)
	    				  ->order_by('PRM.PAYMENT_DATE','ASC')
	  					  ->get();
	  	return $query->result_array();
	}

public function update_other_receipts_details($receipts_details,$type){
		$trimmedArray = array_map('trim', $receipts_details);   // for removing spaces from array values

		$ad_date_format_change = str_replace("/","-",$trimmedArray['payment_date']);
    $payment_date = date('Y-m-d H:i:s', strtotime($ad_date_format_change));

		$cheque_date_format_change = str_replace("/","-",$trimmedArray['cheque_date']);
    $cheque_date = date('Y-m-d H:i:s', strtotime($cheque_date_format_change));

		$data = array(
				'CENTRE_ID' => $trimmedArray['centre'],
				'PAYMENT_TYPE' => $trimmedArray['payment_type'],
				'PAYMENT_DATE' => $payment_date,
				'AMOUNT' => $trimmedArray['amount_with_gst'],
				'AMOUNT_WITHOUT_ST' => $trimmedArray['amount_without_gst'],
				'SERVICE_TAX_PAID' => $trimmedArray['gst_paid'],
				'CHEQUE_DATE' => $cheque_date,
				'CHEQUE_NO' => $trimmedArray['cheque_no'],
				'BANK_NAME' => $trimmedArray['bank_name'],
				'BRANCH' => $trimmedArray['bank_branch'],
				'RECEIVED_BY' => $trimmedArray['received_by'],
				'PARTICULAR' => $trimmedArray['particular'],
				'UPDATE_REMARKS' => $trimmedArray['remarks'],
				'UPDATED_BY' => $this->session->userdata('admin_data')[0]['USER_ID'],
				'UPDATE_DATE' => date('Y-m-d H:i:s'));

    	$data = array_filter($data, function($value) { return !empty($value) || $value === 0; });   // for avoiding selection of blank values from actual array

    	if($type == "delete"){
    		$this->db->set('ISACTIVE',"0");		// for cancel    mysql
    	}
		$this->db->set($data)					// for update    mysql
                 ->where('PENALTY_RECEIPT_ID',$trimmedArray['receipt_id'])
                 ->where('ADMISSION_ID',$trimmedArray['admission_id'])
                 ->update('penalties_receipt_master');

	}

	public function update_transfer_other_receipts($from_admission_id,$to_admission_id,$remarks,$receipt_data){
    	for($i=0;$i<count($receipt_data);$i++){
			$data = array(
				'ADMISSION_ID' => $to_admission_id,
				'REMARK' => $remarks,
				'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['USER_ID'],
				'MODIFIED_DATE' => date('Y-m-d H:i:s'));
	    	$this->db->set($data)					// for update  mysql
                 ->where('ADMISSION_ID',$from_admission_id)
                 ->where('PENALTY_RECEIPT_ID',$receipt_data[$i])
                 ->update('penalties_receipt_master');

    	}

        if($this->db->affected_rows() > 0){
        	return 1;
        }
        else{
        	return 0;
        }
    }

    public function other_cash_check_deposition_data($offset,$centre_id,$perpage,$count){
    	$current_date = date("Y-m-d H:i:s");
    	$where = "PRM.PAYMENT_DATE BETWEEN '2015-01-01 00.00.00' and '$current_date'";
      $query = $this->db->select('PRM.ADMISSION_ID,
 PRM.PENALTY_RECEIPT_ID AS ADMISSION_RECEIPTS_ID,
 PRM.PENALTY_RECEIPT_NO AS ADMISSION_RECEIPT_NO,
 PRM.CENTRE_RECEIPT_ID AS CENTRE_RECEIPT_ID,
 PRM.RECEIPT_NO AS RECEIPT_NO,
 CONCAT(EM.ENQUIRY_FIRSTNAME, " ", EM.ENQUIRY_LASTNAME) As STUDENT_NAME,
 CONCAT(EM.ENQUIRY_ADDRESS1, " ", EM.ENQUIRY_CITY," ",EM.ENQUIRY_STATE," ",EM.ENQUIRY_ZIP," ",EM.ENQUIRY_COUNTRY) As ADDRESS,
 AM.ADMISSION_DATE,
 AM.COURSE_TAKEN,
 AM.TOTALFEES,
 PRM.PAYMENT_DATE,
 PRM.PAYMENT_TYPE,
 PRM.AMOUNT As AMOUNT_PAID,
 PRM.SERVICE_TAX_PAID As AMOUNT_PAID_SERVICETAX,
 if(PRM.BANK_NAME IS NULL," ",PRM.BANK_NAME) as BANK,
 if(PRM.CHEQUE_NO IS NULL," ",PRM.CHEQUE_NO) as CHEQUE_NO,
 if(PRM.CHEQUE_DATE IS NULL ," ",PRM.CHEQUE_DATE) as CHEQUE_DATE,
 if(PRM.BRANCH IS NULL ," ",PRM.BRANCH) as BRANCH,
 PRM.RECEIVED_BY,
 PRM.MODIFIED_BY,
 PRM.MODIFIED_DATE,
 PRM.AMOUNT_WITHOUT_ST As AMOUNT_PAID_FEES,
 PRM.SERVICE_TAX_PAID,
 PRM.UPDATE_REMARKS,
 PRM.CENTRE_ID,
 CM.CENTRE_NAME,
 CONCAT(IFNULL(EmM.EMP_FNAME, ""), " ",IFNULL(EmM.EMP_LASTNAME, "")) As EMPLOYEE_NAME')
                        ->from('penalties_receipt_master PRM')
                        ->join('admission_master AM','PRM.ADMISSION_ID = AM.ADMISSION_ID','left')
                        ->join('enquiry_master EM','AM.ENQUIRY_ID = EM.ENQUIRY_ID','left')
                        ->join('centre_master CM','CM.CENTRE_ID = AM.CENTRE_ID','left')
	    				->join('employee_master EmM', 'PRM.RECEIVED_BY = EmM.EMPLOYEE_ID','left')
                        ->where('PRM.AMOUNT_DEPOSITED IS NULL')
                        ->where('PRM.CENTRE_ID',$centre_id)
                        ->where('(PRM.ISACTIVE = "1" OR PRM.ISACTIVE IS NULL)')
                        ->where($where);
                        if($count != 1){
	    				  	$query = $this->db->limit($perpage,$offset);
	    				}
	    				$query = $this->db->order_by('PRM.PAYMENT_DATE','desc');
		  				$query = $this->db->get();
					  	if ($count == 1) {
					  		return $query->num_rows();
					  	}
					  	else{
					  		return $query->result_array();
					  	}
      return $query->result_array();
    }

    public function get_admission_report($offset,$searched_data,$perpage,$count){
		$query = $this->db->select('
CONCAT(EnM.ENQUIRY_FIRSTNAME, " ", EnM.ENQUIRY_LASTNAME) As STUDENT_NAME,
AM.ADMISSION_ID,
AM.ADMISSION_DATE as ADMISSION_DATE,
AM.COURSE_TAKEN,
AM.TOTALFEES')
	    				  ->from('admission_master AM')
	    				  ->join('enquiry_master EnM', 'AM.ENQUIRY_ID = EnM.ENQUIRY_ID')
	    				  ->join('employee_master EM', 'AM.HANDLED_BY = EM.EMPLOYEE_ID');

	    				  if($searched_data['ENQUIRY_FIRSTNAME'] != '' && $searched_data['ENQUIRY_LASTNAME'] != ''){
					$query = $this->db->like('EnM.ENQUIRY_FIRSTNAME',$searched_data['ENQUIRY_FIRSTNAME'],'after')
									->like('EnM.ENQUIRY_LASTNAME',$searched_data['ENQUIRY_LASTNAME'],'after');
	    				  }
	    				  else{
		    				  if($searched_data['ENQUIRY_FIRSTNAME'] != ''){
						$query = $this->db->like('EnM.ENQUIRY_FIRSTNAME',$searched_data['ENQUIRY_FIRSTNAME'],'after');
		    				  }
		    				  else if($searched_data['ENQUIRY_LASTNAME'] != ''){
						$query = $this->db->like('EnM.ENQUIRY_LASTNAME',$searched_data['ENQUIRY_LASTNAME'],'after');
		    				  }
	    				  }

	    				  if($searched_data['CENTRE_ID'] != ''){
					$query = $this->db->where('AM.CENTRE_ID',$searched_data['CENTRE_ID']);
	    				  }

	    				  if($searched_data['ADMISSION_DATE_FROM'] != '' && $searched_data['ADMISSION_DATE_TO'] != ''){
	                $ad_date_format_change = str_replace("/","-",$searched_data['ADMISSION_DATE_FROM']);
	                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

	                $ad_to_date_format_change = str_replace("/","-",$searched_data['ADMISSION_DATE_TO']);
	                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));

					$query = $this->db->where('AM.ADMISSION_DATE > ',$from_date)
	                                  ->where('AM.ADMISSION_DATE < ',$to_date);
	    				  		}

				  			if($searched_data['ADMISSION_DATE'] != '' ){
			                $ad_date_format_change = str_replace("/","-",$searched_data['ADMISSION_DATE']);
			                $adm_date = date('Y-m-d', strtotime($ad_date_format_change));

							$query = $this->db->where('AM.ADMISSION_DATE',$adm_date);
			    				  		}

			    				  if($searched_data['HANDLED_BY'] != ''){
							$query = $this->db->like('AM.HANDLED_BY',$searched_data['HANDLED_BY'],'after');
	    				  		}

	    				  if($count != 1){
	    				  	$query = $this->db->limit($perpage,$offset);
	    				  }
		  				  $query = $this->db->get();
	  	if ($count == 1) {
	  		return $query->num_rows();
	  	}
	  	else{
	  		return $query->result_array();
	  	}
    }

    public function get_defaulters_list($offset,$searched_data,$perpage,$count){
		$query = $this->db->select('AIT.DUEDATE,AIT.DUE_AMOUNT,AIT.ADMISSION_INSTALLEMENT_ID,CONCAT(EnM.ENQUIRY_FIRSTNAME, " ", EnM.ENQUIRY_LASTNAME) As STUDENT_NAME,
AM.ADMISSION_ID,
AM.ADMISSION_DATE as ADMISSION_DATE,
AM.COURSE_TAKEN,
AM.TOTALFEES')
	    				  ->from('admission_master AM')
	    				  ->join('enquiry_master EnM', 'AM.ENQUIRY_ID = EnM.ENQUIRY_ID')
	    				  ->join('employee_master EM', 'AM.HANDLED_BY = EM.EMPLOYEE_ID')
	    				  ->join('admission_installment_transaction AIT', 'AIT.ADMISSION_ID = AM.ADMISSION_ID');

	    				  if($searched_data['CENTRE'] != ''){
					$query = $this->db->where('AM.CENTRE_ID',$searched_data['CENTRE']);
	    				  }

	    				  if($searched_data['ADMISSION_DATE_FROM'] != '' && $searched_data['ADMISSION_DATE_TO'] != ''){
	                $ad_date_format_change = str_replace("/","-",$searched_data['ADMISSION_DATE_FROM']);
	                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

	                $ad_to_date_format_change = str_replace("/","-",$searched_data['ADMISSION_DATE_TO']);
	                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));

					$query = $this->db->where('AM.ADMISSION_DATE > ',$from_date)
	                                  ->where('AM.ADMISSION_DATE < ',$to_date);
    				  	}
	    				 $query = $this->db->limit($perpage,$offset);

		  				  $query = $this->db->get();
	  	if ($count == 1) {
	  		return $query->num_rows();
	  	}
	  	else{
	  		return $query->result_array();
	  	}
    }

    public function get_outstanding_student_data($offset,$searched_data,$perpage,$count){
		$query = $this->db->select('CONCAT(EnM.ENQUIRY_FIRSTNAME, " ", EnM.ENQUIRY_LASTNAME) As STUDENT_NAME,
AM.ADMISSION_ID,
AM.ADMISSION_DATE as ADMISSION_DATE,
AM.COURSE_TAKEN,
AM.MODULE_TAKEN,
AM.TOTALFEES,
if(EnM.ENQUIRY_MOBILE_NO IS NULL,0,EnM.ENQUIRY_MOBILE_NO) AS CONTACT_NO,
CM.CENTRE_NAME')
	    				  ->from('admission_master AM')
	    				  ->join('centre_master CM', 'AM.CENTRE_ID = CM.CENTRE_ID')
	    				  ->join('enquiry_master EnM', 'AM.ENQUIRY_ID = EnM.ENQUIRY_ID');

	    				  if($searched_data['CENTRE_ID'] != ''){
					$query = $this->db->where('AM.CENTRE_ID',$searched_data['CENTRE_ID']);
	    				  }

	    				  if($searched_data['ENQUIRY_FIRSTNAME'] != '' && $searched_data['ENQUIRY_LASTNAME'] != ''){
					$query = $this->db->like('EnM.ENQUIRY_FIRSTNAME',$searched_data['ENQUIRY_FIRSTNAME'],'after')
									->like('EnM.ENQUIRY_LASTNAME',$searched_data['ENQUIRY_LASTNAME'],'after');
	    				  }
	    				  else{
		    				  if($searched_data['ENQUIRY_FIRSTNAME'] != ''){
						$query = $this->db->like('EnM.ENQUIRY_FIRSTNAME',$searched_data['ENQUIRY_FIRSTNAME'],'after');
		    				  }
		    				  else if($searched_data['ENQUIRY_LASTNAME'] != ''){
						$query = $this->db->like('EnM.ENQUIRY_LASTNAME',$searched_data['ENQUIRY_LASTNAME'],'after');
		    				  }
	    				  }

	    				  if($searched_data['ADMISSION_DATE_FROM'] != '' && $searched_data['ADMISSION_DATE_TO'] != ''){
	                $ad_date_format_change = str_replace("/","-",$searched_data['ADMISSION_DATE_FROM']);
	                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

	                $ad_to_date_format_change = str_replace("/","-",$searched_data['ADMISSION_DATE_TO']);
	                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));

					$query = $this->db->where('AM.ADMISSION_DATE > ',$from_date)
	                                  ->where('AM.ADMISSION_DATE < ',$to_date);
    				  	}

    				  if($count != 1){
    				  //	$query = $this->db->limit($perpage,$offset);
    				  }
	  				  $query = $this->db->get();

		  	if ($count == 1) {
		  		return $query->num_rows();
		  	}
		  	else{
	  			// return $data['outstanding_data']['receipt_data'];
	  			return $query->result_array();
		  	}
    }

public function get_deleted_receipts_details($offset,$postData,$perpage,$count){
	    $query = $this->db->select('AR.ADMISSION_RECEIPTS_ID,
AR.ADMISSION_RECEIPT_NO,
AR.CENTRE_RECEIPT_ID,
AR.RECIEPT_NO AS RECEIPT_NO,
CM.CENTRE_ID,
CM.CENTRE_NAME as CENTRE_NAME,
CONCAT(EnM.ENQUIRY_FIRSTNAME, " ", EnM.ENQUIRY_LASTNAME) As STUDENT_NAME,
CONCAT(EnM.ENQUIRY_MOBILE_NO, ", ", EnM.ENQUIRY_TELEPHONE) As Contact,
AM.ADMISSION_ID,
AM.ADMISSION_DATE as ADMISSION_DATE,
AR.PAYMENT_DATE AS PAYMENT_DATE ,
AR.PAYMENT_TYPE,
AR.AMOUNT_PAID,
AR.AMOUNT_PAID_FEES AS AMOUNT_W/O_ST,
if(AR.CHEQUE_BANK_NAME IS NULL ," ",AR.CHEQUE_BANK_NAME) as BANK,
if(AR.CHEQUE_NO IS NULL ," ",AR.CHEQUE_NO) as CHEQUE_NO,
AR.CHEQUE_DATE AS CHEQUE_DATE,
if(AR.CHEQUE_BRANCH IS NULL ," ",AR.CHEQUE_BRANCH) as BRANCH,
CONCAT(IFNULL(EM.EMP_FNAME, ""), " ",IFNULL(EM.EMP_LASTNAME, "")) AS EMPLOYEE_NAME,
if(AR.AMOUNT_DEPOSITED IS NULL,0,AR.AMOUNT_DEPOSITED)as AMOUNT_DEPOSITED,
if(AR.AMOUNT_DEPOSITED2 IS NULL,0,AR.AMOUNT_DEPOSITED2)as AMOUNT_DEPOSITED2,
AR.DATE_OF_DEPOSIT,
AR.DATE_OF_DEPOSIT2,
AR.DEPOSIT_SLIP_NO AS DEPOSIT_SLIP_NO,
AR.DEPOSIT_SLIP_NO2 AS SLIP_NO2,
if(AR.STATUS IS NULL,0,AR.STATUS) AS STATUS,
AR.UPDATE_REMARKS,
AR.AMOUNT_PAID-if(AR.AMOUNT_DEPOSITED IS NULL,0,AR.AMOUNT_DEPOSITED)- if(AR.AMOUNT_DEPOSITED2 IS NULL,0,AR.AMOUNT_DEPOSITED2) AS BALANCE,
if(AR.INTERNAL IS NULL,0,AR.INTERNAL) AS INTERNAL,
AR.COLLECTION_MONTH')
	    				  ->from('admission_master AM')
	    				  ->join('admission_receipts AR', 'AM.ADMISSION_ID = AR.ADMISSION_ID')
	    				  ->join('enquiry_master EnM', 'AM.ENQUIRY_ID = EnM.ENQUIRY_ID')
	    				  ->join('employee_master EM', 'EM.EMPLOYEE_ID= AR.RECEIVED_BY ')
	    				  ->join('centre_master CM', 'CM.CENTRE_ID=AR.CENTRE_ID');

	    				  if($postData['receipt_from_date'] != '' && $postData['receipt_to_date'] != ''){


	                $date_format_change = str_replace("/","-",$postData['receipt_from_date']);
	                $from_date = date('Y-m-d', strtotime($date_format_change));

	                $to_date_format_change = str_replace("/","-",$postData['receipt_to_date']);
	                $to_date = date('Y-m-d', strtotime($to_date_format_change));

	                $where = "AR.PAYMENT_DATE between '$from_date' and '$to_date'";

					$query = $this->db->where($where);
	    				  		}

	    				  if($postData['centre'] != ''){
					$query = $this->db->where('AM.CENTRE_ID',$postData['centre']);
	    				  }

	    				  if($postData['cheque_bounce_only'] != 'off'){
					$query = $this->db->like('AR.UPDATE_REMARKS','bounce','both');
	    				  }

	    				  $query = $this->db->order_by("AR.PAYMENT_TYPE, AR.CENTRE_RECEIPT_ID");
	    				  if($count != 1){
	    				  	$query = $this->db->limit($perpage,$offset);
	    				  }
		  				  $query = $this->db->where('AR.ISACTIVE',"0")
	  					  					->get();
	  	if ($count == 1) {
	  		return $query->num_rows();
	  	}
	  	else{
	  		return $query->result_array();
	  	}
	}

	public function get_bounce_penalty_receipt_no($admission_id){
		$query = $this->db->select('PENALTY_RECEIPT_NO')
	    				  ->from('penalties_receipt_master')
						  ->where('ADMISSION_ID',$admission_id)
						  ->where('PARTICULAR','CHEQUE BOUNCE')
	  					  ->get();

	  		return $query->result_array();
	}

    public function get_deleted_other_receipts($offset,$searched_data,$perpage,$count){
		$query = $this->db->select('PRM.ADMISSION_ID,
 PRM.PENALTY_RECEIPT_ID AS ADMISSION_RECEIPTS_ID,
 PRM.PENALTY_RECEIPT_NO AS ADMISSION_RECEIPT_NO,
 PRM.CENTRE_RECEIPT_ID AS CENTRE_RECEIPT_ID,
 PRM.RECEIPT_NO AS RECEIPT_NO,
 CONCAT(EnM.ENQUIRY_FIRSTNAME, " ", EnM.ENQUIRY_LASTNAME) As STUDENT_NAME,
 CONCAT(EnM.ENQUIRY_ADDRESS1, " ", EnM.ENQUIRY_CITY," ",EnM.ENQUIRY_STATE," ",EnM.ENQUIRY_ZIP," ",EnM.ENQUIRY_COUNTRY) As ADDRESS,
 AM.ADMISSION_DATE,
 AM.COURSE_TAKEN,
 AM.TOTALFEES,
 PRM.PAYMENT_DATE,
 PRM.PAYMENT_TYPE,
 PRM.AMOUNT As AMOUNT_PAID,
 PRM.AMOUNT_WITHOUT_ST As AMOUNT_W/O_ST,
 PRM.AMOUNT_DEPOSITED As AMOUNT_DEPOSITED,
 PRM.DATE_OF_DEPOSIT As DATE_OF_DEPOSIT,
 PRM.DEPOSIT_SLIP_NO As DEPOSIT_SLIP_NO,
 if(PRM.BANK_NAME IS NULL," ",PRM.BANK_NAME) as BANK,
 if(PRM.CHEQUE_NO IS NULL," ",PRM.CHEQUE_NO) as CHEQUE_NO,
 if(PRM.CHEQUE_DATE IS NULL ," ",PRM.CHEQUE_DATE) as CHEQUE_DATE,
 if(PRM.BRANCH IS NULL ," ",PRM.BRANCH) as BRANCH,
 PRM.RECEIVED_BY,
 PRM.UPDATE_REMARKS,
 PRM.CENTRE_ID,
 PRM.ISACTIVE,
 CONCAT(IFNULL(EM.EMP_FNAME, ""), " ",IFNULL(EM.EMP_LASTNAME, "")) As EMPLOYEE_NAME')
	    				  ->from('penalties_receipt_master PRM')
	    				  ->join('admission_master AM', 'PRM.ADMISSION_ID = AM.ADMISSION_ID')
	    				  ->join('employee_master EM', 'PRM.RECEIVED_BY = EM.EMPLOYEE_ID')
	    				  ->join('enquiry_master EnM', 'AM.ENQUIRY_ID = EnM.ENQUIRY_ID')
	    				  ->where('PRM.ISACTIVE=0');

	    				  if($searched_data['CENTRE_ID'] != ''){
					$query = $this->db->where('PRM.CENTRE_ID',$searched_data['CENTRE_ID']);
	    				  }

	    				  if($searched_data['cheque_bounce_only'] != 'off'){
					$query = $this->db->like('PRM.UPDATE_REMARKS','bounce','both');
	    				  }

	    				  if($searched_data['RECEIPT_DATE_FROM'] != '' && $searched_data['RECEIPT_DATE_TO'] != ''){
	                $ad_date_format_change = str_replace("/","-",$searched_data['RECEIPT_DATE_FROM']);
	                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

	                $ad_to_date_format_change = str_replace("/","-",$searched_data['RECEIPT_DATE_TO']);
	                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));

	                $where = "PRM.PAYMENT_DATE between '$from_date' and '$to_date'";

					$query = $this->db->where($where);

    				  	}

    				  if($count != 1){
    				  	$query = $this->db->limit($perpage,$offset);
    				  }
	  				  $query = $this->db->get();

		  	if ($count == 1) {
		  		return $query->num_rows();
		  	}
		  	else{
	  			return $query->result_array();
		  	}
    }

    public function export_to_excel_deleted_other_receipts($searched_data){
		$query = $this->db->select('PRM.ADMISSION_ID,
 PRM.PENALTY_RECEIPT_ID AS ADMISSION_RECEIPTS_ID,
 PRM.PENALTY_RECEIPT_NO AS ADMISSION_RECEIPT_NO,
 PRM.CENTRE_RECEIPT_ID AS CENTRE_RECEIPT_ID,
 PRM.RECEIPT_NO AS RECEIPT_NO,
 CONCAT(EnM.ENQUIRY_FIRSTNAME, " ", EnM.ENQUIRY_LASTNAME) As STUDENT_NAME,
 CONCAT(EnM.ENQUIRY_ADDRESS1, " ", EnM.ENQUIRY_CITY," ",EnM.ENQUIRY_STATE," ",EnM.ENQUIRY_ZIP," ",EnM.ENQUIRY_COUNTRY) As ADDRESS,
 AM.ADMISSION_DATE,
 AM.COURSE_TAKEN,
 AM.TOTALFEES,
 PRM.PAYMENT_DATE,
 PRM.PAYMENT_TYPE,
 PRM.AMOUNT As AMOUNT_PAID,
 PRM.AMOUNT_WITHOUT_ST As AMOUNT_W/O_ST,
 PRM.AMOUNT_DEPOSITED As AMOUNT_DEPOSITED,
 PRM.DATE_OF_DEPOSIT As DATE_OF_DEPOSIT,
 PRM.DEPOSIT_SLIP_NO As DEPOSIT_SLIP_NO,
 if(PRM.BANK_NAME IS NULL," ",PRM.BANK_NAME) as BANK,
 if(PRM.CHEQUE_NO IS NULL," ",PRM.CHEQUE_NO) as CHEQUE_NO,
 if(PRM.CHEQUE_DATE IS NULL ," ",PRM.CHEQUE_DATE) as CHEQUE_DATE,
 if(PRM.BRANCH IS NULL ," ",PRM.BRANCH) as BRANCH,
 PRM.RECEIVED_BY,
 PRM.UPDATE_REMARKS,
 PRM.CENTRE_ID,
 PRM.ISACTIVE,
 CONCAT(IFNULL(EM.EMP_FNAME, ""), " ",IFNULL(EM.EMP_LASTNAME, "")) As EMPLOYEE_NAME')
	    				  ->from('penalties_receipt_master PRM')
	    				  ->join('admission_master AM', 'PRM.ADMISSION_ID = AM.ADMISSION_ID')
	    				  ->join('employee_master EM', 'PRM.RECEIVED_BY = EM.EMPLOYEE_ID')
	    				  ->join('enquiry_master EnM', 'AM.ENQUIRY_ID = EnM.ENQUIRY_ID')
	    				  ->where('PRM.ISACTIVE=0');

	    				  if($searched_data['CENTRE_ID'] != ''){
					$query = $this->db->where('PRM.CENTRE_ID',$searched_data['CENTRE_ID']);
	    				  }

	    				  if(array_key_exists("cheque_bounce_only", $searched_data)){
		    				  if($searched_data['cheque_bounce_only'] != ''){
						$query = $this->db->like('PRM.UPDATE_REMARKS','bounce','both');
		    				  }
	    				  }

	    				  if($searched_data['RECEIPT_DATE_FROM'] != '' && $searched_data['RECEIPT_DATE_TO'] != ''){
	                $ad_date_format_change = str_replace("/","-",$searched_data['RECEIPT_DATE_FROM']);
	                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

	                $ad_to_date_format_change = str_replace("/","-",$searched_data['RECEIPT_DATE_TO']);
	                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));

	                $where = "PRM.PAYMENT_DATE between '$from_date' and '$to_date'";

					$query = $this->db->where($where);

    				  	}


	  				  $query = $this->db->get();

		  	$data['fields'] = $query->list_fields();
		  	$data['details'] = $query->result_array();
	  			return $data;

    }


public function export_to_excel_all_receipts_report($searched_data){
    	if($searched_data['option'] == "off"){
			$query = $this->db->select('AR.ADMISSION_RECEIPTS_ID,
			AR.ADMISSION_RECEIPT_NO,
			AR.CENTRE_RECEIPT_ID,
			AR.RECIEPT_NO AS RECEIPT_NO,
			CM.CENTRE_ID,
			CM.CENTRE_NAME as CENTRE_NAME,
			CONCAT(EnM.ENQUIRY_FIRSTNAME, " ", EnM.ENQUIRY_LASTNAME) As STUDENT_NAME,
			AM.ADMISSION_ID,
			AM.ADMISSION_DATE as ADMISSION_DATE,
			AR.PAYMENT_DATE AS PAYMENT_DATE ,
			AR.PAYMENT_TYPE,
			"Course Fees" as PAYMENT_FOR,
			AR.AMOUNT_PAID,
			AR.AMOUNT_PAID_FEES,
			if(AR.CHEQUE_BANK_NAME IS NULL ," ",AR.CHEQUE_BANK_NAME) as BANK,
			if(AR.CHEQUE_NO IS NULL ," ",AR.CHEQUE_NO) as CHEQUE_NO,
			AR.CHEQUE_DATE AS CHEQUE_DATE,
			if(AR.CHEQUE_BRANCH IS NULL ," ",AR.CHEQUE_BRANCH) as BRANCH,
			CONCAT(IFNULL(EM.EMP_FNAME, ""), " ",IFNULL(EM.EMP_LASTNAME, "")) AS RECEIVED_BY,
			if(AR.AMOUNT_DEPOSITED IS NULL,0,AR.AMOUNT_DEPOSITED)as AMOUNT_DEPOSITED,
			if(AR.AMOUNT_DEPOSITED2 IS NULL,0,AR.AMOUNT_DEPOSITED2)as AMOUNT_DEPOSITED2,
			AR.DATE_OF_DEPOSIT,
			AR.DATE_OF_DEPOSIT2,
			AR.DEPOSIT_SLIP_NO AS SLIP_NO,
			AR.DEPOSIT_SLIP_NO2 AS SLIP_NO2,
			if(AR.STATUS IS NULL,0,AR.STATUS) AS STATUS,
			AR.REMARKS,
			if(AR.INTERNAL IS NULL,0,AR.INTERNAL) AS INTERNAL,
			AR.COLLECTION_MONTH
			')
	    				  ->from('admission_master AM')
	    				  ->join('admission_receipts AR', 'AM.ADMISSION_ID = AR.ADMISSION_ID')
	    				  ->join('enquiry_master EnM', 'AM.ENQUIRY_ID = EnM.ENQUIRY_ID')
	    				  ->join('employee_master EM', 'EM.EMPLOYEE_ID= AR.RECEIVED_BY ')
	    				  ->join('centre_master CM', 'CM.CENTRE_ID=AR.CENTRE_ID');

	    				  if($searched_data['first_name'] != '' && $searched_data['last_name'] != ''){
					$query = $this->db->like('EnM.ENQUIRY_FIRSTNAME',$searched_data['first_name'],'after')
									->like('EnM.ENQUIRY_LASTNAME',$searched_data['last_name'],'after');
	    				  }
	    				  else{
		    				  if($searched_data['first_name'] != ''){
						$query = $this->db->like('EnM.ENQUIRY_FIRSTNAME',$searched_data['first_name'],'after');
		    				  }
		    				  else if($searched_data['last_name'] != ''){
						$query = $this->db->like('EnM.ENQUIRY_LASTNAME',$searched_data['last_name'],'after');
		    				  }
	    				  }

	    				  if($searched_data['payment_type'] != ''){

						$payment_type = "";
    				  	switch ($searched_data['payment_type']) {
    				  		case 'cash':
    				  			$payment_type = 1;
    				  		break;
    				  		case 'cc':
    				  			$payment_type = 2;
    				  		break;
    				  		case 'cheque':
    				  			$payment_type = 3;
    				  		break;
    				  	}

    				  	$pay_type = $searched_data['payment_type'];

    				  	$where = "(AR.PAYMENT_TYPE = '$payment_type' or AR.PAYMENT_TYPE = '$pay_type')";

						$this->db->where($where);

	    				  }

	    				  if($searched_data['cheque_no'] != ''){
					$query = $this->db->where('AR.CHEQUE_NO',$searched_data['cheque_no']);
    				  		}

    				  		if($searched_data['received_by'] != ''){
					$query = $this->db->like('EM.EMP_FNAME',$searched_data['received_by'],'after');
	    				  		}

	    				  if($searched_data['receipt_from_date'] != '' && $searched_data['receipt_to_date'] != ''){
	                $date_format_change = str_replace("/","-",$searched_data['receipt_from_date']);
	                $from_date = date('Y-m-d', strtotime($date_format_change));

	                $to_date_format_change = str_replace("/","-",$searched_data['receipt_to_date']);
	                $to_date = date('Y-m-d', strtotime($to_date_format_change));

					$query = $this->db->where('AR.PAYMENT_DATE > ',$from_date)
	                                  ->where('AR.PAYMENT_DATE < ',$to_date);
	    				  		}
	    				  if($searched_data['centre'] != ''){
					$query = $this->db->where('AM.CENTRE_ID',$searched_data['centre']);
	    				  }

	    				  if($searched_data['admission_from_date'] != '' && $searched_data['admission_to_date'] != ''){
	                $ad_date_format_change = str_replace("/","-",$searched_data['admission_from_date']);
	                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

	                $ad_to_date_format_change = str_replace("/","-",$searched_data['admission_to_date']);
	                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));

					$query = $this->db->where('AM.ADMISSION_DATE > ',$from_date)
	                                  ->where('AM.ADMISSION_DATE < ',$to_date);
	    				  		}


	    				  if($searched_data['receipt_no_from'] != '' && $searched_data['receipt_no_to'] != ''){
					$query = $this->db->where('AR.CENTRE_RECEIPT_ID > ',$searched_data['receipt_no_from'])
	                                  ->where('AR.CENTRE_RECEIPT_ID < ',$searched_data['receipt_no_to']);
	    				  		}

	    				  if($searched_data['manual_receipt_from'] != '' && $searched_data['manual_receipt_to'] != ''){
					$query = $this->db->where('AR.RECIEPT_NO > ',$searched_data['manual_receipt_from'])
	                                  ->where('AR.RECIEPT_NO < ',$searched_data['manual_receipt_to']);
	    				  		}

	    				  if($searched_data['deposit_date_from'] != '' && $searched_data['deposit_date_to'] != ''){
					$query = $this->db->where('AR.DATE_OF_DEPOSIT > ',$searched_data['deposit_date_from'])
	                                  ->where('AR.DATE_OF_DEPOSIT < ',$searched_data['deposit_date_to']);
	    				  		}

	    				  if($searched_data['deposition_status'] != ''){
	    				  	if($searched_data['deposition_status'] == 'deposited'){
					$query = $this->db->where('AR.DEPOSIT_SLIP_NO IS NOT NULL');
	    				  	}
	    				  	else if($searched_data['deposition_status'] == 'undeposited'){
					$query = $this->db->where('AR.DEPOSIT_SLIP_NO IS NULL');
	    				  	}
	    				  	}
	    				  $query = $this->db->order_by("AR.PAYMENT_TYPE, AR.CENTRE_RECEIPT_ID")
	    				  					->where('AR.ISACTIVE',"1");
    	}
    	else{
			$query = $this->db->select('PRM.PENALTY_RECEIPT_ID AS ADMISSION_RECEIPTS_ID,
			PRM.PENALTY_RECEIPT_NO AS ADMISSION_RECEIPT_NO,
			PRM.CENTRE_RECEIPT_ID AS CENTRE_RECEIPT_ID,
			PRM.RECEIPT_NO AS RECEIPT_NO,
			CM1.CENTRE_NAME as CENTRE_NAME,
			CONCAT(EnM1.ENQUIRY_FIRSTNAME, " ", EnM1.ENQUIRY_LASTNAME) As STUDENT_NAME,
			AM1.ADMISSION_ID,
			AM1.ADMISSION_DATE as ADMISSION_DATE,
			PRM.PAYMENT_DATE AS PAYMENT_DATE ,
			PRM.PAYMENT_TYPE,
			PRM.PARTICULAR AS PAYMENT_FOR,
			PRM.AMOUNT As AMOUNT_PAID,
			PRM.AMOUNT_WITHOUT_ST As AMOUNT_PAID_FEES,
			if(PRM.BANK_NAME IS NULL," ",PRM.BANK_NAME) as BANK,
			if(PRM.CHEQUE_NO IS NULL," ",PRM.CHEQUE_NO) as CHEQUE_NO,
			PRM.CHEQUE_DATE AS CHEQUE_DATE,
			if(PRM.BRANCH IS NULL ," ",PRM.BRANCH) as BRANCH,
			CONCAT(IFNULL(EM1.EMP_FNAME, ""), " ",IFNULL(EM1.EMP_LASTNAME, "")) AS RECEIVED_BY,
			if(PRM.AMOUNT_DEPOSITED IS NULL,0,PRM.AMOUNT_DEPOSITED)as AMOUNT_DEPOSITED,
			if(PRM.AMOUNT_DEPOSITED2 IS NULL,0,PRM.AMOUNT_DEPOSITED2)as AMOUNT_DEPOSITED2,
			PRM.DATE_OF_DEPOSIT,
			PRM.DATE_OF_DEPOSIT2,
			PRM.DEPOSIT_SLIP_NO AS SLIP_NO,
			PRM.DEPOSIT_SLIP_NO2 AS SLIP_NO2,
			PRM.REMARK,
			if(PRM.STATUS IS NULL,0,PRM.STATUS) AS STATUS')
				    				  ->from('admission_master AM1')
				    				  ->join('penalties_receipt_master PRM', 'AM1.ADMISSION_ID = PRM.ADMISSION_ID')
				    				  ->join('enquiry_master EnM1', 'AM1.ENQUIRY_ID = EnM1.ENQUIRY_ID')
				    				  ->join('employee_master EM1', 'EM1.EMPLOYEE_ID= PRM.RECEIVED_BY ')
				    				  ->join('centre_master CM1', 'CM1.CENTRE_ID=PRM.CENTRE_ID');

				    				  if($searched_data['first_name'] != '' && $searched_data['last_name'] != ''){
								$query = $this->db->like('EnM1.ENQUIRY_FIRSTNAME',$searched_data['first_name'],'after')
												->like('EnM1.ENQUIRY_LASTNAME',$searched_data['last_name'],'after');
				    				  }
				    				  else{
					    				  if($searched_data['first_name'] != ''){
									$query = $this->db->like('EnM1.ENQUIRY_FIRSTNAME',$searched_data['first_name'],'after');
					    				  }
					    				  else if($searched_data['last_name'] != ''){
									$query = $this->db->like('EnM1.ENQUIRY_LASTNAME',$searched_data['last_name'],'after');
					    				  }
				    				  }


				    				  if($searched_data['payment_type'] != ''){

			    				  	$payment_type = "";
			    				  	switch ($searched_data['payment_type']) {
			    				  		case 'cash':
			    				  			$payment_type = 1;
			    				  		break;
			    				  		case 'cc':
			    				  			$payment_type = 2;
			    				  		break;
			    				  		case 'cheque':
			    				  			$payment_type = 3;
			    				  		break;
			    				  	}

			    				  	$pay_type = $searched_data['payment_type'];

			    				  	$where = "(PRM.PAYMENT_TYPE = '$payment_type' or PRM.PAYMENT_TYPE = '$pay_type')";

									$this->db->where($where);

				    				  }

								if($searched_data['cheque_no'] != ''){
									$query = $this->db->where('PRM.CHEQUE_NO',$searched_data['cheque_no']);
			    				}

			    				if($searched_data['received_by'] != ''){
					$query = $this->db->like('EM1.EMP_FNAME',$searched_data['received_by'],'after');
	    				  		}

				    				  if($searched_data['receipt_from_date'] != '' && $searched_data['receipt_to_date'] != ''){
				                $date_format_change = str_replace("/","-",$searched_data['receipt_from_date']);
				                $from_date = date('Y-m-d', strtotime($date_format_change));

				                $to_date_format_change = str_replace("/","-",$searched_data['receipt_to_date']);
				                $to_date = date('Y-m-d', strtotime($to_date_format_change));

								$query = $this->db->where('PRM.PAYMENT_DATE > ',$from_date)
				                                  ->where('PRM.PAYMENT_DATE < ',$to_date);
				    				  		}
				    				  if($searched_data['centre'] != ''){
								$query = $this->db->where('AM1.CENTRE_ID',$searched_data['centre']);
				    				  }

				    				  if($searched_data['admission_from_date'] != '' && $searched_data['admission_to_date'] != ''){
				                $ad_date_format_change = str_replace("/","-",$searched_data['admission_from_date']);
				                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

				                $ad_to_date_format_change = str_replace("/","-",$searched_data['admission_to_date']);
				                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));

								$query = $this->db->where('AM1.ADMISSION_DATE > ',$from_date)
				                                  ->where('AM1.ADMISSION_DATE < ',$to_date);
				    				  		}

				    				  if($searched_data['receipt_no_from'] != '' && $searched_data['receipt_no_to'] != ''){
								$query = $this->db->where('PRM.CENTRE_RECEIPT_ID > ',$searched_data['receipt_no_from'])
				                                  ->where('PRM.CENTRE_RECEIPT_ID < ',$searched_data['receipt_no_to']);
				    				  		}

				    				  if($searched_data['manual_receipt_from'] != '' && $searched_data['manual_receipt_to'] != ''){
								$query = $this->db->where('PRM.PENALTY_RECEIPT_NO > ',$searched_data['manual_receipt_from'])
				                                  ->where('PRM.PENALTY_RECEIPT_NO < ',$searched_data['manual_receipt_to']);
				    				  		}

				    				  if($searched_data['deposit_date_from'] != '' && $searched_data['deposit_date_to'] != ''){
								$query = $this->db->where('PRM.DATE_OF_DEPOSIT > ',$searched_data['deposit_date_from'])
				                                  ->where('PRM.DATE_OF_DEPOSIT < ',$searched_data['deposit_date_to']);
				    				  		}

				    				  if($searched_data['deposition_status'] != ''){
				    				  	if($searched_data['deposition_status'] == 'deposited'){
								$query = $this->db->where('PRM.DEPOSIT_SLIP_NO IS NOT NULL');
				    				  	}
				    				  	else if($searched_data['deposition_status'] == 'undeposited'){
								$query = $this->db->where('PRM.DEPOSIT_SLIP_NO IS NULL');
				    				  	}
				    				  	}
				    				  $query = $this->db->order_by("PRM.PAYMENT_TYPE, PRM.CENTRE_RECEIPT_ID")
				    				  					->where('PRM.ISACTIVE',"1");
    	}


			//$query = $this->db->limit(1,10000);

			$query = $this->db->get();

	  		$receipts_details['details'] = $query->result_array();

		  	$receipts_details['fields'] = $query->list_fields();

        	return $receipts_details;
    }



    public function get_receipts_report($searched_data,$count){
    	$this->db->select('AR.ADMISSION_RECEIPTS_ID AS ADMISSION_RECEIPTS_ID,
			AR.ADMISSION_RECEIPT_NO AS ADMISSION_RECEIPT_NO,
			AR.CENTRE_RECEIPT_ID AS CENTRE_RECEIPT_ID,
			AR.RECIEPT_NO AS RECEIPT_NO,
			CM.CENTRE_NAME as CENTRE_NAME,
			CONCAT(EnM.ENQUIRY_FIRSTNAME, " ", EnM.ENQUIRY_LASTNAME) As STUDENT_NAME,
			AM.ADMISSION_ID AS ADMISSION_ID,
			AM.ADMISSION_DATE as ADMISSION_DATE,
			AR.PAYMENT_DATE AS PAYMENT_DATE ,
			AR.PAYMENT_TYPE AS PAYMENT_TYPE,
			"Course Fees" as PAYMENT_FOR,
			AR.AMOUNT_PAID as AMOUNT_PAID,
			AR.AMOUNT_PAID_FEES as AMOUNT_PAID_FEES,
			if(AR.CHEQUE_BANK_NAME IS NULL ," ",AR.CHEQUE_BANK_NAME) as BANK,
			if(AR.CHEQUE_NO IS NULL ," ",AR.CHEQUE_NO) as CHEQUE_NO,
			AR.CHEQUE_DATE AS CHEQUE_DATE,
			if(AR.CHEQUE_BRANCH IS NULL ," ",AR.CHEQUE_BRANCH) as BRANCH,
			CONCAT(IFNULL(EM.EMP_FNAME, ""), " ",IFNULL(EM.EMP_LASTNAME, "")) AS RECEIVED_BY,
			if(AR.AMOUNT_DEPOSITED IS NULL,0,AR.AMOUNT_DEPOSITED) as AMOUNT_DEPOSITED,
			if(AR.AMOUNT_DEPOSITED2 IS NULL,0,AR.AMOUNT_DEPOSITED2) as AMOUNT_DEPOSITED2,
			AR.DATE_OF_DEPOSIT AS DATE_OF_DEPOSIT,
			AR.DATE_OF_DEPOSIT2 AS DATE_OF_DEPOSIT2,
			AR.DEPOSIT_SLIP_NO AS SLIP_NO,
			AR.DEPOSIT_SLIP_NO2 AS SLIP_NO2,
			if(AR.STATUS IS NULL,0,AR.STATUS) AS STATUS,
			AR.REMARKS as REMARK')
	    				  ->from('admission_master AM')
	    				  ->join('admission_receipts AR', 'AM.ADMISSION_ID = AR.ADMISSION_ID')
	    				  ->join('enquiry_master EnM', 'AM.ENQUIRY_ID = EnM.ENQUIRY_ID')
	    				  ->join('employee_master EM', 'EM.EMPLOYEE_ID= AR.RECEIVED_BY ')
	    				  ->join('centre_master CM', 'CM.CENTRE_ID=AR.CENTRE_ID')
	    				  ->where('AR.ISACTIVE','1');

	    				  if($searched_data['first_name'] != '' && $searched_data['last_name'] != ''){
							$this->db->like('EnM.ENQUIRY_FIRSTNAME',$searched_data['first_name'],'after')
									->like('EnM.ENQUIRY_LASTNAME',$searched_data['last_name'],'after');
	    				  }
	    				  else{
		    				  if($searched_data['first_name'] != ''){
							$this->db->like('EnM.ENQUIRY_FIRSTNAME',$searched_data['first_name'],'after');
		    				  }
		    				  else if($searched_data['last_name'] != ''){
							$this->db->like('EnM.ENQUIRY_LASTNAME',$searched_data['last_name'],'after');
		    				  }
	    				  }

	    				  if($searched_data['payment_type'] != ''){

	    				  	$payment_type = "";
	    				  	switch ($searched_data['payment_type']) {
	    				  		case 'cash':
	    				  			$payment_type = 1;
	    				  		break;
	    				  		case 'cc':
	    				  			$payment_type = 2;
	    				  		break;
	    				  		case 'cheque':
	    				  			$payment_type = 3;
	    				  		break;
	    				  	}

	    				  	$pay_type = $searched_data['payment_type'];

	    				  	$where = "(AR.PAYMENT_TYPE = '$payment_type' or AR.PAYMENT_TYPE = '$pay_type')";

							$this->db->where($where);
	    				  }

	    				  if($searched_data['cheque_no'] != ''){
							$this->db->where('AR.CHEQUE_NO',$searched_data['cheque_no']);
    				  		}

    				  		if($searched_data['received_by'] != ''){
							$this->db->like('EM.EMP_FNAME',$searched_data['received_by'],'after');
	    				  		}

	    				  if($searched_data['receipt_from_date'] != '' && $searched_data['receipt_to_date'] != ''){
	                $date_format_change = str_replace("/","-",$searched_data['receipt_from_date']);
	                $from_date = date('Y-m-d', strtotime($date_format_change));

	                $to_date_format_change = str_replace("/","-",$searched_data['receipt_to_date']);
	                $to_date = date('Y-m-d', strtotime($to_date_format_change));

	                $where = "AR.PAYMENT_DATE BETWEEN '$from_date' AND '$to_date'";

    						$this->db->where($where);

	    				  		}
	    				  if($searched_data['centre'] != ''){
							$this->db->where('AM.CENTRE_ID',$searched_data['centre']);
	    				  }

	    				  if($searched_data['admission_from_date'] != '' && $searched_data['admission_to_date'] != ''){
	                $ad_date_format_change = str_replace("/","-",$searched_data['admission_from_date']);
	                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

	                $ad_to_date_format_change = str_replace("/","-",$searched_data['admission_to_date']);
	                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));


	                $where = "AM.ADMISSION_DATE BETWEEN '$from_date' AND '$to_date'";

    						$this->db->where($where);

	    				  		}


	    				  if($searched_data['receipt_no_from'] != '' && $searched_data['receipt_no_to'] != ''){

	    				  	$from = $searched_data['receipt_no_from'];
	    				  	$to = $searched_data['receipt_no_to'];

					$where = "AR.CENTRE_RECEIPT_ID BETWEEN '$from' AND '$to'";

    						$this->db->where($where);

	    				  		}

	    				  if($searched_data['manual_receipt_from'] != '' && $searched_data['manual_receipt_to'] != ''){

							$from = $searched_data['manual_receipt_from'];
	    				  	$to = $searched_data['manual_receipt_to'];

					$where = "AR.RECIEPT_NO BETWEEN '$from' AND '$to'";

    						$this->db->where($where);

	    				  		}

	    				  if($searched_data['deposit_date_from'] != '' && $searched_data['deposit_date_to'] != ''){

					$ad_date_format_change = str_replace("/","-",$searched_data['deposit_date_from']);
	                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

	                $ad_to_date_format_change = str_replace("/","-",$searched_data['deposit_date_to']);
	                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));


					$where = "AR.DATE_OF_DEPOSIT BETWEEN '$from_date' AND '$to_date'";

    						$this->db->where($where);

	    				  		}

	    				  if($searched_data['deposition_status'] != ''){
	    				  	if($searched_data['deposition_status'] == 'deposited'){
								$this->db->where('AR.DEPOSIT_SLIP_NO IS NOT NULL');
	    				  	}
	    				  	else if($searched_data['deposition_status'] == 'undeposited'){
								$this->db->where('AR.DEPOSIT_SLIP_NO IS NULL');
	    				  	}
	    				}
	    				//$this->db->order_by('AR.PAYMENT_DATE','desc');

	    			$query1 = $this->db->get_compiled_select();

    	$this->db->select('PRM.PENALTY_RECEIPT_ID AS ADMISSION_RECEIPTS_ID,
			PRM.PENALTY_RECEIPT_NO AS ADMISSION_RECEIPT_NO,
			PRM.CENTRE_RECEIPT_ID AS CENTRE_RECEIPT_ID,
			PRM.RECEIPT_NO AS RECEIPT_NO,
			CM1.CENTRE_NAME as CENTRE_NAME,
			CONCAT(EnM1.ENQUIRY_FIRSTNAME, " ", EnM1.ENQUIRY_LASTNAME) As STUDENT_NAME,
			AM1.ADMISSION_ID AS ADMISSION_ID,
			AM1.ADMISSION_DATE as ADMISSION_DATE,
			PRM.PAYMENT_DATE AS PAYMENT_DATE ,
			PRM.PAYMENT_TYPE AS PAYMENT_TYPE,
			if(PRM.PARTICULAR > 0,CF.CONTROLFILE_VALUE,PRM.PARTICULAR) AS PAYMENT_FOR,
			PRM.AMOUNT As AMOUNT_PAID,
			PRM.AMOUNT_WITHOUT_ST As AMOUNT_PAID_FEES,
			if(PRM.BANK_NAME IS NULL," ",PRM.BANK_NAME) as BANK,
			if(PRM.CHEQUE_NO IS NULL," ",PRM.CHEQUE_NO) as CHEQUE_NO,
			PRM.CHEQUE_DATE AS CHEQUE_DATE,
			if(PRM.BRANCH IS NULL ," ",PRM.BRANCH) as BRANCH,
			CONCAT(IFNULL(EM1.EMP_FNAME, ""), " ",IFNULL(EM1.EMP_LASTNAME, "")) AS RECEIVED_BY,
			if(PRM.AMOUNT_DEPOSITED IS NULL,0,PRM.AMOUNT_DEPOSITED) as AMOUNT_DEPOSITED,
			if(PRM.AMOUNT_DEPOSITED2 IS NULL,0,PRM.AMOUNT_DEPOSITED2) as AMOUNT_DEPOSITED2,
			PRM.DATE_OF_DEPOSIT AS DATE_OF_DEPOSIT,
			PRM.DATE_OF_DEPOSIT2 AS DATE_OF_DEPOSIT2,
			PRM.DEPOSIT_SLIP_NO AS SLIP_NO,
			PRM.DEPOSIT_SLIP_NO2 AS SLIP_NO2,
			if(PRM.STATUS IS NULL,0,PRM.STATUS) AS STATUS,
			PRM.REMARK AS REMARK')
				    				  ->from('admission_master AM1')
				    				  ->join('penalties_receipt_master PRM', 'AM1.ADMISSION_ID = PRM.ADMISSION_ID')
				    				  ->join('enquiry_master EnM1', 'AM1.ENQUIRY_ID = EnM1.ENQUIRY_ID')
				    				  ->join('employee_master EM1', 'EM1.EMPLOYEE_ID= PRM.RECEIVED_BY ')
				    				  ->join('centre_master CM1', 'CM1.CENTRE_ID=PRM.CENTRE_ID')
				    				  ->join('control_file CF', 'CF.CONTROLFILE_ID=PRM.PARTICULAR')
				    				  ->where('PRM.ISACTIVE','1');

				    				  if($searched_data['first_name'] != '' && $searched_data['last_name'] != ''){
										$this->db->like('EnM1.ENQUIRY_FIRSTNAME',$searched_data['first_name'],'after')
												->like('EnM1.ENQUIRY_LASTNAME',$searched_data['last_name'],'after');
				    				  }
				    				  else{
					    				  if($searched_data['first_name'] != ''){
										$this->db->like('EnM1.ENQUIRY_FIRSTNAME',$searched_data['first_name'],'after');
					    				  }
					    				  else if($searched_data['last_name'] != ''){
										$this->db->like('EnM1.ENQUIRY_LASTNAME',$searched_data['last_name'],'after');
					    				  }
				    				  }


				    				  if($searched_data['payment_type'] != ''){

				    				  	$payment_type = "";
				    				  	switch ($searched_data['payment_type']) {
				    				  		case 'cash':
				    				  			$payment_type = 1;
				    				  		break;
				    				  		case 'cc':
				    				  			$payment_type = 2;
				    				  		break;
				    				  		case 'cheque':
				    				  			$payment_type = 3;
				    				  		break;
				    				  	}

				    				  	$pay_type = $searched_data['payment_type'];

				    				  	$where = "(PRM.PAYMENT_TYPE = '$payment_type' or PRM.PAYMENT_TYPE = '$pay_type')";

										$this->db->where($where);

				    				  }

								if($searched_data['cheque_no'] != ''){
									$this->db->where('PRM.CHEQUE_NO',$searched_data['cheque_no']);
			    				}

			    				if($searched_data['received_by'] != ''){
									$this->db->like('EM1.EMP_FNAME',$searched_data['received_by'],'after');
	    				  		}

				    				  if($searched_data['receipt_from_date'] != '' && $searched_data['receipt_to_date'] != ''){
				                $date_format_change = str_replace("/","-",$searched_data['receipt_from_date']);
				                $from_date = date('Y-m-d', strtotime($date_format_change));

				                $to_date_format_change = str_replace("/","-",$searched_data['receipt_to_date']);
				                $to_date = date('Y-m-d', strtotime($to_date_format_change));


				                $where = "PRM.PAYMENT_DATE BETWEEN '$from_date' AND '$to_date'";

			    						$this->db->where($where);

				    				  		}
				    				  if($searched_data['centre'] != ''){
										$this->db->where('AM1.CENTRE_ID',$searched_data['centre']);
				    				  }

				    				  if($searched_data['admission_from_date'] != '' && $searched_data['admission_to_date'] != ''){
				                $ad_date_format_change = str_replace("/","-",$searched_data['admission_from_date']);
				                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

				                $ad_to_date_format_change = str_replace("/","-",$searched_data['admission_to_date']);
				                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));

				                $where = "AM1.ADMISSION_DATE BETWEEN '$from_date' AND '$to_date'";

			    						$this->db->where($where);

				    				  		}

				    				  if($searched_data['receipt_no_from'] != '' && $searched_data['receipt_no_to'] != ''){

										$from = $searched_data['receipt_no_from'];
				    				  	$to = $searched_data['receipt_no_to'];

								$where = "PRM.CENTRE_RECEIPT_ID BETWEEN '$from' AND '$to'";

			    						$this->db->where($where);

				    				  		}

				    				  if($searched_data['manual_receipt_from'] != '' && $searched_data['manual_receipt_to'] != ''){

										$from = $searched_data['manual_receipt_from'];
				    				  	$to = $searched_data['manual_receipt_to'];

								$where = "PRM.PENALTY_RECEIPT_NO BETWEEN '$from' AND '$to'";

			    						$this->db->where($where);


				    				  		}

				    				  if($searched_data['deposit_date_from'] != '' && $searched_data['deposit_date_to'] != ''){

								$ad_date_format_change = str_replace("/","-",$searched_data['deposit_date_from']);
				                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

				                $ad_to_date_format_change = str_replace("/","-",$searched_data['deposit_date_to']);
				                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));


								$where = "PRM.DATE_OF_DEPOSIT BETWEEN '$from_date' AND '$to_date'";

			    						$this->db->where($where);

				    				  		}

				    				  if($searched_data['deposition_status'] != ''){
				    				  	if($searched_data['deposition_status'] == 'deposited'){
										$this->db->where('PRM.DEPOSIT_SLIP_NO IS NOT NULL');
				    				  	}
				    				  	else if($searched_data['deposition_status'] == 'undeposited'){
										$this->db->where('PRM.DEPOSIT_SLIP_NO IS NULL');
				    				  	}
				    				  	}
				    				//$this->db->order_by('PRM.PAYMENT_DATE','desc');

    						$query2 = $this->db->get_compiled_select();

	    	$query = $this->db->query($query1." UNION ".$query2." order by PAYMENT_FOR,PAYMENT_DATE,CENTRE_RECEIPT_ID");

			  // if($count != 1){
			  // 	$query = $this->db->limit($perpage,$offset);
			  // }
				  // $query = $this->db->get();

		  	if ($count == 1) {
		  		return $query->num_rows();
		  	}
		  	else{
	  			return $query->result_array();
		  	}
    }

    public function get_receipt_edcr_report($searched_data,$count){
    				$query = $this->db->select('
			AM.ADMISSION_ID,
			AM.ADMISSION_DATE as ADMISSION_DATE,
			CONCAT(EnM.ENQUIRY_FIRSTNAME, " ", EnM.ENQUIRY_LASTNAME) As STUDENT_NAME,
			AM.COURSE_TAKEN,
			AM.MODULE_TAKEN,
			AM.TOTALFEES,
			AR.ADMISSION_RECEIPT_NO,
			AR.RECIEPT_NO,
			AR.PAYMENT_DATE,
			AR.PAYMENT_TYPE,
			AR.AMOUNT_PAID,
			AR.AMOUNT_PAID_FEES,
			AR.AMOUNT_PAID_SERVICETAX,
			AR.DATE_OF_DEPOSIT,
			0 AS HOUSE_CALLS,
			DATEDIFF(AR.PAYMENT_DATE,AR.DATE_OF_DEPOSIT) AS DEPOSITION_DELAY,
			"FEES RECEIPT" AS PARTICULAR,
			if(AR.CHEQUE_BANK_NAME IS NULL ," ",AR.CHEQUE_BANK_NAME) as BANK,
			if(AR.CHEQUE_NO IS NULL ," ",AR.CHEQUE_NO) as CHEQUE_NO,
			AR.CHEQUE_DATE AS CHEQUE_DATE,
			if(AR.CHEQUE_BRANCH IS NULL ," ",AR.CHEQUE_BRANCH) as BRANCH,
			AR.COLLECTION_MONTH,
			CM.CENTRE_ID')
	    				  ->from('admission_receipts AR')
	    				  ->join('admission_master AM', 'AM.ADMISSION_ID = AR.ADMISSION_ID',"right")
	    				  ->join('enquiry_master EnM', 'AM.ENQUIRY_ID = EnM.ENQUIRY_ID',"left")
	    				  ->join('employee_master EM', 'EM.EMPLOYEE_ID= AR.RECEIVED_BY',"right")
	    				  ->join('centre_master CM', 'CM.CENTRE_ID=AR.CENTRE_ID',"right");

	    				  if($searched_data['first_name'] != '' && $searched_data['last_name'] != ''){
					$query = $this->db->like('EnM.ENQUIRY_FIRSTNAME',$searched_data['first_name'],'after')
									->like('EnM.ENQUIRY_LASTNAME',$searched_data['last_name'],'after');
	    				  }
	    				  else{
		    				  if($searched_data['first_name'] != ''){
						$query = $this->db->like('EnM.ENQUIRY_FIRSTNAME',$searched_data['first_name'],'after');
		    				  }
		    				  else if($searched_data['last_name'] != ''){
						$query = $this->db->like('EnM.ENQUIRY_LASTNAME',$searched_data['last_name'],'after');
		    				  }
	    				  }

	    				  if($searched_data['receipt_from_date'] != '' && $searched_data['receipt_to_date'] != ''){
	                $date_format_change = str_replace("/","-",$searched_data['receipt_from_date']);
	                $from_date = date('Y-m-d', strtotime($date_format_change));

	                $to_date_format_change = str_replace("/","-",$searched_data['receipt_to_date']);
	                $to_date = date('Y-m-d', strtotime($to_date_format_change));

                 				$where = "AR.PAYMENT_DATE BETWEEN '$from_date' AND '$to_date'";

                				$query = $this->db->where($where);

	    				  		}

						// current user role ids
						    $role_ids = array('35','36','37','38','39','40');

						    if (in_array($this->session->userdata('admin_data')[0]['ROLE_ID'], $role_ids)){
						      $query = $this->db->where('AM.CENTRE_ID',$this->session->userdata('admin_data')[0]['CENTRE_ID']);
						    }
						    else{
						      if ($searched_data['centre'] != '') {
						        $query = $this->db->where('AM.CENTRE_ID',$searched_data['centre']);
						      }
						    }



	    // 				  if($searched_data['centre'] != ''){
					// $query = $this->db->where('AM.CENTRE_ID',$searched_data['centre']);
	    // 				  }

	    // 				  if($searched_data['payment_for'] != ''){
					// $query = $this->db->where('AM.CENTRE_ID',$searched_data['centre']);
	    // 				  }

	    // 				  if($searched_data['collection_month'] != ''){
					// $query = $this->db->where('AR.COLLECTION_MONTH',$searched_data['collection_month']);
	    // 				  }

	    				  if($searched_data['admission_from_date'] != '' && $searched_data['admission_to_date'] != ''){
	                $ad_date_format_change = str_replace("/","-",$searched_data['admission_from_date']);
	                $from_date = date('Y-m-d', strtotime($ad_date_format_change));

	                $ad_to_date_format_change = str_replace("/","-",$searched_data['admission_to_date']);
	                $to_date = date('Y-m-d', strtotime($ad_to_date_format_change));


								$where = "AM.ADMISSION_DATE BETWEEN '$from_date' AND '$to_date'";

                				$query = $this->db->where($where);

	    				  		}

	    				  if($searched_data['deposit_date_from'] != '' && $searched_data['deposit_date_to'] != ''){
					$date_format_change = str_replace("/","-",$searched_data['deposit_date_from']);
	                $from_date = date('Y-m-d', strtotime($date_format_change));

	                $to_date_format_change = str_replace("/","-",$searched_data['deposit_date_to']);
	                $to_date = date('Y-m-d', strtotime($to_date_format_change));

                 				$where = "AR.DATE_OF_DEPOSIT BETWEEN '$from_date' AND '$to_date'";

                				$query = $this->db->where($where);

	    				  		}

	    				  $query = $this->db->order_by("AR.PAYMENT_DATE,AR.CENTRE_RECEIPT_ID")
	    				  					->where('AR.ISACTIVE',"1");

	    // 	if($count != 1){
			  // 	//$query = $this->db->limit($perpage,$offset);
			  // }
				  $query = $this->db->get();



		    // echo "<pre>";
      //       print_r($query->result_array());
      //       echo "</pre>";
      //       exit();


		  	if ($count == 1) {
		  		return $query->num_rows();
		  	}
		  	else{
	  			return $query->result_array();
		  	}
    }

    public function get_enquiries_count_for_receipt_edcr($payment_date,$centre_id){
    		    $query = $this->db->select('COUNT(ENQUIRY_ID) as enquiry_count')
	    				  ->from('enquiry_master')
	    				  ->where('ENQUIRY_DATE',$payment_date)
	    				  ->where('CENTRE_ID',$centre_id)
	  					  ->get();
	  	return $query->row()->enquiry_count;
    }

    public function get_admission_count_for_receipt_edcr($payment_date,$centre_id){
	    $query = $this->db->select('COUNT(ADMISSION_ID) as admission_count')
	    				  ->from('admission_master')
	    				  ->where('ADMISSION_DATE',$payment_date)
	    				  ->where('CENTRE_ID',$centre_id)
	  					  ->get();
	  	return $query->row()->admission_count;
    }

    public function get_course_and_module_names_by_id_array($course_or_module_id_Array,$type){
      $name = "";
      foreach($course_or_module_id_Array as $course_or_module_id){
        if($type == "course"){
          $query = $this->db->select('course_name as cname')
                    ->from('course_master')
                    ->where('course_id',$course_or_module_id)
                    ->get();
        }
        else{
          $query = $this->db->select('module_name as cname')
                    ->from('module_master')
                    ->where('module_id',$course_or_module_id)
                    ->get();
        }
        $name .= ','.$query->row()->cname;
      }

      $name = trim($name,",");

	  	return $name;
    }

}
