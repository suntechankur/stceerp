<br/>
<div class="col-md-12 col-sm-12">
<div class="create_batch_form">
		<div id="box">
			<h2>Admission other receipt</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-12">
						<?php if($this->session->flashdata('flashMsg')){
									 if($this->session->flashdata('flashMsg') == "1"){
									 	echo "<label id='flashmsg' style='color:red'>* Please fill all required details.</label>";
									 }?>
									 <script>
									 	setTimeout(function(){ $("#flashmsg").hide(); }, 3000);
									 </script>
							<?php } ?>
							<div class="form-group">
								<?php echo form_open() ?>
								<div class="row" style="margin-left:-30px;">
								<?php
								foreach ($receipt_details_for_edit as $edit_receipt) {
								?>
								<input type="hidden" name="receipt_id" value="<?php echo $edit_receipt['receipt_id']; ?>">
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>Admission Id : </label><input type="text" class="form-control" value="<?php echo $edit_receipt['ADMISSION_ID']; ?>" readonly="true" name="admission_id">
										</div>
										<div class="col-md-4">
											<label>Student Name : </label><input type="text" class="form-control" name="student_name" value="<?php echo $edit_receipt['STUDENT_NAME']; ?>" readonly="true">
										</div>
										<div class="col-md-4">
											<label>Centre : </label>
											<select name="centre" class="form-control">
                                                <option value="">Please Select</option>
                                                <?php
                                                	foreach($centres as $centre)
                                                		{?>
                                                		<option value="<?php echo $centre['CENTRE_ID'] ?>"
														<?php if(isset($edit_receipt['CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $edit_receipt['CENTRE_ID']) ? ' selected="selected"' : '';}?>
                                                		><?php echo $centre['CENTRE_NAME'] ?></option>
                                                	<?php }
                                                ?>
                                            </select>
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>Admission Date : </label><input type="text" class="form-control" value="<?php echo date("d/m/Y", strtotime($edit_receipt['ADMISSION_DATE'])); ?>" name="admission_date" readonly="true">
										</div>
										<div class="col-md-4">
											<label>Course Taken : </label><input type="text" class="form-control" value="<?php echo $edit_receipt['COURSE_TAKEN']; ?>" name="course_taken" readonly="true">
										</div>
										<div class="col-md-4">
											<label>Course fees : </label><input type="text" class="form-control" value="<?php echo $edit_receipt['TOTALFEES']; ?>" name="course_fees" readonly="true">
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>Amount paid with GST : <span style="color:red;">*</span></label><input type="text" class="form-control amount_with_gst" value="<?php echo $edit_receipt['AMOUNT_PAID']; ?>" name="amount_with_gst">
										</div>
										<div class="col-md-4">
											<label>Amount paid without GST : </label><input type="text" class="form-control amount_without_gst" value="<?php echo $edit_receipt['AMOUNT_PAID_FEES']; ?>" name="amount_without_gst" readonly="true">
										</div>
										<div class="col-md-4">
											<label>GST paid : </label><input type="text" class="form-control gst_paid" name="gst_paid" value="<?php echo $edit_receipt['AMOUNT_PAID_SERVICETAX']; ?>" readonly="true">
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>Payment type : </label>
											<select name="payment_type" class="form-control">
													<option value="">Please Select</option>
													<option <?php if(isset($edit_receipt['PAYMENT_TYPE'])){ echo ($edit_receipt['PAYMENT_TYPE'] == "Cash") ? ' selected="selected"' : ''; echo ($edit_receipt['PAYMENT_TYPE'] == "1") ? ' selected="selected"' : '';}?> value="1">Cash</option>
													<option <?php if(isset($edit_receipt['PAYMENT_TYPE'])){ echo ($edit_receipt['PAYMENT_TYPE'] == "Cheque") ? ' selected="selected"' : ''; echo ($edit_receipt['PAYMENT_TYPE'] == "3") ? ' selected="selected"' : '';}?> value="3">Cheque</option>
													<option <?php if(isset($edit_receipt['PAYMENT_TYPE'])){ echo ($edit_receipt['PAYMENT_TYPE'] == "CC") ? ' selected="selected"' : ''; echo ($edit_receipt['PAYMENT_TYPE'] == "2") ? ' selected="selected"' : '';}?> value="2">CC</option>
											</select>
										</div>
										<div class="col-md-4">
											<label>Payment date : </label>
											<input type="text" class="form-control" name="payment_date" value="<?php echo date("d/m/Y", strtotime($edit_receipt['PAYMENT_DATE']));?>" readonly="true">
										</div>
										<div class="col-md-4">
											<label>Cheque date : </label>
											<input type="text" class="form-control enquiry_date" name="cheque_date" value="<?php echo date("d/m/Y", strtotime($edit_receipt['CHEQUE_DATE']));?>" >
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>Cheque/DD/TID No : </label>
											<input type="text" class="form-control" name="cheque_no" value="<?php echo $edit_receipt['CHEQUE_NO']; ?>">
										</div>
										<div class="col-md-4">
											<label>Bank Name : </label>
											<input type="text" class="form-control" name="bank_name" value="<?php echo $edit_receipt['BANK']; ?>" >
										</div>
										<div class="col-md-4">
											<label>Bank Branch : </label>
											<input type="text" class="form-control" name="bank_branch" value="<?php echo $edit_receipt['BRANCH']; ?>" >
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>Received By : <span style="color:red;">*</span></label>
											<select name="received_by" class="form-control">
											<option value="">Please select</option>
											<?php
                                                	foreach($received_by as $received)
                                                		{?>
                                                		<option value="<?php echo $received['EMPLOYEE_ID'] ?>"
														<?php if(isset($edit_receipt['RECEIVED_BY'])){ echo ($received['EMPLOYEE_ID'] == $edit_receipt['RECEIVED_BY']) ? ' selected="selected"' : '';

															echo ($received['ISACTIVE'] == 0) ? ' style="background:#DCDCDC;" disabled="disabled"' : '';
														}?>
                                                		><?php echo $received['EMP_NAME'] ?></option>
                                                	<?php }
                                                ?>
											</select>
										</div>
										<div class="col-md-4">
											<label>Token/Receipt No (Manual Receipts) : </label>
											<input type="text" class="form-control" name="token_no">
										</div>
										<div class="col-md-4">
											<label>Payment For : </label>
											<select name="particular" class="form-control">
											<option value="">Please select</option>
											<?php
                                                	foreach($payFor as $pay)
                                                		{?>
                                                		<option value="<?php echo $pay['CONTROLFILE_ID'] ?>"
														<?php if(isset($edit_receipt['PARTICULAR'])){ echo ($pay['CONTROLFILE_ID'] == $edit_receipt['PARTICULAR']) ? ' selected="selected"' : '';
														}?>
                                                		><?php echo  $pay['CONTROLFILE_VALUE'] ?></option>
                                                	<?php }
                                                ?>
											</select>
										</div>
									</div>
									<?php if($this->session->flashdata('flashMsgRemarks')){?>
											 <div class="col-lg-12" style="margin-left:0px;">
												<div class="col-md-12">
													<label>Update/Cancel Remarks : <span style="color:red;">*</span></label><input type="text" class="form-control" name="remarks" value="<?php echo $this->session->flashdata('flashMsgRemarks'); ?>">
												</div>
											</div>
									<?php }
									else{?>
											<div class="col-lg-12" style="margin-left:0px;">
												<div class="col-md-12">
													<label>Update/Cancel Remarks : <span style="color:red;">*</span></label><input type="text" class="form-control" name="remarks">
												</div>
											</div>
									<?php } ?>
									<div class="col-lg-12" style="margin-left:0px;">
										<br/>
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary" name="<?php echo $edit_receipt['type']; ?>_search_other_receipts"><?php echo $edit_receipt['type']; ?></button>
										</div>
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary" name="reset">Cancel</button>
										</div>
									</div>
									<?php } ?>
								</div>
								<?php echo form_close();?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br/>
<div class="create_batch_form">
<div id="box"><h2></h2></div>
<div class="row">
<div class="col-sm-12">
<div class="panel panel-default">
<!--<div class="panel-heading">Form Elements</div>-->
<div class="panel-body table-responsive">
<table class="table table-bordered table-condensed table-stripped">
<thead>
<tr>
	<th>Receipt No</th>
	<th>Receipt Date</th>
	<th>Particular</th>
	<th>Amount With GST</th>
	<th>Amount W/o GST</th>
	<th>Payment type</th>
	<th>Receipt</th>
</tr>
</thead>
<tbody>
<?php foreach($get_receipt_details_by_admission_id as $receipts){?>
<tr>
	<td><?php echo $receipts['ADMISSION_RECEIPT_NO'];?></td>
	<td><?php echo date("d/m/Y", strtotime($receipts['PAYMENT_DATE']));?></td>
	<td>
	<?php
	if(is_numeric($receipts['PARTICULAR'])){

		foreach($payFor as $pay)
		{
			if($receipts['PARTICULAR'] == $pay['CONTROLFILE_ID']){
				echo $pay['CONTROLFILE_VALUE'];
			}
		}

	}
	else{
		 echo $receipts['PARTICULAR'];
	}
	?>
	</td>
	<td><?php echo $receipts['AMOUNT_PAID'];?></td>
	<td><?php echo $receipts['AMOUNT_PAID_FEES'];?></td>
	<td>
	<?php
	if(is_numeric($receipts['PAYMENT_TYPE'])){
		$payment_type = "";
		switch($receipts['PAYMENT_TYPE']){
			case "1":
				$payment_type = "CASH";
				break;
			case "2":
				$payment_type = "CC";
				break;
			case "3":
				$payment_type = "CHEQUE";
				break;
		}
		echo $payment_type;
	}
	else{
		echo $receipts['PAYMENT_TYPE'];
	}
	?>
	</td>
	<td><a href="<?php echo base_url('accounts/print-other-receipts/'.$this->encrypt->encode($receipts['ADMISSION_RECEIPTS_ID']).''); ?>" title="Print Other Receipt">Print</a></td>
</tr>
<?php } ?>
</tbody>
</table>
</div>

</div>
</div></div></div>
</div>
