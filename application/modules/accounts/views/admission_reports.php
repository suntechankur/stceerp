<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Admission Reports</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<?php echo form_open(); ?>
							<div class="col-md-12">
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>First Name:</label>
												<input class="form-control" value="<?php if(isset($search_data['ENQUIRY_FIRSTNAME'])){ echo $search_data['ENQUIRY_FIRSTNAME'];} ?>" maxlength="50" name="ENQUIRY_FIRSTNAME" id="ENQUIRY_FIRSTNAME" type="text">
											</div>
											<div class="col-lg-4">
												<label>Last Name:</label>
												<input class="form-control" value="<?php if(isset($search_data['ENQUIRY_LASTNAME'])){ echo $search_data['ENQUIRY_LASTNAME'];} ?>" name="ENQUIRY_LASTNAME" id="ENQUIRY_LASTNAME" maxlength="50" type="text">
											</div>
											<div class="col-lg-4">
												<label>Centre:</label>
												<select name="CENTRE_ID" class="form-control">
													<option value="">Please Select Centre</option>
													<?php foreach ($centres as $centre) { ?>
														<option value="<?php echo $centre['CENTRE_ID']; ?>" <?php if(isset($search_data['CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $search_data['CENTRE_ID']) ? ' selected="selected"' : '';}?>><?php echo $centre['CENTRE_NAME']; ?> </option>	
													<?php } ?>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Handled By:</label>
												<select name="HANDLED_BY" class="form-control">
													<option value="">Admission Handled By</option>
													<?php foreach ($received_by as $handled_by) { ?>
														echo 
														<option value="<?php echo $handled_by['EMPLOYEE_ID']; ?>" <?php if(isset($admission_filter_data['EMPLOYEE_ID'])){ echo ($handled_by['EMPLOYEE_ID'] == $search_data['HANDLED_BY']) ? ' selected="selected"' : '';}?>><?php echo $handled_by['EMP_NAME']; ?> </option>	
													<?php } ?>
												</select>
											</div>
											<div class="col-lg-4">
												<label>From Admission Date:</label>
												<input name="ADMISSION_DATE_FROM" id="ADMISSION_DATE_FROM" value="<?php if(isset($search_data['ADMISSION_DATE_FROM'])){ echo $search_data['ADMISSION_DATE_FROM'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>To Admission Date:</label>
												<input name="ADMISSION_DATE_TO" id="ADMISSION_DATE_TO" value="<?php if(isset($search_data['ADMISSION_DATE_TO'])){ echo $search_data['ADMISSION_DATE_TO'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Admission Date:</label>
												<input name="ADMISSION_DATE" id="ADMISSION_DATE" value="<?php if(isset($search_data['ADMISSION_DATE'])){ echo $search_data['ADMISSION_DATE'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-md-4">
											<br>
												<button type="submit" class="btn btn-primary" name="search_admission_data">Search</button>
											</div>
											<div class="col-md-4">
											<br>
												<button type="submit" name="search_admission_reset" class="btn btn-primary">Reset Filter</button>

											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
						 <?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		<?php if($this->session->userdata('admission_reports')){?>
		<div id="box"> 
			<p>&nbsp;</p>
			<h2 class="text-center"> Admission List</h2>
        </div>
		<div class="row">
		<div class="col-sm-12">
	        <div class="panel panel-default">
	        	<div class="panel-body table-responsive">
	        		<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Admission Id</th>
								<th>Student Name</th>
								<th>Admission Date</th>
								<th>Course Taken</th>
								<th>Fees</th>
								<th>Fees Paid</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($admission_report['receipt_data'] as $adm_repo){?>
							<tr>
								<td><?php echo $adm_repo['ADMISSION_ID'];?></td>
								<td><?php echo $adm_repo['STUDENT_NAME'];?></td>
								<td><?php $admDateRep = str_replace('-', '/', $adm_repo['ADMISSION_DATE']);
	            				$date =  date("d/m/Y", strtotime($admDateRep) );
								echo $date;?></td>
								<td><?php echo $adm_repo['COURSE_TAKEN'];?></td>
								<td><?php echo $adm_repo['TOTALFEES'];?></td>
								<td><?php echo $adm_repo['FEES_PAID'];?></td>
							</tr>
						<?php }?>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td><strong>Total</strong></td>
								<td><strong><?php echo $total_fees;?></strong></td>
								<td><strong><?php echo $total_fees_paid;?></strong></td>
							</tr>
						</tbody>
	        		</table>
	        		<?php echo $admission_report['pagination']; ?>
	        	</div>
			</div>
		</div>
	</div>
	<?php }?>
</div>
</div>