<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>All Receipt Reports</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<?php echo form_open(); ?>
							<div class="col-md-12">
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-12">
												<input type="checkbox" class="form-control" name="option" <?php if(isset($all_receipts_report['option'])){ if($all_receipts_report['option'] == "on"){echo "checked='true'";}} ?>><label>Other Receipt Data</label>
											</div>
										</div>
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>First Name:</label>
												<input name="first_name" id="FIRST_NAME" value="<?php if(isset($all_receipts_report['first_name'])){ echo $all_receipts_report['first_name'];} ?>" class="form-control" type="text">
											</div>
											<div class="col-lg-4">
												<label>Last Name:</label>
												<input name="last_name" id="LAST_NAME" value="<?php if(isset($all_receipts_report['last_name'])){ echo $all_receipts_report['last_name'];} ?>" class="form-control" type="text">
											</div>
											<div class="col-lg-4">
												<label>Centre:</label>
												<select name="centre" class="form-control">
													<option value="">Please Select Centre</option>
													<?php
                                                	foreach($centres as $centre)
                                                		{?>
                                                		<option value="<?php echo $centre['CENTRE_ID'] ?>"
														<?php if(isset($all_receipts_report['centre'])){ echo ($centre['CENTRE_ID'] == $all_receipts_report['centre']) ? ' selected="selected"' : '';}?>
                                                		><?php echo $centre['CENTRE_NAME'] ?></option>
                                                	<?php }
                                                ?>
												</select>
											</div>
										</div>
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Receipt Date From:</label>
												<input name="receipt_from_date" id="RECEIPT_DATE_FROM" value="<?php if(isset($all_receipts_report['receipt_from_date'])){ echo $all_receipts_report['receipt_from_date'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>Receipt Date To:</label>
												<input name="receipt_to_date" id="RECEIPT_DATE_TO" value="<?php if(isset($all_receipts_report['receipt_to_date'])){ echo $all_receipts_report['receipt_to_date'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>Payment Type:</label>
												<select name="payment_type" class="form-control">
													<option value="">All</option>
													<option value="cash" <?php if(isset($all_receipts_report['payment_type'])){
													if($all_receipts_report['payment_type'] == "cash"){echo "selected='selected'";} } ?>>Cash</option>
													<option value="cheque" <?php if(isset($all_receipts_report['payment_type'])){
													if($all_receipts_report['payment_type'] == "cheque"){echo "selected='selected'";} } ?>>Cheque</option>
													<option value="cc" <?php if(isset($all_receipts_report['payment_type'])){
													if($all_receipts_report['payment_type'] == "cc"){echo "selected='selected'";} } ?>>CC</option>
												</select>
											</div>
										</div>
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Admission Date From:</label>
												<input name="admission_from_date" id="ADMISSION_DATE_FROM" value="<?php if(isset($all_receipts_report['admission_from_date'])){ echo $all_receipts_report['admission_from_date'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>Admission Date To:</label>
												<input name="admission_to_date" id="ADMISSION_DATE_TO" value="<?php if(isset($all_receipts_report['admission_to_date'])){ echo $all_receipts_report['admission_to_date'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>Received By:</label>
												<input type="text" class="form-control" name="received_by" value="<?php if(isset($all_receipts_report['received_by'])){ echo $all_receipts_report['received_by'];} ?>">
											</div>
										</div>
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Receipt No From:</label>
												<input name="receipt_no_from" id="RECEIPT_NO_FROM" value="<?php if(isset($all_receipts_report['receipt_no_from'])){ echo $all_receipts_report['receipt_no_from'];} ?>" class="form-control" type="text">
											</div>
											<div class="col-lg-4">
												<label>Receipt No To:</label>
												<input name="receipt_no_to" id="RECEIPT_NO_TO" value="<?php if(isset($all_receipts_report['receipt_no_to'])){ echo $all_receipts_report['receipt_no_to'];} ?>" class="form-control" type="text">
											</div>
											<div class="col-lg-4">
											<br>
												<label>Note: Enter 1001 for receipt no C-215-1001</label>
											</div>
										</div>
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Manual Receipts From:</label>
												<input name="manual_receipt_from" id="MANUAL_RECEIPTS_FROM" value="<?php if(isset($all_receipts_report['manual_receipt_from'])){ echo $all_receipts_report['manual_receipt_from'];} ?>" class="form-control" type="text">
											</div>
											<div class="col-lg-4">
												<label>Manual Receipts To:</label>
												<input name="manual_receipt_to" id="MANUAL_RECEIPTS_TO" value="<?php if(isset($all_receipts_report['manual_receipt_to'])){ echo $all_receipts_report['manual_receipt_to'];} ?>" class="form-control" type="text">
											</div>
											<div class="col-lg-4">
												<label>Deposition Status:</label>
												<select class="form-control" name="deposition_status">
													<option value="all" <?php if(isset($all_receipts_report['deposition_status'])){
													if($all_receipts_report['deposition_status'] == "all"){echo "selected='selected'";} } ?>>All</option>
													<option value="deposited" <?php if(isset($all_receipts_report['deposition_status'])){
													if($all_receipts_report['deposition_status'] == "deposited"){echo "selected='selected'";} } ?>>Deposited</option>
													<option value="undeposited" <?php if(isset($all_receipts_report['deposition_status'])){
													if($all_receipts_report['deposition_status'] == "undeposited"){echo "selected='selected'";} } ?>>Undeposited</option>
												</select>
											</div>
										</div>
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Deposit Date From:</label>
												<input name="deposit_date_from" id="DEPOSIT_DATE_FROM" value="<?php if(isset($all_receipts_report['deposit_date_from'])){ echo $all_receipts_report['deposit_date_from'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>Deposit Date To:</label>
												<input name="deposit_date_to" id="DEPOSIT_DATE_TO" value="<?php if(isset($all_receipts_report['deposit_date_to'])){ echo $all_receipts_report['deposit_date_to'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>Cheque No:</label>
												<input type="text" class="form-control" name="cheque_no" value="<?php if(isset($all_receipts_report['cheque_no'])){ echo $all_receipts_report['cheque_no'];} ?>">
											</div>
										</div>
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Deposit Slip No From:</label>
												<input name="deposit_slip_from" id="RECEIPT_DATE_FROM" value="<?php if(isset($all_receipts_report['deposit_slip_from'])){ echo $all_receipts_report['deposit_slip_from'];} ?>" class="form-control" type="text">
											</div>
											<div class="col-lg-4">
												<label>Deposit Slip No To:</label>
												<input name="deposit_slip_to" id="RECEIPT_DATE_TO" value="<?php if(isset($all_receipts_report['deposit_slip_to'])){ echo $all_receipts_report['deposit_slip_to'];} ?>" class="form-control" type="text">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">

											<div class="col-md-4">
											<br>
												<button type="submit" class="btn btn-primary" name="all_receipt_search">Search</button>
											</div>
											<div class="col-md-4">
											<br>
												<button type="submit" name="all_receipt_reset" class="btn btn-primary">Reset Filter</button>

											</div>
											<div class="col-lg-4">
											<?php if (!empty($this->session->userdata('all_receipts_report'))) { ?>
											<a onClick ="$('#all_receipt_report').tableExport({type:'excel',escape:'false'});" class="btn" style="float:left" title="Export to excel"><i class="fa fa-file-excel-o fa-2x text-success"></i></a>

											<?php } ?>
										</div>
										</div>
									</div>
								</div>
								</div>
							</div>
						 <?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		<?php if($this->session->userdata('all_receipts_report')){?>
		<br/>
		<div id="box"><h2></h2></div>
		<div class="row">
		<div class="col-sm-12">
	        <div class="panel panel-default">
	        	<div class="panel-body table-responsive">
	        		<table class="table table-striped table-hover" id="all_receipt_report">
						<thead>
							<tr>
								<th>Receipt No</th>
								<th>Manual Token/Receipt No</th>
								<th>Receipt Date</th>
								<th>Payment Type</th>
								<th>Payment For</th>
								<th>Admission Id</th>
								<th>Student Name</th>
								<th>Amount With GST</th>
								<th>Amount W/o GST</th>
								<th>Amount Deposited</th>
								<th>Date Of Deposition</th>
								<th>Slip No</th>
								<th>Balance To Be Deposited</th>
								<th>Cheque No</th>
								<th>Bank</th>
								<th>Bank Branch</th>
								<th>Cheque Date</th>
								<th>Received By</th>
								<th>Remarks</th>
								<th>Is Cleared</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
						foreach($all_receipts_search['receipt_data'] as $rec_report){?>
							<tr>
								<td><?php echo $rec_report['ADMISSION_RECEIPT_NO'];?></td>
								<td><?php echo $rec_report['RECEIPT_NO'];?></td>
								<td><?php $admDateRep = str_replace('-', '/', $rec_report['PAYMENT_DATE']);
	            				$date =  date("d/m/Y", strtotime($admDateRep) );
								echo $date;?></td>
								<td><?php
								$paymentType = "";
								switch($rec_report['PAYMENT_TYPE']){
			                        case '1':
			                           $paymentType = 'CASH';
			                        break;
			                        case '2':
			                            $paymentType = 'CC';
			                        break;
			                        case '3':
			                            $paymentType = 'CHEQUE';
			                        break;
			                        default:
			                            $paymentType = $rec_report['PAYMENT_TYPE'];
			                    }
									echo $paymentType;
									?></td>
								<td><?php echo $rec_report['PAYMENT_FOR'];?></td>
								<td><?php echo $rec_report['ADMISSION_ID'];?></td>
								<td><?php echo $rec_report['STUDENT_NAME'];?></td>
								<td><?php echo $rec_report['AMOUNT_PAID'];?></td>
								<td><?php echo $rec_report['AMOUNT_PAID_FEES'];?></td>
								<td><?php echo $rec_report['AMOUNT_DEPOSITED'];?></td>
								<td><?php
								if(strtotime($rec_report['DATE_OF_DEPOSIT']) > 0){
									$admDateRep = str_replace('-', '/', $rec_report['DATE_OF_DEPOSIT']);
	            					$date =  date("d/m/Y", strtotime($admDateRep) );
									echo $date;
								}
								?></td>
								<td><?php echo $rec_report['SLIP_NO'];?></td>
								<td><?php echo $rec_report['balance_to_be_deposit'];?></td>
								<td><?php echo $rec_report['CHEQUE_NO'];?></td>
								<td><?php echo $rec_report['BANK'];?></td>
								<td><?php echo $rec_report['BRANCH'];?></td>
								<td><?php
								if(strtotime($rec_report['CHEQUE_DATE']) > 0){
									$admDateRep = str_replace('-', '/', $rec_report['CHEQUE_DATE']);
	            					$date =  date("d/m/Y", strtotime($admDateRep) );
									echo $date;
								}?></td>
								<td><?php echo $rec_report['RECEIVED_BY'];?></td>
								<td><input type="text" disabled="true" value="<?php echo $rec_report['REMARK']; ?>"></td>
								<td><?php if($rec_report['STATUS'] == 1){?>
										<input type="checkbox" checked="true"></td>
									<?php } else {?>
										<input type="checkbox"></td>
									<?php }?>
								<td>
									<a href="<?php echo base_url('accounts/print-receipts/'.$this->encrypt->encode($rec_report['ADMISSION_RECEIPTS_ID']).''); ?>" title="Print Receipt"><span class="glyphicon glyphicon-print"></span></a>
									<a href="<?php echo base_url('accounts/print-summary/'.$this->encrypt->encode($rec_report['ADMISSION_RECEIPTS_ID']).'/'.$this->encrypt->encode($rec_report['ADMISSION_ID']).''); ?>" title="Print Summary"><span class="glyphicon glyphicon-print" style="color:red;"></span></a>
								</td>
							</tr>
						<?php }?>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>Total CASH</td>
							<td><?php echo $total_paid_cash_amount['amt_gst'];?></td>
							<td><?php echo $total_paid_cash_amount['amt_w/o_gst'];?></td>
							<td><?php echo $total_paid_cash_amount['amt_deposited'];?></td>
							<td></td>
							<td></td>
							<td><?php echo $total_paid_cash_amount['balance_amt_deposited'];?></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>Total CHEQUE</td>
							<td><?php echo $total_paid_check_amount['amt_gst'];?></td>
							<td><?php echo $total_paid_check_amount['amt_w/o_gst'];?></td>
							<td><?php echo $total_paid_check_amount['amt_deposited'];?></td>
							<td></td>
							<td></td>
							<td><?php echo $total_paid_check_amount['balance_amt_deposited'];?></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>Total CC</td>
							<td><?php echo $total_paid_cc_amount['amt_gst'];?></td>
							<td><?php echo $total_paid_cc_amount['amt_w/o_gst'];?></td>
							<td><?php echo $total_paid_cc_amount['amt_deposited'];?></td>
							<td></td>
							<td></td>
							<td><?php echo $total_paid_cc_amount['balance_amt_deposited'];?></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>Grand Total</td>
							<td><?php echo $grand_total['amt_gst'];?></td>
							<td><?php echo $grand_total['amt_w/o_gst'];?></td>
							<td><?php echo $grand_total['amt_deposited'];?></td>
							<td></td>
							<td></td>
							<td><?php echo $grand_total['balance_amt_deposited'];?></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						</tbody>
	        		</table>
	        	</div>
			</div>
		</div>
	</div>
	<?php }?>
</div>
</div>
