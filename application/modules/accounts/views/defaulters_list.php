<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Defaulters List</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<?php echo form_open(); ?>
							<div class="col-md-12">
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Admission Date From:</label>
												<input name="ADMISSION_DATE_FROM" id="ADMISSION_DATE_FROM" value="" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>Admission Date To:</label>
												<input name="ADMISSION_DATE_TO" id="ADMISSION_DATE_TO" value="" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-md-4">
												<label>Centre : </label>
												<select name="CENTRE" class="form-control">
	                                                <option value="">Please Select</option>
	                                                <?php
	                                                	foreach($centres as $centre)
	                                                		{?>
	                                                		<option value="<?php echo $centre['CENTRE_ID'] ?>" 
															<?php if(isset($receipt_filter['centre'])){ echo ($centre['CENTRE_ID'] == $receipt_filter['centre']) ? ' selected="selected"' : '';}?>
	                                                		><?php echo $centre['CENTRE_NAME'] ?></option>
	                                                	<?php }
	                                                ?>
	                                            </select>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Payment Date From:</label>
												<input name="PAYMENT_DATE_FROM" id="PAYMENT_DATE_FROM" value="" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>Payment Date To:</label>
												<input name="PAYMENT_DATE_TO" id="PAYMENT_DATE_TO" value="" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-md-4">
												<label>Status : </label>
												<select name="STATUS" class="form-control">
	                                                <option value="">Please Select</option>
	                                                <option value="Hold">Hold</option>
	                                                <option value="Break">Break</option>
	                                                <option value="Persuing" selected="selected">Persuing</option>
	                                                <option value="Completed">Completed</option>
	                                                <option value="Left">Left</option>
	                                            </select>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
											</div>
											<div class="col-lg-4">
											<br>
												<button type="submit" class="btn btn-primary" name="generate_defaulters_list">Genarate</button>
											</div>
											<div class="col-md-4">
											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
						 <?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		<?php 

		if(isset($dfl)){

			?>
		<div id="box"> 
			<p>&nbsp;</p>
			<h2 class="text-center"> Admission List</h2>
        </div>
		<div class="row">
		<div class="col-sm-12">
	        <div class="panel panel-default">
	        	<div class="panel-body table-responsive">
	        		<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Admission Id</th>
								<th>Student Name</th>
								<th>Admission Date</th>
								<th>Course Taken</th>
								<th>Fees</th>
								<!-- <th>Fees Paid</th>
								<th>To be paid</th> -->
							</tr>
						</thead>
						<tbody>
						<?php 
						

						foreach($dfl as $key =>$adm_repo){
						
							?>
							<tr>
								<td><?php echo $adm_repo['ADMISSION_ID'];?></td>
								<td><?php echo $adm_repo['STUDENT_NAME'];?></td>
								<td><?php $admDateRep = str_replace('-', '/', $adm_repo['ADMISSION_DATE']);
	            				$date =  date("d/m/Y", strtotime($admDateRep) );
								echo $date;?></td>
								<td><?php echo $adm_repo['COURSE_TAKEN'];?></td>
								<td><?php echo $adm_repo['TOTALFEES'];?></td>
								<!-- <td><?php //echo $adm_repo[0]['totAmtPaid'];?></td>
								<td><?php //echo $adm_repo[0]['TotalamtTobePaid'];?></td> -->
							</tr>
						<?php }?>
							
						</tbody>
	        		</table><?php echo $pagination; ?>
	        		<?php //echo $admission_report['pagination']; ?>
	        	</div>
			</div>
		</div>
	</div>
	<?php }?>
</div>
</div>