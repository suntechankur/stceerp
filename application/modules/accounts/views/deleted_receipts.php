<br/>
<div class="col-md-12 col-sm-12">
<div class="create_batch_form">
		<div id="box">
			<h2>Deleted Receipts</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-12">
							<div class="form-group">
								<?php echo form_open() ?>
								<div class="row" style="margin-left:-30px;">
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-3">
											<label>Receipt From Date : </label><input type="text" class="form-control enquiry_date" value="<?php if(isset($deleted_receipt_filter['receipt_from_date'])){ echo $deleted_receipt_filter['receipt_from_date'];} ?>" name="receipt_from_date">
										</div>
										<div class="col-md-3">
											<label>Receipt To Date : </label><input type="text" class="form-control enquiry_date" value="<?php if(isset($deleted_receipt_filter['receipt_to_date'])){ echo $deleted_receipt_filter['receipt_to_date'];} ?>" name="receipt_to_date">
										</div>
										<div class="col-md-3">
											<label>Centre : </label>
											<select name="centre" class="form-control">
                                                <option value="">Please Select</option>
                                                <?php
                                                	foreach($centres as $centre)
                                                		{?>
                                                		<option value="<?php echo $centre['CENTRE_ID'] ?>"
														<?php if(isset($deleted_receipt_filter['centre'])){ echo ($centre['CENTRE_ID'] == $deleted_receipt_filter['centre']) ? ' selected="selected"' : '';}?>
                                                		><?php echo $centre['CENTRE_NAME'] ?></option>
                                                	<?php }
                                                ?>
                                            </select>
										</div>

										<div class="col-md-3">
											<label>Cheque Bounce Only: </label>
											<input type="checkbox" class="form-control" <?php if(isset($deleted_receipt_filter['cheque_bounce_only'])){ if($deleted_receipt_filter['cheque_bounce_only'] == "on"){echo "checked='true'";} } ?> name="cheque_bounce_only">
										</div>
									</div>



									<div class="col-lg-12" style="margin-left:0px;">
										<br/>
										<div class="col-md-3">
											<button type="submit" class="btn btn-primary" name="get_deleted_receipt" value="deleted_receipts">Search</button>
										</div>

										<div class="col-md-3">
											<button type="submit" class="btn btn-primary" name="deleted_receipt_reset">Reset</button>
										</div>

										<?php if (!empty($deleted_receipt_filter)) { ?>
										<div class="col-lg-4">
										<a href="<?php echo base_url('accounts/deleted-receipts-report');?>" class="btn" style="float:left" title="Export to excel"><i class="fa fa-file-excel-o fa-2x text-success"></i></a>
										</div>
										<?php } ?>
									</div>
								</div>
								<?php echo form_close();?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php if($this->session->userdata('deleted_receipt_filter')){?>
<br/>
<div class="create_batch_form">
<div id="box"><h2></h2></div>
<div class="row">
<div class="col-sm-12">
<div class="panel panel-default">
<!--<div class="panel-heading">Form Elements</div>-->
<div class="panel-body table-responsive">
<table class="table table-bordered table-condensed table-stripped">
<thead>
<tr>
    <th>Action</th>
	<th>Admission Id</th>
	<th>Receipt No</th>
	<th>Manual Token/Receipt No</th>
	<th>Receipt Date</th>
	<th>Student Name</th>
	<th>Contact No</th>
	<th>Amount With GST</th>
	<th>Amount W/o GST</th>
	<th>Amount Deposited</th>
	<th>Date Of Deposition</th>
	<th>Slip No</th>
	<th>Cheque No</th>
	<th>Bank</th>
	<th>Bank Branch</th>
	<th>Cheque Date</th>
	<th>Received By</th>
	<th>Remarks</th>
	<th>Cheque Bounce Penalty Receipt No.</th>
</tr>
</thead>
<tbody>
<!--                    <tr><td colspan="18" class="text-center">Sorry Record Not Found</td></tr>							-->
<?php foreach($deleted_receipt_reports['receipt_data'] as $rec_report){ ?>
<tr>
	<td><a href="<?php echo base_url('accounts/print-receipts/'.$this->encrypt->encode($rec_report['ADMISSION_RECEIPTS_ID']).'/delete'); ?>" title="Print Receipt"><span class="glyphicon glyphicon-print"></span></a></td>
	<td><?php echo $rec_report['ADMISSION_ID'];?></td>
	<td><?php echo $rec_report['RECEIPT_NO'];?></td>
	<td><?php echo $rec_report['ADMISSION_RECEIPT_NO'];?></td>
	<td><?php $admDateRep = str_replace('-', '/', $rec_report['PAYMENT_DATE']);
	$date =  date("d/m/Y", strtotime($admDateRep) );
	echo $date;?></td>
	<td><?php echo $rec_report['STUDENT_NAME'];?></td>
	<td><?php echo $rec_report['Contact'];?></td>
	<td><?php echo $rec_report['AMOUNT_PAID'];?></td>
	<td><?php echo $rec_report['AMOUNT_W/O_ST'];?></td>
	<td><?php echo $rec_report['AMOUNT_DEPOSITED'];?></td>
	<td><?php $admDateRep = str_replace('-', '/', $rec_report['DATE_OF_DEPOSIT']);
	$date =  date("d/m/Y", strtotime($admDateRep) );
	echo $date;?></td>
	<td><?php echo $rec_report['DEPOSIT_SLIP_NO'];?></td>
	<td><?php echo $rec_report['CHEQUE_NO'];?></td>
	<td><?php echo $rec_report['BANK'];?></td>
	<td><?php echo $rec_report['BRANCH'];?></td>
	<td><?php $admDateRep = str_replace('-', '/', $rec_report['CHEQUE_DATE']);
	$date =  date("d/m/Y", strtotime($admDateRep) );
	echo $date;?></td>
	<td><?php echo $rec_report['EMPLOYEE_NAME'];?></td>
	<td><?php echo $rec_report['UPDATE_REMARKS'];?></td>
	<td><?php echo $rec_report['bounce_receipt_no'];?></td>
</tr>
<?php } ?>
</tbody>
</table>
<br> </div>
<div class="row">
<div class="col-md-4 pull-right">
<?php echo $deleted_receipt_reports['pagination']; ?>
</div>
</div>
</div>
</div></div>
<?php } ?>
</div></div>
