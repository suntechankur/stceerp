<br/> 
<div class="col-md-12 col-sm-12">
<div class="create_batch_form">
		<div id="box">
			<h2>Other Receipts Cash/Cheque Deposition</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-12">
							<?php if($this->session->flashdata('flashMsg')){
							 if($this->session->flashdata('flashMsg') == "1"){
							 	echo "<label id='flashmsg' style='color:red'>* Please select center.</label>";
							 }?>
							 <script>
							 	setTimeout(function(){ $("#flashmsg").hide(); }, 3000);
							 </script>
							<?php } ?>
							<div class="form-group">
								<?php echo form_open() ?>
								<div class="row" style="margin-left:-30px;">
								<input type="hidden" value="other" id="response">
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>Centre : </label>
											<select name="centre" class="form-control">
                                                <option value="">Please Select</option>
                                                <?php
                                                	foreach($centres as $centre)
                                                		{?>
                                                		<option value="<?php echo $centre['CENTRE_ID'] ?>" 
														<?php if(isset($other_cash_check_deposition['centre'])){ echo ($centre['CENTRE_ID'] == $other_cash_check_deposition['centre']) ? ' selected="selected"' : '';}
														else if(!isset($other_cash_check_deposition['centre'])){
																echo ($centre['CENTRE_ID'] == $this->session->userdata('admin_data')[0]['CENTRE_ID']) ? ' selected="selected"' : '';
															}?>
                                                		><?php echo $centre['CENTRE_NAME'] ?></option>
                                                	<?php }
                                                ?>
                                            </select>
										</div>
										<div class="col-md-6">
											<br/>
											<button type="submit" class="btn btn-primary" name="search_undeposited_amount">Search</button>
										</div>
									</div>
								</div>
								<?php echo form_close();?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<br/>
<div class="create_batch_form">
<div id="box"><h2></h2></div>
<div class="row">
<div class="col-sm-12">
<div class="panel panel-default">
<!--<div class="panel-heading">Form Elements</div>-->
<div class="panel-body table-responsive">
<table class="table table-bordered table-condensed table-stripped">
<thead>
<tr>
	<th>Action</th>
	<th>Admission Id</th>
	<th>Student Name</th>
	<th>Admission Date</th>
	<th>Receipt No</th>
	<th>Manual Token/Receipt No</th>
	<th>Payment Date</th>
	<th>Payment Type</th>
	<th>Receipt Amount</th>
	<th>Amount Dept</th>
	<th>Deposit Date</th>
	<th>Deposit Slip No</th>
	<th>Remarks</th>
</tr>
</thead>
<tbody>

<?php foreach($cash_check_search_data['receipt_data'] as $deposition_data){?>
<tr>
	<td><div class="col-sm-6"><input type="submit" class="btn btn-primary save_cash_cheque_deposition" value="Save" data-receiptid="<?php echo $deposition_data['ADMISSION_RECEIPTS_ID'];?>"></div><div class="col-sm-6"><input type="checkbox" class="form-control cash_cheque_enable"></div></td>
	<td><?php echo $deposition_data['ADMISSION_ID']?></td>
	<td><?php echo $deposition_data['STUDENT_NAME']?></td>
	<td><?php 
$originalDate = $deposition_data['ADMISSION_DATE'];
$newDate = date("d/m/Y", strtotime($originalDate));
echo $newDate;?></td>
	<td><?php echo $deposition_data['ADMISSION_RECEIPT_NO']?></td>
	<td><?php echo $deposition_data['RECEIPT_NO']?></td>
	<td><?php
$paymentdate = $deposition_data['PAYMENT_DATE'];
$newDate = date("d/m/Y", strtotime($paymentdate));
echo $newDate;?></td>
	<td><?php 
			$paymentType = "";
			switch($deposition_data['PAYMENT_TYPE']){
        		case '1': 
                    $paymentType = 'CASH';
                break;
                case '2':
                    $paymentType = 'CC';
                break;
                case '3':
                    $paymentType = 'CHEQUE';
                break;
                default: 
                    $paymentType = $deposition_data['PAYMENT_TYPE'];
            }

		 echo $paymentType;
	?></td>
	<td><?php echo $deposition_data['AMOUNT_PAID']?></td>
	<td><input type="text" disabled="disabled" class="receipt_input amount_dept"></td>
	<td><input type="text" disabled="disabled" class="receipt_input deposit_date enquiry_date"></td>
	<td><input type="text" disabled="disabled" class="receipt_input deposit_slip_no"></td>
	<td><input type="text" disabled="disabled" class="receipt_input remarks"></td>
</tr>
<?php } ?>
</tbody>
</table>
<br> </div>
<div class="row">
<div class="col-md-4 pull-right">
<?php echo $cash_check_search_data['pagination']; ?>
</div>
</div>
</div>
</div></div></div></div