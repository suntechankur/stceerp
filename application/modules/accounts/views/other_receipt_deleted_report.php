<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Other Receipts Deleted Reports</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<?php echo form_open(); ?>
							<div class="col-md-12">
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Receipt Date From:</label>
												<input name="RECEIPT_DATE_FROM" id="RECEIPT_DATE_FROM" value="<?php if(isset($deleted_other_receipt_reports['RECEIPT_DATE_FROM'])){ echo $deleted_other_receipt_reports['RECEIPT_DATE_FROM'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>Receipt Date To:</label>
												<input name="RECEIPT_DATE_TO" id="RECEIPT_DATE_TO" value="<?php if(isset($deleted_other_receipt_reports['RECEIPT_DATE_TO'])){ echo $deleted_other_receipt_reports['RECEIPT_DATE_TO'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>Centre:</label>
												<select name="CENTRE_ID" class="form-control">
													<option value="">Please Select Centre</option>
													<?php
                                                	foreach($centres as $centre)
                                                		{?>
                                                		<option value="<?php echo $centre['CENTRE_ID'] ?>" 
														<?php if(isset($deleted_other_receipt_reports['CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $deleted_other_receipt_reports['CENTRE_ID']) ? ' selected="selected"' : '';}?>
                                                		><?php echo $centre['CENTRE_NAME'] ?></option>
                                                	<?php }
                                                ?>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Cheque Bounce Only</label><br/>
								<input type="checkbox" name="cheque_bounce_only" id="cheque_bounce_only" <?php if(isset($deleted_other_receipt_reports['cheque_bounce_only'])){ if($deleted_other_receipt_reports['cheque_bounce_only'] == "on"){echo "checked='true'";} } ?> class="form-control">
											</div> 
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											
											<div class="col-md-4">
											<br>
												<button type="submit" class="btn btn-primary" name="get_other_deleted_receipt">Search</button>
											</div>
											<div class="col-md-4">
											<br>
												<button type="submit" name="deleted_other_receipt_reset" class="btn btn-primary">Reset Filter</button>

											</div>
											<div class="col-lg-4">
											<?php if (!empty($deleted_other_receipt_reports)) { ?>
										
										<a href="<?php echo base_url('accounts/export-to-excel-deleted-other-receipts');?>" class="btn" style="float:left" title="Export to excel"><i class="fa fa-file-excel-o fa-2x text-success"></i></a>
										<?php } ?>
										</div>
										</div>
									</div>
								</div>
								</div>
							</div>
						 <?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		<?php if($this->session->userdata('deleted_other_receipt_reports')){?>
		<br/>
		<div id="box"><h2></h2></div>
		<div class="row">
		<div class="col-sm-12">
	        <div class="panel panel-default">
	        	<div class="panel-body table-responsive">
	        		<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Action</th>
								<th>Admission Id</th>
								<th>Receipt No</th>
								<th>Manual Token/Receipt No</th>
								<th>Receipt Date</th>
								<th>Student Name</th>
								<th>Amount With GST</th>
								<th>Amount W/o GST</th>
								<th>Amount Deposited</th>
								<th>Date Of Deposition</th>
								<th>Slip No</th>
								<th>Cheque No</th>
								<th>Bank</th>
								<th>Bank Branch</th>
								<th>Cheque Date</th>
								<th>Received By</th>
								<th>Remarks</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($deleted_receipt_reports['receipt_data'] as $rec_report){?>
							<tr>
								<td><a href="<?php echo base_url('accounts/print-other-receipts/'.$this->encrypt->encode($rec_report['ADMISSION_RECEIPTS_ID']).'/delete'); ?>" title="Print Other Receipt"><span class="glyphicon glyphicon-print"></span></a></td>
								<td><?php echo $rec_report['ADMISSION_ID'];?></td>
								<td><?php echo $rec_report['RECEIPT_NO'];?></td>
								<td><?php echo $rec_report['ADMISSION_RECEIPT_NO'];?></td>
								<td><?php $admDateRep = str_replace('-', '/', $rec_report['PAYMENT_DATE']);
	            				$date =  date("d/m/Y", strtotime($admDateRep) );
								echo $date;?></td>
								<td><?php echo $rec_report['STUDENT_NAME'];?></td>
								<td><?php echo $rec_report['AMOUNT_PAID'];?></td>
								<td><?php echo $rec_report['AMOUNT_W/O_ST'];?></td>
								<td><?php echo $rec_report['AMOUNT_DEPOSITED'];?></td>
								<td><?php $admDateRep = str_replace('-', '/', $rec_report['DATE_OF_DEPOSIT']);
	            				$date =  date("d/m/Y", strtotime($admDateRep) );
								echo $date;?></td>
								<td><?php echo $rec_report['DEPOSIT_SLIP_NO'];?></td>
								<td><?php echo $rec_report['CHEQUE_NO'];?></td>
								<td><?php echo $rec_report['BANK'];?></td>
								<td><?php echo $rec_report['BRANCH'];?></td>
								<td><?php $admDateRep = str_replace('-', '/', $rec_report['CHEQUE_DATE']);
	            				$date =  date("d/m/Y", strtotime($admDateRep) );
								echo $date;?></td>
								<td><?php echo $rec_report['EMPLOYEE_NAME'];?></td>
								<td><?php echo $rec_report['UPDATE_REMARKS'];?></td>
							</tr>
						<?php }?>
						</tbody>
	        		</table>
	        		<?php echo $deleted_receipt_reports['pagination']; ?>
	        	</div>
			</div>
		</div>
	</div>
	<?php }?>
</div>
</div>
