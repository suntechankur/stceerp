<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Outstanding Reports</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<?php echo form_open(); ?>
							<div class="col-md-12">
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>First Name:</label>
												<input class="form-control" value="<?php if(isset($outstanding_reports['ENQUIRY_FIRSTNAME'])){ echo $outstanding_reports['ENQUIRY_FIRSTNAME'];} ?>" maxlength="50" name="ENQUIRY_FIRSTNAME" id="ENQUIRY_FIRSTNAME" type="text">
											</div>
											<div class="col-lg-4">
												<label>Last Name:</label>
												<input class="form-control" value="<?php if(isset($outstanding_reports['ENQUIRY_LASTNAME'])){ echo $outstanding_reports['ENQUIRY_LASTNAME'];} ?>" name="ENQUIRY_LASTNAME" id="ENQUIRY_LASTNAME" maxlength="50" type="text">
											</div>
											<div class="col-lg-4">
												<label>Centre:</label>
												<select name="CENTRE_ID" class="form-control">
													<option value="">Please Select Centre</option>
													<?php foreach ($centres as $centre) { ?>
														<option value="<?php echo $centre['CENTRE_ID']; ?>" <?php if(isset($outstanding_reports['CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $outstanding_reports['CENTRE_ID']) ? ' selected="selected"' : '';}?>><?php echo $centre['CENTRE_NAME']; ?> </option>
													<?php } ?>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>From Admission Date:</label>
												<input name="ADMISSION_DATE_FROM" id="ADMISSION_DATE_FROM" value="<?php if(isset($outstanding_reports['ADMISSION_DATE_FROM'])){ echo $outstanding_reports['ADMISSION_DATE_FROM'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>To Admission Date:</label>
												<input name="ADMISSION_DATE_TO" id="ADMISSION_DATE_TO" value="<?php if(isset($outstanding_reports['ADMISSION_DATE_TO'])){ echo $outstanding_reports['ADMISSION_DATE_TO'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<!-- <div class="col-lg-4">
												<label>Show Zero Balance:</label><br/>
								<input type="checkbox" name="zero_balance_data" id="zero_balance_data" class="form-control">
											</div> -->
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
											</div>
											<div class="col-md-4">
											<br>
												<button type="submit" class="btn btn-primary" name="get_outstanding_report">Search</button>
											</div>
											<div class="col-md-4">
											<br>
												<button type="submit" name="outstanding_report_reset" class="btn btn-primary">Reset Filter</button>

											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
						 <?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		<?php if($this->session->userdata('outstanding_reports')){?>
		<br/>
		<div id="box"><h2></h2></div>
		<div class="row">
		<div class="col-sm-12">
	        <div class="panel panel-default">
	        	<div class="panel-body table-responsive">
	        		<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Admission Id</th>
								<th>Admission Date</th>
								<th>Student Name</th>
								<th>Course Taken</th>
								<th>Fees</th>
								<th>Fees Paid</th>
								<th>Balance Fees</th>
								<th>First Receipt Date</th>
								<th>Last Receipt Date</th>
								<th>Contact No</th>
								<th>Centre Name</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($outstanding_data['receipt_data'] as $out_data){?>
							<tr>
								<td><?php echo $out_data['ADMISSION_ID'];?></td>
								<td><?php $admDateRep = str_replace('-', '/', $out_data['ADMISSION_DATE']);
	            				$date =  date("d/m/Y", strtotime($admDateRep) );
								echo $date;?></td>
								<td><?php echo $out_data['STUDENT_NAME'];?></td>
								<td><?php echo $out_data['COURSE_TAKEN'];?></td>
								<td><?php echo $out_data['TOTALFEES'];?></td>
								<td><?php echo $out_data['FEES_PAID'];?></td>
								<td><?php echo $out_data['BALANCE_FEES'];?></td>
								<td><?php echo $out_data['first_receipt_data'];?></td>
								<td><?php echo $out_data['last_receipt_data'];?></td>
								<td><?php echo $out_data['CONTACT_NO'];?></td>
								<td><?php echo $out_data['CENTRE_NAME'];?></td>
							</tr>
						<?php }?>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td><strong>Total</strong></td>
								<td><strong><?php echo $grand_total_fees;?></strong></td>
								<td><strong><?php echo $grand_total_fees_paid;?></strong></td>
								<td><strong><?php echo $grand_total_balance_fees;?></strong></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
	        		</table>
	        		<?php echo $outstanding_data['pagination']; ?>
	        	</div>
			</div>
		</div>
	</div>
	<?php }?>
</div>
</div>
