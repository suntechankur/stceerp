<?php
ob_start();
tcpdf();
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Suntech Computer Education');
$pdf->SetTitle('Print Fees Receipt');
$pdf->SetSubject('Print Fees Receipt');
$pdf->SetKeywords('Suntech Computer Education Fees Receipt.');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('5', '5', '5');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

$pdf->setFontSubsetting(false);

// $pdf->setFontStretching('100%');
$pdf->setFontSpacing('+0.2mm');
$pdf->SetFont('', '', 9);
// ---------------------------------------------------------

// add a page
$pdf->AddPage();

// create some HTML content
$payment_date = date("d/m/Y", strtotime($receipt_details_for_edit[0]['PAYMENT_DATE']));

$payment_type = "";
if (is_numeric($receipt_details_for_edit[0]['PAYMENT_TYPE'])){
    switch($receipt_details_for_edit[0]['PAYMENT_TYPE']){
        case '1':
            $payment_type = 'Cash';
        break;
        case '2':
            $payment_type = 'UPI';
        break;
        case '3':
            $payment_type = 'Cheque';
        break;
    }
}

$payment_for = "";
$centre_name = $receipt_details_for_edit[0]['CENTRE_NAME'];
//$codes = "SAC Code: <strong>999293</strong>";
$receipt_type = "CASH Receipt cum Invoice";
// if(isset($receipt_details_for_edit[0]['RECEIPT_TYPE'])){
//   if($receipt_details_for_edit[0]['RECEIPT_TYPE'] == "university"){
//     $payment_for = " for <strong>".$receipt_details_for_edit[0]['RECEIPT_SPECIFIC_FOR']."</strong>";
//     $centre_name = "KANDIVALI(W)";
//     $codes = "Study Centre Code: <strong>31309</strong>";
//     $receipt_type = "YCMOU CASH Receipt cum Invoice";
//   }
// }

$html = '<div>
            <table width="100%" style="font-size: small; height: 1400px; padding: 2px 15px 20px 15px;">
                <tr>
                    <td align="left">
                    <label style="margin-top:100px !important;">Branch : <strong>'.$centre_name.'</strong></label>
                    </td>
                    <td colspan="2" align="center">
                        <h2>Suntech Computer Education</h2>
                        <span class="style6"><strong>'.$receipt_type.'</strong></span>
                    </td>
                    <td colspan="1">Receipt No.: <strong>'.$receipt_details_for_edit[0]['ADMISSION_RECEIPT_NO'].'</strong><br/>Receipt Date : <strong>'.$payment_date.'</strong></td><br/>
                    <hr />
                </tr>
                <tr>
                    <td colspan="6">Received with thanks from Mr./Mrs./Miss <strong>'.$receipt_details_for_edit[0]['STUDENT_NAME'].'</strong>, a total <strong>'.$payment_type.'</strong> amount (Course Fees) of <strong>Rs.'.$receipt_details_for_edit[0]['AMOUNT_PAID'].'/- (<strong> Rupees '.$receipt_details_for_edit[0]['AMOUNT_IN_WORDS'].' Only </strong>)</strong>';
                    if($payment_type != "Cash"){
                      $html .= ' Bank Name : '.$receipt_details_for_edit[0]['BANK'].' Cheque/DD/TID Number : '.$receipt_details_for_edit[0]['CHEQUE_NO'].' Branch : '.$receipt_details_for_edit[0]['BRANCH'].' Cheque Date : '.date("d/m/Y",strtotime($receipt_details_for_edit[0]['CHEQUE_DATE']));
                    }
            $html .=' towards payment for  Computer Training.
                  </td>
                </tr>

                <tr>
                    <td align="left" colspan="3">
                        <ul>
                            <li>Fees once paid will not be refunded under any Circumstances</li>
                            <li>Students have to pay their monthly installment by 10th of every month</li>
                            <li>Late payment of installment will attract penalty of Rs.50 per day</li>';
                if($payment_type != "Cash"){
                  $html .= '<li>This receipt is valid subject to Realization of Cheque/DD/Online/UPI Payment</li>';
                }
                            $html .= '<li>This receipt has to be produced while collecting the Certificate</li>
                            <li>Students have to attend certain special subjects batches at select centers only</li>
                            <li>Please note provision of computer training will be done only after receiving fees in advance.</li>
                            <li>For any query or complaint call Helpline: 7709356444 / 777</li>
                        </ul>
                    </td>
                     <td align="left" colspan="1">
                     <br/><br/><br/>
                        Commercial Training & Coaching<br/><br/>
                        <strong>Admission ID : '.$receipt_details_for_edit[0]['ADMISSION_ID'].'</strong><br/><br/>
                        Recieved by: <br/><strong>'.$receipt_details_for_edit[0]['EMPLOYEE_NAME'].'</strong>
                    </td>
                </tr>
                <tr><td colspan="3">Print Date : '.date("d/m/y  h:i A").'</td><td colspan="1" align="right">Signature&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
            </table>
        </div>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();


ob_end_clean();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('print_receipts_'.$receipt_details_for_edit[0]['STUDENT_NAME'].'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
?>
