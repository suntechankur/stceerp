<?php

tcpdf();
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Suntech Computer Education');
$pdf->SetTitle('Print student receipt summary');
$pdf->SetSubject('Print student receipt summary');
$pdf->SetKeywords('Suntech Computer Education student receipt summary');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// ---------------------------------------------------------

// add a page
$pdf->AddPage();

// create some HTML content
$date = date("d/m/Y", strtotime($receipt_details_for_edit[0]['PAYMENT_DATE']));
$admission_date = date("d/m/Y", strtotime($receipt_details_for_edit[0]['ADMISSION_DATE']));

$i = 1;
$plan = array();
foreach($get_admission_installment as $installment_plan){
    array_push($plan,$i.') '.date("d/m/Y", strtotime($installment_plan['DUEDATE'])).' - '.$installment_plan['DUE_AMOUNT']);
    $i++;
}

$plans = implode (",  ", $plan);


$fees = array();
foreach($fees_amount_till_date as $all_fees){
    array_push($fees,$all_fees['AMOUNT_PAID_FEES']);
    $i++;
}

$all_fees = implode (" + ", $fees);

$html = '
        <div>
                <div>
                    <span align="center">
                        <h2>Suntech computer education</h2>
                        <strong>Branch : '.$receipt_details_for_edit[0]['CENTRE_NAME'].'</strong>
                   </span>
                    <span align="left">Receipt Date : <strong>'.$date.'</strong></span><br/>
                </div>
            <table width="640px" style="font-size: small; height: 1400px; padding:15px;" border="1">
                <tr><td colspan="6"><strong>Student Summary</strong></td></tr>
                <tr><td colspan="2">Admission ID</td><td colspan="4"><strong>'.$receipt_details_for_edit[0]['ADMISSION_ID'].'</strong></td></tr>
                <tr><td colspan="2">Name</td><td colspan="4"><strong>'.$receipt_details_for_edit[0]['STUDENT_NAME'].'</strong></td></tr>
                <tr><td colspan="2">Admission Date</td><td colspan="4"><strong>'.$admission_date.'</strong></td></tr>
                <tr><td colspan="2">Course Enrolled</td><td colspan="4"><strong>'.$receipt_details_for_edit[0]['COURSE_TAKEN'].'</strong></td></tr>
                <tr><td colspan="2">Total Course Fees</td><td colspan="4"><strong>'.$receipt_details_for_edit[0]['TOTALFEES'].' /-</strong></td></tr>
                <tr><td colspan="2">Course Fees Paid</td><td colspan="4"><strong>'.$fees_paid_till_date[0]['TOTAL_PAID']. ' ('.$all_fees.')</strong></td></tr>
                <tr><td colspan="2">Course Fees Balance</td><td colspan="4"><strong>'.($receipt_details_for_edit[0]['TOTALFEES'] - $fees_paid_till_date[0]['TOTAL_PAID']).' /-</strong></td></tr>
                <tr><td colspan="2">Installment plan for next fees (Next due)</td><td colspan="4">'.$plans.'</td></tr>
            </table>
            <div><p>Print Date : '.date("d/m/y  h:i A").'</p></div>
        </div>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();




// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_006.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
?>
