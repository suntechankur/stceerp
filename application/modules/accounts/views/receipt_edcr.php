<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Collection EDCR</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<?php echo form_open(); ?>
							<div class="col-md-12">
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>First Name:</label>
												<input name="first_name" id="FIRST_NAME" value="<?php if(isset($receipt_edcr_report['first_name'])){ echo $receipt_edcr_report['first_name'];} ?>" class="form-control" type="text">
											</div>
											<div class="col-lg-4">
												<label>Last Name:</label>
												<input name="last_name" id="LAST_NAME" value="<?php if(isset($receipt_edcr_report['last_name'])){ echo $receipt_edcr_report['last_name'];} ?>" class="form-control" type="text">
											</div>
											<div class="col-lg-4">
												<label>Centre:</label>
												<select name="centre" class="form-control">
													<option value="">Please Select Centre</option>
													<?php
                                                	foreach($centres as $centre)
                                                		{?>
                                                		<option value="<?php echo $centre['CENTRE_ID'] ?>"
														<?php if(isset($receipt_edcr_report['centre'])){ echo ($centre['CENTRE_ID'] == $receipt_edcr_report['centre']) ? ' selected="selected"' : '';}?>
                                                		><?php echo $centre['CENTRE_NAME'] ?></option>
                                                	<?php }
                                                ?>
												</select>
											</div>
										</div>
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Receipt Date From:</label>
												<input name="receipt_from_date" id="RECEIPT_DATE_FROM" value="<?php if(isset($receipt_edcr_report['receipt_from_date'])){ echo $receipt_edcr_report['receipt_from_date'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>Receipt Date To:</label>
												<input name="receipt_to_date" id="RECEIPT_DATE_TO" value="<?php if(isset($receipt_edcr_report['receipt_to_date'])){ echo $receipt_edcr_report['receipt_to_date'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
										</div>
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Admission Date From:</label>
												<input name="admission_from_date" id="ADMISSION_DATE_FROM" value="<?php if(isset($receipt_edcr_report['admission_from_date'])){ echo $receipt_edcr_report['admission_from_date'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>Admission Date To:</label>
												<input name="admission_to_date" id="ADMISSION_DATE_TO" value="<?php if(isset($receipt_edcr_report['admission_to_date'])){ echo $receipt_edcr_report['admission_to_date'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
										</div>
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Deposit Date From:</label>
												<input name="deposit_date_from" id="DEPOSIT_DATE_FROM" value="<?php if(isset($receipt_edcr_report['deposit_date_from'])){ echo $receipt_edcr_report['deposit_date_from'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>Deposit Date To:</label>
												<input name="deposit_date_to" id="DEPOSIT_DATE_TO" value="<?php if(isset($receipt_edcr_report['deposit_date_to'])){ echo $receipt_edcr_report['deposit_date_to'];} ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
											</div>
											<div class="col-md-4">
											<br>
												<button type="submit" class="btn btn-primary" name="receipt_edcr_search">Search</button>
											</div>
											<div class="col-md-4">
											<br>
												<button type="submit" name="receipt_edcr_reset" class="btn btn-primary">Reset Filter</button>

											</div>
										</div>
									</div>
								</div>
								</div>
							</div>
						 <?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		<?php if($this->session->userdata('receipt_edcr_report')){?>
		<br/>
		<div id="box"><h2></h2></div>
		<div class="row">
		<div class="col-sm-12">
	        <div class="panel panel-default">
	        	<div class="panel-body table-responsive">
	        		<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Sr No.</th>
								<th>Date</th>
								<th>Enq</th>
								<th>Adm</th>
								<th>House Calls</th>
								<th>Name</th>
								<th>Course Selected</th>
								<th>Billing Amount</th>
								<th>Cash Receipt No.</th>
								<th>Cheque Receipt No.</th>
								<th>Token No.</th>
								<th>New Adm</th>
								<th>Outstanding New Adm</th>
								<th>Outstanding Old Adm</th>
								<th>Cash</th>
								<th>Cheque</th>
								<th>University Share</th>
								<th>Fees (w/o ST/GST)</th>
								<th>ST/GST</th>
								<th>Running Total (w/o ST/GST)</th>
								<th>Deposition Date</th>
								<th>Cheque No</th>
								<th>Cheque Date</th>
								<th>Bank</th>
								<th>Delay in Deposition (Days)</th>
								<th>Payment For</th>
								<th>Collection Month</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$i = 1;
						foreach($all_receipts_edcr['receipt_data'] as $rec_report){?>
							<tr>
								<td>
									<?php echo $i++;?>
								</td>
								<td><?php $admDateRep = str_replace('-', '/', $rec_report['PAYMENT_DATE']);
	            				$date =  date("d/m/Y", strtotime($admDateRep) );
								echo $date;?></td>
								<td><?php echo $rec_report['ENQUIRIES'];?></td>
								<td><?php echo $rec_report['ADMISSION'];?></td>
								<td><?php echo $rec_report['HOUSE_CALLS'];?></td>
								<td><?php echo $rec_report['STUDENT_NAME'];?></td>
								<td><?php echo $rec_report['COURSE_TAKEN'];?></td>
								<td><?php echo $rec_report['billing_amount'];?></td>
								<td><?php echo $rec_report['cash_receipt_no'];?></td>
								<td><?php echo $rec_report['cheque_receipt_no'];?></td>
								<td></td>
								<td><?php echo $rec_report['new_admission'];?></td>
								<td><?php echo $rec_report['new_admission_outstanding'];?></td>
								<td><?php echo $rec_report['old_admission_outstanding'];?></td>
								<td><?php echo $rec_report['cash'];?></td>
								<td><?php echo $rec_report['cheque'];?></td>
								<td></td>
								<td><?php echo $rec_report['AMOUNT_PAID_FEES'];?></td>
								<td><?php echo $rec_report['AMOUNT_PAID_SERVICETAX'];?></td>
								<td><?php echo $rec_report['AMOUNT_PAID_FEES'];?></td>
								<td><?php
								if(strtotime($rec_report['DATE_OF_DEPOSIT']) > 0){
									$admDateRep = str_replace('-', '/', $rec_report['DATE_OF_DEPOSIT']);
	            					$date =  date("d/m/Y", strtotime($admDateRep) );
									echo $date;
								}
								?></td>
								<td><?php echo $rec_report['CHEQUE_NO'];?></td>
								<td><?php
								if($rec_report['CHEQUE_DATE'] != null){
									$admDateRep = str_replace('-', '/', $rec_report['CHEQUE_DATE']);
		            				$date =  date("d/m/Y", strtotime($admDateRep) );
									echo $date;
								}
								?></td>
								<td><?php echo $rec_report['BANK'].$rec_report['BRANCH'];?></td>
								<td><?php echo $rec_report['DEPOSITION_DELAY'];?></td>
								<td><?php echo $rec_report['PARTICULAR'];?></td>
								<td><?php echo $rec_report['COLLECTION_MONTH'];?></td>
							</tr>
						<?php }?>
							<tr style="font-weight: bold;font-size: 20px;">
								<td></td>
								<td></td>
								<td>Total</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td><?php echo $billing_amount; ?></td>
								<td></td>
								<td></td>
								<td><?php echo $token_number; ?></td>
								<td><?php echo $new_admission; ?></td>
								<td><?php echo $outstanding_new_admission; ?></td>
								<td><?php echo $outstanding_old_admission; ?></td>
								<td><?php echo $cash; ?></td>
								<td><?php echo $cheque; ?></td>
								<td></td>
								<td><?php echo $fees_without_gst; ?></td>
								<td><?php echo $fees_gst; ?></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
	        		</table>
	        	</div>
			</div>
		</div>
	</div>
	<?php }?>
</div>
</div>
