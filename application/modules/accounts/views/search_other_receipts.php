<br/> 
<div class="col-md-12 col-sm-12">
<div class="create_batch_form">
		<div id="box">
			<h2>Other Receipts search</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-12">
							<div class="form-group">
								<?php echo form_open() ?>
								<div class="row" style="margin-left:-30px;">
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>First Name : </label><input type="text" class="form-control" value="<?php if(isset($other_receipt_filter['first_name'])){ echo $other_receipt_filter['first_name'];} ?>" name="first_name">
										</div>
										<div class="col-md-4">
											<label>Last Name : </label><input type="text" class="form-control" value="<?php if(isset($other_receipt_filter['last_name'])){ echo $other_receipt_filter['last_name'];} ?>" name="last_name">
										</div>
										<div class="col-md-4">
											<label>Centre : </label>
											<select name="centre" class="form-control">
                                                <option value="">Please Select</option>
                                                <?php
                                                	foreach($centres as $centre)
                                                		{?>
                                                		<option value="<?php echo $centre['CENTRE_ID'] ?>" 
														<?php if(isset($other_receipt_filter['centre'])){ echo ($centre['CENTRE_ID'] == $other_receipt_filter['centre']) ? ' selected="selected"' : '';}?>
                                                		><?php echo $centre['CENTRE_NAME'] ?></option>
                                                	<?php }
                                                ?>
                                            </select>
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>Receipt From Date : </label><input type="text" class="form-control enquiry_date" value="<?php if(isset($other_receipt_filter['receipt_from_date'])){ echo $other_receipt_filter['receipt_from_date'];} ?>" name="receipt_from_date">
										</div>
										<div class="col-md-4">
											<label>Receipt To Date : </label><input type="text" class="form-control enquiry_date" value="<?php if(isset($other_receipt_filter['receipt_to_date'])){ echo $other_receipt_filter['receipt_to_date'];} ?>" name="receipt_to_date">
										</div>
										<div class="col-md-4">
											<label>Payment Type : </label>
											<select name="payment_type" class="form-control">
                                                <option value="">Please Select</option>
                                                <option <?php if(isset($other_receipt_filter['payment_type'])){ echo ($other_receipt_filter['payment_type'] == "Cash") ? ' selected="selected"' : '';}?> value="Cash">Cash</option>
                                                <option <?php if(isset($other_receipt_filter['payment_type'])){ echo ($other_receipt_filter['payment_type'] == "Cheque") ? ' selected="selected"' : '';}?> value="Cheque">Cheque</option> 
                                                <option <?php if(isset($other_receipt_filter['payment_type'])){ echo ($other_receipt_filter['payment_type'] == "CC") ? ' selected="selected"' : '';}?> value="CC">CC</option>
                                            </select>
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>Admission From Date : </label><input type="text" class="form-control enquiry_date" value="<?php if(isset($other_receipt_filter['admission_from_date'])){ echo $other_receipt_filter['admission_from_date'];} ?>" name="admission_from_date">
										</div>
										<div class="col-md-4">
											<label>Admission To Date : </label><input type="text" class="form-control enquiry_date" value="<?php if(isset($other_receipt_filter['admission_to_date'])){ echo $other_receipt_filter['admission_to_date'];} ?>" name="admission_to_date">
										</div>
										<div class="col-md-4">
											<label>Received By : </label>
											<input type="text" class="form-control" value="<?php if(isset($other_receipt_filter['received_by'])){ echo $other_receipt_filter['received_by'];} ?>" name="received_by">
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>Receipt No From : </label>
											<input type="text" class="form-control" value="<?php if(isset($other_receipt_filter['receipt_no_from'])){ echo $other_receipt_filter['receipt_no_from'];} ?>" name="receipt_no_from">
										</div>
										<div class="col-md-4">
											<label>Receipt No To : </label>
											<input type="text" class="form-control" value="<?php if(isset($other_receipt_filter['receipt_no_to'])){ echo $other_receipt_filter['receipt_no_to'];} ?>" name="receipt_no_to">
										</div>
										<div class="col-md-4">
											<br/>
											<label>Note : Enter 1001 for receipt no C-215-1001 </label>
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>Manual Receipt From : </label>
											<input type="text" class="form-control" value="<?php if(isset($other_receipt_filter['manual_receipt_from'])){ echo $other_receipt_filter['manual_receipt_from'];} ?>" name="manual_receipt_from">
										</div>
										<div class="col-md-4">
											<label>Manual Receipt To : </label>
											<input type="text" class="form-control" value="<?php if(isset($other_receipt_filter['manual_receipt_to'])){ echo $other_receipt_filter['manual_receipt_to'];} ?>" name="manual_receipt_to">
										</div>
										<div class="col-md-4">
											<label>Deposition Status : </label>
											<select name="deposition_status" class="form-control">
                                                <option <?php if(isset($other_receipt_filter['deposition_status'])){ echo ($other_receipt_filter['deposition_status'] == "all") ? ' selected="selected"' : '';}?> value="all">All</option>
                                                <option <?php if(isset($other_receipt_filter['deposition_status'])){ echo ($other_receipt_filter['deposition_status'] == "deposited") ? ' selected="selected"' : '';}?> value="deposited">Deposited</option>
                                                <option <?php if(isset($other_receipt_filter['deposition_status'])){ echo ($other_receipt_filter['deposition_status'] == "undeposited") ? ' selected="selected"' : '';}?>  value="undeposited">Undeposited</option>
                                            </select>
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>Deposit Date From : </label>
											<input type="text" class="form-control enquiry_date" value="<?php if(isset($other_receipt_filter['deposit_date_from'])){ echo $other_receipt_filter['deposit_date_from'];} ?>" name="deposit_date_from">
										</div>
										<div class="col-md-4">
											<label>Deposit Date To : </label>
											<input type="text" class="form-control enquiry_date" value="<?php if(isset($other_receipt_filter['deposit_date_to'])){ echo $other_receipt_filter['deposit_date_to'];} ?>" name="deposit_date_to">
										</div>
										<div class="col-md-4">
											<label>Cheque No : </label>
											<input type="text" class="form-control" value="<?php if(isset($other_receipt_filter['cheque_no'])){ echo $other_receipt_filter['cheque_no'];} ?>" name="cheque_no">
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<br/>
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary" name="search_other_receipts">Search</button>
										</div>
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary" name="reset">Reset</button>
										</div>								    
									</div>
								</div>
								<?php echo form_close();?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<br/>
<?php if($this->session->userdata('other_receipt_filter')){?>
<div class="create_batch_form">
<div id="box"><h2></h2></div>
<div class="row">
<div class="col-sm-12">
<div class="panel panel-default">
<!--<div class="panel-heading">Form Elements</div>-->
<div class="panel-body table-responsive">
<table class="table table-bordered table-condensed table-stripped">
<thead>
<tr>
	<th>Action</th>
	<th>Is Cleared</th>
	<th>Remarks</th>
	<th>Receipt No</th>
	<th>Manual Token/Receipt No</th>
	<th>Receipt Date</th>
	<th>Payment Type</th>
	<th>Admission Id</th>
	<th>Name</th>
	<th>Amount With GST</th>
	<th>Amount W/o GST</th>
	<th>Amount Deposited</th>
	<th>Date Of Deposited</th>
	<th>Slip No</th>
	<th>Balance To Be Deposited</th>
	<th>Cheque No</th>
	<th>Bank</th>
	<th>Cheque Date</th>
	<th>Received By</th>
	<th>Study Centre Name</th>
	<th>Collection Month</th>
</tr>
</thead>
<tbody>
<!--                    <tr><td colspan="18" class="text-center">Sorry Record Not Found</td></tr>							-->
<?php 
$count = $this->uri->segment(2) + 1;
foreach($get_receipt_data['receipt_data'] as $receipt_data){ 
?>
<tr>
<td>
<a href="<?php echo base_url('accounts/admission-other-receipts/'.$this->encrypt->encode($receipt_data['ADMISSION_RECEIPTS_ID']).'/'.$this->encrypt->encode($receipt_data['ADMISSION_ID']).'/update'); ?>"  title="Edit"><span class="glyphicon glyphicon-edit"></span></a>
<a href="<?php echo base_url('accounts/admission-other-receipts/'.$this->encrypt->encode($receipt_data['ADMISSION_RECEIPTS_ID']).'/'.$this->encrypt->encode($receipt_data['ADMISSION_ID']).'/delete'); ?>" title="Cancel"><span class="glyphicon glyphicon-remove"></span></a>
<a href="<?php echo base_url('accounts/print-other-receipts/'.$this->encrypt->encode($receipt_data['ADMISSION_RECEIPTS_ID']).'/search'); ?>" title="Print Other Receipt"><span class="glyphicon glyphicon-print"></span></a>
</td>
<td><?php if($receipt_data['STATUS'] == 1){?>
		<input type="checkbox" checked="true"></td>
	<?php } else {?>
		<input type="checkbox"></td>
	<?php }?>
<td><input type="text" disabled="true" value="<?php echo $receipt_data['REMARK']; ?>"></td>
<td><?php echo $receipt_data['ADMISSION_RECEIPT_NO']; ?></td>
<td></td>
<td>
<?php
$originalDate = $receipt_data['PAYMENT_DATE'];
$newDate = date("d/m/Y", strtotime($originalDate));
echo $newDate; ?>
</td>
<td>
<?php 
if(is_numeric($receipt_data['PAYMENT_TYPE'])){
	$payment_type = "";
	switch($receipt_data['PAYMENT_TYPE']){
		case "1":
			$payment_type = "CASH";
			break;
		case "2":
			$payment_type = "CC";
			break;
		case "3":
			$payment_type = "CHEQUE";
			break;
	}
	echo $payment_type; 
}
else{
	echo $receipt_data['PAYMENT_TYPE']; 
}
?>	
</td>
<td><?php echo $receipt_data['ADMISSION_ID']; ?></td>
<td><?php echo $receipt_data['STUDENT_NAME']; ?></td>
<td><?php echo $receipt_data['AMOUNT_PAID']; ?></td>
<td><?php echo $receipt_data['AMOUNT_PAID_FEES']; ?></td>
<td><?php echo $receipt_data['AMOUNT_DEPOSITED']; ?></td>
<td>
<?php
$originalDate = $receipt_data['DATE_OF_DEPOSIT'];
$newDate = date("d/m/Y", strtotime($originalDate));
echo $newDate; ?>
</td>
<td><?php echo $receipt_data['SLIP_NO']; ?></td>
<td><?php echo $receipt_data['BALANCE']; ?></td>
<td><?php echo $receipt_data['CHEQUE_NO']; ?></td>
<td><?php echo $receipt_data['BANK']; ?></td>
<td><?php echo $receipt_data['CHEQUE_DATE']; ?></td>
<td><?php echo $receipt_data['RECEIVED_BY']; ?></td>
<td><?php echo $receipt_data['CENTRE_NAME']; ?></td>
<td>
<?php
$month = date("m", strtotime($originalDate));
?>
<select>
<option disabled selected>Select Month</option>
<option value="01" 
<?php if($month == '01'){
echo 'selected="selected"';
}
?> >Jan</option>
<option value="02" 
<?php if($month == '02'){
echo 'selected="selected"';
}
?>>Feb</option>
<option value="03" 
<?php if($month == '03'){
echo 'selected="selected"';
}
?>>Mar</option>
<option value="04" 
<?php if($month == '04'){
echo 'selected="selected"';
}
?>>Apr</option>
<option value="05" 
<?php if($month == '05'){
echo 'selected="selected"';
}
?>>May</option>
<option value="06" 
<?php if($month == '06'){
echo 'selected="selected"';
}
?>>Jun</option>
<option value="07" 
<?php if($month == '07'){
echo 'selected="selected"';
}
?>>Jul</option>
<option value="08" 
<?php if($month == '08'){
echo 'selected="selected"';
}
?>>Aug</option>
<option value="09" 
<?php if($month == '09'){
echo 'selected="selected"';
}
?>>Sep</option>
<option value="10" 
<?php if($month == '10'){
echo 'selected="selected"';
}
?>>Oct</option>
<option value="11" 
<?php if($month == '11'){
echo 'selected="selected"';
}
?>>Nov</option>
<option value="12" 
<?php if($month == '12'){
echo 'selected="selected"';
}
?>>Dec</option>
</select></td>
</tr>
<?php $count++; } ?>
<tr>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th>Total CASH</th>
	<th><?php echo $total_paid_cash_amount['amt_gst'];?></th>
	<th><?php echo $total_paid_cash_amount['amt_w/o_gst'];?></th>
	<th><?php echo $total_paid_cash_amount['amt_deposited'];?></th>
	<th></th>
	<th></th>
	<th><?php echo $total_paid_cash_amount['balance_amt_deposited'];?></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
</tr>
<tr>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th>Total CHEQUE</th>
	<th><?php echo $total_paid_check_amount['amt_gst'];?></th>
	<th><?php echo $total_paid_check_amount['amt_w/o_gst'];?></th>
	<th><?php echo $total_paid_check_amount['amt_deposited'];?></th>
	<th></th>
	<th></th>
	<th><?php echo $total_paid_check_amount['balance_amt_deposited'];?></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
</tr>
<tr>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th>Total CC</th>
	<th><?php echo $total_paid_cc_amount['amt_gst'];?></th>
	<th><?php echo $total_paid_cc_amount['amt_w/o_gst'];?></th>
	<th><?php echo $total_paid_cc_amount['amt_deposited'];?></th>
	<th></th>
	<th></th>
	<th><?php echo $total_paid_cc_amount['balance_amt_deposited'];?></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
</tr>
<tr>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th>Grand Total</th>
	<th><?php echo $grand_total['amt_gst'];?></th>
	<th><?php echo $grand_total['amt_w/o_gst'];?></th>
	<th><?php echo $grand_total['amt_deposited'];?></th>
	<th></th>
	<th></th>
	<th><?php echo $grand_total['balance_amt_deposited'];?></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
</tr>
</tbody>
</table>
<br> </div>
</div>
</div></div></div></div>
<?php } ?>