<br/> 
<div class="col-md-12 col-sm-12">
<div class="create_batch_form">
		<div id="box">
			<h2>Transfer Receipts</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="col-md-12">
							<div class="form-group">
								<div class="row" style="margin-left:-30px;">
									<div class="col-lg-12" style="margin-left:0px;">
										<input type="hidden" value="receipts" id="response">
										<div class="col-md-3">
											<label style="border-bottom: 1px solid #000;">Transfer From </label>
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-3">
											<label>Admission Id : </label><input type="text" class="form-control" id="from_admission_id">
										</div>
										<div class="col-md-1">
											<label></label>
											<input type="submit" class="btn btn-primary check_transfer_receipt" data-action="from" value="Check">
										</div>
										<div class="col-md-4">
											<label>Name : </label><input type="text" class="form-control" id="from_name" disabled="disabled">
										</div>
										<div class="col-md-4">
											<label>Centre : </label>
											<input type="text" class="form-control" id="from_centre" disabled="disabled">
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>Admission Date : </label><input type="text" class="form-control" id="from_admission_date" disabled="disabled">
										</div>
										<div class="col-md-4">
											<label>Course Taken : </label><input type="text" class="form-control" id="from_course_taken" disabled="disabled">
										</div>
										<div class="col-md-4">
											<label>Total Fees : </label>
											<input type="text" class="form-control" id="from_total_fees" disabled="disabled"> 
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;display:none;" id="from_receipt_table">
										<table class="table table-hover table-striped receiptTbl">
											<thead>
												<tr>
													<th>Action</th>
													<th>Receipt No</th>
													<th>Token No</th>
													<th>Date</th>
													<th>Amount Paid WGST</th>
													<th>Amount Paid W/o GST</th>
													<th>Payment Type</th>
													<th>Balance</th>
												</tr>
											</thead>
											<tbody id="from_receipt_details">
											</tbody>
										</table>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-12">
											<label>Remarks : <span class="text-danger">*</span></label><input type="text" class="form-control" id="from_remarks">
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<br/>
										<div class="col-md-4"></div>
										<div class="col-md-4">
											<button type="submit" class="btn btn-primary col-md-12" id="transfer_receipts">Transfer selected</button>
										</div>							    
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-3">
											<label style="border-bottom: 1px solid #000;">Transfer To</label>
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-3">
											<label>Admission Id : </label><input type="text" class="form-control" id="to_admission_id">
										</div>
										<div class="col-md-1">
											<label></label>
											<input type="submit" class="btn btn-primary check_transfer_receipt" data-action="to" value="Check">
										</div>
										<div class="col-md-4">
											<label>Name : </label>
											<input type="text" class="form-control" id="to_name" disabled="disabled">
										</div>
										<div class="col-md-4">
											<label>Centre : </label>
											<input type="text" class="form-control" id="to_centre" disabled="disabled">
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<div class="col-md-4">
											<label>Admission Date : </label>
											<input type="text" class="form-control" id="to_admission_date" disabled="disabled">
										</div>
										<div class="col-md-4">
											<label>Course Taken : </label>
											<input type="text" class="form-control" id="to_course_taken" disabled="disabled">
										</div>
										<div class="col-md-4">
											<label>Total Fees : </label>
											<input type="text" class="form-control" id="to_total_fees" disabled="disabled">
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;display:none;" id="to_receipt_table">
										<table class="table table-hover table-striped receiptTbl">
											<thead>
												<tr>
													<th>Receipt No</th>
													<th>Token No</th>
													<th>Date</th>
													<th>Amount Paid WGST</th>
													<th>Amount Paid W/o GST</th>
													<th>Payment Type</th>
													<th>Balance</th>
												</tr>
											</thead>
											<tbody id="to_receipt_details">
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<br/>
</div>