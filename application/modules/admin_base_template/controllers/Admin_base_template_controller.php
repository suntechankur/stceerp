<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_base_template_controller extends MX_Controller {
    
	public function dashboard()
	{
		$this->load->admin_view('dashboard');
	}
}
