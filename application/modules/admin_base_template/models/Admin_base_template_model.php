<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_base_template_model extends CI_Model {
	public function userRole($roleId){
		$query = $this->db->select('MENU_XML_FILENAME')
						  ->from('roles')
						  ->where('ROLE_ID',$roleId)
						  ->get();
		return $query->result_array();
	}	
}