<?php
$userId = $this->session->userdata('admin_data')[0]['USER_ID'];
$roleMenus = $roles[0]['MENU_XML_FILENAME'];

// setting up hard code url
$menus = simplexml_load_file(FCPATH . 'resources/'.$roleMenus);
?>
<!DOCTYPE html>
<html class="" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Suntech Computer Education</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .dash-button,
        .dash-button-hide {
            display: none;
        }
    </style>
    <!-- favicon for website head -->
    <link rel="shortcut icon" href="<?php echo base_url('resources/images/'); ?>favicon.png" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url('resources/images/'); ?>favicon.png" type="image/x-icon">
    <!--link for validation -->
    <link href="<?php echo base_url('resources/css/'); ?>pygments-manni.css" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/'); ?>docs.css" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/'); ?>bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/'); ?>jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('resources/css/'); ?>style.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('resources/css/'); ?>bootstrap-multiselect.css" type="text/css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('resources/css/'); ?>jquery.css">
    <link href="<?php echo base_url('resources/css/'); ?>daterangepicker.css" rel="stylesheet">
    <link href="<?php echo base_url('resources/css/'); ?>font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://vitalets.github.io/x-editable/assets/x-editable/bootstrap3-editable/css/bootstrap-editable.css">

    <script src="<?php echo base_url('resources/js/'); ?>jquery.min.js"></script>
     <?php if(isset($add_bel_cust_js)){ ?>
                <script type="text/javascript" src="<?php echo $add_bel_cust_js; // Additional Js Below Custom Js?>"></script>
                <?php } ?>
    </head>
<body>
    <nav id="menu">
        <ul>
            <?php
            foreach ($menus as $menu) {
                if ($menu->url) {
                    $menuUrl = $menu->url;
                }
                else{
                    $menuUrl = '#';
                }
            ?>
                <li><a href="<?php echo base_url($menuUrl); ?>"><?php echo $menu->text; ?></a>
                    <?php if($menu->subMenu){ ?>
                    <ul>
                        <?php
                            foreach($menu->subMenu as $subMenus){

                                foreach($subMenus->menuItem as $subMenu){

                                if ($subMenu->url) {
                                    $subMenuUrl = $subMenu->url;
                                }
                                else{
                                    $subMenuUrl = '#';
                                }
                                ?>
                                <li>
                                    <a href="<?php echo base_url($subMenuUrl); ?>"><?php echo $subMenu->text; ?></a>
                                    <?php if($subMenu->subMenu){ ?>
                                    <ul>
                                        <?php
                                            foreach($subMenu->subMenu as $subSubMenus){

                                                foreach($subSubMenus->menuItem as $subSubMenu){

                                                    if ($subSubMenu->url) {
                                                        $subSubMenuUrl = $subSubMenu->url;
                                                    }
                                                    else{
                                                        $subSubMenuUrl = '#';
                                                    }
                                                    ?>
                                                    <li>
                                                        <a href="<?php echo base_url($subSubMenuUrl); ?>">
                                                            <?php echo $subSubMenu->text;  ?>
                                                        </a>
                                                        <?php if($subSubMenu->subMenu){ ?>
                                                        <ul>
                                                            <?php
                                                            foreach($subSubMenu->subMenu as $subSubSubMenus){

                                                            foreach($subSubSubMenus->menuItem as $subSubSubMenu){
                                                                if ($subSubSubMenu->url) {
                                                        $subSubSubMenuUrl = $subSubSubMenu->url;
                                                    }
                                                    else{
                                                        $subSubSubMenuUrl = '#';
                                                    }

                                                            ?>
                                                            <li>
                                                             <a href="<?php echo base_url($subSubSubMenuUrl); ?>">
                                                                 <?php
                                                                echo $subSubSubMenu->text;
                                                                ?>
                                                             </a>
                                                            </li>
                                                            <?php } } ?>
                                                        </ul>
                                                        <?php } ?>
                                                    </li>
                                        <?php   }
                                            }
                                        ?>
                                    </ul>
                                    <?php } ?>
                                </li>
                            <?php }
                            }
                        ?>
                    </ul>
                    <?php } ?>
                </li>
            <?php } ?>

        </ul>
    </nav>
    <header style="padding-bottom:10px;padding-top:10px;color:#fff;">
        <div class="container">
            <div class="menu"><a href="#menu"><i class="fa fa-bars fa-3x"></i></a></div>
            <div class="logo"><img src="<?php echo base_url('resources/images/theme/') ?>logo.png" style="height:90px;width:300px;"></div>
            <?php
                $employee_name  = $this->session->userdata('admin_data')[0]['EMP_FNAME'].' '.$this->session->userdata('admin_data')[0]['EMP_LASTNAME'];
                if(($this->session->userdata('admin_data')[0]['ROLE_ID'] == "23") || ($this->session->userdata('admin_data')[0]['ROLE_ID'] == "37") || ($this->session->userdata('admin_data')[0]['ROLE_ID'] == "35") || ($this->session->userdata('admin_data')[0]['ROLE_ID'] == "41")){
                    ?>
                    <div class="menu" style="margin-left: 400px;"><label>Welcome, <?php echo $employee_name; ?><br> Employee ID. : <?php echo $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'];?></label><a href="<?php echo base_url('followup_enquiry_details')?>" title="Todays Followups"><i class="fa fa-bell fa-3x"></i><span class="badge" id="followupcount" style="margin-left:-20px;background-color: red;"></span></a></div>
                    <?php
                } else {
            ?>
                    <div class="menu" style="margin-left: 500px;"><label>Welcome, <?php echo $employee_name; ?><br> Employee ID. : <?php echo $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'];?></label>&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url('logout')?>" title="Logout, <?php echo ucfirst($this->session->userdata('admin_data')[0]['EMP_FNAME']);?> !!"><i class="fa fa-power-off fa-2x" style="color:red;"></i></a></div>
            <?php } ?>

            <div class="dash-button">Dashboard Menu</div>
        </div>
    </header>
    <div id="page" class="mm-page mm-slideout" style="">
        <input class="admToken" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
        <div class="container" style="padding:30px 0;">
            <?php $this->load->view($admin_view); ?>
        </div>
    </div>
    <div id="mm-blocker" class="mm-slideout"></div>
    <script src="<?php echo base_url('resources/js/'); ?>jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url('resources/js/'); ?>jquery_002.js"></script>
    <script src="<?php echo base_url('resources/js/'); ?>bootstrap.js"></script>

    <script type="text/javascript" src="<?php echo base_url('resources/js/'); ?>reloaddiv.js"></script>
    <script src="<?php echo base_url('resources/js/'); ?>jquery-ui.js"></script>

    <script src="<?php echo base_url('resources/js/'); ?>moment.min.js"></script>
    <script src="<?php echo base_url('resources/js/'); ?>daterangepicker.js"></script>
    <script src="<?php echo base_url('resources/js/'); ?>validator.js"></script>
    <script src="<?php echo base_url('resources/js/'); ?>widgets.js"></script>
    <script src="<?php echo base_url('resources/js/'); ?>bootstrap-multiselect.js"></script>
    <script src="<?php echo base_url('resources/js/'); ?>jQuery.print.js"></script>
    <script src="https://vitalets.github.io/x-editable/assets/x-editable/bootstrap3-editable/js/bootstrap-editable.js"></script>
    <script src="http://vitalets.github.io/combodate/combodate.js"></script>
    <!-- Additional Javascript Starts -->
    <?php if(isset($add_sup_cust_js)){ ?>
        <script src="<?php echo $add_sup_cust_js; // Additional Above(super)Custom Js?>"></script>
        <?php } ?>
            <!-- Additional Javascript Ends -->
        <script type="text/javascript" src="<?php echo base_url('resources/js/'); ?>custom.js"></script>
        <script type="text/javascript" src="<?php echo base_url('resources/js/'); ?>push.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url('resources/js/'); ?>tableExport.js"></script>
        <script type="text/javascript" src="<?php echo base_url('resources/js/'); ?>jquery.base64.js"></script>
        <script type="text/javascript" src="<?php echo base_url('resources/js/'); ?>html2canvas.js"></script>
        <script type="text/javascript" src="<?php echo base_url('resources/js/'); ?>jspdf/libs/sprintf.js"></script>
        <script type="text/javascript" src="<?php echo base_url('resources/js/'); ?>jspdf/jspdf.js"></script>
        <script type="text/javascript" src="<?php echo base_url('resources/js/'); ?>jspdf/libs/base64.js"></script>

        <?php if(isset($add_bel_cust_js)){ ?>
        <script src="<?php echo $add_bel_cust_js; // Additional Below Custom Js?>"></script>
        <?php } ?>

</body>

</html>