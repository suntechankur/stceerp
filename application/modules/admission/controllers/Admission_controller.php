<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admission_controller extends MX_Controller {

     public function __construct()
        {
                $this->load->model('admission_model');
                $this->load->model('tele_enquiry/tele_enquiry_model');
                $this->load->model('enquiry/enquiry_model');
                $this->load->model('employee/employee_model');
                $this->load->model('students/student_model');
                parent::__construct();
        }

// code for report genration ///////////////////////////////
public function getAdmissionReport()
{
  $data['admission_filter_data'] = $this->session->userdata('admission_filter_data');
  $admission = $this->admission_model->getAdmissionReport($data['admission_filter_data']);

        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);


        $col = 'A';
        for($i=0;$i<count($admission['fields']);$i++){
            $objPHPExcel->getActiveSheet()->SetCellValue($col.'1', $admission['fields'][$i]);
            $objPHPExcel->getActiveSheet()->getStyle("A1:X1")->getFont()->setBold(true);
            $col++;
        }

        $j = 2;
        $alphebetCol = 'A';
        foreach ($admission['details'] as $key => $value)
        {

          foreach ($admission['details'][$key] as $index => $data)
            {

                $objPHPExcel->getActiveSheet()->SetCellValue($alphebetCol.$j,$admission['details'][$key][$index]);

                $alphebetCol++;
                if($alphebetCol == "U")
                {
                  $alphebetCol = 'A';
                }
            }
            $j++;
        }

        $filename = "Admission_Details-" . date("h.i.s.d-m-Y") . ".xls";
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=" . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
}

     public function viewAdmission($offset = 0){
        if(!empty($_POST)){
            $this->session->set_userdata('admission_filter_data',$_POST);

        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('admission_filter_data');
        }

        $data['admission_filter_data'] = $this->session->userdata('admission_filter_data');
        /* Pagination starts */
        $config['base_url'] = base_url('admission/view-admission/');
        $config['total_rows'] = $this->admission_model->getAdmissionList($offset, $per_page = 0, $data['admission_filter_data'],$totalRows=1);
        $config['per_page'] = '10';
        // echo $offset;
        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */
        $data['courses'] = $this->tele_enquiry_model->all_courses();
        $data['modules'] = $this->tele_enquiry_model->all_modules();
        $data['centres'] = $this->tele_enquiry_model->suggested_centre();
        $data['admission_list'] = $this->admission_model->getAdmissionList($offset, $config['per_page'],$data['admission_filter_data']);
        $data['payFor'] = $this->admission_model->payFor();


        // foreach loop for checking other receipt is done or not
        for($i=0;$i<count($data['admission_list']);$i++){

            $data['other_fees_receipts_details'] = $this->admission_model->getOtherReceiptDetailsByAdmissionId($data['admission_list'][$i]['ADMISSION_ID']);
            $data['admission_list'][$i]['FEES_PAID'] = $this->admission_model->getFeesPaidByByAdmissionId($data['admission_list'][$i]['ADMISSION_ID']);
            if($data['admission_list'][$i]['FEES_PAID'] == ""){
              $data['admission_list'][$i]['FEES_PAID']  = 0;
            }
            array_splice($data['admission_list'][$i], 7, 0, array( 'FEES_PAID' => $data['admission_list'][$i]['FEES_PAID']));   // inserting data in between array

            $particular_value = "";
            $particular_id = "";
            $amount_paid = "";
            $total_amount = "";
            $total_amount1 = "";

            if($data['admission_list'][$i]['EXAM_FEES_APPLICABLE'] != 0){
              $particular_value = "EXAM FEE";
            }
            else if($data['admission_list'][$i]['PROCESSING_FEES_APPLICABLE'] != 0){
              $particular_value = "PROCESSING FEE";
            }

            foreach($data['payFor'] as $key => $value){
              if($data['payFor'][$key]['CONTROLFILE_VALUE'] == $particular_value){
                $particular_id = $data['payFor'][$key]['CONTROLFILE_ID'];
              }
            }

            foreach($data['other_fees_receipts_details'] as $otherReceipts){
              if($otherReceipts['PARTICULAR'] == $particular_id){
                $total_amount += $otherReceipts['AMOUNT'];
              }
              else if($otherReceipts['PARTICULAR'] == $particular_value){
                $total_amount1 += $otherReceipts['AMOUNT'];
              }
            }

            $amount_paid = $total_amount + $total_amount1;

            $isOtherReceiptToBeOpened = "";
            $feeType = "";

            if($particular_value == 'EXAM FEE'){
              if($amount_paid < $data['admission_list'][$i]['EXAM_FEES_APPLICABLE']){
                $isOtherReceiptToBeOpened = "true";
              }
              else{
                $isOtherReceiptToBeOpened = "false";
              }
              $feeType = "EXAM FEE";
            }
            else{
              if($amount_paid < $data['admission_list'][$i]['PROCESSING_FEES_APPLICABLE']){
                $isOtherReceiptToBeOpened = "true";
              }
              else{
                $isOtherReceiptToBeOpened = "false";
              }
              $feeType = "PROCESSING FEE";
            }

            $data['admission_list'][$i]['modal_permission'] = $isOtherReceiptToBeOpened;
            $data['admission_list'][$i]['fee_type'] = $feeType;

        }
        // end of foreach function
        $data['add_bel_cust_js'] = base_url('resources/js/admission_cust.js');
        $data['employees'] = $this->employee_model->getEmployeeNamesAsPerCentreLogin();
        // added on today by ankur prajapati dated : 05/01/2018 for getting centre_name from employee_timings
        if(count($data['employees']) == 0){
                $data['employees'] = $this->employee_model->getEmployeeNamesAsPerCentreLoginIfCentreNamesAreNotTheir();
        }

        //$this->employee_model->isPenalty();

        $data['pagination'] = $this->pagination->create_links();

        $this->load->admin_view('view_admission',$data);
    }

     public function addAdmission($id =0){
        $enqId = $this->encrypt->decode($id);

        $data['enqData'] = $this->admission_model->getEnqData($enqId);
        $data['courses'] = $this->tele_enquiry_model->course_interested($enqId);
        $data['occupations'] = $this->enquiry_model->getOccupation();
        $data['educations'] = $this->enquiry_model->getEducation();

        $data['employees'] = $this->employee_model->getEmployeeNamesAsPerCentreLogin();
        $data['modules'] = $this->tele_enquiry_model->module_interested($enqId);
        $data['add_bel_cust_js'] = base_url('resources/js/admission_cust.js');

        $this->form_validation->set_rules('ENQUIRY_FIRSTNAME', 'First Name', 'required');
        $this->form_validation->set_rules('ENQUIRY_LASTNAME', 'Last Name', 'required');
        $this->form_validation->set_rules('TOTALFEES', 'Total Fees', 'required');
        $this->form_validation->set_rules('ENQUIRY_EMAIL', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('ENQUIRY_DATEOFBIRTH', 'D.O.B', 'required');

        if(isset($_POST['ENQUIRY_EDUCATION'])){
            if($_POST['ENQUIRY_EDUCATION'] == ''){
                $this->form_validation->set_rules('ENQUIRY_EDUCATION', 'Education', 'required');
            }
        }
        $this->form_validation->set_rules('ENQUIRY_MOBILE_NO', 'Mobile No.', 'required|min_length[10]|max_length[10]|numeric');
        $this->form_validation->set_rules('ENQUIRY_PARENT_NO', 'Parent No.', 'min_length[10]|max_length[10]|numeric');
        $this->form_validation->set_rules('ENQUIRY_HANDELED_BY', 'Enquiry Handeled By', 'required');
        $this->form_validation->set_rules('IN_TAKE', 'In Take', 'required');
        $this->form_validation->set_rules('ACAD_YEAR', 'Academic Year', 'required');
        if(isset($_POST['UNIVERISTY_NAME'])){
            if($_POST['UNIVERISTY_NAME'] == ''){
                $this->form_validation->set_rules('UNIVERISTY_NAME', 'University Name', 'required');
            }
        }
        $courseItrsdArr = array();
        $moduleItrsdArr = array();
        if(isset($_POST['COURSE_INTERESTED'])){
            $courseItrsd = implode(",", $_POST['COURSE_INTERESTED']);
            $courseItrsdArr = $_POST['COURSE_INTERESTED'];
        }

        if ($this->input->post('MODULE_INTERESTED')) {
            $moduleItrsd = implode(",",$this->input->post('MODULE_INTERESTED'));
            $moduleItrsdArr = $_POST['MODULE_INTERESTED'];
        }

        if (empty($courseItrsdArr) && empty($moduleItrsdArr)) {
            $this->form_validation->set_rules('COURSE_VALIDATOR', 'Course Interested', 'required',array('required'=>'Please Select At least one module or one course'));
        }

        if ($this->form_validation->run() != FALSE){

            // function written for getting exam fees or processing fees using their respective course and module taken
            $get_processing_fees_or_exam_fees = $this->get_processing_fees_or_exam_fees($courseItrsdArr,$moduleItrsdArr);


            $min_fee_sum = $_POST['MINIMUM_FEES'];
            $max_fee_sum = $_POST['MAXIMUM_FEES'];

            $insertAdmission = array(
                'HANDLED_BY' => $_POST['ENQUIRY_HANDELED_BY'],
                'ADMISSION_DATE' => date("Y-m-d 00:00:00.000", strtotime(str_replace('/', '-', $_POST['ADMISSION_DATE']))),
                'MINIMUM_FEES'  =>  $min_fee_sum,
                'MAXIMUM_FEES'  => $max_fee_sum,
                'TOTALFEES' =>  $_POST['TOTALFEES'],
                'STREAM' => $_POST['STREAM'],
                'CENTRE_ID' => $data['enqData'][0]['CENTRE_ID'],
                'ADMISSION_TYPE' => $_POST['ADMISSION_TYPE'],
                'REFERENCE_GIVEN_BY' => $_POST['REFERENCE_GIVEN_BY'],
                'IN_TAKE'   => $_POST['IN_TAKE'],
                'STATUS'   => 'persuing',
                'UNIVERSITY_NAME' => $_POST['UNIVERSITY_NAME'],
                'CREATED_BY' => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
                'CREATED_DATE' => date("Y-m-d H:i:s"),
                'ISACTIVE'  => '1'
            );

            if($get_processing_fees_or_exam_fees['fees_type'] == 'exam_fees'){
                $insertAdmission['EXAM_FEES_APPLICABLE'] = $get_processing_fees_or_exam_fees['amount'];
            }
            else{
                $insertAdmission['PROCESSING_FEES_APPLICABLE'] = $get_processing_fees_or_exam_fees['amount'];
            }

            if(!empty($courseItrsd)){
                $insertAdmission['COURSE_TAKEN'] = $courseItrsd;
            }

            if (!empty($moduleItrsd)) {
                $insertAdmission['MODULE_TAKEN'] = $moduleItrsd;
            }

            if($id != '0'){
                $insertAdmission['ENQUIRY_ID'] = $enqId;
            }
            $insertAdmission['STUDY_CENTRE_ID'] = $data['enqData'][0]['CENTRE_ID'];
            $insertAdmission['1ST_YEAR'] = 0;
            $insertAdmission['2ND_YEAR'] = 0;
            $insertAdmission['3RD_YEAR'] = 0;

            $admissionIdsArray = $this->admission_model->addAdmission($insertAdmission);
            $encAdmissionId = $this->encrypt->encode($admissionIdsArray['mysql_admission_id']);
            // echo $encAdmissionId;
            $dob_rep = str_replace('/', '-', $_POST['ENQUIRY_DATEOFBIRTH']);
            $dob_date =  date("Y-m-d", strtotime($dob_rep) );

            if($id != '0'){
                $admissionData = array(
                    '0' =>
                    array(
                        'ENQUIRY_FIRSTNAME' => $_POST['ENQUIRY_FIRSTNAME'],
                        'ENQUIRY_LASTNAME' => $_POST['ENQUIRY_LASTNAME'],
                        'ENQUIRY_MIDDLENAME' => $_POST['ENQUIRY_MIDDLENAME'],
                        'ENQUIRY_ADDRESS1' => $_POST['ENQUIRY_ADDRESS1'],
                        'ENQUIRY_ADDRESS2' => $_POST['ENQUIRY_ADDRESS2'],
                        'ADMISSION_ID' => $admissionIdsArray['mysql_admission_id'],
                        'ENQUIRY_DATEOFBIRTH' => $dob_date,
                        'ENQUIRY_MOBILE_NO' =>  $_POST['ENQUIRY_MOBILE_NO'],
                        'ENQUIRY_PARENT_NO' =>  $_POST['ENQUIRY_PARENT_NO']) );

               $this->admission_model->updateAdmissionData($enqId,$admissionData,'add_admission');

                if(!empty($moduleItrsdArr)){
                    foreach($moduleItrsdArr as $key=>$value){
                        $module_data = $this->admission_model->getModuleCourseTransaction($moduleItrsdArr[$key]);

                        foreach($module_data as $key=>$value){
                            $moduleAdmissionCourseTransactionInsert = array(
                                //'ADMISSION_ID' => $admissionId,
                                'MODULE_ID' =>  $module_data[$key]['module_id'],
                                'COURSE_ID' =>  $module_data[$key]['course_id'],
                                'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
                                'MODIFIED_DATE' => date("Y-m-d")
                            );

                        $moduleLastId = $this->admission_model->admissionCourseTransaction($moduleAdmissionCourseTransactionInsert,$admissionIdsArray);
                        }
                    }
                }


                if(!empty($courseItrsdArr)){
                        foreach($courseItrsdArr as $key=>$value){
                            $courseAdmissionCourseTransactionInsert = array(
                                //'ADMISSION_ID' => $admissionId,
                                'COURSE_ID' =>  $courseItrsdArr[$key],
                                'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
                                'MODIFIED_DATE' => date("Y-m-d")
                            );
                            $courseLastId = $this->admission_model->admissionCourseTransaction($courseAdmissionCourseTransactionInsert,$admissionIdsArray);

                        }
                }

                $admRecInsArr = array(
                    ''
                    );

                $info = getdate();
                $date = $info['mday'];
                $month = $info['mon'];
                $year = $info['year'];
                $hour = $info['hours'];
                $min = $info['minutes'];
                $sec = $info['seconds'];
                $current_date = "$year-$month-$date";
                $full_current_date = "$year-$month-$date $hour:$min:$sec";

                $studentDesk = array(
                    //'ADMISSION_ID' => $admissionId,
                    //'USERNAME'  => $admissionId,
                    'PASSWORD' => $this->load->encrypt_decrypt_data('welcome',"encrypt"),
                    'ROLE_ID' => '1',
                    'IS_ACTIVE' => '1',
                    'CREATED_ON' => $full_current_date,
                    'LAST_MODIFIED' => $full_current_date
                    );
                $this->admission_model->studentDeskUsers($studentDesk,$admissionIdsArray);

            }


        $downPaymentdate =  date('Y-m-d');

        $insDateRep = str_replace('/', '-', $_POST['INSTALLMENT_DATE']);
        $insDate = date("Y-m-d", strtotime($insDateRep));

        $totalFessCalc = $_POST['TOTALFEES'] - $_POST['DOWNPAYMENT'];
        $totalFeesDivide = ceil($totalFessCalc/$_POST['MONTHLY_PAY']) - 1;

        $lastInstallment = $totalFessCalc - $_POST['MONTHLY_PAY'] * $totalFeesDivide;

        $courseInstallment = array();
        $courseInstallment[] = $_POST['DOWNPAYMENT'];
        $installmentDate[] = $downPaymentdate;
        $numOfInstallments = 1;
        // $installmentDate = '';
        for ($i=0; $i<$totalFeesDivide ; $i++) {
            // echo $i;
            $courseInstallment[] = $_POST['MONTHLY_PAY'];
            $installmentDate[] =  date("Y-m-d",strtotime("+".$i." month", strtotime($insDate)));
            // $installmentDate .=  date("Y-m-d",strtotime("+1 month", strtotime($date_rep))).",";

            $numOfInstallments++;
        }

        for ($j=0; $j <count($installmentDate); $j++) {
            $addAdmissionInstallmentTransaction = array(
            //'ADMISSION_ID' => $admissionId,
            'DUEDATE' => $installmentDate[$j],
            'DUE_AMOUNT' => $courseInstallment[$j],
            'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
            'MODIFIED_DATE' => date("Y-m-d"),
            'ISACTIVE' => '1'
            );
            $this->admission_model->addAdmissionInstallmentTransaction($addAdmissionInstallmentTransaction,$admissionIdsArray);
        }

        $course_enrolled = "";
        $module_enrolled = "";
        // for getting selected course on mail
        foreach($courseItrsdArr as $key=>$value){

            foreach ($data['courses'] as $course) {
                if($courseItrsdArr[$key] == $course['COURSE_ID']){
                    $course_enrolled .= $course['COURSE_NAME'].',';
                }
            }
        }

        foreach($moduleItrsdArr as $key=>$value){

            foreach ($data['modules'] as $module) {
                if($moduleItrsdArr[$key] == $module['MODULE_ID']){
                    $module_enrolled .= $module['MODULE_NAME'].',';
                }
            }
        }

        $final_course_taken = trim($module_enrolled.$course_enrolled,",");

        // $data['enqData'][0]['CENTRE_ID']

            if($encAdmissionId != NULL){
                // added by ankur on 18 jan 2018

                  // for mail send to enquiry
                  // we need to check all values for mail sending in mailer function
                    $from = "erp@suntechedu.com";

                    $email_centre_mail_id = $this->admission_model->get_centre_mail_ids($data['enqData'][0]['CENTRE_ID']);
                    $to = $email_centre_mail_id;

                    $cc = array('ankur@suntechedu.com','rajesh@suntechedu.com','vijay@suntechedu.com');

                    $employee_name = $this->tele_enquiry_model->getEmployeeNameFromEmployeeId($_POST['ENQUIRY_HANDELED_BY']);
                    $subject = "Admission Details of ".$data['enqData'][0]['CENTRE_NAME']." centre Dated on ".$_POST['ADMISSION_DATE'];
                    $body = "<h2>Admission Details</h2>
                            <table border='1' bordercolor='black' cellspacing='0' cellpadding='3'>
                            <tr><td>Admission Name</td><td>: " .$_POST['ENQUIRY_FIRSTNAME']." ".$_POST['ENQUIRY_MIDDLENAME']." ".$_POST['ENQUIRY_LASTNAME']. "</td></tr>
                            <tr><td>Admission Date</td><td>: " .$_POST['ADMISSION_DATE']. "</td></tr>
                            <tr><td>Admission Handled By</td><td>: " .$employee_name. "</td></tr>
                            <tr><td>Stream</td><td>: ".$_POST['STREAM']."</td></tr>
                            <tr><td>Course Taken</td><td>: ".$final_course_taken."</td></tr>
                            <tr><td>Total Fees</td><td>: ".$_POST['TOTALFEES']."</td></tr>
                            <tr><td>Intake Month</td><td>: ".$_POST['IN_TAKE']."</td></tr>
                            <tr><td>Academic Year</td><td>: ".$_POST['ACAD_YEAR']."</td></tr>
                            <tr><td>Email ID</td><td>: ".$_POST['ENQUIRY_EMAIL']."</td></tr>
                            <tr><td>DOB</td><td>: ".$_POST['ENQUIRY_DATEOFBIRTH']."</td></tr>
                            <tr><td>Contact No</td><td>: ".$_POST['ENQUIRY_MOBILE_NO']."</td></tr>
                            <tr><td>Source</td><td>: ".$data['enqData'][0]['Source_name']."</td></tr>
                            </table>
                            <p>* Please do not reply to this mail. This is automated generated email.<br><br>For more information visit on <a href='http://suntechedu.com/'>suntechedu.com</a></p>";


                    $confirmationMail = $this->load->mailer($from,$to,$cc,"","",$subject,$body,"admission");
                    // end of the mailer function

                  // end of mailer code



                redirect(base_url('admissions/admission-installment/'.$encAdmissionId));
            }

        }
        $this->load->admin_view('add_admission',$data);
     }

     public function editAdmission($id){
        $admId = $this->encrypt->decode($id);
        //echo $admId;
        $data['admissionData'] = $this->admission_model->studentAdmissionData($admId);

        $enqId = $data['admissionData'][0]['ENQUIRY_ID'];
        $data['courses'] = $this->tele_enquiry_model->course_interested();
        $data['modules'] = $this->tele_enquiry_model->module_interested();
        $data['educations'] = $this->enquiry_model->getEducation();
        $data['employees'] = $this->employee_model->getEmployeeNames($data['admissionData'][0]['HANDLED_BY']);
        $data['occupations'] = $this->enquiry_model->getOccupation();
        $data['add_bel_cust_js'] = base_url('resources/js/admission_cust.js');

        $this->form_validation->set_rules('ENQUIRY_FIRSTNAME', 'First Name', 'required');
        $this->form_validation->set_rules('ENQUIRY_LASTNAME', 'Last Name', 'required');
        $this->form_validation->set_rules('TOTALFEES', 'Total Fees', 'required');
        $this->form_validation->set_rules('ENQUIRY_EMAIL', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('ENQUIRY_DATEOFBIRTH', 'D.O.B', 'required');

        if(isset($_POST['ENQUIRY_EDUCATION'])){
            if($_POST['ENQUIRY_EDUCATION'] == ''){
                $this->form_validation->set_rules('ENQUIRY_EDUCATION', 'Education', 'required');
            }
        }
        $this->form_validation->set_rules('ENQUIRY_MOBILE_NO', 'Mobile No.', 'required|min_length[10]|max_length[10]|numeric');
        $this->form_validation->set_rules('ENQUIRY_PARENT_NO', 'Parent No.', 'required|min_length[10]|max_length[10]|numeric');
        $this->form_validation->set_rules('ENQUIRY_HANDELED_BY', 'Enquiry Handeled By', 'required');
        $this->form_validation->set_rules('IN_TAKE', 'In Take', 'required');
        $this->form_validation->set_rules('ACAD_YEAR', 'Academic Year', 'required');
        $this->form_validation->set_rules('REMARKS', 'Mention Remarks', 'required');
        if(isset($_POST['UNIVERISTY_NAME'])){
            if($_POST['UNIVERISTY_NAME'] == ''){
                $this->form_validation->set_rules('UNIVERISTY_NAME', 'University Name', 'required');
            }
        }
        $courseItrsdArr = array();
        $moduleItrsdArr = array();
        if(isset($_POST['COURSE_INTERESTED'])){
            $courseItrsd = implode(",", $_POST['COURSE_INTERESTED']);
            $courseItrsdArr = $_POST['COURSE_INTERESTED'];
        }
        if ($this->input->post('MODULE_INTERESTED')) {
            $moduleItrsd = implode(",",$this->input->post('MODULE_INTERESTED'));
            $moduleItrsdArr = $_POST['MODULE_INTERESTED'];
        }


        if ($this->form_validation->run() != FALSE){
            $admDateRep = str_replace('/', '-', $_POST['ADMISSION_DATE']);
            $admDate =  date("Y-m-d H:i:s", strtotime($admDateRep));

            $updateAdmission = array(
                'ADMISSION_DATE' => $admDate,
                'MINIMUM_FEES'  =>  $_POST['MINIMUM_FEES'],
                'MAXIMUM_FEES'  => $_POST['MAXIMUM_FEES'],
                'TOTALFEES' =>  $_POST['TOTALFEES'],
                'STREAM' => $_POST['STREAM'],
                'ADMISSION_TYPE' => $_POST['ADMISSION_TYPE'],
                'REFERENCE_GIVEN_BY' => $_POST['REFERENCE_GIVEN_BY'],
                'IN_TAKE'   => $_POST['IN_TAKE'],
                'YEAR'  =>  $_POST['ACAD_YEAR'],
                'UNIVERSITY_NAME' => $_POST['UNIVERSITY_NAME'],
                'REMARKS' => $_POST['REMARKS'],
                'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
                'MODIFIED_DATE' => date("Y-m-d")
            );

            if(!empty($courseItrsd)){
                $updateAdmission['COURSE_TAKEN'] = $courseItrsd;
            }

            if (!empty($moduleItrsd)) {
                $updateAdmission['MODULE_TAKEN'] = $moduleItrsd;
            }

            $admissionId = $this->admission_model->updateAdmission($admId,$updateAdmission);
            // echo $encAdmissionId;
            $dob_rep = str_replace('/', '-', $_POST['ENQUIRY_DATEOFBIRTH']);
            $dob_date =  date("Y-m-d H:i:s", strtotime($dob_rep) );

            if($id != '0'){
                $admissionData = array(
                    'ENQUIRY_FIRSTNAME' => $_POST['ENQUIRY_FIRSTNAME'],
                    'ENQUIRY_LASTNAME' => $_POST['ENQUIRY_LASTNAME'],
                    'ENQUIRY_MIDDLENAME' => $_POST['ENQUIRY_MIDDLENAME'],
                    'ENQUIRY_ADDRESS1' => $_POST['ENQUIRY_ADDRESS1'],
                    'ENQUIRY_ADDRESS2' => $_POST['ENQUIRY_ADDRESS2'],
                    'ADMISSION_ID' => $admissionId,
                    'ENQUIRY_DATEOFBIRTH' => $dob_date,
                    'ENQUIRY_MOBILE_NO' =>  $_POST['ENQUIRY_MOBILE_NO'],
                    'ENQUIRY_PARENT_NO' =>  $_POST['ENQUIRY_PARENT_NO'],
                    'ENQUIRY_EMAIL' =>  $_POST['ENQUIRY_EMAIL'],
                    'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
                    'MODIFIED_DATE' => date("Y-m-d")
                    );
                $this->admission_model->updateAdmissionData($enqId,$admissionData,'update_admission');
                // -----------------------------------------------------------------
                if(!empty($moduleItrsdArr)){
                    foreach($moduleItrsdArr as $key=>$value){
                        $module_data = $this->admission_model->getModuleCourseTransaction($moduleItrsdArr[$key]);

                        foreach($module_data as $key=>$value){
                            $moduleAdmissionCourseTransactionInsert = array(
                                'ADMISSION_ID' => $admId,
                                'MODULE_ID' =>  $module_data[$key]['module_id'],
                                'COURSE_ID' =>  $module_data[$key]['course_id'],
                                'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
                                'MODIFIED_DATE' => date("Y-m-d")
                            );

                        $moduleLastId = $this->admission_model->updateAdmissionCourseTransaction($admId,$moduleAdmissionCourseTransactionInsert);
                        }
                    }
                }


                if(!empty($courseItrsdArr)){
                        foreach($courseItrsdArr as $key=>$value){
                            $courseAdmissionCourseTransactionInsert = array(
                                'ADMISSION_ID' => $admId,
                                'COURSE_ID' =>  $courseItrsdArr[$key],
                                'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
                                'MODIFIED_DATE' => date("Y-m-d")
                            );
                            $courseLastId = $this->admission_model->updateAdmissionCourseTransaction($admId,$courseAdmissionCourseTransactionInsert);

                        }
                }
                $info = getdate();
                $date = $info['mday'];
                $month = $info['mon'];
                $year = $info['year'];
                $hour = $info['hours'];
                $min = $info['minutes'];
                $sec = $info['seconds'];
                $current_date = "$year-$month-$date";
                $full_current_date = "$year-$month-$date $hour:$min:$sec";

            }

        if(($_POST['DOWNPAYMENT_DATE'] != "") && ($_POST['TOTALFEES'] != "") && ($_POST['DOWNPAYMENT'] != "") && ($_POST['MONTHLY_PAY'] != "")){
          $date_rep = str_replace('/', '-', $_POST['DOWNPAYMENT_DATE']);
          $downPaymentdate =  date("Y-m-d H:i:s", strtotime($date_rep) );
          // -------------------------------------------------------------
          $insDateRep = str_replace('/', '-', date('d/m/Y'));
          $insDate = date("Y-m-d", strtotime($insDateRep));

          $totalFessCalc = $_POST['TOTALFEES'] - $_POST['DOWNPAYMENT'];
          $totalFeesDivide = ceil($totalFessCalc/$_POST['MONTHLY_PAY']) - 1;

          $lastInstallment = $totalFessCalc - $_POST['MONTHLY_PAY'] * $totalFeesDivide;

          $courseInstallment = array();
          $courseInstallment[] = $_POST['DOWNPAYMENT'];
          $installmentDate[] = $downPaymentdate;
          $numOfInstallments = 1;
          // $installmentDate = '';
          for ($i=0; $i<$totalFeesDivide ; $i++) {
              // echo $i;
              $courseInstallment[] = $_POST['MONTHLY_PAY'];
              $installmentDate[] =  date("Y-m-d",strtotime("+".$i." month", strtotime($insDate)));
              // $installmentDate .=  date("Y-m-d",strtotime("+1 month", strtotime($date_rep))).",";

              $numOfInstallments++;
          }

          for ($j=0; $j <count($installmentDate); $j++) {
              $addAdmissionInstallmentTransaction = array(
              'ADMISSION_ID' => $admId,
              'DUEDATE' => $installmentDate[$j],
              'DUE_AMOUNT' => $courseInstallment[$j],
              'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
              'MODIFIED_DATE' => date("Y-m-d"),
              'ISACTIVE' => '1'
              );
              $this->admission_model->updateAdmissionInstallmentTransaction($admId,$addAdmissionInstallmentTransaction);
          }
        }
        else{

        }
         redirect(base_url('admissions/admission-installment/'.$encAdmissionId));
        }
        $this->load->admin_view('edit_admission',$data);
     }

    public function get_processing_fees_or_exam_fees($courseItrsdArr,$moduleItrsdArr){
        $processing_fees = "";
        $exam_fees = "";

        $process_or_exam_fees = array();

        if(count($moduleItrsdArr)){
            foreach($moduleItrsdArr as $modules){
                $module_other_fees_details = $this->admission_model->get_module_or_course_other_fees($modules,'module');
                if(($module_other_fees_details[0]['PROCESSING_FEES_AMOUNT'] != 0) && ($module_other_fees_details[0]['PROCESSING_FEES_AMOUNT'] != null)){
                    $processing_fees += $module_other_fees_details[0]['PROCESSING_FEES_AMOUNT'];
                }
                else if(($module_other_fees_details[0]['EXAM_FEES_AMOUNT'] != 0) && ($module_other_fees_details[0]['EXAM_FEES_AMOUNT'] != null)){
                    $exam_fees += $module_other_fees_details[0]['EXAM_FEES_AMOUNT'];
                }
            }

            if($processing_fees != 0){
                $process_or_exam_fees = array('amount'=>$processing_fees,'fees_type'=>'processing_fees');
            }
            else{
                if($exam_fees > 2000){
                    $exam_fees = 2000;
                }
                $process_or_exam_fees = array('amount'=>$exam_fees,'fees_type'=>'exam_fees');
            }

        }
        else if(count($courseItrsdArr)){
            foreach($courseItrsdArr as $courses){
                $course_other_fees_details = $this->admission_model->get_module_or_course_other_fees($courses,'course');
                if(($course_other_fees_details[0]['EXAM_FEES_AMOUNT'] != 0) && ($course_other_fees_details[0]['EXAM_FEES_AMOUNT'] != null)){
                    $exam_fees += $course_other_fees_details[0]['EXAM_FEES_AMOUNT'];
                }
            }

            if($exam_fees != 0){
                if($exam_fees > 2000){
                    $exam_fees = 2000;
                }
                $process_or_exam_fees = array('amount'=>$exam_fees,'fees_type'=>'exam_fees');
            }

        }

        return $process_or_exam_fees;
    }

    public function discounts(){
        $discounts = $this->admission_model->getDiscount();
        echo json_encode($discounts);
    }

    public function courseFees(){
        $moduleIds = explode(",", $this->input->post('mids'));
        $courseIds = explode(",", $this->input->post('cids'));
        $courseFees = array();
        $moduleFees = array();
        if (!empty($courseIds)) {
            $courseFees = $this->tele_enquiry_model->course_interested($courseIds);
        }
        if(!empty($moduleIds)){
            $moduleFees = $this->tele_enquiry_model->module_interested($moduleIds);
        }

        $totalFees['courseFees'] = $courseFees;
        $totalFees['moduleFees'] = $moduleFees;
        echo json_encode($totalFees);
    }

    public function admissionInstallment($admId){
        $decAdmissionId = $this->encrypt->decode($admId);
        $page_type = "";
        if($this->input->post('type') != ""){
            $page_type = $this->input->post('type');
        }
        $data['admission_data'] = $this->admission_model->getAdmissionData($decAdmissionId);
        $data['courses'] = $this->tele_enquiry_model->course_interested();
        $data['modules'] = $this->tele_enquiry_model->module_interested();
        $data['employees'] = $this->employee_model->getEmployeeNamesAsPerCentreLogin();
        $data['payFor'] = $this->admission_model->payFor();
        $data['receipts'] = $this->admission_model->getOtherReceiptDetailsByAdmissionId($decAdmissionId);
        $data['add_bel_cust_js'] = base_url('resources/js/admission_cust.js');
        if($page_type == "view_admission"){
            echo json_encode($data);
        }
        else{
            $this->load->admin_view('admission_installment_view',$data);
        }
    }

    public function deleteAdmission($admId){
        $decAdmissionId = $this->encrypt->decode($admId);
        $active_status = $this->admission_model->deleteAdmission($decAdmissionId);
        // $this->session->set_flashdata('danger',"1 Record Deleted Successfully.");
        // redirect(base_url('admission/view-admission'));
        $data['status'] = $active_status;
        echo json_encode($data);
    }

    public function getAdmissionReceipt(){
        $encAdmId = $this->input->post('admId');
        $admId = $this->encrypt->decode($encAdmId);
        $installmentPlan = $this->admission_model->getAdmissionInstallmentTransaction($admId);


        $installmentPlanArrDate = array();
        $installmentPlanArrAmount = array();
        $installmentPlanArr = array();

            $TotalamtTobePaid =0;

        if(count($installmentPlan)){
            foreach($installmentPlan as $key => $value){
                array_push($installmentPlanArrDate,$installmentPlan[$key]['DUEDATE']);

                array_push($installmentPlanArrAmount,$installmentPlan[$key]['DUE_AMOUNT']);
            }

            $curDate = date('Y-m-d');


            // echo "<pre>";
            // print_r($installmentPlanArrAmount);
            // echo "</pre>";

            for ($i=0; $i <count($installmentPlanArrDate) ; $i++) {
                $installmentPlanArr[] = array(
                        'DUEDATE' => $installmentPlanArrDate[$i],
                        'DUE_AMOUNT' => $installmentPlanArrAmount[$i]
                    );

                if ($installmentPlanArrDate[$i]<$curDate)
                {
                    $mostRecent[] = $installmentPlanArrDate[$i];
                    $TotalamtTobePaid = $TotalamtTobePaid+$installmentPlanArrAmount[$i];
                }

            }

            $curMonthYear = date("m-Y");

            foreach ($installmentPlanArr as $insPlan) {

                $insDate = date("Y-m-d", strtotime($insPlan['DUEDATE']));

                $insMonthYear = date("m-Y", strtotime($insPlan['DUEDATE']));


                if ($curMonthYear == $insMonthYear) {
                    $insMonthIncDays = date("Y-m-d",strtotime("+3 day", strtotime($insDate)));

                    if($insMonthIncDays >= $curMonthYear){
                        $admReceipt['expDate'] = '1';
                    }
                    else{
                        $admReceipt['expDate'] = '0';
                    }
                }
            }
        }

        $getAdmRec = $this->admission_model->getAdmissionReceipt($admId);
        $totAmtPaid =0;
        for ($i=0; $i <count($getAdmRec) ; $i++) {
            $getAdmRec[$i]['ADMISSION_RECEIPTS_ID_ENC'] = $this->encrypt->encode($getAdmRec[$i]['ADMISSION_RECEIPTS_ID']);
                $totAmtPaid = $totAmtPaid + $getAdmRec[$i]['AMOUNT_PAID'];

        }

        if(isset($mostRecent))
        {
            $mostRecent = max($mostRecent);
            $DateAfterThreeDay = date( "Y-m-d", strtotime( "$mostRecent +3 day" ) );
            $curDate = new DateTime($curDate);
            $date2 = new DateTime($DateAfterThreeDay);
            $diff = $date2->diff($curDate)->format("%a");
        }

        if($totAmtPaid<$TotalamtTobePaid)
        {
            // $penalty = 100*$diff;   disabled for some days
            $penalty ='0';
        }
        else
        {
            $penalty ='0';
        }

        $admReceipt['admReceipt'] = $getAdmRec;
        $admReceipt['insPlan'] = $installmentPlanArr;
        $admReceipt['penalty'] = $penalty;
        echo json_encode($admReceipt);
    }

    public function saveAdmissionReceipt(){
        $admId = $this->input->post('admId');
        $decAdmId = $this->encrypt->decode($admId);
        $receiptData = $this->admission_model->getAdmissionReceiptData($decAdmId);

        // $payDate = date('Y-m-d');
        $recPayDate = str_replace('/', '-', $this->input->post('payDate'));
        $payDate = date("Y-m-d", strtotime($recPayDate));

        // $amtNoSt = round($this->input->post('TOTALFEES')/1.18);
        $amtNoSt = $this->input->post('TOTALFEES');
        $servTax = 0;

        $paymentType = $this->input->post('payType');
        switch ($this->input->post('payType')) {
            case '1':
                $payType = "C";
                break;

            case '2':
            case '3':
                $payType = 'Q';
                break;

            default:
                $payType = '';
                break;
        }

        $this->form_validation->set_rules('TOTALFEES', 'Total Fees', 'required');
        $this->form_validation->set_rules('RECEIVED_BY', 'Received By', 'required');

        if ($this->input->post('payType') == '2' || $this->input->post('payType') == '3'){
            $this->form_validation->set_rules('BANK_NAME', 'Bank Name', 'required');
            $this->form_validation->set_rules('BANK_BRANCH', 'Bank Branch', 'required');
            $this->form_validation->set_rules('CHEQUE_DATE', 'Cheque Date', 'required');
            $this->form_validation->set_rules('CHEQUE_NO', 'Cheque/DD/TID No', 'required');
        }

        $msg = array();
        if ($this->form_validation->run() != FALSE){
            $recCount = $this->admission_model->getReceiptCount($receiptData[0]['CENTRE_ID'],$paymentType,'admission_receipt');

            // $newReceiptCount = $this->admission_model->getMainReceiptCount($receiptData[0]['CENTRE_ID'],$paymentType) + 1;
            $admRecNo = $payType.'-'.$receiptData[0]['CENTRE_ID'].'-'.($recCount + 1);
            $logUserData = $this->session->userdata('admin_data');

            $insertReceiptArray = array(
                'ADMISSION_ID' => $receiptData[0]['ADMISSION_ID'],
                'PAYMENT_TYPE'  =>  $this->input->post('payType'),
                'AMOUNT_PAID'   =>  $this->input->post('TOTALFEES'),
                'AMOUNT_PAID_FEES' => $amtNoSt,
                'AMOUNT_PAID_SERVICETAX' => $servTax,
                'PAYMENT_DATE' => $payDate,
                'CENTRE_ID' =>  $receiptData[0]['CENTRE_ID'],
                'CENTRE_RECEIPT_ID' =>  ($recCount + 1),
                'RECEIVED_BY' =>  $this->input->post('RECEIVED_BY'),
                'ADMISSION_RECEIPT_NO' => $admRecNo,
                'UPDATED_BY' => $logUserData[0]['USER_ID'],
                'UPDATE_DATE' => $payDate,
                'SERVICE_TAX' => '',
                'AMOUNT_DEPOSITED' => $this->input->post('TOTALFEES'),
                'DATE_OF_DEPOSIT' => date('Y-m-d H:i:s'),
                'DEPOSIT_SLIP_NO' => $receiptData[0]['ADMISSION_ID'],
                'CREATED_ON' => date('Y-m-d H:i:s'),
                'CREATED_BY' => $logUserData[0]['USER_ID'],
                'MODIFIED_BY' => $logUserData[0]['USER_ID'],
                'MODIFIED_DATE' => date('Y-m-d H:i:s')
                );

            if ($this->input->post('payType') == '2' || $this->input->post('payType') == '3') {
                $admDateRep = str_replace('/', '-', $this->input->post('CHEQUE_DATE'));
                $insertReceiptArray['CHEQUE_DATE'] = date("Y-m-d", strtotime($admDateRep));
                $insertReceiptArray['CHEQUE_NO'] = $this->input->post('CHEQUE_NO');
                $insertReceiptArray['CHEQUE_BANK_NAME'] = $this->input->post('BANK_NAME');
                $insertReceiptArray['CHEQUE_BRANCH'] = $this->input->post('BANK_BRANCH');
            }

            $msg['admRecData'] = $this->admission_model->saveAdmissionReceiptData($insertReceiptArray);

            for ($i=0; $i <count($msg['admRecData']) ; $i++) {
                $msg['admRecData'][$i]['ADMISSION_RECEIPTS_ID_ENC'] = $this->encrypt->encode($msg['admRecData'][$i]['ADMISSION_RECEIPTS_ID']);

            }

            $totAmtPaid = $this->admission_model->totAmtPaid($decAdmId);
            $totAmtCalc = $msg['admRecData'][0]['TOTALFEES'] - $totAmtPaid;
            $msg['admRecData'][0]['BALANCE'] = $totAmtCalc;
            // $msg = '1';
        }
        else{
            if(validation_errors() != false){
                $msg['errors'][] = $this->form_validation->error_array();
            }
        }

            echo json_encode($msg, JSON_UNESCAPED_SLASHES);
    }

    public function admissionReciptPrint($admRecId){
        $decAdmRecId = $this->encrypt->decode($admRecId);

        $data['admReceipts'] = $this->admission_model->printAdmissionReceipt($decAdmRecId);

        $admId = $data['admReceipts'][0]['ADMISSION_ID'];
        $admRecNo = $data['admReceipts'][0]['ADMISSION_RECEIPT_NO'];
        $course_id = explode(",", $data['admReceipts'][0]['COURSE_TAKEN']);
        $payDate = $data['admReceipts'][0]['PAYMENT_DATE'];

        $data['maxPaidFees'] = $this->admission_model->admissionReceiptMaxPaid($admRecNo);
        $data['courses'] = $this->tele_enquiry_model->course_interested($course_id);
        $data['insPlan'] = $this->admission_model->getAdmissionInstallmentTransaction($admId);

        $data['amtPaid'] = $this->admission_model->amtPaidTillDate($admId,$payDate);

        $this->load->helper('currency');

        $this->load->helper('pdf_helper');
        $this->load->view('pdfreport',$data);
    }

    public function addOtherReceipts(){
        $admId = $this->input->post('admId');
        $decAdmId = $this->encrypt->decode($admId);
        $receiptData = $this->admission_model->getAdmissionReceiptData($decAdmId);

        // $payDate = date('Y-m-d');
        $recPayDate = str_replace('/', '-', $this->input->post('payDate'));
        $payDate = date("Y-m-d", strtotime($recPayDate));

        $amtNoSt = $this->input->post('TOTALFEES');
        $servTax = '';

        $paymentType = $this->input->post('payType');
        switch ($this->input->post('payType')) {
            case '1':
                $payType = "C";
                break;
            case '2':
                $payType = 'Q';
                break;
            case '3':
                $payType = 'Q';
                break;
            default:
                $payType = '';
                break;
        }

        $this->form_validation->set_rules('TOTALFEES', 'Total Fees', 'required');
        $this->form_validation->set_rules('RECEIVED_BY', 'Received By', 'required');

        if ($this->input->post('payType') == '2' || $this->input->post('payType') == '3'){
            $this->form_validation->set_rules('BANK_NAME', 'Bank Name', 'required');
            $this->form_validation->set_rules('BANK_BRANCH', 'Bank Branch', 'required');
            $this->form_validation->set_rules('CHEQUE_DATE', 'Cheque Date', 'required');
            $this->form_validation->set_rules('CHEQUE_NO', 'Cheque/DD/TID No', 'required');
        }

        $msg = array();
        if ($this->form_validation->run() != FALSE){
            $recCount = $this->admission_model->getReceiptCount($receiptData[0]['CENTRE_ID'],$paymentType,'other_receipt');
            // $cenRecId = $payType.'-'.$receiptData[0]['CENTRE_ID'].'-'.$recCount;
            // echo "receipt count is : ".$recCount;
            $cenRecId = $payType.'-'.$receiptData[0]['CENTRE_ID'].'-'.($recCount + 1);
            // echo $cenRecId;
            // exit();
            $logUserData = $this->session->userdata('admin_data');

            $insertReceiptArray = array(
                'ADMISSION_ID' => $receiptData[0]['ADMISSION_ID'],
                'PAYMENT_TYPE'  =>  $this->input->post('payType'),
                'AMOUNT'   =>  $this->input->post('TOTALFEES'),
                'AMOUNT_WITHOUT_ST' => $amtNoSt,
                'SERVICE_TAX_PAID' => $servTax,
                'PAYMENT_DATE' => $payDate,
                'PARTICULAR' => $this->input->post('payFor'),
                'CENTRE_ID' =>  $receiptData[0]['CENTRE_ID'],
                'RECEIVED_BY' =>  $this->input->post('RECEIVED_BY'),
                'CENTRE_RECEIPT_ID' => ($recCount + 1),
                'PENALTY_RECEIPT_NO' => $cenRecId,
                'UPDATED_BY' => $logUserData[0]['USER_ID'],
                'UPDATE_DATE' => $payDate,
                'SERVICE_TAX' => '18',
                'CREATED_ON' => date('Y-m-d H:i:s')
                // 'CREATED_BY' => $logUserData[0]['USER_ID']
                );

            $course_id = $this->input->post('COURSES');
            $courseType = substr($course_id, strpos($course_id, "-") + 1);
            $courseName = substr($course_id, 0, strpos($course_id, '-'));

            if ($courseType == '1') {
                $insertReceiptArray['EXAM_COURSE_ID'] = $courseName;
            }
            if ($courseType == '2') {
                $insertReceiptArray['EXAM_MODULE_ID'] = $courseName;
            }

            if ($this->input->post('payType') == '2' || $this->input->post('payType') == '3') {
                $admDateRep = str_replace('/', '-', $this->input->post('CHEQUE_DATE'));
                $insertReceiptArray['CHEQUE_DATE'] = date("Y-m-d", strtotime($admDateRep));
                $insertReceiptArray['CHEQUE_NO'] = $this->input->post('CHEQUE_NO');
                $insertReceiptArray['BANK_NAME'] = $this->input->post('BANK_NAME');
                $insertReceiptArray['BRANCH'] = $this->input->post('BANK_BRANCH');
            }
            $particular = $this->admission_model->payFor($this->input->post('payFor'));
            $admRecId = $this->admission_model->addOtherReceipts($insertReceiptArray);
            $insertReceiptArray['admRecId'] = $this->encrypt->encode($admRecId);
            $insertReceiptArray['PARTICULAR'] = $particular[0]['CONTROLFILE_VALUE'];
            // echo "<pre>";
            // print_r($insertReceiptArray);
            // echo "</pre>";
            $msg['admRecData'][] = $insertReceiptArray;
        }
        else{
            if(validation_errors() != false){
                $msg['errors'][] = $this->form_validation->error_array();
            }
        }

        echo json_encode($msg, JSON_UNESCAPED_SLASHES);
    }

    public function otherRecData(){
        $encAdmId = $this->input->post('admId');
        $admId = $this->encrypt->decode($encAdmId);

        $otherRecData['payFor'] = $this->admission_model->payFor();
        $getOthRec = $this->admission_model->getOtherReceiptsData($admId);

        for ($i=0; $i <count($getOthRec) ; $i++) {
            $encPenaltyRecId = $this->encrypt->encode($getOthRec[$i]['PENALTY_RECEIPT_ID']);
            $getOthRec[$i]['PENALTY_RECEIPT_ID'] = $encPenaltyRecId;
        }

        $getAdmCourses = $this->admission_model->getAdmCourses($admId);
        $otherRecData['exmCourse'] = array();
        $data =  array();
        foreach($getAdmCourses as $key => $value){
            $data = $this->admission_model->getCourseNameByCourseId($getAdmCourses[$key]['COURSE_ID']);
            array_push($otherRecData['exmCourse'],$data[0]);
        }
        // $getAdmCoursesIdArr = explode(",", $getAdmCourses[0]['COURSE_TAKEN']);
        // $exmCourse = $this->admission_model->getExamCourse($getAdmCoursesIdArr);

        // $getAdmModulesIdArr = explode(",", $getAdmCourses[0]['MODULE_TAKEN']);
        // $exmModule = $this->admission_model->getExamCourse($getAdmModulesIdArr);
        // echo "<pre>";
        // print_r($exmModule);
        // echo "</pre>";
        // exit();
        // $otherRecData['exmCourse'] = $exmCourse;
        $otherRecData['exmModule'] = "";
        $otherRecData['othRecData'] = $getOthRec;

        // echo "<pre>";
        // print_r($otherRecData['exmCourse']);
        // echo "</pre>";
        // exit();

        echo json_encode($otherRecData);
    }

    public function otherRecPrint($encPrId){
        $penaltyId = $this->encrypt->decode($encPrId);
        $data['penaltyRecData'] = $this->admission_model->getOthRecPrintData($penaltyId);
        if ($data['penaltyRecData'][0]['EXAM_COURSE_ID'] != null) {
            $course = $this->admission_model->getExamCourse($data['penaltyRecData'][0]['EXAM_COURSE_ID']);
            // $courseName = $course[0]['COURSE_NAME'];
            $data['penaltyRecData'][0]['COURSE'] = $course[0]['COURSE_NAME'];
        }
        if ($data['penaltyRecData'][0]['EXAM_MODULE_ID'] != null) {
            $module = $this->admission_model->getExamModule($data['penaltyRecData'][0]['EXAM_MODULE_ID']);
            $data['penaltyRecData'][0]['COURSE'] = $course[0]['MODULE_NAME'];
        }
        // echo "<pre>";
        // print_r($data['penaltyRecData']);
        // echo "</pre>";
        // echo $penaltyId;
        $this->load->helper('currency');
        $this->load->helper('pdf_helper');
        $this->load->view('otherRecPrint',$data);
    }

    public function editInstallment($admId){
        $decAdmissionId = $this->encrypt->decode($admId);
        $data['admission_data'] = $this->admission_model->getAdmissionData($decAdmissionId);
        $data['add_bel_cust_js'] = base_url('resources/js/admission_cust.js');
        $this->load->admin_view('edit_installment',$data);
    }

    public function addUpdateInstallment(){
        $admInstId = $this->input->post('admInstId');
        $admId = $this->input->post('admId');
        $admissionData = $this->admission_model->getAdmissionData($admId);
        $lastAdmission =  count($admissionData) - 1;


        $dueDate_replace = str_replace('/', '-', $this->input->post('due_date'));
        $dueDate = date("Y-m-d", strtotime($dueDate_replace));
        $balanceInstallmentCalc = $admissionData[0]['TOTALFEES'] - $admissionData[0]['DUE_AMOUNT'];
        $installmentCalcAuto = $balanceInstallmentCalc;
        // echo $balanceInstallmentCalc;
        for ($i=1; $i <count($admissionData) ; $i++) {
            $installmentCalcAuto = $installmentCalcAuto - $admissionData[$i]['DUE_AMOUNT'];

        }
        if($this->input->post('due_amount') == $installmentCalcAuto){
            echo json_encode("error");
            exit();
        }

        $installmentData = array(
            'DUEDATE' => $dueDate,
            'DUE_AMOUNT' => $this->input->post('due_amount'),
            'ISACTIVE'  =>  '1'
        );
        if ($admInstId != NULL) {
            $this->admission_model->updateAdmissionInstallment($admInstId, $installmentData);
        }
        else{
            $installmentData['ADMISSION_ID'] = $admId;
            $this->admission_model->addAdmissionInstallment($installmentData);
        }

        echo json_encode("Data Saved Succefully");
    }

    public function delInstallment(){
        $admInstId = $this->input->post('admInstId');
        $this->admission_model->deleteInstallment($admInstId);
        echo json_encode("Data Deleted Successfully");
    }

    public function bookExam($admissionId,$examType){
        $decAdmissionId = $this->encrypt->decode($admissionId);
        $data['booking_type'] = $examType;

        $data['studData'] = $this->student_model->getStudentData($decAdmissionId);
        $data['otherReceipts'] = $this->admission_model->getOtherReceiptsData($decAdmissionId);
        $data['courseDetails'] = $this->student_model->getCourseDetails($decAdmissionId);
        $data['centres'] = $this->tele_enquiry_model->suggested_centre();
        $examdetails = $this->student_model->getExamBookingDetails($decAdmissionId);
        $data['practicalexamdetails'] = $examdetails;
        $data['examdate']  = $this->student_model->getExam_BookingDate($data['studData'][0]['STREAM'],$examType);
        $data['examdetails'] = array();
        if($examType == 'practical'){
          foreach($examdetails as $exam){
            array_push($data['examdetails'],$exam['COURSE_ID']);
          }
        }
        else{
          $data['examdetails'] = $examdetails;
        }
        $data['courses'] = $this->tele_enquiry_model->all_courses();
        $data['modules'] = $this->tele_enquiry_model->all_modules();
        if(isset($_POST['book_exam'])){
          if(($_POST['EXAM_FEES_RECEIPT_NO'] != "") && ($_POST['COURSE_ID'] != "") && ($_POST['CENTRE_ID'] != "") && ($examType == "practical")){
            $examDate = str_replace('/', '-', $_POST['EXAM_DATE']);
            $_POST['EXAM_DATE'] = date("Y-m-d", strtotime($examDate));
            $booked = $this->student_model->bookExam($_POST,$examType);
            if($booked){
              $confirmation = $this->hallticket_print($decAdmissionId,$booked,$examType);
              if($confirmation){
                $this->session->set_flashdata('success', 'Exam Booked Successfully !!!');
    	          redirect(base_url('admission/book-exam/'.$this->encrypt->encode($decAdmissionId).'/'.$examType));
              }
            }
          }
          elseif (($_POST['START_DATE'] != "") && ($_POST['END_DATE'] != "") && ($_POST['JOURNAL_MARKS'] != "") && ($_POST['EXAM_TIME'] != "") && ($_POST['EXAM_FEES_RECEIPT_NO'] != "") && ($_POST['COURSE_ID'] != "") && ($_POST['CENTRE_ID'] != "") && ($examType == "theory")) {
            $examDate = str_replace('/', '-', $_POST['EXAM_DATE']);
            $_POST['EXAM_DATE'] = date("Y-m-d", strtotime($examDate));
            $booked = $this->student_model->bookExam($_POST,$examType);
            if($booked == "notfound"){
              $this->session->set_flashdata('danger', 'Question paper not found.');
  	          redirect(base_url('admission/book-exam/'.$this->encrypt->encode($decAdmissionId).'/'.$examType));
            }
            elseif($booked){
              $confirmation = $this->hallticket_print($decAdmissionId,$booked,$examType);
              if($confirmation){
                $this->session->set_flashdata('success', 'Exam Booked Successfully !!!');
    	          redirect(base_url('admission/book-exam/'.$this->encrypt->encode($decAdmissionId).'/'.$examType));
              }
            }
            else{
              $this->session->set_flashdata('danger', 'Please fill all required details.');
              redirect(base_url('admission/book-exam/'.$this->encrypt->encode($decAdmissionId).'/'.$examType));
            }
          }
          else{
            $this->session->set_flashdata('danger', 'Please select all required details.');
            redirect(base_url('admission/book-exam/'.$this->encrypt->encode($decAdmissionId).'/'.$examType));
          }
        }
        $this->load->admin_view('book_exam',$data);
    }

    public function getexamtimingslot(){
      $centreid = $this->input->post('centreid');
      $examdate = $this->input->post('examdate');
      $examType = 'THEORY';
      $exam_slots  = $this->student_model->getExamBookingTimingSlots($centreid,$examdate,$examType);

      echo json_encode($exam_slots);
    }

    public function hallticket_print($admId,$examid,$exam_type){
      $hallticketdetails = $this->student_model->getExamHallticketDetails($admId,$examid,$exam_type);

      $this->load->helper('currency');
      $this->load->helper('pdf_helper');
      tcpdf();
      // create new PDF document
      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      // set document information
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('Suntech Computer Education');
      $pdf->SetTitle('Print student hallticket');
      $pdf->SetSubject('Print student hallticket');
      $pdf->SetKeywords('Suntech Computer Education student hallticket');

      // set default header data
      $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

      // set header and footer fonts
      $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

      // set default monospaced font
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

      // set margins
      $pdf->SetMargins('15', '0', '15');
      $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

      // set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

      // set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

      $pdf->SetPrintHeader(false);
      $pdf->SetPrintFooter(false);

      $pdf->setFontSubsetting(false);

      // add a page
      $pdf->AddPage();

      $examDate_replace = str_replace('/', '-', $hallticketdetails[0]['EXAM_DATE']);
      $examDate = date("d/m/Y", strtotime($examDate_replace));

      $examType = "";
      if($hallticketdetails[0]['EXAM_TYPE'] == "PRACTICAL"){
        $examType = "Practical Exam Hall Ticket";
      }
      else{
        $examType = "Theory Exam Hall Ticket";
      }

      $html = '<div>
                      <div>
                          <span align="center">
                              <h2 style="padding:5px;">Suntech Computer Education</h2>
                              <label>'.$examType.'</label>
                         </span>
                         <hr style="height:1px;">
                      </div>
                  <table width="640px" style="font-size: small; height: 1400px; padding:5px;">
                      <tr><td colspan="2"><strong>Name</strong></td><td colspan="6">: '.$hallticketdetails[0]['Name'].'</td></tr>
                      <tr><td colspan="2"><strong>Subject</strong></td><td colspan="6">: '.$hallticketdetails[0]['COURSE_NAME'].'</td></tr>
                      <tr><td colspan="2"><strong>Admission Id</strong></td><td colspan="6">: '.$hallticketdetails[0]['ADMISSION_ID'].'</td></tr>
                      <tr><td colspan="2"><strong>Exam Id</strong></td><td colspan="6">: '.$hallticketdetails[0]['EXAM_ID'].'</td></tr>
                      <tr><td colspan="2"><strong>Exam Date</strong></td><td colspan="6">: '.$examDate.'</td></tr>
                      <tr><td colspan="2"><strong>Exam Time</strong></td><td colspan="6">: '.$hallticketdetails[0]['EXAM_TIME'].'</td></tr>
                      <tr><td colspan="2"><strong>Centre</strong></td><td colspan="6">: '.$hallticketdetails[0]['Exam_Center'].'</td></tr>
                      <tr><td colspan="2"><strong>Exam Centre</strong></td><td colspan="6">: '.$hallticketdetails[0]['CENTRE'].'</td></tr>
                      <tr><td colspan="2"><strong>Address</strong></td><td colspan="6">: '.$hallticketdetails[0]['ADDRESS'].'</td></tr>
                      <tr><td colspan="2"><strong>Name</strong></td><td colspan="2">_____________________</td><td colspan="2">_____________________</td><td colspan="2">_____________________</td></tr>
                      <tr><td colspan="2"><strong>Signature</strong></td><td colspan="2">_____________________</td><td colspan="2">_____________________</td><td colspan="2">_____________________</td></tr>
                      <tr><td colspan="2"></td><td colspan="2" align="center">Student</td><td colspan="2" align="center">Faculty</td><td colspan="2" align="center">C.M.</td></tr>
                      <tr><td colspan="8"><strong>Note:</strong> Kindly carry your ID Card. Re-examination will carry Rs. 1000/- in case of absenteeism & Rs. 300/- in case of failure or for next exam.</td></tr>
                      <tr><td colspan="8">For exam related queries contact to : <strong>9324488356/9324488368.</strong></td></tr>
                  </table>
              </div>';

      $file_name = $this->encrypt->encode($hallticketdetails[0]['Name']).$hallticketdetails[0]['EXAM_ID'].".pdf";

      // output the HTML content
      $pdf->writeHTML($html, true, false, true, false, '');

      // reset pointer to the last page
      $pdf->lastPage();

      //Close and output PDF document
      $attachment = $pdf->Output($file_name, 'S');

      $attachments = array("0"=>array("file"=>$attachment,"name"=>$file_name));

      // for mail forwarding attachment on student mail id
      $from = "erp@suntechedu.com";
      $to = "ankurprajapati66@gmail.com";
      $cc = array('ankur@suntechedu.com','rajesh@suntechedu.com','vijay@suntechedu.com');
      $subject = $examType." from Suntech computer education on ".date("d/m/Y");
      $body = "<h2>Dear ".$hallticketdetails[0]['Name']."</h2>
               <br/>
               <p>Kindly take print of this mail while coming to the exam centre.</p>
               <p>* Please do not reply to this mail. This is automated generated email.<br><br>For more information visit on <a href='http://suntechedu.com/'>suntechedu.com</a></p>";
      $confirmationMail = $this->load->mailer($from,$to,$cc,"",$attachments,$subject,$body,"studentdesk");
      return $confirmationMail;
    }

    public function issue_certificate($admId){
      $admissionId = $this->encrypt->decode($admId);

      $data['studData'] = $this->student_model->getStudentData($admissionId);
      $data['receiptDetails'] = $this->admission_model->getAdmissionReceipt($admissionId);
      $data['examdetails'] = $this->student_model->getExamBookingDetails($admissionId);
      $data['courseDetails'] = $this->student_model->getCourseDetails($admissionId);
      $data['allCourseDetails'] = $this->student_model->getAllCourseDetails();
      $data['allSavedCertificateDetails'] = $this->student_model->getAllCertificateDetails($admissionId);

      $data['courses'] = $this->tele_enquiry_model->all_courses();
      $data['modules'] = $this->tele_enquiry_model->all_modules();

      $total_fees = 0;
      foreach($data['receiptDetails'] as $receipts){
        if($receipts['AMOUNT_DEPOSITED'] != NULL){
          $total_fees += $receipts['AMOUNT_PAID_FEES'];
        }
      }
      $data['studData'][0]['FEE_PAID'] = $total_fees;
      if(isset($_POST['save_certificate_data'])){
        if(($_POST['EXAM_MASTER_NEW_ID'] != "") && ($_POST['ISSUED_BY'] != "") && ($_POST['COURSE_ID'] != "") && ($_POST['MODULE_ID'] != "")){
          $certificate = $this->student_model->save_certificate_data($_POST);
          if($certificate){
              $this->session->set_flashdata('success', 'Certificate data saved successfully !!!');
              redirect(base_url('admission/issue-certificate/'.$this->encrypt->encode($admissionId).''));
          }
          elseif($certificate == "false"){
            $this->session->set_flashdata('danger', 'Please fill all details and select proper exam data.');
            redirect(base_url('admission/issue-certificate/'.$this->encrypt->encode($admissionId).''));
          }
        }
        else{
          $this->session->set_flashdata('danger', 'Please fill all details and select proper exam data.');
          redirect(base_url('admission/issue-certificate/'.$this->encrypt->encode($admissionId).''));
        }
      }
      $this->load->admin_view('issue_certificate',$data);
    }

    public function merge_exam_marks(){
      $examid = $this->input->post('exam_id');
      $examid_list = explode(",",$examid);
      $exam_details = array();
      foreach($examid_list as $examId){
        $exam_data = $this->student_model->getMarksAndUpdateMarks($examId);
        array_push($exam_details,array("Exam_Id"=>$exam_data[0]['EXAM_ID'],"Project_Marks"=>$exam_data[0]['PROJECT_MARKS'],"Project_Out_Of"=>$exam_data[0]['PROJECT_MARKS_OUT_OF'],"Exam_Type"=>$exam_data[0]['EXAM_TYPE']));
      }

      $projects_marks = 0;
      $projects_marks_out_of = 0;
      foreach($exam_details as $examination_details){
        if($examination_details['Exam_Type'] == "PRACTICAL"){
          $projects_marks = $examination_details['Project_Marks'];
          $projects_marks_out_of = $examination_details['Project_Out_Of'];
        }
        elseif($examination_details['Project_Marks'] > 0){
          $projects_marks = $examination_details['Project_Marks'];
          $projects_marks_out_of = $examination_details['Project_Out_Of'];
        }
        if($examination_details['Exam_Type'] == "THEORY"){
          $exam_marks = array("PROJECT_MARKS"=>$projects_marks,"PROJECT_MARKS_OUT_OF"=>$projects_marks_out_of);
          $exam_data = $this->student_model->updateMarksInExamTable($exam_data[0]['EXAM_ID'],$exam_data[0]['EXAM_TYPE'],$exam_marks);
          if($exam_data){
            echo json_encode("1");
          }
          else{
            echo json_encode("0");
          }
        }
      }
    }

    public function print_certificate_and_marksheet($certificate_id,$type){
      $certificate_id = $this->encrypt->decode($certificate_id);
      $data['certificateData'] = $this->student_model->getCertificateAndMarksDetails($certificate_id);
      $data['type'] = $type;

      $this->load->helper('currency');
      $this->load->helper('pdf_helper');
      $this->load->admin_view('print_certificate_marksheet',$data);
    }

    public function reset_student_desk_password($admission_id){
      $admission_id = $this->encrypt->decode($admission_id);
      $check = $this->admission_model->check_student_desk_user_exist_or_not($admission_id);
      if($check){
        $update = $this->admission_model->reset_student_desk_user_password($check);
        if($update){
          $this->session->set_flashdata('success',"Password Reset Successfully ! Try to login Student Desk with Username: Your Admission Id And Password: Welcome .");
          redirect(base_url('admission/view-admission'));
        }
      }
      else{
        $insert_user = array(
            'ADMISSION_ID' => $admission_id,
            'USERNAME' => $admission_id,
            'PASSWORD' => 'welcome',
            'ROLE_ID' => '1',
            'IS_ACTIVE' => '1',
            'CREATED_ON' => date('Y-m-d')
        );
        $insert = $this->admission_model->insert_student_desk_user_login_details($insert_user);
        if($insert){
          $this->session->set_flashdata('success',"Password Reset Successfully ! Try to login Student Desk with Username: Your Admission Id And Password: Welcome .");
          redirect(base_url('admission/view-admission'));
        }
      }
    }

}
