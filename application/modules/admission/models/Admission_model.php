<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admission_model extends CI_Model {

    public $sql;
    public function __construct() {
        parent::__construct();
    }

    public function getEnqData($enqId){
        $query = $this->db->select('EM.ENQUIRY_FIRSTNAME,EM.ENQUIRY_MIDDLENAME,EM.ENQUIRY_LASTNAME,EM.ENQUIRY_ADDRESS1,EM.ENQUIRY_ADDRESS2,EM.ENQUIRY_CITY,EM.ENQUIRY_ZIP,EM.COURSE_INTERESTED,EM.ENQUIRY_DATEOFBIRTH,EM.ENQUIRY_EMAIL,EM.ENQUIRY_EDUCATION,EM.ENQUIRY_FORM_NO,EM.ENQUIRY_DATE,EM.ENQUIRY_MOBILE_NO,EM.ENQUIRY_PARENT_NO,EM.REMARKS,EM.OCCUPATION,EM.CENTRE_ID,CM.CENTRE_NAME,TE.TELE_ENQUIRY_ID,if(CF.CONTROLFILE_VALUE IS NULL,TE.SOURCE,CF.CONTROLFILE_VALUE) as Source_name')
                          ->from('enquiry_master EM')
                          ->join('centre_master CM','CM.CENTRE_ID = EM.CENTRE_ID')
                          ->join('tele_enquiries TE','TE.TELE_ENQUIRY_ID = EM.TELE_ENQUIRY_ID','left')
                          ->join('control_file CF','CF.CONTROLFILE_ID = TE.SOURCE','left')
                          ->where('EM.ENQUIRY_ID',$enqId)
                          ->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }

    // public function getEmployeeNames($id){
    //     $query = $this->db->select('EM.EMPLOYEE_ID,EM.EMP_FNAME,EM.EMP_LASTNAME')
    //                       ->from('employee_master EM')
    //                       ->join('employee_timings ET','ET.EMPLOYEE_ID = EM.EMPLOYEE_ID','right')
    //                       ->join('user_login UL','UL.EMPLOYEE_ID=ET.EMPLOYEE_ID','right')
    //                       ->where('ET.CENTRE_ID',$id)
    //                       ->where('EM.ISACTIVE','1')
    //                       ->where('UL.ROLE_ID = 38 or UL.ROLE_ID = 35 or UL.ROLE_ID = 40 or UL.ROLE_ID = 41')
    //                       ->order_by('EM.EMP_FNAME','ASC')
    //                       ->get();
    //     return $query->result_array();
    // }

    public function getDiscount(){
        $query = $this->db->select('DISCOUNT_ID,DISCOUNT_SCHEME')
                          ->from('discount_master')
                          ->get();
        return $query->result_array();
    }

    public function getModuleCourseTransaction($moduleItrsd){
        $query = $this->db->select('module_id,course_id')
                          ->from('module_course_transaction')
                          ->where('module_id',$moduleItrsd)
                          ->get();
        return $query->result_array();
    }

//    public function getEnquiryId($enqFirstName,$enqLastName,$enqMobileNo){
//        $query = $this->sql->select('enquiry_id')
//                          ->from('enquiry_master')
//                          ->where('enquiry_firstname',$enqFirstName)
//                          ->where('enquiry_lastname',$enqLastName)
//                          ->where('enquiry_mobile_no',$enqMobileNo)
//                          ->order_by('enquiry_date','desc')
//                          ->get();
//
//        //return $this->db->insert_id();
//        return $query->row()->enquiry_id;
//    }

    public function get_module_or_course_other_fees($moduleid_or_courseid,$type){
      if($type == "module"){
        $query = $this->db->select('EXAM_FEES_AMOUNT,PROCESSING_FEES_AMOUNT')
                      ->from('module_master')
                      ->where('MODULE_ID',$moduleid_or_courseid)
                      ->get();
        return $query->result_array();
      }
      else{
        $query1 = $this->db->select('EXAM_FEES_AMOUNT')
                      ->from('course_master')
                      ->where('COURSE_ID',$moduleid_or_courseid)
                      ->get();
        return $query1->result_array();
      }
    }

    public function addAdmission($insertAdmission){
        // echo "<pre>";
        // print_r($insertAdmission);
        // echo "</pre>";
        $this->db->insert('admission_master',$insertAdmission);    //  for mysql
        // unset($insertAdmission['MODULE_TAKEN']);
        // if($enqIdTobeInsertedInOldDb != null)
        // {
        //   $insertAdmission['ENQUIRY_ID'] = $enqIdTobeInsertedInOldDb;
        // } //  for mssql

        $lastInsertedIdsInArray = array('mysql_admission_id'=>$this->db->insert_id());

        return $lastInsertedIdsInArray;
    }

    public function updateAdmissionData($enqId,$admissionData,$type){
      if($type == 'add_admission'){
        $this->db->set($admissionData[0])
                 ->where('ENQUIRY_ID',$enqId)
                 ->update('enquiry_master');     //  for mysql


        $checkEnquiryId = $this->checkEnquiryId($enqId);
        $admissionData[0]['ISENROLLED'] = '1';
        if($checkEnquiryId == true){
          $this->db->set($admissionData[0])
                   ->where('ENQUIRY_ID',$enqId)
                   ->update('tele_enquiries');   //  for mysql

        }
      }
      else{
        $this->db->set($admissionData)
                 ->where('ENQUIRY_ID',$enqId)
                 ->update('enquiry_master');     //  for mysql


        $checkEnquiryId = $this->checkEnquiryId($enqId);
        $admissionData['ISENROLLED'] = '1';
        if($checkEnquiryId == true){
          $this->db->set($admissionData)
                   ->where('ENQUIRY_ID',$enqId)
                   ->update('tele_enquiries');   //  for mysql

        }
      }

    }

    public function checkEnquiryId($enqId){
      $query = $this->db->select('ENQUIRY_ID')
                        ->from('tele_enquiries')
                        ->where('ENQUIRY_ID',$enqId)
                        ->get();
      if($query->num_rows > 0){
        return true;
      }
      else{
        return false;
      }
    }

    public function admissionCourseTransaction($admissionCourseTransactionInsert,$admissionIdsArray){
      //       echo "<pre>";
      // print_r($admissionCourseTransactionInsert);
      // echo "</pre>";
      $admissionCourseTransactionInsert['ADMISSION_ID'] = $admissionIdsArray['mysql_admission_id'];
      $this->db->insert('admission_course_transaction',$admissionCourseTransactionInsert);     // for mysql

      //$admissionCourseTransactionInsert['ADMISSION_ID'] = $admissionIdsArray['mssql_admission_id'];
      // echo "<br>";
      // echo $admissionCourseTransactionInsert['COURSE_ID'];
      // echo "<br>";
      $courseIds = explode($admissionCourseTransactionInsert['COURSE_ID'],",");
     // print_r($courseIds);
      // foreach(){

      // }
      return $this->db->insert_id();
    }

    public function studentDeskUsers($studentDesk,$admissionIdsArray){
      $studentDesk['ADMISSION_ID'] = $admissionIdsArray['mysql_admission_id'];
      $studentDesk['USERNAME'] = $admissionIdsArray['mysql_admission_id'];
      $this->db->insert('student_desk_users',$studentDesk); // for mysql
    }

    public function addAdmissionInstallmentTransaction($admissionInstallmentTransactionInsert,$admissionIdsArray){
      $admissionInstallmentTransactionInsert['ADMISSION_ID'] = $admissionIdsArray['mysql_admission_id'];
      $this->db->insert('admission_installment_transaction',$admissionInstallmentTransactionInsert);
      // $admissionInstallmentTransactionInsert['ADMISSION_ID'] = $admissionIdsArray['mssql_admission_id'];
    }

    public function getAdmissionData($decAdmissionId){
      $query = $this->db->select('AIT.ADMISSION_INSTALLEMENT_ID,AIT.ADMISSION_ID,AIT.DUEDATE,AIT.DUE_AMOUNT,CM.CENTRE_NAME,AM.COURSE_TAKEN,AM.MODULE_TAKEN,AM.TOTALFEES,EM.ENQUIRY_FIRSTNAME,EM.ENQUIRY_LASTNAME,AM.EXAM_FEES_APPLICABLE,AM.PROCESSING_FEES_APPLICABLE,AM.ADMISSION_DATE')
                        ->from('admission_installment_transaction AIT')
                        ->join('admission_master AM','AM.ADMISSION_ID = AIT.ADMISSION_ID','right')
                        ->join('centre_master CM','CM.CENTRE_ID = AM.CENTRE_ID','right')
                        ->join('enquiry_master EM','EM.ENQUIRY_ID = AM.ENQUIRY_ID','right')
                        ->where('AM.ADMISSION_ID',$decAdmissionId)
                        ->get();
      return $query->result_array();
    }

    public function getOtherReceiptDetailsByAdmissionId($admissionId){
      $query = $this->db->select('*')
                        ->from('penalties_receipt_master')
                        ->where('ADMISSION_ID',$admissionId)
                        ->get();
      return $query->result_array();
    }

    public function getAdmissionReport($admissionFilter){
      $query = $this->db->select('AM.ADMISSION_ID,concat(EM.ENQUIRY_FIRSTNAME," ",EM.ENQUIRY_LASTNAME) as ENQUIRY_NAME,AM.ADMISSION_DATE,CM.CENTRE_NAME,AM.COURSE_TAKEN,AM.MODULE_TAKEN,AM.TOTALFEES,concat(EM.ENQUIRY_ADDRESS1,", ",EM.ENQUIRY_ADDRESS2,", ",EM.ENQUIRY_CITY,",",ENQUIRY_ZIP,", ",ENQUIRY_STATE) as enquiry_address,concat(EM.ENQUIRY_PARENT_NO," ", EM.ENQUIRY_MOBILE_NO) as enquiry_contact,EM.PREFERREDED_TIME1,EM.PREFERREDED_TIME2,EM.PREFERREDED_TIME3,AM.STATUS,AM.REMARKS,AM.COURSE_STATUS_REMARKS_DATE,AM.COURSE_STATUS_REMARKS,AM.YEAR,AM.IN_TAKE,AM.IS_INSTALLMENT_PLAN_DONE,concat(EMP_FNAME," ",EMP_MIDDLENAME," ",EMP_LASTNAME) as employee_name')
                        ->from('admission_master AM')
                        ->join('centre_master CM','CM.CENTRE_ID = AM.CENTRE_ID')
                        ->join('enquiry_master EM','EM.ENQUIRY_ID = AM.ENQUIRY_ID')
                        ->join('employee_master EPM','EPM.EMPLOYEE_ID = EM.ENQUIRY_HANDELED_BY');
      if(!empty($admissionFilter)){
        if ($admissionFilter['ZERO_FEES'] == '1') {
          $query = $this->db->join('admission_receipts AR','AR.ADMISSION_ID = AM.ADMISSION_ID');
        }

        if ($admissionFilter['ENQUIRY_FIRSTNAME'] != '') {
          $query = $this->db->where('EM.ENQUIRY_FIRSTNAME',$admissionFilter['ENQUIRY_FIRSTNAME']);
        }

        if ($admissionFilter['ENQUIRY_LASTNAME'] != '') {
          $query = $this->db->where('EM.ENQUIRY_LASTNAME',$admissionFilter['ENQUIRY_LASTNAME']);
        }

        if ($admissionFilter['CENTRE_ID'] != '') {
          $query = $this->db->where('AM.CENTRE_ID',$admissionFilter['CENTRE_ID']);
        }

        if ($admissionFilter['ADMISSION_DATE_FROM'] != '') {
          $admDateFromFormat = str_replace('/', '-', $admissionFilter['ADMISSION_DATE_FROM']);
          $admDateFrom = date('Y-m-d', strtotime($admDateFromFormat));
          $query = $this->db->where('AM.ADMISSION_DATE >= ', $admDateFrom);
        }

        if ($admissionFilter['ADMISSION_DATE_FROM'] != '' && $admissionFilter['ADMISSION_DATE_TO'] != '' && $admissionFilter['ADMISSION_DATE_FROM'] < $admissionFilter['ADMISSION_DATE_TO']) {
          $admDateToFormat = str_replace('/', '-', $admissionFilter['ADMISSION_DATE_TO']);
          $admDateTo = date('Y-m-d', strtotime($admDateToFormat));
          $query = $this->db->where('AM.ADMISSION_DATE <= ', $admDateTo);
        }

        if ($admissionFilter['ADMISSION_ID'] != '') {
          $query = $this->db->where('AM.ADMISSION_ID',$admissionFilter['ADMISSION_ID']);
        }

        if ($admissionFilter['STREAM'] != '') {
          $query = $this->db->where('AM.STREAM',$admissionFilter['STREAM']);
        }

        if (!empty($admissionFilter['COURSE_TAKEN'])) {
          $courseTaken = implode(",", $admissionFilter['COURSE_TAKEN']);
          $query = $this->db->like('AM.COURSE_TAKEN',$courseTaken);
        }

        if (!empty($admissionFilter['MODULE_TAKEN'])) {
          $moduleTaken = implode(",", $admissionFilter['MODULE_TAKEN']);
          $query = $this->db->like('AM.MODULE_TAKEN',$moduleTaken);
        }

        if ($admissionFilter['STATUS'] != '') {
          $query = $this->db->where('AM.STATUS',$admissionFilter['STATUS']);
        }

        if ($admissionFilter['IN_TAKE'] != '') {
          $query = $this->db->where('AM.IN_TAKE',$admissionFilter['IN_TAKE']);
        }

        if ($admissionFilter['YEAR'] != '') {
          $query = $this->db->where('AM.YEAR',$admissionFilter['YEAR']);
        }
      }
      $query = $this->db->order_by('AM.ADMISSION_ID','DESC');


      $query = $this->db->where('AM.ISACTIVE','1')
                        ->get();

      $admission_list['fields'] = $query->list_fields();
      $admission_list['details'] = $query->result_array();
      return $admission_list;
    }

// function added for getting fees paid by ADMISSION_ID
    public function getFeesPaidByByAdmissionId($admission_id){
      $query = $this->db->select('SUM(AR.AMOUNT_PAID_FEES) as fees_paid')
                        ->from('admission_receipts AR')
                        ->where('AR.ISACTIVE','1')
                        ->where('AR.AMOUNT_DEPOSITED IS NOT NULL')
                        ->where('AR.ADMISSION_ID',$admission_id)
                        ->get();
      return $query->row()->fees_paid;
    }

    public function getAdmissionList($offset, $perpage, $admissionFilter,$totalRows = 0){
      // EM.PREFERREDED_TIME1,EM.PREFERREDED_TIME2,EM.PREFERREDED_TIME3,    removed as its looks not of use
      $query = $this->db->select('AM.ADMISSION_ID,concat(EM.ENQUIRY_FIRSTNAME," ",EM.ENQUIRY_LASTNAME) as ENQUIRY_NAME,AM.ADMISSION_DATE,CM.CENTRE_NAME,AM.COURSE_TAKEN,AM.MODULE_TAKEN,AM.TOTALFEES,concat(EM.ENQUIRY_ADDRESS1,", ",EM.ENQUIRY_ADDRESS2,", ",EM.ENQUIRY_CITY,",",ENQUIRY_ZIP,", ",ENQUIRY_STATE) as enquiry_address,EM.ENQUIRY_EMAIL,concat(if(EM.ENQUIRY_TELEPHONE IS NULL,EM.ENQUIRY_PARENT_NO,EM.ENQUIRY_TELEPHONE)," ", EM.ENQUIRY_MOBILE_NO) as enquiry_contact,AM.STATUS,AM.REMARKS,AM.COURSE_STATUS_REMARKS_DATE,AM.COURSE_STATUS_REMARKS,AM.YEAR,AM.IN_TAKE,AM.IS_INSTALLMENT_PLAN_DONE,concat(EMP_FNAME," ",EMP_LASTNAME) as employee_name,AM.EXAM_FEES_APPLICABLE,AM.PROCESSING_FEES_APPLICABLE')
                        ->from('admission_master AM')
                        ->join('centre_master CM','CM.CENTRE_ID = AM.CENTRE_ID')
                        ->join('enquiry_master EM','EM.ENQUIRY_ID = AM.ENQUIRY_ID')
                        ->join('employee_master EPM','EPM.EMPLOYEE_ID = AM.HANDLED_BY');
      if(!empty($admissionFilter)){
        if ($admissionFilter['ZERO_FEES'] == '1') {
          $query = $this->db->join('admission_receipts AR','AR.ADMISSION_ID = AM.ADMISSION_ID');
        }

        if ($admissionFilter['ENQUIRY_FIRSTNAME'] != '') {
          $query = $this->db->like('EM.ENQUIRY_FIRSTNAME',$admissionFilter['ENQUIRY_FIRSTNAME'],'both');
        }

        if ($admissionFilter['ENQUIRY_LASTNAME'] != '') {
          $query = $this->db->like('EM.ENQUIRY_LASTNAME',$admissionFilter['ENQUIRY_LASTNAME'],'both');
        }

        // current user role ids
        // $role_ids = array('35','36','37','38','39','40','41','42');
        //
        // if (in_array($this->session->userdata('admin_data')[0]['ROLE_ID'], $role_ids)){
        //   $query = $this->db->where('AM.CENTRE_ID',$this->session->userdata('admin_data')[0]['CENTRE_ID']);
        // }
        // else{
          if ($admissionFilter['CENTRE_ID'] != '') {
            $query = $this->db->where('AM.CENTRE_ID',$admissionFilter['CENTRE_ID']);
          }
        // }

        //
        // if ($admissionFilter['ADMISSION_DATE_FROM'] != '') {
        //   $admDateFromFormat = str_replace('/', '-', $admissionFilter['ADMISSION_DATE_FROM']);
        //   $admDateFrom = date('Y-m-d', strtotime($admDateFromFormat));
        //   $query = $this->db->where('AM.ADMISSION_DATE >= ', $admDateFrom);
        // }
        //
        // if ($admissionFilter['ADMISSION_DATE_FROM'] != '' && $admissionFilter['ADMISSION_DATE_TO'] != '' && $admissionFilter['ADMISSION_DATE_FROM'] < $admissionFilter['ADMISSION_DATE_TO']) {
        //   $admDateToFormat = str_replace('/', '-', $admissionFilter['ADMISSION_DATE_TO']);
        //   $admDateTo = date('Y-m-d', strtotime($admDateToFormat));
        //   $query = $this->db->where('AM.ADMISSION_DATE <= ', $admDateTo);
        // }

        if($admissionFilter['ADMISSION_DATE_FROM'] != '' && $admissionFilter['ADMISSION_DATE_TO'] != ''){
          $date_format_change = str_replace("/","-",$admissionFilter['ADMISSION_DATE_FROM']);
          $from_date = date('Y-m-d', strtotime($date_format_change));

          $to_date_format_change = str_replace("/","-",$admissionFilter['ADMISSION_DATE_TO']);
          $to_date = date('Y-m-d', strtotime($to_date_format_change));

          $where = "AM.ADMISSION_DATE BETWEEN '$from_date' and '$to_date'";

          $query = $this->db->where($where);
        }

        if ($admissionFilter['ADMISSION_ID'] != '') {
          $query = $this->db->where('AM.ADMISSION_ID',$admissionFilter['ADMISSION_ID']);
        }

        if ($admissionFilter['STREAM'] != '') {
          $where = "";
          switch($admissionFilter['STREAM']){
            case '1':
              $where = "AM.STREAM like '%1%' OR AM.STREAM like '%basics%'";
              break;
            case '2':
              $where = "AM.STREAM like '%2%' OR AM.STREAM like '%programming%'";
              break;
            case '3':
              $where = "AM.STREAM like '%3%' OR AM.STREAM like '%graphics & animation%'";
              break;
            case '4':
              $where = "AM.STREAM like '%4%' OR AM.STREAM like '%hardware and networking%'";
              break;
            case '5':
              $where = "AM.STREAM like '%5%' OR AM.STREAM like '%others%'";
              break;
          }
          $query = $this->db->where($where);
        }

        if (!empty($admissionFilter['COURSE_TAKEN'])) {
          $courseTaken = implode(",", $admissionFilter['COURSE_TAKEN']);
          $query = $this->db->like('AM.COURSE_TAKEN',$courseTaken);
        }

        if (!empty($admissionFilter['MODULE_TAKEN'])) {
          $moduleTaken = implode(",", $admissionFilter['MODULE_TAKEN']);
          $query = $this->db->like('AM.MODULE_TAKEN',$moduleTaken);
        }

        if ($admissionFilter['STATUS'] != '') {
          $query = $this->db->where('AM.STATUS',$admissionFilter['STATUS']);
        }

        if ($admissionFilter['IN_TAKE'] != '') {
          $query = $this->db->where('AM.IN_TAKE',$admissionFilter['IN_TAKE']);
        }

        if ($admissionFilter['YEAR'] != '') {
          $query = $this->db->where('AM.YEAR',$admissionFilter['YEAR']);
        }
      }
      $query = $this->db->order_by('AM.ADMISSION_ID','DESC');

      if ($totalRows == 0) {
        $query = $this->db->limit($perpage, $offset);
      }

      $query = $this->db->where('AM.ISACTIVE','1')
                        ->get();

      if ($totalRows != 0) {
        return $query->num_rows();
      }
      else{

      }
      return $query->result_array();
    }

    public function getAdmissionCount(){
      $query = $this->db->select('AM.ADMISSION_ID')
                        ->from('admission_master AM')
                        ->get();
      return $query->num_rows();
    }

public function defaultersData_count($ArrayAdmId){
      $query = $this->db->select('AM.ENQUIRY_ID,AM.ADMISSION_ID,concat(EM.ENQUIRY_FIRSTNAME," ",EM.ENQUIRY_LASTNAME) as STUDENT_NAME,AM.ADMISSION_DATE,AM.MINIMUM_FEES,AM.MAXIMUM_FEES,AM.TOTALFEES,AM.STREAM,AM.PARENT_MOBILE_NUMBER,AM.ADMISSION_TYPE,AM.REFERENCE_GIVEN_BY,AM.IN_TAKE,AM.UNIVERSITY_NAME,AM.CENTRE_ID,CM.CENTRE_NAME,AM.ADMISSION_TYPE,AM.YEAR,AM.UNIVERSITY_NAME,AM.REFERENCE_GIVEN_BY,AM.REMARKS,AM.MODULE_TAKEN,AM.COURSE_TAKEN')
                        ->from('admission_master AM')
                        ->join('centre_master CM','CM.CENTRE_ID = AM.CENTRE_ID')
                        ->join('enquiry_master EM','EM.ENQUIRY_ID = AM.ADMISSION_ID')
                        //->where('AM.ADMISSION_ID',$admId)
                        ->where_in('AM.ADMISSION_ID',$ArrayAdmId)
                        ->get();
                        //echo $this->db->last_query();
                        return $query->num_rows();
    }
    public function defaultersData($offset,$perpage,$ArrayAdmId){
      $query = $this->db->select('AM.ENQUIRY_ID,AM.ADMISSION_ID,concat(EM.ENQUIRY_FIRSTNAME," ",EM.ENQUIRY_LASTNAME) as STUDENT_NAME,AM.ADMISSION_DATE,AM.MINIMUM_FEES,AM.MAXIMUM_FEES,AM.TOTALFEES,AM.STREAM,AM.PARENT_MOBILE_NUMBER,AM.ADMISSION_TYPE,AM.REFERENCE_GIVEN_BY,AM.IN_TAKE,AM.UNIVERSITY_NAME,AM.CENTRE_ID,CM.CENTRE_NAME,AM.ADMISSION_TYPE,AM.YEAR,AM.UNIVERSITY_NAME,AM.REFERENCE_GIVEN_BY,AM.REMARKS,AM.MODULE_TAKEN,AM.COURSE_TAKEN')
                        ->from('admission_master AM')
                        ->join('centre_master CM','CM.CENTRE_ID = AM.CENTRE_ID')
                        ->join('enquiry_master EM','EM.ENQUIRY_ID = AM.ADMISSION_ID')
                        //->where('AM.ADMISSION_ID',$admId)
                        ->where_in('AM.ADMISSION_ID',$ArrayAdmId)
                        ->limit($perpage,$offset)
                        ->get();
                        //echo $this->db->last_query();
      return $query->result_array();
    }

    public function studentAdmissionData($admId){
      $query1 = $this->db->select('COUNT(ADMISSION_INSTALLEMENT_ID) as admission_count')
                ->from('admission_installment_transaction')
                ->where('ADMISSION_ID',$admId)
                ->get();
      $admission_count = $query1->row()->admission_count;

      if($admission_count){
            $query = $this->db->select('AM.ENQUIRY_ID,AM.ADMISSION_ID,AM.HANDLED_BY,concat(EPM.EMP_FNAME," ",EPM.EMP_LASTNAME) as employee_name,AM.ADMISSION_DATE,AM.MINIMUM_FEES,AM.MAXIMUM_FEES,AM.TOTALFEES,AM.STREAM,AM.PARENT_MOBILE_NUMBER,AM.ADMISSION_TYPE,AM.REFERENCE_GIVEN_BY,AM.IN_TAKE,AM.UNIVERSITY_NAME,AM.CENTRE_ID,CM.CENTRE_NAME,EM.ENQUIRY_FIRSTNAME,EM.ENQUIRY_MIDDLENAME,EM.ENQUIRY_LASTNAME,EM.ENQUIRY_EMAIL,EM.ENQUIRY_DATEOFBIRTH,EM.ENQUIRY_ADDRESS1,EM.ENQUIRY_ADDRESS2,EM.ENQUIRY_EDUCATION,EM.OCCUPATION,EM.ENQUIRY_DATE,EM.ENQUIRY_FORM_NO,EM.ENQUIRY_MOBILE_NO,EM.ENQUIRY_MOBILE_NO,EM.ENQUIRY_CITY,EM.ENQUIRY_ZIP,EM.ENQUIRY_STATE,AM.ADMISSION_TYPE,AM.YEAR,AM.UNIVERSITY_NAME,AIT.DUEDATE,AIT.DUE_AMOUNT,AM.REFERENCE_GIVEN_BY,AM.REMARKS,AM.MODULE_TAKEN,AM.COURSE_TAKEN,EM.ENQUIRY_PARENT_NO,EM.ENQUIRY_MOBILE_NO')
                          ->from('admission_master AM')
                          ->join('centre_master CM','CM.CENTRE_ID = AM.CENTRE_ID','right')
                          ->join('enquiry_master EM','EM.ENQUIRY_ID = AM.ENQUIRY_ID','right')
                          ->join('employee_master EPM','EPM.EMPLOYEE_ID = EM.ENQUIRY_HANDELED_BY','right')
                          ->join('admission_installment_transaction AIT','AIT.ADMISSION_ID = AM.ADMISSION_ID','right')
                          ->where('AM.ADMISSION_ID',$admId)
                          ->get();
      }
      else{
          $query = $this->db->select('AM.ENQUIRY_ID,AM.ADMISSION_ID,AM.HANDLED_BY,AM.ADMISSION_DATE,AM.MINIMUM_FEES,AM.MAXIMUM_FEES,AM.TOTALFEES,AM.STREAM,AM.PARENT_MOBILE_NUMBER,AM.ADMISSION_TYPE,AM.REFERENCE_GIVEN_BY,AM.IN_TAKE,AM.UNIVERSITY_NAME,AM.CENTRE_ID,CM.CENTRE_NAME,EM.ENQUIRY_FIRSTNAME,EM.ENQUIRY_MIDDLENAME,EM.ENQUIRY_LASTNAME,EM.ENQUIRY_EMAIL,EM.ENQUIRY_DATEOFBIRTH,EM.ENQUIRY_ADDRESS1,EM.ENQUIRY_ADDRESS2,EM.ENQUIRY_EDUCATION,EM.OCCUPATION,EM.ENQUIRY_DATE,EM.ENQUIRY_FORM_NO,EM.ENQUIRY_MOBILE_NO,EM.ENQUIRY_MOBILE_NO,EM.ENQUIRY_CITY,EM.ENQUIRY_ZIP,EM.ENQUIRY_STATE,AM.ADMISSION_TYPE,AM.YEAR,AM.UNIVERSITY_NAME,AM.REFERENCE_GIVEN_BY,AM.REMARKS,AM.MODULE_TAKEN,AM.COURSE_TAKEN,EM.ENQUIRY_PARENT_NO,EM.ENQUIRY_MOBILE_NO')
                        ->from('admission_master AM')
                        ->join('centre_master CM','CM.CENTRE_ID = AM.CENTRE_ID','right')
                        ->join('enquiry_master EM','EM.ENQUIRY_ID = AM.ENQUIRY_ID','right')
                        ->join('employee_master EPM','EPM.EMPLOYEE_ID = EM.ENQUIRY_HANDELED_BY','right')
                        ->where('AM.ADMISSION_ID',$admId)
                        ->get();
      }
                        //echo $this->db->last_query();
      return $query->result_array();
    }

    public function updateAdmission($admId,$updateAdmission){
        $this->db->set($updateAdmission)
                 ->where('ADMISSION_ID',$admId)
                 ->update('admission_master');    //  for mysql

        // unset($updateAdmission['COURSE_TAKEN']);
        // unset($updateAdmission['MODULE_TAKEN']);
        //
    }

    public function updateAdmissionCourseTransaction($admId,$admissionCourseTransactionInsert){
      $this->db->where('admission_id',$admId)
               ->delete('admission_course_transaction');

      if($this->db->affected_rows() > 0){
        $admissionCourseTransactionInsert['ADMISSION_ID'] = $admId;
        $this->db->insert('admission_course_transaction',$admissionCourseTransactionInsert);     // for mysql
        return $this->db->insert_id();
      }

    }

    public function updateAdmissionInstallmentTransaction($admId,$admissionInstallmentTransactionInsert){
      $this->db->where('admission_id',$admId)
               ->delete('admission_installment_transaction');

      if($this->db->affected_rows() > 0){
        $admissionInstallmentTransactionInsert['ADMISSION_ID'] = $admId;
        $this->db->insert('admission_installment_transaction',$admissionInstallmentTransactionInsert);
      }

    }

    public function deleteAdmission($decAdmissionId){
      $this->db->set('ISACTIVE',0)
               ->where('ADMISSION_ID',$decAdmissionId)
               ->update('admission_master');    // for mysql


      if($this->db->affected_rows() > 0){
        return true;
      }
      else{
        $this->db->set('ISACTIVE',1)
                 ->where('ADMISSION_ID',$decAdmissionId)
                 ->update('admission_master');

        return false;
      }
    }

    public function getAdmissionReceiptData($decAdmId){
      $query = $this->db->select('AM.ADMISSION_ID,AM.CENTRE_ID')
                        ->from('admission_master AM')
                        ->where('AM.ADMISSION_ID',$decAdmId)
                        ->get();
      return $query->result_array();
    }

    public function totAmtPaid($decAdmId){
      $query =  $this->db->select_sum('AMOUNT_PAID_FEES')
                         ->where('ADMISSION_ID',$decAdmId)
                         ->get('admission_receipts');
      $sum = $query->result_array();
      if($query->num_rows() > 0){
        return $sum[0]['AMOUNT_PAID_FEES'];
      }
      else{
        return 0;
      }
    }

    public function getReceiptCount($centreId,$payType,$type){

      // echo $centreId."  ".$payType;
      // $query = $this->db->select_max('CENTRE_RECEIPT_ID')
      //                   ->from('admission_receipts')
      //                   ->where('CENTRE_ID',$centreId)
      //                   ->where('PAYMENT_TYPE',$payType)
      //                   ->get();
      // return $query->row_array();

// updated by ankur on 23/10/2017



        switch($payType){
            case '1':
                $paymentTypeNew = "cash";
                break;
            case '2':
                $paymentTypeNew = 'cc';
                break;
            case '3':
                $paymentTypeNew = 'cheque';
                break;
            default:
                $paymentTypeNew = '';
                break;
        }

        $table_name = "";
        if($type == "admission_receipt"){
          $table_name = "admission_receipts";
        }
        else{
          $table_name = "penalties_receipt_master";
        }

        $row = "";
        if($payType == '1'){
          $row = $this->db->query("SELECT MAX(CENTRE_RECEIPT_ID) AS maxCenterId FROM ".$table_name." where CENTRE_ID=".$centreId." and (PAYMENT_TYPE='".$payType."' or PAYMENT_TYPE='cash')")->row();
        }
        else{
          $row = $this->db->query("SELECT MAX(CENTRE_RECEIPT_ID) AS maxCenterId FROM ".$table_name." where CENTRE_ID=".$centreId." and (PAYMENT_TYPE='2' or PAYMENT_TYPE='cc' or PAYMENT_TYPE='3' or PAYMENT_TYPE='cheque')")->row();
        }

        // $row = $this->db->query("SELECT MAX(CENTRE_RECEIPT_ID) AS maxCenterId FROM ".$table_name." where CENTRE_ID=".$centreId." and PAYMENT_TYPE='".$payType."'")->row();
        if ($row) {
            $MAXID = $row->maxCenterId;
        }


        // $row1 = $this->db->query("SELECT MAX(CENTRE_RECEIPT_ID) AS maxCenterId FROM ".$table_name." where CENTRE_ID=".$centreId." and PAYMENT_TYPE='".$paymentTypeNew."'")->row();
        // if ($row) {
        //     $MAXID1 = $row1->maxCenterId;
        // }

        ///if($MAXID > $MAXID1){
          return $MAXID;
        // }
        // else{
        //   return $MAXID1;
        // }


    }

    public function getAdmissionReceipt($admId){
      $query = $this->db->select('AR.ADMISSION_RECEIPTS_ID,AR.ADMISSION_RECEIPT_NO,AR.PAYMENT_DATE,AR.AMOUNT_PAID,AR.AMOUNT_PAID_FEES,AR.PAYMENT_TYPE,AM.TOTALFEES,AR.AMOUNT_PAID_SERVICETAX,AR.CHEQUE_NO,AR.CHEQUE_DATE,AR.CHEQUE_BANK_NAME,AR.AMOUNT_DEPOSITED')
                        ->from('admission_receipts AR')
                        ->join('admission_master AM','AM.ADMISSION_ID = AR.ADMISSION_ID')
                        ->where('AM.ADMISSION_ID',$admId)
                        ->where('AR.ISACTIVE','1')
                        ->get();
      //print $this->db->last_query();
      return $query->result_array();
    }

    public function getAdmissionInstallmentTransaction($admId){
      $query = $this->db->select('DUEDATE,DUE_AMOUNT,ADMISSION_INSTALLEMENT_ID')
                        ->from('admission_installment_transaction')
                        ->where('ADMISSION_ID',$admId)
                        ->get();
                        //print $this->db->last_query();
      return $query->result_array();
    }

    public function saveAdmissionReceiptData($insertReceiptArray){
      $query = $this->db->insert('admission_receipts',$insertReceiptArray);   // for mysql
      if ($query) {
        $recId = $this->db->insert_id();
        $admRecData = $this->db->select('AR.ADMISSION_RECEIPTS_ID,AR.ADMISSION_RECEIPT_NO,AR.PAYMENT_DATE,AR.AMOUNT_PAID,AR.AMOUNT_PAID_FEES,AR.PAYMENT_TYPE,AM.TOTALFEES')
                        ->from('admission_receipts AR')
                        ->join('admission_master AM','AM.ADMISSION_ID = AR.ADMISSION_ID')
                        ->where('AR.ADMISSION_RECEIPTS_ID',$recId)
                        ->get();
        return $admRecData->result_array();
      }
      else{
        return false;
      }
    }

    public function printAdmissionReceipt($decAdmRecId){
      //echo $decAdmRecId;
      $query = $this->db->select('AR.ADMISSION_RECEIPT_NO,AM.ADMISSION_ID,EM.ENQUIRY_FIRSTNAME,EM.ENQUIRY_LASTNAME,AM.ADMISSION_DATE,AM.MODULE_TAKEN,AM.COURSE_TAKEN,AM.TOTALFEES,AR.AMOUNT_PAID_FEES,AR.PAYMENT_DATE,CM.CENTRE_NAME,EM.ENQUIRY_FIRSTNAME,EM.ENQUIRY_LASTNAME,EM.ENQUIRY_ADDRESS1,EM.ENQUIRY_ADDRESS2,EM.ENQUIRY_CITY,EM.ENQUIRY_STATE,EM.ENQUIRY_ZIP,AR.AMOUNT_PAID,AR.AMOUNT_PAID_FEES,AR.AMOUNT_PAID_SERVICETAX,AR.SERVICE_TAX,AR.PAYMENT_TYPE,EMP.EMP_FNAME,EMP.EMP_LASTNAME,AR.CHEQUE_NO,AR.CHEQUE_DATE,AR.CHEQUE_BRANCH,AR.CHEQUE_BANK_NAME')
                        ->from('admission_receipts AR')
                        ->join('admission_master AM','AM.ADMISSION_ID = AR.ADMISSION_ID','right')
                        ->join('enquiry_master EM','EM.ENQUIRY_ID = AM.ENQUIRY_ID','right')   // changes made by ankur on the place of admission_id i put it as enquiry id
                        ->join('centre_master CM','CM.CENTRE_ID = AM.CENTRE_ID','right')
                        ->join('employee_master EMP','EMP.EMPLOYEE_ID = AR.RECEIVED_BY','left')
                        ->where('AR.ADMISSION_RECEIPTS_ID',$decAdmRecId)
                        ->get();
      return $query->result_array();
    }

    public function admissionReceiptMaxPaid($admRecNo){
      $query = $this->db->select_max('AMOUNT_PAID_FEES')
                        ->from('admission_receipts')
                        ->where('ADMISSION_RECEIPT_NO',$admRecNo)
                        ->get();
      return $query->result_array();
    }

    public function amtPaidTillDate($admId,$payDate){
      $query = $this->db->select('AMOUNT_PAID_FEES')
                        ->from('admission_receipts')
                        ->where('PAYMENT_DATE <=',$payDate)
                        ->where('ADMISSION_ID',$admId)
                        ->get();
      return $query->result_array();
    }

    public function payFor($prId = 0){
      $query = $this->db->select('CONTROLFILE_ID,CONTROLFILE_KEY,CONTROLFILE_VALUE')
                        ->from('control_file');
      if ($prId !=0) {
        $query = $this->db->where('CONTROLFILE_ID',$prId);
      }
      else{
        $query = $this->db->where('CONTROLFILE_KEY','Other_charges');
      }

      $query = $this->db->get();
      return $query->result_array();
    }

    public function getOtherReceiptsData($admId){
      $query = $this->db->select('PR.ADMISSION_ID,PR.PENALTY_RECEIPT_ID,PR.PENALTY_RECEIPT_NO,PR.PAYMENT_DATE,if(CF.CONTROLFILE_VALUE IS NULL,PR.PARTICULAR,CF.CONTROLFILE_VALUE) as PARTICULAR,PR.AMOUNT,PR.AMOUNT_WITHOUT_ST,PR.PAYMENT_TYPE,PR.SERVICE_TAX')
                        ->from('penalties_receipt_master PR')
                        ->join('control_file CF','CF.CONTROLFILE_ID = PR.PARTICULAR','left')
                        ->where('PR.ADMISSION_ID',$admId)
                        ->where('PR.ISACTIVE','1')
                        ->get();
      return $query->result_array();
    }

    public function addOtherReceipts($insertReceiptArray){
      $this->db->insert('penalties_receipt_master',$insertReceiptArray);    // for mysql

      // if ($this->db->affected_rows() > 0) {
      //   return $this->db->insert_id();
      // }

      // echo "parameters array : <br><pre>";
      // print_r($insertReceiptArray);
      // echo "</pre>";
      // exit();

      if (array_key_exists("CHEQUE_BANK_NAME",$insertReceiptArray)){
        $bank_name = $insertReceiptArray['CHEQUE_BANK_NAME'];
        unset($insertReceiptArray['CHEQUE_BANK_NAME']);
        $insertReceiptArray['BANK_NAME'] = $bank_name;
      }

      if (array_key_exists("CHEQUE_BRANCH",$insertReceiptArray)){
        $branch = $insertReceiptArray['CHEQUE_BRANCH'];
        unset($insertReceiptArray['CHEQUE_BRANCH']);
        $insertReceiptArray['BRANCH'] = $branch;
      }

      if (($this->db->affected_rows() > 0)) {
        return $this->db->insert_id();
      }
    }

    public function getOthRecPrintData($penaltyId){
      $query = $this->db->select('PR.PAYMENT_TYPE,PR.BRANCH,PR.PENALTY_RECEIPT_NO,PR.PAYMENT_DATE,ER.ENQUIRY_FIRSTNAME,ENQUIRY_LASTNAME,EPM.EMP_FNAME,EMP_LASTNAME,PR.AMOUNT,PR.AMOUNT_WITHOUT_ST,PR.SERVICE_TAX_PAID,PR.SERVICE_TAX,PR.PARTICULAR,PR.EXAM_MODULE_ID,PR.EXAM_COURSE_ID,CM.CENTRE_NAME,PR.ADMISSION_ID,CF.CONTROLFILE_VALUE,PR.EXAM_COURSE_ID,PR.EXAM_MODULE_ID')
                        ->from('penalties_receipt_master PR')
                        ->join('enquiry_master ER','ER.ADMISSION_ID = PR.ADMISSION_ID','left')
                        ->join('employee_master EPM','EPM.EMPLOYEE_ID = PR.RECEIVED_BY')
                        ->join('centre_master CM','CM.CENTRE_ID = PR.CENTRE_ID')
                        ->join('control_file CF', 'CF.CONTROLFILE_ID = PR.PARTICULAR')
                        ->where('PR.PENALTY_RECEIPT_ID',$penaltyId)
                        ->get();
      return $query->result_array();
    }

    public function getAdmCourses($admId){
      $query = $this->db->select('COURSE_ID')
                        ->from('admission_course_transaction')
                        ->where('ADMISSION_ID',$admId)
                        ->get();
      return $query->result_array();
    }

    public function getExamCourse($getAdmCoursesIdArr){
      $query = $this->db->select('COURSE_ID,COURSE_NAME')
                        ->from('course_master')
                        ->where_in('COURSE_ID',$getAdmCoursesIdArr)
                        ->get();
      return $query->result_array();
    }

    public function getExamModule($getAdmModulesIdArr){
      $query = $this->db->select('MODULE_ID,MODULE_NAME')
                        ->from('module_master')
                        ->where_in('MODULE_ID',$getAdmModulesIdArr)
                        ->get();
      return $query->result_array();

    }

    public function deleteAdmissionInstallment($decAdmissionId){
      $this->db->where('ADMISSION_ID',$decAdmissionId)
               ->delete('admission_installment_transaction');
    }

    public function addAdmissionInstallment($installmentData){
      $this->db->insert('admission_installment_transaction',$installmentData);
    }

    public function updateAdmissionInstallment($admInstId,$installmentData){
      $this->db->set($installmentData)
               ->where('ADMISSION_INSTALLEMENT_ID',$admInstId)
               ->update('admission_installment_transaction');
    }

    public function deleteInstallment($admInstId)
    {
      $this->db->where('ADMISSION_INSTALLEMENT_ID',$admInstId)
               ->delete('admission_installment_transaction');
    }

    // added by ankur
    public function getCourseIdByCourseName($course_name){
      $query = $this->db->select('COURSE_ID')
                        ->from('course_master')
                        ->where('COURSE_NAME',$course_name)
                        ->get();


     $id= $query->result();

      foreach ($id as $key => $value) {
        foreach ($id[$key] as $index => $data) {

          $data = $id[$key]->COURSE_ID;
          return $data;
        }
      }

    }

    public function getCourseNameByCourseId($course_id){
      $query = $this->db->select('COURSE_NAME,COURSE_ID')
                        ->from('course_master')
                        ->where('COURSE_ID',$course_id)
                        ->get();
      return $query->result_array();
    }

    public function get_centre_mail_ids($centre_id){
      $query = $this->db->select('email1')
                    ->from('centre_master')
                    ->where('centre_id',$centre_id)
                    ->get();

      return  $query->row()->email1;
    }

    public function check_student_desk_user_exist_or_not($admission_id){
      $query = $this->db->select('user_id')
                    ->from('student_desk_users')
                    ->where('ADMISSION_ID',$admission_id)
                    ->get();

      return  $query->row()->user_id;
    }

    public function reset_student_desk_user_password($user_id){
      $password = $this->load->encrypt_decrypt_data('welcome',"encrypt");
      $this->db->set('PASSWORD',$password)
               ->where('USER_ID',$user_id)
               ->update('student_desk_users');

       if($this->db->affected_rows() > 0){
         return true;
       }
    }

    public function insert_student_desk_user_login_details($insert_user){
      $this->db->insert('student_desk_users',$insert_user);

       if($this->db->affected_rows() > 0){
         return true;
       }
    }
}
