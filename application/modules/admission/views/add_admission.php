<div class="gapping"></div>
<div class="create_batch_form">
    <div id="box">
        <h2>Add Admission</h2></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align:center; color:#F00;"></div>
                <div class="panel-body">
                   <?php echo form_open(); ?>
                       <?php
                        /* This Form is for convert Enquiry */
                        foreach($enqData as $enq){
                        ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-4">
                                            <label>First Name: <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_FIRSTNAME" value="<?php echo $enq['ENQUIRY_FIRSTNAME']; ?>" id="ENQUIRY_FIRSTNAME " required="required" />
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_FIRSTNAME'); ?></span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Middle Name:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_MIDDLENAME" value="<?php echo $enq['ENQUIRY_MIDDLENAME']; ?>" id="ENQUIRY_MIDDLENAME" />
                                        </div>
                                        <div class="col-md-4">
                                            <label>Last Name: <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_LASTNAME" value="<?php echo $enq['ENQUIRY_LASTNAME']; ?>" id="ENQUIRY_LASTNAME" required="required" />
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_LASTNAME'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Flat/Room/Wing No.:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_ADDRESS1" id="ENQUIRY_ADDRESS1" value="<?php echo $enq['ENQUIRY_ADDRESS1']; ?>"> </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Bldg./Plot/Chawl No.:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_ADDRESS2" id="ENQUIRY_ADDRESS2" value="<?php echo $enq['ENQUIRY_ADDRESS2']; ?>"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-4">
                                            <label>State:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_STATE" value="MAHARASHTRA" readonly="readonly" id="ENQUIRY_STATE" /> </div>
                                         <div class="col-md-4">
                                            <label>City:</label>
                                            <select name="ENQUIRY_CITY" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1" <?php if($enq['ENQUIRY_CITY'] == '1'){echo 'selected';} ?> >MUMBAI</option>
                                                <option value="2" <?php if($enq['ENQUIRY_CITY'] == '2'){echo 'selected';} ?>>NAVI MUMBAI</option>
                                                <option value="3" <?php if($enq['ENQUIRY_CITY'] == '3'){echo 'selected';} ?>>THANE</option>
                                                <option value="4" <?php if($enq['ENQUIRY_CITY'] == '4'){echo 'selected';} ?>>PUNE</option>
                                                <option value="5" <?php if($enq['ENQUIRY_CITY'] == '5'){echo 'selected';} ?>>NASHIK</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Zip:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_ZIP" value="<?php echo $enq['ENQUIRY_ZIP']; ?>" id="ENQUIRY_ZIP" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                         <div class="col-md-6">
                                            <label>Stream</label>
                                             <select class="form-control" style="width:100% ;" name="STREAM" id="STREAM">
                                                            <option value="B">Basics</option>
                                                            <option value="P">Programming</option>
                                                            <option value="G">Graphics &amp; animation</option>
                                                            <option value="H">Hardware &amp; Networking</option>
                                                            <option value="O">others</option>
                                             </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Admission Date:(dd/mm/yyyy)</label>
                                            <input class="form-control enquiry_date" type="text" name="ADMISSION_DATE" value="<?php echo date('d/m/Y');?>" id="ADMISSION_DATE"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                   <?php
                                            $selected_course = explode(",",$enq['COURSE_INTERESTED']);


                                            foreach ($selected_course as $diff_course) {
                                                // echo $diff_course;
                                                $diff_course_arr = explode("-", $diff_course);
                                                $course_arr['course_id'] = $diff_course_arr[0];
                                                // condition for handeling the unknown error of course find added by ankur   ---- for course name  without -1
                                                if(!(is_numeric($course_arr['course_id']))){
                                                    $this->load->model('admission_model');
                                                    $course_arr['course_id'] = $this->admission_model->getCourseIdByCourseName($course_arr['course_id']);
                                                }

                                                // condition for handeling the unknown error of course find added by ankur   ---- for only course id without -1
                                                if(!array_key_exists("1", $diff_course_arr)){
                                                    $diff_course_arr[1] = "1";
                                                }
                                                $course_arr['course_type'] = $diff_course_arr[1];
                                                $final_course_arr[] = $course_arr;
                                            }

                                            $course_id = array();
                                            $module_id = array();

                                            $course_min_fee = array();
                                            $course_max_fee = array();

                                            $module_min_fee = array();
                                            $module_max_fee = array();
                                            foreach ($final_course_arr as $final_course) {
                                                if ($final_course['course_type'] == '1') {
                                                    $course_id[] = $final_course['course_id'];
                                                }

                                                if ($final_course['course_type'] == '2') {
                                                    $module_id[] = $final_course['course_id'];
                                                }
                                            }
                                            foreach($courses as $course){
                                                if(in_array($course['COURSE_ID'],$course_id)){

                                                    $course_min_fee[] = $course['MIN_FEES'];
                                                    $course_max_fee[] = $course['MAX_FEES'];
                                                }
                                            }

                                            foreach ($modules as $module) {
                                                if(in_array($module['MODULE_ID'],$module_id)){
                                                    $module_min_fee[] = $course['MIN_FEES'];
                                                    $module_max_fee[] = $course['MAX_FEES'];
                                                }
                                            }
                                            // print_r($module_min_fee);
                                            $min_fee_sum = array_sum($course_min_fee) + array_sum($module_min_fee);
                                            $max_fee_sum = array_sum($course_max_fee) + array_sum($module_max_fee);
                                    ?>
                                    <div class="col-md-12" style="margin-left:0px;">
                                        <div class="col-md-3">
                                             <label>Minimum Fees</label>
                                            <input type="text" readonly class="form-control min_fee" value="<?php echo $min_fee_sum; ?>" name="MINIMUM_FEES">
                                            Discount: <input type="checkbox" class="discount">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Maximum Fees</label>
                                            <input type="text" readonly class="form-control max_fee" value="<?php echo $max_fee_sum; ?>" name="MAXIMUM_FEES">
                                        </div>
                                        <div class="col-md-6">
                                             <label>Total Fees Payable(w/o GST): <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="TOTALFEES" id="total_payable_fees_amount">
                                            <span class="text-danger fee-error"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group discount_style" style="display:none;"></div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-md-12" style="margin-left:0px;">
                                        <div class="col-md-6">
                                             <label>Downpayment: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="DOWNPAYMENT">
                                        </div>
                                        <div class="col-md-6">
                                             <label>Downpayment Date: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control enquiry_date" name="DOWNPAYMENT_DATE">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-md-12" style="margin-left:0px;">
                                        <div class="col-md-6">
                                             <label>Installment Payable: <span class="text-danger">*</span></label>
                                             <input type="text" class="form-control" name="MONTHLY_PAY">
                                        </div>
                                        <div class="col-md-6">
                                             <label>Installment Date: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control enquiry_date" name="INSTALLMENT_DATE">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                <label>Module Enrolled <span class="text-danger">*</span></label>
                                <select class="form-control module_enrolled" name="MODULE_INTERESTED[]" id="" multiple="">
                                    <?php foreach($modules as $module){
                                        $isSelected = in_array($module['MODULE_ID'],$module_id) ? "selected='selected'" : "";
                                        ?>
                                    <option value="<?php echo $module['MODULE_ID']; ?>" <?php echo $isSelected; ?>><?php echo $module['MODULE_NAME']; ?> </option>
                                    <?php } ?>
                                </select>
                                <span class="text-danger"><?php echo form_error('COURSE_INTERESTED'); ?></span>
                            </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-md-12">
                                       <ul class="list-group modules_seld">
                                       <?php
                                            $selected_course = explode(",",$enq['COURSE_INTERESTED']);
                                            foreach($modules as $module){
                                            if(in_array($module['MODULE_ID'],$module_id)){ ?>
                                        <li class="list-group-item">
                                            <?php echo $module['MODULE_NAME']; ?>
                                        </li>
                                        <?php } } ?>
                                       </ul>
                                        <span class="text-danger"><?php echo form_error('COURSE_VALIDATOR'); ?></span>
                                   </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-4" style="float:right">
                                            <?php
                                              if($enq['ENQUIRY_DATEOFBIRTH'] != ""){
                                                $ENQUIRY_DATEOFBIRTH = date("d/m/Y", strtotime($enq['ENQUIRY_DATEOFBIRTH']));
                                              }else{
                                                $ENQUIRY_DATEOFBIRTH = "";
                                              }
                                            ?>
                                            <label>D.O.B: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control enquiry_date" name="ENQUIRY_DATEOFBIRTH" id="ENQUIRY_DATEOFBIRTH" value="<?php echo $ENQUIRY_DATEOFBIRTH; ?>">
                                             <span class="text-danger"><?php echo form_error('ENQUIRY_DATEOFBIRTH'); ?></span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Email: <span class="text-danger">*</span></label>
                                            <input type="email" class="form-control" name="ENQUIRY_EMAIL" value="<?php echo $enq['ENQUIRY_EMAIL']; ?>">
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_EMAIL'); ?></span>
                                        </div>
                                        <div class="col-md-4" style="float:right">
                                            <label>Education: <span class="text-danger">*</span></label>
                                            <select name="ENQUIRY_EDUCATION" class="form-control">
                                                <option value=''>Please Select</option>
                                                <?php foreach($educations as $education){ ?>
                                               <option value="<?php echo $education['CONTROLFILE_ID']; ?>" <?php echo ($education['CONTROLFILE_ID'] == $enq['ENQUIRY_EDUCATION']) ? ' selected="selected"' : '';?>><?php echo $education['CONTROLFILE_VALUE']; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_EDUCATION'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-4">
                                            <label>Occupation:</label>
                                            <select name="OCCUPATION" class="form-control">
                                                <option value="">Please Select</option>
                                                <?php foreach($occupations as $occupation){ ?>
                                                <option value="<?php echo $occupation['CONTROLFILE_ID']; ?>" <?php echo ($occupation['CONTROLFILE_ID'] == $enq['OCCUPATION']) ? ' selected="selected"' : '';?>><?php echo $occupation['CONTROLFILE_VALUE']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Enquiry Date:</label>
                                            <input type="text" id="ENQUIRY_DATE" class="form-control enquiry_date" name="ENQUIRY_DATE" value="<?php $ENQUIRY_DATE = date("d/m/Y", strtotime($enq['ENQUIRY_DATE'])); echo $ENQUIRY_DATE; ?>">
                                            </div>
                                        <div class="col-md-4">
                                            <label>Enquiry Form No.</label>
                                            <input class="form-control" type="text" name="ENQUIRY_FORM_NO" value="<?php echo $enq['ENQUIRY_FORM_NO']; ?>" id="ENQUIRY_FORM_NO">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6" style="float:right">
                                            <label>Mobile No.(Work/Home): <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_MOBILE_NO" value="<?php echo $enq['ENQUIRY_MOBILE_NO']; ?>" id="ENQUIRY_MOBILE_NO" maxlength="10">
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_MOBILE_NO'); ?></span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Parent's No: </label>
                                            <input class="form-control" type="text" name="ENQUIRY_PARENT_NO" value="<?php echo $enq['ENQUIRY_PARENT_NO'] ?>" id="ENQUIRY_PARENT_NO" maxlength="10">
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_PARENT_NO'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                         <div class="col-md-4">
                                            <label>Centre:</label>
                                            <select disabled name="CENTRE_ID" class="form-control">
                                                    <option value="<?php echo $enq['CENTRE_ID'] ?>"><?php echo $enq['CENTRE_NAME'] ?></option>
                                                </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Admission Type:</label>
                                            <select name="ADMISSION_TYPE" class="form-control">
                                                <option value="">Select Admission Type</option>
                                                <option value="1" selected>Regular</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Handeled By: <span class="text-danger">*</span></label>
                                            <select name="ENQUIRY_HANDELED_BY" class="form-control">
                                                   <?php foreach($employees as $employee){ ?>
                                                    <option value="<?php echo $employee['EMPLOYEE_ID'] ?>">
                                                        <?php echo $employee['EMP_FNAME'].' '.$employee['EMP_LASTNAME'] ?>
                                                    </option>
                                                    <?php } ?>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('ENQUIRY_HANDELED_BY'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-4">
                                            <label>Intake: <span class="text-danger">*</span></label>
                                            <select name="IN_TAKE" id="" class="form-control">
                                                 <?php $month = trim(date('m')); ?> 
                                                <option value="">Please Select</option>
                                                <option value="JANUARY" <?php echo ($month=='1')?"selected":"";?> >JANUARY</option>
                                                <option value="FEBRUARY" <?php echo ($month=='2')?"selected":"";?> >FEBRUARY</option>
                                                <option value="MARCH" <?php echo ($month=='3')?"selected":"";?> >MARCH</option>
                                                <option value="APRIL" <?php echo ($month=='4')?"selected":"";?> >APRIL</option>
                                                <option value="MAY" <?php echo ($month=='5')?"selected":"";?> >MAY</option>
                                                <option value="JUNE" <?php echo ($month=='6')?"selected":"";?> >JUNE</option>
                                                <option value="JULY" <?php echo ($month=='7')?"selected":"";?> >JULY</option>
                                                <option value="AUGUST" <?php echo ($month=='8')?"selected":"";?> >AUGUST</option>
                                                <option value="SEPTEMBER" <?php echo ($month=='9')?"selected":"";?> >SEPTEMBER</option>
                                                <option value="OCTOBER" <?php echo ($month=='10')?"selected":"";?> >OCTOBER</option>
                                                <option value="NOVEMBER" <?php echo ($month=='11')?"selected":"";?> >NOVEMBER</option>
                                                <option value="DECEMBER" <?php echo ($month=='12')?"selected":"";?> >DECEMBER</option>

                                            </select>
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Academic Year: <span class="text-danger">*</span></label>
                                            <select name="ACAD_YEAR" id="" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="2019-2020">2019-2020</option>
                                                <option value="2020-2021">2020-2021</option>
                                                <option value="2021-2022" selected>2021-2022</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>University name: <span class="text-danger">*</span></label>
                                            <select name="UNIVERSITY_NAME" id="" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1" selected>STCE</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-6">
                                            <label>Reference Given By:</label>
                                            <input class="form-control" type="text" name="REFERENCE_GIVEN_BY" value="" id="REFERENCE_GIVEN_BY">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Upgraded By:</label>
                                            <input class="form-control" type="text" name="REFERENCE_GIVEN_BY" value="" id="REFERENCE_GIVEN_BY">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                               <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-12">


                                            <label>Remarks: <span class="text-danger"></span></label>
                                            <input class="form-control" type="text" name="REMARKS" value="<?php echo $enq['REMARKS']; ?>" id="REMARKS">
                                            <span class="text-danger"><?php echo form_error('REMARKS'); ?></span>
                                        </div>
                                    </div>
                               </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                <label>Course Enrolled <span class="text-danger">*</span></label>
                                <select class="form-control course_enrolled" style="width:100%;height:158px" name="COURSE_INTERESTED[]" id="COURSE_INTERESTED" multiple="">
                                    <?php
                                        $selected_course = explode(",",$enq['COURSE_INTERESTED']);
                                        foreach($courses as $course){
                                        $isSelected = in_array($course['COURSE_ID'],$course_id) ? "selected='selected'" : "";
                                    ?>
                                        <option value="<?php echo $course['COURSE_ID']; ?>" ><?php echo $course['COURSE_NAME']; ?></option>
                                    <?php } ?>
                                    <option value=''>Other</option>
                                </select>
                                <span class="text-danger"><?php echo form_error('COURSE_INTERESTED'); ?></span>
                            </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-md-12">
                                       <ul class="list-group courses_seld">
                                       <?php
                                            $selected_course = explode(",",$enq['COURSE_INTERESTED']);
                                            foreach($courses as $course){
                                            if(in_array($course['COURSE_ID'],$course_id)){ ?>
                                        <li class="list-group-item">
                                            <?php echo $course['COURSE_NAME'];; ?>
                                        </li>
                                        <?php } } ?>
                                       </ul>
                                   </div>
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <div class="row">
                                   <div class="col-md-12">
                                       <button type='submit' class='btn btn-primary' id="add_admission_validation" disabled="disabled" title="Select proper admission fees.">Add Admission</button>&nbsp;
                                   </div>
                                </div>
                            </div>
                        </div>
                        <?php }
                        echo form_close(); ?>
                </div>
            </div>