<?php
    $courses_arr = explode(",", $admission_data[0]['COURSE_TAKEN']);
    $modules_arr = explode(",", $admission_data[0]['MODULE_TAKEN']);

    $balanceInstallmentCalc = $admission_data[0]['TOTALFEES'] - $admission_data[0]['DUE_AMOUNT'];
?>
<div class="row">
  <div class="col-md-4">
  <?php 
    $courseStr = '';
    $moduleStr = '';
      for($j=0;$j<count($courses);$j++){
          if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
            // $courseNames .= '<li class="list-group-item">'.$courses[$j]['COURSE_NAME'].'</li>';
            $courseStr .= $courses[$j]['COURSE_NAME'].', ';
          }
      }
      for($k=0;$k<count($modules);$k++){
          if(in_array($modules[$k]['MODULE_ID'],$modules_arr)){
            // $courseNames .= '<li class="list-group-item">'.$courses[$j]['COURSE_NAME'].'</li>';
            $moduleStr .= $modules[$k]['MODULE_NAME'].', ';
          }
      }

      $moduleStrTrim = trim($moduleStr,", ");
      $courseStrTrim = trim($courseStr,", ");

      $course = $moduleStrTrim.$courseStrTrim;

  ?>
    <ul class="list-group">
      <li class="list-group-item">Admission Id: <strong><?php echo $admission_data[0]['ADMISSION_ID']; ?></strong></li>
      <li class="list-group-item">Student Name: <strong><?php echo $admission_data[0]['ENQUIRY_FIRSTNAME'].' '.$admission_data[0]['ENQUIRY_LASTNAME']; ?></strong></li>
      <li class="list-group-item">Centre: <strong><?php echo $admission_data[0]['CENTRE_NAME']; ?></strong></li>
      <li class="list-group-item">Course: <strong><?php echo $course; ?></strong></li>
      <li class="list-group-item">Total Fees: <strong><?php echo $admission_data[0]['TOTALFEES']; ?></strong></li>
    </ul>    
  </div>
</div>
  <div id="box">
        <h2 class="text-center"> Admission Details </h2>
  </div>

  <div class="panel panel-default">
    <div class="panel-body table-responsive">
      <table class="table table-striped">
    <thead>
      <tr>
        <th>Installment Pay Date</th>
        <th>Installment Fees Paid</th>
        <th>Installment Fees Pending</th>
      </tr>
    </thead>
    <tbody>

    <?php
      $installmentCalcAuto = $balanceInstallmentCalc;
        ?>


     <tr>
       <td><?php echo $admission_data[0]['DUEDATE']; ?></td>
       <td><?php echo $admission_data[0]['DUE_AMOUNT']; ?></td>
       <td><?php
            $firstInstallment = $admission_data[0]['TOTALFEES'] - $admission_data[0]['DUE_AMOUNT'];
        echo $firstInstallment; ?></td>
     </tr>
     <?php 
     $admCount = count($admission_data) - 1;
     for ($i=1; $i <count($admission_data) ; $i++) { ?>
       <tr>
         <td><?php echo $admission_data[$i]['DUEDATE']; ?></td>
         <td><?php echo $admission_data[$i]['DUE_AMOUNT']; ?></td>
         <td><?php 
          $installmentCalcAuto = $installmentCalcAuto - $admission_data[$i]['DUE_AMOUNT']; 
          echo $installmentCalcAuto;
         ?>
           
         </td>
       </tr>
     <?php } ?>
        <tr>
          <td><?php echo $admission_data[$admCount]['DUEDATE']; ?></td>
          <td><?php echo $installmentCalcAuto; ?></td>
          <td><?php echo '0'; ?></td>
        </tr>
    </tbody>
  </table>    
    </div>
</div>   

<?php

$particular_value = "";
$particular_id = "";
$amount_paid = "";

if($admission_data[0]['EXAM_FEES_APPLICABLE'] != 0){
  $particular_value = "EXAM FEE";
}
else if($admission_data[0]['PROCESSING_FEES_APPLICABLE'] != 0){
  $particular_value = "PROCESSING FEE";
}

foreach($payFor as $key => $value){
  if($payFor[$key]['CONTROLFILE_VALUE'] == $particular_value){
    $particular_id = $payFor[$key]['CONTROLFILE_ID'];
  }
}

foreach($receipts as $otherReceipts){
  if($otherReceipts['PARTICULAR'] == $particular_id){
    $amount_paid += $otherReceipts['AMOUNT'];
  }
}

$isOtherReceiptToBeOpened = "";

if($particular_value == 'EXAM FEE'){
  if($amount_paid < $admission_data[0]['EXAM_FEES_APPLICABLE']){
    $isOtherReceiptToBeOpened = "true";
  }
  else{
    $isOtherReceiptToBeOpened = "false";
  }
}
else{
  if($amount_paid < $admission_data[0]['PROCESSING_FEES_APPLICABLE']){
    $isOtherReceiptToBeOpened = "true";
  }
  else{
    $isOtherReceiptToBeOpened = "false";
  }
}
?>

<button type='submit' class='col-sm-4 btn btn-primary make_payment_other_receipt' data-admission_id="<?php echo $this->encrypt->encode($admission_data[0]['ADMISSION_ID']); ?>" data-exam_fees="<?php echo $admission_data[0]['EXAM_FEES_APPLICABLE'];?>" data-processing_fees="<?php echo $admission_data[0]['PROCESSING_FEES_APPLICABLE'];?>" data-page_type="admission_details" data-permission="<?php echo $isOtherReceiptToBeOpened;?>">Make Fees Payment</button>

  <!-- Receipts Modal Starts -->
    <div id="receiptModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" id="receiptdetails">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Receipts</h4>
          </div>
          <div class="modal-body">

              <!-- starts Tab panes -->
              <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="admissionreceipt">
                  <!-- start admission receipt panel -->

                <div class="modal-body receiptContent">
                    <div class="row">
                      <div class="col-md-4">
                        <label for="ADMISSION_ID">Admission ID:</label>
                        <p class="admId"><?php echo $admission_data[0]['ADMISSION_ID'];?></p>
                      </div>
                      <div class="col-md-4">
                        <label for="STUDENT_NAME">Student Name:</label>
                        <p class="enqName"><?php echo $admission_data[0]['ENQUIRY_FIRSTNAME'].' '.$admission_data[0]['ENQUIRY_LASTNAME']; ?></p>
                      </div>
                      <div class="col-md-4">
                        <label for="centre">Centre:</label>
                          <p><?php echo $admission_data[0]['CENTRE_NAME']; ?></p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <label for="ADMISSION_DATE">Admission Date</label>
                        <p class="admDate"><?php 
                  $admDateRep = str_replace('-', '/', $admission_data[0]['ADMISSION_DATE']);
                    $adm_list['ADMISSION_DATE'] =  date("d/m/Y", strtotime($admDateRep) );

                  echo $adm_list['ADMISSION_DATE']; ?></p>
                      </div>
                      <div class="col-md-4">
                        <label for="">Course And Module Taken</label>
                        <ul class="list-group courseModule">
                        <?php if(($moduleStrTrim != "") && ($courseStrTrim != "")){
                          echo '<li class="list-group-item list-group-item-info moduleTaken"><strong>Modules</strong><li class="list-group-item">'.$moduleStrTrim.'</li></li><li class="list-group-item list-group-item-info courseTaken"><strong>Courses</strong><li class="list-group-item">'.$courseStrTrim.'</li></li>';
                        }
                        else if($moduleStrTrim != ""){
                          echo '<li class="list-group-item list-group-item-info moduleTaken"><strong>Modules</strong><li class="list-group-item">'.$moduleStrTrim.'</li></li>';
                        }
                        else if($courseStrTrim != ""){
                          echo '<li class="list-group-item list-group-item-info courseTaken"><strong>Courses</strong><li class="list-group-item">'.$courseStrTrim.'</li></li>';
                        }?>
                        </ul>
                      </div>
                      <div class="col-md-4">
                        <label for="">Course and Module Total Fees</label>
                        <p class="courseFees"><?php echo $admission_data[0]['TOTALFEES'];?></p>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <label for="TOTALFEES">Amount Paid (With GST): <span class="text-danger">*</span></label>
                      <input type="text" class="form-control amtSt" name="TOTALFEES" autocomplete="off">
                    </div>
                    <div class="col-md-4">
                      <label for="">Amount Paid Fees(Without GST)</label>
                      <p class="amtNoSt"></p>
                    </div>
                    <div class="col-md-4">
                      <label for="">GST Paid:</label>
                      <p class="servTax"></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <label for="">Payment Type</label>
                      <select class="form-control paymentType" name="PAYMENT_TYPE" id="">
                        <option value="1">Cash</option>
                        <option value="2">CC</option>
                        <option value="3">Cheque</option>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <?php $myDate = date('d/m/Y'); ?>
                      <label for="">Payment Date:</label>
                      <input type="text" value="<?php echo $myDate; ?>" name="PAYMENT_DATE" class="form-control payDate" disabled="">
                    </div>
                    <div class="col-md-4">
                      <label for="">Cheque Date:</label>
                      <input type="text" name="CHEQUE_DATE" class="form-control enquiry_date chequeDate" disabled="">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <label for="">Cheque/DD/TID No:</label>
                      <input type="text" class="form-control chequeNo" name="CHEQUE_NO" disabled="">
                    </div>
                    <div class="col-md-4">
                      <label for="">Bank Name</label>
                      <input type="text" class="form-control bankName" name="BANK_NAME" disabled="">
                    </div>
                    <div class="col-md-4">
                      <label for="">Bank Branch</label>
                      <input type="text" class="form-control bankBranch" name="BANK_BRANCH" disabled="">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <label for="">Received By: <span class="text-danger">*</span></label>
                  <select class="form-control recBy" name="RECEIVED_BY" id="">
                    <?php foreach($employees as $employee){ ?>
                      <option value="<?php echo $employee['EMPLOYEE_ID']; ?>"><?php echo $employee['EMP_FNAME'].' '.$employee['EMP_LASTNAME']; ?></option>
                    <?php } ?>
                  </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4">&nbsp;</div>
                    <div class="col-md-4">
                      <p>&nbsp;</p>
                      <div class="col-md-6">
                        <button type="button" class="btn btn-info submit" id="saveAdmissionReceipt" data-admid="<?php echo $this->encrypt->encode($admission_data[0]['ADMISSION_ID']); ?>">Save</button>
                      </div>
                      <div class="col-md-6">
                        <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                      </div>
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <table class="table table-hover table-striped receiptTbl">
                        <thead>
                          <tr>
                            <th>Receipt No</th>
                            <th>Date</th>
                            <th>Amount Paid WST</th>
                            <th>Amount W/o GST</th>
                            <th>Payment Type</th>
                            <th>Balance</th>
                            <th>Receipt</th>
                            <th>Summary</th>
                          </tr>
                        </thead>
                        <tbody class="receiptTblData">
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="row">
                   <div class="container">
                      <div class="col-md-4">
                      <h5>Installment Plan:</h5>
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th>SrNo</th>
                              <th>DP/Installment Date</th>
                              <th>DP/Installment Amount</th>
                            </tr>
                          </thead>
                          <tbody class="instData"></tbody>
                        </table>
                      </div>
                    </div>
                   </div>
                </div>

                        <!-- end admission receipt panel -->
                        </div>
                    </div>
                    <!-- end Tab panes -->

               </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>

        </div>

      </div>
    </div>
  <!-- Receipts Modal Ends -->

  <!-- Other Receipts Modal Starts -->
    <div id="otherReceiptsModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Other Receipt</h4>
          </div>
          <div class="modal-body receiptContent">
              <div class="row">
                <div class="col-md-4">
                  <label for="ADMISSION_ID">Admission ID:</label>
                  <p class="admId"><?php echo $admission_data[0]['ADMISSION_ID'];?></p>
                </div>
                <div class="col-md-4">
                  <label for="STUDENT_NAME">Student Name:</label>
                  <p class="enqName"><?php echo $admission_data[0]['ENQUIRY_FIRSTNAME'].' '.$admission_data[0]['ENQUIRY_LASTNAME']; ?></p>
                </div>
                <div class="col-md-4">
                  <label for="centre">Centre:</label>
                  <p><?php echo $admission_data[0]['CENTRE_NAME']; ?></p>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <label for="ADMISSION_DATE">Admission Date</label>
                  <p class="admDate"><?php 
                  $admDateRep = str_replace('-', '/', $admission_data[0]['ADMISSION_DATE']);
                    $adm_list['ADMISSION_DATE'] =  date("d/m/Y", strtotime($admDateRep) );

                  echo $adm_list['ADMISSION_DATE']; ?></p>
                </div>
                <div class="col-md-4">
                  <label for="">Course And Module Taken</label>
                  <ul class="list-group courseModule">
                    <?php if(($moduleStrTrim != "") && ($courseStrTrim != "")){
                      echo '<li class="list-group-item list-group-item-info moduleTaken"><strong>Modules</strong><li class="list-group-item">'.$moduleStrTrim.'</li></li><li class="list-group-item list-group-item-info courseTaken"><strong>Courses</strong><li class="list-group-item">'.$courseStrTrim.'</li></li>';
                    }
                    else if($moduleStrTrim != ""){
                      echo '<li class="list-group-item list-group-item-info moduleTaken"><strong>Modules</strong><li class="list-group-item">'.$moduleStrTrim.'</li></li>';
                    }
                    else if($courseStrTrim != ""){
                      echo '<li class="list-group-item list-group-item-info courseTaken"><strong>Courses</strong><li class="list-group-item">'.$courseStrTrim.'</li></li>';
                    }?>
                    </ul>
                </div>
                <div class="col-md-4">
                  <label for="">Payment For</label>
                  <select class="form-control payFor" name="payFor" disabled="true">

                  </select>
                </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <label for="TOTALFEES">Amount Paid (With GST): <span class="text-danger">*</span></label>
                <input class="form-control amtSt" name="TOTALFEES" autocomplete="off" type="text">
              </div>
              <div class="col-md-4">
                <label for="">Amount Paid Fees(Without GST)</label>
                <p class="amtNoSt"></p>
              </div>
              <div class="col-md-4">
                <label for="">GST Paid:</label>
                <p class="servTax"></p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <label for="">Payment Type</label>
                <select class="form-control paymentType" name="PAYMENT_TYPE" id="">
                  <option value="1">Cash</option>
                  <option value="2">CC</option>
                  <option value="3">Cheque</option>
                </select>
              </div>
              <div class="col-md-4">
                <label for="">Payment Date:</label>
                <input value="<?php echo date('d/m/Y');?>" name="PAYMENT_DATE" class="form-control payDate" disabled="" type="text">
              </div>
              <div class="col-md-4">
                <label for="">Cheque Date:</label>
                <input name="CHEQUE_DATE" class="form-control enquiry_date chequeDate" disabled="" type="text">
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <label for="">Cheque/DD/TID No:</label>
                <input class="form-control chequeNo" name="CHEQUE_NO" disabled="" type="text">
              </div>
              <div class="col-md-4">
                <label for="">Bank Name</label>
                <input class="form-control bankName" name="BANK_NAME" disabled="" type="text">
              </div>
              <div class="col-md-4">
                <label for="">Bank Branch</label>
                <input class="form-control bankBranch" name="BANK_BRANCH" disabled="" type="text">
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <label for="">Received By: <span class="text-danger">*</span></label>
              <select class="form-control recBy" name="RECEIVED_BY" id="">
                <?php foreach($employees as $employee){ ?>
                  <option value="<?php echo $employee['EMPLOYEE_ID']; ?>"><?php echo $employee['EMP_FNAME'].' '.$employee['EMP_LASTNAME']; ?></option>
                <?php } ?>
              </select>
              </div>
              <div class="col-md-4">
                <label for="">Remarks:</label>
                <input class="form-control bankBranch" name="REMARKS" type="text">
              </div>
              <div class="col-md-4">
                <label for="">Exam/Course Module:</label>
                <select name="exmCrsMod" id="" class="exmCrsMod form-control"></select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">&nbsp;</div>
              <div class="col-md-4">
                <p>&nbsp;</p>
                <div class="col-md-6">
                  <button type="button" class="btn btn-info" id="saveOtherReceipt" data-admid="<?php echo $this->encrypt->encode($admission_data[0]['ADMISSION_ID']); ?>">Save</button>
                </div>
                <div class="col-md-6">
                  <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
              </div>
              <div class="col-md-4">&nbsp;</div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <table class="table table-hover table-striped receiptTbl">
                  <thead>
                    <tr>
                      <th>Receipt No</th>
                      <th>Date</th>
                      <th>Particular</th>
                      <th>Amount Paid WGST</th>
                      <th>Amount W/o GST</th>
                      <th>Payment Type</th>
                      <th>Print</th>
                    </tr>
                  </thead>
                  <tbody class="otherReceiptTblData"></tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
  <!-- Other Receipts Modal Ends -->
