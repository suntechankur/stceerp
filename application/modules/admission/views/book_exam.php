<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2><?php echo ucfirst($booking_type);?> Exam Booking</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?php if($this->session->flashdata('success')) { ?>
						<div class="alert alert-success">
						<strong> <?php echo $this->session->flashdata('success'); ?></strong>
						</div>
						<?php } ?>
						<?php if($this->session->flashdata('danger')) { ?>
						<div class="alert alert-danger">
						<strong> <?php echo $this->session->flashdata('danger'); ?></strong>
						</div>
						<?php } ?>
					</div>
					<?php
						foreach($studData as $sdata){
						?>
					<div class="panel-body">
						<?php echo form_open(); ?>
							<div class="col-md-12">

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Addmission Id: <?php echo $sdata['ADMISSION_ID']?></label>
												<input type="hidden" name="ADMISSION_ID" value="<?php echo $sdata['ADMISSION_ID']?>">
												<input type="hidden" name="STREAM" value="<?php echo $sdata['STREAM']?>">
											</div>
											<div class="col-lg-4">
												<label>Full Name: <?php echo $sdata['ENQUIRY_FIRSTNAME']." ".$sdata['ENQUIRY_MIDDLENAME']." ".$sdata['ENQUIRY_LASTNAME']?></label>
											</div>
											<div class="col-lg-4">
												<label>Centre Name: <?php echo $sdata['CENTRE_NAME']?></label>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Admission Date: <?php $date=date_create($sdata['ADMISSION_DATE']); echo date_format($date,"d/m/Y");?></label>
											</div>
											<div class="col-lg-4">
												<?php
                          $courses_arr = explode(",",$sdata['COURSE_TAKEN']);
                               $courseNames = '';
                                for($j=0;$j<count($courses);$j++){
                                    if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
                                          $courseNames .= ','.$courses[$j]['COURSE_NAME'];
                                        $sdata['COURSE_TAKEN'] = trim($courseNames,",");
                                      }

                                }
                          ?>
                          <?php
                          $modules_arr = explode(",",$sdata['MODULE_TAKEN']);
                               $moduleNames = '';
                                for($j=0;$j<count($modules);$j++){
                                    if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
                                          $moduleNames .= ','.$modules[$j]['MODULE_NAME'];
                                        $sdata['MODULE_TAKEN'] = trim($moduleNames,",");
                                      }

                                }
															 $sdata['COURSE_TAKEN'] = trim($sdata['MODULE_TAKEN'].",".$sdata['COURSE_TAKEN'],",");
															 unset($sdata['MODULE_TAKEN']);
                          ?>
												<label>Course Taken : <?php echo $sdata['COURSE_TAKEN']?></label>
											</div>
											<div class="col-lg-4">
												<label>Admission Type : <?php echo $sdata['ADMISSION_TYPE']?></label>
											</div>
										</div>
									</div>
								</div>

								<?php if($booking_type == "theory"){?>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">

												<div class="col-md-4">
													<label>Course Start Date : <span class="text-danger">*</span></label>
													<input type="text" class="form-control enquiry_date" name="START_DATE">
												</div>
												<div class="col-md-4">
													<label>Course End Date : <span class="text-danger">*</span></label>
													<input type="text" class="form-control enquiry_date" name="END_DATE">
												</div>
												<div class="col-md-4">
													<label>Journal Marks (out of 10) : <span class="text-danger">*</span></label>
													<input type="number" max="10" name="JOURNAL_MARKS" style="display: block;width:100%;height: 34px;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;border-radius: 4px;">
													<input type="hidden" name="JOURNAL_MARKS_OUT_OF" value="10">
												</div>
										</div>
									</div>
								</div>
							<?php }?>

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">

												<div class="col-md-4">
													<label>Exam Fee Receipt No : <span class="text-danger">*</span></label>
													<select name="EXAM_FEES_RECEIPT_NO" class="form-control">
														<option value="">Select Receipt</option>
														<?php foreach($otherReceipts as $otherreceipt){?>
															<option value="<?php echo $otherreceipt['PENALTY_RECEIPT_NO'];?>"><?php echo $otherreceipt['PENALTY_RECEIPT_NO'];?></option>
														<?php } ?>
													</select>
												</div>
												<div class="col-md-8">
													<label>Subject Name : <span class="text-danger">*</span></label>
													<select name="COURSE_ID" class="form-control">
														<option value="">Exam Subject</option>
														<?php foreach($courseDetails as $course){
																		if(!(in_array($course['COURSE_ID'], $examdetails))){?>
																			<option value="<?php echo $course['COURSE_ID'];?>"><?php echo $course['COURSE_NAME'];?></option>
														<?php } } ?>
													</select>
												</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">

												<div class="col-md-4">
													<label>Exam Centre : <span class="text-danger">*</span></label>
													<select name="CENTRE_ID" class="form-control select_exam_centre_name">
														<option value="">Select Centre Name</option>
														<?php foreach($centres as $centre){
																		if($booking_type == "theory"){
																			if(($centre['CENTRE_ID'] == "155") || ($centre['CENTRE_ID'] == "200")){?>
																			<option value="<?php echo $centre['CENTRE_ID'];?>"><?php echo $centre['CENTRE_NAME'];?></option>
														<?php     }
																		}else{?>
																			<option value="<?php echo $centre['CENTRE_ID'];?>"><?php echo $centre['CENTRE_NAME'];?></option>
													<?php			}
																	} ?>
													</select>
												</div>
												<div class="col-md-4">
													<label>Exam Date : <span class="text-danger">*</span></label>
												  <?php	$examDate = str_replace('-', '/',$examdate[0]['EXAM_DATE']);
						            				$exam_date =  date("d/m/Y", strtotime($examDate) ); ?>
													<input type="text"  name="EXAM_DATE" class="form-control enquiry_date" value="<?php echo $exam_date;?>">
												</div>
												<?php if($booking_type == "theory"){?>
												<div class="col-md-4">
													<label>Exam Time : <span class="text-danger">*</span></label>
													<input type="text" name="EXAM_TIME" class="form-control" id="exam_scheduled_time" readonly>
												</div>
											  <?php } ?>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">

												<div class="col-md-4">
												</div>
												<div class="col-md-4">
													<input class="admToken" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
													<button type="submit" name="book_exam" class="btn btn-primary">Book</button>
												</div>

										</div>
									</div>
								</div>
								<?php if($booking_type == "theory"){?>
								<div class="form-group" id="exam_time_select_table" style="display:none;">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">

											<div class="panel panel-default">
												<div class="panel-body table-responsive">
													<table class="table table-striped table-hover">
														<thead>
															<tr>
																<th>Timing</th>
																<th>Capacity</th>
																<th>Booked</th>
																<th>Available Seats</th>
																<th>Select Time</th>
															</tr>
														</thead>
														<tbody id="exam_slots">
															<tr><td>09:00 AM</td><td>20</td><td id="bookedfor900am" data-time1="09:00" data-time2="09:00 AM"></td><td class="availableseat" id="notbookedfor900am" data-time1="09:00" data-time2="09:00 AM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor900am" data-time="09:00 AM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
															<tr><td>09:50 AM</td><td>20</td><td id="bookedfor950am" data-time1="09:50" data-time2="09:50 AM"></td><td class="availableseat" id="notbookedfor950am" data-time1="09:50" data-time2="09:50 AM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor950am" data-time="09:50 AM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
															<tr><td>10:40 AM</td><td>20</td><td id="bookedfor1040am" data-time1="10:40" data-time2="10:40 AM"></td><td class="availableseat" id="notbookedfor1040am" data-time1="10:40" data-time2="10:40 AM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor1040am" data-time="10:40 AM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
															<tr><td>11:30 AM</td><td>20</td><td id="bookedfor1130am" data-time1="11:30" data-time2="11:30 AM"></td><td class="availableseat" id="notbookedfor1130am" data-time1="11:30" data-time2="11:30 AM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor1130am" data-time="11:30 AM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
															<tr><td>12:20 PM</td><td>20</td><td id="bookedfor1220pm" data-time1="12:20" data-time2="12:20 PM"></td><td class="availableseat" id="notbookedfor1220pm" data-time1="12:20" data-time2="12:20 PM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor1220pm" data-time="12:20 PM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
															<tr><td>01:10 PM</td><td>20</td><td id="bookedfor110pm" data-time1="01:10" data-time2="01:10 PM"></td><td class="availableseat" id="notbookedfor110pm" data-time1="01:10" data-time2="01:10 PM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor110pm" data-time="01:10 PM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
															<tr><td>02:00 PM</td><td>20</td><td id="bookedfor200pm" data-time1="02:00" data-time2="02:00 PM"></td><td class="availableseat" id="notbookedfor200pm" data-time1="02:00" data-time2="02:00 PM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor200pm" data-time="02:00 PM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
															<tr><td>02:50 PM</td><td>20</td><td id="bookedfor250pm" data-time1="02:50" data-time2="02:50 PM"></td><td class="availableseat" id="notbookedfor250pm" data-time1="02:50" data-time2="02:50 PM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor250pm" data-time="02:50 PM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
															<tr><td>03:40 PM</td><td>20</td><td id="bookedfor340pm" data-time1="03:40" data-time2="03:40 PM"></td><td class="availableseat" id="notbookedfor340pm" data-time1="03:40" data-time2="03:40 PM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor340pm" data-time="03:40 PM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
															<tr><td>04:30 PM</td><td>20</td><td id="bookedfor430pm" data-time1="04:30" data-time2="04:30 PM"></td><td class="availableseat" id="notbookedfor430pm" data-time1="04:30" data-time2="04:30 PM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor430pm" data-time="04:30 PM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
															<tr><td>05:20 PM</td><td>20</td><td id="bookedfor520pm" data-time1="05:20" data-time2="05:20 PM"></td><td class="availableseat" id="notbookedfor520pm" data-time1="05:20" data-time2="05:20 PM"></td><td><span style="font-weight:bold;cursor:pointer;" class="selectexamtime" id="selectbookedfor520pm" data-time="05:20 PM">Select <i class="glyphicon glyphicon-send"></i></span></td></tr>
														</tbody>
													</table>
												</div>
											</div>

										</div>
									</div>
								</div>
							<?php }?>
								</div>
							</div>
						 <?php echo form_close(); ?>
						</div>
					<?php }?>
					</div>
				</div>
			</div>
		<br/>
		<div id="box"><h2></h2></div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>Exam Id</th>
									<th>Exam Date</th>
									<th>Exam Time</th>
									<th>Subject</th>
									<?php if($booking_type == "theory"){?>
									<th>Internal Exam Marks</th>
									<th>Theory Exam Marks</th>
								  <?php } ?>
									<th>Practical Exam Marks</th>
									<th>Total Marks</th>
									<th>Percentage</th>
									<th>Result</th>
									<th>Exam Type</th>
									<th>Receipt No</th>
									<th>Hallticket</th>
								</tr>
							</thead>
							<tbody>
								<?php

								if($booking_type == "practical"){
									$examdetails = $practicalexamdetails;
								}
								foreach($examdetails as $examdata){
											$final_internal_marks = 0;
											$final_external_marks = 0;

											if($booking_type == "practical"){
												if($examdata['PROJECT_MARKS'] == ""){
													$examdata['PROJECT_MARKS'] = 0;
												}
												if($examdata['PROJECT_MARKS_OUT_OF'] == ""){
													$examdata['PROJECT_MARKS_OUT_OF'] = 0;
												}
												$final_internal_marks = $examdata['PROJECT_MARKS'].'/'.$examdata['PROJECT_MARKS_OUT_OF'];
												$total = $final_internal_marks;
												if(($examdata['PROJECT_MARKS'] == 0) || ($examdata['PROJECT_MARKS_OUT_OF'] == 0)){
													$final_internal_percentage = 0;
												}
												else{
													$final_internal_percentage = round($examdata['PROJECT_MARKS']/$examdata['PROJECT_MARKS_OUT_OF']*100);
												}

												if($final_internal_percentage > 39){
													$result = "<b style='color:green'>Pass</b>";
													$percentage = "<b style='color:green'>".$final_internal_percentage."</b>";
												}
												else{
													$result = "<b style='color:red'>Fail</b>";
													$percentage = "<b style='color:red'>".$final_internal_percentage."</b>";
												}
											}
											else{
												// for internal marks
												if($examdata['JOURNAL_MARKS'] == ""){
													$examdata['JOURNAL_MARKS'] = 0;
												}
												if($examdata['JOURNAL_MARKS_OUT_OF'] == ""){
													$examdata['JOURNAL_MARKS_OUT_OF'] = 0;
												}
												$final_internal_marks = $examdata['JOURNAL_MARKS'].'/'.$examdata['JOURNAL_MARKS_OUT_OF'];
												if(($examdata['JOURNAL_MARKS'] == 0) || ($examdata['JOURNAL_MARKS_OUT_OF'] == 0)){
													$final_internal_percentage = 0;
												}
												else{
													$final_internal_percentage = round($examdata['JOURNAL_MARKS']/$examdata['JOURNAL_MARKS_OUT_OF']*100);
												}
												// for internal marks

												// for external marks
												if($examdata['ONLINE_MARKS'] == ""){
													$examdata['ONLINE_MARKS'] = 0;
												}
												if($examdata['ONLINE_MARKS_OUT_OF'] == ""){
													$examdata['ONLINE_MARKS_OUT_OF'] = 0;
												}
												$final_external_marks = $examdata['ONLINE_MARKS'].'/'.$examdata['ONLINE_MARKS_OUT_OF'];
												if(($examdata['ONLINE_MARKS'] == 0) || ($examdata['ONLINE_MARKS_OUT_OF'] == 0)){
													$final_external_percentage = 0;
												}
												else{
													$final_external_percentage = round($examdata['ONLINE_MARKS']/$examdata['ONLINE_MARKS_OUT_OF']*100);
												}
												// for external marks

												// for practical marks
												if($examdata['PROJECT_MARKS'] == ""){
													$examdata['PROJECT_MARKS'] = 0;
												}
												if($examdata['PROJECT_MARKS_OUT_OF'] == ""){
													$examdata['PROJECT_MARKS_OUT_OF'] = 0;
												}
												$final_practical_marks = $examdata['PROJECT_MARKS'].'/'.$examdata['PROJECT_MARKS_OUT_OF'];
												if(($examdata['PROJECT_MARKS'] == 0) || ($examdata['PROJECT_MARKS_OUT_OF'] == 0)){
													$final_practical_percentage = 0;
												}
												else{
													$final_practical_percentage = round($examdata['PROJECT_MARKS']/$examdata['PROJECT_MARKS_OUT_OF']*100);
												}
												// for practical marks

												$total_marks = $examdata['JOURNAL_MARKS'] + $examdata['ONLINE_MARKS'] + $examdata['PROJECT_MARKS'];
												$total_out_of_marks = $examdata['JOURNAL_MARKS_OUT_OF'] + $examdata['ONLINE_MARKS_OUT_OF'] + $examdata['PROJECT_MARKS_OUT_OF'];

												$total = $total_marks.'/'.$total_out_of_marks;
												if($total_marks == 0){
													$percentage = 0;
												}
												else{
													$percentage = round($total_marks / $total_out_of_marks * 100);
												}

												if($percentage > 39){
													if(($final_internal_percentage > 39) && ($final_external_percentage > 39) && ($final_practical_percentage > 39)){
														$result = "<b style='color:green'>Pass</b>";
													}
													else{
														$result = "<b style='color:red'>Fail</b>";
													}
													$percentage = "<b style='color:green'>".$percentage."</b>";
												}
												else{
													$result = "<b style='color:red'>Fail</b>";
													$percentage = "<b style='color:red'>".$percentage."</b>";
												}
											}

											if($booking_type == "practical"){
												if($examdata['EXAM_TYPE'] == "PRACTICAL"){
									?>
								<tr>
									<td><?php echo $examdata['EXAM_MASTER_NEW_ID'];?></td>
									<td><?php echo $examdata['EXAM_DATE'];?></td>
									<td><?php echo $examdata['EXAM_TIME'];?></td>
									<td><?php echo $examdata['COURSE_NAME'];?></td>
									<td><?php echo $final_internal_marks;?></td>
									<td><?php echo $total;?></td>
									<td><?php echo $percentage;?></td>
									<td><?php echo $result;?></td>
									<td><?php echo $examdata['EXAM_TYPE'];?></td>
									<td><?php echo $examdata['EXAM_FEES_RECEIPT_NO'];?></td>
									<td><span class="glyphicon glyphicon-print" data-exam_id="<?php echo $examdata['EXAM_MASTER_NEW_ID'];?>"></span></td>
								</tr>
							<?php } }else{?>
								<tr>
									<td><?php echo $examdata['EXAM_MASTER_NEW_ID'];?></td>
									<td><?php echo $examdata['EXAM_DATE'];?></td>
									<td><?php echo $examdata['EXAM_TIME'];?></td>
									<td><?php echo $examdata['COURSE_NAME'];?></td>
									<td><?php echo $final_internal_marks;?></td>
									<td><?php echo $final_external_marks;?></td>
									<td><?php echo $final_practical_marks;?></td>
									<td><?php echo $total;?></td>
									<td><?php echo $percentage;?></td>
									<td><?php echo $result;?></td>
									<td><?php echo $examdata['EXAM_TYPE'];?></td>
									<td><?php echo $examdata['EXAM_FEES_RECEIPT_NO'];?></td>
									<td><span class="glyphicon glyphicon-print" data-exam_id="<?php echo $examdata['EXAM_MASTER_NEW_ID'];?>"></span></td>
								</tr>
							<?php } }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
</div>
