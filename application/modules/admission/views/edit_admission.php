<div class="gapping"></div>
<div class="create_batch_form">
    <div id="box">
        <h2>Edit Admission</h2></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align:center; color:#F00;"></div>
                <div class="panel-body">
                   <?php echo form_open();
                         // echo validation_errors();
                        /* This Form is for convert Enquiry */

                        foreach($admissionData as $adm){
                        ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-4">
                                            <label>First Name: <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_FIRSTNAME" value="<?php echo $adm['ENQUIRY_FIRSTNAME']; ?>" id="ENQUIRY_FIRSTNAME " required="required" />
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_FIRSTNAME'); ?></span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Middle Name:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_MIDDLENAME" value="<?php echo $adm['ENQUIRY_MIDDLENAME']; ?>" id="ENQUIRY_MIDDLENAME" />
                                        </div>
                                        <div class="col-md-4">
                                            <label>Last Name: <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_LASTNAME" value="<?php echo $adm['ENQUIRY_LASTNAME']; ?>" id="ENQUIRY_LASTNAME" required="required" />
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_LASTNAME'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Flat/Room/Wing No.:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_ADDRESS1" id="ENQUIRY_ADDRESS1" value="<?php echo $adm['ENQUIRY_ADDRESS1']; ?>"> </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Bldg./Plot/Chawl No.:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_ADDRESS2" id="ENQUIRY_ADDRESS2" value="<?php echo $adm['ENQUIRY_ADDRESS2']; ?>"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-4">
                                            <label>State:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_STATE" value="MAHARASHTRA" readonly="readonly" id="ENQUIRY_STATE" /> </div>
                                         <div class="col-md-4">
                                            <label>City:</label>
                                            <select name="ENQUIRY_CITY" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1" <?php if($adm['ENQUIRY_CITY'] == '1'){echo 'selected';}else if($adm['ENQUIRY_CITY'] == 'MUMBAI'){echo 'selected';} ?> >MUMBAI</option>
                                                <option value="2" <?php if($adm['ENQUIRY_CITY'] == '2'){echo 'selected';}else if($adm['ENQUIRY_CITY'] == 'NAVI MUMBAI'){echo 'selected';} ?>>NAVI MUMBAI</option>
                                                <option value="3" <?php if($adm['ENQUIRY_CITY'] == '3'){echo 'selected';}else if($adm['ENQUIRY_CITY'] == 'THANE'){echo 'selected';} ?>>THANE</option>
                                                <option value="4" <?php if($adm['ENQUIRY_CITY'] == '4'){echo 'selected';}else if($adm['ENQUIRY_CITY'] == 'PUNE'){echo 'selected';} ?>>PUNE</option>
                                                <option value="5" <?php if($adm['ENQUIRY_CITY'] == '5'){echo 'selected';}else if($adm['ENQUIRY_CITY'] == 'NASHIK'){echo 'selected';} ?>>NASHIK</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Zip:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_ZIP" value="<?php echo $adm['ENQUIRY_ZIP']; ?>" id="ENQUIRY_ZIP" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                         <div class="col-md-6">
                                            <label>Stream</label>
                                             <select class="form-control" style="width:100% ;" name="STREAM" id="STREAM">
                                                            <option value="B">Basics</option>
                                                            <option value="P">Programming</option>
                                                            <option value="G">Graphics &amp; animation</option>
                                                            <option value="H">Hardware &amp; Networking</option>
                                                            <option value="O">others</option>
                                             </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Admission Date:(dd/mm/yyyy)</label>
                                            <?php $admDateRep = str_replace('/', '-', $adm['ADMISSION_DATE']);
                                          				$ADMISSION_DATE =  date("d/m/Y", strtotime($admDateRep) );?>
                                            <input class="form-control enquiry_date" type="text" name="ADMISSION_DATE" value="<?php echo $ADMISSION_DATE; ?>" id="ADMISSION_DATE" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                   <?php
                                            $course_id = explode(",",$adm['COURSE_TAKEN']);
                                            $module_id = explode(",",$adm['MODULE_TAKEN']);

                                            $course_min_fee = array();
                                            $course_max_fee = array();

                                            $module_min_fee = array();
                                            $module_max_fee = array();

                                            if (!empty($courses)) {
                                                foreach($courses as $course){
                                                    if(in_array($course['COURSE_ID'],$course_id)){
                                                        $course_min_fee[] = $course['MIN_FEES'];
                                                        $course_max_fee[] = $course['MAX_FEES'];
                                                    }
                                                }
                                            }

                                            if (!empty($modules)) {
                                                foreach ($modules as $module) {
                                                    if(in_array($module['MODULE_ID'],$module_id)){
                                                        $module_min_fee[] = $course['MIN_FEES'];
                                                        $module_max_fee[] = $course['MAX_FEES'];
                                                        }
                                                    }
                                            }

                                            $min_fee_sum = array_sum($course_min_fee) + array_sum($module_min_fee);
                                            $max_fee_sum = array_sum($course_max_fee) + array_sum($module_max_fee);

                                            if($min_fee_sum == 0){
                                              $min_fee_sum = $adm['MINIMUM_FEES'];
                                            }
                                            if($max_fee_sum == 0){
                                              $max_fee_sum = $adm['MAXIMUM_FEES'];
                                            }


                                    ?>
                                    <div class="col-md-12" style="margin-left:0px;">
                                        <div class="col-md-3">
                                             <label>Minimum Fees</label>
                                            <input type="text" readonly class="form-control min_fee" value="<?php echo $min_fee_sum; ?>" name="MINIMUM_FEES">
                                            Discount: <input type="checkbox" class="discount">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Maximum Fees</label>
                                            <input type="text" readonly class="form-control max_fee" value="<?php echo $max_fee_sum; ?>" name="MAXIMUM_FEES">
                                        </div>
                                        <div class="col-md-6">
                                             <label>Total Fees Payable(w/o ST): <span class="text-danger">*</span></label>
                                            <input type="text" value="<?php echo $adm['TOTALFEES']; ?>" class="form-control" name="TOTALFEES">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group discount_style" style="display:none;"></div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                <?php
                                        $downPayment = "";
                                        $downPaymentDate = "";
                                        $downPaymentDueAmount = "";
                                        if(array_key_exists('DUE_AMOUNT', $adm)){
                                                $downPayment = explode(",", $adm['DUE_AMOUNT']);
                                                $downPaymentDate = explode(",", $adm['DUEDATE']);

                                                $downPayment = $downPayment[0];
                                                $downPaymentDate = date("d/m/Y", strtotime($downPaymentDate[0]));
                                                $downPaymentDueAmount = $admissionData[0]['DUE_AMOUNT'];
                                        }
                                ?>
                                    <div class="col-md-12" style="margin-left:0px;">
                                        <div class="col-md-4">
                                             <label>Downpayment: <span class="text-danger">*</span></label>
                                            <input type="text" value="<?php echo $downPayment; ?>" class="form-control" name="DOWNPAYMENT">
                                        </div>
                                        <div class="col-md-4">
                                             <label>Downpayment Date: <span class="text-danger">*</span></label>
                                            <input type="text" value="<?php echo $downPaymentDate; ?>" class="form-control enquiry_date" name="DOWNPAYMENT_DATE">
                                        </div>
                                        <div class="col-md-4">
                                             <label>Monthly Payable: <span class="text-danger">*</span></label>
                                             <input type="text" value="<?php echo $downPaymentDueAmount; ?>" class="form-control" name="MONTHLY_PAY">
                                        </div>
                                    </div>
                                </div>
                            </div>
                           <!--  <div class="form-group discount_cover">
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                            </div> -->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                <label>Module Enrolled <span class="text-danger">*</span></label>

                                <select class="form-control module_enrolled" name="MODULE_INTERESTED[]" id="" multiple="">
                                    <?php foreach($modules as $module){
                                        $isSelected = in_array($module['MODULE_ID'],$module_id) ? "selected='selected'" : "";
                                        ?>
                                    <option value="<?php echo $module['MODULE_ID']; ?>" <?php echo $isSelected; ?>><?php echo $module['MODULE_NAME']; ?> </option>
                                    <?php } ?>
                                </select>
                                <span class="text-danger"><?php echo form_error('COURSE_INTERESTED'); ?></span>
                            </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-md-12">
                                       <ul class="list-group modules_seld">
                                       <?php
                                            $selected_course = explode(",",$adm['COURSE_TAKEN']);
                                            foreach($modules as $module){
                                            if(in_array($module['MODULE_ID'],$module_id)){ ?>
                                        <li class="list-group-item">
                                            <?php echo $module['MODULE_NAME']; ?>
                                        </li>
                                        <?php } } ?>
                                       </ul>
                                        <span class="text-danger"><?php echo form_error('COURSE_VALIDATOR'); ?></span>
                                   </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-4" style="float:right">
                                            <?php
                                            $ENQUIRY_DATEOFBIRTH = date("d/m/Y", strtotime($adm['ENQUIRY_DATEOFBIRTH']));
                                            // echo $ENQUIRY_DATEOFBIRTH;
                                             ?>
                                            <label>D.O.B: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control enquiry_date" name="ENQUIRY_DATEOFBIRTH" id="ENQUIRY_DATEOFBIRTH" value="<?php echo $ENQUIRY_DATEOFBIRTH; ?>">
                                             <span class="text-danger"><?php echo form_error('ENQUIRY_DATEOFBIRTH'); ?></span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Email: <span class="text-danger">*</span></label>
                                            <input type="email" class="form-control" name="ENQUIRY_EMAIL" value="<?php echo $adm['ENQUIRY_EMAIL']; ?>">
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_EMAIL'); ?></span>
                                        </div>
                                        <div class="col-md-4" style="float:right">
                                            <label>Education: <span class="text-danger">*</span></label>
                                            <select name="ENQUIRY_EDUCATION" class="form-control">
                                                <option value=''>Please Select</option>
                                                <?php foreach($educations as $education){ ?>
                                               <option value="<?php echo $education['CONTROLFILE_ID']; ?>" <?php if($adm['ENQUIRY_EDUCATION'] == $education['CONTROLFILE_ID']){ echo ($education['CONTROLFILE_ID'] == $adm['ENQUIRY_EDUCATION']) ? ' selected="selected"' : '';}else{ echo ($education['CONTROLFILE_VALUE'] == $adm['ENQUIRY_EDUCATION']) ? ' selected="selected"' : '';}?>><?php echo $education['CONTROLFILE_VALUE']; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_EDUCATION'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-4">
                                            <label>Occupation:</label>
                                            <select name="OCCUPATION" class="form-control">
                                                <option value="">Please Select</option>
                                                <?php foreach($occupations as $occupation){ ?>
                                                <option value="<?php echo $occupation['CONTROLFILE_ID']; ?>" <?php if(is_numeric($adm['OCCUPATION'])){echo ($occupation['CONTROLFILE_ID'] == $adm['OCCUPATION']) ? ' selected="selected"' : ''; }else{ echo ($occupation['CONTROLFILE_VALUE'] == $adm['OCCUPATION']) ? ' selected="selected"' : ''; }?>><?php echo $occupation['CONTROLFILE_VALUE']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Enquiry Date:</label>
                                            <input type="text" id="ENQUIRY_DATE" class="form-control enquiry_date" name="ENQUIRY_DATE" value="<?php $ENQUIRY_DATE = date("d/m/Y", strtotime($adm['ENQUIRY_DATE'])); echo $ENQUIRY_DATE; ?>">
                                            </div>
                                        <div class="col-md-4">
                                            <label>Enquiry Form No.</label>
                                            <input class="form-control" type="text" name="ENQUIRY_FORM_NO" value="<?php echo $adm['ENQUIRY_FORM_NO']; ?>" id="ENQUIRY_FORM_NO">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6" style="float:right">
                                            <label>Mobile No.(Work/Home): <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_MOBILE_NO" value="<?php if($adm['ENQUIRY_MOBILE_NO'] == 0){ echo "";}else{ echo $adm['ENQUIRY_MOBILE_NO'];} ?>" id="ENQUIRY_MOBILE_NO" maxlength="10">
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_MOBILE_NO'); ?></span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Parent's No: <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_PARENT_NO" value="<?php $mobile_no=""; if($adm['ENQUIRY_PARENT_NO'] != ""){ $mobile_no=$adm['ENQUIRY_PARENT_NO'];}else{ $mobile_no=$adm['PARENT_MOBILE_NUMBER'];} if($mobile_no == 0){ echo "";}else{ echo $mobile_no;} ?>" id="ENQUIRY_PARENT_NO" maxlength="10">
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_PARENT_NO'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                         <div class="col-md-4">
                                            <label>Centre:</label>
                                            <select disabled name="CENTRE_ID" class="form-control">
                                                    <option value="<?php echo $adm['CENTRE_ID'] ?>"><?php echo $adm['CENTRE_NAME'] ?></option>
                                                </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Admission Type:</label>
                                            <select name="ADMISSION_TYPE" class="form-control">
                                                <option value="">Select Admission Type</option>
                                                <option value="1" <?php if($adm['ADMISSION_TYPE'] == '1'){echo 'selected';}else if($adm['ADMISSION_TYPE'] == 'REGULAR'){echo 'selected';} ?>>Regular</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Handeled By: <span class="text-danger">*</span></label>
                                            <select name="ENQUIRY_HANDELED_BY" class="form-control">
                                                   <?php foreach($employees as $employee){
                                                     $selection = "";
                                                      if($adm['HANDLED_BY'] == $employee['EMPLOYEE_ID'])
                                                      {
                                                       $selection = "selected='selected';";
                                                      }
                                                       ?>
                                                    <option value="<?php echo $employee['EMPLOYEE_ID'] ?>" <?php echo $selection;?>>
                                                        <?php echo $employee['EMP_FNAME'].' '.$employee['EMP_LASTNAME'] ?>
                                                    </option>
                                                    <?php } ?>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('ENQUIRY_HANDELED_BY'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-4">
                                            <label>Intake: <span class="text-danger">*</span></label>
                                            <select name="IN_TAKE" id="" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="JANUARY" <?php if($adm['IN_TAKE'] == '1'){echo 'selected';}elseif ($adm['IN_TAKE'] == 'JANUARY') {echo 'selected';} ?>>JANUARY</option>
                                                <option value="FEBRUARY" <?php if($adm['IN_TAKE'] == '2'){echo 'selected';}elseif ($adm['IN_TAKE'] == 'FEBRUARY') {echo 'selected';} ?>>FEBRUARY</option>
                                                <option value="MARCH" <?php if($adm['IN_TAKE'] == '3'){echo 'selected';}elseif ($adm['IN_TAKE'] == 'MARCH') {echo 'selected';} ?> >MARCH</option>
                                                <option value="APRIL" <?php if($adm['IN_TAKE'] == '4'){echo 'selected';}elseif ($adm['IN_TAKE'] == 'APRIL') {echo 'selected';} ?>>APRIL</option>
                                                <option value="MAY" <?php if($adm['IN_TAKE'] == '5'){echo 'selected';}elseif ($adm['IN_TAKE'] == 'MAY') {echo 'selected';} ?> >MAY</option>
                                                <option value="JUNE" <?php if($adm['IN_TAKE'] == '6'){echo 'selected';}elseif ($adm['IN_TAKE'] == 'JUNE') {echo 'selected';} ?> >JUNE</option>
                                                <option value="JULY" <?php if($adm['IN_TAKE'] == '7'){echo 'selected';}elseif ($adm['IN_TAKE'] == 'JULY') {echo 'selected';} ?> >JULY</option>
                                                <option value="AUGUST" <?php if($adm['IN_TAKE'] == '8'){echo 'selected';}elseif ($adm['IN_TAKE'] == 'AUGUST') {echo 'selected';} ?> >AUGUST</option>
                                                <option value="SEPTEMBER" <?php if($adm['IN_TAKE'] == '9'){echo 'selected';}elseif ($adm['IN_TAKE'] == 'SEPTEMBER') {echo 'selected';} ?> >SEPTEMBER</option>
                                                <option value="OCTOBER" <?php if($adm['IN_TAKE'] == '10'){echo 'selected';}elseif ($adm['IN_TAKE'] == 'OCTOBER') {echo 'selected';} ?> >OCTOBER</option>
                                                <option value="NOVEMBER" <?php if($adm['IN_TAKE'] == '11'){echo 'selected';}elseif ($adm['IN_TAKE'] == 'NOVEMBER') {echo 'selected';} ?> >NOVEMBER</option>
                                                <option value="DECEMBER" <?php if($adm['IN_TAKE'] == '12'){echo 'selected';}elseif ($adm['IN_TAKE'] == 'DECEMBER') {echo 'selected';} ?> >DECEMBER</option>

                                            </select>
                                            <br>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Academic Year: <span class="text-danger">*</span></label>
                                            <select name="ACAD_YEAR" id="" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1" <?php if($adm['YEAR'] == '1'){echo 'selected';}elseif($adm['YEAR'] == '2014-2015'){echo 'selected';} ?>>2014-2015</option>
                                                <option value="2" <?php if($adm['YEAR'] == '2'){echo 'selected';}elseif($adm['YEAR'] == '2015-2016'){echo 'selected';} ?>>2015-2016</option>
                                                <option value="3" <?php if($adm['YEAR'] == '3'){echo 'selected';}elseif($adm['YEAR'] == '2016-2017'){echo 'selected';} ?>>2016-2017</option>
                                                <option value="4" <?php if($adm['YEAR'] == '4'){echo 'selected';}elseif($adm['YEAR'] == '2017-2018'){echo 'selected';} ?>>2017-2018</option>
                                                <option value="5" <?php if($adm['YEAR'] == '5'){echo 'selected';}elseif($adm['YEAR'] == '2018-2019'){echo 'selected';} ?>>2018-2019</option>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('ACAD_YEAR'); ?></span>
                                        </div>
                                        <div class="col-md-4">
                                            <label>University name: <span class="text-danger">*</span></label>
                                            <select name="UNIVERSITY_NAME" id="" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1" <?php if($adm['UNIVERSITY_NAME'] == '1'){echo 'selected';}elseif ($adm['UNIVERSITY_NAME'] == 'SAIP') {echo 'selected';} ?>>SAIP</option>
                                                <option value="2" <?php if($adm['UNIVERSITY_NAME'] == '2'){echo 'selected';}elseif ($adm['UNIVERSITY_NAME'] == 'YCMOU') {echo 'selected';} ?>>YCMOU</option>
                                                <option value="3" <?php if($adm['UNIVERSITY_NAME'] == '3'){echo 'selected';}elseif ($adm['UNIVERSITY_NAME'] == 'ANNAMALAI') {echo 'selected';} ?> >ANNAMALAI</option>
                                                <option value="4" <?php if($adm['UNIVERSITY_NAME'] == '4'){echo 'selected';}elseif ($adm['UNIVERSITY_NAME'] == 'SMU') {echo 'selected';} ?>>SMU</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-6">
                                            <label>Reference Given By:</label>
                                            <input class="form-control" type="text" name="REFERENCE_GIVEN_BY" value="<?php echo $adm['REFERENCE_GIVEN_BY']; ?>" id="REFERENCE_GIVEN_BY">
                                        </div>

                                        <div class="col-md-6">
                                            <label>Remarks: <span class="text-danger"></span></label>
                                            <input class="form-control" type="text" name="REMARKS" value="<?php echo $adm['REMARKS']; ?>" list="comment_type" id="REMARKS" autocomplete="off">
                                            <datalist id="comment_type">
                                              <option value="Course Changes">
                                              <option value="University Fees Deductions">
                                            </datalist>
                                            <span class="text-danger"><?php echo form_error('REMARKS'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                <label>Course Enrolled <span class="text-danger">*</span></label>
                                <select class="form-control course_enrolled" style="width:100%;height:158px" name="COURSE_INTERESTED[]" id="COURSE_INTERESTED" multiple="">
                                    <?php
                                        $selected_course = explode(",",$adm['COURSE_TAKEN']);
                                        foreach($courses as $course){
                                        $isSelected = in_array($course['COURSE_ID'],$course_id) ? "selected='selected'" : "";
                                    ?>
                                        <option value="<?php echo $course['COURSE_ID']; ?>" <?php echo $isSelected; ?>><?php echo $course['COURSE_NAME']; ?></option>
                                    <?php } ?>
                                    <option value=''>Other</option>
                                </select>
                                <span class="text-danger"><?php echo form_error('COURSE_INTERESTED'); ?></span>
                            </div>
                                    </div>
                                </div>
                                <div class="row">
                                   <div class="col-md-12">
                                       <ul class="list-group courses_seld">
                                       <?php
                                            $selected_course = explode(",",$adm['COURSE_TAKEN']);
                                            foreach($courses as $course){
                                            if(in_array($course['COURSE_ID'],$course_id)){ ?>
                                        <li class="list-group-item">
                                            <?php echo $course['COURSE_NAME'];; ?>
                                        </li>
                                        <?php } } ?>
                                       </ul>
                                   </div>
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <div class="row">
                                   <div class="col-md-12">
                                       <button type='submit' class='btn btn-primary'>Update</button>
                                   </div>
                                </div>
                            </div>
                        </div>
                        <?php break; }
                        echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
