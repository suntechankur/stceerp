
  <div id="box">
        <h2 class="text-center"> Edit Installment </h2>
  </div>

  <div class="panel panel-default">
    <div class="panel-body table-responsive">
    <?php echo form_open(); ?>
      <table class="table table-striped installmentDet">
    <?php
      if(!empty($admission_data)){
        $balanceInstallmentCalc = $admission_data[0]['TOTALFEES'] - $admission_data[0]['DUE_AMOUNT'];
    ?>
    <thead>
      <tr>
        <th>Installment Pay Date</th>
        <th>Installment Fees Paid</th>
        <th>Installment Fees Pending</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody class="installmentTable">
    <?php $installmentCalcAuto = $balanceInstallmentCalc; ?>
     <tr>
       <td>
        <?php $dueDate = date("d/m/Y", strtotime($admission_data[0]['DUEDATE'])); ?>
        <input type="text" value="<?php echo $dueDate; ?>" name="due_date[]" class="enquiry_date form-control">
      </td>
       <td><input type="text" value="<?php echo $admission_data[0]['DUE_AMOUNT']; ?>" name="due_amount" class="form-control due_amount"></td>
       <td class="feePending"><?php $firstInstallment = $admission_data[0]['TOTALFEES'] - $admission_data[0]['DUE_AMOUNT']; ?>
          <?php echo $firstInstallment; ?>
        </td>
        <td>
          <div class="row">
            <div class="col-md-4">
              <button class="btn btn-info saveInstallment" type="button" data-adm_inst_id="<?php echo $admission_data[0]['ADMISSION_INSTALLEMENT_ID'] ?>" data-adm_id="<?php echo $admission_data[0]['ADMISSION_ID']; ?>"><i class="fa fa-floppy-o"></i> Save </button>
            </div>
            <div class="col-md-4"></div>
          </div>
          </td>
     </tr>
     <?php 
     $admCount = count($admission_data) - 1;
     for ($i=1; $i <count($admission_data) ; $i++) { ?>
       <tr>
         <td>
          <?php $dueDate = date("d/m/Y", strtotime($admission_data[$i]['DUEDATE'])); ?>
          <input type="text" value="<?php echo $dueDate; ?>" name="due_date[]" class="form-control enquiry_date">
         </td>
         <td><input type="text" value="<?php echo $admission_data[$i]['DUE_AMOUNT']; ?>" name="due_amount" class="form-control insAmt due_amount"></td>
         <td class="feePending"><?php 
          $installmentCalcAuto = $installmentCalcAuto - $admission_data[$i]['DUE_AMOUNT']; 
          echo $installmentCalcAuto;
         ?>
         </td>
         <td>
          <!-- <button class="btn btn-danger delInstallment" type="button"><i class="fa fa-trash"></i> Delete</button> -->
            <div class="row">
              <div class="col-md-4">
                <button class="btn btn-info saveInstallment" type="button" data-adm_inst_id="<?php echo $admission_data[$i]['ADMISSION_INSTALLEMENT_ID']; ?>" data-adm_id="<?php echo $admission_data[$i]['ADMISSION_ID']; ?>"><i class="fa fa-floppy-o"></i> Save </button>
              </div>
              <div class="col-md-4">
                <button class="btn btn-danger delInstallment" data-adm_inst_id="<?php echo $admission_data[$i]['ADMISSION_INSTALLEMENT_ID']; ?>" type="button"><i class="fa fa-trash"></i> Delete</button>
              </div>
              <div class="col-md-4"></div>
            </div>
        </td>
       </tr>
     <?php } ?>
        <tr>
          <td>
            <?php 
              $dueDate = date("d/m/Y", strtotime($admission_data[$admCount]['DUEDATE']."+1 Months"));
            ?>
            <input type="text" class="form-control enquiry_date" value="<?php echo $dueDate; ?>" name="due_date[]">
          </td>
          <td>
            <input type="text" class="form-control insAmt due_amount" value="<?php echo $installmentCalcAuto; ?>" name="due_amount">
          </td>
          <td class="feePending"><?php echo '0'; ?></td>
          <td>
            <div class="row">
                <div class="col-md-4">
                  <button class="btn btn-info saveInstallment" type="button" data-adm_inst_id="" data-adm_id="<?php echo $admission_data[0]['ADMISSION_ID']; ?>"><i class="fa fa-floppy-o"></i> Save </button>
                </div>
                <div class="col-md-4">
                  <button class="btn btn-danger delInstallment" type="button"><i class="fa fa-trash"></i> Delete</button>
              </div>
                <div class="col-md-4">
                  <button class="btn btn-primary addInstallment" type="button">Add Details</button>
                </div>
            </div>
          </td>
        </tr>
    </tbody>
    <?php }
    else{ ?>
    <thead>
      <tr>
        <th>Installment Pay Date</th>
        <th>Installment Fees Paid</th>
        <th>Installment Fees Pending</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody class="installmentTable">
    <?php $installmentCalcAuto = $balanceInstallmentCalc; ?>
     <tr>
       <td>
        <?php $dueDate = date("d/m/Y", strtotime($admission_data[0]['DUEDATE'])); ?>
        <input type="text" value="<?php echo $dueDate; ?>" name="due_date[]" class="enquiry_date form-control">
      </td>
       <td><input type="text" value="<?php echo $admission_data[0]['DUE_AMOUNT']; ?>" name="due_amount" class="form-control due_amount"></td>
       <td class="feePending"><?php $firstInstallment = $admission_data[0]['TOTALFEES'] - $admission_data[0]['DUE_AMOUNT']; ?>
          <?php echo $firstInstallment; ?>
        </td>
        <td>
          <div class="row">
            <div class="col-md-4">
              <button class="btn btn-info saveInstallment" type="button" data-adm_inst_id="<?php echo $admission_data[0]['ADMISSION_INSTALLEMENT_ID'] ?>" data-adm_id="<?php echo $admission_data[0]['ADMISSION_ID']; ?>"><i class="fa fa-floppy-o"></i> Save </button>
            </div>
            <div class="col-md-4"></div>
          </div>
          </td>
     </tr>
     <?php 
     $admCount = count($admission_data) - 1;
     for ($i=1; $i <count($admission_data) ; $i++) { ?>
       <tr>
         <td>
          <?php $dueDate = date("d/m/Y", strtotime($admission_data[$i]['DUEDATE'])); ?>
          <input type="text" value="<?php echo $dueDate; ?>" name="due_date[]" class="form-control enquiry_date">
         </td>
         <td><input type="text" value="<?php echo $admission_data[$i]['DUE_AMOUNT']; ?>" name="due_amount" class="form-control insAmt due_amount"></td>
         <td class="feePending"><?php 
          $installmentCalcAuto = $installmentCalcAuto - $admission_data[$i]['DUE_AMOUNT']; 
          echo $installmentCalcAuto;
         ?>
         </td>
         <td>
          <!-- <button class="btn btn-danger delInstallment" type="button"><i class="fa fa-trash"></i> Delete</button> -->
            <div class="row">
              <div class="col-md-4">
                <button class="btn btn-info saveInstallment" type="button" data-adm_inst_id="<?php echo $admission_data[$i]['ADMISSION_INSTALLEMENT_ID']; ?>" data-adm_id="<?php echo $admission_data[$i]['ADMISSION_ID']; ?>"><i class="fa fa-floppy-o"></i> Save </button>
              </div>
              <div class="col-md-4">
                <button class="btn btn-danger delInstallment" data-adm_inst_id="<?php echo $admission_data[$i]['ADMISSION_INSTALLEMENT_ID']; ?>" type="button"><i class="fa fa-trash"></i> Delete</button>
              </div>
              <div class="col-md-4"></div>
            </div>
        </td>
       </tr>
     <?php } ?>
        <tr>
          <td>
            <?php 
              $dueDate = date("d/m/Y", strtotime($admission_data[$admCount]['DUEDATE']."+1 Months"));
            ?>
            <input type="text" class="form-control enquiry_date" value="<?php echo $dueDate; ?>" name="due_date[]">
          </td>
          <td>
            <input type="text" class="form-control insAmt due_amount" value="<?php echo $installmentCalcAuto; ?>" name="due_amount">
          </td>
          <td class="feePending"><?php echo '0'; ?></td>
          <td>
            <div class="row">
                <div class="col-md-4">
                  <button class="btn btn-info saveInstallment" type="button" data-adm_inst_id="" data-adm_id="<?php echo $admission_data[0]['ADMISSION_ID']; ?>"><i class="fa fa-floppy-o"></i> Save </button>
                </div>
                <div class="col-md-4">
                  <button class="btn btn-danger delInstallment" type="button"><i class="fa fa-trash"></i> Delete</button>
              </div>
                <div class="col-md-4">
                  <button class="btn btn-primary addInstallment" type="button">Add Details</button>
                </div>
            </div>
          </td>
        </tr>
    </tbody>
    <?php
      //echo "<tr><td>No Installment Plan Set.</td></tr>";
      }?>
  </table> 
      <!-- <button class="btn btn-primary col-md-offset-4" type="submit">Save</button> -->
  <?php echo form_close(); ?>     
    </div>
  </div>
