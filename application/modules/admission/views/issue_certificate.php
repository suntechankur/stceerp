<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Issue Certificate</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?php if($this->session->flashdata('success')) { ?>
						<div class="alert alert-success">
						<strong> <?php echo $this->session->flashdata('success'); ?></strong>
						</div>
						<?php } ?>
						<?php if($this->session->flashdata('danger')) { ?>
						<div class="alert alert-danger">
						<strong> <?php echo $this->session->flashdata('danger'); ?></strong>
						</div>
						<?php } ?>
					</div>
					<?php
						foreach($studData as $sdata){
						?>
					<div class="panel-body">
						<?php echo form_open(); ?>
							<div class="col-md-12">

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Addmission Id: <?php echo $sdata['ADMISSION_ID']?></label>
												<input type="hidden" name="ADMISSION_ID" value="<?php echo $sdata['ADMISSION_ID']?>">
											</div>
											<div class="col-lg-4">
												<label>Full Name: <?php echo $sdata['ENQUIRY_FIRSTNAME']." ".$sdata['ENQUIRY_MIDDLENAME']." ".$sdata['ENQUIRY_LASTNAME']?></label>
											</div>
											<div class="col-lg-4">
												<label>Centre Name: <?php echo $sdata['CENTRE_NAME']?></label>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Admission Date: <?php $date=date_create($sdata['ADMISSION_DATE']); echo date_format($date,"d/m/Y");?></label>
											</div>
											<div class="col-lg-4">
												<?php
                          $courses_arr = explode(",",$sdata['COURSE_TAKEN']);
                               $courseNames = '';
                                for($j=0;$j<count($courses);$j++){
                                    if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
                                          $courseNames .= ','.$courses[$j]['COURSE_NAME'];
                                        $sdata['COURSE_TAKEN'] = trim($courseNames,",");
                                      }

                                }
                          ?>
                          <?php
                          $modules_arr = explode(",",$sdata['MODULE_TAKEN']);
                               $moduleNames = '';
                                for($j=0;$j<count($modules);$j++){
                                    if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
                                          $moduleNames .= ','.$modules[$j]['MODULE_NAME'];
                                        $sdata['MODULE_TAKEN'] = trim($moduleNames,",");
                                      }

                                }
															 $sdata['COURSE_TAKEN'] = trim($sdata['MODULE_TAKEN'].",".$sdata['COURSE_TAKEN'],",");
															 unset($sdata['MODULE_TAKEN']);
                          ?>
												<label>Course Taken : <?php echo $sdata['COURSE_TAKEN']?></label>
											</div>
											<div class="col-lg-4">
												<label>Admission Type : <?php echo $sdata['ADMISSION_TYPE']?></label>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-md-4">
												<label>Fees Charged : <?php echo $sdata['TOTALFEES']?></label>
											</div>
											<div class="col-md-4">
													<label>Fees Paid : <?php echo $sdata['FEE_PAID']?></label>
											</div>
											<div class="col-md-4">
												<label for="">Balance Fees : <?php echo $sdata['TOTALFEES']-$sdata['FEE_PAID']?></label>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">

												<div class="col-md-6">
													<label>Module : <span class="text-danger">*</span></label>
													<select name="MODULE_ID" class="form-control">
														<option value="" selected disabled>Select Module Name</option>
														<?php foreach($allCourseDetails as $course){?>
																			<option value="<?php echo $course['COURSE_ID'];?>"><?php echo $course['COURSE_NAME'];?></option>
														<?php } ?>
													</select>
												</div>
												<div class="col-md-6">
													<label>Subject Name : <span class="text-danger">*</span></label>
													<select name="COURSE_ID" class="form-control">
														<option value="" selected disabled>Select Subject</option>
														<?php foreach($courseDetails as $course){
																		if(!(in_array($course['COURSE_ID'], $examdetails))){?>
																			<option value="<?php echo $course['COURSE_ID'];?>"><?php echo $course['COURSE_NAME'];?></option>
														<?php } } ?>
													</select>
												</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">

												<div class="col-md-4">
													<label>Certificate Issue Date : <span class="text-danger">*</span></label>
													<input type="text" class="form-control" value="<?php echo date("d/m/Y");?>" readonly>
													<input type="hidden" class="form-control" name="ISSUE_DATE" value="<?php echo date("Y-m-d");?>">
												</div>
												<div class="col-md-4">
													<label>Certificate Issued By : <span class="text-danger">*</span></label>
													<select name="ISSUED_BY" class="form-control">
														<option value="" selected disabled>Certificate Issuer</option>
														<option value="3170">Ankur Prajapati</option>
														<option value="3164">Ramkishor Sharma</option>
													</select>
												</div>

										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">

												<div class="col-md-4">
												</div>
												<div class="col-md-4">
													<input class="admToken" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
													<input type="hidden" class="exam_id_for_certificate" name="EXAM_MASTER_NEW_ID">
													<input type="hidden" name="MODIFIED_BY" value="<?php echo $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'];?>">
													<input type="hidden" name="MODIFIED_DATE" value="<?php echo date("Y-m-d");?>">
													<button type="submit" name="save_certificate_data" class="btn btn-primary">Save</button>
												</div>

										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">

											<div class="panel panel-default">
												<div class="panel-body table-responsive">
													<table class="table table-striped table-hover">
														<thead>
															<tr>
																<th>Course</th>
																<th>Issue Date</th>
																<th>Certificate</th>
																<th>Marksheet</th>
															</tr>
														</thead>
														<tbody>
															<?php foreach($allSavedCertificateDetails as $certificate_data){?>
																<tr>
																	<td>
																		<?php foreach($allCourseDetails as $course){
																						if($course['COURSE_ID'] == $certificate_data['MODULE_ID']){
																							echo $course['COURSE_NAME'];
																						}
																		}?>
																	</td>
																	<td><?php echo $certificate_data['ISSUE_DATE'];?></td>
																	<td><a href="<?php echo base_url('admission/print-certificate/'.$this->encrypt->encode($certificate_data['CERTIFICATE_ID']).'/certificate');?>" target="_blank"><i class="fa fa-address-card fa-2x"></i></a></td>
																	<td><a href="<?php echo base_url('admission/print-marksheet/'.$this->encrypt->encode($certificate_data['CERTIFICATE_ID']).'/marksheet');?>" target="_blank"><i class="fa fa-address-book fa-2x"></i></a></td>
																</tr>
															<?php } ?>
														</tbody>
													</table>
												</div>
											</div>

										</div>
									</div>
								</div>

								</div>
							</div>
						 <?php echo form_close(); ?>
						</div>
					<?php }?>
					</div>
				</div>
			</div>
		<br/>
		<div id="box"><h2></h2></div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body table-responsive">
						<div class="col-md-4"><button type="submit" class="btn btn-primary merge_exam_data" style="width:160px;">Merge Exam Data</button></div>
						<input type="hidden" class="examids">
						<br/><br/>
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>Select</th>
									<th>Exam Id</th>
									<th>Exam Date</th>
									<th>Exam Time</th>
									<th>Subject</th>
									<th>Exam Type</th>
									<th>Internal Exam Marks</th>
									<th>External Exam Marks</th>
									<th>Practical Exam Marks</th>
									<th>Total Marks</th>
									<th>Percentage</th>
									<th>Result</th>
								</tr>
							</thead>
							<tbody>
								<?php

								foreach($examdetails as $examdata){
											$final_internal_marks = 0;
											$final_external_marks = 0;

											if($examdata['JOURNAL_MARKS'] == ""){
												$examdata['JOURNAL_MARKS'] = 0;
											}
											if($examdata['JOURNAL_MARKS_OUT_OF'] == ""){
												$examdata['JOURNAL_MARKS_OUT_OF'] = 0;
											}

											$final_internal_marks = $examdata['JOURNAL_MARKS'].'/'.$examdata['JOURNAL_MARKS_OUT_OF'];

											if($examdata['ONLINE_MARKS'] == ""){
												$examdata['ONLINE_MARKS'] = 0;
											}
											if($examdata['ONLINE_MARKS_OUT_OF'] == ""){
												$examdata['ONLINE_MARKS_OUT_OF'] = 0;
											}

											$final_external_marks = $examdata['ONLINE_MARKS'].'/'.$examdata['ONLINE_MARKS_OUT_OF'];

											if($examdata['PROJECT_MARKS'] == ""){
												$examdata['PROJECT_MARKS'] = 0;
											}
											if($examdata['PROJECT_MARKS_OUT_OF'] == ""){
												$examdata['PROJECT_MARKS_OUT_OF'] = 0;
											}

											$final_project_marks = $examdata['PROJECT_MARKS'].'/'.$examdata['PROJECT_MARKS_OUT_OF'];

											$total_marks = $examdata['JOURNAL_MARKS'] + $examdata['ONLINE_MARKS'] + $examdata['PROJECT_MARKS'];
											$total_out_of_marks = $examdata['JOURNAL_MARKS_OUT_OF'] + $examdata['ONLINE_MARKS_OUT_OF'] + $examdata['PROJECT_MARKS_OUT_OF'];

											if($examdata['JOURNAL_MARKS'] == 0){
												$journal_percentage = 0;
											}else{
												$journal_percentage = round($examdata['JOURNAL_MARKS'] / $examdata['JOURNAL_MARKS_OUT_OF'] * 100);
											}
											if($examdata['ONLINE_MARKS'] == 0){
												$online_percentage = 0;
											}else{
												$online_percentage = round($examdata['ONLINE_MARKS'] / $examdata['ONLINE_MARKS_OUT_OF'] * 100);
											}
											if($examdata['PROJECT_MARKS'] == 0){
												$project_percentage = 0;
											}else{
												$project_percentage = round($examdata['PROJECT_MARKS'] / $examdata['PROJECT_MARKS_OUT_OF'] * 100);
											}

											$total = $total_marks.'/'.$total_out_of_marks;
											if($total_marks == 0){
												$percentage = 0;
											}
											else{
												$percentage = round($total_marks / $total_out_of_marks * 100);
											}

											if(($journal_percentage > 39) && ($online_percentage > 39) && ($project_percentage > 39)){
												$result = "<label style='color:green;'>Pass</label>";
											}
											else{
												$result = "<label style='color:red;'>Fail</label>";
											}

									?>
								<tr>
									<td><input type="checkbox" class="form-control select_exam_id" data-examId="<?php echo $examdata['EXAM_MASTER_NEW_ID'];?>"></td>
									<td><?php echo $examdata['EXAM_MASTER_NEW_ID'];?></td>
									<td><?php echo $examdata['EXAM_DATE'];?></td>
									<td><?php echo $examdata['EXAM_TIME'];?></td>
									<td><?php echo $examdata['COURSE_NAME'];?></td>
									<td><?php echo $examdata['EXAM_TYPE'];?></td>
									<td><?php echo $final_internal_marks;?></td>
									<td><?php echo $final_external_marks;?></td>
									<td><?php echo $final_project_marks;?></td>
									<td><?php echo $total;?></td>
									<td><?php echo $percentage;?></td>
									<td><?php echo $result;?></td>
								</tr>
							<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
</div>
