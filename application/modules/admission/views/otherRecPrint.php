<?php
$payDate = date('d/m/Y',strtotime($penaltyRecData[0]['PAYMENT_DATE']));
tcpdf();
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
// $pdf->SetAuthor('Nicola Asuni');
// $pdf->SetTitle('TCPDF Example 001');

// set default header data
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont('helvetica');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->SetFont('helvetica', '', 9);
$pdf->setFontSubsetting(false);
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();
switch ($penaltyRecData[0]['PAYMENT_TYPE']) {
	case '1':
		$payType = 'CASH';
		$payTxt = '';
		break;
	case '2':
		$payType = 'CC';
		$chequeDate = date("d/m/Y", strtotime($penaltyRecData[0]['CHEQUE_DATE']));
		$payTxt = '<span>by Cheque/D.D./TID No.<b><u>'.$penaltyRecData[0]['CHEQUE_NO'].'</u></b> dated <b><u>'.$chequeDate.'</u></b> drawn on bank <b><u>'.$penaltyRecData[0]['CHEQUE_BANK_NAME'].'</u></b> Branch <b><u>'.$penaltyRecData[0]['CHEQUE_BRANCH'].'</u></b></span>';
		break;
	case '3':
		$payType = 'CHEQUE';
		$chequeDate = date("d/m/Y", strtotime($penaltyRecData[0]['CHEQUE_DATE']));
		$payTxt = '<span>by Cheque/D.D./TID No.<b><u>'.$penaltyRecData[0]['CHEQUE_NO'].'</u></b> dated <b><u>'.$chequeDate.'</u></b> drawn on bank <b><u>'.$penaltyRecData[0]['CHEQUE_BANK_NAME'].'</u></b> Branch <b><u>'.$penaltyRecData[0]['CHEQUE_BRANCH'].'</u></b></span>';
		break;
	default:
		$payType = 'Other';
		$payTxt = '';
		break;
}

$admRecHtml = '<table style="WIDTH:500px;HEIGHT:176px" border="0" cellspacing="0" cellpadding="2">
<tbody>
<tr>
<td align="left">Branch : <span>'.$penaltyRecData[0]['CENTRE_NAME'].'</span> </td>
<td style="WIDTH:209px" align="center"><u>St. Angelo&#39;s Computers Ltd. </u><br><span>'.$payType.' Receipt cum Invoice</span> </td>
<td align="left">Receipt No:<span style="TEXT-DECORATION:underline">'.$penaltyRecData[0]['PENALTY_RECEIPT_NO'].' </span><br>Receipt Date:<span>'.$payDate.'</span></td></tr>
<tr>
<td colspan="3" align="center">
<hr style="COLOR:black" color="black" size="1" width="100%">
</td></tr>
<tr>
<td style="HEIGHT:77px" valign="top" colspan="3" align="left">
<p style="LINE-HEIGHT:15pt;LETTER-SPACING:0px" align="justify">Received with thanks from Mr./Mrs./Miss  <span style="FONT-WEIGHT:bold;TEXT-DECORATION:underline">'.$penaltyRecData[0]['ENQUIRY_FIRSTNAME'].' '.$penaltyRecData[0]['ENQUIRY_LASTNAME'].'</span> a total  <span>'.$payType.' Receipt cum Invoice</span> amount (Course Fees + Service Tax) of <strong><span style="TEXT-DECORATION:underline">Rs.</span></strong><span style="FONT-WEIGHT:bold;TEXT-DECORATION:underline">'.$penaltyRecData[0]['AMOUNT'].'</span>/- ( <span style="FONT-WEIGHT:bold;TEXT-DECORATION:underline">'.numToStr($penaltyRecData[0]['AMOUNT']).'</span><span style="TEXT-DECORATION:underline"> <strong>Only</strong></span> ) out of which Course Fees paid is <span style="TEXT-DECORATION:underline"><strong>Rs.</strong></span><span style="FONT-WEIGHT:bold;TEXT-DECORATION:underline">'.$penaltyRecData[0]['AMOUNT_WITHOUT_ST'].'</span>/- and Service Tax Paid is <strong><span style="TEXT-DECORATION:underline">Rs.</span></strong><span style="FONT-WEIGHT:bold;TEXT-DECORATION:underline">'.$penaltyRecData[0]['SERVICE_TAX_PAID'].'</span>/- <strong>@</strong> <span style="FONT-WEIGHT:bold">'.$penaltyRecData[0]['SERVICE_TAX'].'</span><strong>%</strong> '.$payTxt.' towards payment for  <span style="TEXT-DECORATION:underline"><b>'.$penaltyRecData[0]['CONTROLFILE_VALUE'].' FOR '.$penaltyRecData[0]['COURSE'].'</b></span> course .<br></p></td></tr>
<tr>
<td colspan="3" align="left">
<table style="WIDTH:394px;HEIGHT:136px" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="TEXT-ALIGN:justify;WIDTH:375px;HEIGHT:66px" valign="top">
<ul>
<li style="TEXT-ALIGN:left">Fees once paid will not be refunded under any Circumstances 
<li style="TEXT-ALIGN:left">Central /State Govt.taxes/levies are charged as Applicable 
<li style="TEXT-ALIGN:left">This receipt is valid subject to Realization of Cheque/DD 
<li style="TEXT-ALIGN:left">This receipt has to be produced while collecting the Certificate 
<li style="TEXT-ALIGN:left">For any query or complaint call Helpline:9324433322 
<li style="TEXT-ALIGN:left">Students have to attend certain special subjects batches at <br>select centers only 
<li style="TEXT-ALIGN:left">Students have to pay their monthly installment within 30 days. 
<li style="TEXT-ALIGN:left">Late payment of installment will attract penalty of Rs.100 per day 
<li style="TEXT-ALIGN:left">Please note, in all cases provision of computer training will be done only after receiving fees in advance. 
<li style="TEXT-ALIGN:left">Kindly note that all special commitments given to student with regards to Fees, Batch timings, Center, University, Degree or any other needs to be mentioned in writing by the Front office staff on the company Letter Head. Else it shall not be fulfilled. </li></li></li></li></li></li></li></li></li></li></ul></td>
<td style="HEIGHT:66px" valign="top">
<p><br> <br> Sign: ________________</p>
<p> Recieved by: <br> <span style="FONT-WEIGHT:bold">'.$penaltyRecData[0]['EMP_FNAME'].' '.$penaltyRecData[0]['EMP_LASTNAME'].'.</span></p>
<p> Service Tax No:<br> <span style="FONT-WEIGHT:bold">AAECS9697J-ST001</span> <br><span style="LINE-HEIGHT:115%;FONT-FAMILY:&#39;Verdana&#39;,&#39;sans-serif&#39;;FONT-SIZE:9pt"> Commercial Training &amp; Coaching</span></p>
<p style="margin-left:10px;"><strong>Admission ID: </strong><span style="FONT-WEIGHT:bold">'.$penaltyRecData[0]['ADMISSION_ID'].'</span><br><strong><span></span></strong></p></td></tr></tbody></table>
<p><span>Print Date: '.date("d/m/Y").' - '.date("h:i a").'</span></p></td></tr></tbody></table>';
$pdf->writeHTML($admRecHtml, true, false, false, false, '');
$pdf->Output('output.pdf', 'I');
?>