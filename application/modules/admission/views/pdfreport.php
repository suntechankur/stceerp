<?php
$dueDateArr = explode(",", $insPlan[0]['DUEDATE']);
$dueAmtArr = explode(",", $insPlan[0]['DUE_AMOUNT']);
$insPlan = '';
$j = 1;
$admRecDate = date('Y-m-d',strtotime($admReceipts[0]['PAYMENT_DATE']));
// echo $admRecDate;
$nextDate = '';
for ($i=0; $i <count($dueDateArr) ; $i++) {
	$insDate = date('d/m/Y',strtotime($dueDateArr[$i]));
	// echo $insDate;
	// echo $insDate;
	$insPlan .= $j.') '.$insDate.' - '.$dueAmtArr[$i].', ';
	$j++;
	// echo  $admRecDate.'<br>';
	// $insPlan .= $dueDateArr[$i];
	
	if ($dueDateArr[$i] > $admRecDate) {
		$nextDate .= $dueDateArr[$i].',';
	}
}
	$insPlanTrim = rtrim($insPlan,", ");
	$nextDateTrim = rtrim($nextDate,",");
	$nextDateArr = explode(",", $nextDateTrim);
	$nextDateFormat = date('d-m-Y',strtotime($nextDateArr[0]));

	$payDate = date('d/m/Y',strtotime($admReceipts[0]['PAYMENT_DATE']));
tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
$title = "PDF Report";
$obj_pdf->SetTitle($title);
// $obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
$obj_pdf->SetHeaderData('', '', "St. Angelo's Computers Ltd.", 'Branch : '.$admReceipts[0]['CENTRE_NAME'].' ');
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();

$course_list = '';
$i = 1;
foreach ($courses as $course) {
	$course_list .= $course['COURSE_NAME'].', ';
	$i++;
}

// $amtPaidTot = 0;
$amtPaidTot = '';
foreach($amtPaid as $paid){
	$amtPaidTot .= $paid['AMOUNT_PAID_FEES'].' + ';
	// echo $paid['AMOUNT_PAID'];
}
	$amtPaidTotTrim = rtrim($amtPaidTot,"+ ");
	$amtPaidTotArr = explode("+", $amtPaidTotTrim);
	$amtPaidSum = array_sum($amtPaidTotArr);
$course_list_trim = rtrim($course_list,", ");
// $nextDue = date("Y-m-d",strtotime("+1 month", strtotime($date_rep))).",";
$tbl = '<table border="1"><tr><th colspan="2">Student Summary</th></tr>';
foreach ($admReceipts as $adm) {
	if ($maxPaidFees[0]['AMOUNT_PAID_FEES'] == $adm['AMOUNT_PAID_FEES']) {
		$admFeeCalc = $adm['TOTALFEES'] - $maxPaidFees[0]['AMOUNT_PAID_FEES'];
	}
	else{
		$admFeeCalc = $adm['TOTALFEES'] - $amtPaidSum;
	}
	$tbl .= '<tr>
				<td>Admission ID</td>
				<td>'.$adm["ADMISSION_ID"].'</td>
			 </tr>
			 <tr>
			 	<td>Name</td>
			 	<td>'.$adm["ENQUIRY_FIRSTNAME"].' '.$adm["ENQUIRY_LASTNAME"].'</td>
			 </tr>
			 <tr>
				<td>Admission Date</td>
				<td>17/11/2017</td>
			 </tr>
			 <tr>
			 	<td>Courses Enrolled</td>
			 	<td>'.$course_list_trim.'</td>
			 </tr>
			 <tr>
				<td>Total Course Fees</td>
				<td>'.$adm['TOTALFEES'].' /-</td>
			 </tr>
			 <tr>
				<td>Course Paid Fees</td>
				<td>'.$amtPaidSum.' /- ('.$amtPaidTotTrim.')</td>
			 </tr>
			 <tr>
				<td>Course Fees Balance</td>
				<td>'.$admFeeCalc.' /-</td>
			 </tr>
			 <tr>
				<td>Installment Plan for course fees (Next Due '.$nextDateFormat.' )</td>
				<td>'.$insPlanTrim.'</td>
			 </tr>
				';
}
$tbl .= '</table>';

ob_start();
	$obj_pdf->Write(0, 'Date: '.$payDate.' ', '', 0, 'R', true, 0, false, false, 0);
	$obj_pdf->writeHTML($tbl, true, false, false, false, '');
    // we can have any view part here like HTML, PHP etc
    $content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$obj_pdf->SetPrintHeader(false);
$obj_pdf->SetPrintFooter(false);
$obj_pdf->AddPage();
$obj_pdf->setPage(2);

switch ($admReceipts[0]['PAYMENT_TYPE']) {
	case '1':
		$payType = 'CASH';
		$payTxt = '';
		break;
	case '2':
		$payType = 'CC';
		$chequeDate = date("d/m/Y", strtotime($admReceipts[0]['CHEQUE_DATE']));
		$payTxt = '<span>by Cheque/D.D./TID No.<b><u>'.$admReceipts[0]['CHEQUE_NO'].'</u></b> dated <b><u>'.$chequeDate.'</u></b> drawn on bank <b><u>'.$admReceipts[0]['CHEQUE_BANK_NAME'].'</u></b> Branch <b><u>'.$admReceipts[0]['CHEQUE_BRANCH'].'</u></b></span>';
		break;
	case '3':
		$payType = 'CHEQUE';
		$chequeDate = date("d/m/Y", strtotime($admReceipts[0]['CHEQUE_DATE']));
		$payTxt = '<span>by Cheque/D.D./TID No.<b><u>'.$admReceipts[0]['CHEQUE_NO'].'</u></b> dated <b><u>'.$chequeDate.'</u></b> drawn on bank <b><u>'.$admReceipts[0]['CHEQUE_BANK_NAME'].'</u></b> Branch <b><u>'.$admReceipts[0]['CHEQUE_BRANCH'].'</u></b></span>';
		break;
	default:
		$payType = 'Other';
		$payTxt = '';
		break;
}
$admRecHtml = '<table style="WIDTH:500px;HEIGHT:176px" border="0" cellspacing="0" cellpadding="2">
<tbody>
<tr>
<td align="left">Branch : <span>'.$admReceipts[0]['CENTRE_NAME'].'</span> </td>
<td style="WIDTH:209px" align="center"><u>St. Angelo&#39;s Computers Ltd. </u><br><span>'.$payType.' Receipt cum Invoice</span> </td>
<td align="left">Receipt No:<span style="TEXT-DECORATION:underline">'.$admReceipts[0]['ADMISSION_RECEIPT_NO'].' </span><br>Receipt Date:<span>'.$payDate.'</span></td></tr>
<tr>
<td colspan="3" align="center">
<hr style="COLOR:black" color="black" size="1" width="100%">
</td></tr>
<tr>
<td style="HEIGHT:77px" valign="top" colspan="3" align="left">
<p style="LINE-HEIGHT:15pt;LETTER-SPACING:0px" align="justify">Received with thanks from Mr./Mrs./Miss  <span style="FONT-WEIGHT:bold;TEXT-DECORATION:underline">'.$admReceipts[0]['ENQUIRY_FIRSTNAME'].' '.$admReceipts[0]['ENQUIRY_LASTNAME'].'</span> residing at <span style="FONT-WEIGHT:bold;TEXT-DECORATION:underline">'.$admReceipts[0]['ENQUIRY_ADDRESS1'].', '.$admReceipts[0]['ENQUIRY_ADDRESS2'].', '.$admReceipts[0]['ENQUIRY_CITY'].' '.$admReceipts[0]['ENQUIRY_STATE'].' '.$admReceipts[0]['ENQUIRY_ZIP'].' </span>  a total  <span>'.$payType.' Receipt cum Invoice</span> amount (Course Fees + Service Tax) of <strong><span style="TEXT-DECORATION:underline">Rs.</span></strong><span style="FONT-WEIGHT:bold;TEXT-DECORATION:underline">'.$admReceipts[0]['AMOUNT_PAID'].'</span>/- ( <span style="FONT-WEIGHT:bold;TEXT-DECORATION:underline">'.numToStr($admReceipts[0]['AMOUNT_PAID']).'</span><span style="TEXT-DECORATION:underline"> <strong>Only</strong></span> ) out of which Course Fees paid is <span style="TEXT-DECORATION:underline"><strong>Rs.</strong></span><span style="FONT-WEIGHT:bold;TEXT-DECORATION:underline">'.$admReceipts[0]['AMOUNT_PAID_FEES'].'</span>/- and Service Tax Paid is <strong><span style="TEXT-DECORATION:underline">Rs.</span></strong><span style="FONT-WEIGHT:bold;TEXT-DECORATION:underline">'.$admReceipts[0]['AMOUNT_PAID_SERVICETAX'].'</span>/- <strong>@</strong> <span style="FONT-WEIGHT:bold">'.$admReceipts[0]['SERVICE_TAX'].'</span><strong>%</strong> '.$payTxt.' towards payment for  <span style="TEXT-DECORATION:underline"><b>Computer Training.</b></span><br></p></td></tr>
<tr>
<td colspan="3" align="left">
<table style="WIDTH:394px;HEIGHT:136px" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="TEXT-ALIGN:justify;WIDTH:375px;HEIGHT:66px" valign="top">
<ul>
<li style="TEXT-ALIGN:left">Fees once paid will not be refunded under any Circumstances 
<li style="TEXT-ALIGN:left">Central /State Govt.taxes/levies are charged as Applicable 
<li style="TEXT-ALIGN:left">This receipt is valid subject to Realization of Cheque/DD 
<li style="TEXT-ALIGN:left">This receipt has to be produced while collecting the Certificate 
<li style="TEXT-ALIGN:left">For any query or complaint call Helpline:9324433322 
<li style="TEXT-ALIGN:left">Students have to attend certain special subjects batches at <br>select centers only 
<li style="TEXT-ALIGN:left">Students have to pay their monthly installment within 30 days. 
<li style="TEXT-ALIGN:left">Late payment of installment will attract penalty of Rs.100 per day 
<li style="TEXT-ALIGN:left">Please note, in all cases provision of computer training will be done only after receiving fees in advance. 
<li style="TEXT-ALIGN:left">Kindly note that all special commitments given to student with regards to Fees, Batch timings, Center, University, Degree or any other needs to be mentioned in writing by the Front office staff on the company Letter Head. Else it shall not be fulfilled. </li></li></li></li></li></li></li></li></li></li></ul></td>
<td style="HEIGHT:66px" valign="top">
<p><br> <br> Sign: ________________</p>
<p> Recieved by: <br> <span style="FONT-WEIGHT:bold">'.$admReceipts[0]['EMP_FNAME'].' '.$admReceipts[0]['EMP_LASTNAME'].'.</span></p>
<p> Service Tax No:<br> <span style="FONT-WEIGHT:bold">AAECS9697J-ST001</span> <br><span style="LINE-HEIGHT:115%;FONT-FAMILY:&#39;Verdana&#39;,&#39;sans-serif&#39;;FONT-SIZE:9pt"> Commercial Training &amp; Coaching</span></p>
<p style="margin-left:10px;"><strong>Admission ID: </strong><span style="FONT-WEIGHT:bold">'.$admReceipts[0]['ADMISSION_ID'].'</span><br><strong><span></span></strong></p></td></tr></tbody></table>
<p><span>Print Date: '.date("d/m/Y").' - '.date("h:i a").'</span></p></td></tr></tbody></table>';
$obj_pdf->writeHTML($admRecHtml, true, false, false, false, '');
$obj_pdf->Output('output.pdf', 'I');
?>