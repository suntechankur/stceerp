<?php

tcpdf();
// create new PDF document
if($type == "marksheet"){
  $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);
}
else{
  $pdf = new TCPDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);
}

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('St. Angelos');
$pdf->SetTitle('Print student certificate summary');
$pdf->SetSubject('Print student certificate summary');
$pdf->SetKeywords('St.angelos student certificate summary');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
if($type == "marksheet"){
  $pdf->SetMargins("30%", PDF_MARGIN_TOP, "30%");
}
else{
  $pdf->SetMargins('0', '20', '0');
}

$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// if($type == "marksheet"){
  $pdf->SetFont('times', '', '');
// }
// else{
//   $fontname = TCPDF_FONTS::addTTFfont("resources/fonts/Monotype_Corsiva.ttf", 'TrueTypeUnicode', '', 32);
//   $pdf->SetFont($fontname, 'B', '');
// }
// ---------------------------------------------------------

// add a page
$pdf->AddPage();



// create some HTML content
$date = date("d/m/Y");

$exam_details = "";
$marks = 0;
$marks_out_of = 0;
$percentage = 0;
foreach($certificateData as $certificate_data){
  $i = 1;
  $start_dates = array();
  $end_dates = array();
  foreach($certificate_data[0] as $exam_data){
    $total_marks = $exam_data['JOURNAL_MARKS'] + $exam_data['PROJECT_MARKS'] + $exam_data['ONLINE_MARKS'];
    $total_marks_out_of = $exam_data['JOURNAL_MARKS_OUT_OF'] + $exam_data['PROJECT_MARKS_OUT_OF'] + $exam_data['ONLINE_MARKS_OUT_OF'];
    $marks += $total_marks;
    $marks_out_of += $total_marks_out_of;
    array_push($start_dates,$exam_data['START_DATE']);
    array_push($end_dates,$exam_data['END_DATE']);
    $exam_details .= '<tr><td>'.$i++.'</td><td>'.strtoupper($exam_data['CERTIFICATE_TITLE']).'</td><td>'.$exam_data['JOURNAL_MARKS'].'</td><td>'.$exam_data['JOURNAL_MARKS_OUT_OF'].'</td><td>'.$exam_data['PROJECT_MARKS'].'</td><td>'.$exam_data['PROJECT_MARKS_OUT_OF'].'</td><td>'.$exam_data['ONLINE_MARKS'].'</td><td>'.$exam_data['ONLINE_MARKS_OUT_OF'].'</td><td>'.$total_marks.'</td><td>'.$total_marks_out_of.'</td></tr>';
  }
  $row_count = $i;
  $dates = array_merge($start_dates,$end_dates);
  $course_dates = date("d/m/Y", strtotime(min($dates)))." to ".date("d/m/Y", strtotime(max($dates)));
  $s_date = date("d/m/Y", strtotime(min($dates)));
  $e_date = date("d/m/Y", strtotime(max($dates)));
  $exam_details .= '<tr><td colspan="8" style="font-weight:bold;text-align:right;">Total Marks</td><td>'.$marks.'</td><td>'.$marks_out_of.'</td></tr>';
  $percentage = round($marks / $marks_out_of * 100)." %";
  $percent = round($marks / $marks_out_of * 100);
  $grade = "";

  if (($percent >= 91) && ($percent <= 100))
  {
      $grade = "A+";
  }
  else if (($percent >= 81) && ($percent <= 90))
  {
      $grade = "A";
  }

  else if (($percent >= 71) && ($percent <= 80))
  {
      $grade = "B+";
  }
  else if (($percent >= 61) && ($percent <= 70))
  {
      $grade = "B";
  }
  else if (($percent >= 51) && ($percent <= 60))
  {
      $grade = "C";
  }
  else if (($percent >= 40) && ($percent <= 50))
  {
      $grade = "D";
  }
  else
  {
      $grade = "Fail";
  }
}


foreach($certificateData as $certificate_data){
  if($type == "marksheet"){
    $html = '<h1> </h1><h1> </h1><h1> </h1><h1> </h1><h1> </h1><h1> </h1><h1> </h1><h1> </h1><h1> </h1><h1> </h1>
              <table style="padding: 5px; text-align: right; font-size: 11px;">
                  <tr><td>Date : '.date("d/m/Y").'</td></tr>
              </table>
              <h1> </h1>
              <table width="80%" style="padding: 5px; text-align: left; font-size: 11px; font-weight:bold;">
                <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;NAME</td><td>: '.strtoupper($certificate_data['Name']).'</td></tr>
                <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;COURSE</td><td>: '.strtoupper($certificate_data['COURSE_NAME']).'</td></tr>
                <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;COURSE DURATION</td><td>: '.$course_dates.'</td></tr>
                <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;CENTRE</td><td>: '.$certificate_data['CENTRE'].'</td></tr>
                <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;OVERALL PERFORMANCE</td><td>: '.$percentage.'</td></tr>
                <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;GRADE</td><td>: '.$grade.'</td></tr>
              </table>
              <h1> </h1>
              <table width="100%" cellpadding="1" cellspacing="0" style="height: 1400px; padding: 5px; text-align: center; font-size: 9px; font-weight:bold;" border="1">
                  <thead>
                    <tr>
                      <td rowspan="2" style="text-align: center;">SR NO.</td>
                      <td rowspan="2" style="text-align: center;">SUBJECT</td>
                      <td colspan="2">INTERNAL MARKS</td>
                      <td colspan="2">PRACTICAL/PROJECTS MARKS</td>
                      <td colspan="2">ONLINE EXAM MARKS</td>
                      <td rowspan="2" colspan="2" nowrap="nowrap" style="text-align: center;">TOTAL</td>
                    </tr>
                    <tr>
                      <td nowrap="nowrap">OBTAINED</td>
                      <td nowrap="nowrap">OUT OF</td>
                      <td nowrap="nowrap">OBTAINED</td>
                      <td nowrap="nowrap">OUT OF</td>
                      <td nowrap="nowrap">OBTAINED</td>
                      <td nowrap="nowrap">OUT OF</td>
                    </tr>
                  </thead>
                  <tbody>'.$exam_details.'</tbody>
              </table>
              <h1> </h1>
              <h1> </h1>
              <h1> </h1>
              <h1> </h1>
              <table width="100%" style="padding: 5px; font-size: 11px;">
                  <tr><td></td><td>&nbsp;&nbsp;&nbsp;&nbsp;__________________________________________</td></tr>
                  <tr><td></td><td style="text-align:center;font-weight:bold;font-size:11px;">Authorized Signatory</td></tr>
              </table>';
  }
  else{
    $align = "right";
    $space = "";
    $letter_spacing = "";
    if((strlen($certificate_data['COURSE_NAME']) < 46) && (strlen($certificate_data['COURSE_NAME']) > 40)){
      $align="right";
      $letter_spacing = "letter-spacing:normal";
      $space="&nbsp;&nbsp;&nbsp;&nbsp;";
    }
    else if((strlen($certificate_data['COURSE_NAME']) < 40) && (strlen($certificate_data['COURSE_NAME']) > 28)){
      $align="right";
      $space="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    }
    else if((strlen($certificate_data['COURSE_NAME']) <= 28) && (strlen($certificate_data['COURSE_NAME']) > 20)){
      $align="right";
      $space="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    }
    else if((strlen($certificate_data['COURSE_NAME']) <= 20) && (strlen($certificate_data['COURSE_NAME']) > 0)){
      $align="center";
    }
    $html = '<table width="100%" style="font-size:16pt;letter-spacing:0.5px;">
                  <tr><td style="text-align: center;font-size:13px;">'.$certificate_data['Certificate_id'].'</td><td> </td><td> </td><td> </td></tr>
                  <tr><td style="height:136px;"> </td></tr>
                  <tr><td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'.strtoupper($certificate_data['Name']).'</strong></td><td> </td><td> </td></tr>
                  <tr><td style="height: 45px;"> </td></tr>
                  <tr><td> </td><td colspan="3" style="text-align:'.$align.';'.$letter_spacing.';"><strong>'.strtoupper($certificate_data['COURSE_NAME']).$space.'</strong></td></tr>
                  <tr><td style="height: 35px;"> </td></tr>
                  <tr><td> </td><td> </td><td align="center"><strong>'.$s_date.'&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td align="center"><strong>'.$e_date.'&nbsp;&nbsp;&nbsp;&nbsp;</strong></td></tr>
                  <tr><td style="height: 35px;"> </td></tr>
                  <tr><td> </td><td> </td><td><strong>'."'".$grade."'".'</strong></td><td> </td><td> </td></tr>
                  <tr><td style="height: 123px;"> </td></tr>
                  <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>'.date("d/m/Y").'</strong></td><td> </td><td> </td><td> </td><td> </td></tr>
              </table>';
  }

}




// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();




// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_006.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
?>
