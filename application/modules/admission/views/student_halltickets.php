<?php
tcpdf();
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('St. Angelos');
$pdf->SetTitle('Print student hallticket');
$pdf->SetSubject('Print student hallticket');
$pdf->SetKeywords('St.angelos student hallticket');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// ---------------------------------------------------------

// add a page
$pdf->AddPage();


$html = '
        <div>
                <div>
                    <span align="center">
                        <h2>St. Angelos Computers Ltd.</h2>
                        <strong>Branch : '.$hallticketdetails[0]['Name'].'</strong>
                   </span>
                    <span align="left">Receipt Date : <strong></strong></span><br/>
                </div>
            <table width="640px" style="font-size: small; height: 1400px; padding:15px;" border="1">
                <tr><td colspan="6"><strong>Student Summary</strong></td></tr>
                <tr><td colspan="2">Admission ID</td><td colspan="4"><strong></strong></td></tr>
                <tr><td colspan="2">Name</td><td colspan="4"><strong></strong></td></tr>
                <tr><td colspan="2">Admission Date</td><td colspan="4"><strong></strong></td></tr>
                <tr><td colspan="2">Course Enrolled</td><td colspan="4"><strong></strong></td></tr>
                <tr><td colspan="2">Total Course Fees</td><td colspan="4"><strong> /-</strong></td></tr>
                <tr><td colspan="2">Course Fees Paid</td><td colspan="4"><strong></strong></td></tr>
                <tr><td colspan="2">Course Fees Balance</td><td colspan="4"><strong>/-</strong></td></tr>
                <tr><td colspan="2">Installment plan for next fees (Next due)</td><td colspan="4"></td></tr>
            </table>
            <div><p>GSTIN/UIN No: 27AAECS9697J1ZG<br/>SAC Code: 999293<br/>Commercial Training & Coaching<br/>
(Note: All fees mention are excluding GST) </p><br/><br/><p>Print Date : </p></div>
        </div>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();

//Close and output PDF document
$pdf->Output('example_006.pdf', 'I');


?>
