<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Practical Marks Upload</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?php if($this->session->flashdata('success')) { ?>
						<div class="alert alert-success">
						<strong> <?php echo $this->session->flashdata('success'); ?></strong>
						</div>
						<?php } ?>
						<?php if($this->session->flashdata('danger')) { ?>
						<div class="alert alert-danger">
						<strong> <?php echo $this->session->flashdata('danger'); ?></strong>
						</div>
						<?php } ?>
					</div>
					<div class="panel-body">
						<?php echo form_open(); ?>
							<div class="col-md-12">

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Select File :</label>
												<input type="file" class="form-control" name="attchment">
											</div>
											<div class="col-lg-4">
												<br/>
												<button type="submit" name="upload_marks" class="btn btn-primary">Upload</button>
											</div>
											<div class="col-lg-4">
											</div>
										</div>
									</div>
								</div>

								</div>
							</div>
						 <?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>

</div>
