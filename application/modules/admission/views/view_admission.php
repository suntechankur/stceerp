<!--<div id="wait" style="margin: 0px;padding: 0px;position: fixed;right: 0px;top: 0px;width: 100%;height: 100%;background-color: rgb(102, 102, 102);z-index: 30001;opacity: 0.8;"><img src="<?php// echo base_url('resources/images/') ?>loading.gif" width="64" height="64" /><br>Loading..</div>-->
<div id="wait" style="display: none;position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: url(../resources/images/loading.gif) center no-repeat #fff;"></div>

<?php $admin_cred = $this->session->userdata('admin_data')[0];
	// echo isAdmissionDeletionPermitted;
	// echo "<pre>";
	// print_r($admin_cred);
	// echo "</pre>";
	$isAdmissionDeletionPermitted = false;
	if($admin_cred['EMPLOYEE_ID'] == '987'){
		$isAdmissionDeletionPermitted = true;
	}
?>
<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Admission Search</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
<!-- start message area -->

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('msg')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php $msg = $this->session->flashdata('msg'); echo $msg['msg']; ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>


<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info1')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
</div>
<?php } ?>

<?php if($this->session->flashdata('warning')) { ?>
<div class="alert alert-warning">
<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php } ?>

<!-- End message area -->
					</div>
					<div class="panel-body">
						<?php echo form_open(); ?>
							<div class="col-md-12">

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>First Name:</label>
												<input class="form-control" value="<?php echo $admission_filter_data['ENQUIRY_FIRSTNAME']; ?>" maxlength="50" name="ENQUIRY_FIRSTNAME" id="ENQUIRY_FIRSTNAME" type="text">
											</div>
											<div class="col-lg-4">
												<label>Last Name:</label>
												<input class="form-control" value="<?php echo $admission_filter_data['ENQUIRY_LASTNAME']; ?>" name="ENQUIRY_LASTNAME" id="ENQUIRY_LASTNAME" maxlength="50" type="text">
											</div>
											<div class="col-lg-4">
												<label>Center:</label>
												<select name="CENTRE_ID" class="form-control">
													<option value="">Please Select Centre</option>
													<?php foreach ($centres as $centre) {?>
														<option value="<?php echo $centre['CENTRE_ID']; ?>" <?php if(isset($admission_filter_data['CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $admission_filter_data['CENTRE_ID']) ? ' selected="selected"' : '';}?>><?php echo $centre['CENTRE_NAME']; ?> </option>
													<?php } ?>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>From Admission Date:</label>

												<input name="ADMISSION_DATE_FROM" id="ADMISSION_DATE_FROM" value="<?php echo $admission_filter_data['ADMISSION_DATE_FROM']; ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<label>To Admission Date:</label>
												<input name="ADMISSION_DATE_TO" id="ADMISSION_DATE_TO" value="<?php echo $admission_filter_data['ADMISSION_DATE_TO']; ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
											</div>
											<div class="col-lg-4">
												<div class="col-md-6">
													<label>Course Taken:</label><br>
													<select multiple="multiple" name="COURSE_TAKEN[]" class="course_interested">
														<?php foreach ($courses as $course) {
															$isSelectedCourse = in_array($course['COURSE_ID'],$admission_filter_data['COURSE_TAKEN']) ? "selected='selected'" : "";
															?>
															<option value="<?php echo $course['COURSE_ID'] ?>" <?php echo $isSelectedCourse; ?> ><?php echo $course['COURSE_NAME']; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="col-md-6">
													<label>Module Taken:</label><br>
													<select multiple="multiple" name="MODULE_TAKEN[]" class="module_interested">
														<?php foreach ($modules as $module) {
															$isSelectedModule = in_array($module['MODULE_ID'],$admission_filter_data['MODULE_TAKEN']) ? "selected='selected'" : "";
															?>
															<option value="<?php echo $module['MODULE_ID'] ?>" <?php echo $isSelectedModule; ?>><?php echo $module['MODULE_NAME']; ?></option>
														<?php } ?>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Admission ID:</label>
												<input class="form-control" name="ADMISSION_ID" id="ADMISSION_ID" maxlength="50" type="text" value="<?php echo $admission_filter_data['ADMISSION_ID']; ?>">
											</div>
											<div class="col-lg-4">
                                            <label>Stream</label>
                                             <select class="form-control" style="width:100% ;" name="STREAM" id="STREAM">
                                             				<option value="">Please Select Stream</option>
                                                            <option value="B" <?php if ($admission_filter_data['STREAM'] == 'B') {
                                                            	echo "selected";
                                                            } ?>>Basics</option>
                                                            <option value="P" <?php if ($admission_filter_data['STREAM'] == 'P') {
                                                            	echo "selected";
                                                            } ?>>Programming</option>
                                                            <option value="G" <?php if ($admission_filter_data['STREAM'] == 'G') {
                                                            	echo "selected";
                                                            } ?>>Graphics &amp; animation</option>
                                                            <option value="H" <?php if ($admission_filter_data['STREAM'] == 'H') {
                                                            	echo "selected";
                                                            } ?>>Hardware &amp; Networking</option>
                                                            <option value="O" <?php if ($admission_filter_data['STREAM'] == 'O') {
                                                            	echo "selected";
                                                            } ?>>others</option>
                                             </select>
                                        </div>
                                        <div class="col-lg-4">
                                        	<label for="">Status</label>
                                        	<select class="form-control" style="width:100% ;" name="STATUS" id="STATUS">
                                        		<option value="">Please Select Status</option>
                                        		<option value="1" <?php if ($admission_filter_data['STATUS'] == 'hold') {
                                                            	echo "selected";
                                                            } ?>>Hold</option>
                                        		<option value="2" <?php if ($admission_filter_data['STATUS'] == 'break') {
                                                            	echo "selected";
                                                            } ?>>Break</option>
                                        		<option value="3" <?php if ($admission_filter_data['STATUS'] == 'persuing') {
                                                            	echo "selected";
                                                            } ?>>Persuing</option>
                                        		<option value="4" <?php if ($admission_filter_data['STATUS'] == 'completed') {
                                                            	echo "selected";
                                                            } ?>>Completed</option>
                                        		<option value="5" <?php if ($admission_filter_data['STATUS'] == 'left') {
                                                            	echo "selected";
                                                            } ?>>Left</option>
                                        	</select>
                                        </div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-md-4">
                                            <label>Intake: <span class="text-danger">*</span></label>
                                            <select name="IN_TAKE" id="" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1" <?php if($admission_filter_data['IN_TAKE'] == '1'){echo "selected";} ?>>JANUARY</option>
                                                <option value="2" <?php if($admission_filter_data['IN_TAKE'] == '2'){echo "selected";} ?>>FEBRUARY</option>
                                                <option value="3" <?php if($admission_filter_data['IN_TAKE'] == '3'){echo "selected";} ?>>MARCH</option>
                                                <option value="4" <?php if($admission_filter_data['IN_TAKE'] == '4'){echo "selected";} ?>>APRIL</option>
                                                <option value="5" <?php if($admission_filter_data['IN_TAKE'] == '5'){echo "selected";} ?>>MAY</option>
                                                <option value="6" <?php if($admission_filter_data['IN_TAKE'] == '6'){echo "selected";} ?>>JUNE</option>
                                                <option value="7" <?php if($admission_filter_data['IN_TAKE'] == '7'){echo "selected";} ?>>JULY</option>
                                                <option value="8" <?php if($admission_filter_data['IN_TAKE'] == '8'){echo "selected";} ?>>AUGUST</option>
                                                <option value="9" <?php if($admission_filter_data['IN_TAKE'] == '9'){echo "selected";} ?>>SEPTEMBER</option>
                                                <option value="10" <?php if($admission_filter_data['IN_TAKE'] == '10'){echo "selected";} ?>>OCTOBER</option>
                                                <option value="11" <?php if($admission_filter_data['IN_TAKE'] == '11'){echo "selected";} ?>>NOVEMBER</option>
                                                <option value="12" <?php if($admission_filter_data['IN_TAKE'] == '12'){echo "selected";} ?>>DECEMBER</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Academic Year: <span class="text-danger">*</span></label>
                                            <select name="YEAR" id="" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1" <?php if($admission_filter_data['YEAR'] == '1'){echo "selected";} ?>>2019-2020</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
											<label for="">Zero fees paid student</label>
											<select name="ZERO_FEES" id="ZERO_FEES" class="form-control">
												<option value="">Please select payment type</option>
												<option value="1" <?php if($admission_filter_data['ZERO_FEES'] == '1'){echo "selected";} ?>>Hide</option>
												<option value="2" <?php if($admission_filter_data['ZERO_FEES'] == '2'){echo "selected";} ?>>Show</option>
											</select>
										</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">

												<div class="col-md-4">
													<button type="submit" class="btn btn-primary">Search</button>
												</div>
												<div class="col-md-4">
													<input class="admToken" type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
													<button type="submit" name="reset" class="btn btn-primary">Reset Filter</button>

												</div>

													<?php if (!empty($this->session->userdata('admission_filter_data'))) { ?>
														<div class="col-lg-4">
													<a onClick ="$('#export_to_execl_view_admission').tableExport({type:'excel',escape:'false'});" class="btn" style="float:left" title="Export to excel"><i class="fa fa-file-excel-o fa-2x text-success"></i></a>
														</div>
													<?php } ?>

											</div>
										</div>
									</div>
								</div>
							</div>
						 <?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php if($this->session->userdata('admission_filter_data')){?>
		<div id="box">
			<p>&nbsp; <?php echo $pagination;?></p>
        	<h2 class="text-center">Admission List</h2>
        </div>
        <div class="panel panel-default">
        	<div class="panel-body table-responsive">
        		<table class="table table-striped table-hover" id="export_to_execl_view_admission">
					<thead>
						<tr>
							<th>Action</th>
							<th>Quick Actions</th>
							<th>Admission Id</th>
							<th>Student Name</th>
							<th>Admission Date</th>
							<th>Centre</th>
							<th>Course Taken</th>
							<!-- <th>Module Taken</th> -->
							<!-- module added in course_taken -->
							<th>Total Fees</th>
							<th>Fees Paid</th>
							<th>Address</th>
							<th>Email Id</th>
							<th>Contact No.</th>
							<!-- <th>Preferred Time</th>
							<th>Preferred Time 2</th>
							<th>Preferred Time 3</th> -->
							<th>Status</th>
							<th>Remarks</th>
							<th>Course Status Remark Date</th>
							<th>Course Status Remarks</th>
							<th>Academic Year</th>
							<th>Intake</th>
							<th>Is Installment Plan Done</th>
							<th>Handeled By</th>
							<th>Receipt</th>
							<th>Other Receipts</th>
							<th>Installment</th>
							<!-- <th>Scheme Validity</th> -->
						</tr>
					</thead>
					<tbody>
						<?php
						// echo "<pre>";
						// print_r($admission_list);
						// echo "</pre>";
						foreach ($admission_list as $adm_list) {

							$adm_list['RECEIPTS'] = '<span class="handCursor receipt" data-fees_applicable="'.$adm_list['fee_type'].'" data-other_receipt_modal_permission="'.$adm_list['modal_permission'].'">Receipt</span>';
							$adm_list['OTHER_RECEIPTS'] = '<span class="handCursor otherReceipt" data-toggle="modal" data-target="#otherReceiptsModal" data-url="'.base_url('admission/admission-other-receipt/'.$this->encrypt->encode($adm_list['ADMISSION_ID'])).'" data-admission_id="'.$adm_list['ADMISSION_ID'].'">Other Receipts</span>';
							// $adm_list['OFFICIAL_RECEIPTS'] = '<span class="handCursor officialReceipt" data-target="#officialReceiptsModal" data-fees_applicable="'.$adm_list['fee_type'].'" data-other_receipt_modal_permission="'.$adm_list['modal_permission'].'">Official Receipts</span>';
							unset($adm_list['fee_type']);
							unset($adm_list['FEES_PAID']);
							unset($adm_list['modal_permission']);
							unset($adm_list['EXAM_FEES_APPLICABLE']);
							unset($adm_list['PROCESSING_FEES_APPLICABLE']);
							$admDateRep = str_replace('-', '/', $adm_list['ADMISSION_DATE']);
            				$adm_list['ADMISSION_DATE'] =  date("d/m/Y", strtotime($admDateRep) );
							?>

							<?php
                                    $courses_arr = explode(",",$adm_list['COURSE_TAKEN']);
                                         $courseNames = '';
                                         //echo '<ul class="list-group">';
                                          for($j=0;$j<count($courses);$j++){
                                              if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
                                                    $courseNames .= ','.$courses[$j]['COURSE_NAME'];
                                                  $adm_list['COURSE_TAKEN'] = trim($courseNames,",");
                                                }

                                          }
                                         //echo '</ul>';
                                    ?>
                                    <?php
                                    $modules_arr = explode(",",$adm_list['MODULE_TAKEN']);
                                         $moduleNames = '';
                                         //echo '<ul class="list-group">';
                                          for($j=0;$j<count($modules);$j++){
                                              if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
                                                    $moduleNames .= ','.$modules[$j]['MODULE_NAME'];
                                                  $adm_list['MODULE_TAKEN'] = trim($moduleNames,",");
                                                }

                                          }
                                         //echo '</ul>';
																				 $adm_list['COURSE_TAKEN'] = trim($adm_list['MODULE_TAKEN'].",".$adm_list['COURSE_TAKEN'],",");
																				 unset($adm_list['MODULE_TAKEN']);
                                    // echo "<pre>";
                                    // print_r($modules);
                                    // echo "</pre>";
                                    ?>
						<tr data-admid="<?php echo $this->encrypt->encode($adm_list['ADMISSION_ID']); ?>">
							<td>
								<?php if(IsPracticalExamBookingPermitted){ ?>
								<a href="<?php echo base_url('admission/book-exam/'.$this->encrypt->encode($adm_list['ADMISSION_ID'])).'/theory'; ?>" title="Book Theory Exam" target="_blank"><span class="glyphicon glyphicon-calendar" style="color:red;"></span></a>
								<a href="<?php echo base_url('admission/book-exam/'.$this->encrypt->encode($adm_list['ADMISSION_ID'])).'/practical'; ?>" title="Book Practical Exam" target="_blank"><span class="glyphicon glyphicon-calendar"></span></a>
								<a href="<?php echo base_url('admission/issue-certificate/'.$this->encrypt->encode($adm_list['ADMISSION_ID'])).''; ?>" title="Print Certificate" target="_blank"><span class="glyphicon glyphicon-file" style="color:brown;"></span></a>
								<?php } ?>

								<?php if(IsPracticalExamBookingPermittedByRoleIds){ ?>
								<a href="<?php echo base_url('admission/book-exam/'.$this->encrypt->encode($adm_list['ADMISSION_ID'])).'/practical'; ?>" title="Book Practical Exam" target="_blank"><span class="glyphicon glyphicon-calendar"></span></a>
								<?php } ?>

								<?php if(IsAdmissionUpdationPermitted){ ?>
								<a href="<?php echo base_url('admission/edit-admission/'.$this->encrypt->encode($adm_list['ADMISSION_ID'])); ?>" title="Edit" target="_blank"><span class="glyphicon glyphicon-edit"  style="color:green;"></span></a>
								<?php } ?>

								<?php if(IsAdmissionDeletionPermitted){ ?>
								<span title="Delete" data-url="<?php echo base_url('admission/delete-admission/'.$this->encrypt->encode($adm_list['ADMISSION_ID'])); ?>" data-method="delete" data-popup_header="Delete Admission" data-message="Are you sure you want to delete this entry?" class="glyphicon glyphicon-trash action pointer"></span>
								<?php } ?>
								<a href="<?php echo base_url('students/student-details/'.$this->encrypt->encode($adm_list['ADMISSION_ID'])); ?>" title="View student Details"><span class="glyphicon glyphicon-hdd"></span></a>
								<!-- <a href="<?php //echo base_url('admission/add-parents-receipts/'.$this->encrypt->encode($adm_list['ADMISSION_ID'])) ?>">Add Parents Meeting</a>
								<a href="<?php //echo base_url('admission/add-feedback/'.$this->encrypt->encode($adm_list['ADMISSION_ID']))?>">Add Feedback</a>
								<a href="<?php //echo base_url('admission/student-leave-application/'.$this->encrypt->encode($adm_list['ADMISSION_ID'])) ?>">Leave Application</a>
								<a href="<?php //echo base_url('admission/student-report-card/'.$this->encrypt->encode($adm_list['ADMISSION_ID'])); ?>">Report Card</a>
								<a href="<?php //echo base_url('admission/student-marks-master/'.$this->encrypt->encode($adm_list['ADMISSION_ID'])) ?>">Enter Student Marks</a> -->
							</td>
							<td>
								<a href="<?php echo base_url('admission/reset-password/'.$this->encrypt->encode($adm_list['ADMISSION_ID'])).''; ?>" title="Reset Password"><span class="glyphicon glyphicon-time"></span></a>
							</td>
							<?php foreach($adm_list as $adm_key => $list){
								if($adm_list['IS_INSTALLMENT_PLAN_DONE']=="1")
								{
									$adm_list['IS_INSTALLMENT_PLAN_DONE'] = "Yes";
								}
								elseif($adm_list['IS_INSTALLMENT_PLAN_DONE']=="0")
								{
									$adm_list['IS_INSTALLMENT_PLAN_DONE'] = "No";
								}

								?>
							<td id="<?php echo $adm_key;?>"><?php echo $adm_list[$adm_key]; ?></td>
							<?php } ?>
							<td><a href="<?php echo base_url('admission/editInstallment/'.$this->encrypt->encode($adm_list['ADMISSION_ID'])); ?>">Edit</a></td>
						</tr>
						<?php } ?>
					</tbody>
        		</table>
        	</div>
        </div>
        <?php }?>
	</div>

	<!-- Receipts Modal Starts -->
		<div id="receiptModal" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-lg">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header" id="receiptdetails">
  		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Admission Receipt</h4>
		      </div>
		      <div class="modal-body">

                    <!-- starts Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="admissionreceipt">
                        <!-- start admission receipt panel -->

					      <div class="modal-body receiptContent">
					        	<div class="row">
					        		<div class="col-md-4">
					        			<label for="ADMISSION_ID">Admission ID:</label>
					        			<p class="admId">&nbsp;</p>
					        		</div>
					        		<div class="col-md-4">
					        			<label for="STUDENT_NAME">Student Name:</label>
					        			<p class="enqName">Amar</p>
					        		</div>
					        		<div class="col-md-4">
					        			<label for="centre">Centre:</label>
					        			<select class="form-control cenName" name="CENTRE_ID" id="CENTRE_ID">
					        				<?php foreach ($centres as $centre) { ?>
					        					<option value="<?php echo $centre['CENTRE_ID']; ?>"><?php echo $centre['CENTRE_NAME']; ?></option>
					        				<?php } ?>
					        			</select>
					        		</div>
					        	</div>
					        	<div class="row">
					        		<div class="col-md-4">
					        			<label for="ADMISSION_DATE">Admission Date</label>
					        			<p class="admDate"></p>
					        		</div>
					        		<div class="col-md-4">
					        			<label for="">Course And Module Taken</label>
					        			<ul class="list-group courseModule">
					        			</ul>
					        		</div>
					        		<div class="col-md-4">
						        		<label for="">Course and Module Total Fees</label>
						        		<p class="courseFees">&nbsp;</p>
						        		<input type="hidden" class="balancefees">
						        	</div>
					        </div>
									<!--
									code commented of normal fees and university fees selection
									<div style="border:5px dashed red;padding:15px;margin-bottom:15px;border-radius:10px;">
										<div class="row">
						        	<div class="col-md-4">
						        		<label>Receipt Type <span class="text-danger">*</span></label>
											</div>
											<div class="col-md-4">
						        		<label><input type="radio" name="RECEIPT_TYPE" value="normal" class="receipt_for" data-specific="normal_receipt_for" checked="checked"> Normal</label>
											</div>
											<div class="col-md-4">
						        		<label><input type="radio" name="RECEIPT_TYPE" value="university" class="receipt_for" data-specific="university_receipt_for"> University</label>
						        	</div>
										</div>
										<div class="row" style="display:none;" id="receipt_specific_for">
						        	<div class="col-md-4">
						        		<label>Receipt For BCA<span class="text-danger">*</span></label>
												<select class="form-control" name="RECEIPT_SPECIFIC_FOR" id="receipt_specific_for_option" class="receipt_specific_for">
													<option selected disabled="disabled">Select year</option>
													<option value="BCA 1 year">BCA 1 Year</option>
													<option value="BCA 2 year">BCA 2 Year</option>
													<option value="BCA 3 year">BCA 3 Year</option>
													<option value="MGA 1 year">MGA 1 Year</option>
													<option value="MGA 2 year">MGA 2 Year</option>
													<option value="MGA 3 year">MGA 3 Year</option>
												</select>
											</div>
											<div class="col-md-8"><label style="color:red;font-weight:bold;">* If your receipt type is university then you have to select year in "Receipt for" dropdown.</label></div>
										</div>
									</div> -->
									<div id="further_receipt_actions">
						        <div class="row">
						        	<div class="col-md-4">
						        		<label for="TOTALFEES">Amount Paid (With GST): <span class="text-danger">*</span></label>
						        		<input type="text" class="form-control amtSt" data-type="normal" name="TOTALFEES" autocomplete="off">
						        	</div>
						        	<div class="col-md-4">
						        		<label for="">Amount Paid Fees(Without GST)</label>
						        		<p class="amtNoSt"></p>
						        	</div>
						        	<div class="col-md-4">
						        		<label for="">Service Tax Paid:</label>
						        		<p class="servTax"></p>
						        	</div>
						        </div>
						        <div class="row">
						        	<div class="col-md-4">
						        		<label for="">Payment Type</label>
						        		<select class="form-control paymentType" name="PAYMENT_TYPE" id="">
						        			<option value="1">Cash</option>
						        			<option value="2">UPI</option>
						        			<option value="3">Cheque</option>
						        		</select>
						        	</div>
						        	<div class="col-md-4">
						        		<?php $myDate = date('d/m/Y'); ?>
						        		<label for="">Payment Date:</label>
						        		<input type="text" value="<?php echo $myDate; ?>" name="PAYMENT_DATE" class="form-control payDate enquiry_date">
						        	</div>
						        	<div class="col-md-4">
						        		<label for="">Cheque Date:</label>
						        		<input type="text" name="CHEQUE_DATE" class="form-control enquiry_date chequeDate" disabled="">
						        	</div>
						        </div>
						        <div class="row">
						        	<div class="col-md-4">
						        		<label for="">Cheque/DD/TID No:</label>
						        		<input type="text" class="form-control chequeNo" name="CHEQUE_NO" disabled="">
						        	</div>
						        	<div class="col-md-4">
						        		<label for="">Bank Name</label>
						        		<input type="text" class="form-control bankName" name="BANK_NAME" disabled="">
						        	</div>
						        	<div class="col-md-4">
						        		<label for="">Bank Branch</label>
						        		<input type="text" class="form-control bankBranch" name="BANK_BRANCH" disabled="">
						        	</div>
						        </div>
						        <div class="row">
						        	<div class="col-md-4">
						        		<label for="">Received By: <span class="text-danger">*</span></label>
										<select class="form-control recBy" name="RECEIVED_BY" id="">
											<?php foreach($employees as $employee){ ?>
												<option value="<?php echo $employee['EMPLOYEE_ID']; ?>"><?php echo $employee['EMP_FNAME'].' '.$employee['EMP_LASTNAME']; ?></option>
											<?php } ?>
										</select>
						        	</div>
						        </div>
									</div>
					        <div class="row">
					        	<div class="col-md-4">&nbsp;</div>
					        	<div class="col-md-4">
					        		<p>&nbsp;</p>
					        		<div class="col-md-6">
					        			<button type="button" class="btn btn-info submit" id="saveAdmissionReceipt">Save</button>
					        		</div>
					        		<div class="col-md-6">
					        			<button class="btn btn-danger" data-dismiss="modal">Cancel</button>
					        		</div>
					        	</div>
					        	<div class="col-md-4">&nbsp;</div>
					        </div>
					        <div class="row">
					        	<div class="col-md-12">
					        		<table class="table table-hover table-striped receiptTbl">
					        			<thead>
					        				<tr>
						        				<th>Receipt No</th>
						        				<th>Date</th>
						        				<th>Amount Paid WST</th>
						        				<th>Amount W/o GST</th>
						        				<th>Payment Type</th>
						        				<th>Balance</th>
						        				<th>Receipt</th>
						        				<th>Summary</th>
					        				</tr>
					        			</thead>
					        			<tbody class="receiptTblData" id="mainReceiptsTable">
					        			</tbody>
					        		</table>
					        	</div>
					        </div>
					        <div class="row">
					         <div class="container">
						        	<div class="col-md-4">
						        	<h5>Installment Plan:</h5>
						        		<table class="table table-striped">
						        			<thead>
						        				<tr>
							        				<th>SrNo</th>
							        				<th>DP/Installment Date</th>
							        				<th>DP/Installment Amount</th>
							        			</tr>
						        			</thead>
						        			<tbody class="instData"></tbody>
						        		</table>
						        	</div>
						        </div>
					         </div>
					      </div>

                        <!-- end admission receipt panel -->
                        </div>
                    </div>
                    <!-- end Tab panes -->

               </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>

		    </div>

		  </div>
		</div>
	<!-- Receipts Modal Ends -->

	<!-- Other Receipts Modal Starts -->
		<div id="otherReceiptsModal" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-lg">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Other Receipt</h4>
		      </div>
		      <div class="modal-body receiptContent">
		        	<div class="row">
		        		<div class="col-md-4">
		        			<label for="ADMISSION_ID">Admission ID:</label>
		        			<p class="admId">123456</p>
		        		</div>
		        		<div class="col-md-4">
		        			<label for="STUDENT_NAME">Student Name:</label>
		        			<p class="enqName">Ankur Prajapati</p>
		        		</div>
		        		<div class="col-md-4">
		        			<label for="centre">Centre:</label>
			        			<select class="form-control cenName" name="CENTRE_ID" id="CENTRE_ID">
			        				<?php foreach ($centres as $centre) { ?>
			        					<option value="<?php echo $centre['CENTRE_ID']; ?>"><?php echo $centre['CENTRE_NAME']; ?></option>
			        				<?php } ?>
			        			</select>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-md-4">
		        			<label for="ADMISSION_DATE">Admission Date</label>
		        			<p class="admDate">05/02/2016</p>
		        		</div>
		        		<div class="col-md-4">
		        			<label for="">Course And Module Taken</label>
		        			<ul class="list-group courseModule"><li class="list-group-item list-group-item-info courseTaken"><strong>Courses</strong>SAIG (3D Animation &amp; Visual Effects) V.15</li></ul>
		        		</div>
		        		<div class="col-md-4">
			        		<label for="">Payment For</label>
			        		<select class="form-control payFor" name="payFor" id="">

			        		</select>
			        	</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-4">
		        		<label for="TOTALFEES">Amount Paid (With GST): <span class="text-danger">*</span></label>
		        		<input class="form-control amtSt" data-type="other" name="TOTALFEES" autocomplete="off" type="text">
		        	</div>
		        	<div class="col-md-4">
		        		<label for="">Amount Paid Fees(Without GST)</label>
		        		<p class="amtNoSt"></p>
		        	</div>
		        	<div class="col-md-4">
		        		<label for="">Service Tax Paid:</label>
		        		<p class="servTax"></p>
		        	</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-4">
		        		<label for="">Payment Type</label>
		        		<select class="form-control paymentType" name="PAYMENT_TYPE" id="">
		        			<option value="1">Cash</option>
		        			<option value="2">UPI</option>
		        			<option value="3">Cheque</option>
		        		</select>
		        	</div>
		        	<div class="col-md-4">
	        		<label for="">Payment Date:</label>
		        		<input value="<?php echo date('d/m/Y');?>" name="PAYMENT_DATE" class="form-control payDate enquiry_date" type="text">
		        	</div>
		        	<div class="col-md-4">
		        		<label for="">Cheque Date:</label>
		        		<input name="CHEQUE_DATE" class="form-control enquiry_date chequeDate" disabled="" type="text">
		        	</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-4">
		        		<label for="">Cheque/DD/TID No:</label>
		        		<input class="form-control chequeNo" name="CHEQUE_NO" disabled="" type="text">
		        	</div>
		        	<div class="col-md-4">
		        		<label for="">Bank Name</label>
		        		<input class="form-control bankName" name="BANK_NAME" disabled="" type="text">
		        	</div>
		        	<div class="col-md-4">
		        		<label for="">Bank Branch</label>
		        		<input class="form-control bankBranch" name="BANK_BRANCH" disabled="" type="text">
		        	</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-4">
		        		<label for="">Received By: <span class="text-danger">*</span></label>
							<select class="form-control recBy" name="RECEIVED_BY" id="">
								<!-- <option value="">Emp1</option>
								<option value="">Emp2</option> -->
								<?php foreach($employees as $employee){ ?>
									<option value="<?php echo $employee['EMPLOYEE_ID']; ?>"><?php echo $employee['EMP_FNAME'].' '.$employee['EMP_LASTNAME']; ?></option>
								<?php } ?>
							</select>
		        	</div>
		        	<div class="col-md-4">
		        		<label for="">Remarks:</label>
		        		<input class="form-control bankBranch" name="REMARKS" type="text">
		        	</div>
		        	<div class="col-md-4">
		        		<label for="">Exam/Course Module:</label>
		        		<select name="exmCrsMod" id="" class="exmCrsMod form-control"></select>
		        	</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-4">&nbsp;</div>
		        	<div class="col-md-4">
		        		<p>&nbsp;</p>
		        		<div class="col-md-6">
		        			<button type="button" class="btn btn-info" id="saveOtherReceipt">Save</button>
		        		</div>
		        		<div class="col-md-6">
		        			<button class="btn btn-danger" data-dismiss="modal">Cancel</button>
		        		</div>
		        	</div>
		        	<div class="col-md-4">&nbsp;</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-12">
		        		<table class="table table-hover table-striped receiptTbl">
		        			<thead>
		        				<tr>
			        				<th>Receipt No</th>
			        				<th>Date</th>
			        				<th>Particular</th>
			        				<th>Amount Paid WGST</th>
			        				<th>Amount W/o GST</th>
			        				<th>Payment Type</th>
			        				<th>Print</th>
		        				</tr>
		        			</thead>
		        			<tbody class="otherReceiptTblData"></tbody>
		        		</table>
		        	</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>

		  </div>
		</div>
	<!-- Other Receipts Modal Ends -->

	<!-- Official Receipts Modal Starts -->
		<div id="officialReceiptsModal" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-lg">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Official Receipt</h4>
		      </div>
		      <div class="modal-body receiptContent">
		        	<div class="row">
		        		<div class="col-md-4">
		        			<label for="ADMISSION_ID">Admission ID:</label>
		        			<p class="admId">115962</p>
		        		</div>
		        		<div class="col-md-4">
		        			<label for="STUDENT_NAME">Student Name:</label>
		        			<p class="enqName">SAI KADAM</p>
		        		</div>
		        		<div class="col-md-4">
		        			<label for="centre">Centre:</label>
			        			<select class="form-control cenName" name="CENTRE_ID" id="CENTRE_ID">
			        				<?php foreach ($centres as $centre) { ?>
			        					<option value="<?php echo $centre['CENTRE_ID']; ?>"><?php echo $centre['CENTRE_NAME']; ?></option>
			        				<?php } ?>
			        			</select>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-md-4">
		        			<label for="ADMISSION_DATE">Admission Date</label>
		        			<p class="admDate">05/02/2016</p>
		        		</div>
		        		<div class="col-md-4">
		        			<label for="">Course And Module Taken</label>
		        			<ul class="list-group courseModule"><li class="list-group-item list-group-item-info courseTaken"><strong>Courses</strong>SAIG (3D Animation &amp; Visual Effects) V.15</li></ul>
		        		</div>
		        		<div class="col-md-4">
			        		<label for="">Payment For</label>
			        		<select class="form-control payFor" name="payFor" id="">

			        		</select>
			        	</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-4">
		        		<label for="TOTALFEES">Amount Paid (With GST): <span class="text-danger">*</span></label>
		        		<input class="form-control amtSt" name="TOTALFEES" autocomplete="off" type="text">
		        	</div>
		        	<div class="col-md-4">
		        		<label for="">Amount Paid Fees(Without GST)</label>
		        		<p class="amtNoSt"></p>
		        	</div>
		        	<div class="col-md-4">
		        		<label for="">Service Tax Paid:</label>
		        		<p class="servTax"></p>
		        	</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-4">
		        		<label for="">Payment Type</label>
		        		<select class="form-control paymentType" name="PAYMENT_TYPE" id="">
		        			<option value="1">Cash</option>
		        			<option value="2">CC</option>
		        			<option value="3">Cheque</option>
		        		</select>
		        	</div>
		        	<div class="col-md-4">
	        		<label for="">Payment Date:</label>
		        		<input value="<?php echo date('d/m/Y');?>" name="PAYMENT_DATE" class="form-control payDate enquiry_date" type="text">
		        	</div>
		        	<div class="col-md-4">
		        		<label for="">Cheque Date:</label>
		        		<input name="CHEQUE_DATE" class="form-control enquiry_date chequeDate" disabled="" type="text">
		        	</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-4">
		        		<label for="">Cheque/DD/TID No:</label>
		        		<input class="form-control chequeNo" name="CHEQUE_NO" disabled="" type="text">
		        	</div>
		        	<div class="col-md-4">
		        		<label for="">Bank Name</label>
		        		<input class="form-control bankName" name="BANK_NAME" disabled="" type="text">
		        	</div>
		        	<div class="col-md-4">
		        		<label for="">Bank Branch</label>
		        		<input class="form-control bankBranch" name="BANK_BRANCH" disabled="" type="text">
		        	</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-4">
		        		<label for="">Received By: <span class="text-danger">*</span></label>
							<select class="form-control recBy" name="RECEIVED_BY" id="">
								<!-- <option value="">Emp1</option>
								<option value="">Emp2</option> -->
								<?php foreach($employees as $employee){ ?>
									<option value="<?php echo $employee['EMPLOYEE_ID']; ?>"><?php echo $employee['EMP_FNAME'].' '.$employee['EMP_LASTNAME']; ?></option>
								<?php } ?>
							</select>
		        	</div>
		        	<div class="col-md-4">
		        		<label for="">Remarks:</label>
		        		<input class="form-control bankBranch" name="REMARKS" type="text">
		        	</div>
		        	<div class="col-md-4">
		        		<label for="">Exam/Course Module:</label>
		        		<select name="exmCrsMod" id="" class="exmCrsMod form-control"></select>
		        	</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-4">&nbsp;</div>
		        	<div class="col-md-4">
		        		<p>&nbsp;</p>
		        		<div class="col-md-6">
		        			<button type="button" class="btn btn-info" id="saveOtherReceipt">Save</button>
		        		</div>
		        		<div class="col-md-6">
		        			<button class="btn btn-danger" data-dismiss="modal">Cancel</button>
		        		</div>
		        	</div>
		        	<div class="col-md-4">&nbsp;</div>
		        </div>
		        <div class="row">
		        	<div class="col-md-12">
		        		<table class="table table-hover table-striped receiptTbl">
		        			<thead>
		        				<tr>
			        				<th>Receipt No</th>
			        				<th>Date</th>
			        				<th>Particular</th>
			        				<th>Amount Paid WGST</th>
			        				<th>Amount W/o GST</th>
			        				<th>Payment Type</th>
			        				<th>Print</th>
		        				</tr>
		        			</thead>
		        			<tbody class="officialReceiptTblData"></tbody>
		        		</table>
		        	</div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>

		  </div>
		</div>
	<!-- official Receipts Modal Ends -->
