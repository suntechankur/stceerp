<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analysis_controller extends MX_Controller {
	public function __construct()
        {
        	$this->load->model('analysis_model');
        	$this->load->model('tele_enquiry/tele_enquiry_model');
        }

    /*Lead Conversion Analysis Report Starts*/
    public function leadConvAnalysisRep($offset = 0){
    	if(!empty($_POST)){
            $this->session->set_userdata('conv_filter_data',$_POST);

        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('conv_filter_data');
        }

        $data['conv_filter_data'] = $this->session->userdata('conv_filter_data');


        if (isset($data['conv_filter_data']['from_date'])) {
        	$fromDateTxt = $data['conv_filter_data']['from_date'];
        }
        else{
        	$fromDateTxt = '';
        }

        if (isset($data['conv_filter_data']['to_date'])) {
        	$toDateTxt = $data['conv_filter_data']['to_date'];
        }
        else{
        	$toDateTxt = '';
        }

    	if($fromDateTxt != ''){
    		$fromDateRep = str_replace('/', '-', $fromDateTxt);
    		$fromDate = date("Y-m-d H:i:s", strtotime($fromDateRep));
    	}
    	else{
    		$fromDate = date('Y-m-d H:i:s');
    	}

    	if($toDateTxt != ''){
    		$toDateRep = str_replace('/', '-', $toDateTxt);
    		$toDate = date("Y-m-d H:i:s", strtotime($toDateRep));
    	}
    	else{
    		$toDate = date('Y-m-d H:i:s');
    	}

    	 /* Pagination starts */
        $config['base_url'] = base_url('reports/leadConvAnalysisRep/');
        $config['total_rows'] = $this->analysis_model->getSourcesCount($fromDate,$toDate);
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */

    	$getSources = $this->analysis_model->getSources($fromDate,$toDate,$offset,$config['per_page']);
    	$getSourcesCounts = array();
    	for ($j=0; $j <count($getSources) ; $j++) {
    		$leadsCount = $this->analysis_model->getLeadsCount($fromDate,$toDate,$getSources[$j]['SOURCE']);

    		$walkinCount = $this->analysis_model->getWalkinCount($fromDate,$toDate,$getSources[$j]['SOURCE']);

    		$isEnrolledCount = $this->analysis_model->isEnrolledCount($fromDate,$toDate,$getSources[$j]['SOURCE']);

    		$getSourcesCounts[] = array(
    			'CENTRE_ID' => $getSources[$j]['CENTRE_ID'],
    			'LEADS' => $leadsCount,
    			'WALKINS' => $walkinCount,
    			'ISENROLLED' => $isEnrolledCount
    			);
    	}
    	$data['centres'] = $this->tele_enquiry_model->suggested_centre($fromDate,$toDate);
    	$data['getSourceCounts'] = $getSourcesCounts;
        $data['add_bel_cust_js'] = base_url('resources/js/analysis.js');
    	$data['sources'] = $getSources;
    	$data['pagination'] = $this->pagination->create_links();
    	$this->load->admin_view('leadConvAnalysisRep',$data);
    }
    /*Lead Conversion Analysis Report Ends*/

    public function leadExport($offset = 0){
    	$fromDateTxt = $this->input->post('from_date');
    	$toDateTxt = $this->input->post('to_date');

    	if($fromDateTxt != ''){
    		$fromDateRep = str_replace('/', '-', $fromDateTxt);
    		$fromDate = date("Y-m-d H:i:s", strtotime($fromDateRep));
    	}
    	else{
    		$fromDate = date('Y-m-d H:i:s');
    	}

    	if($toDateTxt != ''){
    		$toDateRep = str_replace('/', '-', $toDateTxt);
    		$toDate = date("Y-m-d H:i:s", strtotime($toDateRep));
    	}
    	else{
    		$toDate = date('Y-m-d H:i:s');
    	}
        $this->load->library('excel');

        $centre = $this->tele_enquiry_model->suggested_centre();

        $getSources = $this->analysis_model->getSources($fromDate,$toDate,$offset = 0,$perPage = 0);

        $this->excel->getActiveSheet()->setCellValue('A2','Source');

        $sourceCol = 3;
        for ($s=0; $s <count($getSources) ; $s++) {
            $this->excel->getActiveSheet()->setCellValue('A'.$sourceCol,$getSources[$s]['SOURCE']);
            $sourceCol++;
        }



		$col = 'A';
		$num = array('0','1');
		$alpha = $this->mkRange('A','ZZ');
        $alphabets = array_merge($num,$alpha);

		$colNo = 1;
        $j = 1;
        $k = 2;
        $l = 3;
        $centreCount = 0;

        $leadsStr = '';
        $walkinsStr = '';
        $enrolledStr = '';

		for ($i=0; $i <count($centre) ; $i++) {

            $colNos = $colNo+1;
            $centreCol = $alpha[$j].'1';

            $leads = $alpha[$j].'2';
            $walkins = $alpha[$k].'2';
            $enrolled = $alpha[$l].'2';

            $leadsStr .= $alpha[$j].',';
            $walkinsStr .= $alpha[$k].',';
            $enrolledStr .= $alpha[$l].',';

            $this->excel->getActiveSheet()->setCellValue($centreCol,$centre[$centreCount]['CENTRE_NAME']);
            $this->excel->getActiveSheet()->setCellValue($leads,'Leads');
            $this->excel->getActiveSheet()->setCellValue($walkins,'Walkins');
			$this->excel->getActiveSheet()->setCellValue($enrolled,'Enrolled');

			$colNo++;
			$j+=3;
            $k+=3;
            $l+=3;
            $centreCount++;
		}

        $leadsTrim = rtrim($leadsStr,",");
        $leadsArr = explode(",", $leadsTrim);

        $walkinsTrim = rtrim($walkinsStr,",");
        $walkinsArr = explode(",", $walkinsTrim);

        $enrolledTrim = rtrim($enrolledStr,",");
        $enrolledArr = explode(",", $enrolledTrim);

        for ($n=0; $n <count($getSources) ; $n++) {
            $leadsCount = $this->analysis_model->getLeadsCount($fromDate,$toDate,$getSources[$n]['SOURCE']);

            $walkinCount = $this->analysis_model->getWalkinCount($fromDate,$toDate,$getSources[$n]['SOURCE']);

            $isEnrolledCount = $this->analysis_model->isEnrolledCount($fromDate,$toDate,$getSources[$n]['SOURCE']);

            $getSourcesCounts[] = array(
                'CENTRE_ID' => $getSources[$n]['CENTRE_ID'],
                'LEADS' => $leadsCount,
                'WALKINS' => $walkinCount,
                'ISENROLLED' => $isEnrolledCount
                );
        }

        $leadsFinalCount = '0';
        $walkinFinalCount = '0';
        $isEnrolledFinalCount = '0';

        $o = 3;
        $p = 0;
        $leadCountStr = '';
        $walkinCountStr = '';
        $isEnrolledCountStr = '';
        for ($m=0; $m <count($getSources) ; $m++) {
            foreach($centre as $cen){
                if($cen['CENTRE_ID'] == $getSourcesCounts[$m]['CENTRE_ID']){
                    $leadCount = $getSourcesCounts[$m]['LEADS'].',';
                    $walkinCount = $getSourcesCounts[$m]['WALKINS'].',';
                    $isEnrolledCount = $getSourcesCounts[$m]['ISENROLLED'].',';
                }
                else{
                    $leadCount = '0,';
                    $walkinCount = '0,';
                    $isEnrolledCount = '0,';

                }

                $leadCountStr .= $leadCount;
                $walkinCountStr .= $walkinCount;
                $isEnrolledCountStr .= $isEnrolledCount;
                $p++;
            }
            $o++;
        }
            $leadCountTrim = rtrim($leadCountStr,",");
            $leadCountArr = explode(",", $leadCountTrim);

            $walkinCountTrim = rtrim($walkinCountStr,",");
            $walkinCountArr = explode(",", $walkinCountTrim);

            $isEnrolledCountTrim = rtrim($isEnrolledCountStr,",");
            $isEnrolledCountArr = explode(",", $isEnrolledCountTrim);

            $centreCountLess = count($centre) - 1;

            $index = 2;
            $centreCount = count($centre);
            for ($q=0; $q <count($leadCountArr) ; $q++) {
                $leadCountVar = $leadCountArr[$q];
                $walkinCountVar = $walkinCountArr[$q];
                $isEnrolledCountVar = $isEnrolledCountArr[$q];

                $finalLeadCount = $leadCountVar;
                if($q%$centreCount == 0){
                    $index++;
                     }
                     $this->excel->getActiveSheet()->setCellValue($leadsArr[$q%$centreCount].$index,$leadCountVar);
                     $this->excel->getActiveSheet()->setCellValue($walkinsArr[$q%$centreCount].$index,$walkinCountVar);
                     $this->excel->getActiveSheet()->setCellValue($enrolledArr[$q%$centreCount].$index,$isEnrolledCountVar);
            }

		$filename='leads_lists.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); /* tell browser what's the file name */
		header('Cache-Control: max-age=0'); //no cache

        /* save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type) */

        /* if you want to save it as .XLSX Excel 2007 format */
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        /* force user to download the Excel file without writing it to server's HD */
		$objWriter->save('php://output');
    }

    public function mkRange($start,$end) {
        $count = $this->strToInt($end) - $this->strToInt($start);
        $r = array();
        do {$r[] = $start++;} while ($count--);
        return $r;
    }

    public function strToInt($str) {
        $str = strrev($str);
        $dec = 0;
        for ($i = 0; $i < strlen($str); $i++) {
            $dec += (base_convert($str[$i],36,10)-9) * pow(26,$i);
        }
        return $dec;
    }

    public function leadSourceReports() {
        if(!empty($_POST)){
            $this->session->set_userdata('lead_source_search',$_POST);
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('lead_source_search');
        }


        $data['lead_source_search'] = $this->session->userdata('lead_source_search');


        if (isset($data['lead_source_search']['from_date'])) {
            $fromDateTxt = $data['lead_source_search']['from_date'];
        }
        else{
            $fromDateTxt = '';
        }

        if (isset($data['lead_source_search']['to_date'])) {
            $toDateTxt = $data['lead_source_search']['to_date'];
        }
        else{
            $toDateTxt = '';
        }

        if($fromDateTxt != ''){
            $fromDateRep = str_replace('/', '-', $fromDateTxt);
            $fromDate = date("Y-m-d H:i:s", strtotime($fromDateRep));
        }
        else{
            $fromDate = date('Y-m-d H:i:s');
        }

        if($toDateTxt != ''){
            $toDateRep = str_replace('/', '-', $toDateTxt);
            $toDate = date("Y-m-d H:i:s", strtotime($toDateRep));
        }
        else{
            $toDate = date('Y-m-d H:i:s');
        }

        $data['sources'] = $this->analysis_model->getTeleSources($fromDate,$toDate);

      $data['getSourceData'] = array();
      $getLeadsCounts = array();
      $getForwardCounts = array();
      $getNotForwardCounts = array();
        for($i=0;$i<count($data['sources']);$i++){
            $leadCount = $this->analysis_model->getLeadsCountByDate($fromDate,$toDate,$data['sources'][$i]['source']);
            array_push($getLeadsCounts, $leadCount);
            $forwardCount = $this->analysis_model->getLeadsForwardedByDate($fromDate,$toDate,$data['sources'][$i]['source']);
            array_push($getForwardCounts, $forwardCount);

            $notForwardCount = $this->analysis_model->getLeadsNotForwardedByDate($fromDate,$toDate,$data['sources'][$i]['source']);
            array_push($getNotForwardCounts, $notForwardCount);

        }

        $data['totalCount'] = array(
            'totalcount' => array_sum($getLeadsCounts),
            'forwarded' => array_sum($getForwardCounts),
            'notforwarded' => array_sum($getNotForwardCounts)
        );


        for($k=0;$k<count($data['sources']);$k++){
            if(is_numeric($data['sources'][$k]['source'])){
                $source_name = $this->analysis_model->getSourceName($data['sources'][$k]['source']);
            }
            else{
                $source_name = $data['sources'][$k]['source'];
            }
            $getSourceData = array(
                    'source' => $source_name,
                    'totalcount' => $getLeadsCounts[$k],
                    'forwarded' => $getForwardCounts[$k],
                    'notforwarded' => $getNotForwardCounts[$k],
                    'percentage' => round($getLeadsCounts[$k] * 100 / $data['totalCount']['totalcount']).' %'
                    );
            array_push($data['getSourceData'] , $getSourceData);
        }


        $this->load->admin_view('leadSourceReports',$data);
    }

    public function export_to_excel_leadSourceReports(){

        if(!empty($_POST)){
            $this->session->set_userdata('lead_source_search',$_POST);
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('lead_source_search');
        }


        $data['lead_source_search'] = $this->session->userdata('lead_source_search');


        if (isset($data['lead_source_search']['from_date'])) {
            $fromDateTxt = $data['lead_source_search']['from_date'];
        }
        else{
            $fromDateTxt = '';
        }

        if (isset($data['lead_source_search']['to_date'])) {
            $toDateTxt = $data['lead_source_search']['to_date'];
        }
        else{
            $toDateTxt = '';
        }

        if($fromDateTxt != ''){
            $fromDateRep = str_replace('/', '-', $fromDateTxt);
            $fromDate = date("Y-m-d H:i:s", strtotime($fromDateRep));
        }
        else{
            $fromDate = date('Y-m-d H:i:s');
        }

        if($toDateTxt != ''){
            $toDateRep = str_replace('/', '-', $toDateTxt);
            $toDate = date("Y-m-d H:i:s", strtotime($toDateRep));
        }
        else{
            $toDate = date('Y-m-d H:i:s');
        }

        $data['sources'] = $this->analysis_model->getTeleSources($fromDate,$toDate);

      $data['getSourceData'] = array();
      $getLeadsCounts = array();
      $getForwardCounts = array();
      $getNotForwardCounts = array();
        for($i=0;$i<count($data['sources']);$i++){
            $leadCount = $this->analysis_model->getLeadsCountByDate($fromDate,$toDate,$data['sources'][$i]['source']);
            array_push($getLeadsCounts, $leadCount);
            $forwardCount = $this->analysis_model->getLeadsForwardedByDate($fromDate,$toDate,$data['sources'][$i]['source']);
            array_push($getForwardCounts, $forwardCount);

            $notForwardCount = $this->analysis_model->getLeadsNotForwardedByDate($fromDate,$toDate,$data['sources'][$i]['source']);
            array_push($getNotForwardCounts, $notForwardCount);

        }

        $data['totalCount'] = array(
            'totalcount' => array_sum($getLeadsCounts),
            'forwarded' => array_sum($getForwardCounts),
            'notforwarded' => array_sum($getNotForwardCounts)
        );


        for($k=0;$k<count($data['sources']);$k++){
            if(is_numeric($data['sources'][$k]['source'])){
                $source_name = $this->analysis_model->getSourceName($data['sources'][$k]['source']);
            }
            else{
                $source_name = $data['sources'][$k]['source'];
            }
            $getSourceData = array(
                    'source' => $source_name,
                    'totalcount' => $getLeadsCounts[$k],
                    'forwarded' => $getForwardCounts[$k],
                    'notforwarded' => $getNotForwardCounts[$k],
                    'percentage' => round($getLeadsCounts[$k] * 100 / $data['totalCount']['totalcount']).' %'
                    );
            array_push($data['getSourceData'] , $getSourceData);
        }

        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getActiveSheet()->setCellValue('A1','Source');
        $objPHPExcel->getActiveSheet()->setCellValue('B1','Total Leads');
        $objPHPExcel->getActiveSheet()->setCellValue('C1','Forwarded Leads');
        $objPHPExcel->getActiveSheet()->setCellValue('D1','Not Forwarded Leads');
        $objPHPExcel->getActiveSheet()->setCellValue('E1','Percentage');

        $j = 2;
        foreach ($data['getSourceData'] as $key => $value) {
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$j,$data['getSourceData'][$key]['source']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$j,$data['getSourceData'][$key]['totalcount']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$j,$data['getSourceData'][$key]['forwarded']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$j,$data['getSourceData'][$key]['notforwarded']);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$j,$data['getSourceData'][$key]['percentage']);

            $j++;
        }

        // for last row insertion of final calculation
        $k = count($data['getSourceData']) + 2;
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$k,$data['totalCount']['totalcount']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$k,$data['totalCount']['forwarded']);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$k,$data['totalCount']['notforwarded']);

        // make bold row
        $objPHPExcel->getActiveSheet()->getStyle("A1:E1")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A".$k.":E".$k)->getFont()->setBold(true);

        $filename = "Leads_source_reports-" . date("h.i.s.d-m-Y") . ".xls";
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=" . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');

    }

    public function enquirySummaryReports(){
        if(isset($_POST['generate_enquiry_summary'])){
            $this->session->set_userdata('enquiry_summary_report',$_POST);
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('enquiry_summary_report');
						redirect('reports/enquirySummaryReports');
        }


        $data['enquiry_summary_report'] = $this->session->userdata('enquiry_summary_report');


        if (isset($data['enquiry_summary_report']['from_date'])) {
            $fromDateTxt = $data['enquiry_summary_report']['from_date'];
        }
        else{
            $fromDateTxt = '';
        }

        if (isset($data['enquiry_summary_report']['to_date'])) {
            $toDateTxt = $data['enquiry_summary_report']['to_date'];
        }
        else{
            $toDateTxt = '';
        }

        if($fromDateTxt != ''){
            $fromDateRep = str_replace('/', '-', $fromDateTxt);
            $fromDate = date("Y-m-d", strtotime($fromDateRep));
        }
        else{
            $fromDate = date('Y-m-d');
        }

        if($toDateTxt != ''){
            $toDateRep = str_replace('/', '-', $toDateTxt);
            $toDate = date("Y-m-d", strtotime($toDateRep));
        }
        else{
            $toDate = date('Y-m-d');
        }

        $data['sources'] = $this->tele_enquiry_model->get_tele_sources();

        $data['summary_data'] = array();
        foreach($data['sources'] as $key => $value){
            $count_tele_enquiries = $this->analysis_model->get_enquiry_count_and_walkins_from_source($fromDate,$toDate,$data['sources'][$key]['controlfile_id'],$data['sources'][$key]['controlfile_value'],"normal");
            $count_walkins = $this->analysis_model->get_enquiry_count_and_walkins_from_source($fromDate,$toDate,$data['sources'][$key]['controlfile_id'],$data['sources'][$key]['controlfile_value'],"walkin");
            $count_admission = $this->analysis_model->get_enquiry_count_and_walkins_from_source($fromDate,$toDate,$data['sources'][$key]['controlfile_id'],$data['sources'][$key]['controlfile_value'],"admission");
            //$admission_id_for_receipt_sum = $this->analysis_model->get_admission_id_from_enquiry_master($fromDate,$toDate,$data['sources'][$key]['controlfile_id'],$data['sources'][$key]['controlfile_value']);
            $total_billing_by_admission_ids = $this->analysis_model->get_sum_of_all_total_billing_by_admission_ids($fromDate,$toDate,$data['sources'][$key]['controlfile_id'],$data['sources'][$key]['controlfile_value']);
            $total_collection_by_admission_ids = $this->analysis_model->get_sum_of_all_total_collection_by_admission_ids($fromDate,$toDate,$data['sources'][$key]['controlfile_id'],$data['sources'][$key]['controlfile_value']);

						if($total_billing_by_admission_ids == NULL){
							$total_billing_by_admission_ids = 0;
						}
						if($total_collection_by_admission_ids == NULL){
							$total_collection_by_admission_ids = 0;
						}
						// echo "<pre>";
						// print_r($admission_id_for_receipt_sum);
						// echo "total billing :".$total_billing_by_admission_ids."<br>";
						// echo "total collection :".$total_collection_by_admission_ids."<br>";
						// echo "</pre>";
            // $fees = 0;
            // $total_fees = 0;
						// $total_deposited_fees = 0;
						// $deposited_fees = 0;
						// if(!empty($admission_id_for_receipt_sum)){
	          //   foreach($admission_id_for_receipt_sum as $admission_ids){
	          //       foreach ($admission_ids as $admission_id) {
	          //           //$fees = $this->analysis_model->get_sum_of_admission_total_fees_and_deposited_fees($admission_id,'total_fees');
	          //           $deposited_fees = $this->analysis_model->get_sum_of_admission_total_fees_and_deposited_fees($admission_id,'deposited_fees');
	          //       }
	          //       //$total_fees += $fees;
	          //       $total_deposited_fees += $deposited_fees;
	          //   }
						// }
						//
						//
						// echo "total collection :".$total_deposited_fees."<br>";
						$checking_for_adding_in_array = $count_admission + $total_billing_by_admission_ids + $total_collection_by_admission_ids + $count_tele_enquiries + $count_walkins;
						if($checking_for_adding_in_array > 0){
	            $enquiry_summary = array(
	                'source' => $data['sources'][$key]['controlfile_value'],
	                'A' => $count_admission,
	                'B' => $total_billing_by_admission_ids,
	                'C' => $total_collection_by_admission_ids,
	                'E' => $count_tele_enquiries,
	                'W' => $count_walkins);

	            array_push($data['summary_data'],$enquiry_summary);
						}
        }

				$data['total_admission_count'] = 0;
				$data['total_billing_amount'] = 0;
				$data['total_collection_amount'] = 0;
				$data['total_enquiries_count'] = 0;
				$data['total_walkins_count'] = 0;
				foreach($data['summary_data'] as $summary){
					$data['total_admission_count'] += $summary['A'];
					$data['total_billing_amount'] += $summary['B'];
					$data['total_collection_amount'] += $summary['C'];
					$data['total_enquiries_count'] += $summary['E'];
					$data['total_walkins_count'] += $summary['W'];
				}

        $this->load->admin_view('enquirySummaryReports',$data);

    }
}
