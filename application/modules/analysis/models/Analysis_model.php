<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analysis_model extends CI_Model {
	public function getSources($fromDate,$toDate,$offset,$perPage){
		$query = $this->db->select('SM.SOURCE_ID,SM.SOURCE_SUBSOURCE,TE.ENQUIRY_DATE,TE.SOURCE,TE.CENTRE_ID')
						  ->from('tele_enquiries TE')
						  ->join('source_master SM','SM.SOURCE_ID = TE.SOURCE','left')
						  ->where('TE.ENQUIRY_DATE >=', $fromDate)
						  ->where('TE.ENQUIRY_DATE <=',$toDate)
						  ->group_by('TE.SOURCE')
						  ->limit($perPage,$offset)
						  ->get();
		return $query->result_array();
	}

	public function getSourcesCount($fromDate,$toDate){
		$query = $this->db->select('SM.SOURCE_ID,SM.SOURCE_SUBSOURCE,TE.ENQUIRY_DATE,TE.SOURCE,TE.CENTRE_ID')
						  ->from('tele_enquiries TE')
						  ->join('source_master SM','SM.SOURCE_ID = TE.SOURCE','left')
						  ->where('TE.ENQUIRY_DATE >=', $fromDate)
						  ->where('TE.ENQUIRY_DATE <=',$toDate)
						  ->group_by('TE.SOURCE')
						  ->get();
		return $query->num_rows();
	}

	public function getLeadsCount($fromDate,$toDate,$sourceId){
		$query = $this->db->select('TE.TELE_ENQUIRY_ID')
						  ->from('tele_enquiries TE')
						  ->where('TE.ENQUIRY_DATE >=', $fromDate)
						  ->where('TE.ENQUIRY_DATE <=',$toDate)
						  ->where('TE.SOURCE',$sourceId)
						  ->get();
		return $query->num_rows();
	}

	public function getWalkinCount($fromDate,$toDate,$sourceId){
		$query = $this->db->select('TE.TELE_ENQUIRY_ID')
						  ->from('tele_enquiries TE')
						  ->where('TE.ENQUIRY_DATE >=', $fromDate)
						  ->where('TE.ENQUIRY_DATE <=',$toDate)
						  ->where('TE.SOURCE',$sourceId)
						  ->where('TE.ENQUIRY_ID !=','')
						  ->get();
		return $query->num_rows();
	}

	public function isEnrolledCount($fromDate,$toDate,$sourceId){
		$query = $this->db->select('TE.TELE_ENQUIRY_ID')
						  ->from('tele_enquiries TE')
						  ->where('TE.ENQUIRY_DATE >=', $fromDate)
						  ->where('TE.ENQUIRY_DATE <=',$toDate)
						  ->where('TE.SOURCE',$sourceId)
						  ->where('ISENROLLED','1')
						  ->or_where('TE.ADMISSION_ID !=','')
						  ->where('TE.ENQUIRY_DATE >=', $fromDate)
						  ->where('TE.ENQUIRY_DATE <=',$toDate)
						  ->where('TE.SOURCE',$sourceId)
						  ->get();
		return $query->num_rows();
	}

	public function getTeleSources($fromDate,$toDate){
		$query = $this->db->select('distinct(source)')
						  ->from('tele_enquiries TE')
						  ->where('TE.ENQUIRY_DATE >=', $fromDate)
						  ->where('TE.ENQUIRY_DATE <=',$toDate)
						  ->get();
		return $query->result_array();
	}

	public function getLeadsCountByDate($fromDate,$toDate,$source){
		$where = "TE.ENQUIRY_DATE BETWEEN '$fromDate' and '$toDate'";
		$query = $this->db->select('count(source) as total')
						  ->from('tele_enquiries TE')
						  ->where($where)
						  ->where('TE.source',$source)
						  ->get();
		return $query->row()->total;
	}

	public function getLeadsForwardedByDate($fromDate,$toDate,$source){
		$where = "TE.ENQUIRY_DATE BETWEEN '$fromDate' and '$toDate'";
		$query = $this->db->select('count(*) as forward')
						  ->from('tele_enquiries TE')
						  ->where($where)
						  ->where('TE.source',$source)
						  ->where('TE.suggested_centre_id != 155')
						  ->get();
		return $query->row()->forward;
	}

	public function getLeadsNotForwardedByDate($fromDate,$toDate,$source){
		$where = "TE.ENQUIRY_DATE BETWEEN '$fromDate' and '$toDate'";
		$query = $this->db->select('count(*) as notforward')
						  ->from('tele_enquiries TE')
						  ->where($where)
						  ->where('TE.source',$source)
						  ->where('TE.suggested_centre_id = 155')
						  ->get();
		return $query->row()->notforward;
	}

	public function getSourceName($source){
        $query = $this->db->select('controlfile_value as source')
                          ->from('control_file')
                          ->where('controlfile_key="HEARD_ABOUT_SACL" and ISACTIVE=1')
                          ->where('controlfile_id',$source)
                          ->get();
        return $query->row()->source;
	}

	public function get_enquiry_count_and_walkins_from_source($fromDate,$toDate,$source_id,$source_name,$type){

				if($type == 'walkin'){
	        $where = "ENQUIRY_DATE BETWEEN '$fromDate' AND '$toDate'";
	        $where1 = '(HEARD_ABOUTUS ="'.$source_id.'" or HEARD_ABOUTUS ="'.$source_name.'")';
					$query = $this->db->select('count(ENQUIRY_ID) as total_enquiry')
														->from('enquiry_master')
														->where('TELE_ENQUIRY_ID IS NOT NULL')
														->where($where)
														->where($where1)
														->get();
				}
				else if($type == 'admission'){
	        $where = "AM.ADMISSION_DATE BETWEEN '$fromDate' AND '$toDate'";
		      $where1 = '(EM.HEARD_ABOUTUS ="'.$source_id.'" or EM.HEARD_ABOUTUS ="'.$source_name.'")';
					$query = $this->db->select('count(AM.ADMISSION_ID) as total_enquiry')
														->from('admission_master AM')
														->join('enquiry_master EM','AM.ENQUIRY_ID=EM.ENQUIRY_ID','left')
														->where($where)
														->where($where1)
														->get();
				}
				else{
		      $where = "ENQUIRY_DATE BETWEEN '$fromDate' AND '$toDate'";
        	$where2 = '(SOURCE ="'.$source_id.'" or SOURCE ="'.$source_name.'")';
	        $query = $this->db->select('count(TELE_ENQUIRY_ID) as total_enquiry')
	                          ->from('tele_enquiries')
														->where($where)
														->where($where2)
	                          ->get();
				}

        return $query->row()->total_enquiry;
	}

	public function get_admission_id_from_enquiry_master($fromDate,$toDate,$source_id,$source_name){
				$where = "AM.ADMISSION_DATE BETWEEN '$fromDate' AND '$toDate'";
				$where1 = '(EM.HEARD_ABOUTUS ="'.$source_id.'" or EM.HEARD_ABOUTUS ="'.$source_name.'")';
				$query = $this->db->select('AM.ADMISSION_ID')
													->from('admission_master AM')
													->join('enquiry_master EM','AM.ENQUIRY_ID=EM.ENQUIRY_ID','left')
													->where($where)
													->where($where1)
													->get();

        return $query->result_array();
	}

	public function get_sum_of_all_total_billing_by_admission_ids($fromDate,$toDate,$source_id,$source_name){
		$where = "EM.ENQUIRY_DATE BETWEEN '$fromDate' AND '$toDate'";
		$where1 = '(EM.HEARD_ABOUTUS ="'.$source_id.'" or EM.HEARD_ABOUTUS ="'.$source_name.'")';
		$query = $this->db->select('SUM(AM.TOTALFEES) as billing')
											->from('admission_master AM')
											->join('enquiry_master EM','AM.ENQUIRY_ID=EM.ENQUIRY_ID','left')
											->where($where)
											->where($where1)
											->get();
		return $query->row()->billing;
	}

	public function get_sum_of_all_total_collection_by_admission_ids($fromDate,$toDate,$source_id,$source_name){
		$where = "(AR.ISACTIVE IS NULL OR AR.ISACTIVE=1)";
		$where1 = "EM.ENQUIRY_DATE BETWEEN '$fromDate' AND '$toDate'";
		$where2 = '(EM.HEARD_ABOUTUS ="'.$source_id.'" or EM.HEARD_ABOUTUS ="'.$source_name.'")';
		$query = $this->db->select('SUM(AR.AMOUNT_PAID_FEES) as collection')
											->from('admission_master AM')
											->join('admission_receipts AR','AM.ADMISSION_ID=AR.ADMISSION_ID','left')
											->join('enquiry_master EM','AM.ENQUIRY_ID=EM.ENQUIRY_ID','left')
											->where($where)
											->where($where1)
											->where($where2)
											->get();
		return $query->row()->collection;
	}

	public function get_sum_of_admission_total_fees_and_deposited_fees($admission_id,$fee_type){
				$query = $this->db->select('*')
													->from('admission_master')
													->where('ADMISSION_ID', $admission_id)
													->get();
				if($query->num_rows())
		    {
					if($fee_type == "total_fees"){
						$query1 = $this->db->select('TOTALFEES as fees')
															->from('admission_master')
															->where('ADMISSION_ID', $admission_id)
															->get();
					}
					else{
						$where = '(ISACTIVE IS NULL OR ISACTIVE=1)';
						$query1 = $this->db->select('SUM(AMOUNT_PAID_FEES) as fees')
															->from('admission_receipts')
															->where('ADMISSION_ID', $admission_id)
															->where($where)
															->get();
					}
					return $query1->row()->fees;
		    }
				else{
					return 0;
				}
	}

}
