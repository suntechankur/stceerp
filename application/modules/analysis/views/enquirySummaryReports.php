<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Enquiry Summary Reports</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
				<?php
				if(isset($enquiry_summary_report['from_date'])){
					$fromDate = $enquiry_summary_report['from_date'];
				}
				else{
					$fromDate = date('d/m/Y');
				}

				if(isset($enquiry_summary_report['to_date'])){
					$toDate = $enquiry_summary_report['to_date'];
				}
				else{
					$toDate = date('d/m/Y');
				}
				 ?>
					<div class="panel-body">
							<div class="col-md-12">
								<div class="form-group">
								 <?php echo form_open(); ?>
									<div class="col-md-3">
										<label for="from_date">From:</label>
										<input type="text" class="form-control enquiry_date" name="from_date" id="from_date" value="<?php echo $fromDate; ?>">
									</div>
									<div class="col-md-3">
										<label for="to_date">To:</label>
										<input type="text" class="form-control enquiry_date" name="to_date" id="to_date" value="<?php echo $toDate; ?>">
									</div>
									<div class="col-md-2">
										<label for="">&nbsp;</label>
										<br>
										<button type="submit" class="btn btn-primary" name="generate_enquiry_summary">Generate</button>
									</div>
									<div class="col-md-2">
										<label for="">&nbsp;</label>
										<br>
										<button type="submit" class="btn btn-primary" name="reset">Reset</button>
									</div>
									<?php echo form_close(); ?>
									<div class="col-md-2">
										<br>
										<?php if (!empty($this->session->userdata('enquiry_summary_report'))) { ?>
										<a onClick ="$('#export_to_excel_enquiry_summary_report').tableExport({type:'excel',escape:'false'});" class="btn" style="float:left" title="Export to excel"><i class="fa fa-file-excel-o fa-2x text-success"></i></a>

										<?php } ?>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br/>
	<?php if($this->session->userdata('enquiry_summary_report')){?>
		<div id="box">
			<h2></h2>
        </div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body table-responsive">
						<table class="table table-striped table-hover" id="export_to_excel_enquiry_summary_report">
							<tr>
								<td><strong>Sr No.</strong></td>
								<td><strong>Source</strong></td>
								<td><strong>A</strong></td>
								<td><strong>B</strong></td>
								<td><strong>C</strong></td>
								<td><strong>E</strong></td>
								<td><strong>W</strong></td>
							</tr>
							<?php $i = 1;
							foreach($summary_data as $sourceData){ ?>
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo $sourceData['source'];?></td>
									<td><?php echo $sourceData['A'];?></td>
									<td><?php echo $sourceData['B'];?></td>
									<td><?php echo $sourceData['C'];?></td>
									<td><?php echo $sourceData['E'];?></td>
									<td><?php echo $sourceData['W'];?></td>
								</tr>
							<?php } ?>

								<tr>
									<td></td>
									<td>Grand Total</td>
									<td><?php echo $total_admission_count;?></td>
									<td><?php echo $total_billing_amount;?></td>
									<td><?php echo $total_collection_amount;?></td>
									<td><?php echo $total_enquiries_count;?></td>
									<td><?php echo $total_walkins_count;?></td>
								</tr>

						</table>
						<br>
					</div>
				</div>
			</div>
		</div>
<?php } ?>
</div>
