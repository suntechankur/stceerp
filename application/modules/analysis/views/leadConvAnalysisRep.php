<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Search Lead Conversion Analysis</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
				<?php
				if(isset($conv_filter_data['from_date'])){
					$fromDate = $conv_filter_data['from_date'];
				}
				else{
					$fromDate = date('d/m/Y');
				}

				if(isset($conv_filter_data['to_date'])){
					$toDate = $conv_filter_data['to_date'];
				}
				else{
					$toDate = date('d/m/Y');
				}
				 ?>
					<div class="panel-body">
							<div class="col-md-12">
								<div class="form-group">
								 <?php echo form_open(); ?>
									<div class="col-md-2">
										<label for="from_date">From:</label>
										<input type="text" class="form-control enquiry_date" name="from_date" id="from_date" value="<?php echo $fromDate; ?>">
									</div>
									<div class="col-md-2">
										<label for="to_date">To:</label>
										<input type="text" class="form-control enquiry_date" name="to_date" id="to_date" value="<?php echo $toDate; ?>">
									</div>
									<div class="col-md-2">
										<label for="">&nbsp;</label>
										<br>
										<button id="generate" type="submit" class="btn btn-primary">Generate</button>
									</div>
									<div class="col-md-2">
										<label for="">&nbsp;</label>
										<br>
										<button id="generate" type="submit" class="btn btn-primary" name="reset">Reset</button>
									</div>
									<!-- <div class="col-md-2">
										<label for="">&nbsp;</label>
										<br> -->
<!-- 										<a href="<?php echo base_url('reports/leadConvAnalysisRepExp'); ?>" title="Export to excel">
											<i class="fa fa-file-excel-o fa-2x text-success" aria-hidden="true"></i>
										</a> -->
									<!-- </div> -->
									<?php echo form_close(); ?>
									<div class="col-md-2">
										<br>
										<?php echo form_open('reports/leadConvAnalysisRepExp'); ?>
										<input type="hidden" name="from_date" class="excel_from_date" value="<?php echo $fromDate; ?>">
										<input type="hidden" name="to_date" class="excel_to_date"  value="<?php echo $toDate; ?>">
										<button class="btn" type="submit">
											<i class="fa fa-file-excel-o fa-2x text-success" aria-hidden="true"></i>
										</button>
										<?php echo form_close(); ?>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="box"> 
			<!-- <p>&nbsp;</p> -->
			<span class=""><?php echo $pagination; ?></span>
        	<h2 class="text-center"> Lead Conversion Analysis</h2>
        </div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body table-responsive">
						<table class="table table-striped table-hover">
							<tr>
								<th>&nbsp;</th>
								<?php foreach($centres as $centre){ ?>
									<th <?php if(!empty($getSourceCounts)){ echo 'colspan="3"'; } ?>><?php echo $centre['CENTRE_NAME']; ?></th>
								<?php } ?>
							</tr>
							<?php if(!empty($getSourceCounts)){ ?>
							<tr>
								<td><strong>source</strong></td>
								<?php foreach($centres as $centre){ ?>
								<td><strong>Leads</strong></td>
								<td><strong>Walkins</strong></td>
								<td><strong>Enrolled</strong></td>
								<?php } ?>
							</tr>
							<?php for ($i=0; $i <count($sources) ; $i++) { ?> 
								<tr>
									<td><?php echo $sources[$i]['SOURCE']; ?></td>
									<?php 
									foreach($centres as $centre){ 
										$leads = '0';
										$walkins = '0';
										$enrolled = '0';
										?>
										<td><?php 
											if($centre['CENTRE_ID'] == $getSourceCounts[$i]['CENTRE_ID']){
												echo $getSourceCounts[$i]['LEADS'];	
											}
											else{
												echo $leads;
											}
											?>
										</td>
										<td>
											<?php 
											if($centre['CENTRE_ID'] == $getSourceCounts[$i]['CENTRE_ID']){
												echo $getSourceCounts[$i]['WALKINS'];	
											}
											else{
												echo $leads;
											}
											?>
										</td>
										<td>
											<?php 
											if($centre['CENTRE_ID'] == $getSourceCounts[$i]['CENTRE_ID']){
												echo $getSourceCounts[$i]['ISENROLLED'];	
											}
											else{
												echo $leads;
											}
											?>
										</td>
									<?php } ?>
								</tr>
							<?php } }else{
								echo '<tr><td colspan="13" class="text-center">No Records Found!</td></tr>';
								} ?>
						</table>
						<br>
					</div>
					<div class="row">
						<div class="col-md-4 pull-right">
							<?php echo $pagination; ?>
							</div>
						</div>
				</div>
			</div>
		</div>
</div>