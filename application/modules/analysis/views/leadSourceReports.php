<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Lead Source Reports</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
				<?php
				if(isset($lead_source_search['from_date'])){
					$fromDate = $lead_source_search['from_date'];
				}
				else{
					$fromDate = date('d/m/Y');
				}

				if(isset($lead_source_search['to_date'])){
					$toDate = $lead_source_search['to_date'];
				}
				else{
					$toDate = date('d/m/Y');
				}
				 ?>
					<div class="panel-body">
							<div class="col-md-12">
								<div class="form-group">
								 <?php echo form_open(); ?>
									<div class="col-md-2">
										<label for="from_date">From:</label>
										<input type="text" class="form-control enquiry_date" name="from_date" id="from_date" value="<?php echo $fromDate; ?>">
									</div>
									<div class="col-md-2">
										<label for="to_date">To:</label>
										<input type="text" class="form-control enquiry_date" name="to_date" id="to_date" value="<?php echo $toDate; ?>">
									</div>
									<div class="col-md-2">
										<label for="">&nbsp;</label>
										<br>
										<button type="submit" class="btn btn-primary">Generate</button>
									</div>
									<div class="col-md-2">
										<label for="">&nbsp;</label>
										<br>
										<button type="reset" class="btn btn-primary" name="reset">Reset</button>
									</div>
									<?php echo form_close(); ?>
									<div class="col-md-2">
										<br>
										<?php echo form_open('reports/export-to-excel-leadSourceReports'); ?>
										<input type="hidden" name="from_date" class="excel_from_date" value="<?php echo $fromDate; ?>">
										<input type="hidden" name="to_date" class="excel_to_date"  value="<?php echo $toDate; ?>">
										<button class="btn" type="submit">
											<i class="fa fa-file-excel-o fa-2x text-success" aria-hidden="true"></i>
										</button>
										<?php echo form_close(); ?>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br/>
		<div id="box"> 
			<h2></h2>
        </div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body table-responsive">
						<table class="table table-striped table-hover">
							<tr>
								<td><strong>Source</strong></td>
								<td><strong>Total leads</strong></td>
								<td><strong>Forwarded Leads</strong></td>
								<td><strong>Not Forwarded Leads</strong></td>
								<td><strong>Percentage</strong></td>

							</tr>
							<?php foreach($getSourceData as $sourceData){ ?> 
								<tr>
									<td><?php echo $sourceData['source'];?></td>
									<td><?php echo $sourceData['totalcount'];?></td>
									<td><?php echo $sourceData['forwarded'];?></td>
									<td><?php echo $sourceData['notforwarded'];?></td>
									<td><?php echo $sourceData['percentage'];?></td>
								</tr>
							<?php } ?>

								<tr>
									<td></td>
									<td><?php echo $totalCount['totalcount'];?></td>
									<td><?php echo $totalCount['forwarded'];?></td>
									<td><?php echo $totalCount['notforwarded'];?></td>
									<td></td>
								</tr>

						</table>
						<br>
					</div>
				</div>
			</div>
		</div>

</div>