<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BatchMaster_controller extends MX_Controller {

     public function __construct()
        {
                $this->load->model('batchMaster_model');
                $this->load->model('tele_enquiry/tele_enquiry_model');
                $this->load->model('admission/admission_model');
                parent::__construct();
        }



/* function for markBatchCompletion*/
public function markBatchCompletion($batchId)
{
	$batchId = $this->encrypt->decode($batchId);

	$completion['batch_details'] = $this->batchMaster_model->batch_detail($batchId);

	$completion['existingStudentsInBatch'] = $this->batchMaster_model->existingStudentsInBatch($batchId);
  $completion['attendanceDate'] = $this->batchMaster_model->attendanceSheetDate($batchId);

	$completion['tp'] = $this->batchMaster_model->totalPresent($batchId);

	$completion['amo10'] = $this->batchMaster_model->AttendanceMarksOutOf10($batchId);

	$completion['totalPresent'] = array($completion['tp'],$completion['amo10']);

	if($this->input->post('markBatchCompletion'))
	{
		$this->form_validation->set_rules('ActualEndDate','Actual EndDate','required|trim|xss_clean');

		if($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('danger',validation_errors());
		}
		else
		{
			if($this->batchMaster_model->isExistsBachCompletion($batchId)>0)
			{
				$this->session->set_flashdata('warning1','Sorry ! This Record Already Saved');
			}
			else
			{
        if($_POST['last_lecture_date'] == ""){
          $this->session->set_flashdata('danger', 'Please mark attendance first.');
          redirect(base_url('batchMaster/markBatchCompletion/'.$this->encrypt->encode($batchId)));
        }
        else{
          $lecture_end_date = date('Y-m-d', strtotime($_POST['ActualEndDate']));
          $last_lecture_date = date('Y-m-d', strtotime($_POST['last_lecture_date']));

          if($lecture_end_date > $last_lecture_date){
            $this->session->set_flashdata('danger', 'Batch Completion date is not proper.');
            redirect(base_url('batchMaster/markBatchCompletion/'.$this->encrypt->encode($batchId)));
          }
          else{
            if($lecture_end_date <= $last_lecture_date){
              if($lecture_end_date != $last_lecture_date){
                $this->session->set_flashdata('danger', 'For batch completion Batch End date should equal to Internal last date.');
                redirect(base_url('batchMaster/markBatchCompletion/'.$this->encrypt->encode($batchId)));
              }
              elseif($lecture_end_date == $last_lecture_date){
                $ActualEndDate = str_replace('/', '-',$this->input->post('ActualEndDate'));
                $ActualEndDate = date('Y-m-d',strtotime($ActualEndDate));

                $createdBy = $this->session->userdata('admin_data')[0]['USER_ID'];
                $createDate=date("Y-m-d H:i:s");

                $update = array(
                'BATCH_STATUS' => 'C',
                'BATCHCOMPLETED' => '1',
                'ACTUALENDDATE' => "$ActualEndDate",
                );

                foreach ($this->input->post('ADMISSION_ID') as $key => $value)
                {
                  $insert = array(
                  'BTCH_INTERNAL_ID' => "$batchId",
                  'ADM_ID' => $this->input->post('ADMISSION_ID')[$key],
                  'MARKS_OBTAINED' => $this->input->post('InternalMarksNew')[$key],
                  // 'FACULTY_REMARK' => $this->input->post('RemarksForStudents')[$key],
                  'MODIFIED_DATE' => $createDate,
                  'MODIFIED_BY' => $createdBy
                  );

                  $save_batch = $this->batchMaster_model->saveBatchCompletion($insert);
                  $batch_update = $this->batchMaster_model->BatchUpdation($batchId,$update);
                  // $save_batch = true;
                  // $batch_update = true;
                  if($save_batch && $batch_update){
                    $centre_name = $completion['batch_details'][0]['CENTRE_NAME'];
                    $course_name = $completion['batch_details'][0]['COURSE_NAME'];
                    $days = $completion['batch_details'][0]['DAYS'];
                    $faculty = $completion['batch_details'][0]['EMP_NAME'];
                    $timing = $completion['batch_details'][0]['STARTTIME']." - ".$completion['batch_details'][0]['ENDTIME'];
                    $batch_start_date = date('d/m/Y',strtotime($completion['batch_details'][0]['STARTDATE']));
                    $batch_end_date = date('d/m/Y',strtotime($completion['batch_details'][0]['EXPECTEDENDDATE']));
                    $batch_expected_end_date = date('d/m/Y',strtotime($completion['batch_details'][0]['INTERNALEXAMDATE']));
                    $ActualEndDate = date('d/m/Y',strtotime($ActualEndDate));

                    $employee_name  = $this->session->userdata('admin_data')[0]['EMP_FNAME'].' '.$this->session->userdata('admin_data')[0]['EMP_LASTNAME'];
                    $from = "emis@saintangelos.com";
                    $to = array('ankur.prajapati@saintangelos.com','dinesh.kanojiya@saintangelos.com');
                    $subject = "Batch Completion Details dated on ".date("d/m/Y")." from ".$employee_name." EMIS Login";
                    $body = "<h2>Batch Details</h2>
                             <br/>
                             <table border='1'>
                             <tr><td>Batch Id.</td><td>$batchId</td></tr>
                             <tr><td>Centre Name</td><td>$centre_name</td></tr>
                             <tr><td>Timings</td><td>$timing</td></tr>
                             <tr><td>Batch Start Date</td><td>$batch_start_date</td></tr>
                             <tr><td>Subject Name</td><td>$course_name</td></tr>
                             <tr><td>Days</td><td>$days</td></tr>
                             <tr><td>Faculty</td><td>$faculty</td></tr>
                             <tr><td>Batch End Date</td><td>$batch_end_date</td></tr>
                             <tr><td>Batch Expected End Date</td><td>$batch_expected_end_date</td></tr>
                             <tr><td><strong>Batch Actual End Date</strong></td><td style='color:green'><strong>$ActualEndDate</strong></td></tr>
                             <tr><td colspan='2' style='font-weight:bold;color:red;'><p>* This Batch Completion send from EMIS.</p></td></tr>
                             </table>";


                    $confirmationMail = $this->load->mailer($from,$to,"","","",$subject,$body,"enquiry");
                    if($confirmationMail){
                      $this->session->set_flashdata('success','Batch Completed Succesfully');
                      redirect(base_url('batchMaster/markBatchCompletion/'.$this->encrypt->encode($batchId)));
                    }
                  }
                }
              }
              else{
                $this->session->set_flashdata('danger', 'For batch completion Batch End date should equal to Internal last date.');
                redirect(base_url('batchMaster/markBatchCompletion/'.$this->encrypt->encode($batchId)));
              }
            }
          }
        }
        exit();
				$this->session->set_flashdata('success','Mark Batch Completion Save Successfully');
				redirect(base_url('BatchMaster/batchsearch'));
			}
		}
	}
	$this->load->admin_view('markBatchCompletion',$completion);
}
/* End function of markBatchCompletion*/

// send batch completion date extend mail shoot implemented on 11/09/2018 by ANKUR PRAJAPATI
public function sendBatchCompletionRequest($batchId){
  $batchId = $this->encrypt->decode($batchId);
  $completion['batch_details'] = $this->batchMaster_model->batch_detail($batchId);
  $centre_name = $completion['batch_details'][0]['CENTRE_NAME'];
  $course_name = $completion['batch_details'][0]['COURSE_NAME'];
  $days = $completion['batch_details'][0]['DAYS'];
  $faculty = $completion['batch_details'][0]['EMP_NAME'];
  $timing = $completion['batch_details'][0]['STARTTIME']." - ".$completion['batch_details'][0]['ENDTIME'];
  $batch_start_date = date('d/m/Y',strtotime($completion['batch_details'][0]['STARTDATE']));
  $batch_end_date = date('d/m/Y',strtotime($completion['batch_details'][0]['EXPECTEDENDDATE']));
  $batch_expected_end_date = date('d/m/Y',strtotime($completion['batch_details'][0]['INTERNALEXAMDATE']));
  $ActualEndDate = date('d/m/Y',strtotime($ActualEndDate));

  $employee_name  = $this->session->userdata('admin_data')[0]['EMP_FNAME'].' '.$this->session->userdata('admin_data')[0]['EMP_LASTNAME'];
  $from = "emis@saintangelos.com";
  $to = array('ankur.prajapati@saintangelos.com','dinesh.kanojiya@saintangelos.com');
  $subject = "Batch Completion Request dated on ".date("d/m/Y")." from ".$employee_name." EMIS Login";
  $body = "<p>Dear Sir/Mam,</p>
           <p>Kindly Extend my batch completion date for below batch details</p>
           <br/>
           <table border='1'>
           <tr><td>Batch Id.</td><td>$batchId</td></tr>
           <tr><td>Centre Name</td><td>$centre_name</td></tr>
           <tr><td>Timings</td><td>$timing</td></tr>
           <tr><td>Batch Start Date</td><td>$batch_start_date</td></tr>
           <tr><td>Subject Name</td><td>$course_name</td></tr>
           <tr><td>Days</td><td>$days</td></tr>
           <tr><td>Faculty</td><td>$faculty</td></tr>
           <tr><td>Batch End Date</td><td>$batch_end_date</td></tr>
           <tr><td>Batch Expected End Date</td><td>$batch_expected_end_date</td></tr>
           </table>
           <br/>
           <p><strong>Thanks & Regards,</strong></p>
           <p>$employee_name</p>";


  $confirmationMail = $this->load->mailer($from,$to,"","","",$subject,$body,"enquiry");
  if($confirmationMail){
    $this->session->set_flashdata('success','Your Query Sent to Batch Administrator.');
    redirect(base_url('batchMaster/markBatchCompletion/'.$this->encrypt->encode($batchId)));
  }
}
// end of mail shoot functinality of batch completion date extend

/*start code for Attendance */
public function attendance($batchId)
{
	$batchId = $this->encrypt->decode($batchId);
	$attendance['batch_details'] = $this->batchMaster_model->batch_detail($batchId);

	$attendance['admissionId'] = $this->batchMaster_model->getBatchMast($batchId);

	$attendance['attendance']  = $this->batchMaster_model->attendanceSheet($batchId);
	//by laxmi
	$attendance['lecture_duration'] = $this->batchMaster_model->lecture_duration($batchId);

	$attendance['attendanceDate'] = $this->batchMaster_model->attendanceSheetDate($batchId);

	$attendance['existingStudentsInBatch'] = $this->batchMaster_model->existingStudentsInBatch($batchId);

	$attendance['attendance_details'] = $this->batchMaster_model->attendance_details($batchId);

	$attendance['attendance_status'] = $this->batchMaster_model->attendance_status($batchId);
	$attendance['new_attendance_status'] = $this->batchMaster_model->new_attendance_status($batchId);
	//by laxmi

	$attendance['centres'] = $this->tele_enquiry_model->get_centres();
	$courseId = $attendance['batch_details'][0]['COURSE_ID'];
	$offset=0;
	$this->load->admin_view('attendance',$attendance);
}
/*End code of Attendance*/


        //// start Import Existing Batch code //////////////////
public function ImportExistingBatch($batchId)
    {
    	$batchId = $this->encrypt->decode($batchId);

    	// $batchId = $this->db->input->post('BATCHID');
    	$data['batch'] = $this->batchMaster_model->get_batch_details($batchId);

    	$FACULTY = $data['batch'][0]['FACULTY'];
    	$data['faculties'] = $this->batchMaster_model->get_emp($FACULTY);

    	$data['centres'] = $this->tele_enquiry_model->get_centres();
    	$data['courses'] = $this->tele_enquiry_model->course_interested();


    	if(!empty($_POST))
    	{
			$this->form_validation->set_rules('CENTRE_ID', 'CENTRE ', 'required');
			$this->form_validation->set_rules('SUBJECT', 'SUBJECT', 'required');
			$this->form_validation->set_rules('STARTDATE', 'STARTDATE', 'required');
			// $this->form_validation->set_rules('EMPLOYEE_ID', 'EMPLOYEE', 'required');
			// $this->form_validation->set_rules('FACULTY_NAME', 'FACULTY NAME', 'required');

			$this->form_validation->set_rules('EXPECTEDENDDATE', 'EXPECTEDENDDATE', 'required');
			$this->form_validation->set_rules('STARTTIME', 'STARTTIME', 'required');
			$this->form_validation->set_rules('ENDTIME', 'ENDTIME', 'required');
			$this->form_validation->set_rules('INTERNALEXAMDATE', 'INTERNALEXAMDATE', 'required');

			$this->form_validation->set_rules('ATTENDANCESHEETNO', 'ATTENDANCESHEETNO', 'required');
			// $this->form_validation->set_rules('SESSION', 'SESSION', 'required');
			// $this->form_validation->set_rules('BATCH_STATUS_REMARK', 'BATCH_STATUS_REMARK', 'required');


			if($this->form_validation->run() == TRUE)
			{



			$MODIFIEDBY= $this->session->userdata('admin_data');
			$MODIFIEDBY = $MODIFIEDBY[0]['USER_ID'];

			if(isset($_POST['DAYS']))
			{
				foreach($_POST['DAYS'] as $check)
				{
					$check = implode('/', $_POST['DAYS']);
				}
			}
			else
			{
				$check = '';
			}

			$STARTDATE = str_replace('/', '-', $this->input->post('STARTDATE'));
		    $STARTDATE =  date("Y-m-d", strtotime($STARTDATE));

		    $EXPECTEDENDDATE = str_replace('/', '-', $this->input->post('EXPECTEDENDDATE'));
		    $EXPECTEDENDDATE =  date("Y-m-d", strtotime($EXPECTEDENDDATE));

			$INTERNALEXAMDATE = str_replace('/', '-', $this->input->post('INTERNALEXAMDATE'));
		    $INTERNALEXAMDATE =  date("Y-m-d", strtotime($INTERNALEXAMDATE));

			  // $BATCHID=base64_encode($_POST['BATCHID']);


	 		  $date=date("Y-m-d H:i:s");

	           $insert  =  array(
			  'CENTRE_ID' =>  $this->input->post('CENTRE_ID'),
			  'SUBJECT' =>  $this->input->post('SUBJECT'),
			  'FACULTY' =>  $this->input->post('EMPLOYEE_ID'),
			  // 'FACULTY_NAME' =>  $this->input->post('FACULTY_NAME'),
			  'STARTDATE' =>  "$STARTDATE",
			  'EXPECTEDENDDATE' => "$EXPECTEDENDDATE",
			  'DAYS' => "$check",
			  'STARTTIME' => $this->input->post('STARTTIME'),
			  'ENDTIME' => $this->input->post('ENDTIME'),
			  'INTERNALEXAMDATE' => "$INTERNALEXAMDATE",
			  'BATCHCOMPLETED' => "0",
			  'ATTENDANCESHEETNO' =>  $this->input->post('ATTENDANCESHEETNO'),
			  'SESSION' =>  $this->input->post('SESSION'),
			  'BATCH_STATUS' => "P",
			  'BATCH_STATUS_REMARK' =>  $this->input->post('BATCH_STATUS_REMARK'),
			  'IS_APPROVED_BY_TC' => '',
			  'APPROVED_BY' => '',
			  'MODIFIED_BY' => "$MODIFIEDBY",
			  'MODIFIED_DATE' => $date
			  );


			  $this->batchMaster_model->BatchCreation($insert);

	          $newBatchId = $this->db->insert_id();

	          $StIns = array(
            'newBatchId' => "$newBatchId",
            'BatchID' => "$batchId",
            'MODIFIED_BY' => "$MODIFIEDBY",
            'MODIFIED_DATE' => "$date"
            );

			  $this->batchMaster_model->inheritBatchStudent($StIns);


	          // $this->session->set_flashdata('msg',array('success' => 'Batch Save Successfully'));
	          $this->session->set_flashdata('success', 'Batch Imported Successfully');
	          $this->session->set_flashdata('newBatchId', $newBatchId);
	          redirect(base_url('BatchMaster/batchsearch'));
	    	}
	    	else
	    	{
		    	$this->session->set_flashdata('warning', 'Please fill valid data.');
		    	// redirect(base_url('BatchMaster/batchupdation/'.$batchId));
	    	}
	    }
    	$this->load->admin_view('ImportExistingBatch',$data);
	}

	////// end Import Existing Batch code ///////////////////////////////


public function facultyaudit()
{
	$AuditReportMaster['centres'] = $this->tele_enquiry_model->get_centres();
	$AuditReportMaster['facultyaudit'] ="";
    if(!empty($_POST))
    {
        $this->session->set_userdata('facultyaudit_filter_data',$_POST);
    }
    else
    {
    	$this->session->unset_userdata('facultyaudit_filter_data');
    }
    if(isset($_POST['reset'])){
        $this->session->unset_userdata('facultyaudit_filter_data');
        redirect(base_url('batchMaster/facultyaudit'));
    }

    $data = array(
	    	'Audit_Month' => $this->input->post('Audit_Month'),
	    	'Audit_Year' => $this->input->post('Audit_Year'),
	    	'CENTRE_ID' => $this->input->post('CENTRE_ID')
	    	);

	$AuditReportMaster['facultyaudit_filter_data'] = $this->session->userdata('facultyaudit_filter_data');
	if($this->input->post('submit') != '' && $this->input->post('submit')=="existing")
	{

		$id ="";
		$this->session->set_flashdata('info','');

        $AuditReportMaster['facultyaudit'] = $this->batchMaster_model->get_facultyaudit_list($data);

    	// $this->session->set_flashdata('success','Updated Successfully');
    	// $this->batchMaster_model->facultyaudit_update($id,$data);
	}
	elseif($this->input->post('submit') != '' && $this->input->post('submit')=="CreateNew")
	{

	    $exists = $this->batchMaster_model->facultyaudit_exists($data);

	    $count = count($exists);
	    // echo $count

	    if(!empty($count))
	    {

	    	// $this->batchMaster_model->facultyaudit_save($data);
	    	$this->session->set_flashdata('info','Sorry! Record already exists. Open its Existing Audit');
	    }
	    else
	    {
	    	// $id ="";


	    	// $this->batchMaster_model->facultyaudit_update($id,$data);
	    	$this->session->set_flashdata('info','');
			$AuditReportMaster['facultyaudit'] = $this->batchMaster_model->get_default_facultyaudit_list($data);
	    }
	}
    $this->load->admin_view('AuditReportMaster',$AuditReportMaster);
}

        /*start code for Save Faculy Audit*/
public function facultyaudit_save()
{

	$this->form_validation->set_rules('STREAM[]', 'STREAM', 'required');
	$this->form_validation->set_rules('AUDIT_DATE[]', 'AUDIT DATE', 'required');
	$this->form_validation->set_rules('SUBJECT[]', 'SUBJECT', 'required');
	$this->form_validation->set_rules('TECHNICAL_KNOWLEDGE[]', 'TECHNICAL KNOWLEDGE', 'required');
	$this->form_validation->set_rules('PRESENTATION_SKILL_MODE_OF_TEACHING[]', 'PRESENTATION_SKILL MODE_OF TEACHING', 'required');
	$this->form_validation->set_rules('QUERY_SOLVING_ABILITY[]', 'QUERY SOLVING ABILITY', 'required');
	$this->form_validation->set_rules('EXAMPLE_GIVEN_WHILE_TEACHING[]', 'EXAMPLE GIVEN WHILE TEACHING', 'required');
	$this->form_validation->set_rules('STUDENT_PARTICIPATION[]', 'STUDENT PARTICIPATION', 'required');
	$this->form_validation->set_rules('BODY_LANGUAGE[]', 'BODY LANGUAGE', 'required');
	$this->form_validation->set_rules('ACADEMIC_FEEDBACK[]', 'ACADEMIC FEEDBACK', 'required');
	$this->form_validation->set_rules('BATCH_FILE_STATUS[]', 'BATCH FILE STATUS', 'required');
	$this->form_validation->set_rules('EMIS_UPDATION[]', 'EMIS UPDATION', 'required');
	$this->form_validation->set_rules('LATE_COMING_ABSENT[]', 'LATE COMING ABSENT', 'required');

	if($this->form_validation->run() == FALSE)
	{
		$this->session->set_flashdata('danger', validation_errors());
	}
	else
	{

		foreach($this->input->post('EMPLOYEE_ID') as $k=>$v)
		{


			$createdBy = $this->session->userdata('admin_data')[0]['USER_ID'];
			$createDate=date("Y-m-d H:i:s");

			$AUDIT_DATE = str_replace('/', '-', $this->input->post('AUDIT_DATE')[$k]);
			$AUDIT_DATE = date('Y-m-d',strtotime($AUDIT_DATE));

			$insert = array(
				'EMPLOYEE_ID' => $this->input->post('EMPLOYEE_ID')[$k],
				'STREAM' => $this->input->post('STREAM')[$k],
				'AUDIT_DATE' => "$AUDIT_DATE",
				'SUBJECT' => $this->input->post('SUBJECT')[$k],
				'TECHNICAL_KNOWLEDGE' => $this->input->post('TECHNICAL_KNOWLEDGE')[$k],
				'PRESENTATION_SKILL_MODE_OF_TEACHING' => $this->input->post('PRESENTATION_SKILL_MODE_OF_TEACHING')[$k],
				'QUERY_SOLVING_ABILITY' => $this->input->post('QUERY_SOLVING_ABILITY')[$k],
				'EXAMPLE_GIVEN_WHILE_TEACHING' => $this->input->post('EXAMPLE_GIVEN_WHILE_TEACHING')[$k],
				'STUDENT_PARTICIPATION' => $this->input->post('STUDENT_PARTICIPATION')[$k],
				'BODY_LANGUAGE' => $this->input->post('BODY_LANGUAGE')[$k],
				'ACADEMIC_FEEDBACK' => $this->input->post('ACADEMIC_FEEDBACK')[$k],
				'BATCH_FILE_STATUS' => $this->input->post('BATCH_FILE_STATUS')[$k],
				'EMIS_UPDATION' => $this->input->post('EMIS_UPDATION')[$k],
				'LATE_COMING_ABSENT' => $this->input->post('LATE_COMING_ABSENT')[$k],
				'FACULTY_TEACHING_PERCENT' => $this->input->post('Teaching')[$k],
				'ACADEMIC_FEEDBACK_PERCENT' => $this->input->post('AcademicFeedback')[$k],
				'EMIS_BATCH_FILE_PERCENT' => $this->input->post('EMIS')[$k],
				'PUNCTUALITY' => $this->input->post('Punctuality')[$k],
				'CREATED_BY' => "$createdBy",
				'CREATED_DATE' => "$createDate",
				'Audit_Month' => $this->input->post('Audit_Month'),
				'Audit_Year' => $this->input->post('Audit_Year'),
				'CENTRE_ID' => $this->input->post('CENTRE_ID')
				);

			if($this->input->post('act')=="CreateNew")
			{
				$this->batchMaster_model->facultyaudit_save($insert);
				$this->session->set_flashdata('success', 'Saved successfully.');
			}
			elseif($this->input->post('act')=="existing")
			{
				$id = $this->input->post('AUDIT_REPORT_MASTER_ID')[$k];

				$this->batchMaster_model->facultyaudit_update($id,$insert);
				$this->session->set_flashdata('success', 'Updated successfully.');
			}

		} /// End foreach

	} ///  End Validation


			// die;
	redirect(base_url('batchMaster/facultyaudit'));
}
/*End code of Save Faculy Audit*/

/*start code for batch status isactive = 0, It will not permanently delete*/
public function studentNoticeDelete($noticeId)
{
	$this->batchMaster_model->studentNoticeDelete($noticeId);
	$this->session->set_flashdata('danger', 'Deleted successfully.');
	redirect(base_url('batchMaster/studentNotice'));
}
/*End code of Batch delete*/


///// Student  Notice///////////////
public function studentNotice($offset = '0'){

		if(!empty($_POST))
	    {

			$this->form_validation->set_rules('NOTICE_TITLE', 'NOTICE TITLE', 'required');
			$this->form_validation->set_rules('NOTICEDISPLAY_DATE', 'NOTICEDISPLAY DATE', 'required');
			$this->form_validation->set_rules('UNIVERSITY_ID', 'UNIVERSITY', 'required');
			$this->form_validation->set_rules('NOTICE_BODY', 'NOTICE BODY', 'required');


			if($this->form_validation->run() == TRUE)
			{

			$MODIFIEDBY= $this->session->userdata('admin_data');
			$MODIFIEDBY = $MODIFIEDBY[0]['USER_ID'];

			$STARTDATE = str_replace('/', '-', $this->input->post('STARTDATE'));
		    $STARTDATE =  date("Y-m-d", strtotime($STARTDATE));

	 		  $date=date("Y-m-d H:i:s");

	           $insert  =  array(
			  'NOTICE_TITLE' =>  $this->input->post('NOTICE_TITLE'),
			  'NOTICEDISPLAY_DATE' =>  $this->input->post('NOTICEDISPLAY_DATE'),
			  'UNIVERSITY_ID' =>  $this->input->post('UNIVERSITY_ID'),
			  'NOTICE_BODY' =>  $this->input->post('NOTICE_BODY'),
			  'CREATE_DATE'=>"$date"
			  );
			  $this->batchMaster_model->saveStudentNotice($insert);
	          $this->session->set_flashdata('success', 'Save Successfully');
	          redirect(base_url('batchMaster/studentNotice'));
	    	}
	    	else
	    	{
		    	$this->session->set_flashdata('warning', 'Please fill valid data.');
		    	redirect(base_url('batchMaster/studentNotice'));
	    	}
		}




        if(!empty($_POST)){
            $this->session->set_userdata('studentNotice_filter_data',$_POST);
            $offset=0;
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('studentNotice_filter_data');
            redirect(base_url('batchMaster/studentNotice'));
        }


        $student_notice_list['studentNotice_filter_data'] = $this->session->userdata('studentNotice_filter_data');
        /* Pagination starts */
        $config['base_url'] = base_url('batchMaster/studentNotice/');
        $config['total_rows'] = $this->batchMaster_model->get_student_notice_count($student_notice_list['studentNotice_filter_data']);
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */
        $student_notice_list['notice_list'] = $this->batchMaster_model->get_student_notice_list($offset,$config['per_page'],$student_notice_list['studentNotice_filter_data']);

        $student_notice_list['pagination'] = $this->pagination->create_links();
        $this->load->admin_view('notice',$student_notice_list);
    }

        ///// End Student Notice ///////////////


        ///// Start To take Batch Request by Student ///////////////
      public function batchRequest($offset = '0'){
    		$batch_request_list['centres'] = $this->tele_enquiry_model->get_centres();

            if(!empty($_POST)){
                $this->session->set_userdata('batch_request_filter_data',$_POST);
                $offset=0;
            }
            if(isset($_POST['reset'])){
                $this->session->unset_userdata('batch_request_filter_data');
                redirect(base_url('batchMaster/batchRequest'));
            }

            if($this->input->post('TODATE') != '' && $this->input->post('FROMDATE') == '')
    		{
    			$this->form_validation->set_rules('FROMDATE','FROM DATE','required');
    		}

        if ($this->form_validation->run() == FALSE)
        {

        	// redirect(base_url('batchMaster/batchRequest'));
        }
        $batch_request_list['batch_request_filter_data'] = $this->session->userdata('batch_request_filter_data');
        /* Pagination starts */
        $config['base_url'] = base_url('batchMaster/batchRequest/');
        $config['total_rows'] = $this->batchMaster_model->get_batch_request_count($batch_request_list['batch_request_filter_data']);
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */
        $batch_request_list['batchRequest'] = $this->batchMaster_model->get_batch_request_list($offset,$config['per_page'],$batch_request_list['batch_request_filter_data']);

        $batch_request_list['pagination'] = $this->pagination->create_links();
        $this->load->admin_view('BatchRequest',$batch_request_list);
    }

        ///// End To take Batch Request by Student ///////////////

public function get_empname()
{
	$empId = $this->input->post('psId');
	$empName = $this->batchMaster_model->get_empname($empId);
	echo json_encode($empName);
}

public function get_duration()
{
	$sId = $this->input->post('sId');
	$d = $this->batchMaster_model->get_duration($sId);
	echo json_encode($d);
}

public function delSelectedStudents()
{
	$csrf = array(
        'name' => $this->security->get_csrf_token_name(),
        'hash' => $this->security->get_csrf_hash()
);
	$Batch_Std_ID = $this->input->post('data_id');
	$Batch_Std_ID = $this->batchMaster_model->del_student($Batch_Std_ID);
	echo json_encode($Batch_Std_ID);
}

public function addSelectedStudentsToBatch()
{
	if($_POST)
	{
		$CREATED_BY= $this->session->userdata('admin_data');
		$CREATED_BY = $CREATED_BY[0]['EMPLOYEE_ID'];

		$CREATED_DATE=date("Y-m-d H:i:s");

    for ($i = 0; $i < count($this->input->post('Admission_ID')); $i++) {
        $insertdata = array(
            'BatchID' => $this->input->post('batch_id'),
            'Admission_ID' => $this->input->post('Admission_ID')[$i],
            'CREATED_BY' => $CREATED_BY,
            'CREATED_DATE' => $CREATED_DATE,
            'ModifiedDate'  =>$CREATED_DATE,
            'ModifiedBy'  => $CREATED_BY
        );

    $data = $this->batchMaster_model->addSelectedStudentsToBatch($insertdata);
    }
	}
	redirect(base_url('BatchMaster/batchsearch'));
}


/*start code for Add Students to batch */
public function addStudentsToBatch($batchId,$offset=0)
{

	if(!empty($_POST)){
        $this->session->set_userdata('addStudentsToBatchFilter_data',$_POST);
        $offset=0;
    }

    if(isset($_POST['reset'])){
        $this->session->unset_userdata('addStudentsToBatchFilter_data');
        redirect(base_url('BatchMaster/batchsearch'));
    }
    $batch['addStudentsToBatchFilter_data'] = $this->session->userdata('addStudentsToBatchFilter_data');

	$batchId = $this->encrypt->decode($batchId);

	$existingStudentsInBatchArr = $this->batchMaster_model->getAdmittedBatchId($batchId);
	$existingStudentsInBatchStr = '';
	for ($i=0; $i <count($existingStudentsInBatchArr) ; $i++) {

		$existingStudentsInBatchStr .= $existingStudentsInBatchArr[$i]['ADMISSION_ID'].',';
	}

	$existingStudentsInBatchTrim = rtrim($existingStudentsInBatchStr,",");
	$existingStudentsInBatch = explode(",", $existingStudentsInBatchTrim);

  $batch['courses'] = $this->tele_enquiry_model->all_courses();
  $batch['modules'] = $this->tele_enquiry_model->all_modules();
	$batch['batch_details'] = $this->batchMaster_model->batch_detail($batchId);
	$batch['existingStudentsInBatch'] = $this->batchMaster_model->existingStudentsInBatch($batchId);

	$batch['centres'] = $this->tele_enquiry_model->get_centres();

	$batchId = $this->session->set_userdata('batchId',$batchId);

	$batchId = $this->session->userdata;

	$courseId = $batch['batch_details'][0]['COURSE_ID'];
	$offset=0;

        /* Pagination starts */
        $config['base_url'] = base_url('BatchMaster/addStudentsToBatch/');
        $config['total_rows'] = $this->batchMaster_model->get_student_count($batchId,$courseId,$existingStudentsInBatch,$batch['addStudentsToBatchFilter_data']);

        $this->pagination->initialize($config);
        /* Pagination Ends */

        // $courses = $this->tele_enquiry_model->course_interested();
        // $modules = $this->tele_enquiry_model->module_interested();
        $batch['students'] = $this->batchMaster_model->get_student_list($batchId,$courseId,$offset,'10',$existingStudentsInBatch,$batch['addStudentsToBatchFilter_data']);
        for($i=0;$i<count($batch['students']);$i++){
          $batch['students'][$i]['FEES_PAID'] = $this->admission_model->getFeesPaidByByAdmissionId($batch['students'][$i]['ADMISSION_ID']);
          $batch_details = $this->batchMaster_model->getStudentBatchDetails($batch['students'][$i]['ADMISSION_ID']);

          $course_completed = "";
          $course_persuing = "";
          foreach($batch_details as $batchdetail){
            if(($batchdetail['BATCHCOMPLETED'] == "1") && ($batchdetail['BATCH_STATUS'] == "C")){
              $course_completed .= $batchdetail['COURSE_NAME'].", ";
            }
            else{
              $course_persuing .= $batchdetail['COURSE_NAME'].", ";
            }
          }

          $batch['students'][$i]['COURSE_COMPLETED'] = trim($course_completed,", ");
          $batch['students'][$i]['COURSE_PERSUING'] = trim($course_persuing,", ");

          // $batch['students'][$i]['COURSE_COMPLETED'] = $this->batchMaster_model->getCourseStatusCompletedOrPersuing($batch['students'][$i]['ADMISSION_ID'],"complete");
          // $batch['students'][$i]['COURSE_PERSUING'] = $this->batchMaster_model->getCourseStatusCompletedOrPersuing($batch['students'][$i]['ADMISSION_ID'],"persuing");
          if($batch['students'][$i]['FEES_PAID'] == ""){
            $batch['students'][$i]['FEES_PAID']  = 0;
          }
        }

	$this->load->admin_view('addStudentsToBatch',$batch);
}
/*End code of Add Students To Batch*/


public function assignment_upload($offset='0')
{
    $assignment_list['courses'] = $this->tele_enquiry_model->course_interested();

		if(!empty($_POST))
	    {

			$this->form_validation->set_rules('SUBJECT', 'SUBJECT', 'required');
			$this->form_validation->set_rules('ASSIGNMENT_NO', 'ASSIGNMENT NO', 'required');
			$this->form_validation->set_rules('LECTURE_FROM', 'LECTURE FROM', 'required');
			$this->form_validation->set_rules('LECTURE_TO', 'LECTURE TO', 'required');

			if($this->form_validation->run() == TRUE)
			{

			if(!empty($_FILES['assignment']))
			{
				$path = $config['upload_path']   = 'uploads/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['remove_spaces']  = true;

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('assignment'))
				{
					 $error = array('error' => $this->upload->display_errors());
					// upload_form error

					$this->session->set_flashdata('failed', 'Sorry !');
					// After that you need to used redirect function instead of load view such as
					redirect(base_url('BatchMaster/assignment-upload/'), $error);
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());
					$file = $data['upload_data']['file_name'];
        }
			}


			$MODIFIEDBY= $this->session->userdata('admin_data');
			$MODIFIEDBY = $MODIFIEDBY[0]['USER_ID'];

	 		$UPLOAD_DATE=date("Y-m-d H:i:s");

	        $insert  =  array(
			  'COURSE_ID' =>  $this->input->post('SUBJECT'),
			  'ASSIGNMENT_NO' =>  $this->input->post('ASSIGNMENT_NO'),
			  'LECTURE_FROM' => $this->input->post('LECTURE_FROM'),
			  'LECTURE_TO' => $this->input->post('LECTURE_TO'),
			  'MODIFIEDBY' => "$MODIFIEDBY",
			  'UPLOAD_DATE' => "$UPLOAD_DATE",
			  'FILE' => "$file"
			  );


			  $checkExists = $this->batchMaster_model->check_assignment_exists($insert);

			  if($checkExists>0)
			  {

				$assignmentId = $checkExists[0]['ASSIGNMENT_ID'];
				$path = "uploads/".$checkExists[0]['FILE'];
				unlink($path);

			  	$this->batchMaster_model->assignmentDelete($assignmentId);
			  }

			  $this->batchMaster_model->assignmentUpload($insert);
			  $this->session->set_flashdata('success', 'Batch Save Successfully');
	          redirect(base_url('BatchMaster/assignment-upload'));
	    	}
	    	else
	    	{
		    	$this->session->set_flashdata('warning', 'Please fill valid data.');
		    	redirect(base_url('BatchMaster/assignment-upload'));
	    	}

    	}

		if(!empty($_POST)){
            $this->session->set_userdata('assignment_filter_data',$_POST);
            $offset=0;
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('assignment_filter_data');
        }
        $assignment_list['assignment_filter_data'] = $this->session->userdata('assignment_filter_data');
        /* Pagination starts */
        $config['base_url'] = base_url('BatchMaster/assignment-upload/');
        $config['total_rows'] = $this->batchMaster_model->get_assignment_count($assignment_list['assignment_filter_data']);
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */
        $assignment_list['assignments'] = $this->batchMaster_model->get_assignment_list($offset,$config['per_page'],$assignment_list['assignment_filter_data']);

        $assignment_list['pagination'] = $this->pagination->create_links();

    	$this->load->admin_view('assignmentUpload',$assignment_list);
	}


/*start code for batch status isactive = 0, It will not permanently delete*/
public function batchDelete($batchId)
{
	$batchId = $this->encrypt->decode($batchId);
	// print_r($MODIFIEDBY= $this->session->userdata('admin_data'));
	// $date=date("Y-m-d H:i:s");

	           $update  =  array(
			  // 'ISACTIVE' => '0',
			  // 'MODIFIEDBY' => "$MODIFIEDBY",
			  'ISACTIVE' => '0'
			  );

		$this->batchMaster_model->batchDelete($batchId,$update);
		$this->session->set_flashdata('danger', 'Batch deleted successfully.');
		// $this->session->set_flashdata('info', "Batch status inactive, I's not deleted  permanently  from database.");
		redirect(base_url('BatchMaster/batchsearch'));
}
/*End code of Batch delete*/

// code for report genration ///////////////////////////////

public function batch_report()
{

	$batch_list['batch_filter_data'] = $this->session->userdata('batch_filter_data');
	$batch = $this->batchMaster_model->get_export_batch_list($batch_list['batch_filter_data']);
	/*print"<pre>";
	print_r($batch);*/


        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);

        $col = 'A';
        for($i=0;$i<count($batch['fields']);$i++){
            $objPHPExcel->getActiveSheet()->SetCellValue($col.'1', $batch['fields'][$i]);
            $objPHPExcel->getActiveSheet()->getStyle("A1:X1")->getFont()->setBold(true);
            $col++;
        }


        $j = 2;
        $alphebetCol = 'A';
        foreach ($batch['details'] as $key => $value) {

            foreach ($batch['details'][$key] as $index => $data) {
                $objPHPExcel->getActiveSheet()->SetCellValue($alphebetCol.$j,$batch['details'][$key][$index]);

                $alphebetCol++;
                if($alphebetCol == "AB"){
                    $alphebetCol = 'A';
                }
            }

            $j++;
        }


//die;
        $filename = "Batch_Details-" . date("h.i.s.d-m-Y") . ".xls";
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=" . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
}


///////////// start batch search code //////////////

public function batchsearch($offset = '0'){
	$batch_list['centres'] = $this->tele_enquiry_model->get_centres();
    $batch_list['courses'] = $this->batchMaster_model->course_interested();

        if(!empty($_POST)){
            $this->session->set_userdata('batch_filter_data',$_POST);
            $offset=0;
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('batch_filter_data');
        }
        $batch_list['batch_filter_data'] = $this->session->userdata('batch_filter_data');
        /* Pagination starts */
        $config['base_url'] = base_url('BatchMaster/batchsearch/');
        $config['total_rows'] = $this->batchMaster_model->get_batch_count($batch_list['batch_filter_data']);
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */
        $batch_list['batches'] = $this->batchMaster_model->get_batch_list($offset,$config['per_page'],$batch_list['batch_filter_data']);
        $BATCHID="";
        $studentBatch='';

        for($i=0; $i<count($batch_list['batches']); $i++)
        {
          $students_in_batch = $this->batchMaster_model->existingStudentsInBatch($batch_list['batches'][$i]['BATCHID']);
          $batch_list['batches'][$i]['NOOFSTUDENTS'] = count($students_in_batch);

          $student_names = "";
          foreach($students_in_batch as $batch){
            $student_names .= '<li>'.$batch['STUDENT_NAME'].'</li>';
          }
          $batch_list['batches'][$i]['STUDENTS'] = trim($student_names," ");
        }

        $batch_list['pagination'] = $this->pagination->create_links();
        $this->load->admin_view('BatchSearch',$batch_list);
    }

    public function export_batch_search(){
        if(!empty($_POST)){
            $this->session->set_userdata('batch_filter_data',$_POST);
        }
        $batch_list['batch_filter_data'] = $this->session->userdata('batch_filter_data');

        $batch_list = $this->batchMaster_model->get_export_batch_list($batch_list['batch_filter_data']);

	    $batch_list['fields'][27] = 'STUDENTINBATCH';

        $BATCHID="";
        $studentBatch='';
       for($i=0; $i<count($batch_list['details']); $i++)
       {

	      $studentInBatch = $this->batchMaster_model->getStudentsInBatch($batch_list['details'][$i]['BATCHID']);

	      $batch_list['details'][$i]['STUDENTINBATCH'] = count($studentInBatch);
        }

	    $this->load->library('excel');
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);

        $col = 'A';
        $j = 1;
        $alphebetCol = $this->mkRange('A','ZZ');
        $k = 0;

        for($i=0;$i<count($batch_list['fields']);$i++){
            $objPHPExcel->getActiveSheet()->SetCellValue($col.'1', $batch_list['fields'][$i]);
            $objPHPExcel->getActiveSheet()->getStyle("A1:AB1")->getFont()->setBold(true);

            $col++;
            $rowNum = $j+1;

            for ($k=0; $k <count($batch_list['details']) ; $k++) {
                $objPHPExcel->getActiveSheet()->SetCellValue($alphebetCol[$i].$rowNum,$batch_list['details'][$k][$batch_list['fields'][$i]]);
            	$rowNum++;
            }
        }

        $filename = "batchsearch-" . date("h.i.s.d-m-Y") . ".xls";
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=" . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $objWriter->save('php://output');
    }

    public function mkRange($start,$end) {
        $count = $this->strToInt($end) - $this->strToInt($start);
        $r = array();
        do {$r[] = $start++;} while ($count--);
        return $r;
    }

    public function strToInt($str) {
        $str = strrev($str);
        $dec = 0;
        for ($i = 0; $i < strlen($str); $i++) {
            $dec += (base_convert($str[$i],36,10)-9) * pow(26,$i);
        }
        return $dec;
    }

    ////////////////// end batch search code ///////

//// start update batch code //////////////////
public function batchUpdation($batchId)
    {

    	$batchId = $this->encrypt->decode($batchId);
    	// $batchId = $this->db->input->post('BATCHID');
    	$data['batch'] = $this->batchMaster_model->get_batch_details($batchId);

    	$FACULTY = $data['batch'][0]['FACULTY'];
    	$data['faculties'] = $this->batchMaster_model->get_emp($FACULTY);

    	$data['centres'] = $this->tele_enquiry_model->get_centres();
    	$data['courses'] = $this->tele_enquiry_model->course_interested();


    	if(!empty($_POST))
    	{
    			// for getting user data from session
          $MODIFIEDBY= $this->session->userdata('admin_data');
    			$update['MODIFIED_BY'] = $MODIFIEDBY[0]['USER_ID'];

    			if(isset($_POST['DAYS']))
    			{
    				foreach($_POST['DAYS'] as $check)
    				{
    					$update['DAYS'] = implode('/', $_POST['DAYS']);
    				}
    			}

          if(!empty($this->input->post('STARTDATE'))){
    			  $STARTDATE = str_replace('/', '-', $this->input->post('STARTDATE'));
    		    $update['STARTDATE'] =  date("Y-m-d", strtotime($STARTDATE));
          }
          if(!empty($this->input->post('EXPECTEDENDDATE'))){
    		    $EXPECTEDENDDATE = str_replace('/', '-', $this->input->post('EXPECTEDENDDATE'));
    		    $update['EXPECTEDENDDATE'] =  date("Y-m-d", strtotime($EXPECTEDENDDATE));
          }
          if(!empty($this->input->post('INTERNALEXAMDATE'))){
    			  $INTERNALEXAMDATE = str_replace('/', '-', $this->input->post('INTERNALEXAMDATE'));
    		    $update['INTERNALEXAMDATE'] =  date("Y-m-d", strtotime($INTERNALEXAMDATE));
          }
  			  // $BATCHID=base64_encode($_POST['BATCHID']);


  	 		   $date = date("Y-m-d H:i:s");
           $update['MODIFIED_DATE'] = $date;
           $update['BATCH_STATUS'] = "P";

           if(!empty($this->input->post('CENTRE_ID'))){
             $update['CENTRE_ID'] = $this->input->post('CENTRE_ID');
           }
           if(!empty($this->input->post('SUBJECT'))){
             $update['SUBJECT'] = $this->input->post('SUBJECT');
           }
           if(!empty($this->input->post('FACULTY'))){
             $update['FACULTY'] = $this->input->post('FACULTY');
           }
           if(!empty($this->input->post('FACULTY_NAME'))){
             $update['FACULTY_NAME'] = $this->input->post('FACULTY_NAME');
           }
           if(!empty($this->input->post('STARTTIME'))){
             $update['STARTTIME'] = $this->input->post('STARTTIME');
           }
           if(!empty($this->input->post('ENDTIME'))){
             $update['ENDTIME'] = $this->input->post('ENDTIME');
           }
           if(!empty($this->input->post('ATTENDANCESHEETNO'))){
             $update['ATTENDANCESHEETNO'] = $this->input->post('ATTENDANCESHEETNO');
           }
           if(!empty($this->input->post('SESSION'))){
             $update['SESSION'] = $this->input->post('SESSION');
           }
           if(!empty($this->input->post('BATCH_STATUS_REMARK'))){
             $update['BATCH_STATUS_REMARK'] = $this->input->post('BATCH_STATUS_REMARK');
           }
          //
          //
          // echo "<pre>";
          // print_r($update);
          // echo "</pre>";
          // exit();
		      $status = $this->batchMaster_model->BatchUpdation($batchId,$update);
          if($status){
            $this->session->set_flashdata('success', 'Batch Updated Successfully');
            redirect(base_url('BatchMaster/batchsearch'));
          }
	    }
    	$this->load->admin_view('BatchUpdation',$data);
	}

	////// end update batch code ///////////////////////////////

public function batchcreation(){
    	$data['centres'] = $this->tele_enquiry_model->get_centres();
    	$data['courses'] = $this->tele_enquiry_model->course_interested();

		if(!empty($_POST))
	    {

			$this->form_validation->set_rules('CENTRE_ID', 'CENTRE ', 'required');
			$this->form_validation->set_rules('SUBJECT', 'SUBJECT', 'required');
			$this->form_validation->set_rules('STARTDATE', 'STARTDATE', 'required');
			$this->form_validation->set_rules('EMPLOYEE_ID', 'EMPLOYEE', 'required');
			// $this->form_validation->set_rules('FACULTY_NAME', 'FACULTY NAME', 'required');

			$this->form_validation->set_rules('EXPECTEDENDDATE', 'EXPECTEDENDDATE', 'required');
			$this->form_validation->set_rules('STARTTIME', 'STARTTIME', 'required');
			$this->form_validation->set_rules('ENDTIME', 'ENDTIME', 'required');
			$this->form_validation->set_rules('INTERNALEXAMDATE', 'INTERNALEXAMDATE', 'required');

			$this->form_validation->set_rules('ATTENDANCESHEETNO', 'ATTENDANCESHEETNO', 'required');
			// $this->form_validation->set_rules('SESSION', 'SESSION', 'required');
			// $this->form_validation->set_rules('BATCH_STATUS_REMARK', 'BATCH_STATUS_REMARK', 'required');

			if($this->form_validation->run() == TRUE)
			{

			$MODIFIEDBY= $this->session->userdata('admin_data');
			$MODIFIEDBY = $MODIFIEDBY[0]['USER_ID'];

			if(isset($_POST['DAYS']))
			{
				foreach($_POST['DAYS'] as $check)
				{
					$check = implode('/', $_POST['DAYS']);
				}
			}
			else
			{
				$check = '';
			}

			$STARTDATE = str_replace('/', '-', $this->input->post('STARTDATE'));
		    $STARTDATE =  date("Y-m-d", strtotime($STARTDATE));

		    $EXPECTEDENDDATE = str_replace('/', '-', $this->input->post('EXPECTEDENDDATE'));
		    $EXPECTEDENDDATE =  date("Y-m-d", strtotime($EXPECTEDENDDATE));

			$INTERNALEXAMDATE = str_replace('/', '-', $this->input->post('INTERNALEXAMDATE'));
		    $INTERNALEXAMDATE =  date("Y-m-d", strtotime($INTERNALEXAMDATE));

		    $admin_cred = $this->session->userdata('admin_data');	// for getting user data from session


			  // $BATCHID=base64_encode($_POST['BATCHID']);


	 		  $date=date("Y-m-d H:i:s");

        if($this->input->post('EMPLOYEE_ID') == "other"){
          $faculty_id=NULL;
        }
        else{
          $faculty_id=$this->input->post('EMPLOYEE_ID');
        }

         $insert  =  array(
			  'CENTRE_ID' =>  $this->input->post('CENTRE_ID'),
			  'SUBJECT' =>  $this->input->post('SUBJECT'),
			  'FACULTY' =>  $faculty_id,
			  'FACULTY_NAME' =>  $this->input->post('FACULTY_NAME'),
			  'STARTDATE' =>  "$STARTDATE",
			  'EXPECTEDENDDATE' => "$EXPECTEDENDDATE",
			  'DAYS' => "$check",
			  'STARTTIME' => $this->input->post('STARTTIME'),
			  'ENDTIME' => $this->input->post('ENDTIME'),
			  'INTERNALEXAMDATE' => "$INTERNALEXAMDATE",
			  'BATCHCOMPLETED' => "0",
			  'ATTENDANCESHEETNO' =>  $this->input->post('ATTENDANCESHEETNO'),
			  'SESSION' =>  $this->input->post('SESSION'),
			  'BATCH_STATUS' => "P",
			  'BATCH_STATUS_REMARK' =>  $this->input->post('BATCH_STATUS_REMARK'),
			  'IS_APPROVED_BY_TC' => '',
			  'APPROVED_BY' => '',
			  'CREATED_BY' => $admin_cred[0]['EMPLOYEE_ID'],
			  'CREATED_DATE' => date('Y-m-d'),
			  'MODIFIED_BY' => "$MODIFIEDBY",
			  'MODIFIED_DATE' => "$date"
			  );
			  $this->batchMaster_model->BatchCreation($insert);
	          $id = $this->db->insert_id();
	          // $this->session->set_flashdata('msg',array('success' => 'Batch Save Successfully'));
	          $this->session->set_flashdata('success', "Batch saved successfully, And Batch Id : $id");
	          redirect(base_url('BatchMaster/batchsearch'));
	    	}
	    	else
	    	{
		    	$this->session->set_flashdata('warning', 'Please fill valid data.');
		    	redirect(base_url('batchMaster/batchcreation'));
	    	}
		}
    	$this->load->admin_view('BatchCreation',$data);
	}

	public function debug_to_console($data)
	{
	    if(is_array($data) || is_object($data))
		{
			echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
		}
		else
		{
			echo("<script>console.log('PHP: ".$data."');</script>");
		}
	}

public function saveAttendance(){
	$batch_id=$this->input->post('batch_id');
	$createdBy = $this->session->userdata('admin_data')[0]['USER_ID'];
	$createDate=date("Y-m-d H:i:s");
	$data=$this->input->post('admissionDetails');
	foreach($data as $admissionDetail)
	{
		$LECTURE_DATE = str_replace('/','-',$admissionDetail['lectureDate']);
    	$LECTURE_DATE = date("Y-m-d",strtotime($LECTURE_DATE));

    	$batch_lecture = array(
    	'BATCH_ID' => $batch_id,
    	'LECTURE_DATE' => $LECTURE_DATE,
    	'LECTURE_NO_AS_PER_FRM' => $admissionDetail['lectureNumber'],
    	'START_TIME' => $this->input->post('start_time'),
    	'END_TIME' => $this->input->post('end_time'),
    	'MODIFIED_BY' => $createdBy,
    	'MODIFIED_DATE' => $createDate,
    	'IS_SMS_SENT' =>'0'
    	);
	 	$BATCH_LECTURE_ID =$this->batchMaster_model->saveBatchLectureDetails($batch_lecture);
    	foreach ($admissionDetail['admissionAttendance'] as $admissionID) {
    	  	$batch_attendance = array(
	       		'BATCH_LECTURE_ID' => $BATCH_LECTURE_ID,
		      	'MODIFFIED_BY' => $createdBy,
		      	'MODIFIED_DATE' => $createDate,
		      	'ADMISSION_ID' 	=> $admissionID['admission'],
		      	'PRESENT'		=> $admissionID['attendance']
	      	);
	      	$this->batchMaster_model->saveBatchAttendanceDetails($batch_attendance);
  		}
    }
   redirect(base_url('batchMaster/batchsearch'));
}

  public function issueAttendenceSheet($batch_id){
    $batch['batch_details'] = $this->batchMaster_model->batch_detail($this->encrypt->decode($batch_id));
  	$batch['existingStudentsInBatch'] = $this->batchMaster_model->existingStudentsInBatch($this->encrypt->decode($batch_id));
  	$this->load->view('attendencesheet',$batch);
  }

  public function upload_assignments_and_projects($batch_id){
    $batch['batch_details'] = $this->batchMaster_model->batch_detail($this->encrypt->decode($batch_id));
  	$batch['existingStudentsInBatch'] = $this->batchMaster_model->existingStudentsInBatch($this->encrypt->decode($batch_id));
    $batch['courses'] = $this->tele_enquiry_model->all_courses();
    $batch['modules'] = $this->tele_enquiry_model->all_modules();
    if(isset($_POST['assign_assignment_or_project']))
    {
      $this->form_validation->set_rules('ASSIGNMENT_TOPIC','Assignments Topic','required|trim');
      $this->form_validation->set_rules('ASSIGNMENT_DESCRIPTION','Assignments Description','required|trim');
      $this->form_validation->set_rules('ASSIGNMENT_SUBMISSION_LAST_DATE','Assignment Submission Last Date','required|trim');
      $this->form_validation->set_rules('TYPE','Type','required');
      if($this->form_validation->run() == TRUE)
      {
        unset($_POST['assign_assignment_or_project']);
        $_POST['ISACTIVE'] = "1";
        $_POST['CREATED_BY'] = $this->session->userdata('admin_data')[0]['USER_ID'];
        $_POST['ASSIGNMENT_DATE'] = date("Y-m-d H:i:s");
        $_POST['CREATED_DATE'] = date("Y-m-d H:i:s");
        $last_date = str_replace('/','-',$_POST['ASSIGNMENT_SUBMISSION_LAST_DATE']);
        $_POST['ASSIGNMENT_SUBMISSION_LAST_DATE'] = date("Y-m-d",strtotime($last_date));

        if(!empty($_FILES['attached_file'])){
            $batch_id = $_POST['BATCH_ID'];
            $config['upload_path']   = 'resources/Reference_Assignment_File/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|zip|rar';
            $config['remove_spaces']  = true;
            $config['file_name'] = $batch_id."_assignment_".$batch_id."_".time();
            $config['file_ext'] = pathinfo($_FILES["attached_file"]["name"],PATHINFO_EXTENSION);

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('attached_file'))
            {
               $error = array('error' => $this->upload->display_errors());
               echo "<script>alert('Check extension of your file or try to upload assignments after some time.');</script>";
            }
            else
            {
              $_POST['ATTACHMENTS_FOR_REFERENCE'] = $config['file_name'] .".". $config['file_ext'];
              $data = array('upload_data' => $this->upload->data());
              $confirmation = $this->batchMaster_model->uploadAssignmentsOrProjects($_POST);
              if($confirmation){
                echo "<script>alert('Assignment / Project with attachemnts assigned to batch.');</script>";
              }
            }
        }
        else{
          $confirmation = $this->batchMaster_model->uploadAssignmentsOrProjects($_POST);
          if($confirmation){
            echo "<script>alert('Assignment / Project assigned to batch.');</script>";
          }
        }
      }
      else{
        echo "<script>alert('Please fill all required fields.');</script>";
      }
    }
  	$this->load->admin_view('upload_assignments_and_projects',$batch);
  }

  public function link_assignment_or_projects_attachments($file_name){
     redirect(base_url('resources/Reference_Assignment_File/'.$file_name));
  }

  public function search_student_exam_details(){
    $data['centres'] = $this->tele_enquiry_model->get_centres();
    $data['exam_details'] = array();
    if(isset($_POST['search_exam_details'])){
      unset($_POST['search_exam_details']);
      $data['exam_details'] = $this->batchMaster_model->get_student_exam_details($_POST);
    }
    $this->load->admin_view('search_exam_details',$data);
  }

  public function upload_weekly_test($batchId)
  {
  	$batchId = $this->encrypt->decode($batchId);
  	$attendance['batch_details'] = $this->batchMaster_model->batch_detail($batchId);
  	$attendance['student_details'] = $this->batchMaster_model->getBatchWeeklyTestMarks($batchId);
    $attendance['exam_dates'] = array();
    if(!(empty($attendance['student_details'][0][0]))){
      for($i=0;$i<count($attendance['student_details'][0][0]);$i++){
        array_push($attendance['exam_dates'],$attendance['student_details'][0][0][$i]['INTERNAL_EXAM_DATE']);
      }
    }

    if(isset($_POST['submit_marks'])){
      unset($_POST['submit_marks']);

      $batch_id = $_POST['batch_id'];
      $count_dates = count($_POST['test_dates']);
      $test_dates = $_POST['test_dates'];
      $admission_ids = $_POST['weekly_test_admission_ids'];
      $test_marks_for_upload = array();
      for($i=0;$i<$count_dates;$i++){
        $marks_details = array();
        for($j=0;$j<count($admission_ids);$j++){
          $recent_date = $test_dates[$i];
          $marks = $_POST['test_marks_'.$i];
          if($recent_date != ""){
            array_push($marks_details,array('admission_id'=>$admission_ids[$j],'marks'=>$marks[$j]));
          }
          $test_marks_for_upload[$recent_date] =$marks_details;
        }

      }

      $weekly_test_marks = "";
      foreach($test_marks_for_upload as $key => $value){
          $i_date = str_replace('/','-',$key);
          $internal_date = date("Y-m-d",strtotime($i_date));
          $modified_by = $this->session->userdata('admin_data')[0]['USER_ID'];

          $batch_internal_dates = array(
          'BATCH_ID' => $batch_id,
          'INTERNAL_EXAM_DATE' => $internal_date,
          'MODIFIED_DATE' => date("Y-m-d"),
          'MODIFIED_BY' => $modified_by
          );

          $internal_batch_id = $this->batchMaster_model->saveWeeklyTestDate($batch_internal_dates);
          foreach($test_marks_for_upload[$key] as $i => $val){
            $batch_internal_details = array(
            'BTCH_INTERNAL_ID' => $internal_batch_id,
            'ADM_ID' => $val['admission_id'],
            'MARKS_OBTAINED' => $val['marks'],
            'MODIFIED_DATE' => date("Y-m-d"),
            'MODIFIED_BY' => $modified_by
            );
            $weekly_test_marks = $this->batchMaster_model->saveWeeklyTestMarks($batch_internal_details);
          }
      }
      if($weekly_test_marks){
        $this->session->set_flashdata('success', 'Marks Uploaded Successfully.');
        redirect(base_url('BatchMaster/batchsearch'));
      }
    }
  	$this->load->admin_view('weekly_test',$attendance);
  }

}
?>
