<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BatchMaster_model extends CI_Model {

  public $sql;
  public function __construct() {
      parent::__construct();
  }

public function saveBatchCompletion($insert)
{
  $this->db->insert('batch_internals_details',$insert);
  if($this->db->affected_rows() > 0){
    return true;
  }
  else{
    return false;
  }
}
public function isExistsBachCompletion($batchId)
{
  $query = $this->db->select('BTCH_INTERNAL_ID')
           ->from('batch_internals_details')
           ->where('BTCH_INTERNAL_ID',$batchId)
           ->get();
  return $query->num_rows();
}
public function totalPresent($batchId)
{
  $query = $this->db->query("SELECT BAD.ADMISSION_ID,SUM(BAD.PRESENT) as totalPresent FROM  batch_attendance_details BAD JOIN  batch_lecture_details BLD ON BLD.BATCH_LECTURE_ID = BAD.BATCH_LECTURE_ID WHERE BLD.BATCH_ID='$batchId'  GROUP BY BAD.ADMISSION_ID");
  return $query->result_array();
}
public function AttendanceMarksOutOf10($batchId)
{
  $query = $this->db->query("SELECT count(BLD.BATCH_LECTURE_ID) as BATCH_LECTURE_ID FROM batch_lecture_details BLD WHERE BLD.BATCH_ID='$batchId'");
  return $query->result_array();
}
public function attendanceSheet($batchId)
{
  $query = $this->db->select("BLD.BATCH_ID, BLD.LECTURE_DATE, BLD.LECTURE_NO_AS_PER_FRM,
                    BAD.ADMISSION_ID,
                    BAD.PRESENT, BLD.BATCH_LECTURE_ID,CONCAT_WS(' ', ENQM.ENQUIRY_FIRSTNAME, ENQM.ENQUIRY_MIDDLENAME, ENQM.ENQUIRY_LASTNAME) AS STUDENT_NAME")
                    ->from('batch_attendance_details BAD')
                    ->distinct('BAD.ADMISSION_ID')
                    ->join('admission_master AM','BAD.ADMISSION_ID=AM.ADMISSION_ID','left')
                    ->join('enquiry_master ENQM','AM.ENQUIRY_ID=ENQM.ENQUIRY_ID','left')
                    ->join('batch_lecture_details BLD','BLD.BATCH_LECTURE_ID = BAD.BATCH_LECTURE_ID','inner')
                    ->where('BLD.BATCH_ID',"$batchId")
                    ->order_by('BLD.LECTURE_DATE','ASC');
    $query = $this->db->get();
    // echo $this->db->last_query();
    return $query->result_array();

}

// function for save Batch Lecture Details by laxmi
  public function saveBatchLectureDetails($batch_lecture)
    {
      $this->db->insert('batch_lecture_details',$batch_lecture);
        return $this->db->insert_id();
    }

// function for save Attendance details by laxmi
   public function saveBatchAttendanceDetails($batch_attendance)
     {
    $this->db->insert('batch_attendance_details',$batch_attendance);
     }
//by laxmi
 public function batchIns($batchIns){
      $this->db->insert('batch_master_std_trans',$batchIns);     //  for mysql
        return $this->db->insert_id();
    }

    public function getBatchMast($batchId){
        $query = $this->db->select('BT.BatchID,BT.Admission_ID,BT.MODIFIED_BY,BT.MODIFIED_DATE,CONCAT_WS(" ", EM.ENQUIRY_FIRSTNAME, EM.ENQUIRY_MIDDLENAME, EM.ENQUIRY_LASTNAME) AS STUDENT_NAME,EM.ENQUIRY_MOBILE_NO as MOBILE_NO')
                          ->from('batch_master_std_trans BT')
                          ->join('admission_master AM','AM.ADMISSION_ID=BT.Admission_ID','right')
                          ->join('enquiry_master EM','EM.ENQUIRY_ID=AM.ENQUIRY_ID','right')
                          ->where('BT.BatchID',$batchId)
                          ->get();
        return $query->result_array();
    }

public function inheritBatchStudent($data)
{

//           $batchId = $this->admission_model->batchIns($batchIns);

//         $getBatchMast = $this->admission_model->getBatchMast($batchId);


  $insert_query = "INSERT INTO batch_master_std_trans (BatchID,Admission_ID,MODIFIED_BY,MODIFIED_DATE) SELECT '$data[newBatchId]',Admission_ID,'$data[MODIFIED_BY]','$data[MODIFIED_DATE]' FROM batch_master_std_trans WHERE BatchID='$data[BatchID]'";

  // $this->db->insert('batch_master_std_trans');
  // $this->db->set('BatchID,Admission_ID,ModifiedBy,ModifiedDate');
  // $this->db->select("$newBatchId,Admission_ID,$MODIFIEDBY,$date");
  // $this->db->from('batch_master_std_trans');
  // $this->db->where('BatchID',$batchId);
  // $query = $this->db->get();


$query = $this->db->query($insert_query);    //  for mysql
  // echo $this->db->last_query();

}
  public function get_facultyaudit_list($data)
{
    $this->db->select("audit_report_master.AUDIT_REPORT_MASTER_ID, audit_report_master.EMPLOYEE_ID,
  CONCAT(employee_master.EMP_FNAME,' ',employee_master.EMP_LASTNAME) AS EMPLOYEE_NAME,
  audit_report_master.STREAM, audit_report_master.AUDIT_DATE, audit_report_master.CENTRE_ID,
  centre_master.CENTRE_NAME, audit_report_master.BATCH_ID, audit_report_master.SUBJECT, audit_report_master.TECHNICAL_KNOWLEDGE,
  audit_report_master.PRESENTATION_SKILL_MODE_OF_TEACHING, audit_report_master.QUERY_SOLVING_ABILITY,
  audit_report_master.EXAMPLE_GIVEN_WHILE_TEACHING, audit_report_master.TEACHING_AID_USED,
  audit_report_master.STUDENT_PARTICIPATION, audit_report_master.BODY_LANGUAGE, audit_report_master.ACADEMIC_FEEDBACK,
  audit_report_master.BATCH_FILE_STATUS, audit_report_master.EMIS_UPDATION, audit_report_master.LATE_COMING_ABSENT,
  audit_report_master.FACULTY_TEACHING_PERCENT, audit_report_master.ACADEMIC_FEEDBACK_PERCENT,
  audit_report_master.EMIS_BATCH_FILE_PERCENT, audit_report_master.IS_AUDIT_CLOSE, audit_report_master.PUNCTUALITY")

    ->from('audit_report_master')
    ->join('employee_master', 'audit_report_master.EMPLOYEE_ID = employee_master.EMPLOYEE_ID')
    ->join('centre_master', 'audit_report_master.CENTRE_ID = centre_master.CENTRE_ID')
    ->where('centre_master.CENTRE_ID',$data['CENTRE_ID'])
    ->where('Audit_Month',$data['Audit_Month'])
    ->where('Audit_Year',$data['Audit_Year']);


    $query = $this->db->get();
    // echo $this->db->last_query();
    $result = $query->result_array();
    return $result;
}
 public function get_default_facultyaudit_list($data)
{
    $this->db->select("0 as AUDIT_REPORT_MASTER_ID,employee_master.EMPLOYEE_ID,CONCAT(employee_master.EMP_FNAME,' ',employee_master.EMP_LASTNAME) AS EMPLOYEE_NAME,employee_master.CENTRE_ID, centre_master.CENTRE_NAME")
    ->from('employee_master')
    ->join('centre_master', 'employee_master.CENTRE_ID = centre_master.CENTRE_ID')
    ->where('employee_master.ISACTIVE','1')
    ->where('employee_master.DEPARTMENT_ID','104')
    ->where('centre_master.CENTRE_ID',$data['CENTRE_ID'])
    ->order_by('centre_master.CENTRE_NAME','ASC');

    $query = $this->db->get();
    // echo $this->db->last_query();
    $result = $query->result_array();
    return $result;
}
public function facultyaudit_exists($data)
{
    $this->db->select('Audit_Month,Audit_Year,CENTRE_ID');
    $this->db->from('audit_report_master');
    $this->db->where('Audit_Month', $data['Audit_Month']);
    $this->db->where('Audit_Year', $data['Audit_Year']);
    $this->db->where('CENTRE_ID', $data['CENTRE_ID']);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}
    public function facultyaudit_save($data)
    {

      $this->db->insert('audit_report_master',$data);
    }

    public function facultyaudit_update($id,$data)
    {
        $this->db->where('AUDIT_REPORT_MASTER_ID', $id);
        $this->db->update('audit_report_master', $data);
    }


public function studentNoticeDelete($noticeId)
{
    $this->db->where('NOTICEID', $noticeId);
    $this->db->delete('student_notice');      // for mysql

}

public function saveStudentNotice($data)
    {
      $this->db->insert('student_notice',$data);    // for mysql

    }
    public function updateStudentNotice($data)
    {
      $this->db->where('NOTICEID', $data['NOTICEID']);
      $this->db->update('student_notice', $data);    // for mysql

    }

public function get_student_notice_count($studentNotice_filter_data)
{
   $query = $this->db->select("SN.NOTICEID")->from('student_notice SN');
   $query = $this->db->get();
   return $query->num_rows();
}
public function get_student_notice_list($offset,$perpage,$studentNotice_filter_data)
{

  $query = $this->db->select("SN.NOTICEID,SN.UNIVERSITY_ID,SN.NOTICEDISPLAY_DATE,SN.NOTICE_BODY, SN.CREATE_DATE, SN.NOTICE_TITLE")

   ->from('student_notice SN')
   ->order_by('CREATE_DATE','DESC');

        $query = $this->db->limit($perpage,$offset);
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
}



  public function get_batch_request_count($batch_request_filter_data)
    {


        $query = $this->db->select("BATCH_REQUIREMENT_ID")
                 ->from('batch_requirement')
                 ->join('admission_master','batch_requirement.ADMISSION_ID=admission_master.ADMISSION_ID','left')
                 ->join('centre_master','batch_requirement.CENTRE_NAME=centre_master.CENTRE_ID','left')
                 ->join('enquiry_master','admission_master.ENQUIRY_ID=enquiry_master.ENQUIRY_ID','left')
                 ->join('employee_master','batch_requirement.RESPONSEGIVENBY=employee_master.EMPLOYEE_ID','left');


        if(!empty($batch_request_filter_data))
        {


            if($batch_request_filter_data['FROMDATE'] != '' && $batch_request_filter_data['TODATE'] == '')
            {
              $FROMDATE = str_replace("/","-",$batch_request_filter_data['FROMDATE']);
              $FROMDATE = date('Y-m-d', strtotime($FROMDATE));


              $TODATE = date('Y-m-d');

               $query = $this->db->where('batch_requirement.MODIFIED_DATE > ',$FROMDATE)
                                 ->where('batch_requirement.MODIFIED_DATE < ',$TODATE);
            }

            if($batch_request_filter_data['FROMDATE'] != '' && $batch_request_filter_data['TODATE'] != '')
            {
              $date_format_change = str_replace("/","-",$batch_request_filter_data['FROMDATE']);
              $start_date = date('Y-m-d', strtotime($date_format_change));

              $to_date_format_change = str_replace("/","-",$batch_request_filter_data['TODATE']);
              $to_date = date('Y-m-d', strtotime($to_date_format_change));

               $query = $this->db->where('batch_requirement.MODIFIED_DATE > ',$start_date)
                                 ->where('batch_requirement.MODIFIED_DATE < ',$to_date);

            }


            if($batch_request_filter_data['CENTRE_ID'] != '')
            {

              $query = $this->db->where('batch_requirement.CENTRE_NAME',$batch_request_filter_data['CENTRE_ID']);
            }

            if($batch_request_filter_data['BATCH_STATUS'] != '')
            {

              $query = $this->db->where('BATCH_REQUIREMENT_STATUS',$batch_request_filter_data['BATCH_STATUS']);
            }


        }

        $query = $this->db->order_by('REQUESTDATE','DEASC');

        $query = $this->db->get();
        return $query->num_rows();
    }


  public function get_batch_request_list($offset,$perpage,$batch_request_filter_data)
    {
        $query = $this->db->select("BATCH_REQUIREMENT_ID,batch_requirement.ADMISSION_ID,
                                    batch_requirement.STUDENT_NAME AS `STUDENT NAME`,
                                    enquiry_master.ENQUIRY_MOBILE_NO AS `STUDENT CONTACT`,
                                    enquiry_master.ENQUIRY_EMAIL AS `STUDENT_EMAIL`,
                                    centre_master.CENTRE_NAME, batch_requirement.COURSE_TAKEN,
                                    CONCAT(PREFERRED_BAND_FROM1,' ','TO',' ',PREFERRED_BAND_TO1) AS `PREFERRED TIMING 1`,
                                    CONCAT(PREFERRED_BAND_FROM2,' ','TO',' ',PREFERRED_BAND_TO2) AS `PREFERRED TIMING 2`,
                                    PREFERRED_LOCATION1, PREFERRED_LOCATION2,
                                    BATCH_REQUIREMENT_STATUS,
                                    BATCH_REQUIREMENT_REMARKS,
                                    REQUESTDATE,
                                    RESPONSEDATE,
                                    CONCAT(employee_master.EMP_FNAME,' ',employee_master.EMP_LASTNAME) AS `APPROVED BY`,
                                    batch_requirement.SUBJECT")
                 ->from('batch_requirement')
                 ->join('admission_master','batch_requirement.ADMISSION_ID=admission_master.ADMISSION_ID','left')
                 ->join('centre_master','batch_requirement.CENTRE_NAME=centre_master.CENTRE_ID','left')
                 ->join('enquiry_master','admission_master.ENQUIRY_ID=enquiry_master.ENQUIRY_ID','left')
                 ->join('employee_master','batch_requirement.RESPONSEGIVENBY=employee_master.EMPLOYEE_ID','left');


        if(!empty($batch_request_filter_data))
        {

            if($batch_request_filter_data['FROMDATE'] != '' && $batch_request_filter_data['TODATE'] == '')
            {

              $FROMDATE = str_replace("/","-",$batch_request_filter_data['FROMDATE']);
              $FROMDATE = date('Y-m-d', strtotime($FROMDATE));


              $TODATE = date('Y-m-d');

               $query = $this->db->where('batch_requirement.MODIFIED_DATE > ',$FROMDATE)
                                 ->where('batch_requirement.MODIFIED_DATE < ',$TODATE);

            }

            if($batch_request_filter_data['FROMDATE'] != '' && $batch_request_filter_data['TODATE'] != '')
            {
              $date_format_change = str_replace("/","-",$batch_request_filter_data['FROMDATE']);
              $start_date = date('Y-m-d', strtotime($date_format_change));

              $to_date_format_change = str_replace("/","-",$batch_request_filter_data['TODATE']);
              $to_date = date('Y-m-d', strtotime($to_date_format_change));

               $query = $this->db->where('batch_requirement.MODIFIED_DATE > ',$start_date)
                                 ->where('batch_requirement.MODIFIED_DATE < ',$to_date);

            }

            if($batch_request_filter_data['CENTRE_ID'] != '')
            {

              $query = $this->db->where('batch_requirement.CENTRE_NAME',$batch_request_filter_data['CENTRE_ID']);
            }

            if($batch_request_filter_data['BATCH_STATUS'] != '')
            {

              $query = $this->db->where('BATCH_REQUIREMENT_STATUS',$batch_request_filter_data['BATCH_STATUS']);
            }


        }

        $query = $this->db->order_by('REQUESTDATE','DEASC');

        $query = $this->db->limit($perpage,$offset);
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }


public function addSelectedStudentsToBatch($data)
    {
      $this->db->insert('batch_master_std_trans',$data);    // for mysql
      $data['ModifiedBy'] = $data['CREATED_BY'];
      $data['ModifiedDate'] = $data['CREATED_DATE'];
      unset($data['CREATED_BY']);
      unset($data['CREATED_DATE']);

    }

  public function del_student($Batch_Std_ID)
  {
      foreach ($Batch_Std_ID as $value)
      {
        $this ->db -> where('Batch_Std_ID',$value)
                   -> delete('batch_master_std_trans');     // for mysql

        $this ->sql-> where('Batch_Std_ID',$value)
                   -> delete('batch_master_std_trans');     // for mssql
      }

  }

public function get_student_count($batchId,$courseId,$existingStudentsInBatch,$addStudentsToBatchFilter_data){

       $query = $this->db->select("AM.ADMISSION_ID")
                          ->from('admission_master AM')
                          ->join('enquiry_master ENQM','ENQM.ENQUIRY_ID = AM.ENQUIRY_ID','left')
                          ->join('centre_master CENM','AM.CENTRE_ID = CENM.CENTRE_ID','left')
                          ->join('admission_course_transaction ACT','AM.ADMISSION_ID = ACT.ADMISSION_ID','left')
                          ->where_not_in('AM.ADMISSION_ID',$existingStudentsInBatch);
             $query = $this->db->where('AM.STATUS','Hold');

            if($courseId != '')
            {
              $query = $this->db->where('ACT.COURSE_ID',$courseId);
            }

            $query = $this->db->order_by('AM.ADMISSION_ID','DESC');


        $query = $this->db->get();
        return $query->num_rows();
    }

  public function getAdmittedBatchId($batchId){
    $query = $this->db->select('ADMISSION_ID')
                      ->from('batch_master_std_trans')
                      ->where('BatchId',$batchId)
                      ->get();
    return $query->result_array();
  }

  public function getStudentsInBatch($batchId){

    $query = $this->db->select("BMST.Admission_ID,CONCAT_WS(' ', ENQM.ENQUIRY_FIRSTNAME, ENQM.ENQUIRY_MIDDLENAME, ENQM.ENQUIRY_LASTNAME) AS STUDENT_NAME")
                      ->from('batch_master_std_trans BMST')
                      ->join('enquiry_master ENQM','ENQM.ENQUIRY_ID = BMST.Admission_ID','right')
                      ->where('BMST.BatchId',$batchId)
                      ->get();
                      //echo $this->db->last_query();
    return $query->result_array();
  }

  public function get_student_list($batchId,$courseId,$offset,$perpage,$existingStudentsInBatch,$addStudentsToBatchFilter_data)
    {
       $query = $this->db->select("ACT.COURSE_ID as subject,AM.ADMISSION_ID,AM.ADMISSION_DATE,AM.COURSE_TAKEN AS COURSE,AM.MODULE_TAKEN AS MODULE,CONCAT_WS(' ', ENQM.ENQUIRY_FIRSTNAME, ENQM.ENQUIRY_MIDDLENAME, ENQM.ENQUIRY_LASTNAME) AS STUDENT_NAME,AM.TOTALFEES AS FEES,ENQM.ENQUIRY_MOBILE_NO AS MOBILE_NO,CENM.CENTRE_NAME,AM.COURSE_TAKEN")
                          ->from('admission_master AM')
                          ->join('enquiry_master ENQM','ENQM.ENQUIRY_ID = AM.ENQUIRY_ID','left')
                          ->join('centre_master CENM','AM.CENTRE_ID = CENM.CENTRE_ID','left')
                          ->join('admission_course_transaction ACT','AM.ADMISSION_ID = ACT.ADMISSION_ID','left')
                          ->where('AM.ISACTIVE','1')
                          ->where_not_in('AM.ADMISSION_ID',$existingStudentsInBatch);

        $where = "(AM.STATUS IS NULL OR AM.STATUS='Hold' OR AM.STATUS='Persuing' OR AM.STATUS='Break')";
        $query = $this->db->where($where);

        if($addStudentsToBatchFilter_data['FirstName'] != '')
        {
          $query = $this->db->like('ENQM.ENQUIRY_FIRSTNAME',$addStudentsToBatchFilter_data['FirstName'],'left');
        }

        if($addStudentsToBatchFilter_data['LastName'] != '')
        {
          $query = $this->db->like('ENQM.ENQUIRY_LASTNAME',$addStudentsToBatchFilter_data['LastName'],'left');
        }

        if($addStudentsToBatchFilter_data['DOA_from'] != '' && $addStudentsToBatchFilter_data['DOA_to'] != '')
        {
          $DOA_from = str_replace('/', '-', $addStudentsToBatchFilter_data['DOA_from']);
          $DOA_from = date('Y-m-d',strtotime($DOA_from));

          $DOA_to = str_replace('/', '-', $addStudentsToBatchFilter_data['DOA_to']);
          $DOA_to = date('Y-m-d',strtotime($DOA_to));

          $where = "AM.ADMISSION_DATE BETWEEN '$DOA_from' and '$DOA_to'";

          $query = $this->db->where($where);
        }

        if($addStudentsToBatchFilter_data['Admission_ID'] != '')
        {
          $query = $this->db->where('AM.ADMISSION_ID',$addStudentsToBatchFilter_data['Admission_ID']);
        }

        if($addStudentsToBatchFilter_data['CENTRE_ID'] != '')
        {
          $query = $this->db->where('ENQM.CENTRE_ID',$addStudentsToBatchFilter_data['CENTRE_ID']);
        }

        // if($courseId != '')
        // {
        //  $query = $this->db->where('ACT.COURSE_ID',$courseId);
        // }

        $query = $this->db->order_by('AM.ADMISSION_ID','DESC')
                          ->group_by('AM.ADMISSION_ID');

       // $query = $this->db->limit($perpage,$offset);
        $query = $this->db->get();

        return $query->result_array();
    }

    public function getCourseStatusCompletedOrPersuing($admission_id,$type)
    {
      $query = $this->db->select("CM.COURSE_NAME as course_name")
                        ->from('StudentCourseStatus SC')
                        ->join('COURSE_MASTER CM','SC.COURSE_ID=CM.COURSE_ID','left')
                        ->where('SC.ADMISSION_ID', $admission_id);
                        if($type == "complete"){
                          $query = $this->db->where('SC.STATUS','1');
                        }
                        else{
                          $query = $this->db->where('SC.STATUS','0');
                        }
                        $query = $this->db->group_by('CM.COURSE_NAME')
                                          ->get();
      // print_r($this->db->last_query());
      // die;
       return $query->result_array();
    }


public function check_assignment_exists($data)
{
  $query = $this->db->select("*")
  ->from('assignment_master')
  ->where('COURSE_ID', $data['COURSE_ID'])
  ->where('ASSIGNMENT_NO', $data['ASSIGNMENT_NO'])
  ->where('LECTURE_FROM', $data['LECTURE_FROM'])
  ->where('LECTURE_TO', $data['LECTURE_TO'])
  ->get();
  // print_r($this->db->last_query());
  // die;
   return $query->result_array();
}

public function assignmentDelete($assignmentId)
{
    $this->db->where('ASSIGNMENT_ID', $assignmentId);
    $this->db->delete('assignment_master');
    // print_r($this->db->last_query());
    // die;
}

public function get_assignment_count($assignment_filter_data)
{
  $query = $this->db->select("AM.ASSIGNMENT_ID")

   ->from('assignment_master AM')

   ->join('course_master CM','AM.COURSE_ID = CM.COURSE_ID','left')

   ->join('employee_master EM','AM.MODIFIEDBY = EM.EMPLOYEE_ID','left');

   $query = $this->db->get();
   return $query->num_rows();
}
public function get_assignment_list($offset,$perpage,$assignment_filter_data)
{

  $query = $this->db->select("AM.ASSIGNMENT_NO,AM.LECTURE_FROM,AM.LECTURE_TO,AM.COURSE_ID,CM.COURSE_NAME,AM.MODIFIEDBY,DATE_FORMAT(AM.UPLOAD_DATE, '%d/%m/%Y') AS UPLOAD_DATE, AM.FILE,CONCAT_WS(' ',EM.EMP_FNAME,EM.EMP_MIDDLENAME,EM.EMP_LASTNAME) as EMP_NAME")

   ->from('assignment_master AM')

   ->join('course_master CM','AM.COURSE_ID = CM.COURSE_ID','left')

   ->join('employee_master EM','AM.MODIFIEDBY = EM.EMPLOYEE_ID','left');

        $query = $this->db->limit($perpage,$offset);
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
}

    public function BatchCreation($data)
    {
      $this->db->insert('batch_master',$data);       // for mysql
      $modified_by = $data['MODIFIED_BY'];
      $modified_date= $data['MODIFIED_DATE'];
      unset($data['MODIFIED_BY']);
      unset($data['MODIFIED_DATE']);
      $data['MODIFIEDBY'] = $modified_by;
      $data['MODIFIEDDATE'] = $modified_date;
      $dataForMsSql = $data;
      unset($dataForMsSql['CREATED_BY']);
      unset($dataForMsSql['CREATED_DATE']);
    }


    public function assignmentUpload($data)
    {
      $this->db->insert('assignment_master',$data);
    }

    public function BatchUpdation($batchId,$data)
    {
        $this->db->where('BATCHID', $batchId);
        $this->db->update('batch_master', $data);    //   for mysql

        $modified_by = $data['MODIFIED_BY'];
        $modified_date= $data['MODIFIED_DATE'];
        unset($data['MODIFIED_BY']);
        unset($data['MODIFIED_DATE']);
        $data['MODIFIEDBY'] = $modified_by;
        $data['MODIFIEDDATE'] = $modified_date;


        if($this->db->affected_rows() > 0){
          return true;
        }
        else{
          return false;
        }
    }
    public function batchDelete($batchId,$data)
    {
        $this->db->where('BATCHID', $batchId);
        $this->db->update('batch_master', $data);     //   for mysql

    }

	    public function get_empname($Id){
        $query = $this->db->select("ET.EMPLOYEE_ID,CONCAT_WS(' ',EM.EMP_FNAME,EM.EMP_MIDDLENAME,EM.EMP_LASTNAME) as EMP_NAME")
                          ->from('employee_timings ET')
                          ->join('employee_master EM','EM.EMPLOYEE_ID = ET.EMPLOYEE_ID','left')
                          ->where('ET.IS_ACTIVE =', '1')
                          ->where('EM.ISACTIVE =', '1')
                          ->where('ET.CENTRE_ID',$Id)
                          ->order_by('EMP_NAME','ASC')
                          ->group_by('EM.EMPLOYEE_ID')
                          ->get();

        return $query->result_array();
    }

    public function get_duration($sId){
        $query = $this->db->select("DURATION")
                          ->from('course_master')
                          ->where('COURSE_ID',$sId)
                          ->get();

        return $query->result_array();
    }

public function existingStudentsInBatch($BATCHID){

        $query = $this->db->select("bmst.BatchID,am.ADMISSION_ID,am.ADMISSION_DATE,CONCAT_WS(' ', enqm.ENQUIRY_FIRSTNAME, enqm.ENQUIRY_LASTNAME) AS STUDENT_NAME,am.TOTALFEES AS FEES,bmst.Batch_Std_ID,enqm.ENQUIRY_MOBILE_NO AS MOBILE_NO,centre_master.CENTRE_NAME,am.COURSE_TAKEN,am.MODULE_TAKEN")
                          ->from('enquiry_master enqm')
                          ->join('admission_master am','enqm.ENQUIRY_ID = am.ENQUIRY_ID','left')
                          ->join('batch_master_std_trans bmst','am.ADMISSION_ID = bmst.Admission_ID','left')
                          // ->join('course_master cm','bm.SUBJECT = cm.COURSE_ID','left')
                          // ->join('employee_master em','bm.FACULTY = em.EMPLOYEE_ID','left')
                          ->join('centre_master','am.CENTRE_ID = centre_master.CENTRE_ID','left')
                          ->where('bmst.BatchID',$BATCHID)
                          ->get();

        return $query->result_array();
    }
      //by laxmi
public function lecture_duration($BATCHID){
        $query = $this->db->select("bm.BATCHID,cm.DURATION")
                          ->from('course_master cm')
                          ->join('batch_master bm','bm.SUBJECT = cm.COURSE_ID','left')
                          ->where('BATCHID',$BATCHID)
                          ->get();

        return $query->result_array();

}

public function attendanceSheetDate($batchId){
  $query = $this->db->select("BLD.BATCH_ID, BLD.LECTURE_DATE, BLD.LECTURE_NO_AS_PER_FRM,
                       BLD.BATCH_LECTURE_ID")
                    ->from('batch_lecture_details BLD')
                    ->join('batch_master bm','bm.BATCHID = BLD.BATCH_ID','left')
                    ->where('BLD.BATCH_ID',"$batchId")
                    ->order_by('BLD.LECTURE_DATE','ASC');
    $query = $this->db->get();
    return $query->result_array();

}
public function attendance_details($BATCHID){
  $ATTENDANCEID=$this->session->userdata('ADMISSION_ID');
  $query = $this->db->select("ba.PRESENT, ba.ADMISSION_ID,BLD.BATCH_ID,ba.batch_lecture_id")
                          ->from('batch_attendance_details ba')
                          ->join('batch_lecture_details BLD','ba.batch_lecture_id =BLD.batch_lecture_id','left')
                          ->where('BLD.BATCH_ID',$BATCHID)
                          ->where('ba.ADMISSION_ID',$ATTENDANCEID)
                          ->get();
  return $query->result_array();
}

public function attendance_status($BATCHID){
  $where = "am.enquiry_id=em.enquiry_id and bmst.admission_id=am.admission_id and
bad.admission_id=am.admission_id and bld.batch_id=bmst.batchid and
bld.batch_lecture_id=bad.batch_lecture_id and bmst.batchid=$BATCHID ";

  $query = $this->db->select("am.admission_id,bmst.batchid,CONCAT_WS(' ',em.enquiry_firstname,em.enquiry_middlename,em.enquiry_lastname) AS STUDENT_NAME, em.enquiry_mobile_no,bld.lecture_date,bld.lecture_no_as_per_frm,bad.present")
                    ->from('admission_master am, enquiry_master em, batch_master_std_trans bmst, batch_attendance_details bad, batch_lecture_details bld')
                    ->where($where)
                    ->order_by('admission_id, lecture_date','ASC')
                    ->get();
  return $query->result_array();
}

public function new_attendance_status($BATCHID){
  $where = "bmst.Admission_ID=am.ADMISSION_ID and am.enquiry_id=em.enquiry_id and bmst.batchid=$BATCHID";

  $query = $this->db->select("am.admission_id,bmst.batchid,CONCAT_WS(' ',em.enquiry_firstname,em.enquiry_middlename,em.enquiry_lastname) AS STUDENT_NAME, em.enquiry_mobile_no")
                    ->from('admission_master am, enquiry_master em, batch_master_std_trans bmst')
                    ->where($where)
                    ->order_by('admission_id','ASC')
                    ->get();
  return $query->result_array();
}

//by laxmi
public function attendance($batchId)
{
  $query = $this->db->select("BLD.BATCH_ID, BLD.LECTURE_DATE, BLD.LECTURE_NO_AS_PER_FRM,
                       BLD.BATCH_LECTURE_ID")
                    ->from('batch_lecture_details BLD')
                    ->join('batch_master bm','bm.BATCHID = BLD.BATCH_ID','left')
                    ->where('BLD.BATCH_ID',"$batchId")
                    ->order_by('BLD.LECTURE_DATE','ASC');
    $query = $this->db->get();

    return $query->result_array();

}

    public function get_batch_details($BATCHID){
        $query = $this->db->select("bm.*,cm.COURSE_ID,cm.DURATION,bm.FACULTY,bm.FACULTY_NAME,CONCAT_WS(' ',em.EMP_FNAME,em.EMP_MIDDLENAME,em.EMP_LASTNAME) as EMP_NAME,em.EMPLOYEE_ID,centre_master.CENTRE_NAME,cm.COURSE_NAME")
                          ->from('batch_master bm')
                          ->join('course_master cm','bm.SUBJECT = cm.COURSE_ID','left')
                          ->join('employee_master em','bm.FACULTY = em.EMPLOYEE_ID','left')
                          ->join('centre_master','bm.CENTRE_ID = centre_master.CENTRE_ID','left')
                          ->where('BATCHID',$BATCHID)
                          ->get();

        return $query->result_array();
    }
 ///// This code for add Students To Batch Details Page/////////////////////////////////
    public function batch_detail($BATCHID){
        $query = $this->db->select("bm.BATCHID,bm.STARTTIME,bm.ENDTIME,bm.STARTDATE,bm.DAYS,bm.EXPECTEDENDDATE,bm.INTERNALEXAMDATE,cm.COURSE_ID,cm.DURATION,bm.FACULTY,bm.FACULTY_NAME,CONCAT_WS(' ',em.EMP_FNAME,em.EMP_MIDDLENAME,em.EMP_LASTNAME) as EMP_NAME,em.EMPLOYEE_ID,centre_master.CENTRE_NAME,cm.COURSE_NAME,bm.BATCH_STATUS")
                          ->from('batch_master bm')
                          ->join('course_master cm','bm.SUBJECT = cm.COURSE_ID','left')
                          ->join('employee_master em','bm.FACULTY = em.EMPLOYEE_ID','left')
                          ->join('centre_master','bm.CENTRE_ID = centre_master.CENTRE_ID','left')
                          ->where('BATCHID',$BATCHID)
                          ->get();

        return $query->result_array();

    }
    ///// end code of add Students To Batch Details Page/////////////////////////////////

    public function get_emp($FACULTY=''){
        $query = $this->db->select("EM.EMPLOYEE_ID,EM.ISACTIVE,CONCAT_WS(' ',EM.EMP_FNAME,EM.EMP_MIDDLENAME,EM.EMP_LASTNAME) as EMP_NAME")
                          ->from('employee_master EM')
                          ->where('EM.ISACTIVE', '1')
                          ->where('EM.EMPLOYEE_ID',$FACULTY)
                          ->order_by('EMP_NAME','ASC')
                          ->get();


        return $query->result_array();
    }
    public function get_batch_count($batch_filter_data){
        $query = $this->db->select('bm.CENTRE_ID')
                 ->from('batch_master bm')
                 ->join('course_master cm','bm.SUBJECT = cm.COURSE_ID','left')
                 ->join('centre_master','bm.CENTRE_ID = centre_master.CENTRE_ID','left')
                 ->join('employee_master em','bm.FACULTY = em.EMPLOYEE_ID','left');
        if(!empty($batch_filter_data))
        {
            if($batch_filter_data['STARTDATE'] != '' && $batch_filter_data['TODATE'] != '')
            {
              $date_format_change = str_replace("/","-",$batch_filter_data['STARTDATE']);
              $start_date = date('Y-m-d', strtotime($date_format_change));

              $to_date_format_change = str_replace("/","-",$batch_filter_data['TODATE']);
              $to_date = date('Y-m-d', strtotime($to_date_format_change));

               $query = $this->db->where('bm.STARTDATE > ',$start_date)
                                 ->where('bm.STARTDATE < ',$to_date);

            }

            if($batch_filter_data['FROMENDDATE'] != '' && $batch_filter_data['TOENDDATE'] != '')
            {
              $FROMENDDATE = str_replace("/","-",$batch_filter_data['FROMENDDATE']);
              $FROMENDDATE = date('Y-m-d', strtotime($FROMENDDATE));

              $TOENDDATE = str_replace("/","-",$batch_filter_data['TOENDDATE']);
              $TOENDDATE = date('Y-m-d', strtotime($TOENDDATE));

               $query = $this->db->where('bm.EXPECTEDENDDATE > ',$FROMENDDATE)
                                 ->where('bm.EXPECTEDENDDATE < ',$TOENDDATE);

            }


            if($batch_filter_data['CENTRE_ID'] != '')
            {

              $query = $this->db->where('bm.CENTRE_ID',$batch_filter_data['CENTRE_ID']);
            }
            if($batch_filter_data['BATCHID'] != '')
            {

              $query = $this->db->where('bm.BATCHID',$batch_filter_data['BATCHID']);
            }
            if($batch_filter_data['SUBJECT'] != '')
            {

              $query = $this->db->where('SUBJECT',$batch_filter_data['SUBJECT']);
            }
            if($batch_filter_data['BATCH_STATUS'] != '')
            {

              $query = $this->db->where('BATCH_STATUS',$batch_filter_data['BATCH_STATUS']);
            }
            if($batch_filter_data['DAYS'] != '')
            {

              $query = $this->db->where('bm.DAYS',$batch_filter_data['DAYS']);
            }

            if($batch_filter_data['FIRSTNAME'] != '')
            {

              $query = $this->db->like('em.EMP_FNAME',$batch_filter_data['FIRSTNAME'],'after');
            }
            if($batch_filter_data['LASTNAME'] != '')
            {

              $query = $this->db->like('em.EMP_LASTNAME',$batch_filter_data['LASTNAME'],'before');
            }
        }
        $query = $this->db->order_by('BATCHID','DESC');
            $query = $this->db->get();
        return $query->num_rows();
    }

    public function course_interested($course_ids = array()){
        $employeeIds = array(3170,2818,3230,1338);
        $query = $this->db->select('COURSE_ID,COURSE_NAME,MIN_FEES,MAX_FEES')
                          ->from('course_master')
                          ->group_by('COURSE_NAME');

        if(!(in_array($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'], $employeeIds))){
          $query = $this->db->where('ISACTIVE','1');
        }

        if($this->session->userdata('admin_data')[0]['CENTRE_ID'] == '205'){
            $query = $this->db->like("course_name","virar","both");
        }
        else{
            $query = $this->db->where("course_name not like '%virar%'");
        }

        if(!empty($course_ids) && is_array($course_ids)){
//            print_r($course_ids);
            $query = $this->db->where_in('COURSE_ID',$course_ids);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_batch_list($offset,$perpage,$batch_filter_data)
    {
        $query = $this->db->select("bm.CENTRE_ID,
                   CONCAT(RTRIM(bm.STARTTIME) , ' - ' , bm.ENDTIME) AS TIME,em.EMP_FNAME,em.EMP_LASTNAME,
                 CONCAT_WS(' ',em.EMP_FNAME,em.EMP_LASTNAME) as FACULTY,bm.FACULTY_NAME,
                 bm.BATCH_STATUS,bm.BATCHCOMPLETED,
                 bm.BATCHID, centre_master.CENTRE_NAME AS LOCATION, cm.COURSE_NAME AS SUBJECT, cm.COURSE_ID AS SUBJECTID,
                 bm.DAYS, bm.STARTDATE, bm.EXPECTEDENDDATE, bm.ActualEndDate, bm.INTERNALEXAMDATE,
                 bm.AttendanceSheetNo AS BATCH_NO, bm.BATCH_AVERAGE_FEEDBACK,
                 bm.FACULTY_REMARKS, bm.AUDITOR_REMARKS, BATCH_STATUS AS BATCH_STATUS_NEW, BATCH_STATUS_REMARK, BATCH_LATE_REMARK,
                 NEW_END_DATE, IS_APPROVED_BY_TC, GRACE_DATE")
                 ->from('batch_master bm')
                 ->join('course_master cm','bm.SUBJECT = cm.COURSE_ID','left')
                 ->join('centre_master','bm.CENTRE_ID = centre_master.CENTRE_ID','left')
                 ->join('employee_master em','bm.FACULTY = em.EMPLOYEE_ID','left')
                 ->where('bm.ISACTIVE',NULL);

        if($this->session->userdata('admin_data')[0]['ROLE_ID'] == 33){
          $query = $this->db->where('bm.FACULTY',$this->session->userdata('admin_data')[0]['EMPLOYEE_ID']);
        }

        if(!empty($batch_filter_data))
        {
            if($batch_filter_data['STARTDATE'] != '' && $batch_filter_data['TODATE'] != '')
            {
              $date_format_change = str_replace("/","-",$batch_filter_data['STARTDATE']);
              $start_date = date('Y-m-d', strtotime($date_format_change));

              $to_date_format_change = str_replace("/","-",$batch_filter_data['TODATE']);
              $to_date = date('Y-m-d', strtotime($to_date_format_change));

               $query = $this->db->where('bm.STARTDATE > ',$start_date)
                                 ->where('bm.STARTDATE < ',$to_date);

            }

            if($batch_filter_data['FROMENDDATE'] != '' && $batch_filter_data['TOENDDATE'] != '')
            {
              $FROMENDDATE = str_replace("/","-",$batch_filter_data['FROMENDDATE']);
              $FROMENDDATE = date('Y-m-d', strtotime($FROMENDDATE));

              $TOENDDATE = str_replace("/","-",$batch_filter_data['TOENDDATE']);
              $TOENDDATE = date('Y-m-d', strtotime($TOENDDATE));

               $query = $this->db->where('bm.EXPECTEDENDDATE > ',$FROMENDDATE)
                                 ->where('bm.EXPECTEDENDDATE < ',$TOENDDATE);

            }


            if($batch_filter_data['CENTRE_ID'] != '')
            {

              $query = $this->db->where('bm.CENTRE_ID',$batch_filter_data['CENTRE_ID']);
            }
            if($batch_filter_data['BATCHID'] != '')
            {

              $query = $this->db->where('bm.BATCHID',$batch_filter_data['BATCHID']);
            }
            if($batch_filter_data['SUBJECT'] != '')
            {

              $query = $this->db->where('SUBJECT',$batch_filter_data['SUBJECT']);
            }
            if($batch_filter_data['BATCH_STATUS'] != '')
            {

              $query = $this->db->where('BATCH_STATUS',$batch_filter_data['BATCH_STATUS']);
            }
            if($batch_filter_data['DAYS'] != '')
            {

              $query = $this->db->where('bm.DAYS',$batch_filter_data['DAYS']);
            }

            if($batch_filter_data['FIRSTNAME'] != '')
            {
              $query = $this->db->like('em.EMP_FNAME',$batch_filter_data['FIRSTNAME'],'after');
            }
            if($batch_filter_data['LASTNAME'] != '')
            {
              $query = $this->db->like('em.EMP_LASTNAME',$batch_filter_data['LASTNAME'],'after');
            }

        }
        // $query = $this->db->where('bm.ISACTIVE','1');  // isactive null in original data commented by ankur
        $query = $this->db->limit($perpage,$offset);
        $query = $this->db->order_by('BATCHID','DESC');
        $query = $this->db->get();
        // echo $this->db->last_query();
        return $query->result_array();
    }


    public function get_export_batch_list($batch_filter_data)
    {
        $query = $this->db->select("centre_master.CENTRE_NAME,
                   CONCAT(RTRIM(bm.STARTTIME) , ' - ' , bm.ENDTIME) AS TIME,em.EMP_FNAME,em.EMP_LASTNAME,
                 CONCAT_WS(' ',em.EMP_FNAME,em.EMP_LASTNAME) as FACULTY,bm.FACULTY_NAME,
                 bm.BATCH_STATUS,bm.BATCHCOMPLETED,
                 bm.BATCHID, cm.COURSE_NAME AS SUBJECT, cm.COURSE_ID AS SUBJECTID,
                 bm.DAYS, bm.STARTDATE, bm.EXPECTEDENDDATE, bm.ActualEndDate, bm.INTERNALEXAMDATE,
                 bm.AttendanceSheetNo AS BATCH_NO, bm.BATCH_AVERAGE_FEEDBACK,
                 bm.FACULTY_REMARKS, bm.AUDITOR_REMARKS, BATCH_STATUS AS BATCH_STATUS_NEW, BATCH_STATUS_REMARK, BATCH_LATE_REMARK,
                 NEW_END_DATE, IS_APPROVED_BY_TC, GRACE_DATE,bm.CENTRE_ID")
                 ->from('batch_master bm')
                 ->join('course_master cm','bm.SUBJECT = cm.COURSE_ID','left')
                 ->join('centre_master','bm.CENTRE_ID = centre_master.CENTRE_ID','left')
                 ->join('employee_master em','bm.FACULTY = em.EMPLOYEE_ID','left');


        if(!empty($batch_filter_data))
        {


            if($batch_filter_data['STARTDATE'] != '' && $batch_filter_data['TODATE'] != '')
            {
              $date_format_change = str_replace("/","-",$batch_filter_data['STARTDATE']);
              $start_date = date('Y-m-d', strtotime($date_format_change));

              $to_date_format_change = str_replace("/","-",$batch_filter_data['TODATE']);
              $to_date = date('Y-m-d', strtotime($to_date_format_change));

               $query = $this->db->where('bm.STARTDATE > ',$start_date)
                                 ->where('bm.STARTDATE < ',$to_date);

            }

            if($batch_filter_data['FROMENDDATE'] != '' && $batch_filter_data['TOENDDATE'] != '')
            {
              $FROMENDDATE = str_replace("/","-",$batch_filter_data['FROMENDDATE']);
              $FROMENDDATE = date('Y-m-d', strtotime($FROMENDDATE));

              $TOENDDATE = str_replace("/","-",$batch_filter_data['TOENDDATE']);
              $TOENDDATE = date('Y-m-d', strtotime($TOENDDATE));

               $query = $this->db->where('bm.EXPECTEDENDDATE > ',$FROMENDDATE)
                                 ->where('bm.EXPECTEDENDDATE < ',$TOENDDATE);

            }


            if($batch_filter_data['CENTRE_ID'] != '')
            {

              $query = $this->db->where('bm.CENTRE_ID',$batch_filter_data['CENTRE_ID']);
            }
            if($batch_filter_data['BATCHID'] != '')
            {

              $query = $this->db->where('bm.BATCHID',$batch_filter_data['BATCHID']);
            }
            if($batch_filter_data['SUBJECT'] != '')
            {

              $query = $this->db->where('SUBJECT',$batch_filter_data['SUBJECT']);
            }
            if($batch_filter_data['BATCH_STATUS'] != '')
            {

              $query = $this->db->where('BATCH_STATUS',$batch_filter_data['BATCH_STATUS']);
            }
            if($batch_filter_data['DAYS'] != '')
            {

              $query = $this->db->where('bm.DAYS',$batch_filter_data['DAYS']);
            }

            if($batch_filter_data['FIRSTNAME'] != '')
            {

              $query = $this->db->like('em.EMP_FNAME',$batch_filter_data['FIRSTNAME'],'after');
            }
            if($batch_filter_data['LASTNAME'] != '')
            {

              $query = $this->db->like('em.EMP_LASTNAME',$batch_filter_data['LASTNAME'],'before');
            }

        }
        $query = $this->db->where('bm.ISACTIVE','1');
        $query = $this->db->order_by('BATCHID','DESC');
        $query = $this->db->limit('8000');
        $query = $this->db->get();

        $batchSearch['fields'] = $query->list_fields();
        $batchSearch['details'] = $query->result_array();

        return $batchSearch;
    }

    public function getStudentBatchDetails($admission_id){
      $query = $this->db->select("CM.COURSE_NAME,BM.BATCHCOMPLETED,BM.BATCH_STATUS")
                        ->from('batch_master_std_trans BMS')
                        ->join('batch_master BM','BMS.BatchID=BM.BATCHID','left')
                        ->join('course_master CM','BM.SUBJECT=CM.COURSE_ID','left')
                        ->where('BMS.Admission_ID',$admission_id)
                        ->get();

      return $query->result_array();
    }

    public function uploadAssignmentsOrProjects($data){
      $query = $this->db->insert("ASSIGNMENT_FEEDING_MASTER",$data);
      return $this->db->insert_id();
    }

    public function get_student_exam_details($data){
        $query = $this->db->select("concat(EnM.ENQUIRY_FIRSTNAME,' ',EnM.ENQUIRY_LASTNAME) as Student_name,EM.ADMISSION_ID,EM.EXAM_MASTER_NEW_ID as EXAM_ID,CoM.COURSE_NAME,CM.CENTRE_NAME as STUDY_CENTRE_NAME,CeM.CENTRE_NAME as CENTRE_NAME,EM.EXAM_DATE,EM.EXAM_TIME,EM.JOURNAL_MARKS as ASSIGNMENT_MARKS,EM.ONLINE_MARKS as EXTERNAL_EXAM_MARKS,EM.IS_EXAM_GIVEN,EM.RESULT,EnM.ENQUIRY_EMAIL,concat(EnM.ENQUIRY_MOBILE_NO,', ',EnM.ENQUIRY_TELEPHONE) as CONTACT_DETAILS")
                          ->from('exam_master_new EM')
                          ->join('admission_master AM','AM.ADMISSION_ID=EM.ADMISSION_ID','right')
                          ->join('enquiry_master EnM','EnM.ENQUIRY_ID=AM.ENQUIRY_ID','right')
                          ->join('course_master CoM','CoM.COURSE_ID=EM.COURSE_ID','right')
                          ->join('centre_master CM','CM.CENTRE_ID=AM.CENTRE_ID','right')
                          ->join('centre_master CeM','CeM.CENTRE_ID=EM.CENTRE_ID','right');
                          if(!empty($data)){
                            if(isset($data['centre_id'])){
                              if($data['centre_id'] != ""){
                                  $query = $this->db->where('AM.CENTRE_ID',$data['centre_id']);
                              }
                            }

                            if(isset($data['exam_centre_id'])){
                              if($data['exam_centre_id'] != ""){
                                  $query = $this->db->where('EM.CENTRE_ID',$data['exam_centre_id']);
                              }
                            }

                            if(isset($data['first_name'])){
                              if($data['first_name'] != ""){
                                  $query = $this->db->like('EnM.ENQUIRY_FIRSTNAME',$data['first_name'],'after');
                              }
                            }

                            if(isset($data['last_name'])){
                              if($data['last_name'] != ""){
                                  $query = $this->db->like('EnM.ENQUIRY_LASTNAME',$data['last_name'],'after');
                              }
                            }

                            if(isset($data['from_exam_date']) && isset($data['to_exam_date'])){
                              if(($data['from_exam_date'] != "") && ($data['to_exam_date'] != "")){
                                $from_date = str_replace('/', '-', $data['from_exam_date']);
                                $from_exam_date = date('Y-m-d',strtotime($from_date));

                                $to_date = str_replace('/', '-', $data['to_exam_date']);
                                $to_exam_date = date('Y-m-d',strtotime($to_date));

                                $where = "EM.EXAM_DATE BETWEEN '$from_exam_date' and '$to_exam_date'";

                                $query = $this->db->where($where);
                              }
                            }
                          }
                          $query = $this->db->order_by('EM.EXAM_MASTER_NEW_ID','DESC')
                                            ->get();


        return $query->result_array();
    }


    public function getBatchWeeklyTestMarks($batch_id){
      $query = $this->db->select('BT.BatchID,BT.Admission_ID,BT.MODIFIED_BY,BT.MODIFIED_DATE,CONCAT_WS(" ", EM.ENQUIRY_FIRSTNAME, EM.ENQUIRY_MIDDLENAME, EM.ENQUIRY_LASTNAME) AS STUDENT_NAME,EM.ENQUIRY_MOBILE_NO as MOBILE_NO')
                        ->from('batch_master_std_trans BT')
                        ->join('admission_master AM','AM.ADMISSION_ID=BT.Admission_ID','right')
                        ->join('enquiry_master EM','EM.ENQUIRY_ID=AM.ENQUIRY_ID','right')
                        ->where('BT.BatchID',$batch_id)
                        ->get();
      $weekly_test_dates = $query->result_array();
      //
      // $query = $this->db->select("BID.BATCH_ID,BID.INTERNAL_EXAM_DATE")
      //                   ->from('batch_internals_date_master BID')
      //                   ->where('BID.BATCH_ID',$bacth_id)
      //                   ->get();
      // $weekly_test_dates = $query->result_array();


      for($i=0;$i<count($weekly_test_dates);$i++){
        // echo $weekly_test_dates[$i]['BATCH_ID'];
        $batchid = $weekly_test_dates[$i]['BatchID'];
        $admission_id = $weekly_test_dates[$i]['Admission_ID'];
        $query1 = $this->db->select("BID.BATCH_ID,BID.INTERNAL_EXAM_DATE,BI.ADM_ID,BI.MARKS_OBTAINED")
                          ->from('batch_internals_date_master BID')
                          ->join('batch_internals_details BI','BI.BTCH_INTERNAL_ID=BID.BTCH_INTERNAL_ID','right')
                          ->where('BID.BATCH_ID',$batchid)
                          ->where('BI.ADM_ID',$admission_id)
                          ->order_by('BID.INTERNAL_EXAM_DATE','ASC')
                          ->get();

        $weekly_test_marks = $query1->result_array();
        array_push($weekly_test_dates[$i],$weekly_test_marks);
      }

      return $weekly_test_dates;
    }

    public function saveWeeklyTestDate($data){
      $query = $this->db->insert("batch_internals_date_master",$data);
      return $this->db->insert_id();
    }

    public function saveWeeklyTestMarks($data){
      $query = $this->db->insert("batch_internals_details",$data);
      return $this->db->insert_id();
    }

}
