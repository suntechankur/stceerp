			
			<section>				
				<div class="container">
					<div class="gapping"></div>
					<div class="create_batch_form">
						<div id="box"> <h2>Faculty Audit Report</h2></div>
						<div class="row">			  
							<div class="col-sm-12">
								<div class="panel panel-default">
                                <div class="panel-heading">

                               
					
<!-- start message area -->

            <?php if($this->session->flashdata('danger')) { ?>
                        <div class="alert alert-danger">
              <strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
              <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php } ?>
                             
                        
            <?php if($this->session->flashdata('failed')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
            </div>
            <?php } ?>
  
            <?php if($this->session->flashdata('info1')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('warning')) { ?>
                        <div class="alert alert-warning">
              <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
            </div>
            <?php } ?>

 <!-- End message area --> 


                                </div>
									<div class="panel-body">
										
										<?php echo form_open();

										// print_r($facultyaudit_filter_data);?>

											<div class="col-md-6">
												<div class="form-group">
													<label>Month:</label>
													<select name="Audit_Month" class="form-control">
														<option value="">Select</option>
														<option  value="January" <?php if(isset($facultyaudit_filter_data['Audit_Month'])){ echo ($facultyaudit_filter_data['Audit_Month'] == "January") ? ' selected="selected"' : '';}?>>January</option>

														<option  value="February" <?php if(isset($facultyaudit_filter_data['Audit_Month'])){ echo ($facultyaudit_filter_data['Audit_Month'] == "February") ? ' selected="selected"' : '';}?>>February</option>

														<option value="March" <?php if(isset($facultyaudit_filter_data['Audit_Month'])){ echo ($facultyaudit_filter_data['Audit_Month'] == "March") ? ' selected="selected"' : '';}?>>March</option>

														<option value="April" <?php if(isset($facultyaudit_filter_data['Audit_Month'])){ echo ($facultyaudit_filter_data['Audit_Month'] == "April") ? ' selected="selected"' : '';}?>>April</option>

														<option value="May" <?php if(isset($facultyaudit_filter_data['Audit_Month'])){ echo ($facultyaudit_filter_data['Audit_Month'] == "May") ? ' selected="selected"' : '';}?>>May</option>

                                                        <option value="June"<?php if(isset($facultyaudit_filter_data['Audit_Month'])){ echo ($facultyaudit_filter_data['Audit_Month'] == "June") ? ' selected="selected"' : '';}?>>June</option>

														<option value="July"<?php if(isset($facultyaudit_filter_data['Audit_Month'])){ echo ($facultyaudit_filter_data['Audit_Month'] == "July") ? ' selected="selected"' : '';}?>>July</option>

														<option value="August"<?php if(isset($facultyaudit_filter_data['Audit_Month'])){ echo ($facultyaudit_filter_data['Audit_Month'] == "August") ? ' selected="selected"' : '';}?>>August</option>

														<option value="September"<?php if(isset($facultyaudit_filter_data['Audit_Month'])){ echo ($facultyaudit_filter_data['Audit_Month'] == "September") ? ' selected="selected"' : '';}?>>September</option>

														<option value="October"<?php if(isset($facultyaudit_filter_data['Audit_Month'])){ echo ($facultyaudit_filter_data['Audit_Month'] == "October") ? ' selected="selected"' : '';}?>>October</option>

                                                        <option value="November"<?php if(isset($facultyaudit_filter_data['Audit_Month'])){ echo ($facultyaudit_filter_data['Audit_Month'] == "November") ? ' selected="selected"' : '';}?>>November</option>

														<option value="December"<?php if(isset($facultyaudit_filter_data['Audit_Month'])){ echo ($facultyaudit_filter_data['Audit_Month'] == "December") ? ' selected="selected"' : '';}?>>December</option>
													</select>
                                                    
												</div>
												
												
												
												
												<div class="form-group">
													<label>Year:</label>
													<select class="form-control" name="Audit_Year">
														<option value="">Select</option>
														<option value="2014" <?php if(isset($facultyaudit_filter_data['Audit_Year'])){ echo ($facultyaudit_filter_data['Audit_Year'] == "2014") ? ' selected="selected"' : '';}?>>2014</option>

														<option value="2015" <?php if(isset($facultyaudit_filter_data['Audit_Year'])){ echo ($facultyaudit_filter_data['Audit_Year'] == "2015") ? ' selected="selected"' : '';}?>>2015</option>

														<option value="2016" <?php if(isset($facultyaudit_filter_data['Audit_Year'])){ echo ($facultyaudit_filter_data['Audit_Year'] == "2016") ? ' selected="selected"' : '';}?>>2016</option>	

                                                        <option value="2017" <?php if(isset($facultyaudit_filter_data['Audit_Year'])){ echo ($facultyaudit_filter_data['Audit_Year'] == "2017") ? ' selected="selected"' : '';}?>>2017</option>	

                                                        <option value="2018" <?php if(isset($facultyaudit_filter_data['Audit_Year'])){ echo ($facultyaudit_filter_data['Audit_Year'] == "2018") ? ' selected="selected"' : '';}?>>2018</option>	
													</select>
												</div>
											</div>
											
											<div class="col-md-6">
                                            
                                               
												
												<div class="form-group">
													
                                                    <label>Centre:</label>
													<select class="form-control" name="CENTRE_ID">
														<option value=''>Please Select</option>
                                                    <?php foreach($centres as $centre){?>
                                                    <option value="<?php echo $centre['CENTRE_ID']; ?>"<?php if(isset($facultyaudit_filter_data['CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $facultyaudit_filter_data['CENTRE_ID']) ? ' selected="selected"' : '';}?>><?php echo $centre['CENTRE_NAME']; ?></option>
                                                    <?php } ?>
													</select>
												</div>
												
                                                <div class="form-group">
                                               <div class="row">			  
							<div class="col-sm-4">
													<button type="submit" name="submit" value="CreateNew" id="CreateNew" class="btn btn-primary" style="float:right; width:150px; height:50px;">Create New Audit</button>
                                                    </div>
                                                    
                                                    <div class="col-sm-4">
													<button type="submit" name="submit" value="existing" id="existing" class="btn btn-primary" style="float:right; width:150px; height:50px;">Open Existing Audit</button>
                                                    </div>
                                                    
                                                    <div class="col-sm-4">
													<button type="submit" name="reset" value="reset" id="reset" class="btn btn-primary" style="float:right; width:150px; height:50px;">Reset</button>
                                                    </div></div>
												</div>
												
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><br><br>

				<?php

if(!empty($facultyaudit))
{
	
	
	?>
		
				<div class="container">
					<div class="row">
                     <div class="col-md-12 col-sm-12"><div id="box"> <h2>Search Report</h2></div>
							<div class="panel panel-default">
								<!--<div class="panel-heading">Form Elements</div>-->
								<div class="panel-body">
                    
						<div class="col-sm-12">
							
							<div id="">
							
                           <?php
								echo form_open('batchMaster/facultyaudit_save');
                            ?>  
                           <div class="panel-body table-responsive">
            <table class="table table-bordered table-condensed table-stripped">
                <thead>
                                 
								  
                               
                                

    <tr>
									
									<th style="width:500px;">Name</th>
                                    <th >Stream</th>
                                    <th>Audit Date</th>
									<th >Centre</th>
									<th >Subject</th>
                                    <th >Technical Knowledge</th>
                                    <th >Presentation/Mode</th>
									<th >Query Solving Ability</th>
									<th>Examples Given</th>
									<th >Student Participation</th>
									
									<th >Body Language</th>
									<th >Acamdmic Feedback</th>
									<th >Batch File Status</th>
									<th>EMIS Updation</th>
									
									<th>Late/Absenteeism</th>
									<th>Teaching%</th>
                                    <th >Academic Feedback</th>
                                    
                                    <th>EMIS/File Updation%</th>
                                    <th >Punctuality</th>
                                    
								</tr>
								</thead>
								
								<tbody>

    <?php
$i=0;
foreach($facultyaudit as $data)
{
	
$Number=$i+1;
?>
	
    <script>
        $(document).ready(function() {
        $("#AUDIT_DATE<?php print $Number?>").datepicker({ dateFormat: 'dd/mm/yy' }).val();
        });
    </script> 
    
     <script>
    
    function add<?php print $Number?>() {
		   var a, b, c, d, e, f, g, h, i, j;
         

	a = parseInt(document.getElementById("TECHNICAL_KNOWLEDGE<?php print $Number?>").value);
	// If textbox value is null i.e empty, then the below mentioned if condition will
	// come into picture and make the value to '0' to avoid errors.
	 if (isNaN(a) == true) {
         a = 0;
     }
	 
     var b = parseInt(document.getElementById("PRESENTATION_SKILL_MODE_OF_TEACHING<?php print $Number?>").value);
     if (isNaN(b) == true) {
         b = 0;
     }
	 
     var c = parseInt(document.getElementById("QUERY_SOLVING_ABILITY<?php print $Number?>").value);
     if (isNaN(c) == true) {
         c = 0;
     }
     var d = parseInt(document.getElementById("EXAMPLE_GIVEN_WHILE_TEACHING<?php print $Number?>").value);
     if (isNaN(d) == true) {
         d = 0;
     }
	 
	 var e = parseInt(document.getElementById("STUDENT_PARTICIPATION<?php print $Number?>").value);
     if (isNaN(e) == true) {
         e = 0;
     }
	 
	 var f = parseInt(document.getElementById("BODY_LANGUAGE<?php print $Number?>").value);
     if (isNaN(f) == true) {
         f = 0;
     }
	 var g = parseInt(document.getElementById("ACADEMIC_FEEDBACK<?php print $Number?>").value);
     if (isNaN(g) == true) {
         g = 0;
     }
	 
	 var h = parseInt(document.getElementById("BATCH_FILE_STATUS<?php print $Number?>").value);
     if (isNaN(h) == true) {
         h = 0;
     }
	 
	 var i = parseInt(document.getElementById("EMIS_UPDATION<?php print $Number?>").value);
     if (isNaN(i) == true) {
         i = 0;
     }
	 
	  var j = parseInt(document.getElementById("LATE_COMING_ABSENT<?php print $Number?>").value);
     if (isNaN(j) == true) {
         j = 0;
     }
	 //alert(a+ b +c +d)
     var Teaching = Math.ceil((a + b + c + d + e + f) / 30 * 100);
	 if(Teaching)
	 {
	 	//$("#Teaching<?php print $Number?>").html(Teaching);
		document.getElementById("Teaching<?php print $Number?>").value=Teaching;
	 }

	 var AcademicFeedback = Math.ceil((g) / 5 * 100);
	 if (AcademicFeedback) {
        //$("#AcademicFeedback<?php print $Number?>").html(AcademicFeedback);
		document.getElementById("AcademicFeedback<?php print $Number?>").value=AcademicFeedback;
     }
	 
	 var EMIS = Math.ceil((h + i) / 10 * 100);
	 if (EMIS) {
        //$("#EMIS<?php print $Number?>").html(EMIS);
		document.getElementById("EMIS<?php print $Number?>").value=EMIS;
     }
	 
	 var Punctuality = Math.ceil((j) / 5 * 100);
	 if (Punctuality) {
        //$("#Punctuality<?php print $Number?>").html(Punctuality);
		document.getElementById("Punctuality<?php print $Number?>").value=Punctuality;
		
		
     }
	 
	 
}
    </script>
   
	<?php 
// print_r($facultyaudit_filter_data);
// die;
	

		if(!empty($facultyaudit_filter_data['submit']=="existing"))
		    {
    ?>
                       
		     
                       <tr>
                         
                                        <td><?php print $data['EMPLOYEE_NAME']?><input type="hidden" name="EMPLOYEE_ID[]" value="<?php print $data['EMPLOYEE_ID'];?>" /></td>
                                        <td><select name="STREAM[]" class="form-control" style="width:150px;"><option value="">---------</option>
                                            <option value="Programming" <?php if($data['STREAM']=="Programming") { print "selected='selected'"; }?>>Programming</option>
                                            <option value="Hardware"<?php if($data['STREAM']=="Hardware") { print "selected='selected'"; }?>>Hardware</option>
                                            <option value="Basic"<?php if($data['STREAM']=="Basic") { print "selected='selected'"; }?>>Basic</option>
                                            <option value="Animation"<?php if($data['STREAM']=="Animation") { print "selected='selected'"; }?>>Animation</option>
                                        </select></td>
                                         
                                        <td><input type="text" class="form-control" style="width:180px;" name="AUDIT_DATE[]" id="AUDIT_DATE<?php print $Number;?>" value="<?php print $data['AUDIT_DATE']?>" /></td>
                                        <td><?php print $data['CENTRE_NAME']?></td>
                                        
                                                              
                                        <td><input type="text" class="form-control" style="width:150px;" name="SUBJECT[]" value="<?php print $data['SUBJECT']?>" /></td>
                                        <td><input type="text" class="form-control" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="TECHNICAL_KNOWLEDGE[]" id="TECHNICAL_KNOWLEDGE<?php print $Number?>" value="<?php print $data['TECHNICAL_KNOWLEDGE']?>" /></td>
                                        <td><input type="text" class="form-control" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="PRESENTATION_SKILL_MODE_OF_TEACHING[]" id="PRESENTATION_SKILL_MODE_OF_TEACHING<?php print $Number?>" value="<?php print $data['PRESENTATION_SKILL_MODE_OF_TEACHING']?>" /></td>
                                        <td><input type="text" class="form-control" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="QUERY_SOLVING_ABILITY[]" id="QUERY_SOLVING_ABILITY<?php print $Number?>" value="<?php print $data['QUERY_SOLVING_ABILITY']?>" /></td>
                                        <td><input type="text" class="form-control" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="EXAMPLE_GIVEN_WHILE_TEACHING[]" id="EXAMPLE_GIVEN_WHILE_TEACHING<?php print $Number?>" value="<?php print $data['EXAMPLE_GIVEN_WHILE_TEACHING']?>" /></td>
                                        
                                         <td><input type="text" class="form-control" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="STUDENT_PARTICIPATION[]" id="STUDENT_PARTICIPATION<?php print $Number?>" value="<?php print $data['STUDENT_PARTICIPATION']?>" /></td>
                                        
                                       
                                         
                                        <td><input type="text" class="form-control" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="BODY_LANGUAGE[]" id="BODY_LANGUAGE<?php print $Number?>" value="<?php print $data['BODY_LANGUAGE']?>" /></td>
                                        <td><input type="text" class="form-control" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="ACADEMIC_FEEDBACK[]" id="ACADEMIC_FEEDBACK<?php print $Number?>" value="<?php print $data['ACADEMIC_FEEDBACK']?>" /></td>
                                        <td><input type="text" class="form-control" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="BATCH_FILE_STATUS[]" id="BATCH_FILE_STATUS<?php print $Number?>" value="<?php print $data['BATCH_FILE_STATUS']?>" /></td>
                                        
                                        <td><input type="text" class="form-control" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="EMIS_UPDATION[]" id="EMIS_UPDATION<?php print $Number?>" value="<?php print $data['EMIS_UPDATION']?>" /></td>
                                        
                                        <td><input type="text" class="form-control" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="LATE_COMING_ABSENT[]" id="LATE_COMING_ABSENT<?php print $Number?>" value="<?php print $data['LATE_COMING_ABSENT']?>" /></td>
                                         
                                        
                                        <td><input type="text" class="form-control"  readonly="readonly" style="width:50px;" name="Teaching[]" id="Teaching<?php print $Number?>" value="<?php print $data['FACULTY_TEACHING_PERCENT']?>" /></td>
                                        <td><input type="text" class="form-control" readonly="readonly" style="width:50px;" name="AcademicFeedback[]" id="AcademicFeedback<?php print $Number?>" value="<?php print $data['ACADEMIC_FEEDBACK_PERCENT']?>" /></td>
                                        
                                        <td><input type="text" class="form-control" readonly="readonly" style="width:50px;" name="EMIS[]" id="EMIS<?php print $Number?>" value="<?php print $data['EMIS_BATCH_FILE_PERCENT']?>" /></td>
                                        <td><input type="text" class="form-control" readonly="readonly" style="width:50px;" name="Punctuality[]" id="Punctuality<?php print $Number?>" value="<?php print $data['PUNCTUALITY']?>" /></td>
                                       
								</tr>      
                               <input type="hidden" name="act" value="existing" />
                                <input type="hidden" name="AUDIT_REPORT_MASTER_ID[]" value="<?php print $data['AUDIT_REPORT_MASTER_ID'];?>" />
                               
                               
					<?php
					 }

                   	 	elseif(!empty($facultyaudit_filter_data['submit']=="CreateNew"))
                     {
                    
                    ?>
                   
                        <tr>
                         
                                        <td><?php print $data['EMPLOYEE_NAME']?><input type="hidden" name="EMPLOYEE_ID[]" value="<?php print $data['EMPLOYEE_ID'];?>" /></td>
                                        <td><select name="STREAM[]" class="form-control" style="width:150px;">
                                                <option value="">---------</option>
                                                <option value="Programming">Programming</option>
                                                <option value="Hardware">Hardware</option>
                                                <option value="Basic">Basic</option>
                                                <option value="Animation">Animation</option>
                                        </select></td>
                                         
                                        <td><input type="text" class="form-control enquiry_date" style="width:180px;" name="AUDIT_DATE[]" id="AUDIT_DATE<?php print $Number;?>" value="" /></td>
                                        <td><?php print $data['CENTRE_NAME']?></td>
                                        
                                                              
                                        <td><input type="text" class="form-control" style="width:150px;" name="SUBJECT[]" value="" /></td>
                                        <td><input type="text" class="form-control txt<?php print $Number?>" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="TECHNICAL_KNOWLEDGE[]" id="TECHNICAL_KNOWLEDGE<?php print $Number?>" value="" /></td>
                                        <td><input type="text" class="form-control txt<?php print $Number?>" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="PRESENTATION_SKILL_MODE_OF_TEACHING[]" id="PRESENTATION_SKILL_MODE_OF_TEACHING<?php print $Number?>" value="" /></td>
                                        <td><input type="text" class="form-control txt<?php print $Number?>" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="QUERY_SOLVING_ABILITY[]" id="QUERY_SOLVING_ABILITY<?php print $Number?>" value="" /></td>
                                        <td><input type="text" class="form-control txt<?php print $Number?>" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="EXAMPLE_GIVEN_WHILE_TEACHING[]" id="EXAMPLE_GIVEN_WHILE_TEACHING<?php print $Number?>" value="" /></td>
                                        
                                         <td><input type="text" class="form-control txt<?php print $Number?>"  style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="STUDENT_PARTICIPATION[]" id="STUDENT_PARTICIPATION<?php print $Number?>" value="" /></td>
                                        
                                       
                                         
                                        <td><input type="text" class="form-control txt<?php print $Number?>" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="BODY_LANGUAGE[]" id="BODY_LANGUAGE<?php print $Number?>" value="" /></td>
                                        <td><input type="text" class="form-control txt<?php print $Number?>" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="ACADEMIC_FEEDBACK[]" id="ACADEMIC_FEEDBACK<?php print $Number?>" value="" /></td>
                                        <td><input type="text" class="form-control txt<?php print $Number?>" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="BATCH_FILE_STATUS[]" id="BATCH_FILE_STATUS<?php print $Number?>" value="" /></td>
                                        
                                        <td><input type="text" class="form-control txt<?php print $Number?>" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="EMIS_UPDATION[]" id="EMIS_UPDATION<?php print $Number?>" value="" /></td>
                                        
                                        <td><input type="text" class="form-control txt<?php print $Number?>" style="width:50px;" onkeypress="return isNumber(event)" maxlength="1" onkeyup="add<?php print $Number?>()" name="LATE_COMING_ABSENT[]" id="LATE_COMING_ABSENT<?php print $Number?>" value="" /></td>
                                         <td><input type="text" class="form-control" readonly="readonly" style="width:50px;" name="Teaching[]" id="Teaching<?php print $Number?>" value="0" /></td>
                                        <td><input type="text" class="form-control" readonly="readonly" style="width:50px;" name="AcademicFeedback[]" id="AcademicFeedback<?php print $Number?>" value="0" /></td>
                                        
                                        <td><input type="text" class="form-control" readonly="readonly" style="width:50px;" name="EMIS[]" id="EMIS<?php print $Number?>" value="0" /></td>
                                        <td><input type="text" class="form-control" readonly="readonly" style="width:50px;" name="Punctuality[]" id="Punctuality<?php print $Number?>" value="0" /></td>
                                       
								</tr>
                                
                            
                                
                                <input type="hidden" name="act" value="CreateNew" />
                                
    <?php

			}

 $i++;

  } /* End foreach */


?>
							
							
								</tbody>
								</table>
                                
                                
							</div><br>
							<?php
							

													
							 $SelectedYM=$facultyaudit_filter_data['Audit_Year']."-".date('m',strtotime($facultyaudit_filter_data['Audit_Month']));
							$OneMonthBack=date('Y-m', strtotime(date('Y-m')." -1 month"));

							if($facultyaudit_filter_data['submit']=="existing")
							{
								

							if(!empty($facultyaudit_filter_data['submit']) && ($facultyaudit_filter_data['submit']=="existing") && ($SelectedYM>$OneMonthBack))
							{
								// ($SelectedYM>$OneMonthBack)


                          	  print "<button class='data_add' style='width:300px'>Update Report<br></button>";
                           }
                           else{
                            	print "<pre style='color:red;'>Please select current year and month if you want enable update report button, Otherwise you can see only.</pre>";
                            	} 
                            }
                            elseif($facultyaudit_filter_data['submit']=="CreateNew")
                            {


	                           if(!empty($facultyaudit_filter_data['submit']) && ($facultyaudit_filter_data['submit']=="CreateNew") && ($SelectedYM>$OneMonthBack))
	                           {
	                           	  	print "<button class='data_add' style='width:300px'>Save Report<br></button>";
	                            }
                            	else{

                            		print "<pre style='color:red;'>Please select current year and month to enable save report button</pre>";
                            	} 
                            }?>

                            <input type="hidden" name="Audit_Month" value="<?php print  $facultyaudit_filter_data['Audit_Month'];?>" />
                            <input type="hidden" name="Audit_Year" value="<?php print $facultyaudit_filter_data['Audit_Year'];?>" />
                            <input type="hidden" name="CENTRE_ID" value="<?php print $facultyaudit_filter_data['CENTRE_ID'];?>" />
                           
                            </form>
                            <br>
                            
							</div><br><br>
						<br></div><br>
					</div>
				</div>
                </div>
                </div>
                </div>
                <?php
			}
else
{
	// print "<pre style='color:red;'>Audit Not Exists, You can Create New.</pre>";
}
			?>
			</section>

			
           <!--  <script type="application/javascript">
            function AuditReport()
			{
				if(confirm("Do You Want To Update / Add ?"))
	       {
			window.location.href="lib/save.php";
			this.AuditReportFrm.submit();
		   }
			}
            </script> -->
            
			
		