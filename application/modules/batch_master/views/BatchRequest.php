
            <section>
                <div class="container">
                    <div class="gapping"></div>
                    <div class="create_batch_form">
                        <div id="box"> <h2>Batch Requests from Students</h2></div>
                        <div class="row">             
                            <div class="col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                    <span style="color:red; text-align: center;"><?php echo validation_errors(); ?></span>
                                        <?php echo form_open();?>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Centre:</label>
                                                    <select class="form-control" name="CENTRE_ID">
                                                        <option value=''>Please Select</option>
                                                    <?php foreach($centres as $centre){?>
                                                    <option value="<?php echo $centre['CENTRE_ID']; ?>"<?php if(isset($batch_request_filter_data['CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $batch_request_filter_data['CENTRE_ID']) ? ' selected="selected"' : '';}?>><?php echo $centre['CENTRE_NAME']; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                                
                                                 <div class="form-group">
                                                    <label>Batch Status:</label>
                                                    <select class="form-control" name="BATCH_STATUS">
                                                        <option value="">Select Batch Status</option>
                                                        <option value="IN PROCESS" <?php if($batch_request_filter_data['BATCH_STATUS']=='IN PROCESS') echo "selected"?>>IN PROCESS</option>
                                                        <option value="APPROVED" <?php if($batch_request_filter_data['BATCH_STATUS']=='APPROVED') echo "selected"?>>APPROVED</option>
                                                        <option value="ON HOLD" <?php if($batch_request_filter_data['BATCH_STATUS']=='ON HOLD') echo "selected"?>>ON HOLD</option>
                                                        <option value="REJECTED" <?php if($batch_request_filter_data['BATCH_STATUS']=='REJECTED') echo "selected"?>>REJECTED</option> 
                                                    </select>
                                                     
                                                 </div>
                                                 
                                                <div class="form-group">
                                                
                                                    <button type="submit" name="submit" value="submit" id="submit" class="btn btn-primary" style="float:right">Search</button>
                                                    
                                                </div>  
                                                
                                              </div>    
                                                
                                            
                                              <div class="col-md-6">
                                            
                                               
                                                <div class="form-group">
                                                    <label>Request Date from:</label>
                                                    <input type="text" name="FROMDATE"  id="FROMDATE" value="<?php echo isset($batch_request_filter_data['FROMDATE']) ? $batch_request_filter_data['FROMDATE'] : '' ?>" class="form-control enquiry_date" placeholder="dd/mm/yyyy">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Request Date to:</label>
                                                    <input type="text" name="TODATE"  id="TODATE" value="<?php echo isset($batch_request_filter_data['TODATE']) ? $batch_request_filter_data['TODATE'] : '' ?>" class="form-control enquiry_date" placeholder="dd/mm/yyyy">
                                                </div>  
                                                
                                                
                                                
                                                
                                                
                                                <div class="form-group">
                                                
                                                    
                                                    <button type="submit" name="reset" value="reset" id="reset" class="btn btn-primary" style="float:left">Reset</button>
                                                </div>
                                                
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><br><br>
        
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                           
                            <div class="panel-body table-responsive">
                            <form action="#">
                                <table class="table table-bordered table-condensed table-stripped">
                               <thead>
                                  <tr>
                                    
                                    <th >Admission ID</th>
                                    <th >Student Name</th>
                                    <th >Student Contact</th>
                                    <th >Student Email</th>
                                    <th >Centre</th>
                                    <th >Course</th>
                                    <th >Preferred Timing 1</th>
                                    <th >Preferred Timing 2</th>
                                    <th >Preferred Location 1</th>
                                    <th >Preferred Location 2</th>                                  
                                    <th >Subject</th>
                                    <th >Request Date</th>
                                    <th style="width:200px;">Batch Status</th>
                                    <th >Remark</th>
                                    <th >Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                
                            
                            <?php 
                            if(!empty($batchRequest))
                            {
                            foreach ($batchRequest as $data) 
                            {                                 
                            ?>
                                 
                                 <tr>
                                        
                                        <td><?php echo $data['ADMISSION_ID']?></td>
                                        <td><?php echo $data['STUDENT NAME']?></td>
                                        <td><?php echo $data['STUDENT CONTACT']?></td>
                                        <td><?php echo $data['STUDENT_EMAIL']?></td>
                                        <td><?php echo $data['CENTRE_NAME']?></td>
                                        <td><?php echo $data['COURSE_TAKEN']?></td>

                                         <td><?php echo $data['PREFERRED TIMING 1']?></td>
                                         <td><?php echo $data['PREFERRED TIMING 2']?></td>
                                         <td><?php echo $data['PREFERRED_LOCATION1']?></td>
                                         <td><?php echo $data['PREFERRED_LOCATION2']?></td>
                                         <td><?php echo $data['SUBJECT']?></td>
                                                                                 
                                         <td><?php echo $data['REQUESTDATE']?></td>
                                         <td><select name="BATCH_STATUS" style="width:150px;">
                                                        
                                                        <option value="IN PROCESS" <?php if($data['BATCH_REQUIREMENT_STATUS'] =="IN PROCESS") echo"selected" ?>>IN PROCESS</option>
                                                        <option value="APPROVED" <?php if($data['BATCH_REQUIREMENT_STATUS'] =="APPROVED") echo"selected" ?>>APPROVED</option>
                                                        <option value="ON HOLD" <?php if($data['BATCH_REQUIREMENT_STATUS'] =="ON HOLD") echo"selected" ?>>ON HOLD</option>
                                                        <option value="REJECTED" <?php if($data['BATCH_REQUIREMENT_STATUS'] =="REJECTED") echo"selected" ?>>REJECTED</option>   
                                             </select> 
                                         </td>
                                          <td><?php echo $data['BATCH_REQUIREMENT_REMARKS']?></td>
                                         
                                        <td><a href="#" alt="Show me suggested batches">Show Suggested Batches</a></td>
                                    
                                    
                                </tr>
<?php }}
else
{
    echo "<tr><td colspan=18>Sorry,No Records Available</td></tr>";
}
?>                          
                                </tbody>
                                
                                </table>
                                <?php echo $pagination; ?>
                                </form>
                            </div>
                            
                            <br><br>
                        <br></div><br>
                    </div>
                </div>
            </section>

        