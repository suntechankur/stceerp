<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
<div class="create_batch_form">
<div id="box">
<h2>Batch Search</h2></div>
<div class="row">
<div class="col-sm-12">
<div class="panel panel-default">
<div class="panel-heading">
<!-- start message area -->

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('newBatchId')) { ?>
<div class="alert alert-success">
<?php echo "New Batch ID Generated: " . $this->session->flashdata('newBatchId'); ?>
</div>
<?php } ?>


<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info1')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
</div>
<?php } ?>

<?php if($this->session->flashdata('warning')) { ?>
<div class="alert alert-warning">
<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php } ?>

<!-- End message area -->

</div>
<div class="panel-body">
<?php echo form_open(); ?>
<div class="col-md-6">
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-4">
<label>Center:</label>
<select name="CENTRE_ID" class="form-control">
<option value=''>Please Select</option>
<?php foreach($centres as $centre){?>
<option value="<?php echo $centre['CENTRE_ID']; ?>"
	<?php if(isset($batch_filter_data['CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $batch_filter_data['CENTRE_ID']) ? ' selected="selected"' : '';}?>><?php echo $centre['CENTRE_NAME']; ?>

</option>
<?php } ?>
</select>

</div>
<div class="col-lg-4">
<label>From (Start Date):</label>
<input type="text" name="STARTDATE" id="STARTDATE" value="<?php if(isset($batch_filter_data['STARTDATE'])){ echo $batch_filter_data['STARTDATE']; }?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY"> </div>
<div class="col-lg-4">
<label>To (Start Date):</label>
<input type="text" name="TODATE" id="TODATE" value="<?php if(isset($batch_filter_data['TODATE'])){ echo $batch_filter_data['TODATE']; }?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY"> </div>
</div>
</div>
</div>
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-4">
<label>Subject:</label>
<select name="SUBJECT" class="form-control getduration" id="languages">
<option value=''>Please Select</option>
<?php
foreach($courses as $course){ ?>
<option value="<?php echo $course['COURSE_ID']; ?>"<?php if(isset($batch_filter_data['SUBJECT'])){ echo ($course['COURSE_ID'] == $batch_filter_data['SUBJECT']) ? ' selected="selected"' : '';}?>><?php echo $course['COURSE_NAME']; ?></option>
<?php } ?>
</select>

</div>
<div class="col-lg-4">
<label>From (End Date):</label>
<input type="text" name="FROMENDDATE" value="<?php echo isset($batch_filter_data['FROMENDDATE']) ? $batch_filter_data['FROMENDDATE'] : '' ?>" id="FROMENDDATE" class="form-control enquiry_date" placeholder="DD/MM/YYYY">
</div>
<div class="col-lg-4">
<label>To (End Date):</label>
<input type="text" name="TOENDDATE" value="<?php echo isset($batch_filter_data['TOENDDATE']) ? $batch_filter_data['TOENDDATE'] : '' ?>" id="TOENDDATE" class="form-control enquiry_date" placeholder="DD/MM/YYYY">
</div>
</div>
</div>
</div>

</div>
<div class="col-md-6">
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-4">
<label>First Name:</label>
<input class="form-control" type="text" value="<?php if(isset($batch_filter_data['FIRSTNAME'])){ echo $batch_filter_data['FIRSTNAME']; }?>" maxlength="50" name="FIRSTNAME" id="FIRSTNAME"> </div>
<div class="col-lg-4">
<label>Last Name:</label>
<input class="form-control" type="text" value="<?php if(isset($batch_filter_data['LASTNAME'])){ echo $batch_filter_data['LASTNAME']; }?>" name="LASTNAME" id="LASTNAME" maxlength="50"> </div>
<div class="col-lg-4">
<label>Batch id:</label>
<input type="text" name="BATCHID" value="<?php echo isset($batch_filter_data['BATCHID']) ? $batch_filter_data['BATCHID'] : '' ?>" placeholder="" class="form-control">
</div>
</div>
</div>
</div>
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-6">
<label>Batch Status:</label>
<select class="form-control" name="BATCH_STATUS">
<option value="">Select Batch Status</option>
<option value="C" <?php if($batch_filter_data['BATCH_STATUS']=='C') echo "selected"?>>Completed</option>
<option value="P" <?php if($batch_filter_data['BATCH_STATUS']=='P') echo "selected"?>>Pursuing</option>
<option value="H" <?php if($batch_filter_data['BATCH_STATUS']=='H') echo "selected"?>>Hold</option>
</select>
</div>

<div class="col-lg-6">
<label>Days:</label>
<select name="DAYS" class="form-control">
<option value="">Select Day</option>
<option  <?php if($batch_filter_data['DAYS']=='T/Th/S/') echo "selected"?> value="T/Th/S/">T/Th/S/</option>
<option  <?php if($batch_filter_data['DAYS']=='M/W/F/') echo "selected"?> value="M/W/F/">M/W/F/</option>
<option <?php if($batch_filter_data['DAYS']=='S/') echo "selected"?> value="S/">S/</option>
<option <?php if($batch_filter_data['DAYS']=='M/T/W/Th/F/S/') echo "selected"?> value="M/T/W/Th/F/S/" >M/T/W/Th/F/S/</option>
<option <?php if($batch_filter_data['DAYS']=='Daily') echo "selected"?> value="Daily">Daily/</option>

</select>
</div>
</div>
</div>
</div>

<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-4">
<button type="submit" value="submit" id="submit" class="btn btn-primary" style="float:right">Search</button>
</div>
<div class="col-lg-4">
<button type="submit" name="reset" value="reset" id="reset" class="btn btn-primary" style="float:left">Reset</button>
</div>
<?php if (!empty($batch_filter_data)) { ?>
<div class="col-lg-4">
<a href="<?php echo base_url('BatchMaster/batchreport');?>" class="btn" style="float:left" title="Export to excel"><i class="fa fa-file-excel-o fa-2x text-success"></i></a>
</div>
<?php } ?>
</div>
</div>
</div>
</div>
<?php echo form_close(); ?>
</div>
</div>
</div>
</div>
</div>
<div id="box"> <span class=""><?php echo $pagination; ?></span>

	 		<h2 class="text-center">Batch Search List</h2>

</div>
<div class="panel panel-default">
<!--<div class="panel-heading">Form Elements</div>-->
<div class="panel-body table-responsive">
<table class="table table-bordered table-condensed table-stripped">
<thead>
<tr>
<th>&nbsp;Action&nbsp;</th>
<?php if(IsBatchActionsPermitted){?>
<th><input type="checkbox" id="checkAll"></th>
<?php }?>
<th >Batch ID.</th>
<th >Subject</th>
<th>Centre Name</th>
<th>Faculty Name</th>
<th >Start And Expected End Date</th>
<th >Days</th>
<th >Batch Time</th>
<th >Students</th>
<th >Internal Exam Date</th>
<th >Actual End Date</th>
<th>No. Of Students</th>
<th >Auditors Remark</th>
<th >Batch Status</th>
<th >Batch Status Remark</th>
<th>Grace Date</th>
<th>Internal Exam Marks</th>
<th>Issue Attendance Sheet</th>
</tr>
</thead>
<tbody>
<!--                    <tr><td colspan="18" class="text-center">Sorry Record Not Found</td></tr>							-->
<?php
$count = $this->uri->segment(2) + 1;

foreach($batches as $key=>$data){

?>
<tr>
<td>
<?php $encBatchId = $this->encrypt->encode($data['BATCHID']);
?>
<?php //if(IsBatchActionsPermitted){?>
<a title="Add Students To Batch" href="<?php echo base_url('BatchMaster/addStudentsToBatch/').$encBatchId; ?>"><span class="glyphicon glyphicon-user"></span></a>

&nbsp;
<a title="Update Batch" href="<?php echo base_url('batchMaster/batchupdation/').$encBatchId; ?>"><span class="glyphicon glyphicon-edit"></span></a>

&nbsp;
<a title="Delete Batch" href="<?php echo base_url('batchMaster/batch-delete/').$encBatchId; ?>" onclick="return confirm('Are you sure to delete this Batch ?')"><span class="glyphicon glyphicon-trash"></span></a>

&nbsp; <a title="Import Existing Batch" href="<?php echo base_url('batchMaster/import-existing-batch/').$encBatchId; ?>" target="_blank" class="glyphicon glyphicon-import"></a>
<?php //} ?>
&nbsp;

<a title="Upload Assignments/Projects" href="<?php echo base_url('batchMaster/upload-assignments/').$encBatchId; ?>"><span class="fa fa-upload" style="color:green;font-size:12px;"></span></a>
&nbsp;
<a title="Upload Weekly Test Marks" href="<?php echo base_url('batchMaster/weekly-test/').$encBatchId; ?>"><span class="fa fa-list-ol" style="font-size:12px;"></span></a>
&nbsp;
<a title="Attendance" href="<?php echo base_url('batchMaster/attendance/').$encBatchId; ?>"><span class="glyphicon glyphicon-saved"></span></a>
&nbsp;
<a title="Mark Batch Completion" href="<?php echo base_url('batchMaster/markBatchCompletion/').$encBatchId; ?>"><span class="glyphicon glyphicon-ok"></span></a>
</td>
<?php if(IsBatchActionsPermitted){?>
<td><input type="checkbox"></td>
<?php }?>


<td><?php print $data['BATCHID']?></td>
<td><?php print $data['SUBJECT']?></td>

<td><?php print $data['LOCATION']?></td>
<td><?php print $data['FACULTY'] ? $data['FACULTY'] : $data['FACULTY_NAME'];?></td>
<td><?php print $data['STARTDATE'] ? date('d/m/Y',strtotime($data['STARTDATE'])) : '';?> - <?php print $data['EXPECTEDENDDATE'] ? date('d/m/Y',strtotime($data['EXPECTEDENDDATE'])) : '';?></td>


<td><?php print $data['DAYS']?></td>
<td><?php print $data['TIME']?></td>
<td><ul><?php print $data['STUDENTS'] ?></ul></td>
<td><?php print $data['INTERNALEXAMDATE'] ? date('d/m/Y',strtotime($data['INTERNALEXAMDATE'])) : '';?></td>
<td><?php print $data['ActualEndDate'] ? date('d/m/Y',strtotime($data['ActualEndDate'])) : '';?></td>
<td><?php print $data['NOOFSTUDENTS'] ?></td>

<td><?php print $data['AUDITOR_REMARKS']?></td>

<td><?php print $data['BATCH_STATUS']?></td>
<td><?php print $data['BATCH_STATUS_REMARK']?></td>
<td><?php print $data['GRACE_DATE'] ? date('d/m/Y',strtotime($data['GRACE_DATE'])) : '';?></td>
<td><a href="#" target="_blank">Internal Exam Marks</a></td>
<td><a href="<?php echo base_url('batchMaster/issueAttendenceSheet/'.$this->encrypt->encode($data['BATCHID']).'');?>" target="_blank">Attendance Sheet</a></td>
</tr>
<?php $count++; } ?>
</tbody>
</table>
<br> </div>
<div class="row">
<div class="col-md-4 pull-right">
<?php echo $pagination; ?>
</div>
</div>
</div>
</div>
