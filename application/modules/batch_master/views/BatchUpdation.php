<script type="text/javascript">
$(document).ready(function(){
	$(".EMPLOYEE_ID").on("change",function(){
		if($(this).val() == "other"){
			$("#otherBox").show();
		}
		else{
			$("#otherBox").hide();
		}
	});
});
</script>


<section>
<div class="container">

<div class="gapping">


</div>
<div class="create_batch_form">
<div id="box"> <h2>Batch Update</h2></div>
<div class="row">

<div class="col-sm-12">
<div class="panel panel-default">
<div class="panel-heading">
<!-- start message area -->

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>


<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info1')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
</div>
<?php } ?>

<?php if($this->session->flashdata('warning')) { ?>
<div class="alert alert-warning">
<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php } ?>

<!-- End message area -->

</div>
<div class="panel-body">
<?php foreach($batch as $data){?>
	<div class="col-sm-12">
		<div class="col-md-3">
			<div class="form-group">
				<label>Center Name :</label>
				<?php foreach($centres as $centre){
					if(isset($data['CENTRE_ID'])){
						 if($centre['CENTRE_ID'] == $data['CENTRE_ID']){
						 	echo "<label>".$centre['CENTRE_NAME']."</label>";
						 }
					 }
				  } ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Subject:</label>
				<?php
				foreach($courses as $course){
					if(isset($data['SUBJECT'])){
						 if($course['COURSE_ID'] == $data['SUBJECT']){
							 echo "<label>".$course['COURSE_NAME']."</label>";
						 }
					 }
				  } ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label>Duration:</label>
				<?php
					if($data['DURATION']!='' || $data['SUBJECT']!='') {
					  echo "<label>".$data['DURATION']."</label>";
					}
					else {
						 echo '';
					} ?>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>From: </label>
				<label><?php if($data['STARTTIME']!='') {  echo $data['STARTTIME'];} else { echo '';} ?></label>
				<label>To: </label>
				<label><?php if($data['ENDTIME']!='') {  echo $data['ENDTIME'];} else { echo '';} ?></label>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="col-md-2">
			<div class="form-group">
				<label>Days:</label>
				<?php
				if(isset($data['DAYS']))
				{
					echo "<label>".$data['DAYS']."</label>";
				}
				?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Faculty Centre:</label>
				<?php
				foreach($centres as $centre){
					if(isset($data['CENTRE_ID'])){
						if($centre['CENTRE_ID'] == $data['CENTRE_ID']) {
							echo "<label>".$centre['CENTRE_NAME']."</label>";
						}
					}
				} ?>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>Faculty Name:</label>
				<?php
					foreach($faculties as $faculty){
					 if(isset($data['FACULTY'])){
						  if($faculty['EMPLOYEE_ID'] == $data['FACULTY']){
								echo "<label>".$faculty['EMP_NAME']."</label>";
							}
						}
					} ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Start Date: </label>
				<label><?php
				if($data['STARTDATE']) {
					$STARTDATE = str_replace("-","/",$data['STARTDATE']);
					$STARTDATE = date('d/m/Y', strtotime($STARTDATE));
					echo $STARTDATE;
				} ?></label>
				<label>Expected End Date: </label>
				<label><?php
				if($data['EXPECTEDENDDATE']) {
					$EXPECTEDENDDATE = str_replace("-","/",$data['EXPECTEDENDDATE']);
					$EXPECTEDENDDATE = date('d/m/Y', strtotime($EXPECTEDENDDATE));
					echo $EXPECTEDENDDATE;
				} ?></label>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="col-md-3">
			<div class="form-group">
				<label>Exam Date:</label>
				<label>
				<?php
				if($data['EXPECTEDENDDATE'])
				{
					$INTERNALEXAMDATE = str_replace("-","/",$data['INTERNALEXAMDATE']);
					$INTERNALEXAMDATE = date('d/m/Y', strtotime($INTERNALEXAMDATE));
					echo $INTERNALEXAMDATE;
				}
				?>
			</label>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<label>Total Sessions:</label>
				<label><?php if($data['SESSION']!='') { echo $data['SESSION'];}?></label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Attendance Sheet No:</label>
				<label><?php if($data['ATTENDANCESHEETNO']!='') { echo $data['ATTENDANCESHEETNO'];}?></label>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>Batch Remarks: </label>
				<label><?php if($data['BATCH_STATUS_REMARK']!='') { echo $data['BATCH_STATUS_REMARK'];}?></label>
			</div>
		</div>
	</div>
	<hr>
	<br>
<?php } ?>
<?php echo form_open(); ?>

<div class="col-md-6">
<div class="form-group">
<label>Center:</label>
<select name="CENTRE_ID" class="form-control">
	<option value=''>Please Select</option>
	<?php foreach($centres as $centre){?>
<option value="<?php echo $centre['CENTRE_ID']; ?>"><?php echo $centre['CENTRE_NAME']; ?></option>
<?php } ?>									</select>
</div>
<div class="form-group">
<label>Subject:</label>
<select name="SUBJECT" class="form-control getduration" id="languages">
	<option value=''>Please Select</option>
	<?php
foreach($courses as $course){ ?>
<option value="<?php echo $course['COURSE_ID']; ?>"><?php echo $course['COURSE_NAME']; ?></option>
<?php } ?>
</select>

</div>

<div class="form-group">
<label>Duration</label>
<div class="row">

	<div class="col-lg-12"><span class="duration"><input type="text" class="form-control"></span></div>
</div>

</div>



<div class="form-group">
<label>Timing </label>
<div class="row" style="margin-left:-30px;">
	<div class="col-lg-12" style="margin-left:0px;">
		<div class="col-lg-6">
			<label>From: </label>
			<input class="form-control pref_time" type="text" data-format="h:mm a" data-template="hh : mm a" maxlength="8" name="STARTTIME" id="STARTTIME">
		</div>
		<div class="col-lg-6">
			<label>To: </label>
			<input class="form-control pref_time" type="text" data-format="h:mm a" data-template="hh : mm a" maxlength="8" name="ENDTIME" id="ENDTIME">
		</div>
	</div>
</div>
</div>




<div class="form-group">

<label>Days:</label><br/>



<label class="form-check-inline">
<input class="form-check-input" type="checkbox" name="DAYS[]" value="M" id="Mon" onChange="endDate()"> Mon &nbsp;&nbsp;
</label>
<label class="form-check-inline">
<input class="form-check-input" type="checkbox" name="DAYS[]" value="T" id="Tue" onChange="endDate()" >Tue &nbsp;&nbsp;
</label>
<label class="form-check-inline">
<input class="form-check-input" type="checkbox" name="DAYS[]" value="W" id="Wed" onChange="endDate()"> Wed &nbsp;&nbsp;
</label>
<label class="form-check-inline">
<input class="form-check-input" type="checkbox" name="DAYS[]" value="Th" id="Thu" onChange="endDate()"> Thu &nbsp;&nbsp;
</label>
<label class="form-check-inline">
<input class="form-check-input" type="checkbox" name="DAYS[]" value="F" id="Fri" onChange="endDate()"> Fri &nbsp;&nbsp;
</label>
<label class="form-check-inline">
<input class="form-check-input" type="checkbox" name="DAYS[]" value="S" id="Sat" onChange="endDate()"> Sat &nbsp;&nbsp;
</label>
<label class="form-check-inline">
<input class="form-check-input" type="checkbox" name="DAYS[]" value="Su" id="Sun" onChange="endDate()"> Sun
</label>

</div>
<div class="form-group">

<label>Faculty Centre:</label>
<select name="CENTRE_NAME" id="CENTRE_NAME" class="form-control CENTRE_NAME">
	<option value=''>Please Select</option>
	<?php foreach($centres as $centre){?>
<option value="<?php echo $centre['CENTRE_ID']; ?>"><?php echo $centre['CENTRE_NAME']; ?></option>
<?php } ?>
</select>

</div>
<div class="form-group"  id="EMPLOYEE_ID">
<label>Select Faculty:</label>
<select name="EMPLOYEE_ID" class="form-control EMPLOYEE_ID">
</select>
</div>
<div id="otherBox" style="display:none;">
	<label>Faculty Name :</label>
	<input name="FACULTY_NAME" type="text" class="form-control"/>
</div>

<div class="form-group" id="input">

</div>


</div>

<div class="col-md-6">
<div class="form-group">
<div class="row" style="margin-left:-30px;">
	<div class="col-lg-12" style="margin-left:0px;">
		<div class="col-lg-6">
			<label>Start Date:</label>

            <input type="text" name="STARTDATE" id="STARTDATE" autocomplete="off" class="form-control enquiry_date" maxlength="10" onChange="endDate()" placeholder="DD/MM/YYYY" disabled="disabled">

		</div>
		<div class="col-lg-6" style="float:right">
			<label>Expected End Date:</label>
			<input type="text" name="EXPECTEDENDDATE" id="EXPECTEDENDDATE" readonly="readonly" class="form-control" placeholder="DD/MM/YYYY">
		</div>
	</div>
</div>
</div>

<div class="form-group">
<label>Exam Date:</label>
<input type="text" name="INTERNALEXAMDATE" id="INTERNALEXAMDATE" readonly="readonly" class="form-control" placeholder="DD/MM/YYYY">
</div>

<div class="form-group">
<label>Total Sessions:</label>
<input type="Text" name="SESSION" id="SESSION" readonly="readonly" class="form-control" placeholder="Sessions">
</div>

<div class="form-group">
<label>Attendance Sheet No:</label>
<input type="Text" name="ATTENDANCESHEETNO" maxlength="100" id="ATTENDANCESHEETNO"  class="form-control">
</div>

<div class="form-group">
<label>Batch Remarks:</label>
<textarea class="form-control" name="BATCH_STATUS_REMARK" style="height:104px;"></textarea>
</div>
<br />
<button type="submit" class="btn btn-primary" >Update</button>  &nbsp;
<button type="reset"  style="float:none">Reset</button>
</div>
</form>

</div>
</div>
</div>
</div>
</div>
</div>
</section>

<script type="text/javascript" src="<?php echo base_url('resources/').'js/getEndDate.js';?>"></script>
