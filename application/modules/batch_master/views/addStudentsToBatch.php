
<section id="login">
<div class="container">
<div class="row">
	<?php foreach($batch_details as $batch);{ ?>
	<div class="col-md-12 col-sm-12">
	<div id="box"><h2>Batch Details</h2></div>
	<div class="panel panel-default">
	<!--<div class="panel-heading">Form Elements</div>-->
	<div class="panel-body">

	<div class="col-sm-12">
		<div class="col-md-3">
			<div class="form-group">
				<label>Batch ID:</label>
				<label id="batchId"><?php echo $batch['BATCHID'];
				?></label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Centre:</label>
				<label><?php echo $batch['CENTRE_NAME'];?></label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Timings:</label>
				<label><?php echo $batch['STARTTIME']." - ".$batch['ENDTIME'];?></label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Start Date:</label>
				<label><?php echo date('d/m/Y',strtotime($batch['STARTDATE']));?></label>
			</div>
		</div>
		</div>
		<div class="col-sm-12">
		<div class="col-md-3">
			<div class="form-group">
				<label>Subject:</label>
				<label><?php echo $batch['COURSE_NAME']; ?></label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Days:</label>
				<label><?php echo $batch['DAYS'];?></label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Faculty:</label>
				<label>
					<?php
						if($batch['FACULTY_NAME'] == ""){
							echo $batch['EMP_NAME'];
						}
						else{
							echo $batch['FACULTY_NAME'];
						}
					?>
				</label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Expected End Date:</label>
				<label><?php echo date('d/m/Y',strtotime($batch['EXPECTEDENDDATE']));?></label>
			</div>
		</div>
	</div>
	</div>

	</div>
	</div>
</div>
</div>
	<?php } ?>
</section>

<div class="container">
<div class="row">
<?php echo form_open();?>
<div class="col-sm-12"><div id="box"> <h2>Search Students to Add</h2></div>
<div class="panel panel-default">
<!--<div class="panel-heading">Form Elements</div>-->
<div class="panel-body">
<div class="col-sm-12">

		<div class="col-md-3">

			<div class="form-group">
				<label>First Name:</label>
				<input type="text" name="FirstName" class="form-control" value="<?php if(isset($addStudentsToBatchFilter_data['FirstName'])) { echo $addStudentsToBatchFilter_data['FirstName'];}?>" >
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">

			<div class="form-group">
				 <label>Last Name:</label>
				 <input type="Text" name="LastName"  class="form-control" value="<?php if(isset($addStudentsToBatchFilter_data['LastName'])) { echo $addStudentsToBatchFilter_data['LastName'];}?>" >
			</div>


			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Date of Admission from:</label>
				<input type="text" maxlength="10" placeholder="DD/MM/YYYY"  name="DOA_from" id="DOA_from" class="form-control enquiry_date" value="<?php if(isset($addStudentsToBatchFilter_data['DOA_from'])) { echo $addStudentsToBatchFilter_data['DOA_from'];}?>" >
			</div>

		</div>
		<div class="col-md-3">


			<div class="form-group">
				<label>Date of Admission to:</label>
				<input type="text" maxlength="10" placeholder="DD/MM/YYYY" name="DOA_to" id="DOA_to" class="form-control enquiry_date" value="<?php if(isset($addStudentsToBatchFilter_data['DOA_to'])) { echo $addStudentsToBatchFilter_data['DOA_to'];}?>" >
			</div>

		</div>

</div>

<div class="col-sm-12">

	<div class="col-md-3">
		<div class="form-group">
				<label>Admission ID:</label>
				<input type="Text" name="Admission_ID"  class="form-control" value="<?php if(isset($addStudentsToBatchFilter_data['Admission_ID'])) { echo $addStudentsToBatchFilter_data['Admission_ID'];}?>" >
			</div>
	</div>

	<div class="col-md-3">
	<label>Centre:</label>
			  <select class="form-control" name="CENTRE_ID">
						<option value=''>Please Select</option>
                    <?php foreach($centres as $centre){?>
                    <option value="<?php echo $centre['CENTRE_ID']; ?>"<?php if(isset($addStudentsToBatchFilter_data['CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $addStudentsToBatchFilter_data['CENTRE_ID']) ? ' selected="selected"' : '';}?>><?php echo $centre['CENTRE_NAME']; ?></option>
                    <?php } ?>
					</select>

	</div>

	<div class="col-md-3">
			<div class="form-group">
				<label> </label>
				<br>
				<button type="submit" class="btn btn-primary">Search</button>

			</div>
	</div>

	<div class="col-md-3">
			<div class="form-group">
				<label> </label>
				<br>

				 <button type="submit" name="reset" value="reset" id="reset" class="btn btn-primary">Reset</button>
			</div>
	</div>

</div>
</div>
</div>
</form>
</div>
</div>
</div>
<br />

<?php
if(empty($students))
{
// echo "<pre>Please Search First to show students...</pre>";
// echo "<tr><td colspan=18>No Students</td></tr>";
}
else
{
?>
<div class="container">
<div class="row">

<div class="col-sm-12"><div id="box"> <h2>Searched <?php // echo $NumberOfStudents;?> Students</h2></div>

<div class="panel panel-default">
<!--<div class="panel-heading">Form Elements</div>-->
<div class="panel-body">

<div id="">
<div class="panel-body table-responsive">
<?php echo form_open();?>

<table  class="table table-bordered table-condensed table-stripped">
<thead>
<tr>

<th><input type="checkbox" class="sel_all" name="ADMISSION_ID" id="checkAll-st"></th>
<th >ADMISSION ID</th>
<th >STUDENT NAME</th>
<th>ADMISSION DATE</th>
<th >COURSE TAKEN</th>
<th >CENTRE NAME</th>
<th >MOBILE NO.</th>

<th >FEES</th>
<th >FEES PAID</th>
<th >COURSE COMPLETED</th>
<th >COURSE PURSUING</th>

</tr>
</thead>
<?php if($this->session->userdata('addStudentsToBatchFilter_data')){?>
<tbody>
<?php
foreach($students as $data)
{?>
<tr>
<td><input type="checkbox" class="sel_data" name="ADMISSION_ID[]" id="ADMISSION_ID[]" value="<?php echo $data['ADMISSION_ID']?>"></td>
<td><?php echo $data['ADMISSION_ID']?></td>
<td><?php echo $data['STUDENT_NAME']?></td>
<td><?php echo date('d/m/Y',strtotime($data['ADMISSION_DATE'])); ?></td>

<td>
	<?php
		if($data['COURSE'] != ""){
			echo $data['COURSE'];
		}
		else{
			echo $data['COURSE_TAKEN'];
		}

		$courses_arr = explode(",",$data['COURSE']);
				 $courseNames = '';
				 //echo '<ul class="list-group">';
					for($j=0;$j<count($courses);$j++){
							if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
										$courseNames .= ','.$courses[$j]['COURSE_NAME'];
									$data['COURSE'] = trim($courseNames,",");
								}

					}

		$modules_arr = explode(",",$data['MODULE']);
				 $moduleNames = '';
				 //echo '<ul class="list-group">';
					for($j=0;$j<count($modules);$j++){
							if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
										$moduleNames .= ','.$modules[$j]['MODULE_NAME'];
									$data['MODULE'] = trim($moduleNames,",");
								}

					}
				 //echo '</ul>';
	 $data['COURSE'] = trim($data['MODULE'].",".$data['COURSE'],",");

echo $data['COURSE'];

?>
</td>
<td><?php echo $data['CENTRE_NAME']?></td>
<td><?php echo $data['MOBILE_NO']?></td>

<td><?php echo $data['FEES']?></td>
<td><?php echo $data['FEES_PAID']?></td>
<td><?php echo $data['COURSE_COMPLETED'];?></td>
<td><?php echo $data['COURSE_PERSUING'];?></td>

</tr>
<?php } ?>
</tbody>
<?php }?>
</table>
<button class="addSelectedStudentsToBatch" style="width:300px;">Add Selected Students<br></button><br>
</div><br>
</form>
</div><br><br>
<br></div><br>
</div>
</div>
</div>
</div>
<?php } ?>
<br>

<div class="col-sm-12"><div id="box"> <h2>Existing Students In Batch</h2></div>
<div class="panel panel-default">
<div class="panel-heading">
<?php //if($_REQUEST['AddStudentsToBatchmsg']) { echo $_REQUEST['AddStudentsToBatchmsg']; } ?>
</div>
<!--<div class="panel-heading">Form Elements</div>-->
<div class="panel-body">


<div id="">
<div class="panel-body table-responsive"  style="height: 319px;">

<?php echo form_open();?>
<table  class="table table-bordered table-condensed table-stripped">
<thead>
<tr>

<th><input type="checkbox" class="sel_all" name="Batch_Std_ID" id="checkAll"></th>
<th >ADMISSION ID</th>
<th >STUDENT NAME</th>
<th>ADMISSION DATE</th>
<th >COURSE TAKEN</th>
<th >CENTRE NAME</th>
<th >FEES</th>
<th >FEES PAID</th>
<th >MOBILE NO.</th>

</tr>
</thead>
<tbody>

<?php

if(empty($existingStudentsInBatch))
{
echo "<tr><td colspan=18>No Students</td></tr>";
}
else
{

foreach($existingStudentsInBatch as $data)
{


?>
<tr>

<td><input type="checkbox" class="sel_data" name="Batch_Std_ID[]" id="Batch_Std_ID[]" value="<?php echo $data['Batch_Std_ID']?>"></td>
<td><?php echo $data['ADMISSION_ID']?></td>
<td><?php echo $data['STUDENT_NAME']?></td>
 <td><?php echo $data['ADMISSION_DATE']?></td>
 <td>
	 <?php
				$courses_arr = explode(",",$data['COURSE_TAKEN']);
						 $courseNames = '';
						 //echo '<ul class="list-group">';
							for($j=0;$j<count($courses);$j++){
									if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
												$courseNames .= ','.$courses[$j]['COURSE_NAME'];
											$data['COURSE_TAKEN'] = trim($courseNames,",");
										}

							}

				$modules_arr = explode(",",$data['MODULE_TAKEN']);
						 $moduleNames = '';
						 //echo '<ul class="list-group">';
							for($j=0;$j<count($modules);$j++){
									if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
												$moduleNames .= ','.$modules[$j]['MODULE_NAME'];
											$data['MODULE_TAKEN'] = trim($moduleNames,",");
										}

							}
						 //echo '</ul>';
			 $data['COURSE_TAKEN'] = trim($data['MODULE_TAKEN'].",".$data['COURSE_TAKEN'],",");

	  echo $data['COURSE_TAKEN'];?></td>

<td><?php echo $data['CENTRE_NAME']?></td>
 <td><?php echo $data['FEES']?></td>
<td><?php ?></td>
<td><?php echo $data['MOBILE_NO']?></td>

</tr>
<?php } }?>
</tbody>

</table>

<br>

<button class="del_selected" style="width:300px;">Remove Selected Students<br></button><br>
</form>

</div>

</div>

</div>
</div><!-- /.col-->
</div>
<!-- /.row -->
