<section>
				
				<div class="container">
					<div class="gapping"></div>
					<div class="create_batch_form">
						<div id="box"> <h2><?php  echo "Upload Assignment"; ?></h2></div>
						<div class="row">			
                        <?php echo form_open_multipart();?>
							<div class="col-sm-12">
								<div class="panel panel-default">
                                 <div class="panel-heading">
                                 	<!-- start message area -->

            <?php if($this->session->flashdata('danger')) { ?>
                        <div class="alert alert-danger">
              <strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
              <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php } ?>
                             
                        
            <?php if($this->session->flashdata('failed')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
            </div>
            <?php } ?>
  
            <?php if($this->session->flashdata('info1')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('warning')) { ?>
                        <div class="alert alert-warning">
              <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
            </div>
            <?php } ?>

 <!-- End message area --> 
                                 </div>
									<div class="panel-body">
                                    
										<div class="col-sm-3">
											
											<div class="form-group">
												<label>Subject:</label>
												<select name="SUBJECT" class="form-control" id="languages">
												<option value=''>Please Select</option>
													<?php 
                                            foreach($courses as $course){ ?>
                                            <option value="<?php echo $course['COURSE_ID']; ?>"><?php echo $course['COURSE_NAME']; ?></option>
                                            <?php } ?>
												</select>

                                                
											</div>	
                                            </div>
                                            
                                            <div class="col-sm-3">
											
											<div class="form-group">
												<label>Assignment:</label>
												<select name="ASSIGNMENT_NO" class="form-control" id="ASSIGNMENT_NO">
                                                <?php
												for($i=1; $i<26; $i++)
												{
												?>
													<option value="<?php print $i?>"><?php print $i?></option>
                                                <?php } ?>
												</select>
                                                
											</div>	
                                            </div>
                                            
                                            <div class="col-sm-3">
                                            <div class="form-group">
												<label>Lecture From:</label>
												<select name="LECTURE_FROM" class="form-control" id="LECTURE_FROM">
                                                <?php
												for($i=1; $i<81; $i++)
												{
												?>
													<option value="<?php print $i?>"><?php print $i?></option>
                                                <?php } ?>
												</select>
											</div>
														
											</div>	
                                            <div class="col-sm-3">
                                            <div class="form-group">
												<label>Lecture To:</label>
												<select name="LECTURE_TO" class="form-control" id="LECTURE_TO">
                                                <?php
												for($i=1; $i<81; $i++)
												{
												?>
													<option value="<?php print $i?>"><?php print $i?></option>
                                                <?php } ?>
												</select>
											</div>
														
											</div>	
                                           
                                            <div class="col-sm-6">
                                            <div class="form-group">
												<label>Upload Assignment (PDF,JPEG,JPG,PNG):</label>
												<input type="file" name="assignment" class="form-control" />
											</div>			
											</div>
                                            
                                                <div class="col-sm-6">
                                                <div class="form-group">
                                                <br />
                                                <button type="submit" class="btn btn-primary" >Upload</button>  &nbsp;
                                                <button type="reset"  style="float:none">Reset</button>
                                                </div>
                                                </div>
										
                            
                            </form>	
                             										
									
								</div>
							</div>
						</div>
					</div>
				</div>
		</section>
        <section>
				<div class="container">
                <div id="box"> <h2><?php  echo "Uploaded Assignment"; ?></h2></div>
					<div class="row">
						<div class="col-sm-12">
                        <div class="panel panel-default">
							<div class="panel-body table-responsive">
							
								<table class="table table-striped table-hover">
									<tr>
										
                                        <th style="width:100px;">Subject</th>
                                        <th style="width:100px;">Assignment No</th>
                                        <th style="width:100px;">Lectures</th>
										<th style="width:100px;">Uploaded by</th>
                                        <th style="width:100px;">UPLOAD DATE</th>
									</tr>
                                    
									<?php foreach($assignments as $data)
       								{?>
									<tr>
										
                                        <td><?php echo $data['COURSE_NAME'];?></td>
                                        <td><?php echo $data['ASSIGNMENT_NO'];?></td>
                                        <td><?php echo $data['LECTURE_FROM'];?> - <?php echo $data['LECTURE_TO'];?></td>
                                        
										<td><?php echo $data['EMP_NAME'];?></td> 
                                        <td><?php echo $data['UPLOAD_DATE'];?></td>
										
									</tr>
									<?php } ?>
								</table>
							</div>
                            </div>
						</div>
						<?php echo $pagination; ?>
					</div>
				</div>
			</section>
			