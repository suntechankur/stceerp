
<script type="text/javascript" src="<?php echo base_url('resources/js/'); ?>attendance_custom.js"></script>
<body>
<section id="login">
	<div class="container">
		<div class="row">
			<?php foreach($batch_details as $batch);{
			?>

                <div class="col-md-12 col-sm-12">
					<div id="box"><h2>Batch Details</h2></div>
						<div class="panel panel-default">
								<!--<div class="panel-heading">Form Elements</div>-->
							<div class="panel-body">

								<div class="col-md-4">
									<div class="form-group">
										<label>Batch ID:</label>
										<label><?php echo $batch['BATCHID'];
											?></label>
										<input type="hidden" id="BATCHID" value="<?php echo $batch['BATCHID'];?>">
									</div>

									<div class="form-group">
										<label>Centre:</label>
										<label><?php echo $batch['CENTRE_NAME'];
										?></label>
									</div>

									<div class="form-group">
										<label>Timings:</label>
										<label><?php echo $batch['STARTTIME']." - ".$batch['ENDTIME'];?></label>
										<input type="hidden" id="STARTTIME" value="<?php echo $batch['STARTTIME'];?>">
										<input type="hidden" id="ENDTIME" value="<?php echo $batch['ENDTIME'];?>">
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label>Start Date:</label>
										<label><?php echo date('d/m/Y',strtotime($batch['STARTDATE']));?></label>
									</div>

									<div class="form-group">
										 <label>Subject:</label>
										 <label><?php echo $batch['COURSE_NAME']; ?></label>
									</div>
									<div class="form-group">
										<label>Days:</label>
										<label><?php echo $batch['DAYS'];?></label>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label>Faculty:</label>
										<label><?php echo $batch['EMP_NAME'];?></label>
									</div>
									<div class="form-group">
										<label>Expected End Date:</label>
										<label><?php echo date('d/m/Y',strtotime($batch['EXPECTEDENDDATE']));?></label>
									</div>
								</div>
							</div>
						</div>
					</div>

					<?php } ?>

                <div class="container">
					<div class="row">
                        <div class="col-md-12 col-sm-12"><div id="box"> <h2>Student Attendance Sheet</h2></div>
							<div class="panel panel-default">
							<div class="panel-body">
							<div id="">
							<div class="panel-body table-responsive">


								<table  class="table table-bordered table-condensed table-stripped">
								<thead>
									<tr>
										<th rowspan="2">ADMISSION ID</th>
			                            <th rowspan="2">STUDENT NAME</th>
			                            <th>LECTURE NO:<span class="glyphicon glyphicon-arrow-right"></span></th>
			                            <?php

			                            foreach($lecture_duration as $data){
			                            }
			                            $count=0;
			                            foreach($attendanceDate as $date){

			                            	echo ("<th>"."<input readonly=readonly class=form-control value=$date[LECTURE_NO_AS_PER_FRM]>"."</th>");
			                            	$count++;
			                            	}
			                            	$counter=0;
			                            	 for($i=$count+1; $i<=$data['DURATION']; $i++){

			                            	 	echo ('<th>'.'<input type=text name=lecture_number[] class=form-control value="" id="lecture_number" style=width:70px;>' .'</th>');
			                            	 	$counter++;
			                            	}?>
			   			                </tr>
			   			                <tr>
			   			                	<th>MOBILE No.</th>
			   			                	<?php
			   			                		$count_date =0;
			   			                	foreach($attendanceDate as $date){?>
			   			                		 <th> <input readonly="readonly" style="width:70px;" placeholder="DD/MM/YYYY" class="enquiry_date add_lecturedate" value ="<?php echo date('d/m/Y',strtotime($date['LECTURE_DATE']));?>" disabled>
			                            		</th>
			                            		<?php
			                            		$count_date++;
	                                    	}

	                                        echo "<input type=hidden id=startVal value='" . $count_date . "' />";
	                                        for($i=$count_date; $i<$data['DURATION']; $i++){
			                            		echo ("<th>" ."<input type=text name='lecture_date[]' style=width:70px; placeholder=DD/MM/YYYY class= enquiry_date add_lecturedate>".

			                            		"</th>");
			                            	}
							            ?></tr>
								</thead>
								<tbody>
								 	<?php
								 		$no_of_entries=0;
									 	$count_new=0;
									 	$newLine=0;
									 	$admin_old=null;
									 	$admin_new=null;
									 	$selectcount=0;
									 	$rowcount=1;
									 	$maxcount=0;
                              foreach($attendance_status as $id){

                                    	$maxcount=$data['DURATION'];
                                    	$no_of_entries=1;
                                    	$admin_old=$admin_new;
	                                    $admin_new=$id['admission_id'];
	                                    if($admin_new!=$admin_old && $admin_old!=null)
	                                    {
	                                    	$rowcount++;
	                                    	for($i=$count_new; $i<$data['DURATION']; $i++){
				                            		 	echo "<td>"
				                            		 	."<input class='select' type=checkbox id='attendancechecked_".$selectcount."'>".
				                            		 	"</td>";
				                            		 	$selectcount++;
				                            		}
	                                    	echo "</tr>";
	                                    	$newLine=0;
	                                    	$count_new=0;

	                                    }
			                                if($newLine==0)
			                                {
			                                  	echo "<tr>";
			                                   	?>
			                                   	<td><?php echo $id['admission_id']?></td>
					                                <td><?php echo $id['STUDENT_NAME']?></td>
					                                <td><?php echo $id['enquiry_mobile_no']?></td>
					                                <input type="hidden" name=admissionid[] value=<?php echo $id['admission_id']?> />
					                            <?php
					                            $newLine=1;
			                                }
	                                           	echo "<td>";
	                                           	if($id['present']=='1'){
	                                           		echo"<input type=checkbox disabled checked>";
	                                           	}
	                                           	else{
	                                           		echo "<input type=checkbox disabled>";
	                                           	}
	  			                            		echo "</td>";
		                            				$count_new++;
			                        	}
			                        	echo "<input type=hidden id=rowcount value='" . $rowcount . "' />";
			                        	echo "<input type=hidden id=maxcount value='" . $maxcount . "' />";
			                        		if($no_of_entries==1){
			                        			for($i=$count_new; $i<$data['DURATION']; $i++){
			                            		 	echo "<td>"
			                            		 	."<input class='select' type='checkbox' id='attendancechecked_".$selectcount."'>".
			                            		 	"</td>";
			                            		 	$selectcount++;
			                            		}

			                            		?>
	                                    </tr>
	                                    <div>
	                                    <?php }
	                                    	if($rowcount==1 && $maxcount==0)
	                                    	{
	                                    		$rowcount=0;
	                                    		$maxcount=$data['DURATION'];
	                                    		$selectcount=0;
	                                    		foreach($new_attendance_status as $id){
	                                    			echo "<tr>";
			                                   		?>
			                                   		<td><?php echo $id['admission_id']?></td>
					                                <td><?php echo $id['STUDENT_NAME']?></td>
					                                <td><?php echo $id['enquiry_mobile_no']?></td>
					                                <input type="hidden" name=admissionid[] value=<?php echo $id['admission_id']?> />
					                            	<?php
	                                    			for($i=0; $i<$data['DURATION']; $i++){
			                            		 	echo "<td>"
			                            		 	."<input class='select' type='checkbox' id='attendancechecked_".$selectcount."'>".
			                            		 	"</td>";
			                            		 	$selectcount++;


			                            		}
			                            		$rowcount++;
		                                    	echo "</tr>";
	                                    		}
	                                    		echo "<div style='display: none;'>";
	                                    			echo "<div id='tocheckpostvalues'>Test</div>";
		                            		 		echo "<input type=text id=rowcount value='" . $rowcount . "' />";
		                        					echo "<input type=text id=maxcount value='" . $maxcount . "' />";
		                        				echo "</div>";
	                                    	}

	                                    ?>
	                                	</div>
								</tbody>
								</table>
								<button id="attendance" name="attendance" class="btn btn-primary center-block" style="width:200px;">Attendance</button>


								<input type=hidden name=lecture_numbers[] />
								<input type=hidden name=lecture_dates[] />
								<input type=hidden name=presents[] />
								<input type=hidden name=adminids[] />

							</div>
  							</div>
						</div>
					</div>
                    </div>
                    </div>
				</div>
</section>
</body>
