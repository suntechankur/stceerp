<?php
$a = ""; $b = "";
$i;
for ($i = 1; $i < 5; $i++)
{
    $a .= rand(0, 9);
}

for ($i = 1; $i < 5; $i++)
{
    $b .= rand(0, 9);
}

$attendancesheetid = $a.$b;
?>

<div>
<div id=Panel1>
<div>
<table style="BORDER-BOTTOM-COLOR: black; BORDER-TOP-COLOR: black; BORDER-RIGHT-COLOR: black; BORDER-LEFT-COLOR: black" id=table1 border=4 rules=none frame=box>
<tbody>
<tr style="HEIGHT: 1px">
<td class=style106 vAlign=bottom align=left></td>
<td vAlign=bottom align=left></td>
<td style="FONT-WEIGHT: bold" vAlign=bottom colSpan=1 ?><span style="display:block;width:208px;word-wrap:break-word;"><?php echo $batch_details[0]['COURSE_NAME'];?></span></td>
<td style="FONT-WEIGHT: bold" class=style150 vAlign=top rowSpan=8 colSpan=1>&nbsp;&nbsp;<IMG style="BORDER-RIGHT-WIDTH: 0px; WIDTH: 409px; BORDER-TOP-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; HEIGHT: 178px; BORDER-LEFT-WIDTH: 0px" id=Image1 src="<?php echo base_url('resources/images/');?>ATTENDANCE_SHEET_LOGO.png"> </td>
<td vAlign=top colSpan=1></td>
<td class=style100 vAlign=bottom colSpan=1></td>
<td style="FONT-WEIGHT: bold" class=style100 vAlign=bottom colSpan=1>&nbsp;&nbsp;&nbsp;&nbsp; <span style="FONT-SIZE: x-large" id=LblSubject4><?php echo $attendancesheetid;?></span> </td></tr>
<tr>
<td class=style106 vAlign=bottom align=left>Subject</td>
<td vAlign=top align=left><STRONG>:</STRONG></td>
<td style="FONT-WEIGHT: bold" class=style105 vAlign=top colSpan=1>________________________</td></td>
<td vAlign=top colSpan=1></td>
<td class=style100 vAlign=top colSpan=1><STRONG>Att. Sheet ID.</STRONG></td>
<td class=style100 vAlign=top colSpan=1><STRONG>:___________________</STRONG></td></tr>
<tr>
<td class=style106 vAlign=bottom align=left>&nbsp;</td>
<td class=style14 vAlign=top align=left>&nbsp;</td>
<td style="FONT-WEIGHT: bold" class=style105 vAlign=bottom colSpan=1><span id=LblSubject0 style="display:block;width:208px;word-wrap:break-word;"><?php echo $batch_details[0]['STARTTIME'].' - '.$batch_details[0]['ENDTIME'].' ('.$batch_details[0]['DAYS'].')';?>)</span> </td>
<td style="FONT-WEIGHT: bold" vAlign=top colSpan=1>&nbsp;</td>
<td class=style100 vAlign=bottom colSpan=1>&nbsp;</td>
<td style="FONT-WEIGHT: bold" class=style100 vAlign=bottom colSpan=1>&nbsp;&nbsp;&nbsp;&nbsp; <span id=LblSubject5><?php echo $batch_details[0]['BATCHID'];?></span> </td></td></tr>
<tr>
<td class=style106 vAlign=top align=left>Days/Timings </td>
<td class=style14 vAlign=top align=left><STRONG>:</STRONG></td>
<td style="FONT-WEIGHT: bold" class=style105 vAlign=top colSpan=1>________________________</td>
<td style="FONT-WEIGHT: bold" vAlign=top colSpan=1>&nbsp;</td>
<td class=style100 vAlign=top colSpan=1><STRONG>eMIS Batch ID.</STRONG></td>
<td class=style100 vAlign=top colSpan=1><STRONG>:___________________</STRONG></td></td></tr>
<tr>
<td vAlign=bottom align=left>&nbsp;</td>
<td vAlign=bottom align=left>&nbsp;</td>
<td style="FONT-WEIGHT: bold" vAlign=bottom colSpan=1><span id=LblSubject1><?php echo date('d/m/Y',strtotime($batch_details[0]['STARTDATE']));?></span> </td>
<td style="FONT-WEIGHT: bold" vAlign=top colSpan=1></td>
<td vAlign=bottom colSpan=1>&nbsp;</td>
<td style="FONT-WEIGHT: bold" class=style100 vAlign=bottom>&nbsp;&nbsp;&nbsp;&nbsp; <span id=LblSubject6 style="display:block;width:165px;word-wrap:break-word;"><?php echo $batch_details[0]['EMP_NAME'];?></span> </td></tr>
<tr>
<td class=style92 vAlign=bottom align=left>Start Date </td>
<td class=style93 vAlign=bottom align=left><STRONG>:</STRONG> </td>
<td style="FONT-WEIGHT: bold" class=style94 vAlign=top colSpan=1>________________________</td>
<td style="FONT-WEIGHT: bold" class=style95 vAlign=top colSpan=1></td>
<td class=style102 vAlign=bottom colSpan=1>Faculty Name</td>
<td style="FONT-WEIGHT: bold" class=style102 vAlign=top>:___________________</td></tr>
<tr>
<td class=style3 vAlign=bottom align=left>&nbsp;</td>
<td class=style8 vAlign=bottom align=left>&nbsp;</td>
<td style="FONT-WEIGHT: bold" class=style12 vAlign=bottom><span id=LblSubject2><?php echo date('d/m/Y',strtotime($batch_details[0]['EXPECTEDENDDATE']));?></span> </td>
<td style="FONT-WEIGHT: bold" vAlign=top colSpan=1>&nbsp;</td>
<td class=style103 vAlign=bottom>&nbsp;</td>
<td style="FONT-WEIGHT: bold" class=style103 vAlign=bottom align=left>&nbsp;&nbsp;&nbsp;&nbsp; <span id=LblSubject7><?php echo $batch_details[0]['CENTRE_NAME'];?></span> </td></tr>
<tr>
<td class=style3 vAlign=bottom align=left>Exp. End Date </td>
<td class=style8 vAlign=bottom align=left><STRONG>:</STRONG> </td>
<td style="FONT-WEIGHT: bold" class=style12 vAlign=top>________________________</td>
<td style="FONT-WEIGHT: bold" vAlign=top colSpan=1>&nbsp;</td>
<td class=style103 vAlign=bottom>Branch Name</td>
<td style="FONT-WEIGHT: bold" class=style103 vAlign=top align=left>:___________________</td></tr>
<tr>
<td colSpan=7>
<div style="TEXT-ALIGN: center">
<table border=2 cellSpacing=0 borderColor=black cellPadding=3 style="width:100%;">
<tbody>
  <tr>
    <th rowSpan=2>Sr.No. </th>
    <th rowSpan=2 width=200>Name Of the Students </th>
    <th colSpan=1>Date </th>
    <th rowSpan=1 colSpan=1></th>
    <th rowSpan=1 colSpan=1></th>
    <th rowSpan=1 colSpan=1></th>
    <th rowSpan=1 colSpan=1>W.T. </th>
    <th rowSpan=1 colSpan=1></th>
    <th rowSpan=1 colSpan=1></th>
    <th rowSpan=1 colSpan=1></th>
    <th rowSpan=1 colSpan=1>W.T. </th>
    <th rowSpan=1 colSpan=1></th>
    <th rowSpan=1 colSpan=1></th>
    <th rowSpan=1 colSpan=1></th>
    <th rowSpan=1 colSpan=1>W.T. </th>
    <th rowSpan=1 colSpan=1></th>
    <th rowSpan=1 colSpan=1></th>
    <th rowSpan=1 colSpan=1></th>
    <th rowSpan=1 colSpan=1>W.T. </th>
    <th rowSpan=1 colSpan=1></th>
    <th rowSpan=1 colSpan=1></th>
    <th rowSpan=2 width=50 colSpan=1>B.C. <BR>Sign </th>
  </tr>
  <tr>
    <th width=80>Adm-Id</th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
    <th width=35></th>
  </tr>
<?php
$i=1;
foreach($existingStudentsInBatch as $students){?>
  <tr>
    <td><span><?php echo $i++;?></span> </td>
    <td><span><?php echo $students['STUDENT_NAME'];?></span> </td>
    <td><span><?php echo $students['ADMISSION_ID'];?></span> </td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
<?php
}

$countStudent = count($existingStudentsInBatch);
$rowCount= 8 - $countStudent;
for($i=0;$i<$rowCount;$i++){?>
<tr>
<td><BR></td>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th></tr>
<?php
}
?>
<tr>
<tr>
<th colSpan=2 align=left>Lecture NO.(As per FRM) </th>
<th><span id=Repeater1_ctl04_Label1>&nbsp;</span> </th>
<th><span id=Repeater1_ctl04_Label3>&nbsp;</span> </th>
<th><span id=Repeater1_ctl04_Label4>&nbsp;</span> </th>
<th><span id=Repeater1_ctl04_Label5>&nbsp;</span> </th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th></tr>
<tr>
<th colSpan=2 align=left ?>Lecture Start Time </th>
<th><span id=Repeater1_ctl04_Label6>&nbsp;</span> </th>
<th><span id=Repeater1_ctl04_Label9>&nbsp;</span> </th>
<th><span id=Repeater1_ctl04_Label10>&nbsp;</span> </th>
<th><span id=Repeater1_ctl04_Label11>&nbsp;</span> </th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th></tr>
<tr>
<th colSpan=2 align=left ?>Lecture End Time </th>
<th><span id=Repeater1_ctl04_Label12>&nbsp;</span> </th>
<th><span id=Repeater1_ctl04_Label13>&nbsp;</span> </th>
<th><span id=Repeater1_ctl04_Label14>&nbsp;</span> </th>
<th><span id=Repeater1_ctl04_Label15>&nbsp;</span> </th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th></tr>
<tr>
<th colSpan=2 align=left>Faculty Sign </th>
<th><span id=Repeater1_ctl04_Label16>&nbsp;</span> </th>
<th><span id=Repeater1_ctl04_Label17>&nbsp;</span> </th>
<th><span id=Repeater1_ctl04_Label18>&nbsp;</span> </th>
<th><span id=Repeater1_ctl04_Label19>&nbsp;</span> </th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th>
<th></th></tr></tbody></TABLE></div></td></tr>
<tr>
<td class=style50 colSpan=7 align=right><STRONG>________________________________________________________________________________________________________________________________________________</STRONG> </td></tr>
<tr valign?bottom?>
<td colSpan=7>All the undersigned students have confirmed to attend the batch mentioned in this confirmation sheet. Students cannot take a break in between the </td></tr>
<tr valign?bottom?>
<td colSpan=6 align=left>current subject. Students can take break only after completing the current subject. In case,Student takes a break in between&nbsp; the subject, it will be </td>
<td class=style100 colSpan=1 align=right>&nbsp;</td></tr>
<tr valign?bottom?>
<td colSpan=5>considered as a completed subject of their syllabus and they will not be allowed to sit for the same subject again.</td>
<td colSpan=2 align=right>&nbsp;<STRONG>*</STRONG> B.C. Sign- Batch Completion Student Sign.</td></tr>
<tr>
<td colSpan=4>________________________________________________________________________________________</td>
<td colSpan=3 align=right>&nbsp;*W.T. - Weekly Test&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td></tr>
<div></div>
<div></div>
</SCRIPT>
</tbody></div></div>
