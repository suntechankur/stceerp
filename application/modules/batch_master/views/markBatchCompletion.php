
<section id="login">
	<div class="container">
		<div class="row">
				<?php foreach($batch_details as $batch);{	?>
          <div class="col-md-3 col-sm-12">
						<div id="box"><h2>Batch Details</h2></div>
							<div class="panel panel-default">
								<!--<div class="panel-heading">Form Elements</div>-->
								<div class="panel-body">

										<div class="col-md-12">
											<div class="form-group">
												<label>Batch ID:</label>
												<label><?php echo $batch['BATCHID'];
												?></label>
											</div>
											<div class="form-group">
												<label>Centre:</label>
												<label><?php echo $batch['CENTRE_NAME'];?></label>
											</div>
											<div class="form-group">
												<label>Timings:</label>
												<label><?php echo $batch['STARTTIME']." - ".$batch['ENDTIME'];?></label>
											</div>
											<div class="form-group">
												<label>Start Date:</label>
												<label><?php echo date('d/m/Y',strtotime($batch['STARTDATE']));?></label>
											</div>

											<div class="form-group">
												 <label>Subject:</label>
												 <label><?php echo $batch['COURSE_NAME']; ?></label>
											</div>
											<div class="form-group">
												<label>Days:</label>
												<label><?php echo $batch['DAYS'];?></label>
											</div>
											<div class="form-group">
												<label>Faculty:</label>
												<label><?php echo $batch['EMP_NAME'];?></label>
											</div>
											<div class="form-group">
												<label>Expected End Date:</label>
												<label><?php echo date('d/m/Y',strtotime($batch['EXPECTEDENDDATE']));?></label>
											</div>
										</div>

								</div>
							</div>
						</div>
				<?php } ?>


        <div class="col-md-9 col-sm-12"><div id="box"> <h2>Mark Batch Completion</h2></div>
					<div class="panel panel-default">
            <div class="panel-heading">
                            <!-- start message area -->

            <?php if($this->session->flashdata('danger')) { ?>
                        <div class="alert alert-danger">
              <?php echo $this->session->flashdata('danger'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
              <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('newBatchId')) { ?>
                        <div class="alert alert-success">
              <?php echo "New Batch ID Generated: " . $this->session->flashdata('newBatchId'); ?>
            </div>
            <?php } ?>


            <?php if($this->session->flashdata('failed')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('info1')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('warning1')) { ?>
                        <div class="alert alert-warning">
              <strong>Warning!</strong> <?php echo $this->session->flashdata('warning1'); ?>
            </div>
            <?php } ?>

 <!-- End message area -->

								<?php //if($_REQUEST['AddStudentsToBatchmsg']) { echo $_REQUEST['AddStudentsToBatchmsg']; } ?>
            </div>
								<!--<div class="panel-heading">Form Elements</div>-->
						<div class="panel-body">
              <?php echo form_open();?>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-12">
										<div class="col-lg-12">
											<?php
											 switch($batch['BATCH_STATUS']){
												 case 'C':
												 	echo '<span style="color:green;font-weight:bold;">Batch Status: Completed</span>';
													break;
												 case 'H':
												 	echo '<span style="color:violet;font-weight:bold;">Batch Status: Hold</span>';
													break;
												 case 'P':
												 	echo '<span style="color:red;font-weight:bold;">Batch Status: Persuing</span>';
													break;
												 default:
												 	echo 'Status Not here';
											 }?>
										</div>
                    <div class="col-lg-6">
                      <label>Actual End Date (dd/mm/yyyy)*</label>
											<?php
											$batch_status = $batch['BATCH_STATUS'];
											$internal_exam_date = date('Y-m-d', strtotime($batch['INTERNALEXAMDATE']));
						          $todays_date = date('Y-m-d');
											$actioninputs = "";
											$actionrequest = "";

											$employee_ids = array('3170','1338');
											if(($batch_status == 'P') || ($batch_status == 'H')){
												if($internal_exam_date <= $todays_date){
													if (!(in_array($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'], $employee_ids))){
														$actioninputs = "disabled='disabled'";
														$actionrequest = "disabled='disabled'";
													}
												}
											}
											elseif($batch_status == 'C'){
												$actioninputs = "disabled='disabled'";
												$actionrequest = "";
											}	?>
                      <input type="text" name="ActualEndDate" class="form-control enquiry_date batch_completion_date" required="" <?php echo $actioninputs;?>>
											<?php
											$last_lecture_date = "";
											if(!(empty($attendanceDate))){
												$last_lecture_date = $attendanceDate[count($attendanceDate)-1]['LECTURE_DATE'];
												$last_date = str_replace('-', '/', $last_lecture_date);
												$last_lecture_date =  date("d/m/Y", strtotime($last_date) );
											}
											?>
											<input type="hidden" value="<?php echo $last_lecture_date;?>" name="last_lecture_date">
                    </div>
                    <div class="col-lg-6">
                    <br>
                        <button type="submit" name="markBatchCompletion" value="Save" id="Attendance" onclick="return confirm('Do you really want to save changes ?')" class="btn btn-primary" <?php echo $actioninputs;?>>Save</button>
                    </div>
										<div class="col-lg-12">
											<?php if($actionrequest != ""){
												echo "<p style='color:red;font-weight:bold;'>* Batch Completion date Expired <br/>Kindly Contact with Batch Administrator for Extending Date of batch completion. </p>";?>
												<button type="submit" name="sendBatchCompletionRequest" formaction="<?php echo base_url('sendBatchCompletionRequest/'.$this->encrypt->encode($batch['BATCHID']));?>" class="btn btn-primary" style="width:270px;">Send Batch Completion Request</button>
											<?php	}?>
										</div>
                  </div>
                </div>
              </div>

							<div id="">
  							<div class="panel-body table-responsive">
  								<table  class="table table-bordered table-condensed table-stripped">
    								<thead>
    								  <tr>
      									<!-- <th><input type="checkbox" class="sel_all" name="Batch_Std_ID" id="checkAll"></th> -->
      									<th>Admission ID</th>
                        <th>Student Name</th>
                        <th>Total Lecture Presents</th>
                        <th>Attendance marks out of 10</th>
                        <th>Weekly Test Marks</th>
                        <th>Internal Marks New</th>
                        <th>Remarks for students</th>
      								</tr>
    								</thead>
    								<tbody>
                    <?php
                    if(empty($existingStudentsInBatch))	{
    									echo "<tr><td colspan=18>No Students</td></tr>";
    								}
                    else {
                    	foreach($existingStudentsInBatch as $k=>$data) {?>
      								 <tr>
                        <td><input type="hidden" name="ADMISSION_ID[]" value="<?php echo $data['ADMISSION_ID']?>"><?php echo $data['ADMISSION_ID']?></td>
                        <td><?php echo $data['STUDENT_NAME']?></td>
                        <?php
													if(!(empty($totalPresent[0]))){
	                        	foreach($totalPresent[0] as $Present)
	                          {
	                            if($data['ADMISSION_ID']==$Present['ADMISSION_ID']) {
	                            	$BATCH_LECTURE = round($Present['totalPresent']/($totalPresent[1][0]['BATCH_LECTURE_ID'])*10);
	                            	$InternalMarksNew = round(($BATCH_LECTURE+10)/2);
	                            	?>
	                                <td><input type="text" value="<?php	echo $Present['totalPresent'];?>" name="TotalLecturePresents[]" class="form-control" readonly="readonly"></td>
	                                <td><input type="text" value="<?php	echo $BATCH_LECTURE;?>" name="AttendanceMarksOutOf10[]" class="form-control" readonly></td>
	                                <td><input type="text" value="10" name="WeeklyTestMarks[]" class="form-control" readonly></td>
	                                <td><input type="text" value="<?php echo $InternalMarksNew;?>" name="InternalMarksNew[]" class="form-control" readonly></td>
	                                <td><textarea name="RemarksForStudents[]" class="form-control" rows="1" cols="4"></textarea></td>
	                          <?php }
														}
													}
													else{
														$read_only = "readonly";
														$admission_ids = array('3170','1338');										        //
										        if (in_array($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'], $admission_ids)){ $read_only = "";}?>
														<td><input type="text" value="" name="TotalLecturePresents[]" class="form-control" readonly></td>
														<td><input type="text" value="" name="AttendanceMarksOutOf10[]" class="form-control" readonly></td>
														<td><input type="text" value="10" name="WeeklyTestMarks[]" class="form-control" readonly></td>
														<td><input type="text" value="" name="InternalMarksNew[]" class="form-control" <?php echo $read_only;?>></td>
														<td><textarea name="RemarksForStudents[]" class="form-control" rows="1" cols="4"></textarea></td>
													<?php
													} ?>
      								</tr>
    								<?php }
                   } ?>
    							</tbody>
    						</table><br>
              </form>
            </div>
          </div>
        </div>
      </div><!-- /.col-->
    </div><!-- /.row -->
  </div>
</div><br>
</div>
</section>
