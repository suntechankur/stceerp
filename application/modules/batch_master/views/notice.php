


            <section id="">


                <div class="container">

                    <div class="gapping"></div>
                    <!-- start message area -->

            <?php if($this->session->flashdata('danger')) { ?>
                        <div class="alert alert-danger">
              <strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
              <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php } ?>


            <?php if($this->session->flashdata('failed')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('info1')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('warning')) { ?>
                        <div class="alert alert-warning">
              <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
            </div>
            <?php } ?>

 <!-- End message area -->
                    <div id="box"> <h2>Notice for Student Desk</h2></div>

                    <div class="row" >

                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <?php echo form_open_multipart();?>
                                    <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Notice Title:</label>
                                                <input type="Text" name="NOTICE_TITLE" value="<?php if(isset($studentNotice_filter_data['NOTICE_TITLE'])){ echo $studentNotice_filter_data['NOTICE_TITLE']; }?>" id="NOTICE_TITLE"  class="form-control" >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Notice Display Date:</label>
                                                <input type="text" name="NOTICEDISPLAY_DATE" value="<?php if(isset($studentNotice_filter_data['NOTICEDISPLAY_DATE'])){ echo $studentNotice_filter_data['NOTICEDISPLAY_DATE']; }?>" id="NOTICEDISPLAY_DATE"  class="form-control enquiry_date" >
                                            </div>

                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>University:</label>
                                                <select name="UNIVERSITY_ID" id="UNIVERSITY_ID"  class="form-control" >
                                                <option value="">-------- Select ---------</option>
                                                <option value="1" <?php if($studentNotice_filter_data['UNIVERSITY_ID']=='1') echo "selected"?>>YCMOU</option>
                                                <option value="2" <?php if($studentNotice_filter_data['UNIVERSITY_ID']=='2') echo "selected"?>>KSOU</option>
                                                 <option value="4" <?php if($studentNotice_filter_data['UNIVERSITY_ID']=='4') echo "selected"?>>ANNAMALAI</option>
                                                  <option value="5" <?php if($studentNotice_filter_data['UNIVERSITY_ID']=='5') echo "selected"?>>SAIP</option>

                                                </select>
                                            </div>

                                        </div>
                                        <?php /*?><div class="col-md-12">
                                            <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Upload Image:</label>
                                                        <input type="file" name="NOTICE_TITLE" value="<?php print $row['NOTICE_TITLE']?>" id="NOTICE_TITLE"  class="form-control" >
                                                    </div>
                                              </div>
                                              <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Upload PDF:</label>
                                                        <input type="file" name="NOTICE_TITLE" value="<?php print $row['NOTICE_TITLE']?>" id="NOTICE_TITLE"  class="form-control" >
                                                    </div>
                                              </div>
                                        </div><?php */?>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Notice Description:</label>
                                                <textarea class="form-control" id="area5" name="NOTICE_BODY" style="height:150px;"><?php if(isset($studentNotice_filter_data['NOTICE_BODY'])){ echo $studentNotice_filter_data['NOTICE_BODY']; }?></textarea>
                                            </div>

                                            <button type="submit" class="btn btn-primary" >Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div><!-- /.col-->
                        </div><!-- /.row -->
                    </div><!--/.main-->
                </div><br><br><br><br>

                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel-body table-responsive">
                                <table  class="table table-bordered table-condensed table-stripped">
                                    <tr>
                                        <th >Display Date</th>
                                        <th >Notice Title</th>
                                        <th >University</th>
                                        <th >Notice Description</th>
                                        <th >Notice Uploaded on</th>
                                        <th >Action</th>

                                   </tr>
<?php
  if(!empty($notice_list))
 {
   foreach ($notice_list as $data) {
?>

                                    <tr>
                                        <td>
                                        <?php echo date("d/m/Y",strtotime($data['NOTICEDISPLAY_DATE'])); ?>
                                        </td>

                                        <td><?php echo $data['NOTICE_TITLE']?></td>
                                        <td><?php if($data['UNIVERSITY_ID']=="1") { echo 'YCMOU'; } elseif($data['UNIVERSITY_ID']=="2") { echo 'KSOU'; } elseif($data['UNIVERSITY_ID']=="4") { echo 'ANNAMALAI'; } elseif($data['UNIVERSITY_ID']=="5") { echo 'SAIP'; }?></td>
                                        <td style="column-width:800px;"><?php  echo htmlspecialchars_decode(stripslashes($data['NOTICE_BODY']));?></td>
                                        <td>
                                            <?php echo date("d/m/Y",strtotime($data['CREATE_DATE'])); ?>
                                        </td>

                                        <td>
<a href="<?php echo base_url('batchMaster/studentNoticeDelete/').$data['NOTICEID']; ?>" onclick="return confirm('Are you sure to delete ?')"><span class="glyphicon glyphicon-trash"></span></a>

                                       </td>
                                    </tr>
                                    <?php } }
                                    else {
                                        echo "<pre>Record not in database</pre>"; # code...
                                     } ?>
                                </table>
                                <span class=""><?php echo $pagination; ?></span>
                            </div><br><br>
                        </div>
                    </div>
                </div>
            </section>
