<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Exam Search</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
					<!-- start message area -->

					<?php if($this->session->flashdata('success')) { ?>
					<div class="alert alert-success">
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
					</div>
					<?php } ?>

					<!-- End message area -->
						<div class="col-md-12">
							<div class="form-group">
								<?php echo form_open(); ?>
								<div class="col-md-12">
									<div class="form-group">
										<div class="row" style="margin-left:-30px;">
											<div class="col-lg-12" style="margin-left:0px;">
												<div class="col-lg-4">
													<label>Centre Name:</label>
													<select class="form-control" name="centre_id">
														<option selected disabled>Select Centre</option>
														<?php foreach($centres as $centre){?>
															<option value="<?php echo $centre['CENTRE_ID'];?>"><?php echo $centre['CENTRE_NAME'];?></option>
														<?php }?>
													</select>
												</div>
												<div class="col-lg-4">
													<label>First Name:</label>
													<input type="text" class="form-control" name="first_name">
												</div>
												<div class="col-lg-4">
													<label>Last Name:</label>
													<input type="text" class="form-control" name="last_name">
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row" style="margin-left:-30px;">
											<div class="col-lg-12" style="margin-left:0px;">
												<div class="col-lg-4">
													<label>Exam Centre:</label>
													<select class="form-control" name="exam_centre_id">
														<option selected disabled>Select Exam Centre</option>
														<option value="155">HO</option>
														<option value="200">Thane</option>
													</select>
												</div>
												<div class="col-lg-4">
													<label>Date From:</label>
													<input type="text" class="form-control enquiry_date" name="from_exam_date">
												</div>
												<div class="col-lg-4">
													<label>Date To:</label>
													<input type="text" class="form-control enquiry_date" name="to_exam_date">
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row" style="margin-left:-30px;">
											<div class="col-lg-12" style="margin-left:0px;">
												<div class="col-lg-4">
												</div>
												<div class="col-lg-2">
													<button type="submit" name="search_exam_details" class="btn btn-primary">Search</button>
												</div>
												<div class="col-lg-2">
												</div>
												<div class="col-lg-4">
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close();?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="subjectDetails">
		<div id="box">
			<p>&nbsp;</p>
			<h2 class="text-center">Exam Details</h2>
		</div>
		<div class="panel panel-default">
			<div class="panel-body table-responsive">
				<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>Admission Id</th>
					<th>Exam Id</th>
					<th>Student Name</th>
					<th>Subject</th>
					<th>Centre Name</th>
					<th>Exam Centre Name</th>
					<th>Exam Date</th>
					<th>Exam Time</th>
					<th>Assignments Marks</th>
					<th>External Exam Marks</th>
					<th>Exam Given</th>
					<th>Result</th>
					<th>Email Id</th>
					<th>Contact No</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$i = 1;
				foreach($exam_details as $details){?>
					<tr>
						<td><?php echo $details['ADMISSION_ID'];?></td>
						<td><?php echo $details['EXAM_ID'];?></td>
						<td><?php echo $details['Student_name'];?></td>
						<td><?php echo $details['COURSE_NAME'];?></td>
						<td><?php echo $details['STUDY_CENTRE_NAME'];?></td>
						<td><?php echo $details['CENTRE_NAME'];?></td>
						<td>
							<?php $examdate = str_replace('-', '/', $details['EXAM_DATE']);
									echo date("d/m/Y", strtotime($examdate) );?>
						</td>
						<td><?php echo date("h:m A", strtotime($examdate) );?></td>
						<td><?php echo $details['ASSIGNMENT_MARKS'];?></td>
						<td><?php echo $details['EXTERNAL_EXAM_MARKS'];?></td>
						<td><?php if($details['IS_EXAM_GIVEN'] == '1'){echo 'YES';}else{echo 'NO';}?></td>
						<td><?php echo $details['RESULT'];?></td>
						<td><?php echo $details['ENQUIRY_EMAIL'];?></td>
						<td><?php echo $details['CONTACT_DETAILS'];?></td>
						<!-- <td align="center"><a href="<?php //echo base_url("placement/placement-remarks/".$this->encrypt->encode($student['ADMISSION_ID']));?>" target="_blank" title="Add FollowUp" style="color:red"><span class="fa fa-2x fa-phone"></span></a></td> -->
					</tr>
				<?php }?>
			</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
