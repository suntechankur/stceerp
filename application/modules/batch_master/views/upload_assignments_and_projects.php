
<div class="container">
<div class="row">
	<?php
	$batch_id = "";
	foreach($batch_details as $batch){ ?>
	<div class="col-md-12 col-sm-12">
	<div id="box"><h2>Batch Details</h2></div>
	<div class="panel panel-default">
	<!--<div class="panel-heading">Form Elements</div>-->
	<div class="panel-body">

	<div class="col-sm-12">
		<div class="col-md-3">
			<div class="form-group">
				<label>Batch ID:</label>
				<label id="batchId"><?php $batch_id = $batch['BATCHID']; echo $batch['BATCHID'];?></label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Centre:</label>
				<label><?php echo $batch['CENTRE_NAME'];?></label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Timings:</label>
				<label><?php echo $batch['STARTTIME']." - ".$batch['ENDTIME'];?></label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Start Date:</label>
				<label><?php echo date('d/m/Y',strtotime($batch['STARTDATE']));?></label>
			</div>
		</div>
		</div>
		<div class="col-sm-12">
		<div class="col-md-3">
			<div class="form-group">
				<label>Subject:</label>
				<label><?php echo $batch['COURSE_NAME'];?></label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Days:</label>
				<label><?php echo $batch['DAYS'];?></label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Faculty:</label>
				<label>
					<?php
						if($batch['FACULTY_NAME'] == ""){
							echo $batch['EMP_NAME'];
						}
						else{
							echo $batch['FACULTY_NAME'];
						}
					?>
				</label>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Expected End Date:</label>
				<label><?php echo date('d/m/Y',strtotime($batch['EXPECTEDENDDATE']));?></label>
			</div>
		</div>
	</div>
	</div>

	</div>
	</div>
</div>
</div>
	<?php } ?>

<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div id="box"><h2>Assignment/Project details</h2></div>
			<div class="panel panel-default">
				<form enctype='multipart/form-data' method='post'>
					<input type='hidden' name='<?php echo $this->security->get_csrf_token_name();?>' value='<?php echo $this->security->get_csrf_hash();?>'>
				<div class="panel-body">
					<div class="col-sm-12">
						<div class="col-md-4">
							<input type="hidden" name="BATCH_ID" value="<?php echo $batch_id;?>">
							<div class="form-group">
								<label>Assignment / Project Topic : <span style="color:red;">*</span></label>
								<input type="text" name="ASSIGNMENT_TOPIC" class="form-control">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Assignment / Project Description : <span style="color:red;">*</span></label>
								<textarea name="ASSIGNMENT_DESCRIPTION" class="form-control"></textarea>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Submission Last Date : <span style="color:red;">*</span></label>
								<input type="text" name="ASSIGNMENT_SUBMISSION_LAST_DATE" class="form-control enquiry_date">
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="col-md-5">
							<div class="form-group">
								<label>Attachments :</label>
								<input type="file" name="attached_file" class="form-control" style="padding:0px !important;">
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Type : <span style="color:red;">*</span></label>
								<select class="form-control" name="TYPE">
									<option selected disabled>Select Type</option>
									<option option="Assignment">Assignment</option>
									<option option="Project">Project</option>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<br/>
								<button type="submit" class="btn btn-primary" name="assign_assignment_or_project">Submit</button>
							</div>
						</div>
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>

<div class="container">
<div class="row">
<div class="col-sm-12"><div id="box"> <h2>Existing Students In Batch</h2></div>
<div class="panel panel-default">
<!--<div class="panel-heading">Form Elements</div>-->
<div class="panel-body">


<div id="">
<div class="panel-body table-responsive"  style="height: 319px;">

<table  class="table table-bordered table-condensed table-stripped">
<thead>
<tr>
<th>ADMISSION ID</th>
<th>STUDENT NAME</th>
<th>ADMISSION DATE</th>
<th>COURSE TAKEN</th>
<th>CENTRE NAME</th>
<th>FEES</th>
<th>FEES PAID</th>
<th>MOBILE NO.</th>

</tr>
</thead>
<tbody>

<?php

if(empty($existingStudentsInBatch))
{
echo "<tr><td colspan=18>No Students</td></tr>";
}
else
{

foreach($existingStudentsInBatch as $data)
{


?>
<tr>
<td><?php echo $data['ADMISSION_ID']?></td>
<td><?php echo $data['STUDENT_NAME']?></td>
 <td><?php echo $data['ADMISSION_DATE']?></td>
 <td>
	 <?php
				$courses_arr = explode(",",$data['COURSE_TAKEN']);
						 $courseNames = '';
						 //echo '<ul class="list-group">';
							for($j=0;$j<count($courses);$j++){
									if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
												$courseNames .= ','.$courses[$j]['COURSE_NAME'];
											$data['COURSE_TAKEN'] = trim($courseNames,",");
										}

							}

				$modules_arr = explode(",",$data['MODULE_TAKEN']);
						 $moduleNames = '';
						 //echo '<ul class="list-group">';
							for($j=0;$j<count($modules);$j++){
									if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
												$moduleNames .= ','.$modules[$j]['MODULE_NAME'];
											$data['MODULE_TAKEN'] = trim($moduleNames,",");
										}

							}
						 //echo '</ul>';
			 $data['COURSE_TAKEN'] = trim($data['MODULE_TAKEN'].",".$data['COURSE_TAKEN'],",");

	  echo $data['COURSE_TAKEN'];?></td>

<td><?php echo $data['CENTRE_NAME']?></td>
 <td><?php echo $data['FEES']?></td>
<td><?php ?></td>
<td><?php echo $data['MOBILE_NO']?></td>

</tr>
<?php } }?>
</tbody>

</table>

</div>

</div>

</div>
</div><!-- /.col-->
</div>
</div>
</div>
<!-- /.row -->
