
<script type="text/javascript" src="<?php echo base_url('resources/js/'); ?>attendance_custom.js"></script>
<body>
<section id="login">
	<div class="container">
		<div class="row">
			<?php foreach($batch_details as $batch){
			?>

                <div class="col-md-12 col-sm-12">
					<div id="box"><h2>Batch Details</h2></div>
						<div class="panel panel-default">
								<!--<div class="panel-heading">Form Elements</div>-->
							<div class="panel-body">

								<div class="col-md-4">
									<div class="form-group">
										<label>Batch ID:</label>
										<label><?php echo $batch['BATCHID'];
											?></label>
										<input type="hidden" id="BATCHID" value="<?php echo $batch['BATCHID'];?>">
									</div>

									<div class="form-group">
										<label>Centre:</label>
										<label><?php echo $batch['CENTRE_NAME'];
										?></label>
									</div>

									<div class="form-group">
										<label>Timings:</label>
										<label><?php echo $batch['STARTTIME']." - ".$batch['ENDTIME'];?></label>
										<input type="hidden" id="STARTTIME" value="<?php echo $batch['STARTTIME'];?>">
										<input type="hidden" id="ENDTIME" value="<?php echo $batch['ENDTIME'];?>">
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label>Start Date:</label>
										<label><?php echo date('d/m/Y',strtotime($batch['STARTDATE']));?></label>
									</div>

									<div class="form-group">
										 <label>Subject:</label>
										 <label><?php echo $batch['COURSE_NAME']; ?></label>
									</div>
									<div class="form-group">
										<label>Days:</label>
										<label><?php echo $batch['DAYS'];?></label>
									</div>
								</div>

								<div class="col-md-4">
									<div class="form-group">
										<label>Faculty:</label>
										<label><?php echo $batch['EMP_NAME'];?></label>
									</div>
									<div class="form-group">
										<label>Expected End Date:</label>
										<label><?php echo date('d/m/Y',strtotime($batch['EXPECTEDENDDATE']));?></label>
									</div>
								</div>
							</div>
						</div>
					</div>

					<?php } ?>

                <div class="container">
					<div class="row">
          	<div class="col-md-12 col-sm-12"><div id="box"> <h2>Weekly test</h2></div>
							<div class="panel panel-default">
								<div class="panel-body">
								<div id="">
									<?php //echo "<pre>";print_r($student_details);exit();?>
									<div class="panel-body table-responsive">
										<?php
										$displayDiv = "style='display:none;text-align:center;background-color:rgba(128, 128, 128, 0.3);'";
										switch($batch['BATCH_STATUS']){
											case 'C':
											 $displayDiv = "style='display: table-cell;text-align:center;background-color:rgba(128, 128, 128, 0.3);'";
											 echo '<span style="color:green;font-weight:bold;">Batch Status: Completed</span>';
											 break;
											case 'H':
											 echo '<span style="color:violet;font-weight:bold;">Batch Status: Hold</span>';
											 break;
											case 'P':
											 echo '<span style="color:red;font-weight:bold;">Batch Status: Persuing</span>';
											 break;
											default:
											 echo 'Status Not here';
										}?>
										<p style="color:red;font-weight:bold;">*Note: Date Seleted columns marks are only accepted for weekly test marks against student.</p>
										<?php echo form_open();?>
										<table  class="table table-bordered table-condensed table-stripped">
											<thead>
												<tr>
													<th>ADMISSION ID</th>
			                    <th>STUDENT NAME</th>
						              <th>MOBILE No.</th>
													<?php
													if(!(empty($exam_dates))){
														foreach($exam_dates as $date){
															echo "<th class='weekly_test_feeded' ".$displayDiv."><label style='font-size:15px;'>".date('d/m/Y',strtotime($date))."</label></th>";
														}
													}
													if($batch['BATCH_STATUS'] == 'P'){
														for($i=0;$i<25;$i++){
															echo "<th><input type='text' class='form-control enquiry_date test_dates' name='test_dates[]' style='width:100px' data-date_no='".$i."' id='weekly_test_date_".$i."'></th>";
														}
													}?>
			                	</tr>
											</thead>
											<tbody>
												<?php foreach($student_details as $detail){?>
													<tr>
	                         	<td><?php echo $detail['Admission_ID'];?><input type="hidden" name="weekly_test_admission_ids[]" class="weekly_test_admission_ids" value="<?php echo $detail['Admission_ID'];?>"></td>
	                          <td><?php echo $detail['STUDENT_NAME'];?></td>
	                          <td><?php echo $detail['MOBILE_NO'];?></td>
														<?php
														if(!(empty($detail[0]))){
															foreach($detail[0] as $details){
																$marks_obtained = $details['MARKS_OBTAINED'];
																echo "<td class='weekly_test_feeded' ".$displayDiv."><label style='font-size:15px;'>".$marks_obtained."</label></td>";
															}
														}
														if($batch['BATCH_STATUS'] == 'P'){
															for($i=0;$i<25;$i++){
																echo "<td><input type='number' min='0' max='10' class='form-control test_marks_".$i."' name='test_marks_".$i."[]' style='width:100px' value='0' data-marks_no='".$i."'  id='weekly_test_marks_".$i."_".$detail['Admission_ID']."'></td>";
															}
														}?>
													</tr>
												<?php }?>
											</tbody>
											<?php if($batch['BATCH_STATUS'] == 'P'){?>
											<tfoot><tr><th colspan="3" class="hide_show_feeded_marks"><span class="fa fa-2x fa-eye-slash" id="marks_toggling_icon" style="float:right;color:red;cursor:pointer;"></span></th></tr></tfoot>
											<?php }?>
										</table>
										<?php if($batch['BATCH_STATUS'] == 'P'){?>
											<button type="submit" name="submit_marks" class="btn btn-primary center-block" style="width:200px;">Submit</button>
											<input type="hidden" name='batch_id' value="<?php echo $batch['BATCHID'];?>"/>
										<?php }?>
										<?php echo form_close();?>
									</div>
  							</div>
							</div>
						</div>
          	</div>
        </div>
				</div>
</section>
</body>
