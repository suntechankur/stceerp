<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_controller extends CI_Controller {

		public function show() {
    	 $this->load->model('Employee_model');
    	   $data['show'] = $this->Employee_model->get_candidate();

    	$config['base_url'] = base_url('employee/view-candidate/');
        //$config['total_rows'] = $this->Employee_model->getEnquiryCount($enq_list['enquiry_data']);
        $config['per_page'] = '10';
        
        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/
        
        $this->pagination->initialize($config);
        /* Pagination Ends */
         $data['pagination'] = $this->pagination->create_links();

        $this->load->admin_view('Employee_view',$data);


	}

} ?>