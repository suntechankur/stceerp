<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_model extends CI_Model {

	public function __construct()	{

	}

    public function getEmployeeNames($id =0){
        $query = $this->db->select('EMPLOYEE_ID,EMP_FNAME,EMP_LASTNAME')
                          ->from('employee_master');
        if($id == 0){
            $query = $this->db->where('ISACTIVE','1');
        }
            $query = $this->db->group_by('EMPLOYEE_ID')
                              ->get();
        return $query->result_array();
    }

// written by ankur on 28/11/2017 for getting active employee details centre wise who handles the frontoffice
    public function getEmployeeNamesAsPerCentreLogin(){
        $employeeIds = array(3170,2818,3230);
        $query = $this->db->select('EM.EMPLOYEE_ID,EM.EMP_FNAME,EM.EMP_LASTNAME,UL.ROLE_ID')
                          ->from('employee_master EM')
                          ->join('employee_timings ET','ET.EMPLOYEE_ID=EM.EMPLOYEE_ID','left')
                          ->join('user_login UL','UL.EMPLOYEE_ID=EM.EMPLOYEE_ID','left');
                          if(!(in_array($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'], $employeeIds))){
                            $query = $this->db->where('EM.CENTRE_ID',$this->session->userdata('admin_data')[0]['CENTRE_ID']);
                          }
                          $query = $this->db->where('EM.ISACTIVE',1)
                          ->order_by('EM.EMP_FNAME','ASC')
                          ->group_by('EM.EMPLOYEE_ID')
                          ->get();
        return $query->result_array();
    }

    public function getEmployeeNamesAsPerCentreLoginIfCentreNamesAreNotTheir(){
        $employeeIds = array(3170,2818,3230);
        $query = $this->db->select('EM.EMPLOYEE_ID,EM.EMP_FNAME,EM.EMP_LASTNAME,UL.ROLE_ID')
                          ->from('employee_master EM')
                          ->join('employee_timings ET','ET.EMPLOYEE_ID=EM.EMPLOYEE_ID','left')
                          ->join('user_login UL','UL.EMPLOYEE_ID=EM.EMPLOYEE_ID','left');
                          if(!(in_array($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'], $employeeIds))){
                            $query = $this->db->where('ET.CENTRE_ID',$this->session->userdata('admin_data')[0]['CENTRE_ID']);
                          }
                          $query = $this->db->where('EM.ISACTIVE',1)
                          ->order_by('EM.EMP_FNAME','ASC')
                          ->group_by('EM.EMPLOYEE_ID')
                          ->get();
        return $query->result_array();
    }
// end of the function

	public function get_candidate() {

    			//$query = $this->db->where('Employee_master', array('EMPLOYEE_ID' => $id,'ISACTIVE' => '1'));
  				$query = $this->db->select('concat(FIRSTNAME," ",LASTNAME) as c_name,PHONE_NO1,EMAIL,ADDRESS1,POST_APPLIED_FOR,,INTERVIEWDATE,EXPERIENCE,INTERESTED_IN')
  								  ->where('CANDIDATE_ID >','4592')
  								  ->get('Candidate_master');
    			return $query->result_array();

				}

  public function getEmpCentre($userId){
    $query = $this->db->select('EM.CENTRE_ID,CM.CENTRE_NAME')
                      ->from('employee_master EM')
                      ->join('user_login UL','UL.EMPLOYEE_ID = EM.EMPLOYEE_ID')
                      ->join('centre_master CM','CM.CENTRE_ID = EM.CENTRE_ID')
                      ->where('UL.USER_ID',$userId)
                      ->get();
    return $query->result_array();
  }

}
