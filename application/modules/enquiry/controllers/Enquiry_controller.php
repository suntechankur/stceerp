<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Enquiry_controller extends MX_Controller {

     public function __construct()
        {
                $this->load->model('enquiry_model');
                $this->load->model('employee/employee_model');
                $this->load->model('tele_enquiry/tele_enquiry_model');
                parent::__construct();
        }

    public function convertEnquiry($id){
        $telEnqId = $this->encrypt->decode($id);
        $data['employees'] = $this->employee_model->getEmployeeNames($telEnqId);
        // $data['courses'] = $this->tele_enquiry_model->course_interested($telEnqId);
        $data['courses'] = $this->tele_enquiry_model->get_course_details_with_fees();
        $data['telEnqData'] = $this->enquiry_model->getTeleEnquiryData($telEnqId);
        $data['occupations'] = $this->enquiry_model->getOccupation();
        $data['educations'] = $this->enquiry_model->getEducation();
        $data['heardabout'] = $this->enquiry_model->getHeardAbout();
        $data['centres'] = $this->tele_enquiry_model->get_centres();
        $data['timing'] = $this->enquiry_model->getTimeMaster();

        if($data['telEnqData'][0]['SUB_SOURCE'] != 0){
            $sid = $data['telEnqData'][0]['SUB_SOURCE'];
        }
        else{
            $sid = $data['telEnqData'][0]['SOURCE'];
        }

        $source = $this->enquiry_model->getHeardAbout($sid);

        $data['source_id'] = $source[0]['SOURCE_ID'];

        if(isset($_POST['COURSE_INTERESTED'])){
            $courseItrsd = implode(",", $_POST['COURSE_INTERESTED']);
            if(empty($_POST['COURSE_INTERESTED'])){
                $this->form_validation->set_rules('COURSE_INTERESTED', 'Course Interested', 'required');
            }
        }

        $this->form_validation->set_rules('ENQUIRY_FIRSTNAME', 'First Name', 'required');
        $this->form_validation->set_rules('ENQUIRY_LASTNAME', 'Last Name', 'required');
        $this->form_validation->set_rules('ENQUIRY_SUBURB', 'Suburb', 'required');
        $this->form_validation->set_rules('ENQUIRY_DATEOFBIRTH', 'D.O.B', 'required');
        $this->form_validation->set_rules('ENQUIRY_EMAIL', 'Email', 'required|valid_email');
        if(isset($_POST['SUGGESTED_CENTRE_ID'])){
            $this->form_validation->set_rules('CENTRE_ID', 'Centre', 'required');
        }
        if(isset($_POST['OCCUPATION'])){
            $this->form_validation->set_rules('OCCUPATION', 'Occupation', 'required');
        }
        if(isset($_POST['ENQUIRY_EDUCATION'])){
            $this->form_validation->set_rules('ENQUIRY_EDUCATION', 'Education', 'required');
        }

        if(isset($_POST['PREFERREDED_TIME1']) && isset($_POST['PREFERREDED_TIME2']) && isset($_POST['PREFERREDED_TIME3'])){
            if($_POST['PREFERREDED_TIME1'] == '' && $_POST['PREFERREDED_TIME2'] == '' && $_POST['PREFERREDED_TIME3'] == ''){
                $this->form_validation->set_rules('PREFERREDED_TIMES', 'Email', 'required',array('required' => 'Select all three Preferred Timings'));
            }

            if ($_POST['PREFERREDED_TIME1'] == $_POST['PREFERREDED_TIME2'] || $_POST['PREFERREDED_TIME2'] == $_POST['PREFERREDED_TIME3'] || $_POST['PREFERREDED_TIME3'] == $_POST['PREFERREDED_TIME1'] || $_POST['PREFERREDED_TIME3'] == $_POST['PREFERREDED_TIME2']) {
                $this->form_validation->set_rules('PREFERREDED_TIMES', 'Email', 'required',array('required' => 'Preferred time cannot be same'));
            }
        }
        $this->form_validation->set_rules('ENQUIRY_FORM_NO','Enquiry form no.','required|numeric');
        $this->form_validation->set_rules('ENQUIRY_MOBILE_NO', 'Mobile No.', 'required|min_length[10]|max_length[10]|numeric|is_unique[enquiry_master.ENQUIRY_MOBILE_NO]');
        $this->form_validation->set_rules('ENQUIRY_PARENT_NO', 'Parent No.', 'required|min_length[10]|max_length[10]|numeric');
        $this->form_validation->set_rules('ENQUIRY_HANDELED_BY', 'Enquiry Handeled By', 'required');
        $this->form_validation->set_rules('COURSE_OFFERED_IN', 'Course offered in', 'required');
        $this->form_validation->set_rules('ENQUIRY_ZIP', 'Zip', 'numeric');

        if ($this->form_validation->run() != FALSE){
            $date_rep = str_replace('/', '-', $_POST['ENQUIRY_DATE']);
            $enq_date =  date("Y-m-d", strtotime($date_rep) );

            $dob_rep = str_replace('/', '-', $_POST['ENQUIRY_DATEOFBIRTH']);
            $dob_date =  date("Y-m-d", strtotime($dob_rep) );

            $courseItrsd = implode(",",$_POST['COURSE_INTERESTED']);
           $insertEnq = array(
               'ENQUIRY_FIRSTNAME' => $_POST['ENQUIRY_FIRSTNAME'],
               'ENQUIRY_MIDDLENAME' => $_POST['ENQUIRY_MIDDLENAME'],
               'ENQUIRY_LASTNAME' => $_POST['ENQUIRY_LASTNAME'],
               'ENQUIRY_ADDRESS1' => $_POST['ENQUIRY_ADDRESS1'],
               'ENQUIRY_AREA' => $_POST['ENQUIRY_AREA'],
               'ENQUIRY_SUBURB' => $_POST['ENQUIRY_SUBURB'],
               'ENQUIRY_STATE' => $_POST['ENQUIRY_STATE'],
               'ENQUIRY_CITY' => $_POST['ENQUIRY_CITY'],
               'ENQUIRY_ZIP' => $_POST['ENQUIRY_ZIP'],
               'COURSE_INTERESTED' => $courseItrsd,
               'ENQUIRY_GENDER' => $_POST['ENQUIRY_GENDER'],
               'ENQUIRY_DATEOFBIRTH' => $dob_date,
               'ENQUIRY_EMAIL' => $_POST['ENQUIRY_EMAIL'],
               'CENTRE_ID'  =>  $_POST['CENTRE_ID'],
               'ENQUIRY_EDUCATION' => $_POST['ENQUIRY_EDUCATION'],
               'PREFERREDED_TIME1' => $_POST['PREFERREDED_TIME1'],
               'PREFERREDED_TIME2' => $_POST['PREFERREDED_TIME2'],
               'PREFERREDED_TIME3' => $_POST['PREFERREDED_TIME3'],
               'PREFERRED_CONTACTTIME' => $_POST['PREFERRED_CONTACTTIME'],
               'ENQUIRY_DATE' => $enq_date,
               'ENQUIRY_TIME' => $_POST['ENQUIRY_TIME'],
               'ENQUIRY_FORM_NO' => $_POST['ENQUIRY_FORM_NO'],
               'ENQUIRY_TELEPHONE' => $_POST['ENQUIRY_PARENT_NO'],
               'ENQUIRY_MOBILE_NO' => $_POST['ENQUIRY_MOBILE_NO'],
               'ENQUIRY_PARENT_NO' => $_POST['ENQUIRY_PARENT_NO'],
               'HEARD_ABOUTUS' => $_POST['HEARD_ABOUTUS'],
               'ENQUIRY_MODE' => $_POST['ENQUIRY_MODE'],
               'ENQUIRY_HANDELED_BY' => $_POST['ENQUIRY_HANDELED_BY'],
               'REMARKS' => $_POST['REMARKS'],
               'COURSE_OFFERED_IN' => $_POST['COURSE_OFFERED_IN'],
               'TELE_ENQUIRY_ID' => $telEnqId
           );
           $this->enquiry_model->addEnquiry($insertEnq);
           $enqId = $this->db->insert_id();
           $this->enquiry_model->updateTelEnqEnqId($telEnqId,$enqId);
           $this->session->set_flashdata('success','Record saved Successfully.');
           redirect(base_url('enquiry/view-enquiry'));
        }
        $this->load->admin_view('add_enquiry',$data);
    }

// code for report genration ///////////////////////////////
public function enquiry_report()
{
  $enq_list['enquiry_data'] = $this->session->userdata('enquiry_data');
  $enquiry = $this->enquiry_model->get_export_enquiry_list($enq_list['enquiry_data']);

        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);


        $col = 'A';
        for($i=0;$i<count($enquiry['fields']);$i++){
            $objPHPExcel->getActiveSheet()->SetCellValue($col.'1', $enquiry['fields'][$i]);
            $objPHPExcel->getActiveSheet()->getStyle("A1:X1")->getFont()->setBold(true);
            $col++;
        }

        $j = 2;
        $alphebetCol = 'A';
        foreach ($enquiry['details'] as $key => $value)
        {

          foreach ($enquiry['details'][$key] as $index => $data)
            {
                $objPHPExcel->getActiveSheet()->SetCellValue($alphebetCol.$j,$enquiry['details'][$key][$index]);

                $alphebetCol++;
                if($alphebetCol == "T")
                {
                  $alphebetCol = 'A';
                }
            }
            $j++;
        }

        $filename = "Enquiry_Details-" . date("h.i.s.d-m-Y") . ".xls";
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=" . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
}

      public function viewSearchEnquiry($offset = '0'){
          /*$this->form_validation->set_rules('ENQUIRY_DATE_FROM', 'DATE FROM', 'less_than[ENQUIRY_DATE_TO]');
          $this->form_validation->set_rules('ENQUIRY_DATE_TO', 'DATE TO', 'greater_than[ENQUIRY_DATE_FROM]');*/


        if(!empty($_POST)){
            $this->session->set_userdata('enquiry_data',$_POST);
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('enquiry_data');
        }
        $enq_list['enquiry_data'] = $this->session->userdata('enquiry_data');
        /* Pagination starts */
        $config['base_url'] = base_url('enquiry/view-enquiry/');
        $config['total_rows'] = $this->enquiry_model->getEnquiryCount($enq_list['enquiry_data']);
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */

        $enq_list['enquiries'] = $this->enquiry_model->getEnquiry($offset,$config['per_page'],$enq_list['enquiry_data']);
        $enq_list['pagination'] = $this->pagination->create_links();
        $enq_list['enq_handled_by'] = $this->enquiry_model->enquiry_handled_by($this->session->userdata('admin_data')[0]['CENTRE_ID']);
        $enq_list['heardabout'] = $this->enquiry_model->getHeardAbout();
        $enq_list['centres'] = $this->tele_enquiry_model->suggested_centre();
        $enq_list['courses'] = $this->tele_enquiry_model->course_interested();
        $this->load->admin_view('view_enquiry',$enq_list);
    }

    public function addEnquiry(){
        $data['employees'] = $this->employee_model->getEmployeeNamesAsPerCentreLogin();

        $data['courses'] = $this->tele_enquiry_model->get_course_details_with_fees();
        $data['occupations'] = $this->enquiry_model->getOccupation();
        $data['educations'] = $this->enquiry_model->getEducation();
        $data['heardabout'] = $this->enquiry_model->getHeardAbout();
        $data['centres'] = $this->tele_enquiry_model->get_centres();
        $data['timing'] = $this->enquiry_model->getTimeMaster();

         if(isset($_POST['COURSE_INTERESTED'])){
            $courseItrsd = implode(",", $_POST['COURSE_INTERESTED']);
        }
        else
        {
            $courseItrsd = '';
        }



        $this->form_validation->set_rules('ENQUIRY_FIRSTNAME', 'First Name', 'required');
        $this->form_validation->set_rules('ENQUIRY_LASTNAME', 'Last Name', 'required');
        $this->form_validation->set_rules('ENQUIRY_SUBURB', 'Suburb', 'required');
        $this->form_validation->set_rules('ENQUIRY_DATEOFBIRTH', 'D.O.B', 'required');
        $this->form_validation->set_rules('ENQUIRY_EMAIL', 'Email', 'required|valid_email');
        if(isset($_POST['SUGGESTED_CENTRE_ID'])){
            $this->form_validation->set_rules('SUGGESTED_CENTRE_ID', 'Centre', 'required');
        }

        if(isset($_POST['ENQUIRY_EDUCATION'])){
            $this->form_validation->set_rules('ENQUIRY_EDUCATION', 'Education', 'required');
        }



        if(isset($_POST['PREFERREDED_TIME1']) && isset($_POST['PREFERREDED_TIME2']) && isset($_POST['PREFERREDED_TIME3'])){

            if($_POST['PREFERREDED_TIME1'] == '' || $_POST['PREFERREDED_TIME2'] == '' || $_POST['PREFERREDED_TIME3'] == ''){
                $this->form_validation->set_rules('PREFERREDED_TIMES', 'Email', 'required',array('required' => 'Select all three Preferred Timings'));
            }
            else{
                if ($_POST['PREFERREDED_TIME1'] == $_POST['PREFERREDED_TIME2'] || $_POST['PREFERREDED_TIME2'] == $_POST['PREFERREDED_TIME3'] || $_POST['PREFERREDED_TIME3'] == $_POST['PREFERREDED_TIME1'] || $_POST['PREFERREDED_TIME3'] == $_POST['PREFERREDED_TIME2']) {
                  $this->form_validation->set_rules('PREFERREDED_TIMES', 'Email', 'required',array('required' => 'Preferred time cannot be same'));
              }
            }
        }

        $this->form_validation->set_rules('ENQUIRY_FORM_NO','Enquiry form no.','required|numeric');
        $this->form_validation->set_rules('ENQUIRY_MOBILE_NO', 'Mobile No.', 'required|min_length[10]|max_length[10]|numeric');
        //$this->form_validation->set_rules('ENQUIRY_TELEPHONE', 'Telephone', 'required|min_length[10]|max_length[10]|numeric');
        $this->form_validation->set_rules('ENQUIRY_HANDELED_BY', 'Enquiry Handeled By', 'required');
        $this->form_validation->set_rules('COURSE_OFFERED_IN', 'Course offered in', 'required');
        $this->form_validation->set_rules('ENQUIRY_ZIP', 'Zip', 'numeric');
        $this->form_validation->set_rules('COURSE_INTERESTED[]', 'Course Interested', 'required');
        if ($this->form_validation->run() == FALSE){
          $this->session->set_flashdata('danger',validation_errors());
          //$this->session->set_flashdata('danger','Please check validation and save again.');
        }
        else
        {

            $date_rep = str_replace('/', '-', $_POST['ENQUIRY_DATE']);
            $enq_date =  date("Y-m-d", strtotime($date_rep) );

            $dob_rep = str_replace('/', '-', $_POST['ENQUIRY_DATEOFBIRTH']);
            $dob_date =  date("Y-m-d", strtotime($dob_rep) );

            $courseItrsd = implode("-1,",$_POST['COURSE_INTERESTED']);
           $insertEnq = array(
               'ENQUIRY_FIRSTNAME' => $_POST['ENQUIRY_FIRSTNAME'],
               'ENQUIRY_MIDDLENAME' => $_POST['ENQUIRY_MIDDLENAME'],
               'ENQUIRY_LASTNAME' => $_POST['ENQUIRY_LASTNAME'],
               'ENQUIRY_ADDRESS1' => $_POST['ENQUIRY_ADDRESS1'],
               'ENQUIRY_AREA' => $_POST['ENQUIRY_AREA'],
               'ENQUIRY_SUBURB' => $_POST['ENQUIRY_SUBURB'],
               'ENQUIRY_STATE' => 'MAHARASHTRA',
               'ENQUIRY_CITY' => $_POST['ENQUIRY_CITY'],
               'ENQUIRY_ZIP' => $_POST['ENQUIRY_ZIP'],
               'COURSE_INTERESTED' => $courseItrsd.'-1',
               'ENQUIRY_GENDER' => $_POST['ENQUIRY_GENDER'],
               'ENQUIRY_DATEOFBIRTH' => $dob_date,
               'ENQUIRY_EMAIL' => $_POST['ENQUIRY_EMAIL'],
               'ENQUIRY_EDUCATION' => $_POST['ENQUIRY_EDUCATION'],
               'PREFERREDED_TIME1' => $_POST['PREFERREDED_TIME1'],
               'PREFERREDED_TIME2' => $_POST['PREFERREDED_TIME2'],
               'PREFERREDED_TIME3' => $_POST['PREFERREDED_TIME3'],
               'PREFERRED_CONTACTTIME' => date('h:i A'),
               'ENQUIRY_DATE' => $enq_date,
               'ENQUIRY_FORM_NO' => $_POST['ENQUIRY_FORM_NO'],
               'ENQUIRY_TELEPHONE' => $_POST['ENQUIRY_TELEPHONE'],
               'ENQUIRY_MOBILE_NO' => $_POST['ENQUIRY_MOBILE_NO'],
               'ENQUIRY_PARENT_NO' => $_POST['ENQUIRY_TELEPHONE'],
               'HEARD_ABOUTUS' => $_POST['HEARD_ABOUTUS'],
               'ENQUIRY_MODE' => $_POST['ENQUIRY_MODE'],
               'ENQUIRY_HANDELED_BY' => $_POST['ENQUIRY_HANDELED_BY'],
               'REMARKS' => $_POST['REMARKS'],
               'COURSE_OFFERED_IN' => $_POST['COURSE_OFFERED_IN'],
               'OCCUPATION' => $_POST['OCCUPATION'],
               'CENTRE_ID'  => $_POST['SUGGESTED_CENTRE_ID']
           );

          // added by ankur on 26th dec 2017
           $centre_details = $this->enquiry_model->get_centre_details_by_centre_id($_POST['SUGGESTED_CENTRE_ID']);

           $centre_contact_details = "";
           if(($centre_details[0]['TELEPHONE1'] != '0') && ($centre_details[0]['TELEPHONE2'] != '0')){
              $centre_contact_details = $centre_details[0]['TELEPHONE1'].", ".$centre_details[0]['TELEPHONE2'];
           }
           else if($centre_details[0]['TELEPHONE1'] != '0'){
              $centre_contact_details = $centre_details[0]['TELEPHONE1'];
           }
           else if($centre_details[0]['TELEPHONE2'] != '0'){
              $centre_contact_details = $centre_details[0]['TELEPHONE2'];
           }
          // till here
           $addEnq = $this->enquiry_model->addEnquiry($insertEnq);
           $enqId = $this->db->insert_id();
           if ($addEnq == true) {
            // added by ankur on 26th dec 2017
              $message = "Dear ".($_POST['ENQUIRY_FIRSTNAME']." ".$_POST['ENQUIRY_MIDDLENAME']." ".$_POST['ENQUIRY_LASTNAME']).",Thanks for enquiring at St.Angelo's ".$centre_details[0]['CENTRE_NAME']." campus for a career course.Pl call on ".$centre_contact_details. " for further assistance and help.";

              $cc = "";
              $recipientsList = $_POST['ENQUIRY_MOBILE_NO'].",".$_POST['ENQUIRY_TELEPHONE'];
              $mobileNumbers = explode(",",$recipientsList);

              $confirmationSms = "";
              $confirmationMail = "";

              // for sms send to enquiry
              //foreach ($mobileNumbers as $numbers) {
                //$confirmationSms = $this->load->sendSms($numbers,$message,$cc,"E");
              //}
              //end of sms code

              // for mail send to enquiry
              if($_POST['ENQUIRY_EMAIL'] != null){
                $from = "erp@suntechedhu.com";
                $to = $_POST['ENQUIRY_EMAIL'];
                $cc = array('ankur@suntechedhu.com','rajesh@suntechedhu.com','vijay@suntechedhu.com');
                $subject = "Enquiry dated on ".date("d/m/Y");
                $body = "<h2>Enquiry Details</h2>
                         <br/>
                         <p>Dear ".($_POST['ENQUIRY_FIRSTNAME']." ".$_POST['ENQUIRY_MIDDLENAME']." ".$_POST['ENQUIRY_LASTNAME']).",</p>
                         <p>Thanks for enquiring at St.Angelo's ".$centre_details[0]['CENTRE_NAME']." campus for a career course.Pl call on ".$centre_contact_details. " for further assistance and help.</p>
                         <p>* Please do not reply to this mail. This is automated generated email.<br><br>For more information visit on <a href='https://suntechedu.com/'>suntechedu.com</a></p>";


                $confirmationMail = $this->load->mailer($from,$to,$cc,"","",$subject,$body,"enquiry");
              }
              // end of mailer code

              // till here
              if($confirmationMail){
                $this->session->set_flashdata('success','Record Added Succesfully');
                $this->session->set_flashdata('enquiry_id',$this->encrypt->encode($enqId));
              }
           }
           else{
              $this->session->set_flashdata('danger','Danger!');
           }
           redirect(base_url('enquiry/add-enquiry'));
        }




        $this->load->admin_view('add_enquiry',$data);
    }

    public function editEnquiry($id){

        $enqId = $this->encrypt->decode($id);
        $data['enqData'] = $this->enquiry_model->editEnquiry($enqId);

        $data['source_id'] = $data['enqData'][0]['HEARD_ABOUTUS'];

        $data['employees'] = $this->employee_model->getEmployeeNames();
        $data['courses'] = $this->tele_enquiry_model->course_interested();
        $data['occupations'] = $this->enquiry_model->getOccupation();
        $data['educations'] = $this->enquiry_model->getEducation();
        $data['heardabout'] = $this->enquiry_model->getHeardAbout();
        $data['centres'] = $this->tele_enquiry_model->get_centres();
        $data['timing'] = $this->enquiry_model->getTimeMaster();


        $this->form_validation->set_rules('COURSE_INTERESTED[]', 'Course Interested', 'required');
        if(isset($_POST['COURSE_INTERESTED']))
        {
          $courseItrsd = implode(",", $_POST['COURSE_INTERESTED']);
        }
        else
        {
          $courseItrsd = '';
        }

        $this->form_validation->set_rules('ENQUIRY_GENDER', 'Gender', 'required');
        $this->form_validation->set_rules('ENQUIRY_FIRSTNAME', 'First Name', 'required');
        $this->form_validation->set_rules('ENQUIRY_LASTNAME', 'Last Name', 'required');
        if((empty($_POST['ENQUIRY_ADDRESS1'])) & (empty($_POST['ENQUIRY_ADDRESS2'])))
        {
        $this->form_validation->set_rules('ENQUIRY_ADDRESS1', 'address 1', 'required');
        }

        $this->form_validation->set_rules('ENQUIRY_AREA', 'Area', 'required');

        $this->form_validation->set_rules('ENQUIRY_SUBURB', 'Suburb', 'required');
        $this->form_validation->set_rules('ENQUIRY_CITY', 'City', 'required');
        $this->form_validation->set_rules('ENQUIRY_DATEOFBIRTH', 'D.O.B', 'required');
        $this->form_validation->set_rules('ENQUIRY_EMAIL', 'Email', 'required|valid_email');
        /*if(isset($_POST['SUGGESTED_CENTRE_ID'])){
            $this->form_validation->set_rules('SUGGESTED_CENTRE_ID', 'Centre', 'required');
        }*/
        if(isset($_POST['OCCUPATION'])){
            $this->form_validation->set_rules('OCCUPATION', 'Occupation', 'required');
        }
        if(isset($_POST['ENQUIRY_EDUCATION'])){
            $this->form_validation->set_rules('ENQUIRY_EDUCATION', 'Education', 'required');
        }

        if(isset($_POST['PREFERREDED_TIME1']) && isset($_POST['PREFERREDED_TIME2']) && isset($_POST['PREFERREDED_TIME3'])){
            if($_POST['PREFERREDED_TIME1'] == '' && $_POST['PREFERREDED_TIME2'] == '' && $_POST['PREFERREDED_TIME3'] == ''){
                $this->form_validation->set_rules('PREFERREDED_TIMES', 'Email', 'required',array('required' => 'Select all three Preferred Timings'));
            }

            if ($_POST['PREFERREDED_TIME1'] == $_POST['PREFERREDED_TIME2'] || $_POST['PREFERREDED_TIME2'] == $_POST['PREFERREDED_TIME3'] || $_POST['PREFERREDED_TIME3'] == $_POST['PREFERREDED_TIME1'] || $_POST['PREFERREDED_TIME3'] == $_POST['PREFERREDED_TIME2']) {
                $this->form_validation->set_rules('PREFERREDED_TIMES', 'Email', 'required',array('required' => 'Preferred time cannot be same'));
            }
        }
        $this->form_validation->set_rules('ENQUIRY_FORM_NO','Enquiry form no.','required|numeric');
        $this->form_validation->set_rules('ENQUIRY_MOBILE_NO', 'Mobile No.', 'required|min_length[10]|max_length[10]|numeric');
        $this->form_validation->set_rules('ENQUIRY_PARENT_NO', 'Parent No.', 'required|min_length[10]|max_length[10]|numeric');
        $this->form_validation->set_rules('ENQUIRY_HANDELED_BY', 'Enquiry Handeled By', 'required');
        $this->form_validation->set_rules('COURSE_OFFERED_IN', 'Course Offered In', 'required');
        //$this->form_validation->set_rules('ENQUIRY_ZIP', 'Zip', 'numeric');
        if ($this->form_validation->run() != FALSE){

            $date_rep = str_replace('/', '-', $_POST['ENQUIRY_DATE']);
            $enq_date =  date("Y-m-d", strtotime($date_rep) );

            $dob_rep = str_replace('/', '-', $_POST['ENQUIRY_DATEOFBIRTH']);
            $dob_date =  date("Y-m-d", strtotime($dob_rep) );

           $updateEnq = array(
               'ENQUIRY_FIRSTNAME' => $_POST['ENQUIRY_FIRSTNAME'],
               'ENQUIRY_MIDDLENAME' => $_POST['ENQUIRY_MIDDLENAME'],
               'ENQUIRY_LASTNAME' => $_POST['ENQUIRY_LASTNAME'],
               'ENQUIRY_ADDRESS1' => $_POST['ENQUIRY_ADDRESS1'],
               'ENQUIRY_ADDRESS2' => $_POST['ENQUIRY_ADDRESS2'],
               'ENQUIRY_AREA' => $_POST['ENQUIRY_AREA'],
               'ENQUIRY_SUBURB' => $_POST['ENQUIRY_SUBURB'],
               'ENQUIRY_STATE' => 'MAHARASHTRA',
               'ENQUIRY_CITY' => $_POST['ENQUIRY_CITY'],
               'ENQUIRY_ZIP' => $_POST['ENQUIRY_ZIP'],
               'COURSE_INTERESTED' => $courseItrsd,
               'ENQUIRY_GENDER' => $_POST['ENQUIRY_GENDER'],
               'ENQUIRY_DATEOFBIRTH' => $dob_date,
               'ENQUIRY_EMAIL' => $_POST['ENQUIRY_EMAIL'],
               'ENQUIRY_EDUCATION' => $_POST['ENQUIRY_EDUCATION'],
               'PREFERREDED_TIME1' => $_POST['PREFERREDED_TIME1'],
               'PREFERREDED_TIME2' => $_POST['PREFERREDED_TIME2'],
               'PREFERREDED_TIME3' => $_POST['PREFERREDED_TIME3'],
               'PREFERRED_CONTACTTIME' => $_POST['PREFERRED_CONTACTTIME'],
               'ENQUIRY_DATE' => $enq_date,
               'ENQUIRY_FORM_NO' => $_POST['ENQUIRY_FORM_NO'],
               'ENQUIRY_MOBILE_NO' => $_POST['ENQUIRY_MOBILE_NO'],
               'ENQUIRY_PARENT_NO' => $_POST['ENQUIRY_PARENT_NO'],
               'HEARD_ABOUTUS' => $_POST['HEARD_ABOUTUS'],
               'ENQUIRY_MODE' => $_POST['ENQUIRY_MODE'],
               'ENQUIRY_HANDELED_BY' => $_POST['ENQUIRY_HANDELED_BY'],
               'REMARKS' => $_POST['REMARKS'],
               'COURSE_OFFERED_IN' => $_POST['COURSE_OFFERED_IN'],
               'OCCUPATION' => $_POST['OCCUPATION'],
               'CENTRE_ID'  => $_POST['CENTRE_ID']
           );

           $this->enquiry_model->updateEnquiry($enqId,$updateEnq);
           $this->session->set_flashdata('success','Record Updated Successfully');
           redirect(base_url('enquiry/view-enquiry'));
        }
        $this->load->admin_view('edit_enquiry',$data);
    }

    public function delEnquiry($id){
        $enqId = $this->encrypt->decode($id);
        $status = $this->enquiry_model->del_enquiry($enqId);
        if($status == 1){
          $this->session->set_flashdata('danger',"1 Record Deleted Successfully.");
          redirect(base_url('enquiry/view-enquiry'));
        }
    }

    public function eod_enquiry(){
      $this->load->admin_view('eod_enquiry');
    }
}
