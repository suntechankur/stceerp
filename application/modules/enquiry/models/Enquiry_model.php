<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Enquiry_model extends CI_Model {

    public $sql;
    public function __construct() {
        parent::__construct();
    }

    public function getTeleEnquiryData($telEnqId){
        $query = $this->db->select('FIRSTNAME,MIDDLENAME,LASTNAME,EMAIL_ID,CENTRE_ID,MOBILE,ENQUIRY_HANDELED_BY,COURSE_INTERESTED,SOURCE,SUB_SOURCE,PARENT_NUMBER')
                          ->from('tele_enquiries')
                          ->where('TELE_ENQUIRY_ID',$telEnqId)
                          ->get();
        return $query->result_array();
    }

    public function getOccupation(){
        $query = $this->db->select('CONTROLFILE_ID,CONTROLFILE_VALUE')
                          ->from('control_file')
                          ->where('CONTROLFILE_KEY','OCCUPATION')
                          ->get();
        return $query->result_array();
    }

    public function getEducation(){
        $query = $this->db->select('CONTROLFILE_ID,CONTROLFILE_VALUE')
                          ->from('control_file')
                          ->where('CONTROLFILE_KEY','EDUCATION')
                          ->get();
        return $query->result_array();
    }

    public function getHeardAbout($sid = 0){
        $query = $this->db->select('controlfile_id as SOURCE_ID,controlfile_value')
                          ->from('control_file')
                          ->where('controlfile_key="HEARD_ABOUT_SACL" and ISACTIVE=1');
        if($sid != 0){
          $query = $this->db->where('controlfile_id',$sid);
        }
        $query = $this->db->order_by('controlfile_value','ASC')
                          ->get();
        return $query->result_array();
    }

    public function enquiry_handled_by($centre_id = 0){
        $query = $this->db->select('EM.EMPLOYEE_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME')
                          ->from('tele_enquiries TE')
                          ->join('followup_details FD','FD.SOURSE_ID = TE.TELE_ENQUIRY_ID')
                          ->join('employee_master EM','EM.EMPLOYEE_ID = TE.ENQUIRY_HANDELED_BY')
                          ->group_by('EM.EMP_FNAME,EM.EMP_LASTNAME')
                          ->where('FD.SOURCE','j')
                          ->where('EM.CENTRE_ID',$centre_id)
                          ->get();
        return $query->result_array();
    }

    public function getTimeMaster(){
      $query = $this->db->get('time_master');
      return $query->result_array();
    }

    public function updateTelEnqEnqId($telEnqId,$enqId){
      $this->db->set('ENQUIRY_ID',$enqId)
               ->where('TELE_ENQUIRY_ID',$telEnqId)
               ->update('tele_enquiries');
    }

    public function get_export_enquiry_list($enquiry_data){
      $query = $this->db->select('E.ENQUIRY_ID,E.ENQUIRY_DATE,CONCAT(E.ENQUIRY_FIRSTNAME," ",E.ENQUIRY_LASTNAME) as ENQUIRY_NAME,SM.SOURCE_SUBSOURCE,E.ENQUIRY_MOBILE_NO,E.ENQUIRY_PARENT_NO,E.ENQUIRY_EMAIL,E.COURSE_INTERESTED,CM.CENTRE_NAME,CONCAT(EMH.EMP_FNAME," ",EMH.EMP_LASTNAME) as HANDLED_BY,E.REMARKS,E.ISENROLLED,E.ADMISSION_ID,FD.FOLLOWUP_DETAILS_ID,FD.PREFERRED_TIME,max(FD.FOLLOWUP_DATE) as FOLLOWUP_DATE,FD.FOLLOWUP_DETAILS,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as FOLLOW_UP_BY,FD.NEXT_FOLLOWUP_DATE')

                 ->from('enquiry_master E')
                 ->join('followup_details FD','FD.SOURSE_ID = E.ENQUIRY_ID','left')
                 ->join('employee_master EM','EM.EMPLOYEE_ID = FD.FOLLOWUP_BY','left')
                 ->join('centre_master CM','CM.CENTRE_ID = E.CENTRE_ID','left')
                 ->join('centre_master SC','SC.CENTRE_ID = E.CENTRE_ID','left')
                 ->join('employee_master EMH','EMH.EMPLOYEE_ID = E.ENQUIRY_HANDELED_BY','left')
                 ->join('source_master SM','SM.SOURCE_ID = E.HEARD_ABOUTUS','left outer')
                 ->group_by('E.ENQUIRY_ID')
                 ->order_by('E.ENQUIRY_ID','desc');
        if(!empty($enquiry_data)){
            $enquiry_date_from = $enquiry_data['ENQUIRY_DATE_FROM'];
            $enquiry_date_to = $enquiry_data['ENQUIRY_DATE_TO'];
            unset($enquiry_data['ENQUIRY_FORM_NO_FROM']);
            unset($enquiry_data['ENQUIRY_FORM_NO_TO']);
            unset($enquiry_data['ENQUIRY_DATE_FROM']);
            unset($enquiry_data['ENQUIRY_DATE_TO']);
            if (isset($enquiry_data['COURSE_INTERESTED'])) {
              $enquiry_data['COURSE_INTERESTED'] = implode(",",$enquiry_data['COURSE_INTERESTED']);
            }
            if($enquiry_date_from != ''){
                $date_format_change = str_replace("/","-",$enquiry_date_from);
                $start_date = date('Y-m-d', strtotime($date_format_change));

                $query = $this->db->where('E.ENQUIRY_DATE >= ',$start_date);
                if($enquiry_date_to != ''){
                  $to_date_format_change = str_replace("/","-",$enquiry_date_to);
                  $to_date = date('Y-m-d', strtotime($to_date_format_change));
                  $query = $this->db->where('E.ENQUIRY_DATE <= ',$to_date);
                }
            }
            foreach($enquiry_data as $filterkey => $filter){
                if($filter != ''){
                    $query = $this->db->like('E.'.$filterkey,$filter,'both');
                }
            }
        }
        $query = $this->db->limit('10000');
        $query = $this->db->get();

        $enquiry['fields'] = $query->list_fields();
        $enquiry['details'] = $query->result_array();
        return $enquiry;
    }

    public function getEnquiry($offset,$perpage,$enquiry_data){
      $query = $this->db->select('E.ENQUIRY_ID,E.ENQUIRY_DATE,CONCAT(E.ENQUIRY_FIRSTNAME," ",E.ENQUIRY_LASTNAME) as NAME,SM.SOURCE_SUBSOURCE,E.ENQUIRY_MOBILE_NO,E.ENQUIRY_EMAIL,E.ENQUIRY_PARENT_NO,E.ENQUIRY_EMAIL,E.COURSE_INTERESTED,CM.CENTRE_NAME,CONCAT(EMH.EMP_FNAME," ",EMH.EMP_LASTNAME) as EMPH_NAME,E.REMARKS,E.ISENROLLED,E.ADMISSION_ID,FD.FOLLOWUP_DETAILS_ID,FD.PREFERRED_TIME,max(FD.FOLLOWUP_DATE) as FOLLOWUP_DATE,FD.FOLLOWUP_DETAILS,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME,FD.NEXT_FOLLOWUP_DATE')
                 ->from('enquiry_master E')
                 ->join('followup_details FD','FD.SOURSE_ID = E.ENQUIRY_ID','left')
                 ->join('employee_master EM','EM.EMPLOYEE_ID = FD.FOLLOWUP_BY','left')
                 ->join('centre_master CM','CM.CENTRE_ID = E.CENTRE_ID','left')
                 ->join('centre_master SC','SC.CENTRE_ID = E.CENTRE_ID','left')
                 ->join('employee_master EMH','EMH.EMPLOYEE_ID = E.ENQUIRY_HANDELED_BY','left')
                 ->join('source_master SM','SM.SOURCE_ID = E.HEARD_ABOUTUS','left outer')
                 ->where('E.IS_ACTIVE','1')
                 ->group_by('E.ENQUIRY_ID')
                 ->order_by('E.ENQUIRY_ID','desc');
        if(!empty($enquiry_data)){
            // $enquiry_date_from = $enquiry_data['ENQUIRY_DATE_FROM'];
            // $enquiry_date_to = $enquiry_data['ENQUIRY_DATE_TO'];
            // unset($enquiry_data['ENQUIRY_FORM_NO_FROM']);
            // unset($enquiry_data['ENQUIRY_FORM_NO_TO']);
            // unset($enquiry_data['ENQUIRY_DATE_FROM']);
            // unset($enquiry_data['ENQUIRY_DATE_TO']);
            // if (isset($enquiry_data['COURSE_INTERESTED'])) {
            //   $enquiry_data['COURSE_INTERESTED'] = $enquiry_data['COURSE_INTERESTED'];
            // }
            // if($enquiry_date_from != ''){
            //     $date_format_change = str_replace("/","-",$enquiry_date_from);
            //     $start_date = date('Y-m-d', strtotime($date_format_change));
            //
            //     $query = $this->db->where('E.ENQUIRY_DATE >= ',$start_date);
            //     if($enquiry_date_to != ''){
            //       $to_date_format_change = str_replace("/","-",$enquiry_date_to);
            //       $to_date = date('Y-m-d', strtotime($to_date_format_change));
            //       $query = $this->db->where('E.ENQUIRY_DATE <= ',$to_date);
            //     }
            // }
            foreach($enquiry_data as $filterkey => $filter){
                if($filter != ''){
                    $query = $this->db->like('E.'.$filterkey,$filter,'after');
                }
            }
        }
            //  $query = $this->db->limit($perpage,$offset);
              $query = $this->db->order_by('E.ENQUIRY_DATE','DESC')
                                ->get();
        return $query->result_array();
    }

    public function getEnquiryCount($enquiry_data){
        $query = $this->db->select('E.ENQUIRY_ID')
                 ->from('enquiry_master E')
                 ->join('followup_details FD','FD.SOURSE_ID = E.ENQUIRY_ID','left')
                 ->join('employee_master EM','EM.EMPLOYEE_ID = FD.FOLLOWUP_BY','left')
                 ->join('employee_master EMH','EMH.EMPLOYEE_ID = E.ENQUIRY_HANDELED_BY','left')
                 ->group_by('E.ENQUIRY_ID')
                 ->where('FD.SOURCE','e');
        if(!empty($enquiry_data)){
            // $enquiry_date_from = $enquiry_data['ENQUIRY_DATE_FROM'];
            // $enquiry_date_to = $enquiry_data['ENQUIRY_DATE_TO'];
            // unset($enquiry_data['ENQUIRY_FORM_NO_FROM']);
            // unset($enquiry_data['ENQUIRY_FORM_NO_TO']);
            // unset($enquiry_data['ENQUIRY_DATE_FROM']);
            // unset($enquiry_data['ENQUIRY_DATE_TO']);
            // if (isset($enquiry_data['COURSE_INTERESTED'])) {
            //   $enquiry_data['COURSE_INTERESTED'] = $enquiry_data['COURSE_INTERESTED'];
            // }
            // if($enquiry_date_from != ''){
            //     $date_format_change = str_replace("/","-",$enquiry_date_from);
            //     $start_date = date('Y-m-d', strtotime($date_format_change));
            //
            //     $query = $this->db->where('E.ENQUIRY_DATE > ',$start_date);
            //     if($enquiry_date_to != ''){
            //       $to_date_format_change = str_replace("/","-",$enquiry_date_to);
            //       $to_date = date('Y-m-d', strtotime($to_date_format_change));
            //       $query = $this->db->where('E.ENQUIRY_DATE <',$to_date);
            //     }
            // }
            foreach($enquiry_data as $filterkey => $filter){
                if($filter != ''){
                    $query = $this->db->like('E.'.$filterkey,$filter,'after');
                }
            }
        }
            $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_centre_details_by_centre_id($centre_id){
      $query = $this->db->select('*')
                          ->from('centre_master')
                          ->where('CENTRE_ID',$centre_id)
                          ->get();
        return $query->result_array();
    }

    public function addEnquiry($addEnquiry){
      $this->db->insert('enquiry_master',$addEnquiry);  // for mysql

      $modified_date =  date("Y-m-d H:i", strtotime(date('Y-m-d')));

      $preffered_time = $addEnquiry['PREFERREDED_TIME1'];
      unset($addEnquiry['COURSE_OFFERED_IN'],$addEnquiry['ENQUIRY_PARENT_NO'],$addEnquiry['PREFERREDED_TIME1']);
      $addEnquiry['PREFERREDED_TIME'] = $preffered_time;
      $addEnquiry['MODIFIED_BY'] = $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'];
      $addEnquiry['MODIFIED_DATE'] = $modified_date;

      if($this->db->affected_rows() > 0){
        return true;
      }
      else{
        return false;
      }
    }

    public function editEnquiry($enqId){
        $query = $this->db->select('ENQUIRY_ID,ENQUIRY_FIRSTNAME,ENQUIRY_MIDDLENAME,ENQUIRY_LASTNAME,ENQUIRY_ADDRESS1,ENQUIRY_ADDRESS2,ENQUIRY_AREA,ENQUIRY_SUBURB,ENQUIRY_CITY,ENQUIRY_ZIP,COURSE_INTERESTED,ENQUIRY_GENDER,ENQUIRY_DATEOFBIRTH,ENQUIRY_EMAIL,ENQUIRY_EDUCATION,PREFERREDED_TIME1,PREFERREDED_TIME2,PREFERREDED_TIME3,PREFERRED_CONTACTTIME,ENQUIRY_DATE,ENQUIRY_TIME,ENQUIRY_FORM_NO,ENQUIRY_MOBILE_NO,ENQUIRY_PARENT_NO,HEARD_ABOUTUS,ENQUIRY_MODE,ENQUIRY_HANDELED_BY,COURSE_OFFERED_IN,REMARKS,CENTRE_ID,OCCUPATION,PREFERREDED_TIME1,PREFERREDED_TIME2,PREFERREDED_TIME3')
                ->from('enquiry_master')
                ->where('ENQUIRY_ID',$enqId)
                ->get();
        return $query->result_array();
    }

    public function updateEnquiry($enqId, $updateEnq){
        $this->db->set($updateEnq)
                 ->where('ENQUIRY_ID',$enqId)
                 ->update('enquiry_master');    // for mysql


      $preffered_time = $updateEnq['PREFERREDED_TIME1'];
      unset($updateEnq['COURSE_OFFERED_IN'],$updateEnq['ENQUIRY_PARENT_NO'],$updateEnq['PREFERREDED_TIME1']);
      $updateEnq['PREFERREDED_TIME'] = $preffered_time;

    }

    public function del_enquiry($enqId){
        $this->db->where('ENQUIRY_ID',$enqId)
                 ->set('IS_ACTIVE',0)
                 ->update('enquiry_master');    // for mysql
        return 1;
    }

// This method is defined to check while converting teleEnq to Walkin Enquiry(ifit already exists)
   public function get_enquiry_by_tele_id($enqId){
        $query = $this->db->select('ENQUIRY_ID,TELE_ENQUIRY_ID')
                ->from('enquiry_master')
                ->where('ENQUIRY_ID',$enqId)
                ->get();
        return $query->num_rows();
    }
}
