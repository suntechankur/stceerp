<div class="gapping"></div>
<div class="create_batch_form">
    <div id="box">
        <h2>Add Enquiry</h2></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align:center; color:#F00;"></div>
                <div class="panel-body">
                    <?php if($this->session->flashdata('enquiry_id')) { ?>
                        <label style="font-family:Comic Sans MS;color:red;font-size:16px;">Do you want to convert this enquiry to admission ?? Then <a href="<?php echo base_url('enquiry/convert-admission/'.$this->session->flashdata('enquiry_id')); ?>"><img src="<?php echo base_url('resources/images/click_here.png');?>" style="height:38px;"></a> . . .</label>
                        <?php } ?>
                    <?php if($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                        </div>
                        <?php } ?>
                    <?php if($this->session->flashdata('danger')) { ?>
                        <div class="alert alert-danger">
                        <strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
                        </div>
                        <?php } ?>
                   <?php echo form_open(); ?>
                       <?php
                        /* This Form is for convert Enquiry */
                        if(isset($telEnqData)){ 
                        foreach($telEnqData as $enq){
                        ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name: <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="ENQUIRY_FIRSTNAME" value="<?php if($this->input->post('ENQUIRY_FIRSTNAME') != ''){echo $this->input->post('ENQUIRY_FIRSTNAME');}else{echo $enq['FIRSTNAME'];} ?>" id="ENQUIRY_FIRSTNAME   " required="required" /> 
                             <span class="text-danger"><?php echo form_error('ENQUIRY_FIRSTNAME'); ?></span>
                            </div>
                            <div class="form-group">
                                <label>Middle Name:</label>
                                <input class="form-control" type="text" name="ENQUIRY_MIDDLENAME" value="<?php if($this->input->post('ENQUIRY_MIDDLENAME') != ''){echo $this->input->post('ENQUIRY_MIDDLENAME');}else{echo $enq['MIDDLENAME'];} ?>" id="ENQUIRY_MIDDLENAME" /> </div>
                            <div class="form-group">
                                <label>Last Name: <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="ENQUIRY_LASTNAME" value="<?php if($this->input->post('ENQUIRY_LASTNAME') != ''){echo $this->input->post('ENQUIRY_LASTNAME');}else{echo $enq['LASTNAME'];}?>" id="ENQUIRY_LASTNAME" required="required" /> 
                                <span class="text-danger"><?php echo form_error('ENQUIRY_LASTNAME'); ?></span>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Flat/Room/Wing No.:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_ADDRESS1" id="ENQUIRY_ADDRESS1" value="<?php echo $this->input->post('ENQUIRY_ADDRESS1');?>"> </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Bldg./Plot/Chawl No.:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_ADDRESS2" id="ENQUIRY_ADDRESS2" value="<?php echo $this->input->post('ENQUIRY_ADDRESS2'); ?>"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Area/Landmark:</label>
                                <input class="form-control" type="text" name="ENQUIRY_AREA" value="<?php echo $this->input->post('ENQUIRY_AREA'); ?>" id="ENQUIRY_AREA" /> </div>
                            <div class="form-group">
                                <label>Nearest Railway Station: <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="ENQUIRY_SUBURB" value="<?php echo $this->input->post('ENQUIRY_SUBURB'); ?>" id="ENQUIRY_SUBURB" /> 
                                <span class="text-danger"><?php echo form_error('ENQUIRY_SUBURB'); ?></span>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>State:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_STATE" value="MAHARASHTRA" readonly="readonly" id="ENQUIRY_STATE" /> </div>
                                         <div class="col-lg-6">
                                            <label>City:</label>
                                            <select name="ENQUIRY_CITY" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1" <?php if ($this->input->post('ENQUIRY_CITY') == '1') {
                                                  echo 'selected';  
                                                } ?>>MUMBAI</option>
                                                <option value="2" <?php if ($this->input->post('ENQUIRY_CITY') == '2') {
                                                  echo 'selected';  
                                                } ?>>NAVI MUMBAI</option>
                                                <option value="3" <?php if ($this->input->post('ENQUIRY_CITY') == '3') {
                                                  echo 'selected';  
                                                } ?>>THANE</option>
                                                <option value="4" <?php if ($this->input->post('ENQUIRY_CITY') == '4') {
                                                  echo 'selected';  
                                                } ?>>PUNE</option>
                                                <option value="5" <?php if ($this->input->post('ENQUIRY_CITY') == '5') {
                                                  echo 'selected';  
                                                } ?>>NASHIK</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Zip:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_ZIP" value="<?php echo $this->input->post('ENQUIRY_ZIP');?>" id="ENQUIRY_ZIP" /> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label>Course Interested/Suggested <span class="text-danger">*</span> <br> Press CTRL+ Select Multiple Course:&nbsp;</label>

                                    <select class="form-control course_interested" name="COURSE_INTERESTED[]" id="COURSE_INTERESTED" multiple="">
                                        <?php
                                            if($this->input->post('COURSE_INTERESTED') != ''){
                                               $selected_course =  $this->input->post('COURSE_INTERESTED');
                                            }
                                            else{
                                                $selected_course = explode(",",$enq['COURSE_INTERESTED']);
                                            }
                                            foreach($courses as $course){ 
                                            $isSelected = in_array($course['COURSE_NAME'],$selected_course) ? "selected='selected'" : "";
                                        ?>
                                            <option value="<?php echo $course['COURSE_NAME']; ?>" <?php echo $isSelected; ?>><?php echo $course['COURSE_NAME']; ?></option>
                                        <?php } ?>
                                        <option value=''>Other</option>
                                    </select>
                                    <span class="text-danger"><?php echo form_error('COURSE_INTERESTED[]'); ?></span>
                                </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                       <ul class="list-group courses_seld">
                                       <?php
                                            // $selected_course = explode(",",$enq['COURSE_INTERESTED']);
                                            foreach($courses as $course){ 
                                            if(in_array($course['COURSE_NAME'],$selected_course)){ ?>
                                        <li class="list-group-item">
                                            <?php echo $course['COURSE_NAME'];; ?>
                                        </li>
                                        <?php } } ?>
                                       </ul>
                                   </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Gender:</label>
                                            <select name="ENQUIRY_GENDER" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1"<?php echo ($this->input->post('ENQUIRY_GENDER')=="1") ? ' selected="selected"' : '';?>>Male</option>
                                                <option value="2"<?php echo ($this->input->post('ENQUIRY_GENDER')=="2") ? ' selected="selected"' : '';?>>Female</option>
                                            </select>
                                        </div>

 
                                        <div class="col-lg-6" style="float:right">
                                            <label>D.O.B: <span class="text-danger">*</span></label>
                                            <div class="input-group"> <input class="form-control enquiry_date" name="ENQUIRY_DATEOFBIRTH" id="ENQUIRY_DATEOFBIRTH" value="<?php echo $this->input->post('ENQUIRY_DATEOFBIRTH')?>" placeholder="DD/MM/YYYY" aria-describedby="age" maxlength="10"> <span class="input-group-addon" id="age"></span> </div>

                                             <span class="text-danger"><?php echo form_error('ENQUIRY_DATEOFBIRTH'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Email: <span class="text-danger">*</span></label>
                                            <input type="email" class="form-control" name="ENQUIRY_EMAIL" value="<?php echo $this->input->post('ENQUIRY_EMAIL')?>"> 
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_EMAIL'); ?></span>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            
                                            <label>Centre: <span class="text-danger">*</span></label>
                                            <div id="SubsourceHint">
                                                <select name="CENTRE_ID" class="form-control">
                                                    <option value=''>Please Select</option>
                                                    <?php foreach($centres as $centre){ ?>
                                                    <option value="<?php echo $centre['CENTRE_ID'] ?>" <?php echo ($centre['CENTRE_ID'] == $this->input->post('CENTRE_ID')) ? ' selected="selected"' : '';?>><?php echo $centre['CENTRE_NAME'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                             <span class="text-danger"><?php echo form_error('CENTRE_ID'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Occupation:</label>

                                            <select name="OCCUPATION" class="form-control">
                                                <option value="">Please Select</option>
                                                <?php 
                                                foreach($occupations as $occupation){
                                                 ?>
                                                <option value="<?php echo $occupation['CONTROLFILE_ID']; ?>" <?php echo ($this->input->post('OCCUPATION')==$occupation['CONTROLFILE_ID']) ? ' selected="selected"' : '';?>><?php echo $occupation['CONTROLFILE_VALUE']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Education: <span class="text-danger">*</span></label>
                                            <select name="ENQUIRY_EDUCATION" class="form-control">
                                                <option value=''>Please Select</option>
                                                <?php foreach($educations as $education){ ?>
                                               <option value="<?php echo $education['CONTROLFILE_ID']; ?>" <?php echo ($education['CONTROLFILE_ID'] == $this->input->post('ENQUIRY_EDUCATION')) ? ' selected="selected"' : '';?>><?php echo $education['CONTROLFILE_VALUE']; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_EDUCATION'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Prefered Batch Time: <span class="text-danger">*</span></label>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select class="form-control" name="PREFERREDED_TIME1" id="PREFERREDED_TIME1">
                                                        <option value="">Please Select</option>
                                                        <?php foreach($timing as $time){ ?>
                                                        <option value="<?php echo $time['time_master_id'] ?>" <?php echo ($time['time_master_id'] == $this->input->post('PREFERREDED_TIME1')) ? ' selected="selected"' : '';?>><?php echo $time['time'] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-12">
                                                    <select class="form-control" name="PREFERREDED_TIME2" id="PREFERREDED_TIME2">
                                                        <option value="">Please Select</option>
                                                        <?php foreach($timing as $time){ ?>
                                                        <option value="<?php echo $time['time_master_id'] ?>" <?php echo ($time['time_master_id'] == $this->input->post('PREFERREDED_TIME2')) ? ' selected="selected"' : '';?>><?php echo $time['time'] ?></option>
                                                        <?php } ?>
                                                    </select> 
                                                </div>
                                                <div class="col-md-12"> 
                                                    <select class="form-control" name="PREFERREDED_TIME3" id="PREFERREDED_TIME3">
                                                        <option value="">Please Select</option>
                                                        <?php foreach($timing as $time){ ?>
                                                        <option value="<?php echo $time['time_master_id'] ?>" <?php echo ($time['time_master_id'] == $this->input->post('PREFERREDED_TIME3')) ? ' selected="selected"' : '';?>><?php echo $time['time'] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <span class="text-danger"><?php echo form_error('PREFERREDED_TIMES'); ?></span>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Prefered Contact Time:</label>
                                            <input id="PREFERRED_CONTACTTIME" class="form-control" name="PREFERRED_CONTACTTIME" value="<?php echo $this->input->post('PREFERRED_CONTACTTIME');?>"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Enquiry Date:</label>
                                            <input type="text" id="ENQUIRY_DATE" class="form-control add_enquiry_date" name="ENQUIRY_DATE" value="<?php echo $this->input->post('ENQUIRY_DATE'); ?>"> 
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Enquiry Form No.<span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_FORM_NO" value="<?php echo $this->input->post('ENQUIRY_FORM_NO'); ?>" id="ENQUIRY_FORM_NO"> 
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_FORM_NO'); ?></span>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Enquiry Time : <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_TIME" id="ENQUIRY_TIME" value="<?php if($this->input->post('ENQUIRY_TIME') != ''){echo $this->input->post('ENQUIRY_TIME');} ?>" maxlength="10">
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Mobile No.(Work/Home): <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_MOBILE_NO" value="<?php if($this->input->post('ENQUIRY_MOBILE_NO') != ''){echo $this->input->post('ENQUIRY_MOBILE_NO');} else{echo $enq['MOBILE'];} ?>" id="ENQUIRY_MOBILE_NO" maxlength="10"> 
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_MOBILE_NO'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Parent's No: <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_PARENT_NO" value="<?php if($this->input->post('ENQUIRY_PARENT_NO') != ''){echo $this->input->post('ENQUIRY_PARENT_NO');}else{ echo $enq['PARENT_NUMBER']; } ?>" id="ENQUIRY_PARENT_NO" maxlength="10"> 
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_PARENT_NO'); ?></span>
                                            </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Heard About:</label>
                                            <select name="HEARD_ABOUTUS" class="form-control">
                                                <option value="">Please Select</option>
                                                <?php foreach($heardabout as $heard){ ?>
                                                <option value="<?php echo $heard['SOURCE_ID']; ?>" <?php if($this->input->post('HEARD_ABOUTUS')!= ''){echo ($heard['SOURCE_ID'] == $this->input->post('HEARD_ABOUTUS')) ? ' selected="selected"' : '';}else{echo ($heard['SOURCE_ID'] == $source_id) ? ' selected="selected"' : '';}?>><?php echo $heard['controlfile_value']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Enquiry Mode:</label>
                                            <select class="form-control" style="width:100% ;" name="ENQUIRY_MODE" id="ENQUIRY_MODE">
                                                <option value="1" <?php if($this->input->post('ENQUIRY_MODE') == '1'){echo 'selected';} ?>>House Calls</option>
                                                <option value="2" <?php if($this->input->post('ENQUIRY_MODE') == '2'){echo 'selected';} ?>>Mailers</option>
                                                <option value="3" <?php if($this->input->post('ENQUIRY_MODE') == '3'){echo 'selected';} ?>>Telephone</option>
                                                <option value="4" <?php if($this->input->post('ENQUIRY_MODE') == '4'){echo 'selected';} ?>>Tie ups</option>
                                                <option value="5" <?php if($this->input->post('ENQUIRY_MODE') == '5'){echo 'selected';} ?>>Walk ins</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Enquiry Handle By: <span class="text-danger">*</span></label>
                                            <select name="ENQUIRY_HANDELED_BY" class="form-control" required>
                                                <option value=''>Please Select</option>
                                                <?php 
                                                    foreach($employees as $employee){ ?>
                                                    <option value="<?php echo $employee['EMPLOYEE_ID'] ?>" <?php if($this->input->post('ENQUIRY_HANDELED_BY') != ''){  echo ($employee['EMPLOYEE_ID'] == $enq['ENQUIRY_HANDELED_BY']) ? ' selected="selected"' : '';}else{
                                                          echo ($employee['EMPLOYEE_ID'] == $this->input->post('ENQUIRY_HANDELED_BY')) ? ' selected="selected"' : '';  
                                                        }?>><?php echo $employee['EMP_FNAME'].' '.$employee['EMP_LASTNAME'] ?></option>
                                                <?php }
                                                ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_HANDELED_BY'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Remarks: </label>
                                            <input class="form-control" type="text" name="REMARKS" value="<?php echo $this->input->post('REMARKS'); ?>" id="REMARKS"> 
                                            <span class="text-danger"><?php echo form_error('REMARKS'); ?></span>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Course Offered in : <span class="text-danger">*</span> </label>
                                            <div class="input-group">
                                                  <span class="input-group-addon" id="basic-addon1">&#8377;</span>
                                                  <input class="form-control" type="text" name="COURSE_OFFERED_IN" value="<?php echo $this->input->post('COURSE_OFFERED_IN'); ?>" id="CourseOffered" aria-describedby="basic-addon1">
                                                </div>
                                            <span class="text-danger"><?php echo form_error('COURSE_OFFERED_IN'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <button type='submit' class='btn btn-primary'>Save</button>&nbsp;
                        </div>
                        <?php } } else {?>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name: <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="ENQUIRY_FIRSTNAME" value="<?php echo $this->input->post('ENQUIRY_FIRSTNAME') ?>" id="ENQUIRY_FIRSTNAME " required="required" /> 
                             <span class="text-danger"><?php echo form_error('ENQUIRY_FIRSTNAME'); ?></span>
                            </div>
                            <div class="form-group">
                                <label>Middle Name:</label>
                                <input class="form-control" type="text" name="ENQUIRY_MIDDLENAME" value="<?php echo $this->input->post('ENQUIRY_MIDDLENAME') ?>" id="ENQUIRY_MIDDLENAME" /> </div>
                            <div class="form-group">
                                <label>Last Name: <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="ENQUIRY_LASTNAME" value="<?php echo $this->input->post('ENQUIRY_LASTNAME') ?>" id="ENQUIRY_LASTNAME" required="required" /> 
                                <span class="text-danger"><?php echo form_error('ENQUIRY_LASTNAME'); ?></span>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Flat/Room/Wing No.:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_ADDRESS1" id="ENQUIRY_ADDRESS1" value="<?php echo $this->input->post('ENQUIRY_ADDRESS1') ?>"> </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Bldg./Plot/Chawl No.:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_ADDRESS2" id="ENQUIRY_ADDRESS2" value="<?php echo $this->input->post('ENQUIRY_ADDRESS2') ?>"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Area/Landmark:</label>
                                <input class="form-control" type="text" name="ENQUIRY_AREA" value="<?php echo $this->input->post('ENQUIRY_AREA') ?>" id="ENQUIRY_AREA" /> </div>
                            <div class="form-group">
                                <label>Nearest Railway Station: <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="ENQUIRY_SUBURB" value="<?php echo $this->input->post('ENQUIRY_SUBURB') ?>" id="ENQUIRY_SUBURB" /> 
                                <span class="text-danger"><?php echo form_error('ENQUIRY_SUBURB'); ?></span>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>State:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_STATE" value="MAHARASHTRA" readonly="readonly" id="ENQUIRY_STATE" /> </div>
                                         <div class="col-lg-6">
                                            <label>City:</label>
                                            <select name="ENQUIRY_CITY" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1" <?php if ($this->input->post('ENQUIRY_CITY') == '1') {
                                                  echo 'selected';  
                                                } ?>>MUMBAI</option>
                                                <option value="2" <?php if ($this->input->post('ENQUIRY_CITY') == '2') {
                                                  echo 'selected';  
                                                } ?>>NAVI MUMBAI</option>
                                                <option value="3" <?php if ($this->input->post('ENQUIRY_CITY') == '3') {
                                                  echo 'selected';  
                                                } ?>>THANE</option>
                                                <option value="4" <?php if ($this->input->post('ENQUIRY_CITY') == '4') {
                                                  echo 'selected';  
                                                } ?>>PUNE</option>
                                                <option value="5" <?php if ($this->input->post('ENQUIRY_CITY') == '5') {
                                                  echo 'selected';  
                                                } ?>>NASHIK</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Zip:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_ZIP" value="<?php echo $this->input->post('ENQUIRY_ZIP') ?>" id="ENQUIRY_ZIP" /> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Course Interested/Suggested <span class="text-danger">*</span> <br> Press CTRL+ Select Multiple Course:&nbsp;</label>

                                <select class="form-control course_interested" style="width:100%;height:158px" name="COURSE_INTERESTED[]" id="COURSE_INTERESTED" multiple="">
                                    <?php
                                        foreach($courses as $course){ 

                                            if(in_array($course['COURSE_ID'],$this->input->post('COURSE_INTERESTED')))
                                            {
                                                $selected = 'selected';
                                                
                                            }
                                            else
                                            {
                                                $selected = '';   
                                            }
                                    ?>
                                        <option value="<?php echo $course['COURSE_ID']; ?>" <?php echo $selected;?>><?php echo $course['COURSE_NAME']; ?></option>
                                    <?php } ?>
                                    <option value=''>Other</option>
                                </select>
                                <span class="text-danger"><?php echo form_error('COURSE_INTERESTED[]'); ?></span>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="list-group courses_seld"></ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Gender:</label>
                                            <select name="ENQUIRY_GENDER" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1" <?php if($this->input->post('ENQUIRY_GENDER') == '1'){echo 'selected';} ?>>Male</option>
                                                <option value="2" <?php if($this->input->post('ENQUIRY_GENDER') == '2'){echo 'selected';} ?>>Female</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>D.O.B: <span class="text-danger">*</span></label>
                                            <div class="input-group"> <input class="form-control enquiry_date" name="ENQUIRY_DATEOFBIRTH" id="ENQUIRY_DATEOFBIRTH" placeholder="DD/MM/YYYY" aria-describedby="age" value="<?php echo $this->input->post('ENQUIRY_DATEOFBIRTH'); ?>" maxlength="10"> <span class="input-group-addon" id="age">NaN</span> </div>

                                            
                                             <span class="text-danger"><?php echo form_error('ENQUIRY_DATEOFBIRTH'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Email: <span class="text-danger">*</span></label>
                                            <input type="email" class="form-control" name="ENQUIRY_EMAIL" value="<?php echo $this->input->post('ENQUIRY_EMAIL'); ?>"> 
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_EMAIL'); ?></span>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Centre: <span class="text-danger">*</span></label>
                                            <div id="SubsourceHint">
                                                <?php
                                                if(($this->session->userdata('admin_data')[0]['ROLE_ID'] == '2') || ($this->session->userdata('admin_data')[0]['ROLE_ID'] == '3')){
                                                ?>
                                                <select name="SUGGESTED_CENTRE_ID" class="form-control">
                                                    <option value=''>Please Select</option>
                                                    <?php foreach($centres as $centre){ ?>
                                                    <option value="<?php echo $centre['CENTRE_ID'] ?>" <?php echo ($centre['CENTRE_ID'] == $this->input->post('SUGGESTED_CENTRE_ID')) ? ' selected="selected"' : '';?>><?php echo $centre['CENTRE_NAME'] ?></option>
                                                    <?php } ?>
                                                </select>
                                                <?php }else{ ?>
                                                <select name="SUGGESTED_CENTRE_ID" class="form-control">
                                                    <?php 
                                                    foreach($centres as $centre){ 
                                                    if($centre['CENTRE_ID'] == $this->session->userdata('admin_data')[0]['CENTRE_ID']){
                                                        ?>
                                                    <option value="<?php echo $centre['CENTRE_ID'] ?>"><?php echo $centre['CENTRE_NAME'] ?></option>
                                                    <?php } 
                                                } ?>
                                                </select>

                                                <?php } ?>
                                            </div>
                                             <span class="text-danger"><?php echo form_error('SUGGESTED_CENTRE_ID'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Occupation:</label>
                                            <select name="OCCUPATION" class="form-control">
                                                <option value="">Please Select</option>
                                                <?php foreach($occupations as $occupation){ ?>
                                                <option value="<?php echo $occupation['CONTROLFILE_ID']; ?>" <?php echo ($occupation['CONTROLFILE_ID'] == $this->input->post('OCCUPATION')) ? ' selected="selected"' : '';?>><?php echo $occupation['CONTROLFILE_VALUE']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Education: <span class="text-danger">*</span></label>
                                            <select name="ENQUIRY_EDUCATION" class="form-control">
                                                <option value=''>Please Select</option>
                                                <?php foreach($educations as $education){ ?>
                                               <option value="<?php echo $education['CONTROLFILE_ID']; ?>" <?php echo ($education['CONTROLFILE_ID'] == $this->input->post('ENQUIRY_EDUCATION')) ? ' selected="selected"' : '';?>><?php echo $education['CONTROLFILE_VALUE']; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_EDUCATION'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Prefered Batch Time: <span class="text-danger">*</span></label>
                                            <div class="row">
                                            <?php 
                                                for($i=1;$i<=3;$i++){
                                            ?>
                                                <div class="col-md-12">
                                                    <select class="form-control" name="PREFERREDED_TIME<?php echo $i; ?>" id="PREFERREDED_TIME<?php echo $i; ?>">
                                                        <option value="">Please Select</option>
                                                        <?php foreach($timing as $time){ ?>
                                                        <option value="<?php echo $time['time_master_id'] ?>" <?php echo ($time['time_master_id'] == $this->input->post('PREFERREDED_TIME'.$i)) ? ' selected="selected"' : '';?>><?php echo $time['time'] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            <?php } ?>
                                            </div>
                                            <span class="text-danger"><?php echo form_error('PREFERREDED_TIMES'); ?></span>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Enquiry Date:</label>
                                            <input type="text" id="ENQUIRY_DATE" class="form-control add_enquiry_date" name="ENQUIRY_DATE" value="<?php echo $this->input->post('ENQUIRY_DATE'); ?>"> 
                                        </div>
                                         <div class="col-lg-6">
                                            <label>Enquiry Form No.<span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_FORM_NO" value="<?php echo $this->input->post('ENQUIRY_FORM_NO'); ?>" id="ENQUIRY_FORM_NO"> 
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_FORM_NO'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Mobile No.(Work/Home): <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_MOBILE_NO" value="<?php echo $this->input->post('ENQUIRY_MOBILE_NO'); ?>" id="ENQUIRY_MOBILE_NO" maxlength="10"> 
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_MOBILE_NO'); ?></span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Parent's No:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_TELEPHONE" value="<?php echo $this->input->post('ENQUIRY_TELEPHONE'); ?>" id="ENQUIRY_TELEPHONE" maxlength="10"> 
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_TELEPHONE'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Enquiry Mode:</label>
                                            <select class="form-control" style="width:100% ;" name="ENQUIRY_MODE" id="ENQUIRY_MODE">
                                                <option value="1" <?php if($this->input->post('ENQUIRY_MODE') == '1'){echo 'selected';} ?>>House Calls</option>
                                                <option value="2" <?php if($this->input->post('ENQUIRY_MODE') == '2'){echo 'selected';} ?>>Mailers</option>
                                                <option value="3" <?php if($this->input->post('ENQUIRY_MODE') == '3'){echo 'selected';} ?>>Telephone</option>
                                                <option value="4" <?php if($this->input->post('ENQUIRY_MODE') == '4'){echo 'selected';} ?>>Tie ups</option>
                                                <option value="5" <?php if($this->input->post('ENQUIRY_MODE') == '5'){echo 'selected';}if($this->input->post('ENQUIRY_MODE') == ''){echo 'selected';} ?>>Walk ins</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Enquiry Handle By: <span class="text-danger">*</span></label>
                                            <select name="ENQUIRY_HANDELED_BY" class="form-control" required>
                                                <option value=''>Please Select</option>
                                                <?php 
                                                    foreach($employees as $employee){ ?>
                                                    <option value="<?php echo $employee['EMPLOYEE_ID'] ?>" <?php echo ($employee['EMPLOYEE_ID'] == $this->input->post('ENQUIRY_HANDELED_BY')) ? ' selected="selected"' : '';?>><?php echo $employee['EMP_FNAME'].' '.$employee['EMP_LASTNAME'] ?></option>
                                                <?php }
                                                ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_HANDELED_BY'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Heard About:</label>
                                            <select name="HEARD_ABOUTUS" class="form-control">
                                                <option value="">Please Select</option>
                                                <?php foreach($heardabout as $heard){ ?>
                                                <option value="<?php echo $heard['SOURCE_ID']; ?>" <?php echo ($heard['SOURCE_ID'] == $this->input->post('HEARD_ABOUTUS')) ? ' selected="selected"' : '';?>><?php echo $heard['controlfile_value']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Course Offered in : <span class="text-danger">*</span> </label>
                                            <div class="input-group">
                                                  <span class="input-group-addon" id="basic-addon1">&#8377;</span>
                                                  <input class="form-control" type="text" name="COURSE_OFFERED_IN" value="<?php echo $this->input->post('COURSE_OFFERED_IN'); ?>" id="CourseOffered" aria-describedby="basic-addon1">
                                                </div>
                                            <span class="text-danger"><?php echo form_error('COURSE_OFFERED_IN'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-12">
                                            <label>Remarks:</label>
                                            <input class="form-control" type="text" name="REMARKS" value="<?php echo $this->input->post('REMARKS') ?>" id="REMARKS"> 
                                            <span class="text-danger"><?php echo form_error('REMARKS'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <button type='submit' class='btn btn-primary'>Save</button>&nbsp;
                        </div>
                    <?php } echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>