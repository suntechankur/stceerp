<div class="gapping"></div>
<div class="create_batch_form">
    <div id="box">
        <h2>Edit Enquiry</h2></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- start message area -->

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('msg')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php $msg = $this->session->flashdata('msg'); echo $msg['msg']; ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>


<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info1')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
</div>
<?php } ?>

<?php if($this->session->flashdata('warning')) { ?>
<div class="alert alert-warning">
<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php } ?>

<!-- End message area --> 
                </div>
                <div class="panel-body">
                   <?php echo form_open(); ?>
                       <?php
                        /* This Form is for convert Enquiry */
                        foreach($enqData as $enq){
                        ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name: <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="ENQUIRY_FIRSTNAME" value="<?php echo $enq['ENQUIRY_FIRSTNAME']; ?>" id="ENQUIRY_FIRSTNAME	" required="required" /> 
                             <span class="text-danger"><?php echo form_error('ENQUIRY_FIRSTNAME'); ?></span>
                            </div>
                            <div class="form-group">
                                <label>Middle Name:</label>
                                <input class="form-control" type="text" name="ENQUIRY_MIDDLENAME" value="<?php echo $enq['ENQUIRY_MIDDLENAME']; ?>" id="ENQUIRY_MIDDLENAME" /> </div>
                            <div class="form-group">
                                <label>Last Name: <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="ENQUIRY_LASTNAME" value="<?php echo $enq['ENQUIRY_LASTNAME']; ?>" id="ENQUIRY_LASTNAME" required="required" /> 
                                <span class="text-danger"><?php echo form_error('ENQUIRY_LASTNAME'); ?></span>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Flat/Room/Wing No.:<span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_ADDRESS1" id="ENQUIRY_ADDRESS1" value="<?php echo $enq['ENQUIRY_ADDRESS1']; ?>">
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_ADDRESS1'); ?></span>
                                             </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Bldg./Plot/Chawl No.:<span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_ADDRESS2" id="ENQUIRY_ADDRESS2" value="<?php echo $enq['ENQUIRY_ADDRESS2']; ?>">
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_ADDRESS2'); ?></span>
                                             </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Area/Landmark:<span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="ENQUIRY_AREA" value="<?php echo $enq['ENQUIRY_AREA']; ?>" id="ENQUIRY_AREA" />
                                <span class="text-danger"><?php echo form_error('ENQUIRY_AREA'); ?></span>
                                 </div>
                            <div class="form-group">
                                <label>Nearest Railway Station: <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="ENQUIRY_SUBURB" value="<?php echo $enq['ENQUIRY_SUBURB']; ?>" id="ENQUIRY_SUBURB" /> 
                                <span class="text-danger"><?php echo form_error('ENQUIRY_SUBURB'); ?></span>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>State:<span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_STATE" value="MAHARASHTRA" readonly="readonly" id="ENQUIRY_STATE" />
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_STATE'); ?></span>
                                             </div>
                                         <div class="col-lg-6">
                                            <label>City:<span class="text-danger">*</span></label>
                                            <select name="ENQUIRY_CITY" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1" <?php if($enq['ENQUIRY_CITY'] == '1'){echo 'selected';} ?> >MUMBAI</option>
                                                <option value="2" <?php if($enq['ENQUIRY_CITY'] == '2'){echo 'selected';} ?>>NAVI MUMBAI</option>
                                                <option value="3" <?php if($enq['ENQUIRY_CITY'] == '3'){echo 'selected';} ?>>THANE</option>
                                                <option value="4" <?php if($enq['ENQUIRY_CITY'] == '4'){echo 'selected';} ?>>PUNE</option>
                                                <option value="5" <?php if($enq['ENQUIRY_CITY'] == '5'){echo 'selected';} ?>>NASHIK</option>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_CITY'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Zip:</label>
                                            <input class="form-control" type="text" name="ENQUIRY_ZIP" value="<?php echo $enq['ENQUIRY_ZIP']; ?>" id="ENQUIRY_ZIP" />
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_ZIP'); ?></span>
                                             </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Course Interested/Suggested <span class="text-danger">*</span> <br> Press CTRL+ Select Multiple Course:&nbsp;</label>
                                <select class="form-control" style="width:100%;height:158px" name="COURSE_INTERESTED[]" id="COURSE_INTERESTED" multiple="">
                                    <?php
                                        $selected_course = explode(",",$enq['COURSE_INTERESTED']);
                                        foreach($courses as $course){ 
                                        $isSelected = in_array($course['COURSE_ID'],$selected_course) ? "selected='selected'" : "";
                                    ?>
                                        <option value="<?php echo $course['COURSE_ID']; ?>" <?php echo $isSelected; ?>><?php echo $course['COURSE_NAME']; ?></option>
                                    <?php } ?>
                                    <option value=''>Other</option>
                                </select>
                                <span class="text-danger"><?php echo form_error('COURSE_INTERESTED[]'); ?></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Gender:<span class="text-danger">*</span></label>
                                            <select name="ENQUIRY_GENDER" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1" <?php if($enq['ENQUIRY_GENDER'] == '1'){ echo 'selected'; } ?> >Male</option>
                                                <option value="2" <?php if($enq['ENQUIRY_GENDER'] == '2'){ echo 'selected'; } ?>>Female</option>
                                                <option value="3" <?php if($enq['ENQUIRY_GENDER'] == '3'){ echo 'selected'; } ?>>Transender</option>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_GENDER'); ?></span>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>D.O.B: <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control enquiry_date" name="ENQUIRY_DATEOFBIRTH" id="ENQUIRY_DATEOFBIRTH" value="<?php echo date("d/m/Y",strtotime($enq['ENQUIRY_DATEOFBIRTH'])); ?>"> 
                                             <span class="text-danger"><?php echo form_error('ENQUIRY_DATEOFBIRTH'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Email: <span class="text-danger">*</span></label>
                                            <input type="email" class="form-control" name="ENQUIRY_EMAIL" value="<?php echo $enq['ENQUIRY_EMAIL']; ?>"> 
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_EMAIL'); ?></span>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Centre: <span class="text-danger">*</span></label>
                                            <div id="SubsourceHint">
                                                <select name="CENTRE_ID" class="form-control">
                                                    <option value=''>Please Select</option>
                                                    <?php foreach($centres as $centre){ ?>
                                                    <option value="<?php echo $centre['CENTRE_ID'] ?>" <?php echo ($centre['CENTRE_ID'] == $enq['CENTRE_ID']) ? ' selected="selected"' : '';?>><?php echo $centre['CENTRE_NAME'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                             <span class="text-danger"><?php echo form_error('CENTRE_ID'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Occupation:<span class="text-danger">*</span></label>
                                            <select name="OCCUPATION" class="form-control">
                                                <option value="">Please Select</option>
                                                <?php foreach($occupations as $occupation){ ?>
                                                <option value="<?php echo $occupation['CONTROLFILE_ID']; ?>" <?php echo ($occupation['CONTROLFILE_ID'] == $enq['OCCUPATION']) ? ' selected="selected"' : '';?>><?php echo $occupation['CONTROLFILE_VALUE']; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('OCCUPATION'); ?></span>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Education: <span class="text-danger">*</span></label>
                                            <select name="ENQUIRY_EDUCATION" class="form-control">
                                                <option value=''>Please Select</option>
                                                <?php foreach($educations as $education){ ?>
                                               <option value="<?php echo $education['CONTROLFILE_ID']; ?>" <?php echo ($education['CONTROLFILE_ID'] == $enq['ENQUIRY_EDUCATION']) ? ' selected="selected"' : '';?>><?php echo $education['CONTROLFILE_VALUE']; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_EDUCATION'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Prefered Batch Time: <span class="text-danger">*</span></label>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select class="form-control" name="PREFERREDED_TIME1" id="PREFERREDED_TIME1">
                                                        <option value="">Please Select</option>
                                                        <?php foreach($timing as $time){ ?>
                                                        <option value="<?php echo $time['TIME_MASTER_ID'] ?>" <?php echo ($time['TIME_MASTER_ID'] == $enq['PREFERREDED_TIME1']) ? ' selected="selected"' : '';?>><?php echo $time['TIME'] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-12">
                                                    <select class="form-control" name="PREFERREDED_TIME2" id="PREFERREDED_TIME2">
                                                        <option value="">Please Select</option>
                                                        <?php foreach($timing as $time){ ?>
                                                        <option value="<?php echo $time['TIME_MASTER_ID'] ?>" <?php echo ($time['TIME_MASTER_ID'] == $enq['PREFERREDED_TIME2']) ? ' selected="selected"' : '';?>><?php echo $time['TIME'] ?></option>
                                                        <?php } ?>
                                                    </select> 
                                                </div>
                                                <div class="col-md-12"> 
                                                    <select class="form-control" name="PREFERREDED_TIME3" id="PREFERREDED_TIME3">
                                                        <option value="">Please Select</option>
                                                        <?php foreach($timing as $time){ ?>
                                                        <option value="<?php echo $time['TIME_MASTER_ID'] ?>" <?php echo ($time['TIME_MASTER_ID'] == $enq['PREFERREDED_TIME3']) ? ' selected="selected"' : '';?>><?php echo $time['TIME'] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <span class="text-danger"><?php echo form_error('PREFERREDED_TIMES'); ?></span>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Prefered Contact Time:</label>
                                            <input id="PREFERRED_CONTACTTIME" class="form-control" name="PREFERRED_CONTACTTIME" value="<?php echo $enq['PREFERRED_CONTACTTIME']; ?>"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Enquiry Date:</label>
                                            <input type="text" id="ENQUIRY_DATE" class="form-control enquiry_date" name="ENQUIRY_DATE" value="<?php $ENQUIRY_DATE = date("d/m/Y", strtotime($enq['ENQUIRY_DATE'])); echo $ENQUIRY_DATE; ?>"> 
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Enquiry Form No.<span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_FORM_NO" value="<?php echo $enq['ENQUIRY_FORM_NO']; ?>" id="ENQUIRY_FORM_NO">
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_FORM_NO'); ?></span> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Mobile No.(Work/Home): <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_MOBILE_NO" value="<?php echo $enq['ENQUIRY_MOBILE_NO']; ?>" id="ENQUIRY_MOBILE_NO" maxlength="10"> 
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_MOBILE_NO'); ?></span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Parent's No: <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" name="ENQUIRY_PARENT_NO" value="<?php echo $enq['ENQUIRY_PARENT_NO'] ?>" id="ENQUIRY_PARENT_NO" maxlength="10"> 
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_PARENT_NO'); ?></span>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Enquiry Mode:</label>
                                            <select class="form-control" style="width:100% ;" name="ENQUIRY_MODE" id="ENQUIRY_MODE">
                                                <option value="1">House Calls</option>
                                                <option value="2">Mailers</option>
                                                <option value="3">Telephone</option>
                                                <option value="4">Tie ups</option>
                                                <option value="5">Walk ins</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Enquiry Handle By: <span class="text-danger">*</span></label>
                                            <select name="ENQUIRY_HANDELED_BY" class="form-control" required>
                                                <option value=''>Please Select</option>
                                                <?php 
                                                    foreach($employees as $employee){ ?>
                                                    <option value="<?php echo $employee['EMPLOYEE_ID'] ?>" <?php if(isset($enq['ENQUIRY_HANDELED_BY'])){ echo ($employee['EMPLOYEE_ID'] == $enq['ENQUIRY_HANDELED_BY']) ? ' selected="selected"' : '';}?>><?php echo $employee['EMP_FNAME'].' '.$employee['EMP_LASTNAME'] ?></option>
                                                <?php }
                                                ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_HANDELED_BY'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Heard About:</label>
                                            <select name="HEARD_ABOUTUS" class="form-control">
                                                <option value="">Please Select</option>
                                                <?php foreach($heardabout as $heard){ ?>
                                                <option value="<?php echo $heard['SOURCE_ID']; ?>" <?php echo ($heard['SOURCE_ID'] == $source_id) ? ' selected="selected"' : '';?>><?php echo $heard['SOURCE_SUBSOURCE']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Course Offered in : <span class="text-danger">*</span> </label>
                                            <div class="input-group">
                                                  <span class="input-group-addon" id="basic-addon1">&#8377;</span>
                                                  <input class="form-control" type="text" name="COURSE_OFFERED_IN" value="<?php echo $enq['COURSE_OFFERED_IN']; ?>" id="CourseOffered" aria-describedby="basic-addon1">
                                                </div>
                                            <span class="text-danger"><?php echo form_error('COURSE_OFFERED_IN'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-12">
                                            <label>Remarks: </label>
                                            <input class="form-control" type="text" name="REMARKS" value="<?php echo $enq['REMARKS']; ?>" id="REMARKS"> 
                                            <span class="text-danger"><?php echo form_error('REMARKS'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <button type='submit' class='btn btn-primary'>Update</button>&nbsp;
                        </div>
                        <?php } 
                        echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>