<div class="col-md-12 col-sm-12">
  <div class="create_batch_form">
<div id="box">
<h2>EOD Reports</h2>
</div>
<div class="row">
<div class="col-sm-12">
								<div class="panel panel-default">
                                <div class="panel-heading">
								<!-- start message area -->

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('msg')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php $msg = $this->session->flashdata('msg'); echo $msg['msg']; ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>


<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info1')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
</div>
<?php } ?>

<?php if($this->session->flashdata('warning')) { ?>
<div class="alert alert-warning">
<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php } ?>

<!-- End message area --> 
                                </div>
									<div class="panel-body">
                    <div class="col-lg-12">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>EOD Date: <span class="text-danger">*</span></label>
                          <input type="text" class="form-control add_enquiry_date">
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-md-3"></div>
                        <div class="col-md-3 text-center">
                          <label>Today</label>
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-md-3 text-center">
                          <label>Till Date</label>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Admissions
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Billing
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Collection
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          New Collection
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          PDC Collection
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Outstanding
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Enquiry (Total)
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          SAIG
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          SAIG + Degree Admissions
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Only Degree Admissions
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Animation SAIG/Degree Admissions
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Programming SAIG/Degree Admissions
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          H/W & Networking SAIG/Degree Admissions
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Student Meetings
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Parent Meetings
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Faculty Meeting
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          SMS Done
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Emails Done
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Database Collected
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Tieups Done
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Data Calling Done
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Data Calling Walkins
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Data Calling Admissions
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          House Calls
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Loyalty House Calls
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Direct Walkins
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Direct Walkin Admissions
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          HO Enquiries Walkins
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          HO Walkin Admissions
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Total Upgrades
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Upgrade to SAIG/Degree
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Reference
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3 text-center">
                          Defaulter's Fees Received
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                        <div class="col-md-3">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-1">
                          <span class="text-danger">*</span>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3">
                          EOD Details:
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-md-12">
                          <textarea class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 10px;">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">
                          <button class="btn btn-primary">Save</button>
                        </div>
                    </div>
									</div>
								</div>
							</div>
							</div>
							</div>

</div>