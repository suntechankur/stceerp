<div class="col-md-12 col-sm-12">
<div class="create_batch_form">
<div id="box">
<h2>Enquiry Search</h2>
</div>
<div class="row">
<div class="col-sm-12">
								<div class="panel panel-default">
                                <div class="panel-heading">
								<!-- start message area -->

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('msg')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php $msg = $this->session->flashdata('msg'); echo $msg['msg']; ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>


<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info1')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
</div>
<?php } ?>

<?php if($this->session->flashdata('warning')) { ?>
<div class="alert alert-warning">
<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php } ?>

<!-- End message area -->
                                </div>
									<div class="panel-body">
                    <?php echo form_open(); ?>
											<div class="col-md-12">
												<div class="form-group">
													<div class="row" style="margin-left:-30px;">
														<div class="col-lg-12" style="margin-left:0px;">
															<div class="col-lg-12">
																<p style="color:red;font-weight:bold;">*Note : Kindly try to search enquiry details by unique value by following below steps.</p><ol style="font-weight:bold;"><li>Try to search data by mobile number</li><li>or Email Id</li><li>or by Firstname And Lastname</li><li>And At the end if you are not find your desired data then you can go for adding enquiry data Manually.</li></ol>
																<hr style="background-image: -moz-linear-gradient(left,rgba(0,0,0,0),rgba(0,0,0,0.55),rgba(0,0,0,0));height:3px;border:0;margin-bottom:40px;">
															</div>
														</div>
														<div class="col-lg-12" style="margin-left:0px;">
															<div class="col-lg-3">
																<label>Mobile:</label>
																<input class="form-control" value="<?php echo $enquiry_data['ENQUIRY_MOBILE_NO']; ?>" name="ENQUIRY_MOBILE_NO" id="ENQUIRY_MOBILE_NO" type="text">
															</div>
															<div class="col-lg-3">
																<label>Emai Id.:</label>
																<input class="form-control" value="<?php echo $enquiry_data['ENQUIRY_EMAIL']; ?>" name="ENQUIRY_EMAIL" id="ENQUIRY_EMAIL" type="text">
															</div>
															<div class="col-lg-3">
	                                  <label>First Name:</label>
	                                 <input class="form-control" value="<?php echo $enquiry_data['ENQUIRY_FIRSTNAME']; ?>" maxlength="50" name="ENQUIRY_FIRSTNAME" id="ENQUIRY_FIRSTNAME" type="text">
															</div>
															<div class="col-lg-3">
	                            <label>Last Name:</label>
	                              <input class="form-control" value="<?php echo $enquiry_data['ENQUIRY_LASTNAME']; ?>" name="ENQUIRY_LASTNAME" id="ENQUIRY_LASTNAME" maxlength="50" type="text">
															</div>
														</div>
													</div>
												</div>
                       <!-- <div class="form-group">
                        <div class="row" style="margin-left:-30px;">
                          <div class="col-lg-12" style="margin-left:0px;">
                           <div class="col-lg-4">
                              <label>Is Enrolled:</label>
                              <select name="ISENROLLED" class="form-control">
                                  <option value="">Please Select</option>
                                  <option value="1" <?php// if(isset($enquiry_data['ISENROLLED']) && $enquiry_data['ISENROLLED'] == '1'){echo 'selected="selected"';} ?> >Enrolled</option>
                                  <option value="0" <?php// if(isset($enquiry_data['ISENROLLED']) && $enquiry_data['ISENROLLED'] == '0'){echo 'selected="selected"';} ?>>Not Enrolled</option>
                              </select>
                            </div>
                            <div class="col-lg-4">
                               <label>Enquiry Form No.(From)</label>
                               <input class="form-control" value="<?php// echo $enquiry_data['ENQUIRY_FORM_NO_FROM']; ?>" name="ENQUIRY_FORM_NO_FROM" id="ENQUIRY_FORM_NO_FROM" maxlength="50" type="text">
                            </div>
                            <div class="col-lg-4">
                              <label>Enquiry Form No.(To)</label>
                              <input class="form-control" value="<?php// echo $enquiry_data['ENQUIRY_FORM_NO_TO']; ?>" name="ENQUIRY_FORM_NO_TO" id="ENQUIRY_FORM_NO_TO" maxlength="50" type="text">
                            </div>
                          </div>
                        </div>
                      </div> -->

                      <!-- <div class="form-group">
												<div class="row" style="margin-left:-30px;">
													<div class="col-lg-12" style="margin-left:0px;">
														<div class="col-lg-4">
                                <label>From Date:</label>
                                <input name="ENQUIRY_DATE_FROM" id="ENQUIRY_DATE_FROM" value="<?php// echo $enquiry_data['ENQUIRY_DATE_FROM']; ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
                                <span class="text-danger"><?php// echo form_error('ENQUIRY_DATE_FROM'); ?></span>
														</div>
														<div class="col-lg-4">
                            	<label>To Date:</label>
                             	<input name="ENQUIRY_DATE_TO" id="ENQUIRY_DATE_TO" value="<?php// echo $enquiry_data['ENQUIRY_DATE_TO']; ?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">
                             	<span class="text-danger"><?php// echo form_error('ENQUIRY_DATE_TO'); ?></span>
														</div>

                            <div class="col-lg-4">
                              <label>Center:</label>
                              <select name="CENTRE_ID" class="form-control">
                                 <option value="">Select Centre</option>
                                  <?php// foreach($centres as $centre){ ?>
                                      <option value="<?php// echo $centre['CENTRE_ID']; ?>" <?php// if(isset($enquiry_data['CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $enquiry_data['CENTRE_ID']) ? ' selected="selected"' : '';}?>><?php echo $centre['CENTRE_NAME']; ?></option>
                                  <?php// } ?>
                              </select>
														</div>
													</div>
												</div>
											</div> -->
										</div>

											<!-- <div class="col-md-6">
	                      <div class="form-group">
													<div class="row" style="margin-left:-30px;">
														<div class="col-lg-12" style="margin-left:0px;">
															<div class="col-lg-5">
	                                  <label>Heard About:</label>
	                                  <select name="HEARD_ABOUTUS" class="form-control">
	                                      <option value="">Please Select</option>
	                                      <?php// foreach($heardabout as $heard){ ?>
	                                      <option value="<?php //echo $heard['SOURCE_ID']; ?>" <?php// if(isset($enquiry_data['HEARD_ABOUTUS'])){ echo ($heard['SOURCE_ID'] == $enquiry_data['HEARD_ABOUTUS']) ? ' selected="selected"' : '';}?>><?php echo $heard['SOURCE_SUBSOURCE']; ?></option>
	                                      <?php// } ?>
	                                  </select>
															</div>
															<div class="col-lg-4">
	                                  <label>Course Intrested:</label>
	                                  <input type="text" name="COURSE_INTERESTED" id="COURSE_INTERESTED" class="form-control" value="<?php// echo $enquiry_data['COURSE_INTERESTED'];?>">
	                            </div>
	                            <div class="col-lg-3">
															</div>
														</div>
													</div>
												</div>-->
	                      <div class="form-group">
	                          <div class="row" style="margin-left:-30px;">
	                              <div class="col-lg-12" style="margin-left:0px;">
																		<div class="col-lg-3">
																		</div>
	                                  <div class="col-lg-3">
	                                      <button type="submit" value="submit" id="submit" class="btn btn-primary" style="float:right">Search</button>
	                                  </div>
	                                  <div class="col-lg-3">
	                                      <button type="submit" name="reset" value="reset" id="reset" class="btn btn-primary" style="float:left">Reset</button>
	                                  </div>
																		<?php if (!empty($this->session->userdata('enquiry_data'))) { ?>
																			<div class="col-lg-3">
																				<a onClick ="$('#export_to_execl_view_enquiry').tableExport({type:'excel',escape:'false'});" class="btn" style="float:left" title="Export to excel"><i class="fa fa-file-excel-o fa-2x text-success"></i></a>
																			</div>
																		<?php } ?>
	                              </div>
	                          </div>
	                      </div>
											</div>
                      <?php echo form_close(); ?>
									</div>
								</div>
							</div>
							</div>
							</div>
              <?php if($this->session->userdata('enquiry_data')){?>
							<div id="box"> <p>&nbsp;</p><span class=""><?php echo $pagination; ?></span>
        <h2 class="text-center"> Enquiry List</h2>
        <!--        <div class="row"><div class="col-md-4 pull-right"></div></div>--></div>
    <div class="panel panel-default">
        <!--<div class="panel-heading">Form Elements</div>-->
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-condensed table-stripped" id="export_to_execl_view_enquiry">
                <thead>
                    <tr>
                        <th>Action</th>
                        <th>Sr No.</th>
                        <th>Enquiry Id</th>
                        <th>Convert to Admissions</th>
                        <th>Add Follow Up</th>
                        <th>Enquiry Date</th>
                        <th>Name</th>
                        <th>Heard About Us</th>
                        <th>Mobile No.</th>
                        <th>Parent's No.</th>
                        <th>EMail ID.</th>
                        <th>Course Interested</th>
                        <th>Centre</th>
                        <th>Handled By</th>
                        <th>Remark</th>
                        <th>Is Enrolled</th>
                        <th>Admission ID</th>
                        <th>Preferred Timing</th>
                        <th>Last FollowUp Date</th>
                        <th>Last FollowUp Details</th>
                        <th>FollowUp By</th>
                        <th>Next FollowUp Date</th>
                    </tr>
                </thead>
                <tbody>
                     <?php
$count = $this->uri->segment(2) + 1;
foreach($enquiries as $enquiry){
    unset($enquiry['FOLLOWUP_DETAILS_ID']);

                    $enquiry_id = $this->encrypt->encode($enquiry['ENQUIRY_ID']);
                    ?>
                        <tr>
                            <td>
                              <span data-url="<?php echo base_url('enquiry/del-enquiry/'.$enquiry_id) ?>" data-method="delete" data-popup_header="Delete Enquiry" data-message="Are you sure you want to delete this entry?" class="glyphicon glyphicon-trash action pointer"></span>
                            <!-- </td> -->
                            <!-- <td> -->
                              <a href="<?php echo base_url('enquiry/edit-enquiry/'.$enquiry_id); ?>"><span class="glyphicon glyphicon-edit"></span></a>
                              <!-- <a href="<?php //echo base_url('enquiry/del-enquiry/'.$enquiry_id) ?>" onclick="return confirm('Are you sure to delete this?')"><span class="glyphicon glyphicon-trash"></span></a> -->
                            </td>
                            <td><?php echo $count; ?></td>
                            <td><?php echo $enquiry['ENQUIRY_ID']; ?></td>
                            <td>
                                <?php if($enquiry['ADMISSION_ID'] == 0){ ?><a href="<?php echo base_url('enquiry/convert-admission/'.$enquiry_id); ?>"><h1><span class="glyphicon glyphicon-education" title="New admission"></span></h1></a><?php }else{ echo "Already admitted. | ";?><a href="<?php echo base_url('enquiry/convert-admission/'.$enquiry_id); ?>"><h6><span class="glyphicon glyphicon-education" title="Second admission"></span></h6></a><?php } ?>
                            </td>
                            <td><a href="<?php $k="enquiry"; echo base_url('tele-enquiry-follow-up/details/'.$enquiry_id).'/'.$k ?>"><span class="glyphicon glyphicon-earphone" title="Follow up the Candidate"></span></a></td>
                            <?php
                                    if($enquiry['FOLLOWUP_DATE'] == '0000-00-00 00:00:00' || $enquiry['FOLLOWUP_DATE'] == ''){
                                        $FOLLOWUP_DATE = 'N/a';
                                    }
                                    else{
                                       $FOLLOWUP_DATE = date("d/m/Y", strtotime($enquiry['FOLLOWUP_DATE']));
                                    }

                                    if($enquiry['NEXT_FOLLOWUP_DATE'] == '0000-00-00 00:00:00' || $enquiry['NEXT_FOLLOWUP_DATE'] == ''){
                                        $NEXT_FOLLOWUP_DATE = 'N/a';
                                    }
                                    else{
                                       $NEXT_FOLLOWUP_DATE = date("d/m/Y", strtotime($enquiry['NEXT_FOLLOWUP_DATE']));
                                    }
                                     unset($enquiry['ENQUIRY_ID']);
                                     foreach($enquiry as $key => $enq){
                                        $ENQUIRY_DATE = date("d/m/Y", strtotime($enquiry['ENQUIRY_DATE']));
                                        $enquiry['ENQUIRY_DATE'] = $ENQUIRY_DATE;

                                        $enquiry['FOLLOWUP_DATE'] = $FOLLOWUP_DATE;

                                        $enquiry['NEXT_FOLLOWUP_DATE'] = $NEXT_FOLLOWUP_DATE;
                            ?>
                                <td>
                                    <?php echo $enquiry[$key]; if($enquiry['ISENROLLED'] == '0'){$enquiry['ISENROLLED'] = 'No';}else if($enquiry['ISENROLLED'] == '1'){$enquiry['ISENROLLED'] = 'Yes';}

                                    $coursesDiff = explode(",",$enquiry['COURSE_INTERESTED']);
                                    $courseArr = array();
                                    $courseInt = array();
                                    for($j=0;$j<count($coursesDiff);$j++)
                                    {
                                      $courseType = substr($coursesDiff[$j], strpos($coursesDiff[$j], "-") + 1);
                                      $courseId = strtok($coursesDiff[$j], "-");
                                      if ($courseType == '1')
                                      {
                                        $courseArr[] = strtok($coursesDiff[$j], "-");
                                      }
                                    }
                                    $courseNames = '';
                                    echo '<ul class="list-group">';
                                    for($j=0;$j<count($courses);$j++)
                                    {
                                      if(in_array($courses[$j]['COURSE_ID'],$courseArr))
                                      {
                                        $courseNames .= '<li class="list-group-item">'.$courses[$j]['COURSE_NAME'].'</li>';
                                        $enquiry['COURSE_INTERESTED'] = $courseNames;
                                      }
                                    }
                                    echo '</ul>';
                                    ?>
                                </td>
                                <?php  } ?>
                        </tr>
                        <?php $count++; } ?>
                </tbody>
            </table>
            <br> </div>
        <div class="row">
            <div class="col-md-4 pull-right">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
    <?php } ?>
							</div>
