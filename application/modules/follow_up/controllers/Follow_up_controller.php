<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Follow_up_controller extends MX_Controller {

     public function __construct()
        {
                $this->load->model('follow_up_model');
                parent::__construct();
        }
     public function teleEnquiryFollowUp($id,$isEnquiry){

        $telEnqId = $this->encrypt->decode($id);

        // $EnqId = $this->encrypt->decode($id);
        $this->load->model('employee/employee_model');
        $this->load->model('tele_enquiry/tele_enquiry_model');

        if($isEnquiry=="enquiry")
        {
            $data['EnqDet'] = $this->follow_up_model->getEnquiryDetail($telEnqId);

        }
        else
        {
            $data['telEnqDet'] = $this->follow_up_model->getTeleEnquiryDetail($telEnqId);

        }

        //exit();
        $data['followUpDetails'] = $this->follow_up_model->getFollowUpData($telEnqId);
        $data['sources'] = $this->tele_enquiry_model->get_tele_sources();
        $data['employees'] = $this->employee_model->getEmployeeNames();
        $data['centres'] = $this->follow_up_model->getCentre();
        $data['courses'] = $this->tele_enquiry_model->course_interested();
        $data['followUpStatus'] = $this->follow_up_model->followUpStatus();
        $data['add_bel_cust_js'] = base_url('resources/js/follow_up_cust.js');
        $data['telEnqId'] = $telEnqId;
        $this->load->admin_view('follow_up_view',$data);
     }

    public function addTeleEnquiryFollowUp(){
        $PREF_TIME = $_POST['FROM_TIME'].' - '.$_POST['TO_TIME'];
        $this->form_validation->set_rules('FOLLOWUP_BY', 'Follow-up by', 'trim|required|xss_clean');
        $this->form_validation->set_rules('FOLLOWUP_DETAILS', 'Follow-up Details', 'trim|required|xss_clean');
        $this->form_validation->set_rules('STATUS', 'Status', 'trim|required|xss_clean');
        if($this->form_validation->run() == TRUE){
        $telEnqId = $this->input->post('telEnqId');

        $modified_date =  date("Y-m-d H:i", strtotime(date('Y-m-d')));
        $addFollowUp = array(
            'SOURSE_ID' => $telEnqId,
            'CENTRE_ID' => $_POST['CENTRE_ID'],
            'FOLLOWUP_BY' => $_POST['FOLLOWUP_BY'],
            'PREFERRED_TIME' => $PREF_TIME,
            'FOLLOWUP_DETAILS' => $_POST['FOLLOWUP_DETAILS'],
            'STATUS' => $_POST['STATUS'],
            'SOURCE' => 'J',
            'IS_FOLLOWUPED' => '0',
            'MODIFIED_DATE'  => $modified_date,
            'MODIFIED_BY'  => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID']
        );
        $this->follow_up_model->updateTeleEnquiryFollowUpBool($telEnqId);
         if(isset($_POST['FOLLOWUP_DATE'])){
            $followUp_date_rep = str_replace('/', '-', $_POST['FOLLOWUP_DATE']);
            $followUpDate = date("Y-m-d H:i", strtotime($followUp_date_rep) );
            $addFollowUp['FOLLOWUP_DATE'] = $followUpDate;
        }

        // if($_POST['STATUS'] != '6'){
            if(isset($_POST['NEXT_FOLLOWUP_DATE'])){
                $nextFollowUp_date_rep = str_replace('/', '-', $_POST['NEXT_FOLLOWUP_DATE']);
                $nextFollowUpDate = date("Y-m-d H:i", strtotime($nextFollowUp_date_rep) );
                $addFollowUp['NEXT_FOLLOWUP_DATE'] = $nextFollowUpDate;
            }
        // }

        if(isset($_POST['VISIT_DATE'])){
            if($_POST['VISIT_DATE'] != ''){
                $visit_date_rep = str_replace('/', '-', $_POST['VISIT_DATE']);
                $visitDate = date("Y-m-d H:i", strtotime($visit_date_rep) );
            }
            else{
                $visitDate = NULL;
            }
            $addFollowUp['VISIT_DATE'] = $visitDate;
        }
        $this->follow_up_model->addFollowUp($addFollowUp);
        $followUpId = $this->encrypt->encode($this->db->insert_id());
        $msg = 'success';
        }else{
            if(validation_errors() != false){
                $msg['errors'][] = $this->form_validation->error_array();
            }
        }
        echo json_encode($msg);
    }

    public function centreDropDown(){
        $centres_list = $this->follow_up_model->getCentre();
        $centre_count = count($centres_list);

        for($i=0;$i<$centre_count;$i++){
            $centres[] = array(
                'value' => $centres_list[$i]['CENTRE_ID'],
                'text' => $centres_list[$i]['CENTRE_NAME']
            );
        }
        echo json_encode($centres);
    }

    public function employeeNames(){
        $this->load->model('employee/employee_model');
        $employees_list = $this->employee_model->getEmployeeNames();
        $employees_count = count($employees_list);

        for($i=0;$i<$employees_count;$i++){
            $employees[] = array(
                'value' => $employees_list[$i]['EMPLOYEE_ID'],
                'text' => $employees_list[$i]['EMP_FNAME'].' '.$employees_list[$i]['EMP_LASTNAME']
            );
        }
        echo json_encode($employees);
    }

    public function updateTeleEnquiryFollowUp(){
//        $followUpId = $_POST['pk'];
        $followUpId = $this->encrypt->decode($_POST['pk']);

        $updateFollowUp = array(
            $_POST['name'] => $_POST['value']
        );
        if($_POST['name'] != 'set_from_time' || $_POST['name'] != 'set_to_time'){
            $getTime = $this->follow_up_model->getFollowUpTime($followUpId);
            $pref_time = '';
            foreach($getTime as $time){
                $pref_time .= $time['PREFERRED_TIME'];
            }
            $time_array = explode("-",$pref_time);
            if($_POST['name'] == 'set_from_time'){
                $updateFollowUp = array(
                    'PREFERRED_TIME' => $_POST['value'].' - '.$time_array[1]
                );
            }
            else if($_POST['name'] == 'set_to_time'){
                $updateFollowUp = array(
                    'PREFERRED_TIME' => $time_array[0].' - '.$_POST['value']
                );
            }
        }
        $this->follow_up_model->updateFollowUp($followUpId,$updateFollowUp);
    }

    public function deleteTeleEnquiryFollowUp(){
        $telEnqId = $this->encrypt->decode($this->input->post('tel_enq_id'));
        $followUpId = $this->encrypt->decode($this->input->post('data_id'));
        $this->follow_up_model->deleteFollowUp($telEnqId, $followUpId);
    }

    public function followUpStatusDropDown(){
        $followUpStatus = $this->follow_up_model->followUpStatus();
        for ($i=0; $i <count($followUpStatus) ; $i++) {
            $followUpStatusArr[] = array(
                'value' => $followUpStatus[$i]['FollowUp_Status_ID'],
                'text'  => $followUpStatus[$i]['FollowUp_Status']
                );
        }

        echo json_encode($followUpStatusArr);
    }
}
