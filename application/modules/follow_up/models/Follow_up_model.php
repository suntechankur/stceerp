<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Follow_up_model extends CI_Model {
  public $sql;
  public function __construct() {
      parent::__construct();
  }

    public function getTeleEnquiryDetail($telEnqId){
        $query = $this->db->select('TE.FIRSTNAME,TE.MIDDLENAME,TE.LASTNAME,TE.EMAIL_ID,QM.QUALIFICATION,TE.ENQUIRY_DATE,TE.MOBILE,TE.PARENT_NUMBER,TE.LOCATION,TE.STREAM,TE.ISENROLLED,TE.WALKIN,TE.REMARKS,TE.COURSE_INTERESTED,SC.CENTRE_NAME as SCENTRE_NAME,TE.SOURCE,CR.CENTRE_NAME,SM.SOURCE_SUBSOURCE,SSM.SOURCE_SUBSOURCE as SSOURCE_SUBSOURCE')
                          ->from('tele_enquiries TE')
                          ->join('qualification_master QM','QM.QUALIFICATION_ID = TE.QUALIFICATION','left')
//                          ->join('course_master CM','CM.COURSE_ID = TE.COURSE_INTERESTED','left')
                          ->join('centre_master SC','SC.CENTRE_ID = TE.SUGGESTED_CENTRE_ID','left')
                          ->join('centre_master CR','CR.CENTRE_ID = TE.CENTRE_ID','left')
                          ->join('source_master SM','SM.SOURCE_ID = TE.SOURCE','left')
                          ->join('source_master SSM','SM.SOURCE_ID = TE.SUB_SOURCE','left')
                          ->where('TE.TELE_ENQUIRY_ID',$telEnqId)
                          ->get();
        return $query->result_array();
    }

    public function getEnquiryDetail($EnqId){

$query = $this->db->select('E.ENQUIRY_ID,E.ENQUIRY_DATE,E.ENQUIRY_FIRSTNAME,E.ENQUIRY_LASTNAME,SM.SOURCE_SUBSOURCE,E.ENQUIRY_MOBILE_NO,E.ENQUIRY_PARENT_NO,E.ENQUIRY_EMAIL,E.COURSE_INTERESTED,CM.CENTRE_NAME,CONCAT(EMH.EMP_FNAME," ",EMH.EMP_LASTNAME) as EMPH_NAME,SM.SOURCE_SUBSOURCE,E.REMARKS,E.ISENROLLED,E.ADMISSION_ID,FD.FOLLOWUP_DETAILS_ID,FD.PREFERRED_TIME,max(FD.FOLLOWUP_DATE) as FOLLOWUP_DATE,FD.FOLLOWUP_DETAILS,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME,FD.NEXT_FOLLOWUP_DATE')
                 ->from('enquiry_master E')
                 ->join('followup_details FD','FD.SOURSE_ID = E.ENQUIRY_ID','left')
                 ->join('employee_master EM','EM.EMPLOYEE_ID = FD.FOLLOWUP_BY','left')
                 ->join('centre_master CM','CM.CENTRE_ID = E.CENTRE_ID','left')
                 ->join('centre_master SC','SC.CENTRE_ID = E.CENTRE_ID','left')
                 ->join('employee_master EMH','EMH.EMPLOYEE_ID = E.ENQUIRY_HANDELED_BY','left')
                 ->join('source_master SM','SM.SOURCE_ID = E.HEARD_ABOUTUS','left outer')
                 ->where('E.ENQUIRY_ID',$EnqId)
                 ->group_by('E.ENQUIRY_ID')
                 ->order_by('E.ENQUIRY_ID','desc')
                 ->get();
        return $query->result_array();
    }

    public function getFollowUpData($telEnqId){
        $query = $this->db->select('FD.FOLLOWUP_DETAILS_ID,FD.SOURSE_ID,CM.CENTRE_NAME,FD.FOLLOWUP_DATE,EM.EMP_FNAME,EM.EMP_LASTNAME,FD.PREFERRED_TIME,FD.STATUS,FD.NEXT_FOLLOWUP_DATE,FD.VISIT_DATE,FD.FOLLOWUP_DETAILS,FS.FollowUp_Status')
                          ->from('followup_details FD')
                          ->join('centre_master CM', 'CM.CENTRE_ID = FD.CENTRE_ID')
                          ->join('employee_master EM', 'EM.EMPLOYEE_ID = FD.FOLLOWUP_BY')
                          ->join('followup_status FS','FS.FollowUp_Status_ID = FD.STATUS')
                          ->where('FD.SOURSE_ID',$telEnqId)
                          ->where('FD.SOURCE','J')
                          ->order_by('FD.FOLLOWUP_DETAILS_ID','ASC')
                          ->get();
        return $query->result_array();
    }

    public function getCentre(){
        $query = $this->db->select('CENTRE_ID,CENTRE_NAME')
                          ->from('centre_master')
                          ->where('ISACTIVE',1)
                          ->get();
        return $query->result_array();
    }

    public function getFollowUpTime($followUpId){
        $query = $this->db->select('PREFERRED_TIME')
                          ->from('followup_details')
                          ->where('FOLLOWUP_DETAILS_ID',$followUpId)
                          ->get();
        return $query->result_array();
    }

    public function addFollowUp($addFollowUp){
      $this->db->set('IS_FOLLOWUPED','1')
               ->where('SOURSE_ID',$addFollowUp['SOURSE_ID'])
               ->update('followup_details');
               // to update followup status

     $this->db->insert('followup_details',$addFollowUp);
    }

    public function updateFollowUp($followUpId,$updateFollowUp){
        $this->db->set($updateFollowUp)
                 ->where('FOLLOWUP_DETAILS_ID',$followUpId)
                 ->update('followup_details');
        echo $this->db->last_query();
    }

    public function updateTeleEnquiryFollowUpBool($telEnqId){
     $this->db->set('FOLLOWED_UP','1')
              ->where('TELE_ENQUIRY_ID',$telEnqId)
              ->update('tele_enquiries');
    }

    public function deleteFollowUp($telEnqId, $followUpId){
        $this->db->where('FOLLOWUP_DETAILS_ID',$followUpId)
                 ->delete('followup_details');

        $query = $this->db->select('SOURSE_ID')
                          ->from('followup_details')
                          ->where('SOURSE_ID',$telEnqId)
                          ->get();
        if($query->num_rows() == 0){
          $this->db->set('FOLLOWED_UP','0')
                   ->where('TELE_ENQUIRY_ID',$telEnqId)
                   ->update('tele_enquiries');
        }
    }

    public function followUpStatus(){
      $query = $this->db->where('ISACTIVE',1)
                        ->get('followup_status')
                        ->result_array();
      return $query;
    }
}
