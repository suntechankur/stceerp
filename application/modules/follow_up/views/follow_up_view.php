<div id="wait" style="display: none;position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: url(../resources/images/loading.gif) center no-repeat #fff;"></div>

<div class="gapping"></div>
<div class="create_batch_form">
      <div id="box">
        <?php if(isset($EnqDet))
        {
        ?>
        <h2>Enquiry Details</h2>
        <?php } else{
            ?>
            <h2>Tele-enquiry Details</h2>
            <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">
                <div class="col-sm-12">

            <?php if(isset($EnqDet))
            {


                foreach($EnqDet as $enq){
?>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name:</label>
                                <input readonly="readonly" class="form-control" type="text" name="FIRSTNAME" value="<?php echo $enq['ENQUIRY_FIRSTNAME']; ?>" id="FIRSTNAME" required="required"> </div>

                            <div class="form-group">
                                <label>Last Name:</label>
                                <input readonly="readonly" class="form-control" type="text" name="LASTNAME" value="<?php echo $enq['ENQUIRY_LASTNAME']; ?>" id="LASTNAME" required="required"> </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>E-mail:</label>
                                            <input readonly="readonly" class="form-control" type="text" name="EMAIL_ID" id="EMAIL_ID" value="<?php echo $enq['ENQUIRY_EMAIL']; ?>" required=""> </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Enquiry Date:</label>
                                            <?php $ENQUIRY_DATE=date("d/m/Y", strtotime($enq['ENQUIRY_DATE'])); ?>
                                                <input readonly="readonly" class="form-control hasDatepicker" type="text" value="<?php echo $ENQUIRY_DATE; ?>" maxlength="10" name="ENQUIRY_DATE" id="ENQUIRY_DATE" placeholder="DD/MM/YYYY">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                            <div class="form-group">
                            <label>Course Interested</label>
                            <?php
                                echo '<ul class="list-group">';
                                $coursesDiff = explode(",",$enq['COURSE_INTERESTED']);
                                $courseArr = array();
                                $courseNames = '';
                                for($i=0;$i<count($coursesDiff);$i++)
                                {
                                    $courseArr = substr($coursesDiff[$i], 0,-2);
                                    for($j=0;$j<count($courses);$j++)
                                    {
                                        if(in_array($courseArr,$courses[$j]))
                                        {
                                            echo $courseNames = '<li class="list-group-item">'.$courses[$j]['COURSE_NAME'].'</li>';
                                        }
                                    }
                                }
                                echo '</ul>';
                            ?>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-12">

                                            <label>Centre:</label>
                                            <select name="CENTRE_ID" class="form-control" required="" disabled="disabled">
                                                <option value="">
                                                    <?php echo $enq['CENTRE_NAME']; ?>
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Source:</label>
                                            <select name="SOURCE" class="form-control" onchange="GetSubSource(this.value)" required="" disabled="disabled">
                                                <option value="">
                                                    <?php echo $enq['SOURCE_SUBSOURCE']; ?>
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Sub Source:</label>
                                            <div id="SubsourceHint">
                                                <select name="SUB_SOURCE" class="form-control" disabled="disabled">
                                                    <?php if( $enq['SOURCE_SUBSOURCE'] == 0){ ?>
                                                        <option value="">Other</option>
                                                        <?php }else{ ?>
                                                            <option value="">
                                                                <?php echo $enq['SSOURCE_SUBSOURCE']; ?>
                                                            </option>
                                                            <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                        <label>Mobile No.:</label>
                                            <input readonly="readonly" class="form-control" value="<?php echo $enq['ENQUIRY_MOBILE_NO']; ?>" onkeypress="return isNumber(event)" type="text" name="MOBILE" id="MOBILE" style="width:100%" maxlength="10" size="10">
                                             </div>
                                        <div class="col-lg-6" style="float:right">
                                         <label>Parent's No.:</label>
                                            <input readonly="readonly" class="form-control" type="text" name="PARENT_NUMBER" value="<?php echo $enq['ENQUIRY_PARENT_NO']; ?>" id="PARENT_NUMBER" style="width:100%" maxlength="10" size="10">
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                           </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Remarks:</label>
                                <input readonly="readonly" class="form-control" type="text" value="<?php echo $enq['REMARKS']; ?>" name="REMARKS"> </div>
                            <br> </div>
                        <?php }
            }
            else
            {
             foreach($telEnqDet as $enq){ ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name:</label>
                                <input readonly="readonly" class="form-control" type="text" name="FIRSTNAME" value="<?php echo $enq['FIRSTNAME']; ?>" id="FIRSTNAME" required="required"> </div>
                            <div class="form-group">
                                <label>Middle Name:</label>
                                <input readonly="readonly" class="form-control" type="text" name="MIDDLENAME" value="<?php echo $enq['MIDDLENAME']; ?>" id="MIDDLENAME"> </div>
                            <div class="form-group">
                                <label>Last Name:</label>
                                <input readonly="readonly" class="form-control" type="text" name="LASTNAME" value="<?php echo $enq['LASTNAME']; ?>" id="LASTNAME" required="required"> </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>E-mail:</label>
                                            <input readonly="readonly" class="form-control" type="text" name="EMAIL_ID" id="EMAIL_ID" value="<?php echo $enq['EMAIL_ID']; ?>" required=""> </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Current Qualification:</label>
                                            <select name="QUALIFICATION" class="form-control" required="" disabled="disabled"> 0
                                                <option value="">
                                                    <?php echo $enq['QUALIFICATION']; ?>
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Selected Courses:&nbsp;</label>
                                <ul class="list-group"><?php

                                        $courses_arr = explode(",",$enq['COURSE_INTERESTED']);
                                         for($j=0;$j<count($courses_arr);$j++){?>

                                                <li class="list-group-item"><?php echo $courses_arr[$j]; ?></li>
                                            <?php

                                          }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Suggested Center:</label>
                                            <select name="SUGGESTED_CENTRE_ID" class="form-control" required="" disabled="disabled">
                                                <option value="">
                                                    <?php echo $enq['SCENTRE_NAME']; ?>
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Centre:</label>
                                            <select name="CENTRE_ID" class="form-control" required="" disabled="disabled">
                                                <option value="">
                                                    <?php echo $enq['CENTRE_NAME']; ?>
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Source:</label>
                                            <select name="SOURCE" class="form-control" onchange="GetSubSource(this.value)" required="" disabled="disabled">
                                                <option value="">

                                                    <?php
                                                        foreach($sources as $source){
                                                            if(is_numeric($enq['SOURCE'])){
                                                                if($source['controlfile_id'] == $enq['SOURCE']){
                                                                    echo $source['controlfile_value'];
                                                                }
                                                            }
                                                            else{
                                                                echo $enq['SOURCE'];
                                                            }
                                                        }
                                                    ?>
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Sub Source:</label>
                                            <div id="SubsourceHint">
                                                <select name="SUB_SOURCE" class="form-control" disabled="disabled">
                                                    <?php if( $enq['SOURCE_SUBSOURCE'] == 0){ ?>
                                                        <option value="">Other</option>
                                                        <?php }else{ ?>
                                                            <option value="">
                                                                <?php echo $enq['SSOURCE_SUBSOURCE']; ?>
                                                            </option>
                                                            <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Enquiry Date:</label>
                                            <?php $ENQUIRY_DATE=date("d/m/Y", strtotime($enq['ENQUIRY_DATE'])); ?>
                                                <input readonly="readonly" class="form-control hasDatepicker" type="text" value="<?php echo $ENQUIRY_DATE; ?>" maxlength="10" name="ENQUIRY_DATE" id="ENQUIRY_DATE" placeholder="DD/MM/YYYY"> </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Mobile No.:</label>
                                            <input readonly="readonly" class="form-control" value="<?php echo $enq['MOBILE']; ?>" onkeypress="return isNumber(event)" type="text" name="MOBILE" id="MOBILE" style="width:100%" maxlength="10" size="10"> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Parent's No.:</label>
                                            <input readonly="readonly" class="form-control" type="text" name="PARENT_NUMBER" value="<?php echo $enq['PARENT_NUMBER']; ?>" id="PARENT_NUMBER" style="width:100%" maxlength="10" size="10"> </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Location:</label>
                                            <select class="form-control" style="width:100% ;" name="LOCATION" id="LOCATION" disabled="disabled">
                                                <option value="">
                                                    <?php echo $enq['LOCATION']; ?>
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Stream:</label>
                                            <select class="form-control" style="width:100% ;" name="STREAM" id="STREAM" disabled="disabled">
                                                <option value="basics">
                                                    <?php echo $enq['STREAM']; ?>
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <br>
                                            <label class="form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="ISENROLLED" value="1" id="ISENROLLED" disabled="disabled" <?php if($enq[ 'ISENROLLED']=='1' ){echo 'checked';} ?>> Is Enrolled &nbsp;&nbsp; </label>
                                            <label class="form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="WALKIN" value="1" id="WALKIN" disabled="disabled" <?php if($enq[ 'WALKIN']=='1' ){echo 'checked';} ?>> Is Walkin &nbsp;&nbsp; </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Remarks:</label>
                                <input readonly="readonly" class="form-control" type="text" value="<?php echo $enq['REMARKS']; ?>" name="REMARKS"> </div>
                            <br> </div>
                        <?php } }?>
        </div>
    </div>
</div>
</div>
</div>
</div>


<div class="gapping"></div>
<div class="create_batch_form">
    <div id="box">
        <h2>Follow Up Details</h2>
        <?php if($this->session->flashdata('msg')) {
$msg = $this->session->flashdata('msg');
echo $msg['msg'];
} ?>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">
                <div class="col-sm-12">
                    <div id="st_batch" style="display: block;">
                        <div id="st_batchtable table-responsive">
                            <table class="table table-bordered table-condensed table-stripped followUpTable">
                                <form role="form" data-toggle="validator" method="post" action="lib/save.php" novalidate="true"></form>
                                <thead>
                                    <tr>
                                        <th>Action</th>
                                        <th>Center Name</th>
                                        <th>Follow-up Date</th>
                                        <th>Follow-up By</th>
                                        <th width="20%">Preferred timing</th>
                                        <th>Follow-up Details</th>
                                        <th>Status</th>
                                        <th>Next Follow-up Date</th>
                                        <th>Visited Date(YYYY-MM-DD)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($followUpDetails as $followUp){

                                        $teleEnqId = $this->encrypt->encode($followUp['SOURSE_ID']);
                                        $followup_details_id = $this->encrypt->encode($followUp['FOLLOWUP_DETAILS_ID']);
                                    ?>
                                    <tr data-followUpId="<?php echo $followup_details_id; ?>" data-telEnqId="<?php echo $teleEnqId; ?>">
                                            <td>
                                            <?php if(IsFollowupDetailsDeletionPermitted){?>
                                            <span class="del_data" data-url="<?php echo base_url('tele-enquiry-follow-up/deleteTeleEnquiryFollowUp'); ?>"><span class="glyphicon glyphicon-trash"></span></span>
                                            <?php } ?>
                                            </td>
                                        <td>
                                        <a href="" class="centreName" data-source="<?php echo base_url('tele-enquiry-follow-up/centreDropDown'); ?>" data-prepend="Select Centre"><?php echo $followUp['CENTRE_NAME']; ?></a>
                                        </td>
                                        <td><a href="#" class="followUpDate">
                                            <?php
                                             if($followUp['FOLLOWUP_DATE'] == NULL){
                                                echo 'Null';
                                             }
                                             else{
                                                $followUpDate = date("d/m/Y", strtotime($followUp['FOLLOWUP_DATE']));
                                                echo $followUpDate;
                                             }
                                            ?>
                                            </a></td>
                                        <td><a class="followUpBy" href="#" data-source="employeeNames" data-prepend="Select Employees"><?php echo $followUp['EMP_FNAME'].' '.$followUp['EMP_LASTNAME']; ?></a></td>
                                        <td>
                                            <?php
                                                if($followUp['PREFERRED_TIME'] != ''){
                                                    $prefTime = explode("-",$followUp['PREFERRED_TIME']);
                                                    $fromTime = $prefTime[0];
                                                    $toTime = $prefTime[1];
                                                }
                                                else{
                                                    $fromTime = '00';
                                                    $toTime = '00';
                                                }
                                            ?>
<!--                                                <a href="" class=""></a>-->
                                            <span><a class="set_from_time" href="#" value="8:30 am"><?php echo $fromTime; ?></a></span> -
                                            <span><a class="set_to_time" href="#"><?php echo $toTime; ?></a></span>
                                        <?php //echo $followUp['PREFERRED_TIME']; ?>
                                        </td>
                                        <td><a href="#" class="FOLLOWUP_DETAILS"><?php echo $followUp['FOLLOWUP_DETAILS']; ?></a></td>
                                        <td><?php

                                            ?>
                                        <a href="#" data-value="<?php echo $followUp['STATUS']; ?>" class="STATUS"><?php echo $followUp['FollowUp_Status']; ?></a>
                                        </td>
                                        <td><a href="#" class="nextFollowUpDate">
                                           <?php
                                             if(empty($followUp['NEXT_FOLLOWUP_DATE'])){
                                                 $nextFollowUpDate = 'N/a';
                                             }
                                             else{
                                                $nextFollowUpDate = date("d/m/Y", strtotime($followUp['NEXT_FOLLOWUP_DATE']));
                                             }
                                             echo $nextFollowUpDate;
                                            ?>
                                            </a></td>
                                        <td><a href="" class="visitDate">
                                            <?php
                                            if(is_null($followUp['VISIT_DATE'])){
                                             echo "Null";
                                            }
                                            else{
                                                $visitDate = date("d/m/Y", strtotime($followUp['VISIT_DATE']));
                                                echo $visitDate;
                                            }
                                            ?>
                                        </a></td>
                                    </tr>
                                    <?php } ?>
                                    <tr id="followupDetailsOperation">
                                        <th colspan="1">
                                            <input type="button" class="btn btn-primary add_follow_up" value="Add" data-fid="<?php echo $telEnqId; ?>" data-url="<?php echo base_url('tele-enquiry-follow-up/add-follow-up'); ?>">
                                        </th>
                                        <th>
                                            <select name="CENTRE_ID" class="form-control" id="CENTRE_ID">
                                                <?php


                                                if($this->session->userdata('admin_data')[0]['ROLE_ID'] == '23'){
                                                    $newArray = array();

                                                    if(($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '3070') || ($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '3061') || ($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '3140') || ($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '3114') || ($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '2731')){

                                                        // for Kavita Madvikar
                                                        if($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '2731'){

                                                            array_push($newArray, $centres[3]);
                                                        }
                                                        // for Priyanka Borkar
                                                        if($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '3070'){

                                                            array_push($newArray, $centres[6]);
                                                            array_push($newArray, $centres[11]);
                                                        }
                                                        // for Anju Verma
                                                        if($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '3061'){
                                                            array_push($newArray, $centres[9]);
                                                        }
                                                        // for Priyanka Pawar
                                                        if($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '3140'){
                                                            array_push($newArray, $centres[2]);
                                                        }
                                                        // for Mariyam Khan
                                                        if($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '3114'){
                                                            array_push($newArray, $centres[1]);
                                                        }
                                                        $centres = $newArray;

                                                    }
                                                    else{
                                                        $centres;
                                                    }
                                                }

                                                foreach($centres as $centre){ ?>
                                                <option value="<?php echo $centre['CENTRE_ID']; ?>"><?php echo $centre['CENTRE_NAME']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </th>
                                        <th>
                                            <input type="text" name="FOLLOWUP_DATE" id="FOLLOWUP_DATE" placeholder="DD/MM/YYYY" value="" class="form-control setDate" required="required">
                                        </th>
                                        <th>
                                            <select name="FOLLOWUP_BY" id="FOLLOWUP_BY" class="form-control" required="">
                                                <?php


                                                if($this->session->userdata('admin_data')[0]['ROLE_ID'] == '23'){
                                                    $newArray = array();

                                                    if(($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '3070') || ($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '3061') || ($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '3140') || ($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '3114') || ($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == '2731')){

                                                        foreach ($employees as $empData) {
                                                            if($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'] == $empData['EMPLOYEE_ID']){

                                                                array_push($newArray, $empData);
                                                            }
                                                        }
                                                        $employees = $newArray;

                                                    }
                                                    else{
                                                        $employees ;
                                                    }
                                                }

                                                foreach($employees as $employee){ ?>
                                                <option value="<?php echo $employee['EMPLOYEE_ID']; ?>"><?php echo $employee['EMP_FNAME'].' '.$employee['EMP_LASTNAME']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </th>
                                        <th>
                                            <label for="from_time">From</label>
                                            <input class="pref_time form-control" id="from_time" data-format="h:mm a" data-template="hh : mm a" name="from_time" value="" type="text">
                                            <p>&nbsp;</p>
                                            <label for="to_time">To &nbsp;&nbsp;&nbsp;</label>
                                            <input class="pref_time" id="to_time" data-format="h:mm a" data-template="hh : mm a" name="to_time" value="" type="text">
                                            <br/><br/>
                                            <span style="font-size:10;color:red;">* If Preffered time is required then select time else keep blank.</span><br/>
                                            E.g. From: 08:30 am To: 09:30 pm
                                        </th>
                                        <th>
                                            <input type="text" name="FOLLOWUP_DETAILS" id="FOLLOWUP_DETAILS" class="form-control" required="required">
                                        </th>
                                        <th>
                                            <select name="STATUS" class="form-control" id="STATUS" required="required">
                                                <option value="">Please Select</option>
                                                <?php foreach ($followUpStatus as $followUpStatusValue) { ?>
                                                    <option value="<?php echo $followUpStatusValue['FollowUp_Status_ID']; ?>"><?php echo $followUpStatusValue['FollowUp_Status']; ?></option>
                                                <?php } ?>

                                            </select>
                                        </th>
                                        <th>
                                            <input type="text" name="NEXT_FOLLOWUP_DATE" id="NEXT_FOLLOWUP_DATE" placeholder="DD/MM/YYYY" value="<?php echo date('d/m/Y', strtotime(date('Y-m-d').' +1 day'));?>" class="form-control setDate">
                                        </th>
                                        <th>
                                            <input type="text" value="" name="VISIT_DATE" id="VISIT_DATE" placeholder="DD/MM/YYYY" class="form-control hasDatepicker setDate" disabled=""> </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <br> </div>
                    <br>
                    <br>
                    <br>
                </div>
                <br> </div>
            </div>
        </div>
    </div>
</div>
<br />
<br />
<br />

  </div>
</div>
