<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HR_controller extends MX_Controller {
    
     public function __construct()
        {
                $this->load->model('HR_model');
                parent::__construct();
        }
    
    public function view_employee($offset = '0'){ 
        if(!empty($_POST)){
            $this->session->set_userdata('filter_data',$_POST);   
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('filter_data');
        }
        $employee_list['filter_data'] = $this->session->userdata('filter_data');
        /* Pagination starts */
        $config['base_url'] = base_url('view-employee/');
        $config['total_rows'] = $this->HR_model->get_employee_count($employee_list['filter_data']);
        $config['per_page'] = '10';
        
        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/
        
        $this->pagination->initialize($config);
        /* Pagination Ends */
        $employee_list['employees'] = $this->HR_model->get_employee_list($offset,$config['per_page'],$employee_list['filter_data']);
        print_r("<pre>");
        print_r($employee_list['employees']);
        print_r("</pre>");
        exit();

        $employee_list['pagination'] = $this->pagination->create_links();
        $employee_list['centres'] = $this->HR_model->get_centres();
        $employee_list['department'] = $this->HR_model->get_sources();
        $employee_list['designation'] = $this->HR_model->suggested_centre();
        
        $this->load->admin_view('view_employee',$employee_list);
    }
    
	public function add_employee()
	{
		$this->load->admin_view('add_employee');
	}
    
    public function edit_tele_enquiry($encrypt_id){
        $id = $this->encrypt->decode($encrypt_id);
        $data['tele_enquiries'] = $this->tele_enquiry_model->edit_tele_enquiry($id);
        $data['centres'] = $this->tele_enquiry_model->get_centres();
        $data['sources'] = $this->tele_enquiry_model->get_sources();
        $data['courses'] = $this->tele_enquiry_model->course_interested();
        $data['enq_handled_by'] = $this->tele_enquiry_model->enquiry_handled_by();
        
        $this->form_validation->set_rules('FIRSTNAME', 'First Name', 'required');
        $this->form_validation->set_rules('LASTNAME', 'Last Name', 'required');
        $this->form_validation->set_rules('EMAIL_ID', 'Email', 'required');
        $this->form_validation->set_rules('QUALIFICATION', 'Qualification', 'required');
        $this->form_validation->set_rules('MOBILE', 'Mobile', 'required|min_length[10]|max_length[10]|numeric');
        $this->form_validation->set_rules('ENQUIRY_DATE', 'Enquiry date', 'required');
        
        if(empty($_POST['SUGGESTED_CENTRE']) || $_POST['SUGGESTED_CENTRE'] == ''){
            $this->form_validation->set_rules('SUGGESTED_CENTRE', 'Suggested Centre', 'required');
        }
        
        if(empty($_POST['ENQUIRY_HANDELED_BY']) || $_POST['ENQUIRY_HANDELED_BY'] == ''){
            $this->form_validation->set_rules('ENQUIRY_HANDELED_BY', 'Enquiry Handled By', 'required');
        }
        
        if(empty($_POST['SOURCE']) || $_POST['SOURCE'] == ''){
            $this->form_validation->set_rules('SOURCE', 'Source', 'required');
        }
        
        if(empty($_POST['COURSE_INTERESTED'])){
            $this->form_validation->set_rules('COURSE_INTERESTED', 'Course Interested', 'required');
        }

        if(isset($_POST['SOURCE'])){
            $sid = $_POST['SOURCE'];
            $subSource = $this->tele_enquiry_model->checkSubsource($sid);
            if($subSource == true){
                $this->form_validation->set_rules('SUBSOURCE', 'Sub Source', 'required');
            }
        }
        
        if ($this->form_validation->run() != FALSE)
        {
            $date_rep = str_replace('/', '-', $_POST['ENQUIRY_DATE']);
            $enq_date =  date("Y-m-d H:i", strtotime($date_rep) );
            
            $update_enquiry = array(
                'FIRSTNAME' => $_POST['FIRSTNAME'],
                'MIDDLENAME' => $_POST['MIDDLENAME'],
                'LASTNAME'  => $_POST['LASTNAME'],
                'EMAIL_ID'  => $_POST['EMAIL_ID'],
                'QUALIFICATION'  => $_POST['QUALIFICATION'],
                'SUGGESTED_CENTRE_ID'  => $_POST['SUGGESTED_CENTRE'],
                'ENQUIRY_HANDELED_BY'  => $_POST['ENQUIRY_HANDELED_BY'],
                'SOURCE'  => $_POST['SOURCE'],
                'SUB_SOURCE'  => $_POST['SUBSOURCE'],
                'ENQUIRY_DATE'  => $enq_date,
                'MOBILE'  => $_POST['MOBILE'],
                'PARENT_NUMBER'  => $_POST['PARENT_NUMBER'],
                'STREAM'  => $_POST['STREAM'],
                'REMARKS'  => $_POST['REMARKS'],
            );
            
            if(!empty($_POST['COURSE_INTERESTED'])){
                $update_enquiry['COURSE_INTERESTED'] = implode(",",$_POST['COURSE_INTERESTED']);
            }
            
            if(isset($_POST['ISOUTOFSTATION'])){
                $update_enquiry['ISOUTOFSTATION'] = $_POST['ISOUTOFSTATION'];
            }
            
            date_default_timezone_set('Asia/Kolkata'); // CDT
            $info = getdate();
            $date = $info['mday'];
            $month = $info['mon'];
            $year = $info['year'];
            $hour = $info['hours'];
            $min = $info['minutes'];
            $sec = $info['seconds'];
            $current_date = "$year-$month-$date $hour:$min:$sec";
            
//            $update_enquiry['CREATED_ON'] = $current_date;
            $this->tele_enquiry_model->update_tele_enquiry($id,$update_enquiry);
            $this->session->set_flashdata('msg',array('msg' => 'Record Updated Succesfully'));
            redirect(base_url('view-tele-enquiry'));
        }
		$this->load->admin_view('edit_tele_enquiry',$data);
    }
    
    public function del_tele_enquiry($id){
        $telEnqId = $this->encrypt->decode($id);
        $this->tele_enquiry_model->del_tele_enquiry($telEnqId);
        redirect(base_url('view-tele-enquiry'));
    }
    
    public function enquiry_handler(){
        $centreId = $this->input->post('scId');
        $enquiry_handlers = $this->tele_enquiry_model->get_enquiry_handlers($centreId);
        echo json_encode($enquiry_handlers);
    }
    
    public function sub_sources(){
        $psId = $this->input->post('psId');
        $sub_sources = $this->tele_enquiry_model->get_sub_sources($psId);
        echo json_encode($sub_sources);
    }

    public function addCandidate(){
        $this->load->admin_view('add_candidate');
    }
}
