<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HR_model extends CI_Model {
    
	public function get_centres()
	{
		$query = $this->db->select('CENTRE_ID,CENTRE_NAME')
                          ->from('centre_master')
                          ->where('ISACTIVE','1')
                          ->get();
        return $query->result_array();                  
	}
    
    public function get_qualification(){
        $query = $this->db->get('qualification_master');
        return $query->result_array();
    }
    
    public function get_enquiry_handlers($centreId){
        $query = $this->db->select('ENQUIRY_FIRSTNAME,ENQUIRY_LASTNAME,ENQUIRY_ID')
                          ->from('enquiry_master')
                          ->where('CENTRE_ID',$centreId)
                          ->get();
        
        return $query->result_array();
    }
    
    public function get_sources($sid = 0){
        $query = $this->db->select('SOURCE_ID,SOURCE_SUBSOURCE')
                          ->from('source_master');
        if($sid == 0){
          $query = $this->db->where('PARENT_SOURCE_ID', '0');
        }
        else{
          $query = $this->db->where('SOURCE_ID',$sid);
        }
        $query = $this->db->order_by('SOURCE_SUBSOURCE','ASC')
                          ->get();
        return $query->result_array();
    }

    public function get_modules(){
      $query = $this->db->select('MODULE_ID,MODULE_NAME')
                        ->from('module_master')
                        ->where('ISACTIVE','1')
                        ->get();
      return $query->result_array();
    }
    
    public function get_sub_sources($psId){
        $query = $this->db->select('SOURCE_ID,SOURCE_SUBSOURCE')
                          ->from('source_master')
                          ->where('PARENT_SOURCE_ID !=', '0')
                          ->where('PARENT_SOURCE_ID',$psId)
                          ->order_by('SOURCE_SUBSOURCE','ASC')
                          ->get();
        return $query->result_array();
    }
    
    public function get_employee_list($offset,$perpage,$filter_data){
        $query = $this->db->select('EM.EMPLOYEE_ID,EM.BIOMETRIC_DEVICE_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as NAME,EM.GENDER,CM.CENTRE_NAME,
          
          EM.DATEOFBIRTH,EM.DATEOFJOIN,CF.CONTROLFILE_VALUE AS DESIGNATION,EM.QUALIFICATION,EM.BASIC,EM.HRA,EM.CTC,EM.ADDRESS1,EM. EMP_TELEPHONE,EM.EMP_MOBILENO,EM.EMP_OFFICIAL_EMAIL,EM.EMP_EMAIL,EM.EMPLOYEEPFNO,EM.PAN_CARD_NO,EM.BANKACCOUNTNO,EM.BANK_BRANCH,EM.UA_NUMBER,EM.ISACTIVE')
                 ->from('employee_master EM')
                 ->join('centre_master CM','CM.CENTRE_ID = EM.CENTRE_ID','left')
                 // ->join('employee_timings ET','EM.EMPLOYEE_ID = ET.EMPLOYEE_ID','left')
                 ->join('control_file CF','CF.CONTROLFILE_ID = EM.CURRENT_DESIGNATION','left')
                 // ->where('ET.IS_ACTIVE','1')
                 ->order_by('EM.EMPLOYEE_ID','desc');
        
        
              $query = $this->db->limit($perpage,$offset);   
              $query = $this->db->get();
       return $query->result_array();
    }
    
    public function enquiry_handled_by(){
        $query = $this->db->select('EM.EMPLOYEE_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME')
                          ->from('tele_enquiries TE')
                          ->join('followup_details FD','FD.SOURSE_ID = TE.TELE_ENQUIRY_ID')
                          ->join('employee_master EM','EM.EMPLOYEE_ID = TE.ENQUIRY_HANDELED_BY')
                          ->group_by('EM.EMP_FNAME,EM.EMP_LASTNAME')
                          ->where('FD.SOURCE','j')
                          ->get();
        return $query->result_array();
    }
    
    public function suggested_centre(){
        $query = $this->db->select('CENTRE_ID,CENTRE_NAME')
                          ->from('centre_master')
                          ->get();
        return $query->result_array();
    }
    
    public function course_interested($course_ids = array()){
        $query = $this->db->select('COURSE_ID,COURSE_NAME,MIN_FEES,MAX_FEES')
                          ->from('course_master')
                          ->group_by('COURSE_NAME')
                         ->where('ISACTIVE','1');
        if(!empty($course_ids) && is_array($course_ids)){
//            print_r($course_ids);
            $query = $this->db->where_in('COURSE_ID',$course_ids);
        }
        $query = $this->db->get();
        return $query->result_array();
    }    

    public function module_interested($module_ids = array()){
        $query = $this->db->select('MODULE_ID,MODULE_NAME,MIN_FEES,MAX_FEES')
                          ->from('module_master')
                          ->group_by('MODULE_NAME')
                         ->where('ISACTIVE','1');
        if(!empty($module_ids) && is_array($module_ids)){
//            print_r($course_ids);
            $query = $this->db->where_in('MODULE_ID',$module_ids);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function get_employee_count($filter_data){
         
      $query = $this->db->select('EM.EMPLOYEE_ID')
                        ->from('EMPLOYEE_MASTER EM');
                        
      $query = $this->db->get();
        return $query->num_rows();
    
    }
    
    public function add_tele_enquiry($insert_enquiry){
        $insert_enquiry['CENTRE_ID'] = '155';
        $this->db->insert('tele_enquiries',$insert_enquiry);
    }
    
    public function edit_tele_enquiry($id){
        $query = $this->db->select('TE.FIRSTNAME,TE.MIDDLENAME,TE.LASTNAME,TE.EMAIL_ID,TE.QUALIFICATION,CM.CENTRE_NAME,SM.SOURCE_ID,SM.SOURCE_SUBSOURCE,TE.SOURCE,TE.SUB_SOURCE,TE.ENQUIRY_DATE,TE.MOBILE,TE.COURSE_INTERESTED,TE.PARENT_NUMBER,TE.LOCATION,TE.STREAM,TE.ISOUTOFSTATION,TE.ISWORKING,TE.REMARKS,TE.SUGGESTED_CENTRE_ID,TE.ENQUIRY_HANDELED_BY')
                 ->from('tele_enquiries TE')
                 ->join('qualification_master QM','QM.QUALIFICATION_ID = TE.QUALIFICATION','left')
                 ->join('centre_master CM','CM.CENTRE_ID = TE.CENTRE_ID','left')
                 ->join('source_master SM','SM.SOURCE_ID = TE.SUB_SOURCE','left')
                 ->where('TE.TELE_ENQUIRY_ID',$id)
                 ->get();
        return $query->result_array();
    }
    
    public function update_tele_enquiry($id,$update_enquiry){
        $this->db->set($update_enquiry)
                 ->where('TELE_ENQUIRY_ID',$id)
                 ->update('tele_enquiries');
    }
    
    public function del_tele_enquiry($telEnqId){
        $this->db->where('TELE_ENQUIRY_ID',$telEnqId)
                 ->delete('tele_enquiries');
    }

    public function checkSubsource($sid){
      $query = $this->db->select('PARENT_SOURCE_ID')
                        ->from('source_master')
                        ->where('PARENT_SOURCE_ID',$sid)
                        ->get();
      if($query->num_rows() > 0){
        return true;
      }
      else{
        return false;
      }
    }
}