<div class="create_batch_form">
    <div id="box">
    <h2>Candidate Details</h2>

    </div>
    <div class="row">

        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- start message area -->

                    <!-- End message area --> 

                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                              <label for="usr">Post Applied for: <span class="text-danger"></span></label>
                              <select name="" id="" class="form-control">
                                  <option value="1">Faculty</option>
                              </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">Interested in: <span class="text-danger">*</span></label>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                <div class="col-md-4">
                                    <input type="checkbox" name="interested_in" value="1" checked="">Full Time
                                </div>
                                <div class="col-md-4">
                                    <input type="checkbox" name="interested_in" value="2">Part Time
                                </div>
                                <div class="col-md-4">
                                    <input type="checkbox" name="interested_in" value="3"> Visiting Basis
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>