<div class="gapping"></div>
<div class="create_batch_form">
    <div id="box">
        <h2>Add Employee</h2>
        <?php if($this->session->flashdata('msg')) {
            $msg = $this->session->flashdata('msg');
            echo $msg['msg'];
        } ?>
        </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align:center; color:#F00;"></div>
                <div class="panel-body">
                   <?php 
                    $attr = array('role'=>'form');
                    echo form_open('',$attr); ?>
<!--                    <form role="form" data-toggle="validator" method="post" action="lib/save.php" novalidate="true">-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name: <span class="text-danger">*</span></label>
                                <input class="form-control" name="FIRSTNAME" value="" id="FIRSTNAME" type="text"> 
                                <span class="text-danger"><?php echo form_error('FIRSTNAME'); ?></span>
                                </div>
                            <div class="form-group">
                                <label>Middle Name:</label>
                                <input class="form-control" name="MIDDLENAME" value="" id="MIDDLENAME" type="text"> </div>
                            <div class="form-group">
                                <label>Last Name: <span class="text-danger">*</span></label>
                                <input class="form-control" name="LASTNAME" value="" id="LASTNAME" type="text"> 
                                <span class="text-danger"><?php echo form_error('LASTNAME'); ?></span>
                                </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>E-mail: <span class="text-danger">*</span></label>
                                            <input class="form-control" name="EMAIL_ID" id="EMAIL_ID" value="" type="text"> 
                                            <span class="text-danger"><?php echo form_error('EMAIL_ID'); ?></span>
                                            </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Current Qualification: <span class="text-danger">*</span></label>
                                            <select name="QUALIFICATION" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1">Xth Pass </option>
                                                <option value="2">XIIth Pass </option>
                                                <option value="3">Under Graduate </option>
                                                <option value="4">Graduate </option>
                                                <option value="5">Post Graduate </option>
                                                <option value="6">Other </option>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('QUALIFICATION'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select Multiple Course: <span class="text-danger">*</span></label><br />
                                        <select class="form-control course_interested" name="COURSE_INTERESTED[]" id="COURSE_INTERESTED" multiple>
                                        <?php 
                                            foreach($courses as $course){ ?>
                                            <option value="<?php echo $course['COURSE_ID']; ?>"><?php echo $course['COURSE_NAME']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('COURSE_INTERESTED'); ?></span>
                                    </div>
                                    <div class="row">
                                   <div class="col-md-12">
                                       <ul class="list-group courses_seld"></ul>
                                   </div>
                                </div>
                                </div>
                                <div class="col-md-6">
                                        <label class="form-check-inline">
                                            <input class="form-check-input" name="ISWORKING" value="1" id="ISWORKING" type="checkbox"> Is Working &nbsp;&nbsp; </label>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Suggested Center: <span class="text-danger">*</span></label>
                                            <select name="SUGGESTED_CENTRE" class="form-control suggested_centre">
                                                 <option value="">Please Select Centre</option>
                                                 <?php foreach($centres as $centre){?>
                                                <option value="<?php echo $centre['CENTRE_ID']; ?>"><?php echo $centre['CENTRE_NAME']; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('SUGGESTED_CENTRE'); ?></span>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Enquiry Handled By:</label>
                                            <select name="ENQUIRY_HANDELED_BY" class="form-control enquiry_handler">
                                            <?php foreach($enq_handled_by as $enq_handler){ ?>
                                                        <option value="<?php echo $enq_handler['EMPLOYEE_ID']; ?>"><?php echo $enq_handler['EMP_NAME']; //$filter_data['ENQUIRY_HANDELED_BY'] ?></option>
                                                    <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_HANDELED_BY'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Source: <span class="text-danger">*</span></label>
                                            <select name="SOURCE" class="form-control source_master">
                                                <option value="">Please Select Source</option>
                                                <?php foreach($sources as $source){ ?>
                                                <option value="<?php echo $source['SOURCE_ID'] ?>"><?php echo $source['SOURCE_SUBSOURCE'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('SOURCE'); ?></span>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Sub Source:</label>
                                            <div id="SubsourceHint">
                                                <script type="text/javascript" src="js/reloaddiv.js"></script>
                                                <select name="SUBSOURCE" class="form-control sub_source">
<!--                                                    <option value="">Other</option>-->
                                                </select>
                                            </div>
                                            <span class="text-danger"><?php echo form_error('SUBSOURCE'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Enquiry Date: <span class="text-danger">*</span></label>
                                            <input class="form-control enquiry_date add_enquiry_date" maxlength="10" name="ENQUIRY_DATE" id="enquiry_date" type="text">
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_DATE'); ?></span>
                                              </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Mobile No.: <span class="text-danger">*</span></label>
                                            <input class="form-control" value="" onkeypress="return isNumber(event)" name="MOBILE" id="MOBILE" style="width:100%" maxlength="10" size="10" type="text"> 
                                            <span class="text-danger"><?php echo form_error('MOBILE'); ?></span>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-6">
                                            <label>Parent's No.:</label>
                                            <input class="form-control" name="PARENT_NUMBER" value="" id="PARENT_NUMBER" style="width:100%" maxlength="10" size="10" type="text"> </div>
                                      <div class="col-md-6">
                                           <div class="form-group">
                                                        <label>Stream:</label>
                                                        <select class="form-control" style="width:100% ;" name="STREAM" id="STREAM">
                                                            <option value="basics">Basics</option>
                                                            <option value="Programming">Programming</option>
                                                            <option value="Graphics_animation">Graphics &amp; animation</option>
                                                            <option value="Hardware_Networking">Hardware &amp; Networking</option>
                                                            <option value="others">others</option>
                                                        </select>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Remarks:</label>
                                        <input class="form-control" name="REMARKS" type="text"> 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <br>
                                        <label class="form-check-inline">
                                        <input class="form-check-input" name="ISOUTOFSTATION" value="1" id="ISOUTOFSTATION" type="checkbox"> Is Out Of Station &nbsp;&nbsp; </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="">Preferred Time</label>
                                </div>
                                <div class="col-md-6">
                                <!-- <label for="">Preferred Timing</label> -->
                                    <label for="from_time">From</label>
                                        <input class="pref_time form-control" id="from_time" data-format="h:mm a" data-template="hh : mm a" name="from_time" value="8:30 am" type="text">
                                </div>
                                <div class="col-md-6">
                                        <label for="to_time">To &nbsp;&nbsp;&nbsp;</label>
                                        <input class="pref_time" id="to_time" data-format="h:mm a" data-template="hh : mm a" name="to_time" value="9:30 am" type="text">
                                </div>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">Create</button> &nbsp;
                            <button type="reset" style="float:none">Reset</button>
                        </div>
<!--                    </form>-->
               <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>