<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
    <div class="create_batch_form">
        <div id="box">
            <h2>Employee Search</h2></div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align:center; color:#F00;"> </div>
                    <div class="panel-body">
                        <?php echo form_open(); ?>   
                            
                                <div class="form-group">
                                    <div class="row" style="margin-left:-30px;">
                                        <div class="col-lg-12" style="margin-left:0px;">
                                            <div class="col-lg-4">
                                                <label>First Name:</label>
                                                <input class="form-control" type="text" value="<?php if(isset($filter_data['FIRSTNAME'])){ echo $filter_data['FIRSTNAME']; }?>" maxlength="50" name="FIRSTNAME" id="FIRSTNAME">
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Last Name:</label>
                                                <input class="form-control" type="text" value="<?php if(isset($filter_data['LASTNAME'])){ echo $filter_data['LASTNAME']; }?>" maxlength="50" name="LASTNAME" id="LASTNAME">
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Centre:</label>
                                                <select name="CENTRE_ID" class="form-control">
                                                   <option value="">Select Centre</option>
                                                    <?php foreach($centres as $centre){ ?>
                                                        <option value="<?php echo $centre['CENTRE_ID']; ?>" <?php if(isset($enquiry_data['CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $enquiry_data['CENTRE_ID']) ? ' selected="selected"' : '';}?>><?php echo $centre['CENTRE_NAME']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                      </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row" style="margin-left:-30px;">
                                        <div class="col-lg-12" style="margin-left:0px;">
                                            <div class="col-lg-4">
                                                <label>Date Of Join From.:</label>
                                                 <input name="JOINING_FROM" id="JOINING_FROM" value="" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">  </div>
                                            <div class="col-lg-4">
                                                <label>Date Of Join To.:</label>
                                                 <input name="JOINING_TO" id="JOINING_TO" value="" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">  </div>
                                            <div class="col-lg-4">
                                                <label>Active:</label>
                                                <select name="ACTIVE" class="form-control">
                                                    <option value="">All</option>
                                                    <option value="0">Active</option>
                                                    <option value="1">Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row" style="margin-left:-30px;">
                                        <div class="col-lg-12" style="margin-left:0px;">
                                                                                        <div class="col-lg-4">
                                                <label>Date Of Leave From.:</label>
                                                 <input name="LEAVING_FROM" id="LEAVING_FROM" value="" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">  </div>
                                            <div class="col-lg-4">
                                                <label>Date Of Leave To.:</label>
                                                 <input name="LEAVING_TO" id="LEAVING_TO" value="" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">  </div>
                                            <div class="col-lg-4">
                                                <label>Department:</label>
                                                <select name="DEPARTMENT" class="form-control">
                                                   <option value="">Select Department</option>
                                                    <?php foreach($department as $dpt){ ?>
                                                        <option value="<?php echo $dpt['DepartmentId']; ?>" <?php if(isset($employee_data['DepartmentId'])){ echo ($dpt['DepartmentId'] == $enquiry_data['DepartmentId']) ? ' selected="selected"' : '';}?>><?php echo $dpt['DepartmentId']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="form-group">
                                    <div class="row" style="margin-left:-30px;">
                                        <div class="col-lg-12" style="margin-left:0px;">
                                            <div class="col-lg-4">
                                                <label>Designation:</label>
                                                <select name="DESIGNATION" class="form-control">
                                                   <option value="">Select Designation</option>
                                                    <?php foreach($designation as $dsg){ ?>
                                                        <option value="<?php echo $dsg['DepartmentId']; ?>" <?php if(isset($employee_data['DepartmentId'])){ echo ($dsg['DepartmentId'] == $enquiry_data['DepartmentId']) ? ' selected="selected"' : '';}?>><?php echo $dsg['DepartmentId']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                                               
                            
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="box"> <span class=""><?php echo $pagination; ?></span>
        <h2 class="text-center"> Employee List</h2>
        <!--        <div class="row"><div class="col-md-4 pull-right"></div></div>--></div>
    <div class="panel panel-default">
        <!--<div class="panel-heading">Form Elements</div>-->
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-condensed table-stripped">
                <thead>
                    <tr>
                        <th>Action</th>
                        <th>Sr No.</th>
                        <th>Employee ID</th>
                        <th>Biometric ID</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Centre</th>
                        <th>Timings</th>
                        <th>Date of Join</th>
                        <th>Date of Birth</th>
                        <th>Designation</th>
                        <th>Qualification</th>
                        <th>CTC</th>
                        <th>Basic</th>
                        <th>HRA</th>
                        <th>Address</th>
                        <th>Contact No</th>
                        <th>Official Email</th>
                        <th>Personal Email</th>
                        <th>PF Number</th>
                        <th>PAN Card No</th>
                        <th>Is Active</th>
                        <th>Bank Account No.</th>
                        <th>Branch Name</th>
                        <th>UAN</th>
                       
                    </tr>
                </thead>
                <tbody>
                    <!--                    <tr><td colspan="18" class="text-center">Sorry Record Not Found</td></tr>							-->
                    <?php 
$count = $this->uri->segment(2) + 1;
foreach($employees as $employee){ 
        
                    $employee_id = $this->encrypt->encode($employee['EMPLOYEE_ID']);
                    ?>
                        <tr>
                            <td><a href=""><span class="glyphicon glyphicon-edit"></span></a></td>
                            <td><?php echo $count; ?></td>
                            <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                            <td>
                                <?php echo $employee['BIOMETRIC_DEVICE_ID']; ?> 
                            </td>
                            <td>
                                <?php echo $employee['NAME']; ?>
                            </td>
                            <td>
                                <?php echo $employee['GENDER']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                             <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                             <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                             <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                            

                            
                        </tr>
                        <?php $count++; } ?>
                </tbody>
            </table>
            <br> </div>
        <div class="row">
            <div class="col-md-4 pull-right">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
</div>