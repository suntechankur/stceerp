<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
    <div class="create_batch_form">
        <div id="box">
            <h2>Tele Enquiry Search</h2></div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align:center; color:#F00;"> </div>
                    <div class="panel-body">
                        <?php echo form_open(); ?>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" style="margin-left:-30px;">
                                        <div class="col-lg-12" style="margin-left:0px;">
                                            <div class="col-lg-4">
                                                <label>Enquiry Handled By:</label>
                                                <select name="ENQUIRY_HANDELED_BY" class="form-control">
                                                   <option value="">Select Enquiry Handler</option>
                                                    <?php foreach($enq_handled_by as $enq_handler){ ?>
                                                        <option value="<?php echo $enq_handler['EMPLOYEE_ID']; ?>" <?php if(isset($filter_data['ENQUIRY_HANDELED_BY'])){ echo ($enq_handler['EMPLOYEE_ID'] == $filter_data['ENQUIRY_HANDELED_BY']) ? ' selected="selected"' : '';}?>>
                                                            <?php echo $enq_handler['EMP_NAME']; //$filter_data['ENQUIRY_HANDELED_BY'] ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>From Date:</label>
                                                <input type="text" name="STARTDATE" id="STARTDATE" value="<?php if(isset($filter_data['STARTDATE'])){ echo $filter_data['STARTDATE']; }?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY"> </div>
                                            <div class="col-lg-4">
                                                <label>To Date:</label>
                                                <input type="text" name="TODATE" id="TODATE" value="<?php if(isset($filter_data['TODATE'])){ echo $filter_data['TODATE']; }?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY"> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row" style="margin-left:-30px;">
                                        <div class="col-lg-12" style="margin-left:0px;">
                                            <div class="col-lg-4">
                                                <label>Mobile No.:</label>
                                                <input type="Text" name="MOBILE" value="<?php if(isset($filter_data['MOBILE'])){ echo $filter_data['MOBILE'];} ?>" maxlength="10" id="MOBILE" class="form-control"> </div>
                                            <div class="col-lg-4">
                                                <label>Parent's No.:</label>
                                                <input type="Text" name="PARENT_NUMBER" value="<?php if(isset($filter_data['PARENT_NUMBER'])){ echo $filter_data['PARENT_NUMBER'];} ?>" maxlength="10" id="PARENT_NUMBER" class="form-control"> </div>
                                            <div class="col-lg-4">
                                                <label>Source:</label>
                                                <select name="SOURCE" class="form-control source_master" <?php if(isset($filter_data['SUB_SOURCE'])){ if($filter_data['SOURCE'] != ''){ if($filter_data['SUB_SOURCE'] != ''){echo 'data-subsource="'.$filter_data['SUB_SOURCE'].'"';}else{echo 'data-subsource="err"';}}} ?> >
                                                    <option value="">Select Source</option> 
                                                    <?php foreach($sources as $source){ ?>
                                                    <option value="<?php echo $source['SOURCE_ID']; ?>" <?php if(isset($filter_data['SOURCE'])){ echo ($source['SOURCE_ID'] == $filter_data['SOURCE']) ? ' selected="selected"' : ''; }?>><?php echo $source['SOURCE_SUBSOURCE'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row" style="margin-left:-30px;">
                                        <div class="col-lg-12" style="margin-left:0px;">
                                            <div class="col-lg-4">
                                                <label>Is Enrolled:</label>
                                                <select name="ISENROLLED" class="form-control">
                                                    <option value="">Please Select</option>
                                                    <option value="1" <?php if(isset($filter_data['ISENROLLED']) && $filter_data['ISENROLLED'] == '1'){echo 'selected="selected"';} ?> >Enrolled</option>
                                                    <option value="0" <?php if(isset($filter_data['ISENROLLED']) && $filter_data['ISENROLLED'] == '0'){echo 'selected="selected"';} ?>>Not Enrolled</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Is Walkin:</label>
                                                <select name="WALKIN" class="form-control">
                                                    <option value="">Please Select</option>
                                                    <option value="1" <?php if(isset($filter_data['WALKIN']) && $filter_data['WALKIN'] == '1'){echo 'selected="selected"';} ?>>Walkin</option>
                                                    <option value="0" <?php if(isset($filter_data['WALKIN']) && $filter_data['WALKIN'] == '0'){echo 'selected="selected"';} ?>>Not Walkin</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Is HO:</label>
                                                <select name="ISHO" class="form-control">
                                                    <option value="">Please Select</option>
                                                    <option value="0">Include</option>
                                                    <option value="1">Exclude</option>
                                                    <option value="155">Not Forwarded</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row" style="margin-left:-30px;">
                                        <div class="col-lg-12" style="margin-left:0px;">
                                            <div class="col-lg-4">
                                                <label>First Name:</label>
                                                <input class="form-control" type="text" value="<?php if(isset($filter_data['FIRSTNAME'])){ echo $filter_data['FIRSTNAME']; }?>" maxlength="50" name="FIRSTNAME" id="FIRSTNAME"> </div>
                                            <div class="col-lg-4">
                                                <label>Last Name:</label>
                                                <input class="form-control" type="text" value="<?php if(isset($filter_data['LASTNAME'])){ echo $filter_data['LASTNAME']; }?>" name="LASTNAME" id="LASTNAME" maxlength="50"> </div>
                                            <div class="col-lg-4">
                                                <label>Qualification:</label>
                                                <select name="QUALIFICATION" class="form-control">
													<option value="">Please Select</option>
                                                    <option value="1" <?php if(isset($filter_data['QUALIFICATION']) && $filter_data['QUALIFICATION'] == '1'){echo 'selected="selected"';} ?>>Xth Pass </option>
                                                    <option value="2" <?php if(isset($filter_data['QUALIFICATION']) && $filter_data['QUALIFICATION'] == '2'){echo 'selected="selected"';} ?>>XIIth Pass </option>
                                                    <option value="3" <?php if(isset($filter_data['QUALIFICATION']) && $filter_data['QUALIFICATION'] == '3'){echo 'selected="selected"';} ?>>Under Graduate </option>
                                                    <option value="4" <?php if(isset($filter_data['QUALIFICATION']) && $filter_data['QUALIFICATION'] == '4'){echo 'selected="selected"';} ?>>Graduate </option>
                                                    <option value="5" <?php if(isset($filter_data['QUALIFICATION']) && $filter_data['QUALIFICATION'] == '5'){echo 'selected="selected"';} ?>>Post Graduate </option>
                                                    <option value="6" <?php if(isset($filter_data['QUALIFICATION']) && $filter_data['QUALIFICATION'] == '6'){echo 'selected="selected"';} ?>>Other </option>
												</select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row" style="margin-left:-30px;">
                                        <div class="col-lg-12" style="margin-left:0px;">
                                            <div class="col-lg-4">
                                                <label>Sub Source:</label>
                                                <select name="SUB_SOURCE" class="form-control sub_source">
                                                    <option value="">Select Source First</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Suggested Centre:</label>
                                                <select name="SUGGESTED_CENTRE_ID" class="form-control">
                                                   <option value="">Select Centre</option>
                                                    <?php foreach($centres as $centre){ ?>
                                                        <option value="<?php echo $centre['CENTRE_ID']; ?>" <?php if(isset($filter_data['SUGGESTED_CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $filter_data['SUGGESTED_CENTRE_ID']) ? ' selected="selected"' : '';}?>><?php echo $centre['CENTRE_NAME']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Course Interested:</label>
                                                <select class="form-control" name="COURSE_INTERESTED" id="COURSE_INTERESTED" value="">
                                                   <option value="">Select Course</option>
                                                    <?php foreach($courses as $course){ ?>
                                                        <option value="<?php echo $course['COURSE_NAME']; ?>" <?php if(isset($filter_data['COURSE_INTERESTED'])){ echo ($course['COURSE_NAME'] == $filter_data['COURSE_INTERESTED']) ? ' selected="selected"' : ''; }?>><?php echo $course['COURSE_NAME']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row" style="margin-left:-30px;">
                                        <div class="col-lg-12" style="margin-left:0px;">
                                            <div class="col-lg-4">
                                                <label>Is Out Station:</label>
                                                <select name="ISOUTOFSTATION" class="form-control">
                                                    <option value=''>Please Select</option>
                                                    <option value="1" <?php if(isset($filter_data['ISOUTOFSTATION']) && $filter_data['ISOUTOFSTATION'] == '1'){echo 'selected="selected"';} ?>>Out Station</option>
                                                    <option value="0" <?php if(isset($filter_data['ISOUTOFSTATION']) && $filter_data['ISOUTOFSTATION'] == '0'){echo 'selected="selected"';} ?>>Not Out Station</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Is Working:</label>
                                                <select name="ISWORKING" class="form-control">
                                                    <option value=''>Please Select</option>
                                                    <option value="1" <?php if(isset($filter_data['ISWORKING']) && $filter_data['ISWORKING'] == '1'){echo 'selected="selected"';} ?>>Working</option>
                                                    <option value="0" <?php if(isset($filter_data['ISWORKING']) && $filter_data['ISWORKING'] == '0'){echo 'selected="selected"';} ?>>Not Working</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Stream:</label>
                                                <select name="STREAM" class="form-control">
                                                    <option value=''>Please Select</option>
                                                    <option value="Programming" <?php if(isset($filter_data['STREAM']) && $filter_data['STREAM'] == 'Programming'){echo 'selected="selected"';} ?>>Programming</option>
                                                    <option value="Graphics & Animation" <?php if(isset($filter_data['STREAM']) && $filter_data['STREAM'] == 'Graphics & Animation'){echo 'selected="selected"';} ?>>Graphics & Animation</option>
                                                    <option value="Hardware & Networking" <?php if(isset($filter_data['STREAM']) && $filter_data['STREAM'] == 'Hardware & Networking'){echo 'selected="selected"';} ?>>Hardware & Networking</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row" style="margin-left:-30px;">
                                        <div class="col-lg-12" style="margin-left:0px;">
                                            <div class="col-lg-6">
                                                <button type="submit" value="submit" id="submit" class="btn btn-primary" style="float:right">Search</button>
                                            </div>
                                            <div class="col-lg-6">
                                                <button type="submit" name="reset" value="reset" id="reset" class="btn btn-primary" style="float:left">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="box"> <span class=""><?php echo $pagination; ?></span>
        <h2 class="text-center"> Tele Enquiry List</h2>
        <!--        <div class="row"><div class="col-md-4 pull-right"></div></div>--></div>
    <div class="panel panel-default">
        <!--<div class="panel-heading">Form Elements</div>-->
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-condensed table-stripped">
                <thead>
                    <tr>
                        <th>Action</th>
                        <th>Sr No.</th>
                        <th>Tele Enquiry Id</th>
                        <th>Convert to Enquiry</th>
                        <th>Add Follow Up</th>
                        <th>Enquiry Date</th>
                        <th>Name</th>
                        <th>Source</th>
                        <th>Mobile No.</th>
                        <th>Parent's No.</th>
                        <th>EMail ID.</th>
                        <th>Course Interested</th>
                        <th>Stream</th>
                        <th>Centre</th>
                        <th>Suggested Centre</th>
                        <th>Handled By</th>
                        <th>Remark</th>
                        <th>Is Walkin</th>
                        <th>Is out of station</th>
                        <th>Enquiry ID</th>
                        <th>Is Enrolled</th>
                        <th>Admission ID</th>
                        <th>Preferred Timing</th>
                        <th>Last Tele FollowUp Date</th>
                        <th>Last Tele FollowUp Details</th>
                        <th>FollowUp By</th>
                        <th>Next FollowUp Date</th>
                    </tr>
                </thead>
                <tbody>
                    <!--                    <tr><td colspan="18" class="text-center">Sorry Record Not Found</td></tr>							-->
                    <?php 
$count = $this->uri->segment(2) + 1;
foreach($tele_enquiries as $enquiry){ 
    unset($enquiry['FOLLOWUP_DETAILS_ID']);
    
                    $enquiry_id = $this->encrypt->encode($enquiry['TELE_ENQUIRY_ID']);
                    ?>
                        <tr>
                            <td><a href="<?php echo base_url('edit-tele-enquiry/'.$enquiry_id); ?>"><span class="glyphicon glyphicon-edit"></span></a> 
                                <!-- <a href="<?php //echo base_url('del-tele-enquiry/'.$enquiry_id) ?>"><span class="glyphicon glyphicon-trash"></span></a> -->
                            </td>
                            <td><?php echo $count; ?></td>
                            <td><?php echo $enquiry['TELE_ENQUIRY_ID']; ?></td>
                            <td>
                                <?php if($enquiry['ENQUIRY_ID'] == null){ ?><a href="<?php echo base_url('tele-enquiry/convert-enquiry/'.$enquiry_id); ?>"><span class="glyphicon glyphicon-question-sign"></span></a><?php }else{echo 'Enquiry already Exists';} ?>
                            </td>
                            <td><a href="<?php echo base_url('tele-enquiry-follow-up/details/'.$enquiry_id) ?>"><span class="glyphicon glyphicon-earphone" title="Follow up the Candidate"></span></a></td>
                            <?php
                                    if($enquiry['FOLLOWUP_DATE'] == '0000-00-00 00:00:00' || $enquiry['FOLLOWUP_DATE'] == ''){
                                        $FOLLOWUP_DATE = 'N/a';
                                    }
                                    else{
                                       $FOLLOWUP_DATE = date("d/m/Y", strtotime($enquiry['FOLLOWUP_DATE'])); 
                                    }
    
                                    if($enquiry['NEXT_FOLLOWUP_DATE'] == '0000-00-00 00:00:00' || $enquiry['NEXT_FOLLOWUP_DATE'] == ''){
                                        $NEXT_FOLLOWUP_DATE = 'N/a';
                                    }
                                    else{
                                       $NEXT_FOLLOWUP_DATE = date("d/m/Y", strtotime($enquiry['NEXT_FOLLOWUP_DATE'])); 
                                    }
                                     unset($enquiry['TELE_ENQUIRY_ID']);
                                     foreach($enquiry as $key => $enq){
                                        $ENQUIRY_DATE = date("d/m/Y", strtotime($enquiry['ENQUIRY_DATE']));
                                        $enquiry['ENQUIRY_DATE'] = $ENQUIRY_DATE;
                                         
                                        $enquiry['FOLLOWUP_DATE'] = $FOLLOWUP_DATE;
                                         
//                                        $NEXT_FOLLOWUP_DATE = date("d/m/Y", strtotime($enquiry['NEXT_FOLLOWUP_DATE']));
                                        $enquiry['NEXT_FOLLOWUP_DATE'] = $NEXT_FOLLOWUP_DATE;
                            ?>
                                <td>
                                    <?php echo $enquiry[$key]; if($enquiry['ISENROLLED'] == '0'){$enquiry['ISENROLLED'] = 'No';}else if($enquiry['ISENROLLED'] == '1'){$enquiry['ISENROLLED'] = 'Yes';} if($enquiry['WALKIN'] == '0'){$enquiry['WALKIN'] = 'No';}else if($enquiry['WALKIN'] == '1'){$enquiry['WALKIN'] = 'Yes';}if($enquiry['ISOUTOFSTATION'] == '0'){$enquiry['ISOUTOFSTATION'] = 'No';}else if($enquiry['ISOUTOFSTATION'] == '1'){$enquiry['ISOUTOFSTATION'] = 'Yes';}
                                    $courses_arr = explode(",",$enquiry['COURSE_INTERESTED']);
                                         $courseNames = '';
                                         echo '<ul class="list-group">';
                                          for($j=0;$j<count($courses);$j++){
                                              if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
                                                    $courseNames .= '<li class="list-group-item">'.$courses[$j]['COURSE_NAME'].'</li>';
                                                  $enquiry['COURSE_INTERESTED'] = $courseNames;
                                                }
                                              
                                          }
                                         echo '</ul>';
                                    ?>
                                </td>
                                <?php  } ?>
                        </tr>
                        <?php $count++; } ?>
                </tbody>
            </table>
            <br> </div>
        <div class="row">
            <div class="col-md-4 pull-right">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
</div>