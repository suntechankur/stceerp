<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HRA_controller extends MX_Controller {

     public function __construct()
        {
                $this->load->model('HRA_model');
                $this->load->model('tele_enquiry/tele_enquiry_model');
                parent::__construct();
        }

    public function edit_PaidLeave()
    {
        $encEmpId = $this->input->post('empId');
        $empId = $this->encrypt->decode($encEmpId);
        // $year = $this->input->post('year');
        $getEmpPl = $this->HRA_model->editEmpPl($empId);
        echo json_encode($getEmpPl);
    }
    public function updatePL()
    {

        $empId = $this->encrypt->decode($this->input->post('empId'));
        $updatePaidLeave = array(
                'EMPLOYEE_ID' => $empId,
                'YEAR' => $this->input->post('YEAR'),
                'APRIL' => $this->input->post('APRIL'),
                'MAY' => $this->input->post('MAY'),
                'JUNE' => $this->input->post('JUNE'),
                'JULY' => $this->input->post('JULY'),
                'AGUST' => $this->input->post('AGUST'),
                'SEPTEMBER' => $this->input->post('SEPTEMBER'),
                'OCTOBER' => $this->input->post('OCTOBER'),
                'NOVEMBER' => $this->input->post('NOVEMBER'),
                'DECEMBER' => $this->input->post('DECEMBER'),
                'JANUARY' => $this->input->post('JANUARY'),
                'FEBRUARY' => $this->input->post('FEBRUARY'),
                'MARCH' => $this->input->post('MARCH'),
                'TOTAL_PL' => $this->input->post('TOTAL_PL'),
                'REMARKS' => $this->input->post('REMARKS')
                );

        $this->HRA_model->updatePL($empId,$updatePaidLeave);
    }

	public function employee_paid_leaves($offset='0')
    {
        $EmpPaidLeave['centres'] = $this->tele_enquiry_model->get_centres();
        $EmpPaidLeave['designations'] = $this->HRA_model->get_designation();
        $EmpPaidLeave['departments'] = $this->HRA_model->get_department();

        if(!empty($_POST)){
            $this->session->set_userdata('filter_pl',$_POST);
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('filter_pl');
        }

        $EmpPaidLeave['filter_pl'] = $this->session->userdata('filter_pl');

        /* Pagination starts */
        $config['base_url'] = base_url('employee-paid-leaves/');
        $config['total_rows'] = $this->HRA_model->get_employee_pl_count($EmpPaidLeave['filter_pl']);
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */
        $EmpPaidLeave['pl'] = $this->HRA_model->get_employee_pl($offset,$config['per_page'],$EmpPaidLeave['filter_pl']);


        $EmpPaidLeave['pagination'] = $this->pagination->create_links();
        $EmpPaidLeave['add_bel_cust_js'] = base_url('resources/js/hr.js');
        $this->load->admin_view('employee_paid_leaves',$EmpPaidLeave);
    }


    public function hr_settings()
    {
        $payroll_data = $this->HRA_model->getpayrolldetails();
        for($i=0;$i<count($payroll_data);$i++){
            $journalName = str_replace(' ', '_', $payroll_data[$i]['CONTROLFILE_KEY']);
            $data['payroll_data'][$journalName] = $payroll_data[$i]['CONTROLFILE_VALUE'];
        }
        $data['designations'] = $this->HRA_model->get_designation();
        $this->load->admin_view('hr_settings',$data);
    }

	public function post_vacancy()
    {
        $post_vacancy['computer_skill'] = $this->HRA_model->get_skill();
        $post_vacancy['centres'] = $this->HRA_model->get_centres();
        $post_vacancy['msg'] = "";

        $post_vacancy['vacancyData'] = $this->HRA_model->get_vacancy_details();
        if(!empty($_POST))
        {
            $this->form_validation->set_rules('DESIGNATION', 'DESIGNATION', 'required');
            $this->form_validation->set_rules('NO_OF_POSITIONS', 'POSITIONS', 'required');
            $this->form_validation->set_rules('SALARY', 'SALARY', 'required');
            $this->form_validation->set_rules('CENTRE_ID', 'CENTRE_ID', 'required');
           if ($this->form_validation->run() != FALSE){

            $createDate=date("Y-m-d H:i:s");
            if($this->input->post('SKILL')!='')
            {
            $SKILLS = implode(",",$this->input->post('SKILL'));
            }
            else
            {
              $SKILLS = '';
            }

             $insert = array(
                     'DESIGNATION' => $this->input->post('DESIGNATION'),
                     'CENTRE_ID' => $this->input->post('CENTRE_ID'),
                     'SKILLS_REQUIRED' => $SKILLS,
                     'EXPERIENCE_FROM' => $this->input->post('EXPERIENCE_FROM'),
                     'EXPERIENCE_TO' => $this->input->post('EXPERIENCE_TO'),
                     'SALARY' => $this->input->post('SALARY'),
                     'NO_OF_POSITIONS' => $this->input->post('NO_OF_POSITIONS'),
                     'OTHER_DETAILS' => $this->input->post('OTHER_DETAILS'),
                     'IS_ACTIVE' => '1',
                     'CREATED_DATE' => $createDate,
                     );
              $Vacancy_details = $this->HRA_model->add_vacancy($insert);
              if($Vacancy_details){
                    $post_vacancy['msg'] = "<strong>Vacancy Created!!</strong>";
                    redirect(base_url('hra/post_vacancy'));
              }
              else{
                    $post_vacancy['msg'] = "<strong>An error occured please try again</strong>";
              }
             }

         }
        $this->load->admin_view('post_vacancy',$post_vacancy);
    }

    public function close_vacancy($id)
    {
        echo $id;
        $close_vacancy = $this->HRA_model->close_vacancy($id);
        redirect(base_url('hra/post_vacancy'));
    }


	public function add_employee()
	{
            $data['centres'] = $this->tele_enquiry_model->get_centres();
            $data['cities'] = $this->HRA_model->get_city();
            // $data['states'] = $this->HRA_model->get_state();
            $data['designations'] = $this->HRA_model->get_designation();
            $data['streams'] = $this->HRA_model->get_stream();
            $data['departments'] = $this->HRA_model->get_department();
            $data['bankBranches'] = $this->HRA_model->get_bankBranch();
            $data['computer_skill'] = $this->HRA_model->get_skill();
            $data['courses'] = $this->tele_enquiry_model->course_interested();

        if(!empty($_POST))
        {
          // echo "<pre>";
          // print_r($_POST);
          // echo "</pre>";
          // exit();

            $conNoMsg = 'the contact no field must be min 10 char in length and max 12 char in length and should contain only numbers.';

            $this->form_validation->set_rules('EMP_FNAME', 'First Name', 'required');
            $this->form_validation->set_rules('EMP_LASTNAME', 'Last Name', 'required');
            $this->form_validation->set_rules('GENDER', 'Gender', 'required');
            $this->form_validation->set_rules('ADDRESS1', 'Address', 'required');
            $this->form_validation->set_rules('CITY', 'City', 'required');
            $this->form_validation->set_rules('SUBURB', 'SUBURB', 'required');
            $this->form_validation->set_rules('STATE', 'STATE', 'required');

            $this->form_validation->set_rules('EMP_MOBILENO', 'Mobile', 'required|min_length[10]|max_length[12]|numeric');
            $this->form_validation->set_rules('EMP_TELEPHONE', 'Contact No.2', 'numeric|min_length[10]|max_length[12]');
            $this->form_validation->set_rules('DATEOFBIRTH', 'DATE OF BIRTH', 'required');
            $this->form_validation->set_rules('DATEOFJOIN', 'DATE OF JOIN', 'required');
            $this->form_validation->set_rules('CURRENT_DESIGNATION', 'CURRENT DESIGNATION', 'required');
            $this->form_validation->set_rules('CURRENT_SALARY', 'CURRENT SALARY', 'trim|required|numeric|min_length[1]|is_natural_no_zero');

            $this->form_validation->set_rules('CTC', 'CTC', 'trim|required|numeric|min_length[1]|is_natural_no_zero');
            $this->form_validation->set_rules('STREAM', 'STREAM', 'required');
            $this->form_validation->set_rules('ref_contact[]', 'Contact No', 'numeric|min_length[10]|max_length[12]',array('numeric' => $conNoMsg, 'min_length[10]' => $conNoMsg, 'max_length[12]' => $conNoMsg));
            $this->form_validation->set_rules('QUALIFICATION', 'QUALIFICATION', 'required');
            $this->form_validation->set_rules('SKILL[]', 'SKILL', 'required');
            $this->form_validation->set_rules('EMP_EMAIL', 'E-mail', 'valid_email');
            $this->form_validation->set_rules('EMP_OFFICIAL_EMAIL', 'Office E-mail', 'valid_email');
            $this->form_validation->set_rules('BASIC', 'Basic Salary', 'numeric');
            $this->form_validation->set_rules('HRA', 'HRA', 'numeric');
            $this->form_validation->set_rules('BIOMETRIC_DEVICE_ID', 'Biometric Device ID', 'numeric');
            $this->form_validation->set_rules('ZIP', 'Zip', 'numeric');

            if ($this->form_validation->run() != FALSE)
            {

            $DATEOFBIRTH = str_replace("/","-",$this->input->post('DATEOFBIRTH'));
            $DATEOFBIRTH = date("Y-m-d",strtotime($DATEOFBIRTH));

            $SKILL = implode(",",$this->input->post('SKILL'));


            $DATEOFJOIN = str_replace("/","-",$this->input->post('DATEOFJOIN'));
            $DATEOFJOIN = date("Y-m-d",strtotime($DATEOFJOIN));

            $createdBy = $this->session->userdata('admin_data')[0]['USER_ID'];
            $createDate=date("Y-m-d H:i:s");

            $insert = array(
                'EMP_FNAME' => $this->input->post('EMP_FNAME'),
                'EMP_MIDDLENAME' => $this->input->post('EMP_MIDDLENAME'),
                'EMP_LASTNAME' => $this->input->post('EMP_LASTNAME'),
                'EMP_EMAIL' => $this->input->post('EMP_EMAIL'),
                'GENDER' => $this->input->post('GENDER'),
                'ADDRESS1' => $this->input->post('ADDRESS1'),

                'ADDRESS2' => $this->input->post('ADDRESS2'),
                'CITY' => $this->input->post('CITY'),
                'ZIP' => $this->input->post('ZIP'),
                'SUBURB' => $this->input->post('SUBURB'),
                'STATE' => $this->input->post('STATE'),
                'EMP_TELEPHONE' => $this->input->post('EMP_TELEPHONE'),
                'EMP_MOBILENO' => $this->input->post('EMP_MOBILENO'),
                'DATEOFBIRTH' => $DATEOFBIRTH,
                'PAN_CARD_NO' => $this->input->post('PAN_CARD_NO'),
                'QUALIFICATION' => $this->input->post('QUALIFICATION'),

                'STREAM' => $this->input->post('STREAM'),
                'SKILL' => $SKILL,
                'DATEOFJOIN' => $DATEOFJOIN,
                'CURRENT_DESIGNATION' => $this->input->post('CURRENT_DESIGNATION'),
                'CURRENT_SALARY' => $this->input->post('CURRENT_SALARY'),
                'BANKACCOUNTNO' => $this->input->post('BANKACCOUNTNO'),

                'BRANCH_ID' => $this->input->post('BRANCH_ID'),
                'EMPLOYEEPFNO' => $this->input->post('EMPLOYEEPFNO'),
                'BIOMETRIC_DEVICE_ID' => $this->input->post('BIOMETRIC_DEVICE_ID'),
                'EMP_OFFICIAL_EMAIL' => $this->input->post('EMP_OFFICIAL_EMAIL'),
                'TA_GIVEN' => $this->input->post('TA_GIVEN'),
                'PF_APPLICABLE' => $this->input->post('PF_APPLICABLE'),

                'ISACTIVE' => $this->input->post('ISACTIVE'),
                'DEPARTMENT_ID' => $this->input->post('DEPARTMENT_ID'),
                'UA_NUMBER' => $this->input->post('UA_NUMBER'),
                'COMMITMENTWITHEMPLOYEE' => $this->input->post('COMMITMENTWITHEMPLOYEE'),
                'BASIC' => $this->input->post('BASIC'),
                'HRA' => $this->input->post('HRA'),

                'CTC' => $this->input->post('CTC'),

                'REFERENCE_NAME1' => $this->input->post('REFERENCE_NAME1'),
                'REFERENCE_RELATION1' => $this->input->post('REFERENCE_RELATION1'),
                'REFERENCE_CONTACT_NO1' => $this->input->post('REFERENCE_CONTACT_NO1'),
                'REFERENCE_NAME2' => $this->input->post('REFERENCE_NAME2'),
                'REFERENCE_RELATION2' => $this->input->post('REFERENCE_RELATION2'),
                'REFERENCE_CONTACT_NO2' => $this->input->post('REFERENCE_CONTACT_NO2'),
                'REFERENCE_NAME3' => $this->input->post('REFERENCE_NAME3'),
                'REFERENCE_RELATION3' => $this->input->post('REFERENCE_RELATION3'),
                'REFERENCE_CONTACT_NO3' => $this->input->post('REFERENCE_CONTACT_NO3'),

                'REMARKS' => $this->input->post('REMARKS'),

                'MODIFIED_BY' => $createdBy,
                'MODIFIED_DATE' => $createDate

            );

            echo "<pre>";
            print_r($insert);
            exit();
            // $EMPLOYEE_ID = $this->HRA_model->add_employee($insert);

            ////// Start code of save Employee Shifting time ////////
            // $this->HRA_model->deleteEmpShifting($EMPLOYEE_ID);
            foreach ($this->input->post('CENTRE_ID') as $k => $value) {

                    $dynamic_timing = $this->input->post('dynamic_timing')[$k];

                $num = $k+1;
                $days = implode(",",$this->input->post("days"."$num"));

                if(!empty($this->input->post('days'."$num")))
                {
                    $days = implode(",", $this->input->post('days'."$num"));
                }
                else
                {
                    $days ="";
                }

                // 'DYNAMIC_TIMING' => $dynamic_timing,

                $shifting = array(
                'employee_id' => $EMPLOYEE_ID,
                'timing_from' => $this->input->post('from_time')[$k],
                'timing_to' => $this->input->post('to_time')[$k],
                'days' => $days,
                'centre_id' => $value,
                'is_active' => "1",
                'modified_by' => $createdBy,
                'modified_date' => $createDate
                );

                print_r($shifting);
                echo "</pre>";
                // $this->HRA_model->addEmpShifting($shifting);
            }

            ////// End code of save Employee Shifting time ////////

            ////// Start code of set Login Credential ////////

            // $loginArr = array(
            //     'EMPLOYEE_ID' => $EMPLOYEE_ID,
            //     'ROLE_ID' => '',
            //     'LOGINNAME' => $EMPLOYEE_ID,
            //     'PASSWORD' => 'welcome',
            //     'ISACTIVE' => '1',
            //     'MODIFIED_BY' => $createdBy,
            //     'MODIFIED_DATE' => $createDate
            //     );
            //
            // $this->HRA_model->EmpLoginCredencial($loginArr);

            ////// End code of set Login Credential ////////

            $this->session->set_flashdata('success','Record Added Succesfully');

            redirect(base_url('hra/view-employee'));

            }
            else
            {
                $this->session->set_flashdata('danger','Please fill mandatory fields');
            }
        }
        $this->load->admin_view('add_employee', $data);
    }

	public function update_employee($id)
	{
            $id = $this->encrypt->decode($id);
            $data['centres'] = $this->tele_enquiry_model->get_centres();

            $data['cities'] = $this->HRA_model->get_city();
            $data['states'] = $this->HRA_model->get_state();
            $data['designations'] = $this->HRA_model->get_designation();
            $data['streams'] = $this->HRA_model->get_stream();
            $data['departments'] = $this->HRA_model->get_department();
            $data['bankBranches'] = $this->HRA_model->get_bankBranch();
            $data['computer_skill'] = $this->HRA_model->get_skill();
            $data['employee'] = $this->HRA_model->get_employee($id);
            $data['empShift'] = $this->HRA_model->getEmpShiftTiming($id);
        if(!empty($_POST))
        {

            $this->form_validation->set_rules('EMP_FNAME', 'First Name', 'required');
            $this->form_validation->set_rules('EMP_LASTNAME', 'Last Name', 'required');
            $this->form_validation->set_rules('GENDER', 'Gender', 'required');
            $this->form_validation->set_rules('ADDRESS1', 'Address', 'required');
            $this->form_validation->set_rules('CITY', 'City', 'required');
            $this->form_validation->set_rules('SUBURB', 'SUBURB', 'required');
            $this->form_validation->set_rules('STATE', 'STATE', 'required');

            $this->form_validation->set_rules('EMP_MOBILENO', 'Mobile', 'required|min_length[10]|max_length[12]|numeric');

            $this->form_validation->set_rules('DATEOFBIRTH', 'DATE OF BIRTH', 'required');
            $this->form_validation->set_rules('DATEOFJOIN', 'DATE OF JOIN', 'required');
            $this->form_validation->set_rules('CURRENT_DESIGNATION', 'CURRENT DESIGNATION', 'required');
            $this->form_validation->set_rules('CURRENT_SALARY', 'CURRENT SALARY', 'trim|required|numeric|min_length[1]|greater_than[0.99]');

            $this->form_validation->set_rules('CTC', 'CTC', 'trim|required|numeric|min_length[1]|greater_than[0.99]');
            $this->form_validation->set_rules('STREAM', 'STREAM', 'required');
            $this->form_validation->set_rules('QUALIFICATION', 'QUALIFICATION', 'required');
            $this->form_validation->set_rules('SKILL[]', 'SKILL', 'required');

            if ($this->form_validation->run() != FALSE)
            {

            $DATEOFBIRTH = str_replace("/","-",$this->input->post('DATEOFBIRTH'));
            $DATEOFBIRTH = date("Y-m-d",strtotime($DATEOFBIRTH));

            $SKILL = implode(",",$this->input->post('SKILL'));


            $DATEOFJOIN = str_replace("/","-",$this->input->post('DATEOFJOIN'));
            $DATEOFJOIN = date("Y-m-d",strtotime($DATEOFJOIN));

            if(!empty($this->input->post('ref_name')))
            {
                $ref_name = implode(",",$this->input->post('ref_name'));
            }
            else
            {
                $ref_name = "";
            }
            if(!empty($this->input->post('ref_relation')))
            {
                $ref_relation = implode(",",$this->input->post('ref_relation'));
            }
            else
            {
                $ref_relation = "";
            }
            if(!empty($this->input->post('ref_contact')))
            {
                $ref_contact = implode(",",$this->input->post('ref_contact'));
            }
            else
            {
                $ref_contact = "";
            }




        $update = array(
            'EMP_FNAME' => $this->input->post('EMP_FNAME'),
            'EMP_MIDDLENAME' => $this->input->post('EMP_MIDDLENAME'),
            'EMP_LASTNAME' => $this->input->post('EMP_LASTNAME'),
            'EMP_EMAIL' => $this->input->post('EMP_EMAIL'),
            'GENDER' => $this->input->post('GENDER'),
            'ADDRESS1' => $this->input->post('ADDRESS1'),

            'ADDRESS2' => $this->input->post('ADDRESS2'),
            'CITY' => $this->input->post('CITY'),
            'ZIP' => $this->input->post('ZIP'),
            'SUBURB' => $this->input->post('SUBURB'),
            'STATE' => $this->input->post('STATE'),
            'EMP_TELEPHONE' => $this->input->post('EMP_TELEPHONE'),
            'EMP_MOBILENO' => $this->input->post('EMP_MOBILENO'),
            'DATEOFBIRTH' => $DATEOFBIRTH,
            'PAN_CARD_NO' => $this->input->post('PAN_CARD_NO'),
            'QUALIFICATION' => $this->input->post('QUALIFICATION'),

            'STREAM' => $this->input->post('STREAM'),
            'SKILL' => $SKILL,
            'DATEOFJOIN' => $DATEOFJOIN,
            'CURRENT_DESIGNATION' => $this->input->post('CURRENT_DESIGNATION'),
            'CURRENT_SALARY' => $this->input->post('CURRENT_SALARY'),
            'BANKACCOUNTNO' => $this->input->post('BANKACCOUNTNO'),

            'BRANCH_ID' => $this->input->post('BRANCH_ID'),
            'EMPLOYEEPFNO' => $this->input->post('EMPLOYEEPFNO'),
            'BIOMETRIC_DEVICE_ID' => $this->input->post('BIOMETRIC_DEVICE_ID'),
            'EMP_OFFICIAL_EMAIL' => $this->input->post('EMP_OFFICIAL_EMAIL'),
            'TA_GIVEN' => $this->input->post('TA_GIVEN'),
            'PF_APPLICABLE' => $this->input->post('PF_APPLICABLE'),

            'ISACTIVE' => $this->input->post('ISACTIVE'),
            'DEPARTMENT_ID' => $this->input->post('DEPARTMENT_ID'),
            'UA_NUMBER' => $this->input->post('UA_NUMBER'),
            'COMMITMENTWITHEMPLOYEE' => $this->input->post('COMMITMENTWITHEMPLOYEE'),
            'BASIC' => $this->input->post('BASIC'),
            'HRA' => $this->input->post('HRA'),
            'CTC' => $this->input->post('CTC'),
            'REF_NAME' => "$ref_name",
            'REF_RELATION' => "$ref_relation",
            'REF_CONTACT' => "$ref_contact",
            'REMARKS' => $this->input->post('REMARKS')
        );

            $this->HRA_model->update_employee($id,$update);
            $this->HRA_model->deleteEmpShifting($id);

            foreach ($this->input->post('CENTRE_ID') as $k => $value) {



                    $dynamic_timing = $this->input->post('dynamic_timing')[$k];

                $num = $k+1;
                $days = implode(",",$this->input->post("days"."$num"));

                if(!empty($this->input->post('days'."$num")))
                {
                    $days = implode(",", $this->input->post('days'."$num"));
                }
                else
                {
                    $days ="";
                }

                $shifting = array(
                'EMPLOYEE_ID' => $id,
                'DYNAMIC_TIMING' => $dynamic_timing,
                'FROM_TIMING' => $this->input->post('from_time')[$k],
                'TO_TIMING' => $this->input->post('to_time')[$k],
                'DAYS' => $days,
                'CENTRE' => $this->input->post('CENTRE_ID')[$k]
                );

                if($this->input->post('CENTRE_ID')[$k]!="");
                {
                    $this->HRA_model->addEmpShifting($shifting);
                }
            }


            $this->session->set_flashdata('success','Record Updated Succesfully');
            redirect(base_url('hra/view-employee'));
            }
            else
            {
                $this->session->set_flashdata('danger','Please fill mandatory fields');
            }

        }

		$this->load->admin_view('update_employee', $data);
	}

  public function view_employee($offset = '0'){
        if(!empty($_POST)){
            $this->session->set_userdata('view_employee_filter_data',$_POST);
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('view_employee_filter_data');
        }
        $employee_list['filter_data'] = $this->session->userdata('view_employee_filter_data');

        /* Pagination starts */
        $config['base_url'] = base_url('hra/view-employee/');
        $config['total_rows'] = $this->HRA_model->get_employee_count($employee_list['filter_data']);
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */
        $employee_list['employees'] = $this->HRA_model->get_employee_list($offset,$config['per_page'],$employee_list['filter_data']);

        for ($i=0; $i <count($employee_list['employees']) ; $i++) {

            // $empShiftDays[$employee_list['employees'][$i]['EMPLOYEE_ID']] = $this->HRA_model->getEmpShiftDays($employee_list['employees'][$i]['EMPLOYEE_ID']);
        }

        if(isset($empShiftDays))
        {
            $employee_list['empShiftDays'] = $empShiftDays;
        }
        $employee_list['designation'] = $this->HRA_model->get_designation();
        $employee_list['department'] = $this->HRA_model->get_department();
        $employee_list['pagination'] = $this->pagination->create_links();
        $employee_list['centres'] = $this->HRA_model->get_centres();
        $this->load->admin_view('view_employee',$employee_list);
    }
    public function update_staff_login_details()
    {

    $id = $this->input->post('user_id');
    $MODIFIED_BY = $this->session->userdata('admin_data')[0]['USER_ID'];

    $update = array(
        'LOGINNAME'=>$this->input->post('loginname'),
        'PASSWORD'=>$this->input->post('user_password'),
        'MODIFIED_BY' =>$MODIFIED_BY,
        'MODIFIED_DATE' =>date('Y-m-d H:i:s')
        );

     $result = $this->HRA_model->update_staff_login($id,$update);

     echo json_encode($result);

    }
    public function update_staff_login($offset='0')
    {
        $id= $this->uri->segment(3);

        if($id!='')
        {
            $id = $this->encrypt->decode($id);
        }
        else
        {
            $id = '';
        }


        if(!empty($_POST)){
            $this->session->set_userdata('filter_data',$_POST);
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('filter_data');
        }
        $employee_list['filter_data'] = $this->session->userdata('filter_data');
        /* Pagination starts */
        $config['base_url'] = base_url('hra/update-staff-login/');
        $config['total_rows'] = $this->HRA_model->get_staff_login_count($employee_list['filter_data']);
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */
        $employee_list['employees'] = $this->HRA_model->get_staff_login($offset,$config['per_page'],$employee_list['filter_data']);

        $employee_list['pagination'] = $this->pagination->create_links();
        $employee_list['id'] = $id;
        $this->load->admin_view('update_staff_login',$employee_list);
    }

    public function export_employee_details(){
        $employee_list['filter_data'] = $this->session->userdata('filter_data');
        $employeelist = $this->HRA_model->employee_list_data($employee_list['filter_data']);

        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);

        $col = 'A';
        $j = 2;
        $alphebetCol = 'A';


        for($i=0;$i<count($employeelist['fields']);$i++){
            $objPHPExcel->getActiveSheet()->SetCellValue($col.'1', $employeelist['fields'][$i]);
            $objPHPExcel->getActiveSheet()->getStyle("A1:X1")->getFont()->setBold(true);
            $col++;
            foreach ($employeelist['details'] as $key => $value) {
                foreach ($employeelist['details'][$key] as $index => $data) {
                    $objPHPExcel->getActiveSheet()->SetCellValue($alphebetCol.$j,$employeelist['details'][$key][$index]);
                    $alphebetCol++;
                    if($alphebetCol == "Y"){
                        $alphebetCol = 'A';
                    }
                }

                $j++;

            }
        }

        $filename = "employeedetails-" . date("h.i.s.d-m-Y") . ".xls";
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=" . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $objWriter->save('php://output');
    }

    public function empIdPdf(){
      if(isset($_POST['empId'])){
        $empIdStr = '';
        for ($i=0; $i <count($_POST['empId']) ; $i++) {
            $empIdStr .= $this->encrypt->decode($_POST['empId'][$i]).',';
        }
        $empIdTrim = rtrim($empIdStr,",");
        $empIdArr = explode(",", $empIdTrim);
        $data['empIdsToPrint'] = $this->HRA_model->printEmpId($empIdArr);
        $this->load->helper('pdf_helper');
        $this->load->view('employeeIdPdf',$data);
      }
    }

    public function empIdCard(){
        $empId = $this->session->userdata('admin_data')[0]['USER_ID'];
        $data['checkIdCard'] = $this->HRA_model->checkIdCard($empId);
        $data['empDet'] = $this->HRA_model->showEmpId($empId);

        $data['add_bel_cust_js'] = base_url('resources/js/hr.js');

        if(!empty($_FILES['profImg'])){
            if($_FILES['profImg']['name'] != ''){
                $config['upload_path']          = './resources/uploads/emp/prof/';
                $config['allowed_types']        = 'jpg|png';
                $config['max_size']             = 100;
                $config['max_width']            = 160;
                $config['max_height']           = 160;

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('profImg'))
                {
                    $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                    $upload_data = array('upload_data' => $this->upload->data());
                    $prof_pic = array(
                        'EMPLOYEE_ID' => $empId,
                        'IMAGE_UPLOADED' => 'resources/uploads/emp/prof/'.$upload_data['upload_data']['file_name']
                        );
                    if (empty($data['checkIdCard'])) {
                     $this->HRA_model->insIdCard($prof_pic);
                    }
                    else{
                        $this->HRA_model->updateIdCard($empId,$prof_pic);
                    }

                    redirect(base_url('hra/idCard/'.$encEmpId));
                }
            }
        }

        $this->load->admin_view('hrIdCard',$data);
    }

    public function add_new_designation(){
        $currenttimestamp = date('Y-m-d h:i:s');

        $addnewdesignation = array(
                'CONTROLFILE_KEY' => "DESIGNATION",
                'CONTROLFILE_VALUE' => strtoupper($this->input->post('designation')),
                'CONTROLFILE_DESCRIPTION' => $this->input->post('description'),
                'MODIFIED_BY' => "0",
                'MODIFIED_DATE' => $currenttimestamp,
                'ISACTIVE' => "1",
                'Source_Lead_Rate' => "0.00"
                );

        $result = $this->HRA_model->addnewdesignation($addnewdesignation);
        if($result) {
            redirect(base_url('hra/hr_settings'));
        }
    }

    public function update_payroll_details(){
        $paydate = $this->input->post('paydate');
        $displayattendance = $this->input->post('showattendance');
        $fromdate = $this->input->post('fromdate');
        $todate = $this->input->post('todate');
        if($displayattendance == "on"){
            $displayattendance = "True";
        }
        else {
            $displayattendance = "False";
        }

        $result = $this->HRA_model->updatepayrolldetails($paydate,$displayattendance,$fromdate,$todate);

        if($result) {
            redirect(base_url('hra/hr_settings'));
        }
    }

    public function reinstate_exemployeesearch(){
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $designation = $this->input->post('designation');

        $data = $this->HRA_model->getexemployeedetails($firstname,$lastname,$designation);
        for($i=0;$i<count($data);$i++) {
            $desg = $this->HRA_model->getDesignationName($data[$i]['CURRENT_DESIGNATION']);
            if(empty($desg)) {
                $data[$i]['designation'] = "";
            }
            else {
                $data[$i]['designation'] = $desg[0]['CONTROLFILE_VALUE'];
            }
        }
        echo json_encode($data);
    }

    public function add_reinstate_employee(){
        $empid = $this->input->post('empid');

        $data['ex-employee-data'] = $this->HRA_model->getsearchedexemployeedetails($empid);

        foreach ($data['ex-employee-data'][0] as $key => $value) {
            unset($data['ex-employee-data'][0]['EMPLOYEE_ID'],$data['ex-employee-data'][0]['MODIFIED_DATE'],$data['ex-employee-data'][0]['ISACTIVE']);
        }

        for($i=0;$i<count($data['ex-employee-data']);$i++){
            $data['ex-employee-data'][0]['MODIFIED_DATE'] = date("Y-m-d h:i:s");
            $data['ex-employee-data'][0]['ISACTIVE'] = "1";

        }

        $result = $this->HRA_model->addreinstateemployee($data['ex-employee-data'][0]);

        echo json_encode($result);
    }

    public function addAppraisal($offset=0)
    {

        $appraisal['department'] = $this->HRA_model->get_department();
        $appraisal['centres'] = $this->tele_enquiry_model->get_centres();
        $appraisal['designation'] = $this->HRA_model->get_designation();

        if(!empty($_POST))
        {
            $this->session->set_userdata('appraisal_filter_data',$_POST);
            $offset='0';
        }
        if(!empty($_POST['reset']))
        {
            $this->session->unset_userdata('appraisal_filter_data');
        }
        $appraisal['appraisal_filter_data'] = $this->session->userdata('appraisal_filter_data');

        /* Pagination starts */
        $config['base_url'] = base_url('hra/addAppraisal/');
        $config['total_rows'] = $this->HRA_model->get_appraisal_employee_count($appraisal['appraisal_filter_data']);
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */

        $appraisal['appraisal_list'] = $this->HRA_model->get_appraisal_employee_list($offset,$config['per_page'],$appraisal['appraisal_filter_data']);

        $appraisal['pagination'] = $this->pagination->create_links();

        $this->load->admin_view('add_appraisal',$appraisal);
    }

    public function performanceAppraisalUpdate($APPRAISAL_ID)
    {
        $APPRAISAL_ID = $this->encrypt->decode($APPRAISAL_ID);
        $data['appraisal'] = $this->HRA_model->get_appraisal_update_data($APPRAISAL_ID);
        $data['department'] = $this->HRA_model->get_department();
        $data['designation'] = $this->HRA_model->get_designation();
        $data['centres'] = $this->tele_enquiry_model->get_centres();
        if(!empty($_POST))
        {
            $this->form_validation->set_rules('emp_no','EMPLOYEE ID','required');
            $this->form_validation->set_rules('DEPARTMENT','CURRENT DEPARTMENT','required');
            $this->form_validation->set_rules('DESIGNATION','CURRENT DESIGNATION','required');
            $this->form_validation->set_rules('current_salary','CURRENT SALARY','required');
            $this->form_validation->set_rules('last_reviewer','LAST REVIEWER','required');
            $this->form_validation->set_rules('last_review_date','LAST REVIEWER DATE','required');
            $this->form_validation->set_rules('Appraisal_due_date','APPRAISAL DUE DATE','required');
            $this->form_validation->set_rules('reviewer_name','REVIEWER NAME','required');
            $this->form_validation->set_rules('review_date','REVIEWER DATE','required');
            $this->form_validation->set_rules('Effective_date','EFFECTIVE DATE','required');
            $this->form_validation->set_rules('NEW_DEPARTMENT','NEW DEPARTMENT','required');
            $this->form_validation->set_rules('NEW_DESIGNATION','NEW DESIGNATION','required');
            $this->form_validation->set_rules('new_salary','NEW SALARY','required');
            $this->form_validation->set_rules('NEW_CTC','NEW CTC','required');
            $this->form_validation->set_rules('NEW_BASIC','NEW BASIC','required');
            $this->form_validation->set_rules('NEW_HRA','NEW HRA','required');
            $this->form_validation->set_rules('remarks','REMARKS','required');

            if($this->form_validation->run() == TRUE)
            {
                $CREATED_BY= $this->session->userdata('admin_data');
                $CREATED_BY = $CREATED_BY[0]['USER_ID'];

                $CREATED_DATE=date("Y-m-d H:i:s");

                $change_format = str_replace("/", "-", $this->input->post('last_review_date'));
                $last_review_date = date('Y-m-d', strtotime($change_format));

                $change_format = str_replace("/", "-", $this->input->post('review_date'));
                $review_date = date('Y-m-d', strtotime($change_format));

                $change_format = str_replace("/", "-", $this->input->post('Effective_date'));
                $Effective_date = date('Y-m-d', strtotime($change_format));

                $change_format = str_replace("/", "-", $this->input->post('Appraisal_due_date'));
                $Appraisal_due_date = date('Y-m-d', strtotime($change_format));

                $EMPLOYEE_ID = $this->input->post('emp_no');

                $update = array(
                    'EMPLOYEE_ID' => $EMPLOYEE_ID,
                    'OLD_DEPARTMENT_ID' => $this->input->post('DEPARTMENT'),
                    'DEPARTMENT_ID' => $this->input->post('NEW_DEPARTMENT'),
                    'LAST_REVIEW_DATE' => $last_review_date,
                    'REVIEW_DATE' => $review_date,
                    'LAST_REVIEWER_NAME' => $this->input->post('last_reviewer'),
                    'REVIEWER_NAME' => $this->input->post('reviewer_name'),
                    'OLD_SALARY' => $this->input->post('current_salary'),
                    'NEW_SALARY' => $this->input->post('new_salary'),
                    'OLD_DESIGNATION' => $this->input->post('DESIGNATION'),
                    'NEW_DESIGNATION' => $this->input->post('NEW_DESIGNATION'),
                    'EFFECTIVE_DATE' => $Effective_date,
                    'APPRAISAL_DUE_DATE' => $Appraisal_due_date,
                    'REMARKS' => $this->input->post('remarks'),
                    'CREATED_DATE' => $CREATED_DATE,
                    'CREATED_BY' => $CREATED_BY
                    );
                $this->HRA_model->update_appraisal($APPRAISAL_ID,$update);

            $updateEmployee = array(
            'CURRENT_SALARY' => $this->input->post('new_salary'),
            'BASIC' => $this->input->post('NEW_BASIC'),
            'HRA' => $this->input->post('NEW_HRA'),
            'CTC' => $this->input->post('NEW_CTC'),
            'CURRENT_DESIGNATION' => $this->input->post('NEW_DESIGNATION')
            );

            $this->HRA_model->update_employee($EMPLOYEE_ID,$updateEmployee);
            $this->session->set_flashdata('success','Appraisal Update Successfully');
            redirect(base_url('hra/empAppraisalSystem'));
            }
            else
            {
                $this->session->set_flashdata('warning','Please fill valid data.');
            }

        }

        $this->load->admin_view('performance_Appraisal_Update',$data);
    }

    public function performanceAppraisal($EMPLOYEE_ID)
    {
        $EMPLOYEE_ID = $this->encrypt->decode($EMPLOYEE_ID);
        $data['appraisal'] = $this->HRA_model->get_appraisal_add_data($EMPLOYEE_ID);
        $data['department'] = $this->HRA_model->get_department();
        $data['designation'] = $this->HRA_model->get_designation();
        $data['centres'] = $this->tele_enquiry_model->get_centres();


        if(!empty($_POST))
        {

            $this->form_validation->set_rules('emp_no','EMPLOYEE ID','required');
            $this->form_validation->set_rules('DEPARTMENT','CURRENT DEPARTMENT','required');
            $this->form_validation->set_rules('DESIGNATION','CURRENT DESIGNATION','required');
            $this->form_validation->set_rules('current_salary','CURRENT SALARY','required');
            $this->form_validation->set_rules('last_reviewer','LAST REVIEWER','required');
            $this->form_validation->set_rules('last_review_date','LAST REVIEWER DATE','required');
            $this->form_validation->set_rules('Appraisal_due_date','APPRAISAL DUE DATE','required');
            $this->form_validation->set_rules('reviewer_name','REVIEWER NAME','required');
            $this->form_validation->set_rules('review_date','REVIEWER DATE','required');
            $this->form_validation->set_rules('Effective_date','EFFECTIVE DATE','required');
            $this->form_validation->set_rules('NEW_DEPARTMENT','NEW DEPARTMENT','required');
            $this->form_validation->set_rules('NEW_DESIGNATION','NEW DESIGNATION','required');
            $this->form_validation->set_rules('new_salary','NEW SALARY','required');
            $this->form_validation->set_rules('NEW_CTC','NEW CTC','required');
            $this->form_validation->set_rules('NEW_BASIC','NEW BASIC','required');
            $this->form_validation->set_rules('NEW_HRA','NEW HRA','required');
            $this->form_validation->set_rules('remarks','REMARKS','required');

            if($this->form_validation->run() == TRUE)
            {
                $CREATED_BY= $this->session->userdata('admin_data');
                $CREATED_BY = $CREATED_BY[0]['USER_ID'];

                $CREATED_DATE=date("Y-m-d H:i:s");

                $change_format = str_replace("/", "-", $this->input->post('last_review_date'));
                $last_review_date = date('Y-m-d', strtotime($change_format));

                $change_format = str_replace("/", "-", $this->input->post('review_date'));
                $review_date = date('Y-m-d', strtotime($change_format));

                $change_format = str_replace("/", "-", $this->input->post('Effective_date'));
                $Effective_date = date('Y-m-d', strtotime($change_format));

                $change_format = str_replace("/", "-", $this->input->post('Appraisal_due_date'));
                $Appraisal_due_date = date('Y-m-d', strtotime($change_format));



                $insert = array(
                    'EMPLOYEE_ID' => $EMPLOYEE_ID,
                    'OLD_DEPARTMENT_ID' => $this->input->post('DEPARTMENT'),
                    'DEPARTMENT_ID' => $this->input->post('NEW_DEPARTMENT'),
                    'LAST_REVIEW_DATE' => $last_review_date,
                    'REVIEW_DATE' => $review_date,
                    'LAST_REVIEWER_NAME' => $this->input->post('last_reviewer'),
                    'REVIEWER_NAME' => $this->input->post('reviewer_name'),
                    'OLD_SALARY' => $this->input->post('current_salary'),
                    'NEW_SALARY' => $this->input->post('new_salary'),
                    'OLD_DESIGNATION' => $this->input->post('DESIGNATION'),
                    'NEW_DESIGNATION' => $this->input->post('NEW_DESIGNATION'),
                    'EFFECTIVE_DATE' => $Effective_date,
                    'APPRAISAL_DUE_DATE' => $Appraisal_due_date,
                    'REMARKS' => $this->input->post('remarks'),
                    'CREATED_DATE' => $CREATED_DATE,
                    'CREATED_BY' => $CREATED_BY
                    );
                $this->HRA_model->add_appraisal($insert);



            $updateEmployee = array(
            'CURRENT_SALARY' => $this->input->post('new_salary'),
            'BASIC' => $this->input->post('NEW_BASIC'),
            'HRA' => $this->input->post('NEW_HRA'),
            'CTC' => $this->input->post('NEW_CTC'),
            'CURRENT_DESIGNATION' => $this->input->post('NEW_DESIGNATION')
            );

            $this->HRA_model->update_employee($EMPLOYEE_ID,$updateEmployee);

            $this->session->set_flashdata('success','Appraisal Saved Successfully');
            redirect(base_url('hra/addAppraisal'));
            }
            else
            {
                $this->session->set_flashdata('warning','Please fill valid data.');
            }

        }

        $this->load->admin_view('performance_Appraisal',$data);
    }
    public function employeeAppraisalSystem($offset='0')
    {
        $data['designation'] = $this->HRA_model->get_designation();
        $data['centres'] = $this->tele_enquiry_model->get_centres();

        if(!empty($_POST))
        {
            $this->session->set_userdata('appraisal_search_data',$_POST);
            $offset='0';
        }
        if(!empty($_POST['reset']))
        {
            $this->session->unset_userdata('appraisal_search_data');
        }
        $data['appraisal_search_data'] = $this->session->userdata('appraisal_search_data');

        /* Pagination starts */
        $config['base_url'] = base_url('hra/empAppraisalSystem/');
        $config['total_rows'] = $this->HRA_model->search_appraisals_count($data['appraisal_search_data']);
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */

        $data['appraisal_list'] = $this->HRA_model->search_appraisals($offset,$config['per_page'],$data['appraisal_search_data']);

        $data['pagination'] = $this->pagination->create_links();

        $this->load->admin_view('emp_Appraisal_System',$data);
    }

// contollers added by ankur on 13/09/2017
    public function update_controls()
    {
        if(isset($_POST['search_controls'])){
            $this->session->set_userdata('update_controls_data',$_POST);
        }
        if(isset($_POST['reset_controls'])){
            $this->session->unset_userdata('update_controls_data');
            redirect('hra/update-controls');
        }
        $data['update_controls_data'] = $this->session->userdata('update_controls_data');

        $data['searched_data'] = $this->HRA_model->search_controls($data['update_controls_data']);

        $this->load->admin_view('update_controls',$data);
    }

    public function update_control_data(){
        $update_control_data = array(
                'CONTROLFILE_KEY' => $this->input->post('control_key'),
                'CONTROLFILE_VALUE' => strtoupper($this->input->post('control_value')),
                'CONTROLFILE_DESCRIPTION' => $this->input->post('description')
                );

        $control_id = $this->input->post('control_id');

        $result = $this->HRA_model->update_control_file($control_id,$update_control_data);

        echo json_encode($result);
    }

    public function bank_master()
    {
        if(isset($_POST['save_bank_name'])){
            $this->session->set_userdata('save_bank_name',$_POST);
            $data['save_bank_name'] = $this->session->userdata('save_bank_name');
            $bank_name = array(
                'BANK_NAME' => $this->input->post('BANK_NAME')
                );

            echo $this->input->post('BANK_NAME')."   jhbhj";

            if($this->input->post('BANK_NAME') != ''){
                $searched_data = $this->HRA_model->add_bank_name($bank_name);

                if($searched_data == "1"){
                    $this->session->set_flashdata('flashMsg', '1');
                    redirect(base_url('hra/bank-master'));
                }
            }
            else{
                $this->session->set_flashdata('flashMsg', '2');
                redirect(base_url('hra/bank-master'));
            }

        }
        if(isset($_POST['reset_name'])){
            $this->session->unset_userdata('save_bank_name');
            redirect('hra/bank-master');
        }

        $data['banks'] = $this->HRA_model->get_bank();

        $this->load->admin_view('bank_master',$data);
    }

    public function update_bank_name(){
        $update_bank_data = array(
                'BANK_NAME' => $this->input->post('bank_name')
                );

        $bank_id = $this->input->post('bank_id');

        $result = $this->HRA_model->update_bank_name($bank_id,$update_bank_data);

        echo json_encode($result);
    }

    public function add_bank_branch($bank_id,$bank_name){
        $bankId= $this->encrypt->decode($bank_id);
        $bankName= $this->encrypt->decode($bank_name);

        $data['branch_details'] = $this->HRA_model->get_bank_branch_details($bankId);

        if(isset($_POST['save_bank_branch_name'])){
            $this->session->set_userdata('save_bank_branch_name',$_POST);
            $data['save_bank_branch_name'] = $this->session->userdata('save_bank_branch_name');

            $bank_branch_data = array(
                'BANK_ID' => $bankId,
                'BRANCH_NAME' => $this->input->post('bank_branch_name'),
                'IFSC_CODE' => $this->input->post('ifsc_code')
                );

            echo $this->input->post('BANK_NAME')."   jhbhj";

            if(($this->input->post('bank_branch_name') != '') && ($this->input->post('ifsc_code') != '')){
                $searched_data = $this->HRA_model->add_bank_branch_name($bank_branch_data);

                if($searched_data == "1"){
                    $this->session->set_flashdata('flashMsg', '1');
                    redirect(base_url('hra/add-bank-branch/'.$this->encrypt->encode($bankId).'/'.$this->encrypt->encode($bankName).''));
                }
            }
            else{
                $this->session->set_flashdata('flashMsg', '2');
                redirect(base_url('hra/add-bank-branch/'.$this->encrypt->encode($bankId).'/'.$this->encrypt->encode($bankName).''));
            }

        }
        if(isset($_POST['reset_branch_name'])){
            $this->session->unset_userdata('save_bank_branch_name');
            redirect('hra/add-bank-branch/'.$this->encrypt->encode($bankId).'/'.$this->encrypt->encode($bankName).'');
        }

        $data['bank_id'] = $this->encrypt->decode($bank_id);
        $data['bank_name'] = $bankName;
        $this->load->admin_view('add_bank_branch',$data);
    }

    public function update_bank_branch_name(){
        $update_branch_data = array(
                'BRANCH_NAME' => $this->input->post('branch_name'),
                'IFSC_CODE' => $this->input->post('ifsc_code')
                );

        $bank_id = $this->input->post('bank_id');
        $branch_id = $this->input->post('branch_id');

        $result = $this->HRA_model->update_branch_name($bank_id,$branch_id,$update_branch_data);

        echo json_encode($result);
    }

    public function delete_bank_branch_name(){
        $branch_id = $this->input->post('branch_id');
        $data = array(
                'BRANCH_ID' => $this->input->post('branch_id')
                );

        $result = $this->HRA_model->delete_branch_name($data);

        echo json_encode($result);
    }

    public function monthly_attendance(){
        if(isset($_POST['generate_attendance'])){
            $this->session->set_userdata('attendance_data',$_POST);
        }
        if(isset($_POST['reset_attendance'])){
            $this->session->unset_userdata('attendance_data');
            redirect('hra/attendance');
        }

        $data['employee_attendance_search'] = $this->session->userdata('attendance_data');

        $data['centres'] = $this->tele_enquiry_model->get_centres();
        $data['employee_details'] = $this->HRA_model->get_employee_details($data['employee_attendance_search']);

        $from_date = date('d-m-Y', strtotime(str_replace('/', '-', $data['employee_attendance_search']['from_date'])));
        $to_date = date('d-m-Y', strtotime(str_replace('/', '-', $data['employee_attendance_search']['to_date'])));
        $start = strtotime($from_date);
        $end = strtotime($to_date);
        $day = (24*60*60);

        // for getting working hours reports using
        if((isset($data['employee_attendance_search']['from_date'])) && (isset($data['employee_attendance_search']['to_date']))){
          $from_date_month_name = date("F",strtotime(str_replace('/', '-', $data['employee_attendance_search']['from_date'])));
          $from_date_year = date("Y",strtotime(str_replace('/', '-', $data['employee_attendance_search']['from_date'])));
          $to_date_month_name = date("F",strtotime(str_replace('/', '-', $data['employee_attendance_search']['to_date'])));
          $to_date_year = date("Y",strtotime(str_replace('/', '-', $data['employee_attendance_search']['to_date'])));
          $datediff = $end - $start;
          $days_count = round($datediff / (60 * 60 * 24));
          if($from_date_year == $to_date_year){
            if($from_date_month_name == $from_date_month_name){
              $report_of = $from_date_month_name." ".$from_date_year;
            }
          }
          else{
            $report_of = $from_date_month_name." ".$from_date_year." - ".$to_date_month_name." ".$to_date_year;
          }
        }

        $dateArray = array();
        for($i=$start; $i<= $end; $i+=86400){
          $date_with_days = array('Date'=>date('d-m-Y', $i),'Day'=>date('l', $i));
          array_push($dateArray,$date_with_days);
        }

          $holidays_list = $this->HRA_model->get_holidays_list();
          $holidays_data = array();
          foreach($holidays_list as $holidays){
            $timestamp = strtotime($holidays['DATE']);
            $date = date('d-m-Y',$timestamp);
            array_push($holidays_data,$date);
          }

          $holidays_modifid = array();
          foreach($holidays_list as $holiday){
            $timestamp = strtotime($holiday['DATE']);
            $date = date('d-m-Y',$timestamp);
            array_push($holidays_modifid,$date);
          }

          if(!empty($data['employee_attendance_search'])){
            if(($data['employee_attendance_search']['from_date'] != "") && ($data['employee_attendance_search']['to_date'] != "") && ($data['employee_attendance_search']['centre_name'] != "")){
              $employee_details_with_attendance = array(); // blank array for final attandance data with employee_details

              foreach($data['employee_details'] as $employee){
                $data['employee_timings'] = $this->HRA_model->get_employee_timing($employee['EMPLOYEE_ID']);   // for getting employee timing details from employee timing table
                $employee_timing = array();
                $employee_timing_with_days = array();
                foreach($data['employee_timings'] as $employe_timing){
                  array_push($employee_timing,$employe_timing['Timing']);
                  array_push($employee_timing_with_days,array('Days'=>$employe_timing['DAYS'],'Time'=>$employe_timing['TIMING_FROM']));
                }
                $employee_timing = implode(", ", $employee_timing);

                $shift_details = array();
                foreach($employee_timing_with_days as $employee_time_and_day){
                  $day_initial = explode("/",$employee_time_and_day['Days']);
                  foreach($day_initial as $days){
                    switch ($days) {
                      case 'M':
                          array_push($shift_details,array('Day_name'=>'Monday','Time'=>$employee_time_and_day['Time']));
                          break;
                      case 'T':
                          array_push($shift_details,array('Day_name'=>'Tuesday','Time'=>$employee_time_and_day['Time']));
                          break;
                      case 'W':
                          array_push($shift_details,array('Day_name'=>'Wednesday','Time'=>$employee_time_and_day['Time']));
                          break;
                      case 'Th':
                          array_push($shift_details,array('Day_name'=>'Thursday','Time'=>$employee_time_and_day['Time']));
                          break;
                      case 'F':
                          array_push($shift_details,array('Day_name'=>'Friday','Time'=>$employee_time_and_day['Time']));
                          break;
                      case 'S':
                          array_push($shift_details,array('Day_name'=>'Saturday','Time'=>$employee_time_and_day['Time']));
                          break;
                    }
                  }
                }

                $data['attendance_report'] = $this->HRA_model->get_attendance_report($employee['BIOMETRIC_DEVICE_ID'],$data['employee_attendance_search']['from_date'],$data['employee_attendance_search']['to_date']);
                // for getting employee attendance details from biometric device id from exportnewlog  table

                $data['leave_approval_details'] = $this->HRA_model->get_leave_approval_report($employee['EMPLOYEE_ID'],$employee['BIOMETRIC_DEVICE_ID'],$data['employee_attendance_search']['from_date'],$data['employee_attendance_search']['to_date']);


                $attendancedate = array();  // blank array for getting all attendance datetime between from and to date
                $off_punch_count = 0;
                for($index=0;$index<count($dateArray);$index++){
                  $attendance_dates = array();  // blank array for attendance datetime one by one between from and to date

                  foreach($data['attendance_report'] as $attendance){
                    $timestamp = strtotime($attendance['LogDateTime']);
                    $date = date('d-m-Y',$timestamp);

                    if($dateArray[$index]['Date'] == $date){
                      array_push($attendance_dates,$attendance['LogDateTime']);
                    }
                  }

                  $count_timing = count($attendance_dates) - 1;
                  $in_time = "";
                  $out_time = "";
                  $difference = "";
                  $color = "";
                  $text_color = "";
                  $hrs_color = "";
                  $shift_time_difference = "";

                  if(count($attendance_dates) == 0){
                    if(in_array($dateArray[$index]['Date'], $holidays_data)){
                      $in_time = "O";
                      $out_time = "O";
                      $difference = "O";
                    }
                    else if($dateArray[$index]['Day'] == "Sunday"){
                      $in_time = "O";
                      $out_time = "O";
                      $difference = "O";
                    }
                    else{
                      $in_time = "A";
                      $out_time = "A";
                      $difference = "A";
                      $text_color = "red";
                      foreach($data['leave_approval_details'] as $leave_approvals){
                        $modified_date_format = date('Y-m-d 00:00:00',strtotime($dateArray[$index]['Date']));
                        if(in_array($modified_date_format, $leave_approvals)){
                          if($leave_approvals['approval_status'] == "approved"){
                            $in_time = "AL";
                            $out_time = "AL";
                            $difference = "AL";
                            $text_color = "green";
                          }
                          elseif($leave_approvals['approval_status'] == "disapproved"){
                            $in_time = "NAL";
                            $out_time = "NAL";
                            $difference = "NAL";
                            $text_color = "blue";
                          }
                        }
                      }
                    }
                  }

                  for($j=0;$j<count($attendance_dates);$j++){
                    $in_time = date('h:i A',strtotime($attendance_dates['0']));
                    $out_time = date('h:i A',strtotime($attendance_dates[$count_timing]));
                    foreach($shift_details as $timing_details){
                      if($timing_details['Day_name'] == $dateArray[$index]['Day']){
                        $shift_time = date('h:i A',strtotime($timing_details['Time']));

                        if(strtotime($in_time) > strtotime($timing_details['Time'])){
                          $shift_time_difference = $this->dateDiff($shift_time, $in_time);
                        }
                        else{
                          $shift_time_difference = '0';
                        }
                      }
                    }

                    // hours difference updated on 11/10/2018 by ankur
                    $difference = $this->dateDiff($out_time, $in_time);
                    if($difference == ""){
                      $difference = "0";
                      $hrs_color = "";
                    }
                    if($difference < 8.30){
                      if($difference == 'A'){
                        $hrs_color = "";
                      }
                      elseif ($difference == 'O') {
                        $hrs_color = "";
                      }
                      else{
                        $hrs_color = "red";
                      }
                    }
                    // end of difference implemented

                  }

                  // color bifercation for holidays
                  if(in_array($dateArray[$index]['Date'], $holidays_data)){
                    $color = "rgba(220, 220, 220, 0.9)";
                    $off_punch_count+= 1;
                  }
                  if($dateArray[$index]['Day'] == "Sunday"){
                    $color = "rgba(255, 255, 0, 0.4)";
                    $off_punch_count+= 1;
                  }

                  // if($in_time != "O"){
                  //   if(in_array($dateArray[$index]['Date'], $holidays_data)){
                  //     $color = "rgba(0, 125, 0, 0.4)";
                  //   }
                  //   if($dateArray[$index]['Day'] == "Sunday"){
                  //     $color = "rgba(0, 125, 0, 0.4)";
                  //   }
                  // }

                  $attendance_details = array(
                                            'Employee_id'=>$employee['EMPLOYEE_ID'],
                                            'Biometric_id'=>$employee['BIOMETRIC_DEVICE_ID'],
                                            'Centre_id'=>$employee['CENTRE_ID'],
                                            'EMPLOYEE_NAME'=>$employee['EMPLOYEE_NAME'],
                                            'Date'=>$dateArray[$index]['Date'],
                                            'Date_no'=>date('d',strtotime($dateArray[$index]['Date'])),
                                            'Day'=>$dateArray[$index]['Day'],
                                            'In'=>$in_time,
                                            'Out'=>$out_time,
                                            'Hrs'=>$difference,
                                            'Hrs_Color'=>$hrs_color,
                                            'Time_diff'=>$shift_time_difference,
                                            'Color'=>$color,
                                            'Textcolor'=>$text_color);

                  array_push($attendancedate,$attendance_details);
                }

                array_push($employee_details_with_attendance,array('EMPLOYEE_ID'=>$employee['EMPLOYEE_ID'],'EMPLOYEE_NAME'=>$employee['EMPLOYEE_NAME'],'EMPLOYEE_TIMING'=>$employee_timing,'BIOMETRIC_ID'=>$employee['BIOMETRIC_DEVICE_ID'],'CENTRE_ID'=>$employee['CENTRE_ID'],'Attendance_Data'=>$attendancedate,"Actual_off_days"=>$off_punch_count));

              }

              $data['employee_details_with_attendance'] = $employee_details_with_attendance;
            }
          }
          // echo "<pre>";
          // print_r($data);
          // echo "</pre>";
        $this->load->admin_view('attendance',$data);
    }

  // Time format is UNIX timestamp or
  // PHP strtotime compatible strings
  public function dateDiff($time1, $time2, $precision = 6) {
    date_default_timezone_set("UTC");
    // If not numeric then convert texts to unix timestamps
    if (!is_int($time1)) {
      $time1 = strtotime($time1);
    }
    if (!is_int($time2)) {
      $time2 = strtotime($time2);
    }

    // If time1 is bigger than time2
    // Then swap time1 and time2
    if ($time1 > $time2) {
      $ttime = $time1;
      $time1 = $time2;
      $time2 = $ttime;
    }

    // Set up intervals and diffs arrays
    $intervals = array('year','month','day','hour','minute','second');
    $diffs = array();

    // Loop thru all intervals
    foreach ($intervals as $interval) {
      // Create temp time from time1 and interval
      $ttime = strtotime('+1 ' . $interval, $time1);
      // Set initial values
      $add = 1;
      $looped = 0;
      // Loop until temp time is smaller than time2
      while ($time2 >= $ttime) {
        // Create new temp time from time1 and interval
        $add++;
        $ttime = strtotime("+" . $add . " " . $interval, $time1);
        $looped++;
      }

      $time1 = strtotime("+" . $looped . " " . $interval, $time1);
      $diffs[$interval] = $looped;
    }

    $count = 0;
    $times = array();
    // Loop thru all diffs
    foreach ($diffs as $interval => $value) {
      // Break if we have needed precission
      if ($count >= $precision) {
        break;
      }
      // Add value and interval
      // if value is bigger than 0
      if ($value > 0) {
        // Add s if value is not 1
        if ($value != 1) {
          $interval .= "s";
        }
        // Add value and interval to times array
        $times[] = $value . " " . $interval;
        $count++;
      }
    }

    // Return string with times

    $actual_time =  "";
    $hr = "";
    $min = "";
    foreach($times as $time){
      if (strpos($time, 'hour') !== false) {
        $hr = preg_replace('/\D/', '', $time);
      }
      if (strpos($time, 'minute') !== false) {
        $min = preg_replace('/\D/', '', $time);
      }
    }

    $actual_time = $hr.".".$min;
    if($min == ""){
      $actual_time = $hr;
    }
    elseif(($hr == "") && ($min != "")){
      $actual_time = "0.".$min;
    }
    // return implode(".", $times);
    return $actual_time;
  }

  public function leave_approval($biometric_id,$employee_id){
    $employee['biometric_id'] = $this->encrypt->decode($biometric_id);
    $employee['employee_id'] = $this->encrypt->decode($employee_id);
    $data['employee_details'] = $this->HRA_model->get_employee_details($employee);

    $data['absent_days_details'] = array();
    if(isset($_POST['get_attendance'])){
      $from_date = date('d-m-Y', strtotime(str_replace('/', '-', $_POST['from_date'])));
      $to_date = date('d-m-Y', strtotime(str_replace('/', '-', $_POST['to_date'])));
      $start = strtotime($from_date);
      $end = strtotime($to_date);

      $dateArray = array();
      for($i=$start; $i<= $end; $i+=86400){
        $date_with_days = array('Date'=>date('d-m-Y', $i),'Day'=>date('l', $i));
        array_push($dateArray,$date_with_days);
      }

      $holidays_list = $this->HRA_model->get_holidays_list();
      $holidays_modifid = array();
      foreach($holidays_list as $holiday){
        $timestamp = strtotime($holiday['DATE']);
        $date = date('d-m-Y',$timestamp);
        array_push($holidays_modifid,$date);
      }

      $data['attendance_report'] = $this->HRA_model->get_attendance_report($employee['biometric_id'],$_POST['from_date'],$_POST['to_date']);
      $data['leave_approval_details'] = $this->HRA_model->get_leave_approval_report($employee['employee_id'],$employee['biometric_id'],$_POST['from_date'],$_POST['to_date']);

      $absent_days_details = array();
      for($index=0;$index<count($dateArray);$index++){
        $attendance_dates = array();  // blank array for attendance datetime one by one between from and to date

        foreach($data['attendance_report'] as $attendance){
          $timestamp = strtotime($attendance['LogDateTime']);
          $date = date('d-m-Y',$timestamp);

          if($dateArray[$index]['Date'] == $date){
            array_push($attendance_dates,$attendance['LogDateTime']);
          }
        }

        if(count($attendance_dates) == 0){
          $a = "A";
          if(!(in_array($dateArray[$index]['Date'], $attendance_dates)) && !(in_array($dateArray[$index]['Date'], $holidays_modifid)) && ($dateArray[$index]['Day'] != "Sunday")){
            $absent_days = array('date'=>$dateArray[$index]['Date'],'type'=>$a,'status'=>"--",'approval_id'=>"--");
            $approval_status = "";
            foreach($data['leave_approval_details'] as $leave_approvals){
              $modified_date_format = date('Y-m-d 00:00:00',strtotime($dateArray[$index]['Date']));
              if(in_array($modified_date_format, $leave_approvals)){
                // $approval_status = $leave_approvals['approval_status'];
                $absent_days = array('date'=>$dateArray[$index]['Date'],'type'=>$a,'status'=>$leave_approvals['approval_status'],'approval_id'=>$leave_approvals['approval_id']);
              }
            }
            array_push($absent_days_details,$absent_days);
          }
        }
      }

      $data['absent_days_details'] = $absent_days_details;
    }

    $this->load->admin_view('approve_employee_leaves',$data);
  }

  public function update_attendance($biometric_id,$employee_id){
    $employee['biometric_id'] = $this->encrypt->decode($biometric_id);
    $employee['employee_id'] = $this->encrypt->decode($employee_id);
    $data['employee_details'] = $this->HRA_model->get_employee_details($employee);

    $data['absent_days_details'] = array();
    if(isset($_POST['get_attendance'])){
      $from_date = date('d-m-Y', strtotime(str_replace('/', '-', $_POST['from_date'])));
      $to_date = date('d-m-Y', strtotime(str_replace('/', '-', $_POST['to_date'])));
      $start = strtotime($from_date);
      $end = strtotime($to_date);

      $dateArray = array();
      for($i=$start; $i<= $end; $i+=86400){
        $date_with_days = array('Date'=>date('d-m-Y', $i),'Day'=>date('l', $i));
        array_push($dateArray,$date_with_days);
      }

      $holidays_list = $this->HRA_model->get_holidays_list();
      $holidays_modifid = array();
      foreach($holidays_list as $holiday){
        $timestamp = strtotime($holiday['DATE']);
        $date = date('d-m-Y',$timestamp);
        array_push($holidays_modifid,$date);
      }

      $data['employee_timings'] = $this->HRA_model->get_employee_timing($employee['employee_id']);   // for getting employee timing details from employee timing table
      $employee_timing = array();
      $employee_timing_with_days = array();
      foreach($data['employee_timings'] as $employe_timing){
        array_push($employee_timing,$employe_timing['Timing']);
        array_push($employee_timing_with_days,array('Days'=>$employe_timing['DAYS'],'Time'=>$employe_timing['TIMING_FROM']));
      }
      $employee_timing = implode(", ", $employee_timing);

      $shift_details = array();
      foreach($employee_timing_with_days as $employee_time_and_day){
        $day_initial = explode("/",$employee_time_and_day['Days']);
        foreach($day_initial as $days){
          switch ($days) {
            case 'M':
                array_push($shift_details,array('Day_name'=>'Monday','Time'=>$employee_time_and_day['Time']));
                break;
            case 'T':
                array_push($shift_details,array('Day_name'=>'Tuesday','Time'=>$employee_time_and_day['Time']));
                break;
            case 'W':
                array_push($shift_details,array('Day_name'=>'Wednesday','Time'=>$employee_time_and_day['Time']));
                break;
            case 'Th':
                array_push($shift_details,array('Day_name'=>'Thursday','Time'=>$employee_time_and_day['Time']));
                break;
            case 'F':
                array_push($shift_details,array('Day_name'=>'Friday','Time'=>$employee_time_and_day['Time']));
                break;
            case 'S':
                array_push($shift_details,array('Day_name'=>'Saturday','Time'=>$employee_time_and_day['Time']));
                break;
          }
        }
      }

      $data['attendance_report'] = $this->HRA_model->get_attendance_report($employee['biometric_id'],$_POST['from_date'],$_POST['to_date']);

      $attendance_details = array();
      for($index=0;$index<count($dateArray);$index++){
        $attendance_dates = array();  // blank array for attendance datetime one by one between from and to date

        foreach($data['attendance_report'] as $attendance){
          $timestamp = strtotime($attendance['LogDateTime']);
          $date = date('d-m-Y',$timestamp);

          if($dateArray[$index]['Date'] == $date){
            array_push($attendance_dates,$attendance['LogDateTime']);
          }
        }

        $count_timing = count($attendance_dates) - 1;

        $in_time = "";
        $out_time = "";
        $shift_time = "";
        for($j=0;$j<count($attendance_dates);$j++){
          $in_time = date('h:i A',strtotime($attendance_dates['0']));
          $out_time = date('h:i A',strtotime($attendance_dates[$count_timing]));
          foreach($shift_details as $timing_details){
            if($timing_details['Day_name'] == $dateArray[$index]['Day']){
              $shift_time = date('h:i A',strtotime($timing_details['Time']));
            }
          }
        }

        if(count($attendance_dates) == 0){
          $date_details = array('date'=>$dateArray[$index]['Date'],'status'=>"Approved",'in_time'=>'--','out_time'=>'--','shift_time'=>'--');
          if(in_array($dateArray[$index]['Date'], $holidays_modifid) || ($dateArray[$index]['Day'] != "Sunday")){
            $date_details = array('date'=>$dateArray[$index]['Date'],'status'=>"Approved",'in_time'=>$in_time,'out_time'=>$out_time,'shift_time'=>$shift_time);
          }
          array_push($attendance_details,$date_details);
        }

      }

      $data['attendance_details'] = $attendance_details;
    }

    // echo "<pre>";
    // print_r($data);
    // echo "</pre>";
    $this->load->admin_view('update_attendance_details',$data);
  }

  public function approve_or_disapprove_leaves(){
    if($_POST['approval_id'] != ""){
      $update_leave_approval_details = array(
                                  'approval_status'=>$_POST['status'],
                                  'leave_date'=>$_POST['date'],
                                  'biometric_id'=>$_POST['biometric_id'],
                                  'employee_id'=>$_POST['employee_id'],
                                  'modified_by'=>$this->session->userdata('admin_data')[0]['USER_ID'],
                                  'modified_date'=>date('Y-m-d h:m:s')
                                  );
      $leave_status = $this->HRA_model->update_leave_approvals($update_leave_approval_details,$_POST['approval_id']);
    }
    else{
      $insert_leave_approval_details = array(
                                  'approval_status'=>$_POST['status'],
                                  'leave_date'=>$_POST['date'],
                                  'biometric_id'=>$_POST['biometric_id'],
                                  'employee_id'=>$_POST['employee_id'],
                                  'approved_by'=>$this->session->userdata('admin_data')[0]['USER_ID'],
                                  'approved_date'=>date('Y-m-d h:m:s')
                                  );
      $leave_status = $this->HRA_model->insert_leave_approvals($insert_leave_approval_details);
    }

    if($leave_status){
      echo json_encode("1");
    }
    else{
      echo json_encode("0");
    }
  }

  public function holiday_list(){
    $data['holidays'] = $this->HRA_model->get_holiday_list_of_current_year();
    $this->load->admin_view('holidays_list',$data);
  }

  public function download_xml(){
    $this->load->admin_view('downloads');
  }
}
