<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HRA_model extends CI_Model {

  public $sql;
  public function __construct() {
      $this->etimetrack = $this->load->database('attendance_default',TRUE);
      parent::__construct();
  }

  public function get_employee_pl($offset,$perpage,$filter_pl){
        $query = $this->db->select('EM.EMPLOYEE_ID,EM.BIOMETRIC_DEVICE_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as NAME,CM.CENTRE_NAME,
          EM.DATEOFJOIN,CF.CONTROLFILE_VALUE AS DESIGNATION,EM.ISACTIVE,PL.YEAR,PL.APRIL,PL.MAY,PL.JUNE,PL.JULY,PL.AGUST,PL.SEPTEMBER,PL.OCTOBER,PL.NOVEMBER,PL.DECEMBER,PL.JANUARY,PL.FEBRUARY,PL.MARCH,PL.TOTAL_PL,PL.REMARKS')
                 // ->select_max('PL.YEAR')
                 ->from('employee_master EM')
                 ->join('centre_master CM','CM.CENTRE_ID = EM.CENTRE_ID','left')
                 ->join('paid_leave PL','EM.EMPLOYEE_ID = PL.EMPLOYEE_ID','left')
                 ->join('control_file CF','CF.CONTROLFILE_ID = EM.CURRENT_DESIGNATION','left');

      if(!empty($filter_pl)) {
        if ($filter_pl['FIRSTNAME'] != '') {
          $query = $this->db->like('EM.EMP_FNAME',$filter_pl['FIRSTNAME']);
        }

        if ($filter_pl['LASTNAME'] != '') {
          $query = $this->db->like('EM.EMP_LASTNAME',$filter_pl['LASTNAME']);
        }

        if ($filter_pl['CENTRE_ID'] != '') {
          $query = $this->db->where('EM.CENTRE_ID',$filter_pl['CENTRE_ID']);
        }

        if ($filter_pl['FROMJOINDATE'] != '') {
          $joiningFromFormat = str_replace('/', '-', $filter_pl['FROMJOINDATE']);
          $joiningDateFrom = date('Y-m-d', strtotime($joiningFromFormat));
          $query = $this->db->where('EM.DATEOFJOIN >= ', $joiningDateFrom);
        }

        if ($filter_pl['FROMJOINDATE'] != '' && $filter_pl['TOJOINDATE'] != '' && $filter_pl['FROMJOINDATE'] < $filter_pl['TOJOINDATE']) {
          $joiningToFormat = str_replace('/', '-', $filter_pl['TOJOINDATE']);
          $joiningDateTo = date('Y-m-d', strtotime($joiningToFormat));
          $query = $this->db->where('EM.DATEOFJOIN <= ', $joiningDateTo);
        }

        if ($filter_pl['CURRENT_DESIGNATION'] != '') {
          $query = $this->db->where('EM.CURRENT_DESIGNATION',$filter_pl['CURRENT_DESIGNATION']);
        }

        if ($filter_pl['DEPARTMENT_ID'] != '') {
          $query = $this->db->where('EM.DEPARTMENT_ID',$filter_pl['DEPARTMENT_ID']);
        }

        if ($filter_pl['ISACTIVE'] != '') {
          $query = $this->db->where('EM.ISACTIVE',$filter_pl['ISACTIVE']);
        }
        // print_r($filter_pl['YEAR']);
        if ($filter_pl['YEAR'] != '') {
          $query = $this->db->like('PL.YEAR',$filter_pl['YEAR']);
        }
        }
              $query = $this->db->where('EM.ISACTIVE','1');
              $query = $this->db->group_by('EM.EMPLOYEE_ID');
              $query = $this->db->limit($perpage,$offset);
              $query = $this->db->get();
              // echo $this->db->last_query();
       return $query->result_array();
    }

     public function get_employee_pl_count($filter_pl){

      $query = $this->db->select('EM.EMPLOYEE_ID')
                        ->from('employee_master EM')
                        ->where('EM.ISACTIVE','1');
      $query = $this->db->get();
        return $query->num_rows();

    }

  public function add_employee($insert)
  {
    $this->db->insert('employee_master',$insert);    // for mysql

    $qualification = $insert['QUALIFICATION'];
    $ref_contact = $insert['REF_CONTACT'];
    $ref_name = $insert['REF_NAME'];
    $ref_relation = $insert['REF_RELATION'];
    $un_number = $insert['UA_NUMBER'];
    unset($insert['QUALIFICATION'],$insert['REF_CONTACT'],$insert['REF_NAME'],$insert['REF_RELATION'],$insert['SKILL'],$insert['STREAM'],$insert['UA_NUMBER']);

    $insert['QUALIFACTION'] = $qualification;
    $insert['REFERENCE_CONTACT_NO1'] = $ref_contact;
    $insert['REFERENCE_NAME1'] = $ref_name;
    $insert['REFERENCE_RELATION1'] = $ref_relation;
    $insert['UN_NUMBER'] = $un_number;

    return $this->db->insert_id();
  }
  public function updatePL($EMPLOYEE_ID,$update)
  {
    $query = $this->db->select('EMPLOYEE_ID,YEAR')
                      ->from('paid_leave')
                      ->where('EMPLOYEE_ID',$EMPLOYEE_ID)
                      ->where('YEAR',$update['YEAR'])
                      ->get();
    if ($query->num_rows() > 0) {
      $this->db->set($update)
               ->where('EMPLOYEE_ID',$EMPLOYEE_ID)
               ->where('YEAR',$update['YEAR'])
               ->update('paid_leave');
      // echo $this->db->last_query();
    }
    else{
      $this->db->insert('paid_leave',$update);
    }

    // print"<pre>";
    // print_r($update);
    // die;
  }
  public function editEmpPl($empId){
     $query = $this->db->select("EM.EMPLOYEE_ID,EM.BIOMETRIC_DEVICE_ID,CONCAT(EM.EMP_FNAME,' ',EM.EMP_LASTNAME) as NAME,CM.CENTRE_NAME,
          EM.DATEOFJOIN,CF.CONTROLFILE_VALUE AS DESIGNATION,EM.ISACTIVE,PL.YEAR,PL.APRIL,PL.MAY,PL.JUNE,PL.JULY,PL.AGUST,PL.SEPTEMBER,PL.OCTOBER,PL.NOVEMBER,PL.DECEMBER,PL.JANUARY,PL.FEBRUARY,PL.MARCH,PL.TOTAL_PL,PL.REMARKS")
                 ->from('employee_master EM')
                 ->join('centre_master CM','CM.CENTRE_ID = EM.CENTRE_ID','left')
                 ->join('paid_leave PL','EM.EMPLOYEE_ID = PL.EMPLOYEE_ID','left')
                 ->join('control_file CF','CF.CONTROLFILE_ID = EM.CURRENT_DESIGNATION','left')
                 ->where('EM.EMPLOYEE_ID',$empId)
                 ->where('EM.ISACTIVE','1')
                 // ->where('PL.YEAR',$year)
                 ->get();
     return $query->result_array();
  }

  public function update_employee($empId,$updatePaidLeave)
  {
    $this->db->set($updatePaidLeave)
             ->where('EMPLOYEE_ID',$empId)
             ->update('employee_master');   // for mysql

    $qualification = $updatePaidLeave['QUALIFICATION'];
    $ref_contact = $updatePaidLeave['REF_CONTACT'];
    $ref_name = $updatePaidLeave['REF_NAME'];
    $ref_relation = $updatePaidLeave['REF_RELATION'];
    $un_number = $updatePaidLeave['UA_NUMBER'];
    unset($updatePaidLeave['QUALIFICATION'],$updatePaidLeave['REF_CONTACT'],$updatePaidLeave['REF_NAME'],$updatePaidLeave['REF_RELATION'],$updatePaidLeave['SKILL'],$updatePaidLeave['STREAM'],$updatePaidLeave['UA_NUMBER']);

    $updatePaidLeave['QUALIFACTION'] = $qualification;
    $updatePaidLeave['REFERENCE_CONTACT_NO1'] = $ref_contact;
    $updatePaidLeave['REFERENCE_NAME1'] = $ref_name;
    $updatePaidLeave['REFERENCE_RELATION1'] = $ref_relation;
    $updatePaidLeave['UN_NUMBER'] = $un_number;

  }
  public function EmpLoginCredencial($loginArr)
  {
    $this->db->insert('user_login',$loginArr);
  }
  public function addEmpShifting($addEmpShifting)
  {
    $this->db->insert('employee_timings',$addEmpShifting);
  }
  public function deleteEmpShifting($id)
  {
    // $this->db->where('EMPLOYEE_ID',$id);
    // $this->db->delete('shifting_master');
    $this->db->where('EMPLOYEE_ID',$id);
    $this->db->delete('employee_timings');
  }

  public function get_skill()
  {
    $query = $this->db->select('EMPLOYEE_SKILL_ID,EMPLOYEE_SKILL')
                          ->from('employee_skills_master')
                          ->where('IS_ACTIVE','1')
                          ->get();
        return $query->result_array();
  }

  public function get_centres()
  {
    $query = $this->db->select('CENTRE_ID,CENTRE_NAME')
                          ->from('centre_master')
                          ->where('ISACTIVE','1')
                          ->get();
        return $query->result_array();
  }

  public function get_city()
  {
    $query = $this->db->select('CITY_ID,CITY_NAME')
                          ->from('city_master')
                          ->where('ISACTIVE','1')
                          ->get();
        return $query->result_array();
  }
  public function get_state()
  {
    $query = $this->db->select('STATE_ID,STATE_NAME')
                          ->from('state_master')
                          ->where('ISACTIVE','1')
                          ->get();
        return $query->result_array();
  }
  public function get_designation()
  {
    $query = $this->db->select('CONTROLFILE_ID,CONTROLFILE_VALUE')
                          ->from('control_file')
                          ->where('CONTROLFILE_KEY','DESIGNATION')
                          ->where('ISACTIVE','1')
                          ->get();
        return $query->result_array();
  }
  public function get_stream()
  {
    $query = $this->db->select('CONTROLFILE_ID,CONTROLFILE_VALUE')
                          ->from('control_file')
                          ->where('CONTROLFILE_KEY','EDUCATION')
                          ->where('ISACTIVE','1')
                          ->get();
        return $query->result_array();
  }
  public function get_department()
  {
    $query = $this->db->select('DEPARTMENT_ID,DEPARTMENT_NAME')
                          ->from('department_master')
                          ->where('IS_ACTIVE','1')
                          ->get();
        return $query->result_array();
  }
    public function get_bankBranch()
	{
		$query = $this->db->select('BRANCH_ID,BANK_ID,BRANCH_NAME')
                          ->from('bank_branch')
                          ->get();
        return $query->result_array();
	}

    public function get_qualification(){
        $query = $this->db->get('qualification_master');
        return $query->result_array();
    }

  public function add_vacancy($insert)
  {
    $this->db->insert('vacancy_details',$insert);
    if ($this->db->affected_rows() > 0) {
      return true;
    }
  }

   public function get_vacancy_details()
   {
     $query = $this->db->order_by('CREATED_DATE','desc')
                       ->limit('15')
                        ->get('vacancy_details');

     return $query->result_array();
   }

   public function close_vacancy($id)
   {
     $this->db->set('IS_ACTIVE','0')
              ->where('VACANCY_ID',$id)
              ->update('vacancy_details');
   }


    public function get_employee($id){
        $query = $this->db->select('EM.EMPLOYEE_ID,EM.BIOMETRIC_DEVICE_ID,EM.EMP_FNAME,EMP_MIDDLENAME,EM.EMP_LASTNAME,EM.GENDER,CM.CENTRE_NAME,
          EM.DATEOFBIRTH,EM.DATEOFJOIN,CF.CONTROLFILE_VALUE AS DESIGNATION,EM.QUALIFICATION,EM.BASIC,EM.HRA,EM.CTC,EM.REF_NAME,EM.REF_RELATION,EM.REF_CONTACT,EM.SKILL,EM.ADDRESS1,EM.ADDRESS2,EM.CITY,EM.ZIP,EM.SUBURB,EM.STATE,EM.STREAM,EM.CURRENT_DESIGNATION,EM. EMP_TELEPHONE,EM.EMP_MOBILENO,EM.CURRENT_SALARY,EM.BRANCH_ID,EM.EMP_OFFICIAL_EMAIL,EM.EMP_EMAIL,EM.EMPLOYEEPFNO,EM.PAN_CARD_NO,EM.BANKACCOUNTNO,EM.BANK_BRANCH,EM.TA_GIVEN,EM.PF_APPLICABLE,EM.DEPARTMENT_ID,EM.UA_NUMBER,EM.COMMITMENTWITHEMPLOYEE,EM.REMARKS,EM.ISACTIVE,SM.CENTRE,SM.DYNAMIC_TIMING,SM.FROM_TIMING,SM.TO_TIMING,SM.DAYS')
                 ->from('employee_master EM')
                 ->join('centre_master CM','CM.CENTRE_ID = EM.CENTRE_ID','left')
                 ->join('shifting_master SM','SM.EMPLOYEE_ID = EM.EMPLOYEE_ID','left')
                 ->join('control_file CF','CF.CONTROLFILE_ID = EM.CURRENT_DESIGNATION','left')
                 ->where('EM.EMPLOYEE_ID',$id)
                 ->order_by('EM.EMPLOYEE_ID','desc');


              $query = $this->db->limit('1');
              $query = $this->db->get();
       return $query->result_array();
    }
    public function getEmpShiftTiming($id){
        $query = $this->db->select('SM.SHIFTING_ID,SM.CENTRE,SM.DYNAMIC_TIMING,SM.FROM_TIMING,SM.TO_TIMING,SM.DAYS')
                 ->from('shifting_master SM')
                 ->where('SM.EMPLOYEE_ID',"$id")
                 ->order_by('SM.SHIFTING_ID','ASC');
              $query = $this->db->get();
              // print $this->db->last_query();
       return $query->result_array();
    }

    public function getEmpShiftDays($empId){
      $query = $this->db->select('SM.DAYS,SM.FROM_TIMING,SM.TO_TIMING,CM.CENTRE_NAME')
                        ->from('shifting_master SM')
                        ->join('centre_master CM','CM.CENTRE_ID = SM.CENTRE')
                        ->where('EMPLOYEE_ID',$empId)
                        ->get();
      return $query->result_array();
    }

    public function get_employee_list($offset,$perpage,$filter_data){
        $query = $this->db->select('EM.EMPLOYEE_ID,EM.BIOMETRIC_DEVICE_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as NAME,EM.GENDER,CM.CENTRE_NAME,EM.STREAM,
          EM.DATEOFBIRTH,EM.DATEOFJOIN,CF.CONTROLFILE_VALUE AS DESIGNATION,EM.QUALIFICATION,EM.BASIC,EM.HRA,EM.CTC,EM.ADDRESS1,EM. EMP_TELEPHONE,EM.EMP_MOBILENO,EM.EMP_OFFICIAL_EMAIL,EM.EMP_EMAIL,EM.EMPLOYEEPFNO,EM.PAN_CARD_NO,EM.BANKACCOUNTNO,EM.BANK_BRANCH,EM.UA_NUMBER,EM.ISACTIVE')
                 ->from('employee_master EM')
                 ->join('centre_master CM','CM.CENTRE_ID = EM.CENTRE_ID','left')
                 // ->join('control_file CF','CF.CONTROLFILE_ID = ET.EMPLOYEE_ID','left')
                 ->join('control_file CF','CF.CONTROLFILE_ID = EM.CURRENT_DESIGNATION','left');
      if(!empty($filter_data)) {
        if ($filter_data['FIRSTNAME'] != '') {
          $query = $this->db->like('EM.EMP_FNAME',$filter_data['FIRSTNAME']);
        }

        if ($filter_data['LASTNAME'] != '') {
          $query = $this->db->like('EM.EMP_LASTNAME',$filter_data['LASTNAME']);
        }

        if ($filter_data['CENTRE_ID'] != '') {
          $query = $this->db->where('EM.CENTRE_ID',$filter_data['CENTRE_ID']);
        }

        if ($filter_data['JOINING_FROM'] != '') {
          $joiningFromFormat = str_replace('/', '-', $filter_data['JOINING_FROM']);
          $joiningDateFrom = date('Y-m-d', strtotime($joiningFromFormat));
          $query = $this->db->where('EM.DATEOFJOIN >= ', $joiningDateFrom);
        }

        if ($filter_data['JOINING_FROM'] != '' && $filter_data['JOINING_TO'] != '' && $filter_data['JOINING_FROM'] < $filter_data['JOINING_TO']) {
          $joiningToFormat = str_replace('/', '-', $filter_data['JOINING_TO']);
          $joiningDateTo = date('Y-m-d', strtotime($joiningToFormat));
          $query = $this->db->where('EM.DATEOFJOIN <= ', $joiningDateTo);
        }

        if ($filter_data['LEAVING_FROM'] != '') {
          $leavingFromFormat = str_replace('/', '-', $filter_data['LEAVING_FROM']);
          $leavingDateFrom = date('Y-m-d', strtotime($leavingFromFormat));
          $query = $this->db->where('EM.DATEOFLEAVING >= ', $leavingDateFrom);
        }

        if ($filter_data['LEAVING_FROM'] != '' && $filter_data['LEAVING_TO'] != '' && $filter_data['LEAVING_FROM'] < $filter_data['LEAVING_TO']) {
          $leavingToFormat = str_replace('/', '-', $filter_data['LEAVING_TO']);
          $leavingDateTo = date('Y-m-d', strtotime($leavingToFormat));
          $query = $this->db->where('EM.DATEOFLEAVING <= ', $leavingDateTo);
        }

        if ($filter_data['DESIGNATION'] != '') {
          $query = $this->db->where('EM.CURRENT_DESIGNATION',$filter_data['DESIGNATION']);
        }

        if ($filter_data['DEPARTMENT'] != '') {
          $query = $this->db->where('EM.DEPARTMENT_ID',$filter_data['DEPARTMENT']);
        }

        if ($filter_data['ACTIVE'] != '') {
          $query = $this->db->where('EM.ISACTIVE',$filter_data['ACTIVE']);
        }
                }
              $query = $this->db->order_by('EM.EMPLOYEE_ID','desc');
              $query = $this->db->limit($perpage,$offset);
              $query = $this->db->get();

       return $query->result_array();

    }

    public function get_staff_login_count($filter_data){
        $query = $this->db->select('UL.USER_ID,EM.EMPLOYEE_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as NAME,UL.ROLE_ID,UL.LOGINNAME,UL.PASSWORD,EM.GENDER,CM.CENTRE_NAME,EM.ISACTIVE')
                 ->from('employee_master EM')
                 ->join('centre_master CM','CM.CENTRE_ID = EM.CENTRE_ID','left')
                 ->join('user_login UL','UL.EMPLOYEE_ID = EM.EMPLOYEE_ID','left');
      if(!empty($filter_data)) {
        if ($filter_data['FIRSTNAME'] != '') {
          $query = $this->db->like('EM.EMP_FNAME',$filter_data['FIRSTNAME']);
        }

        if ($filter_data['LASTNAME'] != '') {
          $query = $this->db->like('EM.EMP_LASTNAME',$filter_data['LASTNAME']);
        }

        if ($filter_data['ACTIVE'] != '') {
          $query = $this->db->where('EM.ISACTIVE',$filter_data['ACTIVE']);
        }
                }
              $query = $this->db->order_by('EM.EMPLOYEE_ID','desc');
              $query = $this->db->get();

       return $query->num_rows();

    }
public function get_staff_login($offset,$perpage,$filter_data){
        $query = $this->db->select('UL.USER_ID,EM.EMPLOYEE_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as NAME,UL.ROLE_ID,UL.LOGINNAME,UL.PASSWORD,EM.GENDER,CM.CENTRE_NAME,EM.ISACTIVE')
                 ->from('employee_master EM')
                 ->join('centre_master CM','CM.CENTRE_ID = EM.CENTRE_ID','left')
                 ->join('user_login UL','UL.EMPLOYEE_ID = EM.EMPLOYEE_ID','left');
      if(!empty($filter_data)) {
        if ($filter_data['FIRSTNAME'] != '') {
          $query = $this->db->like('EM.EMP_FNAME',$filter_data['FIRSTNAME']);
        }

        if ($filter_data['LASTNAME'] != '') {
          $query = $this->db->like('EM.EMP_LASTNAME',$filter_data['LASTNAME']);
        }

        if ($filter_data['ACTIVE'] != '') {
          $query = $this->db->where('EM.ISACTIVE',$filter_data['ACTIVE']);
        }
                }
              $query = $this->db->order_by('EM.EMPLOYEE_ID','desc');
              $query = $this->db->limit($perpage,$offset);
              $query = $this->db->get();

       return $query->result_array();

    }
    public function employee_list_data($filter_data){
        $query = $this->db->select('EM.EMPLOYEE_ID,EM.BIOMETRIC_DEVICE_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as NAME,EM.GENDER,CM.CENTRE_NAME,EM.STREAM,
          EM.DATEOFBIRTH,EM.DATEOFJOIN,CF.CONTROLFILE_VALUE AS DESIGNATION,EM.QUALIFICATION,EM.BASIC,EM.HRA,EM.CTC,EM.ADDRESS1,EM. EMP_TELEPHONE,EM.EMP_MOBILENO,EM.EMP_OFFICIAL_EMAIL,EM.EMP_EMAIL,EM.EMPLOYEEPFNO,EM.PAN_CARD_NO,EM.BANKACCOUNTNO,EM.BANK_BRANCH,EM.UA_NUMBER,EM.ISACTIVE')
                 ->from('employee_master EM')
                 ->join('centre_master CM','CM.CENTRE_ID = EM.CENTRE_ID','left')
                 // ->join('control_file CF','CF.CONTROLFILE_ID = ET.EMPLOYEE_ID','left')
                 ->join('control_file CF','CF.CONTROLFILE_ID = EM.CURRENT_DESIGNATION','left');
      if(!empty($filter_data)) {
        if ($filter_data['FIRSTNAME'] != '') {
          $query = $this->db->like('EM.EMP_FNAME',$filter_data['FIRSTNAME']);
        }

        if ($filter_data['LASTNAME'] != '') {
          $query = $this->db->like('EM.EMP_LASTNAME',$filter_data['LASTNAME']);
        }

        if ($filter_data['CENTRE_ID'] != '') {
          $query = $this->db->where('EM.CENTRE_ID',$filter_data['CENTRE_ID']);
        }

        if ($filter_data['JOINING_FROM'] != '') {
          $joiningFromFormat = str_replace('/', '-', $filter_data['JOINING_FROM']);
          $joiningDateFrom = date('Y-m-d', strtotime($joiningFromFormat));
          $query = $this->db->where('EM.DATEOFJOIN >= ', $joiningDateFrom);
        }

        if ($filter_data['JOINING_FROM'] != '' && $filter_data['JOINING_TO'] != '' && $filter_data['JOINING_FROM'] < $filter_data['JOINING_TO']) {
          $joiningToFormat = str_replace('/', '-', $filter_data['JOINING_TO']);
          $joiningDateTo = date('Y-m-d', strtotime($joiningToFormat));
          $query = $this->db->where('EM.DATEOFJOIN <= ', $joiningDateTo);
        }

        if ($filter_data['LEAVING_FROM'] != '') {
          $leavingFromFormat = str_replace('/', '-', $filter_data['LEAVING_FROM']);
          $leavingDateFrom = date('Y-m-d', strtotime($leavingFromFormat));
          $query = $this->db->where('EM.DATEOFLEAVING >= ', $leavingDateFrom);
        }

        if ($filter_data['LEAVING_FROM'] != '' && $filter_data['LEAVING_TO'] != '' && $filter_data['LEAVING_FROM'] < $filter_data['LEAVING_TO']) {
          $leavingToFormat = str_replace('/', '-', $filter_data['LEAVING_TO']);
          $leavingDateTo = date('Y-m-d', strtotime($leavingToFormat));
          $query = $this->db->where('EM.DATEOFLEAVING <= ', $leavingDateTo);
        }

        if ($filter_data['DESIGNATION'] != '') {
          $query = $this->db->where('EM.CURRENT_DESIGNATION',$filter_data['DESIGNATION']);
        }

        if ($filter_data['DEPARTMENT'] != '') {
          $query = $this->db->where('EM.DEPARTMENT_ID',$filter_data['DEPARTMENT']);
        }

        if ($filter_data['ACTIVE'] != '') {
          $query = $this->db->where('EM.ISACTIVE',$filter_data['ACTIVE']);
        }
                }
              $query = $this->db->order_by('EM.EMPLOYEE_ID','desc');
              $query = $this->db->get();

        $employee_data['details'] = $query->result_array();
        $employee_data['fields'] = $query->list_fields();
       return $employee_data;
    }

    public function get_employee_count($filter_data){

      $query = $this->db->select('EM.EMPLOYEE_ID')
                        ->from('employee_master EM');

      if(!empty($filter_data))
      {

        if ($filter_data['FIRSTNAME'] != '') {
          $query = $this->db->like('EM.EMP_FNAME',$filter_data['FIRSTNAME']);
        }

        if ($filter_data['LASTNAME'] != '') {
          $query = $this->db->like('EM.EMP_LASTNAME',$filter_data['LASTNAME']);
        }

        if ($filter_data['CENTRE_ID'] != '') {
          $query = $this->db->where('EM.CENTRE_ID',$filter_data['CENTRE_ID']);
        }

        if ($filter_data['JOINING_FROM'] != '') {
          $joiningFromFormat = str_replace('/', '-', $filter_data['JOINING_FROM']);
          $joiningDateFrom = date('Y-m-d', strtotime($joiningFromFormat));
          $query = $this->db->where('EM.DATEOFJOIN >= ', $joiningDateFrom);
        }

        if ($filter_data['JOINING_FROM'] != '' && $filter_data['JOINING_TO'] != '' && $filter_data['JOINING_FROM'] < $filter_data['JOINING_TO']) {
          $joiningToFormat = str_replace('/', '-', $filter_data['JOINING_TO']);
          $joiningDateTo = date('Y-m-d', strtotime($joiningToFormat));
          $query = $this->db->where('EM.DATEOFJOIN <= ', $joiningDateTo);
        }

        if ($filter_data['LEAVING_FROM'] != '') {
          $leavingFromFormat = str_replace('/', '-', $filter_data['LEAVING_FROM']);
          $leavingDateFrom = date('Y-m-d', strtotime($leavingFromFormat));
          $query = $this->db->where('EM.DATEOFLEAVING >= ', $leavingDateFrom);
        }

        if ($filter_data['LEAVING_FROM'] != '' && $filter_data['LEAVING_TO'] != '' && $filter_data['LEAVING_FROM'] < $filter_data['LEAVING_TO']) {
          $leavingToFormat = str_replace('/', '-', $filter_data['LEAVING_TO']);
          $leavingDateTo = date('Y-m-d', strtotime($leavingToFormat));
          $query = $this->db->where('EM.DATEOFLEAVING <= ', $leavingDateTo);
        }

        if ($filter_data['DESIGNATION'] != '') {
          $query = $this->db->where('EM.CURRENT_DESIGNATION',$filter_data['DESIGNATION']);
        }

        if ($filter_data['DEPARTMENT'] != '') {
          $query = $this->db->where('EM.DEPARTMENT_ID',$filter_data['DEPARTMENT']);
        }

        if ($filter_data['ACTIVE'] != '') {
          $query = $this->db->where('EM.ISACTIVE',$filter_data['ACTIVE']);
        }
      }
        $query = $this->db->order_by('EM.EMPLOYEE_ID','desc');
        $query = $this->db->get();
        return $query->num_rows();

    }

  public function search_appraisals_count($appraisal_search_data)
  {
    $query = $this->db->select('EAD.APPRAISAL_ID,EM.EMPLOYEE_ID,EM.CENTRE_ID,CM.CENTRE_NAME,EM.DATEOFJOIN,EM.EMP_FNAME,EM.EMP_LASTNAME,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME, EAD.NEW_SALARY, EAD.OLD_SALARY,EAD.REVIEW_DATE,EAD.REMARKS,EAD.EFFECTIVE_DATE,EAD.APPRAISAL_DUE_DATE,CF.CONTROLFILE_VALUE AS DESIGNATION,EAD.OLD_DESIGNATION,EAD.NEW_DESIGNATION,EM.CURRENT_DESIGNATION')
                        ->from('employee_appraisal_details EAD')
                        ->join('employee_master EM','EM.EMPLOYEE_ID = EAD.EMPLOYEE_ID','left')
                        ->join('centre_master CM','CM.CENTRE_ID = EM.CENTRE_ID','left')
                        ->join('control_file CF','CF.CONTROLFILE_ID = EAD.OLD_DESIGNATION','left outer');



      if(!empty($appraisal_search_data))
      {
        if ($appraisal_search_data['fname'] != '')
        {
          $query = $this->db->like('EMP_FNAME',$appraisal_search_data['fname']);
        }

        if ($appraisal_search_data['lname'] != '')
        {
          $query = $this->db->like('EM.EMP_LASTNAME',$appraisal_search_data['lname']);
        }

        if ($appraisal_search_data['CENTRE_ID'] != '')
        {
          $query = $this->db->where('EM.CENTRE_ID',$appraisal_search_data['CENTRE_ID']);
        }

        if ($appraisal_search_data['review_from_date'] != '')
        {
          $change_date_from = str_replace("/", "-", $appraisal_search_data['review_from_date']);
          $review_from_date = date('Y-m-d',strtotime($change_date_from));

          $change_date_to = str_replace("/", "-", $appraisal_search_data['review_to_date']);
          $review_to_date = date('Y-m-d', strtotime($change_date_to));

          $query = $this->db->where('EM.EMP_LASTNAME > ',$review_from_date)
                            ->where('EM.EMP_LASTNAME < ',$review_to_date);
        }

        if ($appraisal_search_data['DESIGNATION'] != '')
        {
          $query = $this->db->like('EM.CURRENT_DESIGNATION',$appraisal_search_data['DESIGNATION']);
        }

      }
              $query = $this->db->get();
      return $query->num_rows();
  }
  public function search_appraisals($offset,$perpage,$appraisal_search_data)
  {
    $query = $this->db->select('EAD.APPRAISAL_ID,EM.EMPLOYEE_ID,EM.CENTRE_ID,CM.CENTRE_NAME,EM.DATEOFJOIN,EM.EMP_FNAME,EM.EMP_LASTNAME,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME, EAD.NEW_SALARY, EAD.OLD_SALARY,EAD.REVIEW_DATE,EAD.REMARKS,EAD.EFFECTIVE_DATE,EAD.APPRAISAL_DUE_DATE,CF.CONTROLFILE_VALUE AS DESIGNATION,EAD.OLD_DESIGNATION,EAD.NEW_DESIGNATION,EM.CURRENT_DESIGNATION')
                        ->from('employee_appraisal_details EAD')
                        ->join('employee_master EM','EM.EMPLOYEE_ID = EAD.EMPLOYEE_ID','left')
                        ->join('centre_master CM','CM.CENTRE_ID = EM.CENTRE_ID','left')
                        ->join('control_file CF','CF.CONTROLFILE_ID = EAD.OLD_DESIGNATION','left outer');



      if(!empty($appraisal_search_data))
      {
        if ($appraisal_search_data['fname'] != '')
        {
          $query = $this->db->like('EMP_FNAME',$appraisal_search_data['fname']);
        }

        if ($appraisal_search_data['CENTRE_ID'] != '')
        {
          $query = $this->db->where('EM.CENTRE_ID',$appraisal_search_data['CENTRE_ID']);
        }

        if ($appraisal_search_data['lname'] != '')
        {
          $query = $this->db->like('EM.EMP_LASTNAME',$appraisal_search_data['lname']);
        }

        if ($appraisal_search_data['review_from_date'] != '')
        {
          $change_date_from = str_replace("/", "-", $appraisal_search_data['review_from_date']);
          $review_from_date = date('Y-m-d',strtotime($change_date_from));

          $change_date_to = str_replace("/", "-", $appraisal_search_data['review_to_date']);
          $review_to_date = date('Y-m-d', strtotime($change_date_to));

          $query = $this->db->where('EAD.REVIEW_DATE >= ',$review_from_date)
                            ->where('EAD.REVIEW_DATE <= ',$review_to_date);
        }

        if ($appraisal_search_data['DESIGNATION'] != '')
        {
          $query = $this->db->like('EM.CURRENT_DESIGNATION',$appraisal_search_data['DESIGNATION']);
        }

      }
        $query = $this->db->limit($perpage,$offset);
        $query = $this->db->get();

  //echo $this->db->last_query();
      return $query->result_array();
  }
  public function get_appraisal_add_data($appraisal_id)
  {
      $query = $this->db->select('EM.EMPLOYEE_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME,EM.CURRENT_DESIGNATION,EM.CURRENT_SALARY')
                        ->from('employee_master EM');
      $query = $this->db->where('EM.EMPLOYEE_ID',$appraisal_id);
      $query = $this->db->get();
      //echo $this->db->last_query();
      return $query->result_array();
  }
  public function get_appraisal_update_data($appraisal_id)
  {
      /*$query = $this->db->select('EM.EMPLOYEE_ID,EM.CENTRE_ID,EM.DATEOFJOIN,EM.EMP_FNAME,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME, EAD.NEW_SALARY, EAD.OLD_SALARY,EAD.REVIEW_DATE,EAD.REMARKS,EAD.EFFECTIVE_DATE,EAD.APPRAISAL_DUE_DATE')
                        ->from('employee_appraisal_details EAD')
                        ->join('employee_master EM','EM.EMPLOYEE_ID = EAD.EMPLOYEE_ID','left');*/

                        $query = $this->db->select('EAD.APPRAISAL_ID,EM.EMPLOYEE_ID,EM.CENTRE_ID,CM.CENTRE_NAME,EM.DATEOFJOIN,EM.EMP_FNAME,EM.EMP_LASTNAME,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME, EAD.NEW_SALARY, EAD.OLD_SALARY,EAD.REVIEW_DATE,EAD.REMARKS,EAD.EFFECTIVE_DATE,EAD.APPRAISAL_DUE_DATE,CF.CONTROLFILE_VALUE AS DESIGNATION,EAD.OLD_DESIGNATION,EAD.NEW_DESIGNATION,EM.CURRENT_DESIGNATION,EAD.LAST_REVIEW_DATE,EAD.REVIEWER_NAME,EAD.LAST_REVIEWER_NAME,EM.CTC,EM.HRA,EM.BASIC,EAD.DEPARTMENT_ID,EAD.OLD_DEPARTMENT_ID')
                        ->from('employee_appraisal_details EAD')
                        ->join('employee_master EM','EM.EMPLOYEE_ID = EAD.EMPLOYEE_ID','left')
                        ->join('centre_master CM','CM.CENTRE_ID = EM.CENTRE_ID','left')
                        ->join('control_file CF','CF.CONTROLFILE_ID = EAD.OLD_DESIGNATION','left outer');

      $query = $this->db->where('EAD.APPRAISAL_ID',$appraisal_id);
      $query = $this->db->get();
      //echo $this->db->last_query();
      return $query->result_array();
  }



   public function get_appraisal_employee_count($appraisal_filter_data)
  {
    $query = $this->db->select('EM.EMPLOYEE_ID,EM.BIOMETRIC_DEVICE_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME,EM.GENDER,EM.CENTRE_ID,EM.DATEOFJOIN,EM.EMP_FNAME,EM.CURRENT_SALARY AS SALARY,EM.DATEOFJOIN,EM.DATEOFBIRTH,CF.CONTROLFILE_VALUE AS DESIGNATION,EM.QUALIFICATION,EM.CTC,EM.BASIC,EM.HRA,EM.ADDRESS1,CONCAT(EM.EMP_TELEPHONE,", ",EM.EMP_MOBILENO) AS CONTACTNO,EM.EMP_OFFICIAL_EMAIL,EM.EMP_EMAIL,EM.EMPLOYEEPFNO,EM.PAN_CARD_NO,EM.DATEOFLEAVING,EM.LEAVING_REMARKS,EM.ISACTIVE,EM.BANKACCOUNTNO,BANK.BRANCH_NAME,EM.UA_NUMBER,EM.DEPARTMENT_ID')
                        ->from('employee_master EM')
                        ->join('control_file CF','CF.CONTROLFILE_ID = EM.CURRENT_DESIGNATION','left outer')
                        ->join('department_master DM','EM.DEPARTMENT_ID = DM.DEPARTMENT_ID','left outer')
                        ->join('bank_branch BANK','EM.BRANCH_ID = BANK.BRANCH_ID','left outer');

                         $query = $this->db->where('CF.CONTROLFILE_KEY','DESIGNATION');


      if(!empty($appraisal_filter_data))
      {

        if ($appraisal_filter_data['emp_no'] != '')
        {
          $query = $this->db->where('EM.EMPLOYEE_ID',$appraisal_filter_data['emp_no']);
        }

        if ($appraisal_filter_data['emp_name'] != '')
        {
          $query = $this->db->like('EM.EMP_FNAME',$appraisal_filter_data['emp_name']);
        }
        if ($appraisal_filter_data['centre_name'] != '')
        {
          $query = $this->db->where('EM.CENTRE_ID',$appraisal_filter_data['centre_name']);
        }


        if ($appraisal_filter_data['joining_from'] != '' && $appraisal_filter_data['joining_to']!='')
        {
          $date_format_change_from = str_replace("/", "-", $appraisal_filter_data['joining_from']);
          $joining_from_date = date('Y-m-d',strtotime($date_format_change_from));

          $date_format_change_to = str_replace("/", "-", $appraisal_filter_data['joining_to']);
          $joining_to_date = date('Y-m-d',strtotime($date_format_change_to));

          $query = $this->db->where('EM.DATEOFJOIN >',$joining_from_date)
                            ->where('EM.DATEOFJOIN <',$joining_to_date);
        }

        if ($appraisal_filter_data['joining_from'] != '' && $appraisal_filter_data['joining_to']=='')
        {
          $date_format_change_from = str_replace("/", "-", $appraisal_filter_data['joining_from']);
          $joining_from_date = date('Y-m-d',strtotime($date_format_change_from));

          $query = $this->db->where('EM.DATEOFJOIN >',$joining_from_date);

        }


        if ($appraisal_filter_data['is_active'] != '')
        {
          $query = $this->db->where('EM.ISACTIVE',$appraisal_filter_data['is_active']);
        }

        if ($appraisal_filter_data['designation'] != '')
        {
          $query = $this->db->like('CF.CONTROLFILE_ID',$appraisal_filter_data['designation']);
        }

        if ($appraisal_filter_data['department'] != '')
        {
          $query = $this->db->where('EM.DEPARTMENT_ID',$appraisal_filter_data['department']);
        }

      }

              $query = $this->db->get();
      return $query->num_rows();
  }
  public function get_appraisal_employee_list($offset,$perpage,$appraisal_filter_data)
  {

    // EM.EMPLOYEE_ID AS TIMING,EM.EMP_MOBILENO,DM.DEPARTMENT_ID,DM.DEPARTMENT_NAME,

      $query = $this->db->select('EM.EMPLOYEE_ID,EM.BIOMETRIC_DEVICE_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME,EM.GENDER,EM.CENTRE_ID,CM.CENTRE_NAME,EM.EMP_FNAME,EM.CURRENT_SALARY AS SALARY,EM.DATEOFJOIN,EM.DATEOFBIRTH,CF.CONTROLFILE_ID,CF.CONTROLFILE_VALUE AS DESIGNATION,EM.QUALIFICATION,EM.CTC,EM.BASIC,EM.HRA,EM.ADDRESS1,CONCAT(EM.EMP_TELEPHONE,", ",EM.EMP_MOBILENO) AS CONTACTNO,EM.EMP_OFFICIAL_EMAIL,EM.EMP_EMAIL,EM.EMPLOYEEPFNO,EM.PAN_CARD_NO,EM.DATEOFLEAVING,EM.LEAVING_REMARKS,EM.ISACTIVE,EM.BANKACCOUNTNO,BANK.BRANCH_NAME,EM.UA_NUMBER,DM.DEPARTMENT_NAME,EM.DEPARTMENT_ID')
                        ->from('employee_master EM')
                        ->join('control_file CF','CF.CONTROLFILE_ID = EM.CURRENT_DESIGNATION','left outer')
                        ->join('department_master DM','EM.DEPARTMENT_ID = DM.DEPARTMENT_ID','left outer')
                        ->join('centre_master CM','CM.CENTRE_ID = EM.CENTRE_ID','left')
                        ->join('bank_branch BANK','EM.BRANCH_ID = BANK.BRANCH_ID','left outer');

                        $query = $this->db->where('CF.CONTROLFILE_KEY','DESIGNATION');


      if(!empty($appraisal_filter_data))
      {

        if ($appraisal_filter_data['emp_no'] != '')
        {
          $query = $this->db->where('EM.EMPLOYEE_ID',$appraisal_filter_data['emp_no']);
        }

        if ($appraisal_filter_data['emp_name'] != '')
        {
          $query = $this->db->like('EM.EMP_FNAME',$appraisal_filter_data['emp_name']);
        }
        if ($appraisal_filter_data['centre_name'] != '')
        {
          $query = $this->db->where('EM.CENTRE_ID',$appraisal_filter_data['centre_name']);
        }


        if ($appraisal_filter_data['joining_from'] != '' && $appraisal_filter_data['joining_to']!='')
        {
          $date_format_change_from = str_replace("/", "-", $appraisal_filter_data['joining_from']);
          $joining_from_date = date('Y-m-d',strtotime($date_format_change_from));

          $date_format_change_to = str_replace("/", "-", $appraisal_filter_data['joining_to']);
          $joining_to_date = date('Y-m-d',strtotime($date_format_change_to));

          $query = $this->db->where('EM.DATEOFJOIN >',$joining_from_date)
                            ->where('EM.DATEOFJOIN <',$joining_to_date);
        }

        if ($appraisal_filter_data['joining_from'] != '' && $appraisal_filter_data['joining_to']=='')
        {
          $date_format_change_from = str_replace("/", "-", $appraisal_filter_data['joining_from']);
          $joining_from_date = date('Y-m-d',strtotime($date_format_change_from));

          $query = $this->db->where('EM.DATEOFJOIN >',$joining_from_date);

        }


        if ($appraisal_filter_data['is_active'] != '')
        {
          $query = $this->db->where('EM.ISACTIVE',$appraisal_filter_data['is_active']);
        }

        if ($appraisal_filter_data['designation'] != '')
        {
          $query = $this->db->like('CF.CONTROLFILE_ID',$appraisal_filter_data['designation']);
        }

        if ($appraisal_filter_data['department'] != '')
        {
          $query = $this->db->where('EM.DEPARTMENT_ID',$appraisal_filter_data['department']);
        }

      }
              $query = $this->db->limit($perpage,$offset);
              $query = $this->db->get();
              //echo $this->db->last_query();
      return $query->result_array();
  }

    public function printEmpId($empIdArr){
      $query = $this->db->select('EM.EMPLOYEE_ID,EM.EMP_FNAME,EM.EMP_LASTNAME,CF.CONTROLFILE_VALUE,EM.DATEOFJOIN')
                        ->from('employee_master EM')
                        ->join('control_file CF','CF.CONTROLFILE_ID = EM.CURRENT_DESIGNATION')
                        ->where_in('EM.EMPLOYEE_ID',$empIdArr)
                        ->get();
      return $query->result_array();
    }

    public function showEmpId($empId){
      $query = $this->db->select('EM.EMPLOYEE_ID,EM.EMP_FNAME,EM.EMP_LASTNAME,CF.CONTROLFILE_VALUE,EM.DATEOFJOIN')
                        ->from('employee_master EM')
                        ->join('control_file CF','CF.CONTROLFILE_ID = EM.CURRENT_DESIGNATION','left')
                        ->where('EM.EMPLOYEE_ID',$empId)
                        ->get();
      return $query->result_array();
    }

    public function checkIdCard($empId){
      $query = $this->db->select('IMAGE_UPLOADED')
                        ->from('employee_id_master')
                        ->where('EMPLOYEE_ID',$empId)
                        ->get();
      return $query->result_array();
    }

    public function insIdCard($prof_pic){
      $this->db->insert('employee_id_master',$prof_pic);
    }

    public function updateIdCard($empId,$prof_pic){
      $this->db->set($prof_pic)
               ->where('EMPLOYEE_ID',$empId)
               ->update('employee_id_master');
    }

    public function update_staff_login($id,$update)
    {
      $this->db->set($update)
      ->where('USER_ID',$id)
      ->update('user_login');

      return TRUE;

    }

    public function addnewdesignation($addnewdesignation){
      $this->db->insert('control_file',$addnewdesignation);
      if ($this->db->affected_rows() > 0) {
        return true;
      }
    }

    public function getpayrolldetails(){
      $where = "CONTROLFILE_KEY = 'ATTENDANCE ACTIVE' OR CONTROLFILE_KEY ='ATTENDANCE FROM DATE' OR CONTROLFILE_KEY = 'ATTENDANCE TO DATE' OR CONTROLFILE_KEY = 'PAY SLIP DATE'";
      $query = $this->db->select('CONTROLFILE_KEY,CONTROLFILE_VALUE')
                        ->from('control_file')
                        ->where($where)
                        ->get();
      return $query->result_array();
    }

    public function updatepayrolldetails($paydate,$displayattendance,$fromdate,$todate){
    $currenttimestamp = date('Y-m-d h:i:s');

    $query = $this->db->query("UPDATE `control_file` SET `CONTROLFILE_VALUE` = '$paydate' , `MODIFIED_DATE` = '$currenttimestamp' WHERE `CONTROLFILE_KEY` = 'PAY SLIP DATE'");
    $query1 = $this->db->query("UPDATE `control_file` SET `CONTROLFILE_VALUE` = '$displayattendance' , `MODIFIED_DATE` = '$currenttimestamp' WHERE `CONTROLFILE_KEY` = 'ATTENDANCE ACTIVE'");
    $query2 = $this->db->query("UPDATE `control_file` SET `CONTROLFILE_VALUE` = '$fromdate' , `MODIFIED_DATE` = '$currenttimestamp' WHERE `CONTROLFILE_KEY` = 'ATTENDANCE FROM DATE'");
    $query3 = $this->db->query("UPDATE `control_file` SET `CONTROLFILE_VALUE` = '$todate' , `MODIFIED_DATE` = '$currenttimestamp' WHERE `CONTROLFILE_KEY` = 'ATTENDANCE TO DATE'");

      if($query && $query1 && $query2 && $query3){
        return TRUE;
      }

    }

    public function getexemployeedetails($firstname,$lastname,$designation) {
      if($firstname && $lastname && $designation) {
        $where = "ISACTIVE = '0' AND EMP_FNAME LIKE '$firstname%' AND EMP_LASTNAME LIKE '$lastname%' AND CURRENT_DESIGNATION = '$designation'";
        $query = $this->db->select('EMPLOYEE_ID,CENTRE_ID,EMP_FNAME,EMP_MIDDLENAME,EMP_LASTNAME,CURRENT_DESIGNATION,EMP_TELEPHONE,EMP_MOBILENO,DATEOFLEAVING')
                          ->from('employee_master')
                          ->where($where)
                          ->get();
        return $query->result_array();
      }
      else if($firstname && $lastname) {
        $where = "ISACTIVE = '0' AND EMP_FNAME LIKE '$firstname%' AND EMP_LASTNAME LIKE '$lastname%'";
        $query = $this->db->select('EMPLOYEE_ID,CENTRE_ID,EMP_FNAME,EMP_MIDDLENAME,EMP_LASTNAME,CURRENT_DESIGNATION,EMP_TELEPHONE,EMP_MOBILENO,DATEOFLEAVING')
                          ->from('employee_master')
                          ->where($where)
                          ->get();
        return $query->result_array();
      }
      else if($firstname && $designation) {
        $where = "ISACTIVE = '0' AND EMP_FNAME LIKE '$firstname%' AND CURRENT_DESIGNATION = '$designation'";
        $query = $this->db->select('EMPLOYEE_ID,CENTRE_ID,EMP_FNAME,EMP_MIDDLENAME,EMP_LASTNAME,CURRENT_DESIGNATION,EMP_TELEPHONE,EMP_MOBILENO,DATEOFLEAVING')
                          ->from('employee_master')
                          ->where($where)
                          ->get();
        return $query->result_array();
      }
      else if($lastname && $designation) {
        $where = "ISACTIVE = '0' AND EMP_LASTNAME LIKE '$lastname%' AND CURRENT_DESIGNATION = '$designation'";
        $query = $this->db->select('EMPLOYEE_ID,CENTRE_ID,EMP_FNAME,EMP_MIDDLENAME,EMP_LASTNAME,CURRENT_DESIGNATION,EMP_TELEPHONE,EMP_MOBILENO,DATEOFLEAVING')
                          ->from('employee_master')
                          ->where($where)
                          ->get();
        return $query->result_array();
      }
      else if($firstname) {
        $where = "ISACTIVE = '0' AND EMP_FNAME LIKE '$firstname%'";
        $query = $this->db->select('EMPLOYEE_ID,CENTRE_ID,EMP_FNAME,EMP_MIDDLENAME,EMP_LASTNAME,CURRENT_DESIGNATION,EMP_TELEPHONE,EMP_MOBILENO,DATEOFLEAVING')
                          ->from('employee_master')
                          ->where($where)
                          ->get();
        return $query->result_array();
      }
      else if($lastname) {
        $where = "ISACTIVE = '0' AND EMP_LASTNAME LIKE '$lastname%'";
        $query = $this->db->select('EMPLOYEE_ID,CENTRE_ID,EMP_FNAME,EMP_MIDDLENAME,EMP_LASTNAME,CURRENT_DESIGNATION,EMP_TELEPHONE,EMP_MOBILENO,DATEOFLEAVING')
                          ->from('employee_master')
                          ->where($where)
                          ->get();
        return $query->result_array();
      }
      else if($designation) {
        $where = "ISACTIVE = '0' AND CURRENT_DESIGNATION = '$designation'";
        $query = $this->db->select('EMPLOYEE_ID,CENTRE_ID,EMP_FNAME,EMP_MIDDLENAME,EMP_LASTNAME,CURRENT_DESIGNATION,EMP_TELEPHONE,EMP_MOBILENO,DATEOFLEAVING')
                          ->from('employee_master')
                          ->where($where)
                          ->get();
        return $query->result_array();
      }
    }

    public function getDesignationName($designationId){
        $where = "CONTROLFILE_KEY ='DESIGNATION' AND CONTROLFILE_ID='$designationId'";
        $query = $this->db->select('CONTROLFILE_VALUE')
                          ->from('control_file')
                          ->where($where)
                          ->get();
        return $query->result_array();
    }

    public function getsearchedexemployeedetails($empid){
        $where = "EMPLOYEE_ID ='$empid' AND ISACTIVE='0'";
        $query = $this->db->select('*')
                          ->from('employee_master')
                          ->where($where)
                          ->get();
        return $query->result_array();
    }

    public function addreinstateemployee($addreinstateemployee){
      $this->db->insert('employee_master',$addreinstateemployee);   // for mysql
      return $this->db->insert_id();
    }

    public function add_appraisal($appraisal)
    {
      $this->db->insert('employee_appraisal_details',$appraisal);
      if ($this->db->affected_rows() > 0) {
        return true;
      }
    }

    public function update_appraisal($id,$update)
    {
      $this->db->set($update)
      ->where('APPRAISAL_ID',$id)
      ->update('employee_appraisal_details');
     // echo $this->db->last_query();
      return true;
    }


    public function search_controls($searched_data){
        $query = $this->db->select('CONTROLFILE_ID ,
                  CONTROLFILE_KEY ,
                  CONTROLFILE_VALUE,
                  CONTROLFILE_DESCRIPTION')
                 ->from('control_file');

              if(!empty($searched_data)) {
                if ($searched_data['control_file_key'] != '') {
                  $query = $this->db->like('CONTROLFILE_KEY',$searched_data['control_file_key'],'after');
                }
                if ($searched_data['control_file_value'] != '') {
                  $query = $this->db->like('CONTROLFILE_VALUE',$searched_data['control_file_value'],'after');
                }
                if ($searched_data['description'] != '') {
                  $query = $this->db->like('CONTROLFILE_DESCRIPTION',$searched_data['description'],'after');
                }
              }

              $query = $this->db->where('ISACTIVE','1');
              $query = $this->db->order_by('CONTROLFILE_VALUE');
              $query = $this->db->get();

      return $query->result_array();
    }

    public function update_control_file($id,$searched_data){
      $query = $this->db->set($searched_data)
               ->where('CONTROLFILE_ID',$id)
               ->update('control_file');

      if ($this->db->affected_rows() > 0) {
        return true;
      }
      else{
        return false;
      }
    }

    public function get_bank(){
      $query = $this->db->select('*')
                        ->from('bank_master');

      $query = $this->db->get();

      return $query->result_array();
    }

    public function add_bank_name($bank_name){
      $query = $this->db->insert('bank_master',$bank_name);

      if ($this->db->affected_rows() > 0) {
        return true;
      }
      else{
        return false;
      }
    }

    public function update_bank_name($id,$bank_data){
      $query = $this->db->set($bank_data)
                        ->where('BANK_ID',$id)
                        ->update('bank_master');

      if ($this->db->affected_rows() > 0) {
        return true;
      }
      else{
        return false;
      }
    }

    public function get_bank_branch_details($id){
      $query = $this->db->select('*')
                        ->from('bank_branch')
                        ->where('BANK_ID',$id);

      $query = $this->db->get();

      return $query->result_array();
    }

    public function add_bank_branch_name($bank_data){
      $query = $this->db->insert('bank_branch',$bank_data);

      if ($this->db->affected_rows() > 0) {
        return true;
      }
      else{
        return false;
      }
    }

    public function update_branch_name($ba_id,$br_id,$bank_data){
      $query = $this->db->set($bank_data)
                        ->where('BANK_ID',$ba_id)
                        ->where('BRANCH_ID',$br_id)
                        ->update('bank_branch');

      if ($this->db->affected_rows() > 0) {
        return true;
      }
      else{
        return false;
      }
    }

    public function delete_branch_name($br_id){
      $query = $this->db->delete('bank_branch',$br_id);

      if ($this->db->affected_rows() > 0) {
        return true;
      }
      else{
        return false;
      }
    }

    public function get_employee_timing($employee_id){
      $query = $this->db->select('concat(cm.CENTRE_NAME,": ",em.DAYS," - ",em.TIMING_FROM," to ",em.TIMING_TO) as Timing,cm.CENTRE_NAME,em.DAYS,em.TIMING_FROM,em.TIMING_TO')
                        ->from('EMPLOYEE_TIMINGS em')
                        ->join('centre_master cm','em.centre_id=cm.centre_id','left')
                        ->where('em.IS_ACTIVE','1')
                        ->where('em.EMPLOYEE_ID',$employee_id);

      $query = $this->db->get();

      return $query->result_array();
    }

    public function get_employee_details($attendance_data){
      $query = $this->db->select('EMPLOYEE_ID,CENTRE_ID,CONCAT(EMP_FNAME," ",EMP_MIDDLENAME," ",EMP_LASTNAME) as EMPLOYEE_NAME,BIOMETRIC_DEVICE_ID')
                        ->from('employee_master');

      if(!empty($attendance_data)) {
        if(isset($attendance_data['first_name'])){
          if ($attendance_data['first_name'] != '') {
            $query = $this->db->like('EMP_FNAME',$attendance_data['first_name'],'after');
          }
        }
        if(isset($attendance_data['last_name'])){
          if ($attendance_data['last_name'] != '') {
            $query = $this->db->like('EMP_LASTNAME',$attendance_data['last_name'],'after');
          }
        }
        if(isset($attendance_data['active_status'])){
          if ($attendance_data['active_status'] != '') {
            $query = $this->db->where('ISACTIVE',$attendance_data['active_status']);
          }
        }
        if(isset($attendance_data['centre_name'])){
          if ($attendance_data['centre_name'] != '') {
            $query = $this->db->where('CENTRE_ID',$attendance_data['centre_name']);
          }
        }
        if(isset($attendance_data['employee_id'])){
          if ($attendance_data['employee_id'] != '') {
            $query = $this->db->where('EMPLOYEE_ID',$attendance_data['employee_id']);
          }
        }
      }

      $query = $this->db->get();

      return $query->result_array();
    }

    public function get_attendance_report($biometric_id,$from_date,$to_date){
      $date_format_change = str_replace("/","-",$from_date);
      $from_date = date('Y-m-d', strtotime($date_format_change));

      $to_date_format_change = str_replace("/","-",$to_date);
      $to_date = date('Y-m-d', strtotime($to_date_format_change. ' + 1 day'));

      $where = " LogDateTime BETWEEN '$from_date' and '$to_date'";

      $query = $this->etimetrack->select('*')
                                ->from('exportnewlog')
                                ->where('EmpCode',$biometric_id)
                                ->where($where)
                                ->order_by('LogDateTime','ASC');

      $query = $this->etimetrack->get();

      return $query->result_array();
    }

    public function get_leave_approval_report($employee_id,$biometric_id,$from_date,$to_date){
      $date_format_change = str_replace("/","-",$from_date);
      $from_date = date('Y-m-d', strtotime($date_format_change));

      $to_date_format_change = str_replace("/","-",$to_date);
      $to_date = date('Y-m-d', strtotime($to_date_format_change. ' + 1 day'));

      $where = 'leave_date between "'.$from_date.'" and "'.$to_date.'"';
      $query = $this->db->select('approval_id,leave_date,approval_status,employee_id,biometric_id')
                        ->from('employee_leave_approval')
                        ->where('employee_id',$employee_id)
                        ->where('biometric_id',$biometric_id)
                        ->where($where)
                        ->get();

      return $query->result_array();
    }

    public function insert_leave_approvals($leave_details)
    {
      $this->db->insert('employee_leave_approval',$leave_details);
      if ($this->db->affected_rows() > 0) {
        return true;
      }
      else{
        return false;
      }
    }

    public function update_leave_approvals($leave_details,$approval_id)
    {
      unset($leave_details['approval_id']);
      $this->db->set($leave_details)
               ->where('approval_id',$approval_id)
               ->update('employee_leave_approval');
      if ($this->db->affected_rows() > 0) {
        return true;
      }
      else{
        return false;
      }
    }

    public function get_holidays_list(){
      $query = $this->db->select('DATE')
                        ->from('holidays')
                        ->where('IS_PAID','true')
                        ->get();
      return $query->result_array();
    }

    public function get_holiday_list_of_current_year(){
      $today = date('Y');
      $query = $this->db->select('OCCASSION,DATE')
                        ->from('holidays')
                        ->where('IS_PAID','true')
                        ->like('DATE',$today,'after')
                        ->get();

      return  $query->result_array();
    }
}
