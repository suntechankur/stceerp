<div class="gapping"></div>
<div class="create_batch_form">
<div id="box">
<h2>Performance Appraisal System</h2>

</div>
<div class="row">

<div class="col-sm-12">
<div class="panel panel-default">
<div class="panel-heading">
<!-- start message area -->

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('newBatchId')) { ?>
<div class="alert alert-success">
<?php echo "New Batch ID Generated: " . $this->session->flashdata('newBatchId'); ?>
</div>
<?php } ?>


<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info1')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
</div>
<?php } ?>

<?php if($this->session->flashdata('warning')) { ?>
<div class="alert alert-warning">
<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php } ?>

<!-- End message area --> 
</div>
<div class="panel-body">
<?php echo form_open();?>

<!--                    <form role="form" data-toggle="validator" method="post" action="lib/save.php" novalidate="true">-->
<div class="row">

	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Employee No: <span class="text-danger">*</span></label>
			    <input class="form-control" name="emp_no" id="emp_no" type="text"> 
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Employee Name: </label>
			    <input class="form-control" name="emp_name" id="emp_name" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Center : </label>
			    <div class="form-group">
			    	<select name="centre_name">
			            <option value="">Select Centre</option>
                         <?php foreach($centres as $centre){ ?>
                            <option value="<?php echo $centre['CENTRE_ID']; ?>" <?php if(isset($centre['CENTRE_ID'])==$appraisal_filter_data['centre_name']){ echo "selected";}?>><?php echo $centre['CENTRE_NAME']; ?>
                         	</option>
                         <?php } ?>
			    	</select>
			    </div>
			</div>	
		</div>

	</div>
	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group">
			    <label>From Date Of Join:<span class="text-danger">*</span></label>
			    <input name="joining_from" id="joining_from" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">  
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>To Date Of Join: </label>
			    <input class="form-control enquiry_date" name="joining_to" id="date_join_to" maxlength="10" placeholder="DD/MM/YYYY" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Is Active: </label>
			    <input class="form-control" name="is_active" id="is_active" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>

	</div>
	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Date Of Leaving(From):<span class="text-danger">*</span></label>
			    <input class="form-control" name="date_leaving_from" id="date_leaving_from" type="text"> 
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Date Of Leaving(To):</label>
			    <input class="form-control" name="date_leaving_to" id="date_leaving_to" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Designation: </label>
			    <select name="designation" class="form-control">
                    <option value="">Select Designation</option>
                        <?php foreach($designation as $dsg){ ?>
                        <option value="<?php echo $dsg['CONTROLFILE_ID']; ?>" <?php if(isset($filter_data['DESIGNATION'])){ echo ($dsg['CONTROLFILE_ID'] == $filter_data['DESIGNATION']) ? ' selected="selected"' : '';}?>><?php echo $dsg['CONTROLFILE_VALUE']; ?></option>
                    	<?php } ?>
                </select>
			</div>	
		</div>

	</div>	
	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Department :<span class="text-danger">*</span></label>
			     <select name="department" class="form-control">
                     <option value="">Select Department</option>
                     <?php foreach($department as $dpt){ ?>
                      <option value="<?php echo $dpt['DEPARTMENT_ID']; ?>" <?php if(isset($filter_data['DEPARTMENT'])){ echo ($dpt['DEPARTMENT_ID'] == $filter_data['DEPARTMENT']) ? ' selected="selected"' : '';}?>><?php echo $dpt['DEPARTMENT_NAME']; ?></option>
                              <?php } ?>
                </select>
			</div>	
		</div>
	</div>
	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group" align="center">
			    <button name="submit" id="submit" class="btn btn-primary">Search</button> 
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group" align="center">
			    <button type="submit" name="reset" value="reset" id="reset" class="btn btn-primary">Reset</button> 
			</div>	
		</div>
	</div>
</div>
<?php echo form_close();?>
</div>
</div>
</div>
</div>

</div>

<p></p>
<?php if($this->session->userdata('appraisal_filter_data'))
{?>
<div id="box">
	<h2 class="text-center">Appraisal Search List</h2>
	
</div>
<div class="panel panel-default">
<div class="panel-body table-responsive">
<table class="table table-bordered table-condensed table-stripped">
<thead>
<tr>
<th >Employee ID.</th>
<th >Biometric ID</th>
<th>Name</th>
<th>Gender</th>
<th >Centre</th>
<th >DOJ</th>
<th >DOB</th>
<th >Designation</th>
<th >Qualification</th>
<th>CTC</th>
<th >Basic</th>

<th>HRA</th>
<th>Address</th>
<th>Contact No.</th>
<th>Official E-mail</th>    
<th>E-mail</th>
<th>PF No.</th>
<th>PAN Card No.</th>
<th>Date of leaving</th>
<th>Remarks for leaving</th>
<th>Is Active</th>
<th> </th>
<th>Bank A/C. No.</th>
<th>Branch Name</th>
<th>UAN</th>
</tr>
</thead>
<tbody>

<?php 
$count = $this->uri->segment(2) + 1;
foreach($appraisal_list as $key=>$data){ 
?>
<tr>

<?php
$EMPLOYEE_ID = $this->encrypt->encode($data['EMPLOYEE_ID']);
?>

<td><?php print $data['EMPLOYEE_ID']?></td>
<td><?php print $data['BIOMETRIC_DEVICE_ID'];?></td>
<td><?php print $data['EMP_NAME'];?></td>
<td><?php print $data['GENDER'];?></td>
<td><?php print $data['CENTRE_NAME'];?></td>
<td><?php print $data['DATEOFJOIN'];?></td>
<td><?php print $data['DATEOFBIRTH'];?></td>
<td><?php print $data['DESIGNATION'];?></td>
<td><?php print $data['QUALIFICATION'];?></td>
<td><?php print $data['CTC'];?></td>
<td><?php print $data['BASIC'];?></td>
<td><?php print $data['HRA'];?></td>
<td><?php print $data['ADDRESS1'];?></td>
<td><?php print $data['CONTACTNO'];?></td>
<td><?php print $data['EMP_OFFICIAL_EMAIL'];?></td>
<td><?php print $data['EMP_EMAIL'];?></td>
<td><?php print $data['EMPLOYEEPFNO'];?></td>
<td><?php print $data['PAN_CARD_NO'];?></td>
<td><?php print $data['DATEOFLEAVING'];?></td>
<td><?php print $data['LEAVING_REMARKS'];?></td>
<td><?php print $data['ISACTIVE'];?></td>
<td><a href="<?php echo base_url('hra/performanceAppraisal/').$EMPLOYEE_ID; ?>">Appraisal</a></td>
<td><?php print $data['BANKACCOUNTNO'];?></td>
<td><?php print $data['BRANCH_NAME'];?></td>
<td><?php print $data['UA_NUMBER'];?></td>

</tr>
<?php $count++; } ?>
</tbody>
</table>
<br> </div>
<div class="row">
<div class="col-md-4 pull-right">
<?php echo $pagination; ?>
</div>
</div>
</div>
<?php } ?>
