<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
    <div class="create_batch_form">
        <div id="box">
            <h2>Bank Master</h2></div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align:center; color:#F00;"> </div>
                        <div class="panel-body">
                            <?php echo form_open(); ?>   

                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-4">
                                            <label>You have choose : <span><?php echo " ".$bank_name;?></span></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-4">
                                            <label>Branch Name :</label>
                                            <input name="bank_branch_name" class="form-control" type="text">
                                        </div>
                                        <div class="col-md-4">
                                            <label>IFSC Code :</label>
                                            <input name="ifsc_code" class="form-control" type="text">
                                        </div>
                                        <div class="col-md-4">
                                            <br>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary" name="save_bank_branch_name">Save</button>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" name="reset_branch_name" class="btn btn-primary" style="float:left">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php echo form_close(); ?>
                        </div>
                        </div>
                    </div>
            </div>
    </div>
        <br/>
        <div id="box"><h2></h2></div>
        <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>Sr no.</th>
                                <th>Branch Name</th>
                                <th>Branch Code</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $i = 1;
                            foreach($branch_details as $bank){?>
                            <tr>
                                <td><span class="glyphicon glyphicon-edit edit_branch_name" data-branch-id="<?php echo $bank['BRANCH_ID']; ?>" data-branch-name="<?php echo $bank['BRANCH_NAME']; ?>" data-ifsc-code="<?php echo $bank['IFSC_CODE']; ?>" data-toggle="modal" data-target="#edit_branch_name" title="Edit Branch Name"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="glyphicon glyphicon-trash delete_branch" data-branch-id="<?php echo $bank['BRANCH_ID']; ?>" title="delete"></span></td>
                                <td><?php echo $i; $i++;?></td>
                                <td><?php echo $bank['BRANCH_NAME'];?></td>
                                <td><?php echo $bank['IFSC_CODE'];?></td>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
</div>


<!-- add bank branch name model -->
<div id="edit_branch_name" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Branch Name</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" name="branch_detail_id" id="branch_detail_id">
                    <input type="hidden" name="bank_detail_id" id="bank_detail_id" value="<?php echo $bank_id;?>">
                    <input type="hidden" name="branch_detail_name" id="branch_detail_name">
                    <input type="hidden" name="branch_ifsc_code" id="branch_ifsc_code">
                    <div class="col-md-4">
                        <div class="form-group">    
                            <label>Branch Name :</label>
                            <input class="form-control modal_branch_name" id="modal_branch_name" type="text" name="control_key">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">    
                            <label>IFSC Code :</label>
                            <input class="form-control modal_ifsc_code" id="modal_ifsc_code" type="text" name="control_key">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">    
                            <br/>
                            <button type="submit" id="save_branch_name" class="btn btn-primary" style="float:left">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- add bank branch name model -->