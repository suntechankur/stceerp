<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script type="text/javascript">

$(document).ready(function(){

    var counter = 2;

    $("#addButton").click(function () {

    if(counter>4){
            alert("Only 4 references allow");
            return false;
    }   

    var newTextBoxDiv = $(document.createElement('div'))
         .attr("id", 'TextBoxDiv' + counter);

                
    newTextBoxDiv.after().html("<div class='form-group'><div class='row'><div class='col-md-12'><div class='col-md-4'> <label>Name : </label><input type='textbox' class='form-control' name='ref_name[]' id='textbox1' > </div>"
     +
          "<div class='col-md-4'><label>Relation : </label><input type='textbox' class='form-control' name='ref_relation[]' id='textbox2' ></div>"
           +
          "<div class='col-md-4'><label>Contact No.: </label><input type='textbox' class='form-control' name='ref_contact[]' id='textbox3' ></div>"

          +
          "</div></div></div>");

    newTextBoxDiv.appendTo("#TextBoxesGroup");


    counter++;
     });

     $("#removeButton").click(function () {
    if(counter==1){
          alert("No more references to remove");
          return false;
       }   

    counter--;

        $("#TextBoxDiv" + counter).remove();

     });

    ///// code for EMP Shift Timing///////////////////////////

    var count = 2;

    $("#addShift").click(function () {

    if(count>10){
            alert("Only 10 Shifting Time Allow");
            return false;
    }   

    var newShiftDiv = $(document.createElement('div'))
         .attr("id", 'ShiftDiv' + count).addClass('shiftDiv');

var centres = $('.centre').html();
                
newShiftDiv.after().html('<div class="form-group"><div class="row"><div class="col-md-6"><label>Center:</label><select name="CENTRE_ID[]" class="form-control centre">'+centres+'</select></div><div class="col-md-6"><input id="dynamic_timing" value="1" name="dynamic_timing[]" type="checkbox"><label>dynamic Timing (Timing is Frequently Changed)</label></div></div></div><div class="form-group"><div class="row"><div class="col-md-12"></div><div class="col-md-6"><label for="from_time">From</label> <input class="pref_time form-control" id="from_time" data-format="h:mm a" data-template="hh : mm a" name="from_time[]" value="10:00 am" type="text"></div><div class="col-md-6">   <label for="to_time">To &nbsp;&nbsp;&nbsp;</label>    <input class="pref_time" id="to_time" data-format="h:mm a" data-template="hh : mm a" name="to_time[]" value="06:30 pm" type="text"></div></div></div><div class="form-group"><div class="row"><div class="col-md-12 days_cont"><label>Days: </label> &nbsp;<input type="checkbox" data-day="Mon" value="Mon" name="" class="days"> Mon &nbsp;&nbsp;<input type="checkbox" data-day="Tue" value="Tue" name="days[]" class="days"> Tue &nbsp;&nbsp;<input type="checkbox" data-day="Wed" value="Wed" name="days[]" class="days"> Wed &nbsp;&nbsp;<input type="checkbox" data-day="Thu" value="Thu" name="days[]" class="days"> Thu &nbsp;&nbsp;<input type="checkbox" data-day="Fri" value="Fri" name="days[]" class="days"> Fri &nbsp;&nbsp;<input type="checkbox" data-day="Sat" value="Sat" name="days[]" class="days"> Sat &nbsp;&nbsp;<input type="checkbox" data-day="Sun" value="Sun" name="days[]" class="days"> Sun &nbsp;&nbsp;</div></div></div><hr>');
    newShiftDiv.appendTo("#ShiftGroup");
    prefTime();

    count++;
     });

     $("#removeShift").click(function () {
    if(count==1){
          alert("No more Shifting to remove");
          return false;
       }   

    count--;

        $("#ShiftDiv" + count).remove();

     });

     $('#ShiftGroup').on('change','.centre',function(){
        var centre = $(this).val();
       if(!$('#ShiftGroup').hasClass('day_set')){
           $(this).closest('.shiftDiv').find('.days').each(function(){
                    var setDays = $(this).data('day')+'-'+centre;
                    $(this).val(setDays);
           });
           $(this).closest('.shiftDiv').addClass('day_set');
       }
        
     });

  });
</script>

<div class="gapping"></div>
<div class="create_batch_form">
    <div id="box">
        <h2>Add Employee</h2>
        
        </div>
    <div class="row">
    
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- start message area -->

            <?php if($this->session->flashdata('danger')) { ?>
                        <div class="alert alert-danger">
              <strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
              <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('newBatchId')) { ?>
                        <div class="alert alert-success">
              <?php echo "New Batch ID Generated: " . $this->session->flashdata('newBatchId'); ?>
            </div>
            <?php } ?>
                             
                        
            <?php if($this->session->flashdata('failed')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
            </div>
            <?php } ?>
  
            <?php if($this->session->flashdata('info1')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('warning')) { ?>
                        <div class="alert alert-warning">
              <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
            </div>
            <?php } ?>

 <!-- End message area --> 

                </div>
                <div class="panel-body">
                   <?php 
                    $attr = array('role'=>'form');
                    echo form_open('',$attr); ?>
<!--                    <form role="form" data-toggle="validator" method="post" action="lib/save.php" novalidate="true">-->
                        <div class="col-md-6">
                        
                            <label for=""><h4>PERSONAL DETAILS</h4></label>
                        
                            <div class="form-group">
                                <label>First Name: <span class="text-danger">*</span></label>
                                <input class="form-control" name="EMP_FNAME" value="" id="EMP_FNAM" type="text" required=""> 
                                <span class="text-danger"><?php echo form_error('EMP_FNAME'); ?></span>
                                </div>
                            <div class="form-group">
                                <label>Middle Name:</label>
                                <input class="form-control" name="EMP_MIDDLENAME" value="" id="EMP_MIDDLENAME" type="text">
                                <span class="text-danger"><?php echo form_error('EMP_MIDDLENAME'); ?></span>
                                 </div>

                            <div class="form-group">
                                <label>Last Name: <span class="text-danger">*</span></label>
                                <input class="form-control" name="EMP_LASTNAME" value="" id="EMP_LASTNAME" type="text"> 
                                <span class="text-danger"><?php echo form_error('EMP_LASTNAME'); ?></span>
                                </div>
                            <div class="form-group">
                                 <div class="row">
                                    
                                        <div class="col-lg-12">
                                            <label>E-mail: <span class="text-danger"></span></label>
                                            <input class="form-control" name="EMP_EMAIL" id="EMP_EMAIL" value="" type="text"> 
                                            <span class="text-danger"><?php echo form_error('EMP_EMAIL'); ?></span>
                                            </div>
                                        
                                   
                                </div>
                            </div>

<div class="form-group">
 <label>Gender: <span class="text-danger">*</span></label>
 <div class="row">

                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input id="GENDER" name="GENDER" value="F" type="radio">Female
                                </label>
                            </div>
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input id="GENDER" name="GENDER" value="M" type="radio">Male
                                </label>
                            </div>
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input id="GENDER" name="GENDER" value="U" type="radio">Unknown
                                </label>
                            </div>
                        </div>
</div>

<div class="form-group">
                            <div class="row">
                                    
                                        <div class="col-lg-6">
                                            <label>Address1: <span class="text-danger">*</span></label>
                                            <input name="ADDRESS1" class="form-control">
                                                 
                                            <span class="text-danger"><?php echo form_error('ADDRESS1'); ?></span>
                                        </div>
                                    
                                        <div class="col-lg-6">
                                            <label>Address2: <span class="text-danger"></span></label>
                                            <input name="ADDRESS2" class="form-control">
                                            <span class="text-danger"><?php echo form_error('ADDRESS2'); ?></span>
                                        </div>
                                    
                                    </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                        <div class="col-lg-6">
                                            <label>City: <span class="text-danger">*</span></label>
                                            <select name="CITY" class="form-control source_master">
                                                <option value="">Please Select City</option>
                                                <?php foreach($cities as $city){ ?>
                                                <option value="<?php echo $city['CITY_ID'] ?>"><?php echo $city['CITY_NAME'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('CITY'); ?></span>
                                        </div>
                                    
                                        <div class="col-lg-6">
                                            <label>Zip Code:</label>
                                            
                                                <input name="ZIP" class="form-control">
                                            
                                            <span class="text-danger"><?php echo form_error('ZIP'); ?></span>
                                        </div>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                        <div class="col-lg-6">
                                            <label>Suburb: <span class="text-danger">*</span></label>
                                            <select name="SUBURB" class="form-control">
                                                <option value="">Please Select Suburb</option>

                                                <option value="Central">Central</option>

                                                <option value="Western">Western</option>

                                                <option value="Horbour">Horbour</option>
                                            
                                            </select>
                                            <span class="text-danger"><?php echo form_error('SUBURB'); ?></span>
                                        </div>
                                    
                                        <div class="col-lg-6">
                                            <label>State: <span class="text-danger">*</span></label>
                                            <select name="STATE" class="form-control">
                                                <option value="">Please Select State</option>
                                                <?php foreach($states as $state){ ?>
                                                <option value="<?php echo $state['STATE_ID'] ?>"><?php echo $state['STATE_NAME'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('STATE'); ?></span>
                                        </div>
                                            
                                    </div>
                                </div>
                            
                            <div class="form-group">
                                <div class="row">
                                        
                                    
                                        

                                        <div class="col-md-6">
                                            <label>Contact No.1: <span class="text-danger">*</span></label>
                                                        <input name="EMP_MOBILENO" class="form-control">
                                            
                                            <span class="text-danger"><?php echo form_error('EMP_MOBILENO'); ?></span>
                                        </div>

                                        <div class="col-md-6">
                                           
                                                        <label>Contact No.2:</label>
                                                        <input name="EMP_TELEPHONE" class="form-control">
                                            
                                            <span class="text-danger"><?php echo form_error('EMP_TELEPHONE'); ?></span>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">

                                    <div class="col-md-6">
                                            
                                        <label>Pan Card No.: <span class="text-danger"></span></label>
                                            <input class="form-control" name="PAN_CARD_NO" id="PAN_CARD_NO" value="" type="text"> 
                                            <span class="text-danger"><?php echo form_error('PAN_CARD_NO'); ?></span>
                                    </div>

                                        
                                    
                                        <div class="col-md-6">
                                           
                                                        <label>Date of birth(mm/dd/yyyy): <span class="text-danger">*</span></label>
                                                        <input name="DATEOFBIRTH" class="form-control enquiry_date" required="">
                                            
                                            <span class="text-danger"><?php echo form_error('DATEOFBIRTH'); ?></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                    <div class="col-lg-6">
                                            <label>Current Qualification: <span class="text-danger">*</span></label>
                                            <select name="QUALIFICATION" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1">Xth Pass </option>
                                                <option value="2">XIIth Pass </option>
                                                <option value="3">Under Graduate </option>
                                                <option value="4">Graduate </option>
                                                <option value="5">Post Graduate </option>
                                                <option value="6">Other </option>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('QUALIFICATION'); ?></span>
                                        </div>

                                                                           
                                        <div class="col-md-6">
                                           
                                            <label>Stream: <span class="text-danger">*</span></label>
                                            <select name="STREAM" class="form-control">
                                                <option value="">Please Select</option>
                                                <?php foreach($streams as $stream){ ?>
                                                <option value="<?php echo $stream['CONTROLFILE_ID'] ?>"><?php echo $stream['CONTROLFILE_VALUE'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('STREAM'); ?></span>
                                        </div>
                                    </div>
                                </div>



                            <div id='TextBoxesGroup'>
            <div id='TextBoxDiv'>
        <div class="form-group"> 
        <div class='row'>
        <div class="col-md-12">
        
            <div class='select-box'><br>
            <label>References : </label> &nbsp;
               <span class="glyphicon glyphicon-plus text-info handCursor" id='addButton'></span>
               <span class="glyphicon glyphicon-minus text-danger handCursor" id='removeButton'></span>
            </div>

        </div>
        </div>
        <div class="form-group"> 
        <div class="row">
        
        
            <div class='col-md-12'>
            
            
                <div class='col-md-4'>
                    <label>Name : </label><input type='textbox' class='form-control' name='ref_name[]' id='textbox1' >
                </div>

                <div class='col-md-4'>
                    <label>Relation : </label><input type='textbox' class='form-control' name='ref_relation[]' id='textbox2' >
                </div>

                <div class='col-md-4'>
                    <label>Contact No.: </label><input type='textbox' class='form-control' name='ref_contact[]' id='textbox3' >
                </div>
            
                </div>
                </div>
                </div>
            </div>
            </div>
            </div>

            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Computer Skills: <span class="text-danger">*</span></label><br />
                                        <select class="form-control course_interested" name="SKILL[]" id="SKILL" multiple>
                                        <?php 
                                            foreach($computer_skill as $skill){ ?>
                                            <option value="<?php echo $skill['EMPLOYEE_SKILL_ID']; ?>"><?php echo $skill['EMPLOYEE_SKILL']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('SKILL[]'); ?></span>
                                    </div>
                                    <div class="row">
                                   <div class="col-md-12">
                                       <ul class="list-group courses_seld"></ul>
                                   </div>
                                </div>
                                </div>
                                <div class="col-md-6">
                                        <label class="form-check-inline">
                                              &nbsp;&nbsp; </label>


                                    
                                </div>
                            </div>

    </div>
                <div class="col-md-6">
                    <label for=""><h4>COMPANY DETAILS</h4></label>
                    

                    <div class="form-group">
                        <div class="row">
                             <div class="col-md-6">
                                
                                    <label>Date of joining(dd/mm/yyyy): <span class="text-danger">*</span></label>
                                    <input class="form-control enquiry_date" name="DATEOFJOIN" type="text"> 
                                    <span class="text-danger"><?php echo form_error('DATEOFJOIN'); ?></span>
                                </div>
                            
                            <div class="col-md-6">

                                   <label>Current Designation: <span class="text-danger">*</span></label>
                                        <select name="CURRENT_DESIGNATION" class="form-control" required="">
                                            <option value="">Please Select</option>
                                            <?php
                                            
                                             foreach($designations as $designation){ 
                                            
                                                ?>
                                            <option value="<?php echo $designation['CONTROLFILE_ID'] ?>"><?php echo $designation['CONTROLFILE_VALUE'] ?></option>
                                            <?php } ?>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('CURRENT_DESIGNATION'); ?></span>
                                
                            </div>
                        </div>
                    </div>


                        <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Current Salary: <span class="text-danger">*</span></label>
                                                    <input name="CURRENT_SALARY" class="form-control" required="">
                                        
                                        <span class="text-danger"><?php echo form_error('CURRENT_SALARY'); ?></span>
                                    </div>
                                
                                    <div class="col-md-6">
                                       
                                                    <label>Bank A/C No.:</label>
                                                    <input name="BANKACCOUNTNO" class="form-control">
                                        
                                        <span class="text-danger"><?php echo form_error('BANKACCOUNTNO'); ?></span>
                                    </div>
                                </div>
                            </div>


                        <div class="form-group">
                            <div class="row">
                             <div class="col-md-6">
                                
                                    <label>Bank & Branch Name:</label>
                                    

                                    <select name="BRANCH_ID" class="form-control">
                                            <option value="">Please Select</option>
                                            <?php foreach($bankBranches as $bankBranche){ ?>
                                            <option value="<?php echo $bankBranche['BRANCH_ID'] ?>"><?php echo "SVC(".$bankBranche['BRANCH_NAME'].")" ?></option>
                                            <?php } ?>
                                        </select>
                                    <span class="text-danger"><?php echo form_error('BRANCH_ID'); ?></span>
                                </div>
                            
                            <div class="col-md-6">
                               
                                   <label>P/F No.:</label>
                                       <input class="form-control" name="EMPLOYEEPFNO" type="text"> 
                                        <span class="text-danger"><?php echo form_error('EMPLOYEEPFNO'); ?></span>
                                
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                            <div class="row">
                            <div class="col-md-6">
                                    
                                <label>Biometric Device ID:</label>
                                <input class="form-control" name="BIOMETRIC_DEVICE_ID" type="text">
                                <span class="text-danger"><?php echo form_error('BIOMETRIC_DEVICE_ID'); ?></span>
                            </div>
                            
                            <div class="col-md-6">
                               
                                   <label>Official E-mail:</label>
                                       <input class="form-control" name="EMP_OFFICIAL_EMAIL" type="text"> 
                                        <span class="text-danger"><?php echo form_error('EMP_OFFICIAL_EMAIL'); ?></span>
                                
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                     <label>Others:</label>
                     <div class="row">

                        <div class="col-sm-4">
                            
                                <input name="TA_GIVEN" type="checkbox" value="1"> Tea Allowance Given
                            
                        </div>
                        <div class="col-sm-5">
                            
                                <input name="PF_APPLICABLE" type="checkbox" value="1"> PF Deduction Applicable
                            
                        </div>
                        <div class="col-sm-3">
                            
                                <input name="ISACTIVE" type="checkbox" value="1" checked="checked"> Is Active
                            
                        </div>
                        </div>
                    </div>

                    
                    <div class="form-group">
                            <div class="row">
                            <div class="col-md-6">
                                    
                                <label>Department:</label>
                                
                                    <select name="DEPARTMENT_ID" class="form-control">
                                            <option value="">Please Select</option>
                                            <?php foreach($departments as $department){ ?>
                                            <option value="<?php echo $department['DepartmentId'] ?>"><?php echo $department['DepartmentName'] ?></option>
                                            <?php } ?>
                                    </select>

                                <?php echo form_error('DEPARTMENT_ID'); ?></span>
                            </div>
                            
                            <div class="col-md-6">
                               
                               <label>UA Number:</label>
                                   <input class="form-control" name="UA_NUMBER" type="text"> 
                                    <span class="text-danger">
                                        <?php echo form_error('UA_NUMBER'); ?>
                                    </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                            <div class="row">
                            <div class="col-md-6">
                                    
                                <label>Commitment:</label>
                                
                                <select name="COMMITMENTWITHEMPLOYEE" class="form-control">
                                        <option value="">Please Select</option>
                                        <option value="Certificates">Certificates</option>
                                        <option value="1 year Bond">1 year Bond</option>
                                        <option value="No Commitment">No Commitment</option>
                                        <option value="Others">Others</option>
                                </select>

                                <span class="text-danger"><?php echo form_error('COMMITMENTWITHEMPLOYEE'); ?></span>
                            </div>
                            
                            <div class="col-md-6">
                               
                                   <label>Basic Salary:</label>
                                       <input class="form-control" name="BASIC" type="text"> 
                                        <span class="text-danger"><?php echo form_error('BASIC'); ?></span>
                                
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                            <div class="row">
                            <div class="col-md-6">
                                    
                                <label>HRA:</label>
                                <input class="form-control" name="HRA" type="text">
                                <span class="text-danger"><?php echo form_error('HRA'); ?></span>
                            </div>
                            
                            <div class="col-md-6">
                               
                                   <label>CTC:</label>
                                       <input class="form-control" name="CTC" type="text"> 
                                        <span class="text-danger"><?php echo form_error('CTC'); ?></span>
                                
                            </div>
                        </div>
                    </div>

                    

                    
                    <div class="info"></div>

                    <label for=""> Employee Shift Timing </label> 
                    <span class="glyphicon glyphicon-plus text-info handCursor" id='addShift'></span> 
               <span class="glyphicon glyphicon-minus text-danger handCursor" id='removeShift'></span>

                    <div id='ShiftGroup' class="ShiftGroup">
                    <div id='ShiftDiv' class="shiftDiv">
                    <hr>
                    <div class="form-group">
                        <div class="row">
                        
                        

                        <div class="col-md-6">
                        <label>Center:</label>
                                
                                    <select name="CENTRE_ID[]" class="form-control centre">
                                            <option value="">Please Select</option>
                                            <?php foreach($centres as $centre){ ?>
                                            <option value="<?php echo $centre['CENTRE_ID'] ?>"><?php echo $centre['CENTRE_NAME'] ?></option>
                                            <?php } ?>
                                    </select>

                                <span class="text-danger"><?php echo form_error('CENTRE_ID'); ?></span>
                        </div>

                        <div class="col-md-6">
                            
                            <input id="dynamic_timing" value="1" name="dynamic_timing[]" type="checkbox">    
                            <label>Dynamic Timing (Timing is Frequently Changed)</label>       
                        </div>
                        </div> 
                    </div>
                    


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                
                            </div>
                            <div class="col-md-6">
                            <!-- <label for="">Preferred Timing</label> -->
                                <label for="from_time">From</label>
                                    <input class="pref_time form-control" id="from_time" data-format="h:mm a" data-template="hh : mm a" name="from_time[]" value="10:00 am" type="text">
                            </div>
                            <div class="col-md-6">
                                    <label for="to_time">To &nbsp;&nbsp;&nbsp;</label>
                                    <input class="pref_time" id="to_time" data-format="h:mm a" data-template="hh : mm a" name="to_time[]" value="06:30 pm" type="text">
                            </div>
                        </div>
                    </div>

                    

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12 days_cont">        
                            <label>Days: </label> &nbsp;
                            <input type="checkbox" data-day="Mon" value="Mon" name="days[]" class="days"> Mon &nbsp;&nbsp;
                            <input type="checkbox" data-day="Tue" value="Tue" name="days[]" class="days"> Tue &nbsp;&nbsp;
                            <input type="checkbox" data-day="Wed" value="Wed" name="days[]" class="days"> Wed &nbsp;&nbsp; 
                            <input type="checkbox" data-day="Thu" value="Thu" name="days[]" class="days"> Thu &nbsp;&nbsp;
                            <input type="checkbox" data-day="Fri" value="Fri" name="days[]" class="days"> Fri &nbsp;&nbsp;
                            <input type="checkbox" data-day="Sat" value="Sat" name="days[]" class="days"> Sat &nbsp;&nbsp;
                            <input type="checkbox" data-day="Sun" value="Sun" name="days[]" class="days"> Sun &nbsp;&nbsp;
                            
                        </div>
                    </div>
                </div>
                    <hr>
                    </div>
                    </div>



                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                   <label>Remark:</label>
                                       <textarea class="form-control" name="REMARKS"></textarea> 
                                        <span class="text-danger"><?php echo form_error('REMARKS'); ?></span>
                                
                            </div>
                        </div>
                    </div>






                        <br>
                        <button type="submit" class="btn btn-primary">Save</button> &nbsp;
                        <button type="reset" style="float:none">Reset</button>
                    </div>
                </div>

              


        </div>


            </div>

            <?php echo form_close(); ?>
        </div>
    </div>
    
</div>
