<div class="gapping"></div>
<div class="create_batch_form">
<div id="box">
<h2>Add Employee</h2>

</div>
<div class="row">

<div class="col-sm-12">
<div class="panel panel-default">
<div class="panel-heading">
<!-- start message area -->

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('newBatchId')) { ?>
<div class="alert alert-success">
<?php echo "New Batch ID Generated: " . $this->session->flashdata('newBatchId'); ?>
</div>
<?php } ?>


<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info1')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
</div>
<?php } ?>

<?php if($this->session->flashdata('warning')) { ?>
<div class="alert alert-warning">
<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php } ?>

<!-- End message area -->

</div>
<div class="panel-body">
<?php
$attr = array('role'=>'form');
echo form_open('',$attr); ?>
<!--                    <form role="form" data-toggle="validator" method="post" action="lib/save.php" novalidate="true">-->
<div class="col-md-6">

<label for=""><h4>PERSONAL DETAILS</h4></label>

<div class="form-group">
<label>First Name: <span class="text-danger">*</span></label>
<input class="form-control" name="EMP_FNAME" value="<?php echo $this->input->post('EMP_FNAME');?>" id="EMP_FNAM" type="text" required="">
<span class="text-danger"><?php echo form_error('EMP_FNAME'); ?></span>
</div>
<div class="form-group">
<label>Middle Name:</label>
<input class="form-control" name="EMP_MIDDLENAME" value="<?php echo $this->input->post('EMP_MIDDLENAME');?>" id="EMP_MIDDLENAME" type="text">

</div>

<div class="form-group">
<label>Last Name: <span class="text-danger">*</span></label>
<input class="form-control" name="EMP_LASTNAME" value="<?php echo $this->input->post('EMP_LASTNAME');?>" id="EMP_LASTNAME" type="text" required="">
<span class="text-danger"><?php echo form_error('EMP_LASTNAME'); ?></span>
</div>
<div class="form-group">
<div class="row">

<div class="col-lg-12">
    <label>E-mail: <span class="text-danger"></span></label>
    <input class="form-control" name="EMP_EMAIL" id="EMP_EMAIL" value="<?php echo $this->input->post('EMP_EMAIL');?>" type="email">
    <span class="text-danger"><?php echo form_error('EMP_EMAIL'); ?></span>
    </div>


</div>
</div>

<div class="form-group">
<label>Gender: <span class="text-danger">*</span></label>
<div class="row">

<div class="col-sm-4">
<label class="radio-inline">
<input id="GENDER" name="GENDER" value="F"<?php echo ($this->input->post('GENDER')=="F") ? "checked" : '';?> type="radio" required="">Female
</label>
</div>
<div class="col-sm-4">
<label class="radio-inline">
<input id="GENDER" name="GENDER" value="M"<?php echo ($this->input->post('GENDER')=="M") ? "checked" : '';?> type="radio" required="">Male
</label>
</div>
<div class="col-sm-4">

</div>
<span class="text-danger"><?php echo form_error('GENDER'); ?></span>
</div>
</div>

<div class="form-group">
<div class="row">

<div class="col-lg-6">
    <label>Address1: <span class="text-danger">*</span></label>
    <input name="ADDRESS1" class="form-control" value="<?php echo $this->input->post('ADDRESS1');?>" required="">

    <span class="text-danger"><?php echo form_error('ADDRESS1'); ?></span>
</div>

<div class="col-lg-6">
    <label>Address2: <span class="text-danger"></span></label>
    <input name="ADDRESS2" value="<?php echo $this->input->post('ADDRESS2');?>" class="form-control">

</div>

</div>
</div>
<div class="form-group">
<div class="row">
<div class="col-lg-6">
    <label>City: <span class="text-danger">*</span></label>
    <select name="CITY" class="form-control source_master" required="">
        <option value="">Please Select City</option>
        <?php foreach($cities as $city){ ?>
        <option value="<?php echo $city['CITY_NAME'] ?>"<?php echo ($this->input->post('CITY')) ? "selected" : '';?>><?php echo $city['CITY_NAME'] ?></option>
        <?php } ?>
    </select>
    <span class="text-danger"><?php echo form_error('CITY'); ?></span>
</div>

<div class="col-lg-6">
    <label>Zip Code:</label>

<input type="text" max='6' name="ZIP" value="<?php echo $this->input->post('ZIP');?>" class="form-control">

</div>

</div>
</div>
<div class="form-group">
<div class="row">
<div class="col-lg-6">
    <label>Suburb: <span class="text-danger">*</span></label>
    <select name="SUBURB" class="form-control" required="">
        <option selected disabled>Please Select Suburb</option>

        <option value="Central"<?php echo ($this->input->post('SUBURB')=="Central") ? "selected" : '';?>>Central</option>

        <option value="Western"<?php echo ($this->input->post('SUBURB')=="Western") ? "selected" : '';?>>Western</option>

        <option value="Horbour"<?php echo ($this->input->post('SUBURB')=="Horbour") ? "selected" : '';?>>Horbour</option>

    </select>
    <span class="text-danger"><?php echo form_error('SUBURB'); ?></span>
</div>

<div class="col-lg-6">
    <label>State: <span class="text-danger">*</span></label>
    <select name="STATE" class="form-control" required="">
        <option selected disabled>Please Select State</option>
        <option value="Maharashtra">Maharashtra</option>
    </select>
    <span class="text-danger"><?php echo form_error('STATE'); ?></span>
</div>

</div>
</div>

<div class="form-group">
<div class="row">




<div class="col-md-6">
    <label>Contact No.1: <span class="text-danger">*</span></label>
    <input type="text" pattern="[1-9]{1}[0-9]{9}" max='10' name="EMP_MOBILENO"  value="<?php echo $this->input->post('EMP_MOBILENO');?>" class="form-control" required="">

    <span class="text-danger"><?php echo form_error('EMP_MOBILENO'); ?></span>
</div>

<div class="col-md-6">
  <label>Contact No.2:</label>
  <input type="tel" name="EMP_TELEPHONE" value="<?php echo $this->input->post('EMP_TELEPHONE');?>" class="form-control">
  <span class="text-danger"><?php echo form_error('EMP_TELEPHONE'); ?></span>
</div>
</div>
</div>

<div class="form-group">
  <div class="row">
    <div class="col-md-6">
      <label>Pan Card No.: <span class="text-danger"></span></label>
      <input class="form-control" name="PAN_CARD_NO" value="<?php echo $this->input->post('PAN_CARD_NO');?>" id="PAN_CARD_NO" type="text">
    </div>
    <div class="col-md-6">
      <label>Date of birth(mm/dd/yyyy): <span class="text-danger">*</span></label>
      <input name="DATEOFBIRTH" value="<?php echo $this->input->post('DATEOFBIRTH');?>" class="form-control enquiry_date" required="">
      <span class="text-danger"><?php echo form_error('DATEOFBIRTH'); ?></span>
    </div>
  </div>
</div>

<div class="form-group">
  <div class="row">
    <div class="col-lg-6">
      <label>Current Qualification: <span class="text-danger">*</span></label>
      <select name="QUALIFICATION" class="form-control" required="">
        <option selected disabled>Please Select</option>
        <option value="Xth Pass"<?php echo ($this->input->post('QUALIFICATION')=="Xth Pass") ? "selected" : '';?>>Xth Pass </option>
        <option value="XIIth Pass"<?php echo ($this->input->post('QUALIFICATION')=="XIIth Pass") ? "selected" : '';?>>XIIth Pass </option>
        <option value="Under Graduate"<?php echo ($this->input->post('QUALIFICATION')=="Under Graduate") ? "selected" : '';?>>Under Graduate </option>
        <option value="Graduate"<?php echo ($this->input->post('QUALIFICATION')=="Graduate") ? "selected" : '';?>>Graduate </option>
        <option value="Post Graduate"<?php echo ($this->input->post('QUALIFICATION')=="Post Graduate") ? "selected" : '';?>>Post Graduate </option>
        <option value="Other"<?php echo ($this->input->post('QUALIFICATION')=="Other") ? "selected" : '';?>>Other </option>
      </select>
      <span class="text-danger"><?php echo form_error('QUALIFICATION'); ?></span>
    </div>


    <div class="col-md-6">
      <label>Specialization: <span class="text-danger">*</span></label>
      <select name="STREAM" class="form-control" required="">
        <option selected disabled>Please Select</option>
        <?php foreach($streams as $stream){ ?>
        <option value="<?php echo $stream['CONTROLFILE_ID'] ?>"<?php echo ($this->input->post('STREAM')) ? "selected" : '';?>><?php echo $stream['CONTROLFILE_VALUE'] ?></option>
        <?php } ?>
      </select>
      <span class="text-danger"><?php echo form_error('STREAM'); ?></span>
    </div>
  </div>
</div>



<div id='TextBoxesGroup'>
  <div id='TextBoxDiv'>
    <div class="form-group">
      <div class='row'>
        <div class="col-md-12">
          <div class='select-box'><br>
            <label>References : </label> &nbsp;
            <span class="glyphicon glyphicon-plus text-info handCursor" id='addButton'></span>
            <span class="glyphicon glyphicon-minus text-danger handCursor" id='removeButton'></span>
          </div>
        </div>
      </div>

    <div class="form-group">
      <div class="row">
        <div class='col-md-12'>
          <div class='col-md-4'>
            <label>Name : <span class="text-danger">*</span></label>
            <input type='textbox' class='form-control' name='REFERENCE_NAME1' value="<?php echo $this->input->post('REFERENCE_NAME1'); ?>" required>
          </div>
          <div class='col-md-4'>
            <label>Relation : <span class="text-danger">*</span></label>
            <input type='textbox' class='form-control' name='REFERENCE_RELATION1' value="<?php echo $this->input->post('REFERENCE_RELATION1'); ?>" required>
          </div>
          <div class='col-md-4'>
            <label>Contact No.: <span class="text-danger">*</span></label>
            <input type='textbox' class='form-control' name='REFERENCE_CONTACT_NO1' value="<?php echo $this->input->post('REFERENCE_CONTACT_NO1'); ?>" required>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group">
      <div class="row">
        <div class='col-md-12'>
          <div class='col-md-4'>
            <label>Name : </label>
            <input type='textbox' class='form-control' name='REFERENCE_NAME2' value="<?php echo $this->input->post('REFERENCE_NAME2'); ?>" required>
          </div>
          <div class='col-md-4'>
            <label>Relation : </label>
            <input type='textbox' class='form-control' name='REFERENCE_RELATION2' value="<?php echo $this->input->post('REFERENCE_RELATION2'); ?>" required>
          </div>
          <div class='col-md-4'>
            <label>Contact No.: </label>
            <input type='textbox' class='form-control' name='REFERENCE_RELATION2' value="<?php echo $this->input->post('REFERENCE_RELATION2'); ?>" required>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group">
      <div class="row">
        <div class='col-md-12'>
          <div class='col-md-4'>
            <label>Name : </label>
            <input type='textbox' class='form-control' name='REFERENCE_NAME3' value="<?php echo $this->input->post('REFERENCE_NAME3'); ?>" required>
          </div>
          <div class='col-md-4'>
            <label>Relation : </label>
            <input type='textbox' class='form-control' name='REFERENCE_RELATION3' value="<?php echo $this->input->post('REFERENCE_RELATION3'); ?>" required>
          </div>
          <div class='col-md-4'>
            <label>Contact No.: </label>
            <input type='textbox' class='form-control' name='REFERENCE_RELATION3' value="<?php echo $this->input->post('REFERENCE_RELATION3'); ?>" required>
          </div>
        </div>
      </div>
    </div>

    </div>
  </div>
</div>

  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>Computer Skills: <span class="text-danger">*</span></label><br />
        <select class="form-control course_interested" name="SKILL[]" id="SKILL" multiple required="">
        <?php
            foreach($courses as $skill){

                $isSelected = '';
                if ($compSkills) {
                    $isSelected = in_array($skill['COURSE_NAME'],$compSkills) ? "selected='selected'" : "";
                 }
                ?>
            <option value="<?php echo $skill['COURSE_NAME']; ?>" <?php echo $isSelected;?>><?php echo $skill['COURSE_NAME']; ?></option>
            <?php } ?>
        </select>
        <span class="text-danger"><?php echo form_error('SKILL[]'); ?></span>
      </div>
      <div class="row">
        <div class="col-md-12">
        <ul class="list-group courses_seld"></ul>
        </div>
      </div>
    </div>
    <div class="col-md-6">
    <label class="form-check-inline">
          &nbsp;&nbsp; </label>
    </div>
  </div>

</div>
<div class="col-md-6">
<label for=""><h4>COMPANY DETAILS</h4></label>


<div class="form-group">
<div class="row">
<div class="col-md-6">

<label>Date of joining(dd/mm/yyyy): <span class="text-danger">*</span></label>
<input class="form-control enquiry_date" value="<?php echo $this->input->post('DATEOFJOIN');?>" name="DATEOFJOIN" type="text" required="">
<span class="text-danger"><?php echo form_error('DATEOFJOIN'); ?></span>
</div>

<div class="col-md-6">

<label>Current Designation: <span class="text-danger">*</span></label>
<select name="CURRENT_DESIGNATION" class="form-control" required="">
    <option value="">Please Select</option>
    <?php

     foreach($designations as $designation){

        ?>
    <option value="<?php echo $designation['CONTROLFILE_ID'] ?>"<?php echo ($this->input->post('CURRENT_DESIGNATION')) ? "selected" : '';?>><?php echo $designation['CONTROLFILE_VALUE'] ?></option>
    <?php } ?>
</select>
<span class="text-danger"><?php echo form_error('CURRENT_DESIGNATION'); ?></span>

</div>
</div>
</div>


<div class="form-group">
<div class="row">
<div class="col-md-6">
<label>Current Salary: <span class="text-danger">*</span></label>
            <input name="CURRENT_SALARY" value="<?php echo $this->input->post('CURRENT_SALARY');?>" class="form-control" required="">

<span class="text-danger"><?php echo form_error('CURRENT_SALARY'); ?></span>
</div>

<div class="col-md-6">

            <label>Bank A/C No.:</label>
            <input name="BANKACCOUNTNO" value="<?php echo $this->input->post('BANKACCOUNTNO');?>" class="form-control">
</div>
</div>
</div>


<div class="form-group">
<div class="row">
<div class="col-md-6">

<label>Bank & Branch Name:</label>


<select name="BRANCH_ID" class="form-control">
    <option value="">Please Select</option>
    <?php foreach($bankBranches as $bankBranche){ ?>
    <option value="<?php echo $bankBranche['BRANCH_ID'] ?>"<?php echo ($this->input->post('BRANCH_ID')) ? "selected" : '';?>><?php echo "SVC(".$bankBranche['BRANCH_NAME'].")" ?></option>
    <?php } ?>
</select>

</div>

<div class="col-md-6">

<label>P/F No.:</label>
<input class="form-control" name="EMPLOYEEPFNO" value="<?php echo $this->input->post('EMPLOYEEPFNO');?>" type="text">

</div>
</div>
</div>

<div class="form-group">
<div class="row">
<div class="col-md-6">

<label>Biometric Device ID:</label>
<input class="form-control" name="BIOMETRIC_DEVICE_ID" value="<?php echo $this->input->post('BIOMETRIC_DEVICE_ID');?>" type="text">

</div>

<div class="col-md-6">

<label>Official E-mail:</label>
<input class="form-control" name="EMP_OFFICIAL_EMAIL" value="<?php echo $this->input->post('EMP_OFFICIAL_EMAIL');?>" type="text">
</div>
</div>
</div>

<div class="form-group">
<label>Others:</label>
<div class="row">

<div class="col-sm-4">

<input name="TA_GIVEN" type="checkbox" value="1"<?php echo ($this->input->post('TA_GIVEN')=="1") ? "checked" : '';?>> Tea Allowance Given

</div>
<div class="col-sm-5">

<input name="PF_APPLICABLE" type="checkbox" value="1"<?php echo ($this->input->post('PF_APPLICABLE')=="1") ? "checked" : '';?>> PF Deduction Applicable

</div>
<div class="col-sm-3">

<input name="ISACTIVE" type="checkbox" value="1" checked="checked"> Is Active

</div>
</div>
</div>


<div class="form-group">
<div class="row">
<div class="col-md-6">

<label>Department:</label>

<select name="DEPARTMENT_ID" class="form-control">
    <option value="">Please Select</option>
    <?php foreach($departments as $department){ ?>
    <option value="<?php echo $department['DEPARTMENT_ID'] ?>"<?php echo ($this->input->post('DEPARTMENT_ID')) ? "selected" : '';?>><?php echo $department['DEPARTMENT_NAME'] ?></option>
    <?php } ?>
</select>

</div>

<div class="col-md-6">

<label>UA Number:</label>
<input class="form-control" name="UA_NUMBER" value="<?php echo $this->input->post('UA_NUMBER');?>" type="text">

</div>
</div>
</div>

<div class="form-group">
<div class="row">
<div class="col-md-6">

<label>Commitment:</label>

<select name="COMMITMENTWITHEMPLOYEE" class="form-control">
<option value="">Please Select</option>
<option value="Certificates"<?php echo ($this->input->post('COMMITMENTWITHEMPLOYEE')=="Certificates") ? "selected" : '';?>>Certificates</option>
<option value="1 year Bond"<?php echo ($this->input->post('COMMITMENTWITHEMPLOYEE')=="1 year Bond") ? "selected" : '';?>>1 year Bond</option>
<option value="No Commitment"<?php echo ($this->input->post('COMMITMENTWITHEMPLOYEE')=="No Commitment") ? "selected" : '';?>>No Commitment</option>
<option value="Others"<?php echo ($this->input->post('COMMITMENTWITHEMPLOYEE')=="Others") ? "selected" : '';?>>Others</option>
</select>

</div>

<div class="col-md-6">

<label>Basic Salary:</label>
<input class="form-control" name="BASIC" value="<?php echo $this->input->post('BASIC');?>" type="text">

</div>
</div>
</div>
<div class="form-group">
<div class="row">
<div class="col-md-6">

<label>HRA:</label>
<input class="form-control" name="HRA" value="<?php echo $this->input->post('HRA');?>" type="text">

</div>

<div class="col-md-6">

<label>CTC:<span class="text-danger">*</span></label>
<input class="form-control" name="CTC" value="<?php echo $this->input->post('CTC');?>" type="text" required="">
<span class="text-danger"><?php echo form_error('CTC'); ?></span>

</div>
</div>
</div>




<div class="info"></div>

<label for=""> Employee Shift Timing </label>
<span class="glyphicon glyphicon-plus text-info handCursor" id='addShift'></span>
<span class="glyphicon glyphicon-minus text-danger handCursor" id='removeShift'></span>

<div id='ShiftGroup' class="ShiftGroup">
<div id='ShiftDiv' class="shiftDiv">
<hr>
<?php
$centresId = $this->input->post('CENTRE_ID');
$dynamicTime = $this->input->post('dynamic_timing');
$centreCount = 1;
if ($centresId) {
$centreCount = count($centresId);
}
for ($i=0; $i <$centreCount ; $i++) { ?>
<div class="form-group">
<div class="row">



<div class="col-md-6">

<label>Centre:</label>

<select name="CENTRE_ID[]" class="form-control centre">
    <option value="">Please Select</option>
    <?php foreach($centres as $centre){ ?>
    <option value="<?php echo $centre['CENTRE_ID'] ?>" <?php echo ($centre['CENTRE_ID'] == $centresId[$i]) ? ' selected="selected"' : '';?>><?php echo $centre['CENTRE_NAME'] ?></option>
    <?php } ?>
</select>

<span class="text-danger"><?php echo form_error('CENTRE_ID'); ?></span>
</div>

<div class="col-md-6">

<input id="dynamic_timing" value="<?php echo $dynamicTime[$i]; ?>" <?php if($dynamicTime[$i] == '1'){echo 'checked';} ?> name="dynamic_timing[]" type="checkbox">
<input type='hidden' value='<?php echo $dynamicTime[$i]; ?>' name="dynamic_timing[]">
<label>Dynamic Timing (Timing is Frequently Changed)</label>
</div>
</div>
</div>



<div class="form-group">
<div class="row">
<div class="col-md-12">

</div>
<div class="col-md-6">
<!-- <label for="">Preferred Timing</label> -->
<label for="from_time">From</label>
<input class="pref_time form-control" id="from_time" data-format="h:mm a" data-template="hh : mm a" name="from_time[]" value="10:00 am" type="text">
</div>
<div class="col-md-6">
<label for="to_time">To &nbsp;&nbsp;&nbsp;</label>
<input class="pref_time" id="to_time" data-format="h:mm a" data-template="hh : mm a" name="to_time[]" value="06:30 pm" type="text">
</div>
</div>
</div>



<div class="form-group">
<div class="row">
<div class="col-md-12 days_cont">
<label>Days: </label> &nbsp;
<input type="checkbox" data-day="Mon" value="Mon" name="days1[]" class="days"> Mon &nbsp;&nbsp;
<input type="checkbox" data-day="Tue" value="Tue" name="days1[]" class="days"> Tue &nbsp;&nbsp;
<input type="checkbox" data-day="Wed" value="Wed" name="days1[]" class="days"> Wed &nbsp;&nbsp;
<input type="checkbox" data-day="Thu" value="Thu" name="days1[]" class="days"> Thu &nbsp;&nbsp;
<input type="checkbox" data-day="Fri" value="Fri" name="days1[]" class="days"> Fri &nbsp;&nbsp;
<input type="checkbox" data-day="Sat" value="Sat" name="days1[]" class="days"> Sat &nbsp;&nbsp;
<input type="checkbox" data-day="Sun" value="Sun" name="days1[]" class="days"> Sun &nbsp;&nbsp;

</div>
</div>
</div>
<hr>
<?php } ?>
</div>
</div>

    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                    <label>Remark:</label>
                    <textarea class="form-control" name="REMARKS"><?php echo $this->input->post('REMARKS');?></textarea>
            </div>
        </div>
    </div>

<br>
<button type="submit" class="btn btn-primary">Save</button> &nbsp;
<button type="reset" style="float:none">Reset</button>
</div>
</div>




</div>
</div>
    <?php echo form_close(); ?>
</div>
</div>

</div>
