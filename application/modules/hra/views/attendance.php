
<style>
.rotatecell{
  -webkit-transform: rotate(+90deg);
  -moz-transform: rotate(+90deg);
}
</style>
<div class="gapping"></div>
<div class="create_batch_form">
  <div id="box">
  <h2>Monthly Attendance Report</h2>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
        <!-- start message area -->

        <?php if($this->session->flashdata('1')){ ?>
        <div class="alert alert-danger">
        <?php echo "Please select From Date and To Date"; ?>
        </div>
        <?php } ?>

        <!-- End message area -->

        </div>
        <div class="panel-body">
          <?php echo form_open();?>
          <div class="col-md-12">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-4">
                  <label>First Name: <span class="text-danger">*</span></label>
                  <?php if(IsEmployeeAttendancePermittedByRoleIds){?>
                    <input type='textbox' class='form-control' name='first_name' value="<?php if(isset($employee_attendance_search['first_name'])){ echo $employee_attendance_search['first_name']; }?>">
                  <?php }else{?>
                    <input type='hidden' class='form-control' name='first_name' value="<?php echo $this->session->userdata('admin_data')[0]['EMP_FNAME'];?>"> <?php echo $this->session->userdata('admin_data')[0]['EMP_FNAME'];?>
                  <?php }?>
                </div>
                <div class="col-lg-4">
                  <label>Last Name: <span class="text-danger">*</span></label>
                  <?php if(IsEmployeeAttendancePermittedByRoleIds){?>
                    <input type='textbox' class='form-control' name='last_name' value="<?php if(isset($employee_attendance_search['last_name'])){ echo $employee_attendance_search['last_name']; }?>">
                  <?php }else{?>
                    <input type='hidden' class='form-control' name='last_name' value="<?php echo $this->session->userdata('admin_data')[0]['EMP_LASTNAME'];?>"> <?php echo $this->session->userdata('admin_data')[0]['EMP_LASTNAME'];?>
                  <?php }?>
                </div>
                <div class="col-lg-4">
                  <label>Is Active: <span class="text-danger">*</span></label>
                  <?php if(IsEmployeeAttendancePermittedByRoleIds){?>
                  <select name="active_status" class="form-control">
                    <option value="1" <?php if(isset($employee_attendance_search['active_status'])){ if($employee_attendance_search['active_status'] == "1"){ echo "selected='selected'";}}?>>Active</option>
                    <option value="0" <?php if(isset($employee_attendance_search['active_status'])){ if($employee_attendance_search['active_status'] == "0"){ echo "selected='selected'";}}?>>Is Active</option>
                  </select>
                  <?php }else{
                        if($this->session->userdata('admin_data')[0]['ISACTIVE'] == "1"){?>
                          <input type='hidden' class='form-control' name='active_status' value="1"> Active
                      <?php }if($this->session->userdata('admin_data')[0]['ISACTIVE'] == "0"){?>
                          <input type='hidden' class='form-control' name='active_status' value="0"> In Active
                    <?php }?>
                  <?php }?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-4">
                  <label>From Date: <span class="text-danger">*</span></label>
                  <input type='textbox' class='form-control enquiry_date' name='from_date' value="<?php if(isset($employee_attendance_search['from_date'])){ echo $employee_attendance_search['from_date']; }?>" required>
                </div>
                <div class="col-lg-4">
                  <label>To Date: <span class="text-danger">*</span></label>
                  <input type='textbox' class='form-control enquiry_date' name='to_date' value="<?php if(isset($employee_attendance_search['to_date'])){ echo $employee_attendance_search['to_date']; }?>" required>
                </div>
                <div class="col-lg-4">
                  <label>Centre Name: <span class="text-danger">*</span></label>
                  <?php if(IsEmployeeAttendancePermittedByRoleIds){?>
                    <select name="centre_name" class="form-control" required>
                      <option value="" seleceted="seleceted">Select Centre Name</option>
                      <?php
                        foreach($centres as $centre){
                          $selected = "";
                          if(isset($employee_attendance_search['centre_name'])){
                            if($employee_attendance_search['centre_name'] == $centre['CENTRE_ID']){
                              $selected = "selected='selected'";
                            }
                          }
                          echo "<option value='".$centre['CENTRE_ID']."' ".$selected.">".$centre['CENTRE_NAME']."</option>";
                        }
                      ?>
                    </select>
                  <?php }else{
                      foreach($centres as $centre){
                          if($this->session->userdata('admin_data')[0]['CENTRE_ID'] == $centre['CENTRE_ID']){
                            echo "<input type='hidden' class='form-control' name='centre_name' value='".$centre['CENTRE_ID']."'> ".$centre['CENTRE_NAME'];
                          }

                      }
                    }?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-4">
                </div>
                <div class="col-lg-4">
                  <button type="submit" class="btn btn-primary" style="width:143px;" name="generate_attendance">Generate Report</button> &nbsp;
                  <button type="submit" class="btn btn-primary" name="reset_attendance">Reset</button> &nbsp;
                </div>
                <div class="col-lg-4">
                  <?php if (!empty($this->session->userdata('attendance_data'))) { ?>
                  <a onClick ="$('#export_to_excel_attendance_details').tableExport({type:'excel',escape:'false'});" class="btn" style="float:left" title="Export to excel"><i class="fa fa-file-excel-o fa-2x text-success"></i></a>

                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
          <?php echo form_close();?>
        </div>
      </div>
    </div>
  </div>
</div>
<br/>

  <?php if(!empty($employee_attendance_search)){?>
    <div class="panel panel-default">
      <div class="panel-body table-responsive">
        <table class="table table-bordered" id="export_to_excel_attendance_details">
          <?php if(!empty($employee_details_with_attendance)){
          for($i=0;$i<count($employee_details_with_attendance);$i++){ ?>
          <thead class="thead-light">
            <tr>
              <th>Employee Code</th>
              <th>Name</th>
              <th>Timing</th>
              <th>P</th>
              <th>L</th>
              <th>H</th>
              <th>A</th>
              <th>WO</th>
              <th>Total</th>
              <th>Days</th>
              <th>Direction</th>
              <?php
                for($j=0;$j<count($employee_details_with_attendance[0]['Attendance_Data']);$j++){
                  $cell_title = "";
                  if($employee_details_with_attendance[0]['Attendance_Data'][$j]['Color'] == 'rgba(255, 255, 0, 0.4)'){
                    $cell_title = "title='Sunday'";
                  }
                  else if($employee_details_with_attendance[0]['Attendance_Data'][$j]['Color'] == 'rgba(220, 220, 220, 0.9)'){
                    $cell_title = "title='Bank Holiday'";
                  }
                  else if($employee_details_with_attendance[0]['Attendance_Data'][$j]['Color'] == 'rgba(0, 125, 0, 0.4)'){
                    $cell_title = "title='Worked on Holiday'";
                  }
                 echo "<th style='background-color:".$employee_details_with_attendance[0]['Attendance_Data'][$j]['Color']."' ".$cell_title.">".$employee_details_with_attendance[0]['Attendance_Data'][$j]['Date_no']."</th>";
                }?>
            </tr>
          </thead>
          <tbody>
              <tr>
                <th><?php echo $employee_details_with_attendance[$i]['BIOMETRIC_ID']?></th>
                <th><?php echo $employee_details_with_attendance[$i]['EMPLOYEE_NAME']?></th>
                <th><?php echo $employee_details_with_attendance[$i]['EMPLOYEE_TIMING']?></th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>IN</td>
                <?php for($j=0;$j<count($employee_details_with_attendance[$i]['Attendance_Data']);$j++){
                    if($employee_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'A' || $employee_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'NAL' || $employee_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'AL' || $employee_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'O'){
                      echo "<td style='background-color:".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['Color'].";font-weight:bold;color:".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['Textcolor']."'>".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['In']."</td>";
                    }
                    else{
                      echo "<td style='writing-mode: tb-rl;background-color:".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['Color']."'>".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['In']."</td>";
                    }
                 } ?>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>OUT</td>
                <?php for($j=0;$j<count($employee_details_with_attendance[$i]['Attendance_Data']);$j++){
                    if($employee_details_with_attendance[$i]['Attendance_Data'][$j]['Out'] == 'A' || $employee_details_with_attendance[$i]['Attendance_Data'][$j]['Out'] == 'NAL' || $employee_details_with_attendance[$i]['Attendance_Data'][$j]['Out'] == 'AL' || $employee_details_with_attendance[$i]['Attendance_Data'][$j]['Out'] == 'O'){
                      echo "<td  style='background-color:".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['Color'].";font-weight:bold;color:".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['Textcolor']."'>".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['Out']."</td>";
                    }
                    else{
                      echo "<td class='' style='writing-mode: tb-rl;background-color:".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['Color']."'>".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['Out']."</td>";
                    }
                 } ?>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Hrs</td>
                <?php
                $worked_hours_normal = 0;
                $off_days_working_hours = 0;
                for($j=0;$j<count($employee_details_with_attendance[$i]['Attendance_Data']);$j++){
                  if(!($employee_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs'] == 'A') || !($employee_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs'] == 'AL') || !($employee_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs'] == 'NAL') || !($employee_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs'] == 'O')){
                    $worked_hours_normal += $employee_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs'];
                  }
                  if(($employee_details_with_attendance[$i]['Attendance_Data'][$j]['Color'] == 'rgba(220, 220, 220, 0.9)') || ($employee_details_with_attendance[$i]['Attendance_Data'][$j]['Color'] == 'rgba(255, 255, 0, 0.4)')){
                    $off_days_working_hours += $employee_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs'];
                  }
                    echo "<td style='background-color:".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['Color'].";color:".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs_Color']."'>".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs']."</td>";
                 }
                  $actual_worked_hours = $worked_hours_normal - $off_days_working_hours ?>
              </tr>
              <tr>
                <th>Process</th>
                <th><a target="_blank" href="<?php echo base_url('hra/employee_leave_approval/'.$this->encrypt->encode($employee_details_with_attendance[$i]['BIOMETRIC_ID']).'/'.$this->encrypt->encode($employee_details_with_attendance[$i]['EMPLOYEE_ID']));?>">Leave Approvals</a></th>
                <th><a target="_blank" href="<?php echo base_url('hra/update_attendance_timing/'.$this->encrypt->encode($employee_details_with_attendance[$i]['BIOMETRIC_ID']).'/'.$this->encrypt->encode($employee_details_with_attendance[$i]['EMPLOYEE_ID']));?>">Update Attendance</a></th>
                <?php
                  $days = count($employee_details_with_attendance[$i]['Attendance_Data']);
                  $paid_off_count = "";
                  $off_count = 0;
                  $off_punch_count = 0;
                  $absent_count = 0;
                  $approved_leaves_count = 0;
                  $not_approved_leaves_count = 0;
                  $late_count = 0;
                  $present_count = 0;
                  $present_count_wo_off = 0;
                  $actual_off_count = 0;
                  $half_day_count = 0;

                  for($j=0;$j<count($employee_details_with_attendance[$i]['Attendance_Data']);$j++){

                    if(($employee_details_with_attendance[$i]['Attendance_Data'][$j]['Color'] == 'rgba(255, 255, 0, 0.4)') || ($employee_details_with_attendance[$i]['Attendance_Data'][$j]['Color'] == 'rgba(220, 220, 220, 0.9)')){
                      $off_count+= 1;
                      if($employee_details_with_attendance[$i]['Attendance_Data'][$j]['In'] != 'O'){
                        $off_punch_count+= 1;
                      }
                    }

                    if(($employee_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'A') || ($employee_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'AL') || ($employee_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'NAL')){
                      $absent_count+= 1;
                      if($employee_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'AL'){
                        $approved_leaves_count+= 1;
                      }
                      elseif ($employee_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'NAL') {
                        $not_approved_leaves_count+= 1;
                      }
                    }

                    if($employee_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs_Color'] == 'red'){
                      $half_day_count+= 1;
                    }

                    if($employee_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'O'){
                      $actual_off_count+= 1;
                    }

                    $numpart = explode(".", $employee_details_with_attendance[$i]['Attendance_Data'][$j]['Time_diff']);
                    if(count($numpart) > 1){
                      if(($numpart[1] > 5 )){
                        $late_count+= 1;
                      }
                      elseif(($numpart[0] > 0 )){
                        $late_count+= 1;
                      }
                    }

                  }
                  $paid_off_count = $off_count - $off_punch_count;
                  $present_count = $days - $actual_off_count - $absent_count;
                  $present_count_wo_off = $days - $off_count;
                ?>
                <th><?php echo $present_count;?></th>
                <th><?php echo $late_count;?></th>
                <th><?php echo $half_day_count;?></th>
                <th><?php echo $absent_count;?></th>
                <th><?php echo $paid_off_count;?></th>
                <th><?php echo "0";?></th>
                <th><?php echo $days;?></th>
                <th>Late</th>
                <?php for($j=0;$j<count($employee_details_with_attendance[$i]['Attendance_Data']);$j++){
                        $text_color = "black";
                        $numpart = explode(".", $employee_details_with_attendance[$i]['Attendance_Data'][$j]['Time_diff']);
                        if(count($numpart) > 1){
                          if(($numpart[1] > 5 )){
                            $text_color = "red";
                          }
                          elseif(($numpart[0] > 0 )){
                            $text_color = "red";
                          }
                        }
                    echo "<th style='background-color:".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['Color'].";color:".$text_color."'>".$employee_details_with_attendance[$i]['Attendance_Data'][$j]['Time_diff']."</th>";
                 } ?>
              </tr>
              <tr>
                <?php
                  $work_text_styling = "text-align:right;";
                  $extra_text_styling = "text-align:right;";
                  $final_text_styling = "text-align:right;";
                  if($actual_worked_hours > ($present_count_wo_off * 8.30)){
                    $work_text_styling = "text-align:right;color:green;font-weight:bold;";
                  }
                  else if($actual_worked_hours < ($present_count_wo_off * 8.30)){
                    $work_text_styling = "text-align:right;color:red;font-weight:bold;";
                  }

                  if($off_days_working_hours > ($off_punch_count * 8.30)){
                    $extra_text_styling = "text-align:right;color:green;font-weight:bold;";
                  }
                  else if($off_days_working_hours < ($off_punch_count * 8.30)){
                    $extra_text_styling = "text-align:right;color:red;font-weight:bold;";
                  }

                  if(($actual_worked_hours + $off_days_working_hours) > (($present_count_wo_off * 8.30) + ($off_punch_count * 8.30))){
                    $final_text_styling = "text-align:right;color:green;font-weight:bold;";
                  }
                  else if(($actual_worked_hours + $off_days_working_hours) < (($present_count_wo_off * 8.30) + ($off_punch_count * 8.30))){
                    $final_text_styling = "text-align:right;color:red;font-weight:bold;";
                  }
                ?>
                <td colspan="12">
                  <table class="table table-bordered">
                    <tr><td></td><th>Total Days</th><th>Days Off</th><th>Worked days</th><th>Hrs. worked</th><th>Actual Working Hrs</th></tr>
                    <tr><th>Working Details:</th><td style="text-align:right;"><?php echo $days;?></td><td style="text-align:right;"><?php echo $employee_details_with_attendance[$i]['Actual_off_days'];?></td><td style="text-align:right;"><?php echo $present_count_wo_off;?></td><td style="<?php echo $work_text_styling;?>"><?php echo $actual_worked_hours;?></td><td style="text-align:right;"><?php echo ($present_count_wo_off * 8.30);?></td></tr>
                    <tr><th>Extra Working details:</th><td style="text-align:right;"></td><td style="text-align:right;"></td><td style="text-align:right;"><?php echo $off_punch_count;?></td><td style="<?php echo $extra_text_styling;?>"><?php echo $off_days_working_hours;?></td><td style="text-align:right;"><?php echo ($off_punch_count * 8.30);?></td></tr>
                    <tr><td></td><td style="text-align:right;"></td><td style="text-align:right;"></td><td style="text-align:right;"><?php echo ($present_count_wo_off + $off_punch_count);?></td><td style="<?php echo $final_text_styling;?>"><?php echo ($actual_worked_hours + $off_days_working_hours);?></td><td style="text-align:right;"><?php echo (($present_count_wo_off * 8.30) + ($off_punch_count * 8.30));?></td></tr>
                  </table>
                </td>
                <td colspan="10">
                  <table class="table table-bordered">
                    <tr><th colspan="3">Leaves Bifercation</th></tr>
                    <tr><th>Leaves taken</th><th>Approved leaves</th><th>Not Approved leaves</th></tr>
                    <?php
                      $absent_leave_styling="";
                      $approved_leave_styling="";
                      $not_approved_leave_styling="";
                      if($absent_count != 0){
                        $absent_leave_styling = "color:red;font-weight:bold;";
                      }
                      if($approved_leaves_count != 0){
                        $approved_leave_styling = "color:green;font-weight:bold;";
                      }
                      if($not_approved_leaves_count != 0){
                        $not_approved_leave_styling = "color:blue;font-weight:bold;";
                      }
                    ?>
                    <tr><td style="<?php echo $absent_leave_styling;?>"><?php echo $absent_count;?></td><td style="<?php echo $approved_leave_styling;?>"><?php echo $approved_leaves_count;?></td><td style="<?php echo $not_approved_leave_styling;?>"><?php echo $not_approved_leaves_count;?></td></tr>
                  </table>
                </td>
              </tr>
              <th colspan="100"></th>
          </tbody>
        <?php }
        }?>
        </table>
      </div>
    </div>
  <?php }?>
