<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
    <div class="create_batch_form">
        <div id="box">
            <h2>Bank Master</h2></div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align:center; color:#F00;"> </div>
                        <div class="panel-body">
                            <?php echo form_open(); ?>   

                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-4">
                                            <label>Bank Name:</label>
                                            <input class="form-control" type="text" value="<?php if(isset($save_bank_name['BANK_NAME'])){ echo $save_bank_name['BANK_NAME']; }?>" maxlength="50" name="BANK_NAME">
                                        </div>
                                        <div class="col-lg-4">
                                            <br>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary" name="save_bank_name">Save</button>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" name="reset_name" class="btn btn-primary" style="float:left">Reset</button>
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <?php if($this->session->flashdata('flashMsg')){
                                                     if($this->session->flashdata('flashMsg') == "1"){
                                                        echo "<label id='flashmsg' style='color:red'>* Bank added successfully.</label>";
                                                     }
                                                     else{
                                                        echo "<label id='flashmsg' style='color:red'>* Please provide proper bank name.</label>";
                                                        }?>
                                                     <script>
                                                        setTimeout(function(){ $("#flashmsg").hide(); }, 3000);
                                                     </script>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php echo form_close(); ?>
                        </div>
                        </div>
                    </div>
            </div>
    </div>
        <br/>
        <div id="box"><h2></h2></div>
        <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>Sr no.</th>
                                <th>Bank Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $i = 1;
                            foreach($banks as $bank){?>
                            <tr>
                                <td><span class="glyphicon glyphicon-edit edit_bank_name" data-bank-id="<?php echo $bank['BANK_ID']; ?>" data-bank-name="<?php echo $bank['BANK_NAME']; ?>" data-toggle="modal" data-target="#edit_bank_name" title="Edit"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="<?php echo base_url('hra/add-bank-branch/'.$this->encrypt->encode($bank['BANK_ID']).'/'.$this->encrypt->encode($bank['BANK_NAME']).''); ?>" title="Add Branch"><span class="glyphicon glyphicon-plus"></span></a></td>
                                <td><?php echo $i; $i++;?></td>
                                <td><?php echo $bank['BANK_NAME'];?></td>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
</div>

<!-- edit bank name model -->
<div id="edit_bank_name" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Bank Name</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" name="bank_id" id="bank_id">
                    <input type="hidden" name="bank_name" id="bank_name">
                    <div class="col-md-4">
                        <div class="form-group">    
                            <label>Bank Name :</label>
                            <input class="form-control modal_bank_name" id="modal_bank_name" type="text" name="control_key">
                        </div>
                    </div>
                    <div class="col-md-4">
                    <br/>
                        <button type="submit" id="save_bank_name" class="btn btn-primary" style="float:left">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- edit bank name model -->