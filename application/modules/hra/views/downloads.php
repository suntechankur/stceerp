
<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
<div class="create_batch_form">
    <div id="box">
        <h2>Downloads</h2>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="form-group">
    						<!-- starting of hidden div reinstate ex employee -->
    					          	<div class="row">
    					          		<div class="col-lg-12">
    					          		<table class="table table-striped table-hover">
    							            <thead>
    							              <tr>
    							              	<th>Sr No.</th>
    							                <th>Category</th>
    							                <th>File</th>
    							                <th>Last Updated</th>
    							                <th>Download</th>
    							              </tr>
    							            </thead>
    							            <tbody>
                                <?php
                                $i = 1;
                                $xml = simplexml_load_file(base_url("resources/events/Downloads.xml"));
                                foreach ($xml as $download){?>
    							            	<tr>
    							            		<td><?php echo $i++;?></td>
                                  <td><?php echo $download->Category;?></td>
                                  <td><?php echo $download->Url;?></td>
    							            		<td><?php echo $download->updatedDate;?></td>
                                  <td align="center"><a href="<?php echo base_url("resources/events/".$download->Url);?>" target="_blank" style="color:red"><span class="fa fa-2x fa-download"></span></a></td>
                      				</tr>
                              <?php }?>
    							            </tbody>
    						              </table>
    					          		</div>
    					          	</div>
    						<!-- ending of hidden div of reinstate employee -->
					             </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>
