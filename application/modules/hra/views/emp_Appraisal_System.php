<div class="gapping"></div>
<div class="create_batch_form">
<div id="box">
<h2>Employee Appraisal Report</h2>

</div>
<div class="row">

<div class="col-sm-12">
<div class="panel panel-default">
<div class="panel-heading">
<!-- start message area -->

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>


<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info1')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
</div>
<?php } ?>

<?php if($this->session->flashdata('warning')) { ?>
<div class="alert alert-warning">
<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php }?>

<!-- End message area --> 
</div>
<div class="panel-body">
<?php echo form_open(); ?>

<div class="row">

	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group">
			    <label>First Name:<span class="text-danger">*</span></label>
			    <input class="form-control" name="fname" id="fname" value="<?php if(isset($appraisal_search_data['fname'])){ echo $appraisal_search_data['fname'];}?>" type="text"> 
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Last Name : </label>
			    <input class="form-control" name="lname" id="lname" value="<?php if(isset($appraisal_search_data['lname'])){ echo $appraisal_search_data['lname'];}?>" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Center : </label>
			    <div class="form-group">
			    <select name="CENTRE_ID">
			    <option value="">Select Centre</option>
                                                <?php foreach($centres as $centre){ ?>
                                                <option value="<?php echo $centre['CENTRE_ID']; ?>" <?php if(isset($appraisal_search_data['CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $appraisal_search_data['CENTRE_ID']) ? ' selected="selected"' : '';}?>><?php echo $centre['CENTRE_NAME']; ?></option>
                                                <?php } ?>
			    </select></div>
			    <span class="text-danger"></span>
			</div>	
		</div>

	</div>
	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Review Date From:<span class="text-danger">*</span></label>
			    <input class="form-control enquiry_date" name="review_from_date" value="<?php if(isset($appraisal_search_data['review_from_date'])){ echo $appraisal_search_data['review_from_date'];}?>" type="text"> 
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Review Date To: </label>
			    <input class="form-control enquiry_date" name="review_to_date"  value="<?php if(isset($appraisal_search_data['review_to_date'])){ echo $appraisal_search_data['review_to_date'];}?>" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Designation : </label>
			    <select name="DESIGNATION" class="form-control">
                    <option value="">Select Designation</option>
                        <?php foreach($designation as $dsg){ ?>
                        <option value="<?php echo $dsg['CONTROLFILE_ID']; ?>" <?php if(isset($appraisal_search_data['DESIGNATION'])){ echo ($dsg['CONTROLFILE_ID'] == $appraisal_search_data['DESIGNATION']) ? ' selected="selected"' : '';}?>><?php echo $dsg['CONTROLFILE_VALUE']; ?></option>
                    	<?php } ?>
                </select>
			</div>	
		</div>

	</div>
	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Appraisal(Latest/Old/Next) :<span class="text-danger">*</span></label>
			    <div class="form-group">
				    <select>
				    	<option value="0" >Select</option>
				    	<option value="Latest">Latest</option>
				    	<option value="Old">Old</option>
				    </select>
			    </div>
			</div>	
		</div>
	</div>
	<div class="col-md-12">
		<div class="col-sm-6">
			<div class="form-group" align="center">
			    <button name="submit" value="submit" id="submit" class="btn btn-primary">Search</button> 
			</div>	
		</div>
		<div class="col-sm-6">
			<div class="form-group" align="center">
			    <button name="reset" value="reset" id="reset" class="btn btn-primary">Reset</button> 
			</div>	
		</div>
	</div>
</div>
<?php echo form_close();?>
</div>
</div>
</div>
</div>
</div>



<p></p>
<?php if($this->session->userdata('appraisal_filter_data'))
{?>
<div id="box">
	<h2 class="text-center">Appraisal Search List</h2>
	
</div>
<div class="panel panel-default">
<div class="panel-body table-responsive">
<table class="table table-bordered table-condensed table-stripped">
<thead>
<tr>
<th >Appraisal ID.</th>
<th >Employee ID.</th>
<th>Name</th>
<th >Centre</th>
<th >DOJ</th>
<th >Review Date</th>
<th >Appraisal Date</th>
<th >Nex Appraisal Date</th>
<th >Old Designation</th>
<th >New Designation</th>
<th >Old Salary</th>
<th >New Salary</th>
<th>Remarks</th>
<th>Appraisal Letter / Action</th>
</tr>
</thead>
<tbody>

<?php 
$count = $this->uri->segment(2) + 1;
foreach($appraisal_list as $key=>$data){ 
?>
<tr>

<?php
$EMPLOYEE_ID = $this->encrypt->encode($data['EMPLOYEE_ID']);
$APPRAISAL_ID = $this->encrypt->encode($data['APPRAISAL_ID']);
?>
<td><?php print $data['APPRAISAL_ID'];?></td>
<td><?php print $data['EMPLOYEE_ID']?></td>
<td><?php print $data['EMP_NAME'];?></td>
<td><?php print $data['CENTRE_NAME'];?></td>
<td><?php print date('d/m/Y',strtotime($data['DATEOFJOIN']));?></td>
<td><?php print date('d/m/Y',strtotime($data['REVIEW_DATE']));?></td>
<td><?php print date('d/m/Y',strtotime($data['EFFECTIVE_DATE']));?></td>
<td><?php print date('d/m/Y',strtotime($data['APPRAISAL_DUE_DATE']));?></td>
<td><?php print $data['OLD_DESIGNATION'];?></td>
<td><?php print $data['NEW_DESIGNATION'];?></td>
<td><?php print $data['OLD_SALARY'];?></td>
<td><?php print $data['NEW_SALARY'];?></td>
<td><?php print $data['REMARKS'];?></td>
<td>Print <a href="<?php echo base_url('hra/performanceAppraisalUpdate/').$APPRAISAL_ID;?>">Edit</a></td>


</tr>
<?php $count++; } ?>
</tbody>
</table>
<br> </div>
<div class="row">
<div class="col-md-4 pull-right">
<?php echo $pagination; ?>
</div>
</div>
</div>
<?php } ?>