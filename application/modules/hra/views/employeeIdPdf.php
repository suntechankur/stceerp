<?php 

tcpdf();
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nic');
$pdf->SetTitle('TCPDF Example 006');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// ---------------------------------------------------------

// add a page
$pdf->AddPage();

// create some HTML content
$imgSrc = base_url('resources/images/id_card_2017.png');

	$division = count($empIdsToPrint);
	$diffdivision = $division;
    $rowdivision = $division/3;
	$ceil = ceil($rowdivision);

$k=0;
$html = '<table cellpadding="7" cellspacing="1">';
for ($i=0; $i < $ceil ; $i++) {
	$html .= '<tr>';
	for ($j=0; $j < 3 ; $j++) {
		$doj = date("d M Y", strtotime($empIdsToPrint[$k]['DATEOFJOIN']));
		$empName = strtoupper($empIdsToPrint[$k]['EMP_FNAME'].' '.$empIdsToPrint[$k]['EMP_LASTNAME']);
		$desg = strtoupper($empIdsToPrint[$k]['CONTROLFILE_VALUE']);
		$html .= '<td>
<table style="width:180px; border: 1px solid black;">
<tr>
<td>
<img src="'.base_url('resources/images/icard/bottome_band.jpg').'" alt="band" style="width:210px;height:15px">
</td>
</tr>
<tr>
<td align="center">
<img src="'.base_url('resources/images/icard/logo.png').'" alt="logo" style="width:130px;height:55px">
</td>
</tr>
<tr >
<td align="center">
<br>
<img src="'.base_url('resources/images/icard/prof.jpg').'" alt="user" style="width:100px;height:110px">
</td>
</tr>
<tr >
<td align="center">
<span><strong>'.$empName.'</strong></span><br>
<span style="font-size:8px"><strong>'.$desg.'</strong></span><br>
<span style="font-size:8px"><strong>Employee ID: <span>'.$empIdsToPrint[$k]['EMPLOYEE_ID'].'</span></strong></span>
<br>
<br>
</td>
</tr>
<tr>
<td align="center">
<br>
<span style="font-size:8px">Joining Date: <strong> <span>'.$doj.'</span></strong></span>
</td>
</tr>
<tr>
<td>
<img src="'.base_url('resources/images/icard/bottome_band.jpg').'" alt="band" style="width:210px;height:15px">
</td>
</tr>
</table>
</td>';
		$diffdivision = $diffdivision - 1;
		if($diffdivision == 0)
		{
			break;
		}
	$k++;}
	$html .= '</tr><br><br><br><br><br>';
}

$html .= '</table>';
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();




// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_006.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
?>		