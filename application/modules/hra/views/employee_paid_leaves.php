<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
<div class="create_batch_form">
<div id="box">
<h2>Paid Leave Search</h2></div>
<div class="row">
<div class="col-sm-12">
<div class="panel panel-default">
<div class="panel-heading">
<!-- start message area -->

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('newBatchId')) { ?>
<div class="alert alert-success">
<?php echo "New Batch ID Generated: " . $this->session->flashdata('newBatchId'); ?>
</div>
<?php } ?>


<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info1')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
</div>
<?php } ?>

<?php if($this->session->flashdata('warning')) { ?>
<div class="alert alert-warning">
<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php } ?>

<!-- End message area --> 

</div>
<div class="panel-body">
<?php echo form_open(); ?>   
<div class="col-md-6">
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-4">
<label>Center:</label>
<select name="CENTRE_ID" class="form-control">
    <option value=''>Please Select</option>
    <?php foreach($centres as $centre){?>
    <option value="<?php echo $centre['CENTRE_ID']; ?>"<?php echo ($centre['CENTRE_ID'] == $filter_pl['CENTRE_ID']) ? ' selected="selected"' : '';?>><?php echo $centre['CENTRE_NAME']; ?></option>
    <?php } ?>
</select>   

</div>
<div class="col-lg-4">
<label>First Name:</label>
<input class="form-control" type="text" value="<?php echo $filter_pl['FIRSTNAME'] ? $filter_pl['FIRSTNAME'] : '';?>" maxlength="50" name="FIRSTNAME" id="FIRSTNAME">
</div>
<div class="col-lg-4">
<label>Last Name:</label>
<input class="form-control" type="text" value="<?php echo $filter_pl['LASTNAME'] ? $filter_pl['LASTNAME'] : '';?>" name="LASTNAME" id="LASTNAME" maxlength="50">
</div>


</div>
</div>
</div>
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-4">
<label>Designation:</label>

<select name="CURRENT_DESIGNATION" class="form-control">
<option value="">Please Select</option>
<?php

foreach($designations as $designation){ 

?>
<option value="<?php echo $designation['CONTROLFILE_ID'] ?>"<?php echo $designation['CONTROLFILE_ID']==$filter_pl['CURRENT_DESIGNATION'] ? 'selected' : '';?>><?php echo $designation['CONTROLFILE_VALUE'] ?></option>
<?php } ?>
</select>
<span class="text-danger"><?php echo form_error('CURRENT_DESIGNATION'); ?></span>

</div>
<div class="col-lg-4">
<label>Department:</label>
<select name="DEPARTMENT_ID" class="form-control">
<option value="">Please Select</option>
<?php foreach($departments as $department){ ?>
<option value="<?php echo $department['DepartmentId'] ?>"<?php echo ($department['DepartmentId'] == $filter_pl['DEPARTMENT_ID']) ? ' selected="selected"' : '';?>><?php echo $department['DepartmentName'] ?></option>
<?php } ?>
</select>
<?php echo form_error('DEPARTMENT_ID'); ?></span>
</div>
<div class="col-lg-4">
<label>Is Active:</label>
<select class="form-control" name="ISACTIVE">
<option value="">Select</option>

<option value="0"<?php echo $filter_pl['ISACTIVE']=='0' ? 'selected' : '';?>>All</option> 
<option value="1"<?php echo $filter_pl['ISACTIVE']=='1' ? 'selected' : '';?>>Active</option> 
<option value="2"<?php echo $filter_pl['ISACTIVE']=='2' ? 'selected' : '';?>>InActive</option> 
</select>
</div>
</div>
</div>
</div>

</div>
<div class="col-md-6">
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">

<div class="col-lg-4">
<label>From date of join:</label>
<input type="text" name="FROMJOINDATE" id="FROMJOINDATE" value="<?php if(isset($filter_pl['FROMJOINDATE'])){ echo $filter_pl['FROMJOINDATE']; }?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY">
</div>
<div class="col-lg-4">
<label>To date of join:</label>
<input type="text" name="TOJOINDATE" id="TOJOINDATE" value="<?php if(isset($filter_pl['TOJOINDATE'])){ echo $filter_pl['TOJOINDATE']; }?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY">
</div>
<div class="col-lg-4">
<label>Years:</label>
<select class="form-control" name="YEAR">
<option value="">Select Year</option>
<option value="2014-2015"<?php echo $filter_pl['YEAR']=="2014-2015" ? 'selected' : ''?>>2014-2015</option>
<option value="2015-2016"<?php echo $filter_pl['YEAR']=="2015-2016" ? 'selected' : ''?>>2015-2016</option>
<option value="2016-2017"<?php echo $filter_pl['YEAR']=="2016-2017" ? 'selected' : ''?>>2016-2017</option>
<option value="2017-2018"<?php echo $filter_pl['YEAR']=="2017-2018" ? 'selected' : ''?>>2017-2018</option>

</select>
</div>

</div>
</div>
</div>
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-6">
<br>
<button type="submit" value="submit" id="submit" class="btn btn-primary" style="float:right">Search</button> 
</div>

<div class="col-lg-6">
<br>
<button type="submit" name="reset" value="reset" id="reset" class="btn btn-primary" style="float:left">Reset</button> 
</div>


</div>
</div>
</div>


</div>
<?php echo form_close(); ?>
</div>
</div>
</div>
</div>
</div>
<div id="box"> <span class=""><?php echo $pagination; ?></span>
<h2 class="text-center">Paid Leaves</h2>
<!--        <div class="row"><div class="col-md-4 pull-right"></div></div>--></div>
<div class="panel panel-default">
<!--<div class="panel-heading">Form Elements</div>-->
<div class="panel-body table-responsive">
<table class="table table-bordered table-condensed table-stripped">
<thead>
<tr>

<th>&nbsp;Action&nbsp;</th>
<th >Employee ID</th>
<th >Boimetric ID</th>
<th>Name</th>
<th >Center</th>
<th >Date of join</th>
<th >Designation</th>
<th >PL Year</th>
<th >Apr</th>
<th >May</th>
<th >Jun</th>
<th >Jul</th>
<th >Aug</th>
<th >Sep</th>
<th >Oct</th>
<th >Nov</th>
<th >Dec</th>
<th >Jan</th>
<th >Feb</th>
<th >Mar</th>
<th >Total PL</th>
<th >Remening PL</th>
<th style="padding-left: 50px;padding-right: 50px;">Remarks</th>

</tr>
</thead>
<tbody>
<!--                    <tr><td colspan="18" class="text-center">Sorry Record Not Found</td></tr>							-->
<?php 

foreach($pl as $data){ 
?>
<tr >
<?php $EMPLOYEE_ID = $this->encrypt->encode($data['EMPLOYEE_ID']);?>                           
<td>

<span class="glyphicon glyphicon-edit handCursor edit_PaidLeave" data-empid="<?php echo $EMPLOYEE_ID; ?>" data-toggle="modal" data-target="#myModal"></span>
</td>
<td><?php print $data['EMPLOYEE_ID']?></td>
<td><?php print $data['BIOMETRIC_DEVICE_ID']?></td>
<td><?php print $data['NAME']?></td>
<td><?php print $data['CENTRE_NAME']?></td>
<td><?php print date("d/m/Y",strtotime($data['DATEOFJOIN']))?></td>

<td><?php echo $data['DESIGNATION'] ? $data['DESIGNATION'] : ''?></td>
<td class="year"><?php echo $data['YEAR'] ? $data['YEAR'] : ''?></td>
<td><?php echo $data['APRIL'] ? $data['APRIL'] : '0'?></td>
<td><?php echo $data['MAY'] ? $data['MAY'] : '0'?></td>
<td><?php echo $data['JUNE'] ? $data['JUNE'] : '0'?></td>
<td><?php echo $data['JULY'] ? $data['JULY'] : '0'?></td>
<td><?php echo $data['AGUST'] ? $data['AGUST'] : '0'?></td>
<td><?php echo $data['SEPTEMBER'] ? $data['SEPTEMBER'] : '0'?></td>
<td><?php echo $data['OCTOBER'] ? $data['OCTOBER'] : '0'?></td>
<td><?php echo $data['NOVEMBER'] ? $data['NOVEMBER'] : '0'?></td>
<td><?php echo $data['DECEMBER'] ? $data['DECEMBER'] : '0'?></td>
<td><?php echo $data['JANUARY'] ? $data['JANUARY'] : '0'?></td>
<td><?php echo $data['FEBRUARY'] ? $data['FEBRUARY'] : '0'?></td>
<td><?php echo $data['MARCH'] ? $data['MARCH'] : '0'?></td>
<td><?php
    $joindate = date("d", strtotime($data['DATEOFJOIN']));
    if($joindate>1)
    {
    $beginDate = strtotime(date("Y-m-d", strtotime($data['DATEOFJOIN'])) . "+6 months");
    }
    else
    {
       $beginDate = strtotime(date("Y-m-d", strtotime($data['DATEOFJOIN'])) . "+5 months"); 
    }
    $endDate = strtotime("2018-03-31");

    $totalAllowPL = ((date('Y',$endDate) - date('Y',$beginDate)) * 12) + (date('m',$endDate) - date('m',$beginDate));
    if($totalAllowPL>12)
    {
        $totalAllowPL = 12;
    }
    

 echo $data['TOTAL_PL'] ? $data['TOTAL_PL'] : '0'?>/
 <?php echo $totalAllowPL;?></td>

<td>
<?php

$remeningPL = $totalAllowPL-($data['TOTAL_PL']);
  echo $remeningPL ? $remeningPL : '0'?></td>
<td><?php echo $data['REMARKS'] ? $data['REMARKS'] : ''?></td>




</tr>
<?php } ?>
</tbody>
</table>
<br> </div>
<div class="row">
<div class="col-md-4 pull-right">
<?php echo $pagination; ?>
</div>
</div>
</div>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Paid Leave</h4>
      </div>
      <div class="modal-body plBody">
       
        </div>                           
      <div class="modal-footer">
        <div class="col-md-3">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        <div class="col-md-2">
            <button type="button" class="btn btn-info updatePl" data-dismiss="modal">Change</button>
        </div>
      </div>
    </div>

  </div>
</div>