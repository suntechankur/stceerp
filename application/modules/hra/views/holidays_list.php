
<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
<div class="create_batch_form">
    <div id="box">
        <h2>Holidays List of Year <?php echo date('Y');?></h2>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="form-group">
    						<!-- starting of hidden div reinstate ex employee -->
    					          	<div class="row">
    					          		<div class="col-lg-12">
    					          		<table class="table table-striped table-hover">
    							            <thead>
    							              <tr>
    							              	<th>Sr No.</th>
    							                <th>Date</th>
    							                <th>Occassion Name</th>
    							              </tr>
    							            </thead>
    							            <tbody>
                                <?php
                                $i = 1;
                                foreach($holidays as $holiday){?>
    							            	<tr>
    							            		<td><?php echo $i++;?></td>
    							            		<td><?php echo date("d/m/Y", strtotime($holiday['DATE']));?></td>
    							            		<td><?php echo $holiday['OCCASSION'];?></td>
    							            	</tr>
                              <?php }?>
    							            </tbody>
    						              </table>
    					          		</div>
    					          	</div>
    						<!-- ending of hidden div of reinstate employee -->
					             </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>
