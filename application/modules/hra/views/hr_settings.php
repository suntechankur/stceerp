
<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
<div class="create_batch_form">
    <div id="box">
        <h2>HR SETTINGS</h2>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align:center; color:#F00;"></div>
                <div class="panel-body">
                   <?php 
                    $attr = array('role'=>'form');
                    echo form_open('',$attr); ?>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                  <div class="col-lg-12">
								  		<span class="handCursor btn btn-primary" data-toggle="modal" data-target="#designationModal" align="center"><strong>Add new Designation</strong></span>
								  </div>	
								</div>
								<hr />
								<br>
								<div class="row">
								  <div class="col-lg-12">	
							 			<span class="handCursor btn btn-primary" data-toggle="modal" data-target="#payrollModal" ><strong>Edit Payroll and Attendance Dates</strong></span>
								  </div>	
								</div>
								<hr />
								<br>
								<div class="row">
								  <div class="col-lg-12">	
										<span class="handCursor btn btn-primary" id="openReinstate"><strong>Reinstate Ex-Employee</strong></span>
								  </div>	
								</div>
						<!-- starting of hidden div reinstate ex employee -->
								<div id="reinstateEmployee" style="display:none;">
								<br/>
								<center><h3>Reinstate an employee</h3></center>
								<br/>
									<div class="row">
										<div class="col-md-6">
										    <div class="form-group">	
											    <label>First Name :</label>
											    <input class="form-control" type="text" id="firstname" required="required">
										    </div>
										</div>
										<div class="col-md-6">
										    <div class="form-group">	
											    <label>Last Name :</label>
											    <input class="form-control" type="text" id="lastname" required="required">
										    </div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
										    <div class="form-group">	
											    <label>Designation :</label>
											    <select class="form-control" id="designation">
											    <option selected="selected" disabled="disabled">
											    	Select Designation
											    </option>
											    	<?php   
					                                 foreach($designations as $designation){ 
					                                    ?>
					                                <option value="<?php echo $designation['CONTROLFILE_ID'] ?>"><?php echo $designation['CONTROLFILE_VALUE'] ?></option>
					                                <?php } ?>
											    </select>
										    </div>
										</div>
										<div class="col-md-6">
										    <div class="form-group">
										    	<br/>	
											    <input type="button" class="btn btn-primary" value="Search Employee" id="reinstateemployeesearch">
										    </div>
										</div>
									</div>

								</div>
								<br/>
					          	<div class="row" id="reinstateemployeedata" style="display: none;">
					          		<div class="col-lg-12">
					          		<table class="table table-striped table-hover">
							            <thead>
							              <tr>
							              	<th>Sr No.</th>
							                <th>Emp ID</th>
							                <th>Full Name</th>
							                <th>Designation</th>
							                <th>Emp Phone</th>
							                <th>Emp Mobile No.</th>
							                <th>Date of Leaving</th>
							                <th></th>
							              </tr>
							            </thead>
							            <tbody id="employeedetails">
							            	<tr>
							            		<td colspan="7">No records found</td>
							            	</tr>
							            </tbody>
						              </table>
					          		</div>
					          	</div>
						<!-- ending of hidden div of reinstate employee -->
							</div>	
                        </div>

                  <?php echo form_close(); ?>
                </div>
            </div>
        </div>

 <div id="designationModal" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-lg">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
              	<button type="button" class="close" data-dismiss="modal">&times;</button>	
		        <h4 class="modal-title">Add New Designation</h4>
		      </div>
			<div class="modal-body">
			<?php echo form_open('hr_settings_add_new_designation');?>
				<div class="row">
					<div class="col-md-6">
					    <div class="form-group">	
						    <label>Designation :</label>
						    <input class="form-control" type="text" name="designation" required="required">
					    </div>
					</div>
					<div class="col-md-6">
					    <div class="form-group">	
						    <label>Description :</label>
						    <input class="form-control" type="text" name="description" required="required">
					    </div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="col-md-4">
							<button type="submit" name="add" class="btn btn-primary" style="float:left">Add</button>
						</div>
						<div class="col-md-4">
							<button type="reset" name="reset" class="btn btn-primary" style="float:left">Reset</button>
						</div>
					</div>
				</div>
			<?php echo form_close(); ?>
			</div>
</div>
</div>
</div>

<div id="payrollModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Payroll Settings</h4>
			</div>
			<div class="modal-body">
			<?php echo form_open('hr_settings_update_payroll_details');?>
				<div class="row">
					<div class="col-md-6">
					    <div class="form-group">	
						    <label>Pay slip show date till :</label>
						    <input class="form-control enquiry_date" type="text" name="paydate" value = "<?php echo $payroll_data['PAY_SLIP_DATE'];?>" required="required">
					    </div>
					</div>
					<div class="col-md-6">
					    <div class="form-group">	
						    <label>Display attendance :</label>
						    <?php if($payroll_data['ATTENDANCE_ACTIVE'] == "True"){?>
						    	<input class="form-control" type="checkbox" name="showattendance" checked="True">
						    <?php }else{ ?>
						    	<input class="form-control" type="checkbox" name="showattendance">
						    <?php } ?>
					    </div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
					    <div class="form-group">	
						    <label>From date :</label>
						    <input class="form-control enquiry_date" type="text" name="fromdate" value = "<?php echo $payroll_data['ATTENDANCE_FROM_DATE'];?>" required="required">
					    </div>
					</div>
					<div class="col-md-6">
					    <div class="form-group">	
						    <label>To date :</label>
						    <input class="form-control enquiry_date" type="text" name="todate" value = "<?php echo $payroll_data['ATTENDANCE_TO_DATE'];?>" required="required">
					    </div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="col-md-4">
							<button type="submit" name="add" class="btn btn-primary" style="float:left">Update</button>
						</div>
						<div class="col-md-4">
							<button type="reset" name="reset" class="btn btn-primary" style="float:left">Reset</button>
						</div>
					</div>
				</div>
			<?php echo form_close();?>
			</div>
		</div>
	</div>
</div>