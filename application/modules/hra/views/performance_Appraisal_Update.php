<div class="gapping"></div>
<div class="create_batch_form">
<div id="box">
<h2>Employee Performance Appraisal Update</h2>

</div>
<div class="row">

<div class="col-sm-12">
<div class="panel panel-default">
<div class="panel-heading">
<!-- start message area -->

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>


<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info1')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
</div>
<?php } ?>

<?php if($this->session->flashdata('warning')) { ?>
<div class="alert alert-warning">
<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php }?>

<!-- End message area --> 
</div>
<div class="panel-body">
<?php echo form_open();
print_r($appraisal);
?>

<div class="row">
	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Employee No:<span class="text-danger">*</span></label>
			    <input class="form-control" readonly='' name="emp_no" id="emp_no" value="<?php echo $appraisal['0']['EMPLOYEE_ID'];?>" type="text"> 
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Employee Name: </label>
			    <input class="form-control" name="emp_name" id="emp_name" value="<?php echo $appraisal['0']['EMP_NAME'];?>" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Center : </label>
			    <div class="form-group">
				    <select name="CENTRE_ID">
				    <option value="">Select Centre</option>
	                                                <?php foreach($centres as $centre){ ?>
	                                                <option value="<?php echo $centre['CENTRE_ID']; ?>" <?php echo ($centre['CENTRE_ID'] == $appraisal['0']['CENTRE_ID']) ? ' selected="selected"' : '';?>><?php echo $centre['CENTRE_NAME']; ?></option>
	                                                <?php } ?>
				    </select>
			    </div>
			</div>	
		</div>
	</div>
	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Current Department:<span class="text-danger">*</span></label>
			    <select name="DEPARTMENT" class="form-control">
                     <option value="">Select Department</option>
                             <?php foreach($department as $dpt){ ?>
                             <option value="<?php echo $dpt['DEPARTMENT_ID']; ?>" <?php echo ($dpt['DEPARTMENT_ID'] == $appraisal['0']['OLD_DEPARTMENT_ID']) ? ' selected="selected"' : '';?>><?php echo $dpt['DEPARTMENT_NAME']; ?></option>
                              <?php } ?>
                </select>
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Current Designation: </label>
			    <select name="DESIGNATION" class="form-control">
                    <option value="">Select Designation</option>
                        <?php foreach($designation as $dsg){ ?>
                        <option value="<?php echo $dsg['CONTROLFILE_ID']; ?>" <?php if($appraisal['0']['OLD_DESIGNATION'] == $dsg['CONTROLFILE_ID']){ echo "selected";}?>><?php echo $dsg['CONTROLFILE_VALUE']; ?></option>
                    	<?php } ?>
                </select>
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Current Salary: </label>
			    <input class="form-control" name="current_salary" id="center" value="<?php echo $appraisal['0']['OLD_SALARY']?>" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>

	</div>
	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Last Reviewer:<span class="text-danger">*</span></label>
			    <input class="form-control" name="last_reviewer" id="last_reviewer" value="<?php echo $appraisal['0']['LAST_REVIEWER_NAME'];?>" type="text"> 
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Last Review Date:</label>
			    <input class="form-control enquiry_date" name="last_review_date" id="last_review_date" value="<?php echo date('d/m/Y',strtotime($appraisal['0']['LAST_REVIEW_DATE']));?>" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Appraisal Due Date:</label>
			    <input class="form-control enquiry_date" name="Appraisal_due_date" id="Appraisal_due_date" value="<?php echo date('d/m/Y',strtotime($appraisal['0']['APPRAISAL_DUE_DATE']));?>" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>

	</div>
		<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Reviewer Name:<span class="text-danger">*</span></label>
			    <input class="form-control" name="reviewer_name" id="reviewer_name" value="<?php echo $appraisal['0']['REVIEWER_NAME'];?>" type="text"> 
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Review Date:</label>
			    <input class="form-control enquiry_date" name="review_date" id="review_date" value="<?php echo date('d/m/Y',strtotime($appraisal['0']['REVIEW_DATE']));?>" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Effective Date:</label>
			    <input class="form-control enquiry_date" name="Effective_date" id="Effective_date" value="<?php echo date('d/m/Y',strtotime($appraisal['0']['EFFECTIVE_DATE']));?>" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>

	</div>	
	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group">
			    <label>New Department: <span class="text-danger">*</span></label>
				 <select name="NEW_DEPARTMENT" class="form-control">
                     <option value="">Select Department</option>
                         <?php foreach($department as $dpt){ ?>
                         <option value="<?php echo $dpt['DEPARTMENT_ID']; ?>" <?php if($dpt['DEPARTMENT_ID'] == $appraisal['0']['DEPARTMENT_ID']) { echo "selected";}?>><?php echo $dpt['DEPARTMENT_NAME']; ?></option>
                         <?php } ?>
                </select> 
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>New Designation:</label>
			   <select name="NEW_DESIGNATION" class="form-control">
                    <option value="">Select Designation</option>
                        <?php foreach($designation as $dsg){ ?>
                        <option value="<?php echo $dsg['CONTROLFILE_ID']; ?>" <?php echo ($dsg['CONTROLFILE_ID'] == $appraisal['0']['NEW_DESIGNATION']) ? ' selected="selected"' : '';?>><?php echo $dsg['CONTROLFILE_VALUE']; ?></option>
                    	<?php } ?>
                </select>
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>New Salary:</label>
			    <input class="form-control" name="new_salary" id="new_salary" value="<?php echo $appraisal['0']['NEW_SALARY'];?>" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>

	</div>	
	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group">
			    <label>New CTC: <span class="text-danger">*</span></label>
			    <input class="form-control" name="NEW_CTC" id="NEW_CTC" value="<?php echo $appraisal['0']['CTC'];?>" type="text"> 
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>New Basic:</label>
			    <input class="form-control" name="NEW_BASIC" id="NEW_BASIC" value="<?php echo $appraisal['0']['BASIC'];?>" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group">
			    <label>New HRA :</label>
			    <input class="form-control" name="NEW_HRA" id="NEW_HRA" value="<?php echo $appraisal['0']['HRA'];?>" type="text"> 
			    <span class="text-danger"></span>
			</div>	
		</div>

	</div>	
	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group">
			    <label>Remarks <span class="text-danger">*</span></label>
			  <div ><textarea class="form-control" name="remarks" ><?php echo $appraisal['0']['REMARKS']?></textarea></div>
			</div>	
		</div>
	</div>	

	<div class="col-md-12">
		<div class="col-sm-4">
			<div class="form-group" align="center">
			    <button name="submit" id="submit" class="btn btn-primary">Update</button> 
			</div>	
		</div>
		<div class="col-sm-4">
			<div class="form-group" align="center">
			    <button name="reset" id="reset" class="btn btn-primary">Reset</button> 
			</div>	
		</div>
	</div>
</div>
<?php echo form_close();?>
</div>
</div>
</div>
</div>
</div>
