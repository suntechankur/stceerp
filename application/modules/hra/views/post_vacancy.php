<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
    <div class="create_batch_form">
        <div id="box">
            <h2>Post Vacancy Details</h2></div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading"> </div>
                    <div class="panel-body">
                        <?php echo form_open(); ?>   
                                <?php
                                echo $msg;
                                ?>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                               
                                                <label>Designation:</label>
                                                <input class="form-control" type="text" value="<?php echo $this->input->post('DESIGNATION');?>" maxlength="50" name="DESIGNATION" id="DESIGNATION">
                                                <span class="text-danger"><?php echo form_error('DESIGNATION'); ?></span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>No of Positions:</label>
                                                <input class="form-control" type="text" value="<?php echo $this->input->post('NO_OF_POSITIONS');?>" maxlength="50" name="NO_OF_POSITIONS" id="NO_OF_POSITIONS">
                                                <span class="text-danger"><?php echo form_error('NO_OF_POSITIONS'); ?></span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Skills: </label><br />
                                                
                                                <select class="form-control course_interested" name="SKILL[]" id="SKILL" multiple>
                                                <?php 

                                                    foreach($computer_skill as $skill){

                                                  if (in_array($skill['EMPLOYEE_SKILL_ID'], $this->input->post('SKILL')))
                                                  {
                                                  $selected = "selected";
                                                  }
                                                else
                                                  {
                                                    $selected = "";
                                                  }
                                                     ?>
                                                    <option value="<?php echo $skill['EMPLOYEE_SKILL_ID']; ?>" <?php echo $selected;?>><?php echo $skill['EMPLOYEE_SKILL']; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('SKILL[]'); ?></span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-2">
                                                <label>Experience From:</label>
                                                  <select name="EXPERIENCE_FROM" class="form-control">
                                                    <option value="0" <?php echo ($this->input->post('EXPERIENCE_FROM')==0) ? 'selected' : '';?>>0</option>
                                                    <option value="1" <?php echo ($this->input->post('EXPERIENCE_FROM')==1) ? 'selected' : '';?>>1</option>
                                                    <option value="2" <?php echo ($this->input->post('EXPERIENCE_FROM')==2) ? 'selected' : '';?>>2</option>
                                                    <option value="3" <?php echo ($this->input->post('EXPERIENCE_FROM')==3) ? 'selected' : '';?>>3</option>
                                                    <option value="4" <?php echo ($this->input->post('EXPERIENCE_FROM')==4) ? 'selected' : '';?>>4</option>
                                                    <option value="5" <?php echo ($this->input->post('EXPERIENCE_FROM')==5) ? 'selected' : '';?>>5</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <label>To:</label>
                                                 <select name="EXPERIENCE_TO" class="form-control">
                                                    <option value="0" <?php echo ($this->input->post('EXPERIENCE_TO')==0) ? 'selected' : '';?>>0</option>
                                                    <option value="1" <?php echo ($this->input->post('EXPERIENCE_TO')==1) ? 'selected' : '';?>>1</option>
                                                    <option value="2" <?php echo ($this->input->post('EXPERIENCE_TO')==2) ? 'selected' : '';?>>2</option>
                                                    <option value="3" <?php echo ($this->input->post('EXPERIENCE_TO')==3) ? 'selected' : '';?>>3</option>
                                                    <option value="4" <?php echo ($this->input->post('EXPERIENCE_TO')==4) ? 'selected' : '';?>>4</option>
                                                    <option value="5" <?php echo ($this->input->post('EXPERIENCE_TO')==5) ? 'selected' : '';?>>5</option>
                                                    <option value="6" <?php echo ($this->input->post('EXPERIENCE_TO')==6) ? 'selected' : '';?>>6</option>
                                                    <option value="onwards">onwards</option>
                                                </select>
                                            </div>
                                            
                                            <div class="col-lg-4">
                                                <label>Salary</label>
                                                 <input class="form-control" type="text" value="<?php echo $this->input->post('SALARY');?>" maxlength="20" name="SALARY" id="SALARY">
                                                 <span class="text-danger"><?php echo form_error('SALARY'); ?></span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Centre:</label>
                                                <select name="CENTRE_ID" class="form-control">
                                                   <option value=""></option>
                                                   <?php foreach($centres as $centre){ ?>
                                                        <option value="<?php echo $centre['CENTRE_ID']; ?>" <?php echo ($this->input->post('CENTRE_ID')==$centre['CENTRE_ID']) ? 'selected' : '';?> ><?php echo $centre['CENTRE_NAME']; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="text-danger"><?php echo form_error('CENTRE_ID'); ?></span>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                        <div class="col-lg-12">
                                        
                                              <label>Other Details</label>
                                              <input class="form-control" type="textarea" value="<?php echo $this->input->post('OTHER_DETAILS');?>"  name="OTHER_DETAILS" id="OTHER_DETAILS">                                         
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-lg-4">
                                            </div>
                                            <div class="col-lg-2">
                                               <br>
                                                <button type="submit" class="btn btn-primary">SAVE</button>
                                               
                                            </div>
                                            <div class="col-lg-2">
                                                <br>
                                                <button type="reset" name="reset" value="reset" id="reset" class="btn btn-primary">RESET</button>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                                               
                            
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    
        
        <div id="box"> 
            <h2 class="text-center"> Vacancies Created</h2>
            <div class="panel panel-default">
                <div class="panel-body table-responsive">
                    <table class="table table-bordered table-condensed table-stripped">
                     <thead>
                        <tr>
                            <th>Action</th>
                            <th>Date</th>
                            <th>Designation</th>
                            <th>Salary</th>
                            <th>Other Details</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php
                             
                      foreach ($vacancyData as $key => $value) {
                     ?>
                        <tr>
                        <?php if($value['IS_ACTIVE'] == '1'){ ?>
                            <td style="background-color:#D5F5E3"><a href="<?php echo base_url('hra/close_vacancy/'.$value['VACANCY_ID']) ?>">Close</a></td>
                         <?php   }
                            else{
                                echo "<td style='background-color:#F5B7B1'>Closed</td>";
                            }
                         ?>       
                           
                            <td ><?php echo date("d/m/Y",strtotime($value['CREATED_DATE']));?></td>
                            <td><?php echo $value['DESIGNATION']?></td>
                            <td><?php echo $value['SALARY']?></td>
                            <td><?php echo $value['OTHER_DETAILS']?></td>
                        </tr>
                        <?php } ?>
                     </tbody>
                    </table>
                </div>
            </div>
           
       </div>
    </div>
