
<style>
time.icon
{
  font-size: 1em; /* change icon size */
  display: block;
  position: relative;
  width: 7em;
  height: 7em;
  background-color: #fff;
  margin: 2em auto;
  box-shadow: 0 1px 0 #bdbdbd, 0 2px 0 #fff, 0 3px 0 #bdbdbd, 0 4px 0 #fff, 0 5px 0 #bdbdbd, 0 0 0 1px #bdbdbd;
  overflow: hidden;
  -webkit-backface-visibility: hidden;
  -webkit-transform: rotate(0deg) skewY(0deg);
  -webkit-transform-origin: 50% 10%;
  transform-origin: 50% 10%;
}

time.icon *
{
  display: block;
  width: 100%;
  font-size: 1em;
  font-weight: bold;
  font-style: normal;
  text-align: center;
}

time.icon strong
{
  position: absolute;
  top: 0;
  padding: 0.4em 0;
  color: #fff;
  background-color: #e31e24;/*#fd9f1b;*/
  border-bottom: 1px dashed white;/* #f37302;*/
  box-shadow: 0 2px 0 #e31e24;/*#fd9f1b;*/
}

time.icon em
{
  position: absolute;
  bottom: 0.3em;
  color: #e31e24;/*#fd9f1b;*/
}

time.icon span
{
  width: 100%;
  font-size: 2.8em;
  letter-spacing: -0.05em;
  padding-top: 0.8em;
  color: #2f2f2f;
}

time.icon:hover, time.icon:focus
{
  -webkit-animation: swing 0.6s ease-out;
  animation: swing 0.6s ease-out;
}

.status{
  font-size: 30px;
  text-align:center;
  margin:1em auto;
}
.approval_icon
{
  border:5px solid green;
  color:green;
  padding:2px;
  width:200px;
  -webkit-transform: rotate(-10deg);
  -moz-transform: rotate(-10deg);
  -o-transform: rotate(-10deg);
  -ms-transform: rotate(-10deg);
  transform: rotate(-10deg);
}

.disapproval_icon{
  border:5px solid red;
  color:red;
  padding:2px;
  width:250px;
  -webkit-transform: rotate(-10deg);
  -moz-transform: rotate(-10deg);
  -o-transform: rotate(-10deg);
  -ms-transform: rotate(-10deg);
  transform: rotate(-10deg);
}

</style>
<script type="text/javascript" src="<?php echo base_url('resources/js/'); ?>timedropper.min.js"></script>
<link href="<?php echo base_url('resources/css/'); ?>timedropper.min.css" rel="stylesheet">
<div class="gapping"></div>
<div class="create_batch_form">
  <div id="box">
  <?php
  $employee_name = "";
  $employee_id = "";
  $biometric_id = "";
  foreach($employee_details as $details){
    $employee_name = $details['EMPLOYEE_NAME'];
    $employee_id = $details['EMPLOYEE_ID'];
    $biometric_id = $details['BIOMETRIC_DEVICE_ID'];
  }?>
  <h2>Update Attedance of <?php echo $employee_name;?></h2>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
        <!-- start message area -->

        <?php if($this->session->flashdata('1')){ ?>
        <div class="alert alert-danger">
        <?php echo "Please select From Date and To Date"; ?>
        </div>
        <?php } ?>

        <!-- End message area -->

        </div>
        <div class="panel-body">
          <?php echo form_open();?>
          <div class="col-md-12">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-4">
                  <label>From Date: <span class="text-danger">*</span></label>
                  <input type='textbox' class='form-control enquiry_date' name='from_date' value="" required>
                </div>
                <div class="col-lg-4">
                  <label>To Date: <span class="text-danger">*</span></label>
                  <input type='textbox' class='form-control enquiry_date' name='to_date' value="" required>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-4">
                </div>
                <div class="col-lg-4">
                  <button type="submit" class="btn btn-primary" style="width:143px;" name="get_attendance">Get Leaves report</button> &nbsp;
                  <button type="submit" class="btn btn-primary" name="reset_attendance">Reset</button> &nbsp;
                </div>
                <div class="col-lg-4">

                </div>
              </div>
            </div>
          </div>
          <?php echo form_close();?>
        </div>
      </div>
    </div>
  </div>
</div>
<br/>

<?php if(!empty($attendance_details)){?>
<div class="create_batch_form">
  <div id="box">
  <h2>Leave Reports</h2>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-body table-responsive">
          <table class="table table-bordered">
            <thead class="thead-light">
              <tr>
                <th>Sr No.</th>
                <th>Employee Detail</th>
                <th>Date Details</th>
                <th>Update Dates</th>
                <th>Approve / Not Approve</th>
              </tr>
          </thead>
          <tbody>
              <?php
              $i = 1;
                foreach($attendance_details as $details){
                 $day_name = date('l',strtotime($details['date']));
                 $month_year = date('M / Y',strtotime($details['date']));
                 $day_date = date('d',strtotime($details['date']));

                 if($details['status'] == "approved"){
                   $status = '<div class="status approval_icon"><div style="border:1px solid green;"><strong>Approved</strong></div></div>';
                   $approval_attribute = "data-approval_id='".$details['approval_id']."'";
                 }
                 elseif($details['status'] == "disapproved"){
                   $status = '<div class="status disapproval_icon"><div style="border:1px solid red;"><strong>Not Approved</strong></div></div>';
                   $approval_attribute = "data-approval_id='".$details['approval_id']."'";
                 }
                 else{
                   $status = '<div class="status" style="color:red;font-size:20px;width:250px;margin:2em auto;"><strong>Kindly contact with HR.</strong></div>';
                   $approval_attribute = "data-approval_id=''";
                 }
                 $date_data =  '<time class="icon"><em>'.$day_name.'</em><strong>'.$month_year.'</strong><span>'.$day_date.'</span></time>';
                 echo "<tr style='font-weight:bold;'><td>".$i++."</td><td>".$employee_name." (".$biometric_id.") "."</td><td>".$date_data."</td><td>In Time: <input type='text' class='alarm' placeholder='Select In Time'> & Out Time: <input type='text' class='alarm' placeholder='Select Out Time'></td><td class='approval_actions'><span class='fa fa-2x fa-calendar-check-o' style='color:green;cursor:pointer;' title='Approve Leave' data-type='approved' ".$approval_attribute." data-employee_id=".$employee_id." data-biometric_id=".$biometric_id." data-date='".date('Y-m-d',strtotime($details['date']))."'></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='fa fa-2x fa-ellipsis-v'></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='fa fa-2x fa-calendar-times-o' title='DisApprove Leave' style='color:red;cursor:pointer;'data-type='disapproved' ".$approval_attribute." data-employee_id=".$employee_id." data-biometric_id=".$biometric_id." data-date='".date('Y-m-d',strtotime($details['date']))."'></span></td></tr>";
                }?>
          </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php }?>
<script>$( ".alarm" ).timeDropper();</script>
