<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
    <div class="create_batch_form">
        <div id="box">
            <h2>Control File Search</h2></div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align:center; color:#F00;"> </div>
                        <div class="panel-body">
                            <?php echo form_open(); ?>   

                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-4">
                                            <label>Control File Key:</label>
                                            <input class="form-control" type="text" value="<?php if(isset($update_controls_data['control_file_key'])){ echo $update_controls_data['control_file_key']; }?>" maxlength="50" name="control_file_key" id="FIRSTNAME">
                                        </div>
                                        <div class="col-lg-4">
                                            <label>Control File Value:</label>
                                            <input class="form-control" type="text" value="<?php if(isset($update_controls_data['control_file_value'])){ echo $update_controls_data['control_file_value']; }?>" maxlength="50" name="control_file_value" id="LASTNAME">
                                        </div>
                                        <div class="col-lg-4">
                                            <label>Description:</label>
                                            <input class="form-control" type="text" value="<?php if(isset($update_controls_data['description'])){ echo $update_controls_data['description']; }?>" maxlength="50" name="description" id="LASTNAME">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-4">
                                            <br>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary" name="search_controls">Search</button>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" name="reset_controls" class="btn btn-primary" style="float:left">Reset</button>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php echo form_close(); ?>
                        </div>
                        </div>
                    </div>
            </div>
    </div>
    <?php if($this->session->userdata('update_controls_data')){?>
        <br/>
        <div id="box"><h2></h2></div>
        <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Action</th>
                                <th>Control File Id</th>
                                <th>Control File Key</th>
                                <th>Control File Value</th>
                                <th>Control File Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($searched_data as $controls){?>
                            <tr>
                                <td><span class="glyphicon glyphicon-edit edit_control_file_data" data-control-id="<?php echo $controls['CONTROLFILE_ID']; ?>" data-control-key="<?php echo $controls['CONTROLFILE_KEY']; ?>" data-control-value="<?php echo $controls['CONTROLFILE_VALUE']; ?>" data-control-description="<?php echo $controls['CONTROLFILE_DESCRIPTION']; ?>" data-toggle="modal" data-target="#controls" title="Edit"></span></td>
                                <td><?php echo $controls['CONTROLFILE_ID'];?></td>
                                <td><?php echo $controls['CONTROLFILE_KEY'];?></td>
                                <td><?php echo $controls['CONTROLFILE_VALUE'];?></td>
                                <td><?php echo $controls['CONTROLFILE_DESCRIPTION'];?></td>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    <?php }?>
</div>


<div id="controls" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Control File Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" name="control_id" id="control_id">
                    <input type="hidden" name="control_key" id="control_key">
                    <input type="hidden" name="control_value" id="control_value">
                    <input type="hidden" name="description" id="description">
                    <div class="col-md-4">
                        <div class="form-group">    
                            <label>Control File Key :</label>
                            <input class="form-control modal_control_key" id="modal_control_key" type="text" name="control_key">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">    
                            <label>Value :</label>
                            <input class="form-control modal_control_value" id="modal_control_value" type="text" name="control_value">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">    
                            <label>Description :</label>
                            <input class="form-control modal_control_description" id="modal_control_description" type="text" name="description">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-4">
                            <button type="submit" id="save_control_file_data" class="btn btn-primary" style="float:left">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>