<?php foreach ($employee as $key => $value) {
    ?>
    
<div class="gapping"></div>
<div class="create_batch_form">
    <div id="box">
        <h2>Update Employee</h2>
        
        </div>
    <div class="row">
    
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!-- start message area -->

            <?php if($this->session->flashdata('danger')) { ?>
                        <div class="alert alert-danger">
              <strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
              <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('newBatchId')) { ?>
                        <div class="alert alert-success">
              <?php echo "New Batch ID Generated: " . $this->session->flashdata('newBatchId'); ?>
            </div>
            <?php } ?>
                             
                        
            <?php if($this->session->flashdata('failed')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
            </div>
            <?php } ?>
  
            <?php if($this->session->flashdata('info1')) { ?>
                        <div class="alert alert-info">
              <strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
            </div>
            <?php } ?>

            <?php if($this->session->flashdata('warning')) { ?>
                        <div class="alert alert-warning">
              <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
            </div>
            <?php } ?>

 <!-- End message area --> 

                </div>
                <div class="panel-body">
                   <?php 
                    $attr = array('role'=>'form');
                    echo form_open('',$attr); ?>
<!--                    <form role="form" data-toggle="validator" method="post" action="lib/save.php" novalidate="true">-->
                        <div class="col-md-6">
                        
                            <label for=""><h4>PERSONAL DETAILS</h4></label>
                        
                            <div class="form-group">
                                <label>First Name: <span class="text-danger">*</span></label>
                                <input class="form-control" name="EMP_FNAME" value="<?php echo ($value['EMP_FNAME']) ? $value['EMP_FNAME'] : '';?>" id="EMP_FNAM" type="text" required=""> 
                                <span class="text-danger"><?php echo form_error('EMP_FNAME'); ?></span>
                                </div>
                            <div class="form-group">
                                <label>Middle Name:</label>
                                <input class="form-control" name="EMP_MIDDLENAME" value="<?php echo ($value['EMP_MIDDLENAME']) ? $value['EMP_MIDDLENAME'] : '';?>" id="EMP_MIDDLENAME" type="text">
                                <span class="text-danger"><?php echo form_error('EMP_MIDDLENAME'); ?></span>
                                 </div>

                            <div class="form-group">
                                <label>Last Name: <span class="text-danger">*</span></label>
                                <input class="form-control" name="EMP_LASTNAME" value="<?php echo ($value['EMP_LASTNAME']) ? $value['EMP_LASTNAME'] : '';?>" id="EMP_LASTNAME" type="text" required=""> 
                                <span class="text-danger"><?php echo form_error('EMP_LASTNAME'); ?></span>
                                </div>
                            <div class="form-group">
                                 <div class="row">
                                    
                                        <div class="col-lg-12">
                                            <label>E-mail: <span class="text-danger"></span></label>
                                            <input class="form-control" name="EMP_EMAIL" id="EMP_EMAIL" value="<?php echo ($value['EMP_EMAIL']) ? $value['EMP_EMAIL'] : '';?>" type="text"> 
                                            <span class="text-danger"><?php echo form_error('EMP_EMAIL'); ?></span>
                                            </div>
                                        
                                   
                                </div>
                            </div>

<div class="form-group">
 <label>Gender: <span class="text-danger">*</span></label>
 <div class="row">

                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input id="GENDER" name="GENDER" value="F"<?php echo $value['GENDER']=='F' ? "checked=checked" : '';?> type="radio" required="">Female
                                </label>
                            </div>
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input id="GENDER" name="GENDER" value="M"<?php echo $value['GENDER']=='M' ? "checked=checked" : '';?> type="radio" required="">Male
                                </label>
                            </div>
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input id="GENDER" name="GENDER" value="U"<?php echo $value['GENDER']=='U' ? "checked=checked" : '';?> type="radio" required="">Unknown
                                </label>
                            </div>
                        </div>
</div>

<div class="form-group">
                            <div class="row">
                                    
                                        <div class="col-lg-6">
                                            <label>Address1: <span class="text-danger">*</span></label>
                                            <input name="ADDRESS1" value="<?php echo $value['ADDRESS1'] ? $value['ADDRESS1'] : ''; ?>" class="form-control">
                                                 
                                            <span class="text-danger"><?php echo form_error('ADDRESS1'); ?></span>
                                        </div>
                                    
                                        <div class="col-lg-6">
                                            <label>Address2: <span class="text-danger"></span></label>
                                            <input name="ADDRESS2" value="<?php echo $value['ADDRESS2'] ? $value['ADDRESS2'] : ''; ?>" class="form-control">
                                            <span class="text-danger"><?php echo form_error('ADDRESS2'); ?></span>
                                        </div>
                                    
                                    </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                        <div class="col-lg-6">
                                            <label>City: <span class="text-danger">*</span></label>
                                            <select name="CITY" class="form-control source_master" required="">
                                                <option value="">Please Select City</option>
                                                <?php foreach($cities as $city){ ?>
                                                <option value="<?php echo $city['CITY_ID'] ?>"<?php echo $city['CITY_ID']==$value['CITY'] ? "selected=selected" : '';?>><?php echo $city['CITY_NAME'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('CITY'); ?></span>
                                        </div>
                                    
                                        <div class="col-lg-6">
                                            <label>Zip Code:</label>
                                            
                                                <input name="ZIP" value="<?php echo $value['ZIP'] ? $value['ZIP'] : ''; ?>" class="form-control">
                                            
                                            <span class="text-danger"><?php echo form_error('ZIP'); ?></span>
                                        </div>
                                    
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="row">
                                        <div class="col-lg-6">
                                            <label>Suburb: <span class="text-danger">*</span></label>
                                            <select name="SUBURB" class="form-control">
                                                <option value="">Please Select Suburb</option>

                                                <option value="Central"<?php echo $value['SUBURB']=="Central" ? "selected=selected" : '';?>>Central</option>

                                                <option value="Western"<?php echo $value['SUBURB']=="Western" ? "selected=selected" : '';?>>Western</option>

                                                <option value="Horbour"<?php echo $value['SUBURB']=="Horbour" ? "selected=selected" : '';?>>Horbour</option>
                                            
                                            </select>
                                            <span class="text-danger"><?php echo form_error('SUBURB'); ?></span>
                                        </div>
                                    
                                        <div class="col-lg-6">
                                            <label>State: <span class="text-danger">*</span></label>
                                            <select name="STATE" class="form-control" required="">
                                                <option value="">Please Select State</option>
                                                <?php foreach($states as $state){ ?>
                                                <option value="<?php echo $state['STATE_ID'] ?>"<?php echo $state['STATE_ID']==$value['STATE'] ? "selected=selected" : '';?>><?php echo $state['STATE_NAME'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('STATE'); ?></span>
                                        </div>
                                            
                                    </div>
                                </div>
                            
                            <div class="form-group">
                                <div class="row">
                                        
                                    
                                        

                                        <div class="col-md-6">
                                            <label>Contact No.1: <span class="text-danger">*</span></label>
                                                        <input name="EMP_MOBILENO" value="<?php echo $value['EMP_MOBILENO'] ? $value['EMP_MOBILENO'] : ''; ?>" class="form-control" required="">
                                            
                                            <span class="text-danger"><?php echo form_error('EMP_MOBILENO'); ?></span>
                                        </div>

                                        <div class="col-md-6">
                                           
                                                        <label>Contact No.2:</label>
                                                        <input name="EMP_TELEPHONE" value="<?php echo $value['EMP_TELEPHONE'] ? $value['EMP_TELEPHONE'] : ''; ?>" class="form-control">
                                            
                                            <span class="text-danger"><?php echo form_error('EMP_TELEPHONE'); ?></span>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">

                                    <div class="col-md-6">
                                            
                                        <label>Pan Card No.: <span class="text-danger"></span></label>
                                            <input class="form-control" name="PAN_CARD_NO" id="PAN_CARD_NO" value="<?php echo $value['PAN_CARD_NO'] ? $value['PAN_CARD_NO'] : ''; ?>" type="text"> 
                                            <span class="text-danger"><?php echo form_error('PAN_CARD_NO'); ?></span>
                                    </div>

                                        
                                    
                                        <div class="col-md-6">
                                           
                                                        <label>Date of birth(mm/dd/yyyy): <span class="text-danger">*</span></label>
                                                        <input name="DATEOFBIRTH" value="<?php echo $value['DATEOFBIRTH'] ? date('d/m/Y',strtotime($value['DATEOFBIRTH'])) : ''; ?>" class="form-control enquiry_date" required="">
                                            
                                            <span class="text-danger"><?php echo form_error('DATEOFBIRTH'); ?></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                    <div class="col-lg-6">
                                            <label>Current Qualification: <span class="text-danger">*</span></label>
                                            <select name="QUALIFICATION" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="Xth Pass"<?php echo $value['QUALIFICATION']=="Xth Pass" ? "selected=selected" : '';?>>Xth Pass </option>
                                                <option value="XIIth Pass"<?php echo $value['QUALIFICATION']=="XIIth Pass" ? "selected=selected" : '';?>>XIIth Pass </option>
                                                <option value="Under Graduate"<?php echo $value['QUALIFICATION']=="Under Graduate" ? "selected=selected" : '';?>>Under Graduate </option>
                                                <option value="Graduate"<?php echo $value['QUALIFICATION']=="Graduate" ? "selected=selected" : '';?>>Graduate </option>
                                                <option value="Post Graduate"<?php echo $value['QUALIFICATION']=="Post Graduate" ? "selected=selected" : '';?>>Post Graduate </option>
                                                <option value="Other"<?php echo $value['QUALIFICATION']=="Other" ? "selected=selected" : '';?>>Other </option>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('QUALIFICATION'); ?></span>
                                        </div>

                                                                           
                                        <div class="col-md-6">
                                           
                                            <label>Stream: <span class="text-danger">*</span></label>
                                            <select name="STREAM" class="form-control" required="">
                                                <option value="">Please Select</option>
                                                <?php foreach($streams as $stream){ ?>
                                                <option value="<?php echo $stream['CONTROLFILE_ID'] ?>"<?php echo $value['STREAM']==$stream['CONTROLFILE_ID'] ? "selected=selected" : '';?>><?php echo $stream['CONTROLFILE_VALUE'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('STREAM'); ?></span>
                                        </div>
                                    </div>
                                </div>



                            <div id='TextBoxesGroup'>
            <div id='TextBoxDiv'>
        <div class="form-group"> 
        <div class='row'>
        <div class="col-md-12">
        
            <div class='select-box'><br>
            <label>References : </label> &nbsp;
               <span class="glyphicon glyphicon-plus text-info handCursor" id='addButton'></span>
               <span class="glyphicon glyphicon-minus text-danger handCursor" id='removeButton'></span>
            </div>

        </div>
        </div>
        <div class="form-group"> 
        <div class="row">
        <div class='col-md-12'>
        <?php
        $REF_NAME = explode(",", $value['REF_NAME']);
        $REF_RELATION = explode(",", $value['REF_RELATION']);
        $REF_CONTACT = explode(",", $value['REF_CONTACT']);
        
        foreach ($REF_NAME as $k => $v) { 
        ?>
                <div class='col-md-4'>
                    <label>Name : </label><input type='textbox' value="<?php echo $value['REF_NAME'] ? $v : ''; ?>" class='form-control' name='ref_name[]' id='textbox1' >
                </div>

                <div class='col-md-4'>
                    <label>Relation : </label><input type='textbox' value="<?php echo $value['REF_RELATION'] ? $REF_RELATION[$k] : ''; ?>" class='form-control' name='ref_relation[]' id='textbox2' >
                </div>

                <div class='col-md-4'>
                    <label>Contact No.: </label><input type='textbox' value="<?php echo $REF_CONTACT[$k] ? $REF_CONTACT[$k] : ''; ?>" class='form-control' name='ref_contact[]' id='textbox3' >
                </div>
            

            <?php }?>
            <!-- <div class="col-md-4"></div> -->
                </div>
                </div>
                </div>
            </div>
            </div>
            </div>
            
            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Computer Skills: <span class="text-danger">*</span></label><br />
                                        <select class="form-control course_interested" name="SKILL[]" id="SKILL" multiple required="">
                                        <?php 
                                            $testSKILL = explode(",",$value['SKILL']);
                                            foreach($computer_skill as $skill){ 
                                                
                                                ?>
                                            <option value="<?php echo $skill['EMPLOYEE_SKILL_ID']; ?>"<?php echo in_array($skill['EMPLOYEE_SKILL_ID'],$testSKILL) ? "selected='selected'" : "";?>><?php echo $skill['EMPLOYEE_SKILL']; ?></option>
                                            <?php } ?>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('SKILL[]'); ?></span>
                                    </div>
                                    <div class="row">
                                   <div class="col-md-12">
                                       <ul class="list-group courses_seld"></ul>
                                   </div>
                                </div>
                                </div>
                                <div class="col-md-6">
                                        <label class="form-check-inline">
                                              &nbsp;&nbsp; </label>


                                    
                                </div>
                            </div>

    </div>
                <div class="col-md-6">
                    <label for=""><h4>COMPANY DETAILS</h4></label>
                    

                    <div class="form-group">
                        <div class="row">
                             <div class="col-md-6">
                                
                                    <label>Date of joining(dd/mm/yyyy): <span class="text-danger">*</span></label>
                                    <input class="form-control enquiry_date" name="DATEOFJOIN" value="<?php echo $value['DATEOFJOIN'] ? date('d/m/Y',strtotime($value['DATEOFJOIN'])) : ''; ?>" type="text" required=""> 
                                    <span class="text-danger"><?php echo form_error('DATEOFJOIN'); ?></span>
                                </div>
                            
                            <div class="col-md-6">

                                   <label>Current Designation: <span class="text-danger">*</span></label>
                                        <select name="CURRENT_DESIGNATION" class="form-control" required="">
                                            <option value="">Please Select</option>
                                            <?php
                                            
                                             foreach($designations as $designation){ 
                                            
                                                ?>
                                            <option value="<?php echo $designation['CONTROLFILE_ID'] ?>"<?php echo $value['CURRENT_DESIGNATION']==$designation['CONTROLFILE_ID'] ? "selected=selected" : '';?>><?php echo $designation['CONTROLFILE_VALUE'] ?></option>
                                            <?php } ?>
                                        </select>
                                        <span class="text-danger"><?php echo form_error('CURRENT_DESIGNATION'); ?></span>
                                
                            </div>
                        </div>
                    </div>


                        <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Current Salary: <span class="text-danger">*</span></label>
                                                    <input name="CURRENT_SALARY" value="<?php echo $value['CURRENT_SALARY'] ? $value['CURRENT_SALARY'] : ''; ?>" class="form-control" required="">
                                        
                                        <span class="text-danger"><?php echo form_error('CURRENT_SALARY'); ?></span>
                                    </div>
                                
                                    <div class="col-md-6">
                                       
                                                    <label>Bank A/C No.:</label>
                                                    <input name="BANKACCOUNTNO" value="<?php echo $value['BANKACCOUNTNO'] ? $value['BANKACCOUNTNO'] : ''; ?>" class="form-control">
                                        
                                        <span class="text-danger"><?php echo form_error('BANKACCOUNTNO'); ?></span>
                                    </div>
                                </div>
                            </div>


                        <div class="form-group">
                            <div class="row">
                             <div class="col-md-6">
                                
                                    <label>Bank & Branch Name:</label>
                                    

                                    <select name="BRANCH_ID" class="form-control">
                                            <option value="">Please Select</option>
                                            <?php foreach($bankBranches as $bankBranche){ ?>
                                            <option value="<?php echo $bankBranche['BRANCH_ID'] ?>"<?php echo $value['BRANCH_ID']==$bankBranche['BRANCH_ID'] ? "selected=selected" : '';?>><?php echo "SVC(".$bankBranche['BRANCH_NAME'].")" ?></option>
                                            <?php } ?>
                                        </select>
                                    <span class="text-danger"><?php echo form_error('BRANCH_ID'); ?></span>
                                </div>
                            
                            <div class="col-md-6">
                               
                                   <label>P/F No.:</label>
                                       <input class="form-control" value="<?php echo $value['EMPLOYEEPFNO'] ? $value['EMPLOYEEPFNO'] : ''; ?>" name="EMPLOYEEPFNO" type="text"> 
                                        <span class="text-danger"><?php echo form_error('EMPLOYEEPFNO'); ?></span>
                                
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                            <div class="row">
                            <div class="col-md-6">
                                    
                                <label>Biometric Device ID:</label>
                                <input class="form-control"  value="<?php echo $value['BIOMETRIC_DEVICE_ID'] ? $value['BIOMETRIC_DEVICE_ID'] : ''; ?>" name="BIOMETRIC_DEVICE_ID" name="BIOMETRIC_DEVICE_ID" type="text">
                                <span class="text-danger"><?php echo form_error('BIOMETRIC_DEVICE_ID'); ?></span>
                            </div>
                            
                            <div class="col-md-6">
                               
                                   <label>Official E-mail:</label>
                                       <input class="form-control" value="<?php echo $value['EMP_OFFICIAL_EMAIL'] ? $value['EMP_OFFICIAL_EMAIL'] : ''; ?>" name="EMP_OFFICIAL_EMAIL" name="EMP_OFFICIAL_EMAIL" type="text"> 
                                        <span class="text-danger"><?php echo form_error('EMP_OFFICIAL_EMAIL'); ?></span>
                                
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                     <label>Others:</label>
                     <div class="row">

                        <div class="col-sm-4">
                            
                                <input name="TA_GIVEN" type="checkbox" value="1"<?php echo $value['TA_GIVEN']=="1" ? 'checked=checked' : '';?>> Tea Allowance Given
                            
                        </div>
                        <div class="col-sm-5">
                            
                                <input name="PF_APPLICABLE" type="checkbox" value="1"<?php echo $value['PF_APPLICABLE']=="1" ? 'checked=checked' : '';?>> PF Deduction Applicable
                            
                        </div>
                        <div class="col-sm-3">
                            
                                <input name="ISACTIVE" type="checkbox" value="1"<?php echo $value['ISACTIVE']=="1" ? 'checked=checked' : '';?> checked="checked"> Is Active
                            
                        </div>
                        </div>
                    </div>

                    
                    <div class="form-group">
                            <div class="row">
                            <div class="col-md-6">
                                    
                                <label>Department:</label>
                                
                                    <select name="DEPARTMENT_ID" class="form-control">
                                            <option value="">Please Select</option>
                                            <?php foreach($departments as $department){ ?>
                                            <option value="<?php echo $department['DEPARTMENT_ID'] ?>"<?php echo $value['DEPARTMENT_ID']==$department['DEPARTMENT_ID'] ? "selected=selected" : '';?>><?php echo $department['DEPARTMENT_NAME'] ?></option>
                                            <?php } ?>
                                    </select>

                                <?php echo form_error('DEPARTMENT_ID'); ?></span>
                            </div>
                            
                            <div class="col-md-6">
                               
                               <label>UA Number:</label>
                                   <input class="form-control" value="<?php echo $value['UA_NUMBER'] ? $value['UA_NUMBER'] : ''; ?>" name="UA_NUMBER" type="text"> 
                                    <span class="text-danger">
                                        <?php echo form_error('UA_NUMBER'); ?>
                                    </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                            <div class="row">
                            <div class="col-md-6">
                                    
                                <label>Commitment:</label>
                                
                                <select name="COMMITMENTWITHEMPLOYEE" class="form-control">
                                        <option value="">Please Select</option>
                                        <option value="Certificates"<?php echo $value['COMMITMENTWITHEMPLOYEE']=="Certificates" ? "selected=selected" : '';?>>Certificates</option>
                                        <option value="1 year Bond"<?php echo $value['COMMITMENTWITHEMPLOYEE']=="1 year Bond" ? "selected=selected" : '';?>>1 year Bond</option>
                                        <option value="No Commitment"<?php echo $value['COMMITMENTWITHEMPLOYEE']=="No Commitment" ? "selected=selected" : '';?>>No Commitment</option>
                                        <option value="Others"<?php echo $value['COMMITMENTWITHEMPLOYEE']=="Others" ? "selected=selected" : '';?>>Others</option>
                                </select>

                                <span class="text-danger"><?php echo form_error('COMMITMENTWITHEMPLOYEE'); ?></span>
                            </div>
                            
                            <div class="col-md-6">
                               
                                   <label>Basic Salary:</label>
                                       <input class="form-control" name="BASIC" value="<?php echo $value['BASIC'] ? $value['BASIC'] : ''; ?>" type="text"> 
                                        <span class="text-danger"><?php echo form_error('BASIC'); ?></span>
                                
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                            <div class="row">
                            <div class="col-md-6">
                                    
                                <label>HRA:</label>
                                <input class="form-control" name="HRA" value="<?php echo $value['HRA'] ? $value['HRA'] : ''; ?>" type="text">
                                <span class="text-danger"><?php echo form_error('HRA'); ?></span>
                            </div>
                            
                            <div class="col-md-6">
                               
                                   <label>CTC:</label>
                                       <input class="form-control" name="CTC" value="<?php echo $value['CTC'] ? $value['CTC'] : ''; ?>" type="text"> 
                                        <span class="text-danger"><?php echo form_error('CTC'); ?></span>
                                
                            </div>
                        </div>
                    </div>

                    

                    
                    <div class="info"></div>

                    <label for=""> Employee Shift Timing </label> 
                    <span class="glyphicon glyphicon-plus text-info handCursor" id='addShift'></span> 
               <span class="glyphicon glyphicon-minus text-danger handCursor" id='removeShift'></span>

                    
                    <div id='ShiftGroup' class="ShiftGroup">
                    <div id='ShiftDiv' class="shiftDiv">


                        <?php
                   if($empShift)
                   {
                    $count = count($empShift);
                    foreach ($empShift as $k => $v) {
                    ?>
                   
                    <div id = <?php echo "shift".$k;?>>
                     <hr>
                    <div class="form-group">
                        <div class="row">
                        
                        

                        <div class="col-md-6">

                        <label>Center:</label>

                                    <select name="CENTRE_ID[]" class="form-control centre">
                                            <option value="">Please Select</option>
                                            <?php foreach($centres as $centre){ ?>
                                            <option value="<?php echo $centre['CENTRE_ID'] ?>"<?php echo $v['CENTRE']==$centre['CENTRE_ID'] ? "selected=selected" : '';?>><?php echo $centre['CENTRE_NAME'] ?></option>
                                            <?php } ?>
                                    </select>

                                <span class="text-danger"><?php echo form_error('CENTRE_ID'); ?></span>
                        </div>

                        <div class="col-md-6">
                            <input id="dynamic_timing" value="1" <?php echo
                             $v['DYNAMIC_TIMING']=="1" ? 'checked' : '';?> name="dynamic_timing[]" type="checkbox">    
                             <input type='hidden' value='0' name="dynamic_timing[]">
                            <label>Dynamic Timing (Timing is Frequently Changed)</label>       
                        </div>
                        </div> 
                    </div>
                    


                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                
                            </div>
                            <div class="col-md-5">
                            <!-- <label for="">Preferred Timing</label> -->
                                <label for="from_time">From</label>
                                    <input class="pref_time form-control" id="from_time" data-format="h:mm a" data-template="hh : mm a" name="from_time[]" value="<?php echo $v['FROM_TIMING'] ? $v['FROM_TIMING'] : '10:00 am'; ?>" type="text">
                            </div>
                            <div class="col-md-5">
                                    <label for="to_time">To &nbsp;&nbsp;&nbsp;</label>
                                    <input class="pref_time" id="to_time" data-format="h:mm a" data-template="hh : mm a" name="to_time[]" value="<?php echo $v['TO_TIMING'] ? $v['TO_TIMING'] : '06:30 pm'; ?>" type="text">
                            </div>

                        <div class="col-md-2 remove" divid = <?php echo "shift".$k;?>>
                            remove
                        </div>

                        </div>
                    </div>

                    
                    
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12 days_cont">        
                            <label>Days: </label> &nbsp;
                          <?php 

                          $DAYS = explode(",",$v['DAYS']);
                         ?>
                           <input type="checkbox" data-day="Mon" value="Mon"<?php echo (in_array("Mon",$DAYS)) ? "checked" : '';?> name="days<?php echo $count?>[]" class="days"> Mon &nbsp;&nbsp;
                            <input type="checkbox" data-day="Tue" value="Tue"<?php echo (in_array("Tue",$DAYS)) ? "checked" : '';?> name="days<?php echo $count?>[]" class="days"> Tue &nbsp;&nbsp;
<input type="checkbox" data-day="Wed" value="Wed"<?php echo (in_array("Wed",$DAYS)) ? "checked" : '';?> name="days<?php echo $count?>[]" class="days"> Wed &nbsp;&nbsp; 
<input type="checkbox" data-day="Thu" value="Thu"<?php echo (in_array("Thu",$DAYS)) ? "checked" : '';?> name="days<?php echo $count?>[]" class="days"> Thu &nbsp;&nbsp;
<input type="checkbox" data-day="Fri" value="Fri"<?php echo (in_array("Fri",$DAYS)) ? "checked" : '';?> name="days<?php echo $count?>[]" class="days"> Fri &nbsp;&nbsp;
<input type="checkbox" data-day="Sat" value="Sat"<?php echo (in_array("Sat",$DAYS)) ? "checked" : '';?> name="days<?php echo $count?>[]" class="days"> Sat &nbsp;&nbsp;
<input type="checkbox" data-day="Sun" value="Sun"<?php echo (in_array("Sun",$DAYS)) ? "checked" : '';?> name="days<?php echo $count?>[]" class="days"> Sun &nbsp;&nbsp;


                        </div>


                    </div>
                </div>
                </div>
               
<?php $count = $count -1; }

}
else{ ?>

<?php 
$centresId = $this->input->post('CENTRE_ID');
$dynamicTime = $this->input->post('dynamic_timing');
$centreCount = 1;
if ($centresId) {
$centreCount = count($centresId);
}
for ($i=0; $i <$centreCount ; $i++) { ?>
<div class="form-group">
<div class="row">



<div class="col-md-6">

<label>Centre:</label>

<select name="CENTRE_ID[]" class="form-control centre">
    <option value="">Please Select</option>
    <?php foreach($centres as $centre){ ?>
    <option value="<?php echo $centre['CENTRE_ID'] ?>" <?php echo ($centre['CENTRE_ID'] == $centresId[$i]) ? ' selected="selected"' : '';?>><?php echo $centre['CENTRE_NAME'] ?></option>
    <?php } ?>
</select>

<span class="text-danger"><?php echo form_error('CENTRE_ID'); ?></span>
</div>

<div class="col-md-6">

<input id="dynamic_timing" value="<?php echo $dynamicTime[$i]; ?>" <?php if($dynamicTime[$i] == '1'){echo 'checked';} ?> name="dynamic_timing[]" type="checkbox">
<input type='hidden' value='<?php echo $dynamicTime[$i]; ?>' name="dynamic_timing[]">    
<label>Dynamic Timing (Timing is Frequently Changed)</label>       
</div>
</div> 
</div>



<div class="form-group">
<div class="row">
<div class="col-md-12">

</div>
<div class="col-md-6">
<!-- <label for="">Preferred Timing</label> -->
<label for="from_time">From</label>
<input class="pref_time form-control" id="from_time" data-format="h:mm a" data-template="hh : mm a" name="from_time[]" value="10:00 am" type="text">
</div>
<div class="col-md-6">
<label for="to_time">To &nbsp;&nbsp;&nbsp;</label>
<input class="pref_time" id="to_time" data-format="h:mm a" data-template="hh : mm a" name="to_time[]" value="06:30 pm" type="text">
</div>

</div>
</div>



<div class="form-group">
<div class="row">
<div class="col-md-12 days_cont">        
<label>Days: </label> &nbsp;
<input type="checkbox" data-day="Mon" value="Mon" name="days1[]" class="days"> Mon &nbsp;&nbsp;
<input type="checkbox" data-day="Tue" value="Tue" name="days1[]" class="days"> Tue &nbsp;&nbsp;
<input type="checkbox" data-day="Wed" value="Wed" name="days1[]" class="days"> Wed &nbsp;&nbsp; 
<input type="checkbox" data-day="Thu" value="Thu" name="days1[]" class="days"> Thu &nbsp;&nbsp;
<input type="checkbox" data-day="Fri" value="Fri" name="days1[]" class="days"> Fri &nbsp;&nbsp;
<input type="checkbox" data-day="Sat" value="Sat" name="days1[]" class="days"> Sat &nbsp;&nbsp;
<input type="checkbox" data-day="Sun" value="Sun" name="days1[]" class="days"> Sun &nbsp;&nbsp;

</div>
</div>
</div>
<hr>

<?php } }?>
       </div>
    </div>         
                    
                    

    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                   <label>Remark:</label>
                       <textarea class="form-control" name="REMARKS"><?php echo $value['REMARKS'] ? $value['REMARKS'] : ''?></textarea> 
                        <span class="text-danger"><?php echo form_error('REMARKS'); ?></span>
                
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary">Update</button> &nbsp;
                <button type="reset" style="float:none">Reset</button>
            </div>
        </div>
    </div>

    

              


    </div>
    </div>
        <?php echo form_close(); ?>
    </div>
    </div>
    
</div>
<?php } ?>