<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
    <div class="create_batch_form">
        <div id="box">
            <h2>Employee Search</h2></div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align:center; color:#F00;"> </div>
                        <div class="panel-body">
                            <?php echo form_open(); ?>   

<div class="form-group">
    <div class="row" style="margin-left:-30px;">
        <div class="col-lg-12" style="margin-left:0px;">
            <div class="col-lg-6">
                <label>First Name:</label>
                <input class="form-control" type="text" value="<?php if(isset($filter_data['FIRSTNAME'])){ echo $filter_data['FIRSTNAME']; }?>" maxlength="50" name="FIRSTNAME" id="FIRSTNAME">
            </div>
            <div class="col-lg-6">
                <label>Last Name:</label>
                <input class="form-control" type="text" value="<?php if(isset($filter_data['LASTNAME'])){ echo $filter_data['LASTNAME']; }?>" maxlength="50" name="LASTNAME" id="LASTNAME">
            </div>
            
        </div>
    </div>
</div>
                        

<div class="form-group">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-4">
                <label>Active:</label>
                <select name="ACTIVE" class="form-control">
                <option value="">All</option>
                <option value="1">Active</option>
                <option value="0">Inactive</option>
                </select>
            </div>

            <div class="col-md-4"><br>
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
            <div class="col-md-4"><br>
                <button type="submit" name="reset" value="reset" id="reset" class="btn btn-primary" style="float:left">Reset</button>
            </div>
        </div>
    </div>
</div>


<?php echo form_close(); ?>
</div>
</div>
</div>
</div>
</div>
<?php 
echo form_open("hra/update-staff-login-details"); ?>
<div id="box"> <span class="">&nbsp;</span>
    <h2 class="text-center"> Employee List</h2>
    </div>
    <div class="panel panel-default">
        
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-condensed table-stripped">
                <thead>
                    <tr>
                        <th>Action</th>
                        <th>Sr No.</th>
                        <th>Employee ID</th>
                        
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Centre</th>
                        <th>Login Name</th>
                        <th>Password</th>
                        <th>Is Active</th>

                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $count = $this->uri->segment(2) + 1;

                    foreach($employees as $employee){ 

                        $employee_id = $this->encrypt->encode($employee['EMPLOYEE_ID']);
                        ?>
                        <tr>
                            <input type="hidden" id="current_employee_id">
                            <td>
<span class="handCursor glyphicon glyphicon-edit update_staff_login_details" data-toggle="modal" data-target="#update_staff_login_modal" data-id="<?php echo $employee['USER_ID'];?>" data-name="<?php echo $employee['LOGINNAME'];?>"></span>
                            </td>
                            <td><?php echo $count; ?></td>
                            <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                            
                            <td>
                                <?php echo $employee['NAME']; ?>
                            </td>
                            <td>
                                <?php echo $employee['GENDER']; ?>
                            </td>
                            <td>
                                <?php echo $employee['CENTRE_NAME']; ?>
                            </td>
                            <td>
                                <?php echo $employee['LOGINNAME']; ?>
                            </td>
                            <td>

        
            <?php  echo '***'; ?>
        
                                 
                            </td>
                            
                            
                            
                            <td>
                                <input name="" type="checkbox" value="" <?php if($employee['ISACTIVE'] == '1'){ echo 'checked';} ?> />
                            </td>
                           

                        </tr>
                        <?php $count++; } ?>
                    </tbody>
                </table>
                <br> </div>
                <div class="row">
                    <div class="col-md-4 pull-right">
                        <?php echo $pagination; ?>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>



        <!-- Other Receipts Modal Starts -->
        <div id="update_staff_login_modal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update</h4>
              </div>
              <div class="modal-body receiptContent">
                    
                    
                
                
                
                

                <div class="row">
                    
                        <div class="col-md-6">
                            <label>Login Name:</label>
                            <input type="text" name="LOGINNAME" value="" class='form-control username_detail'>
                        </div>
                        <div class="col-md-6">
                        <label>Password:</label>
                           <input type='password'  name='PASSWORD' class='form-control user_password'>
                        </div>
                    
                    
                </div>

                <div class="row">
                    <div class="col-md-4">&nbsp;</div>
                    <div class="col-md-4">
                        <p>&nbsp;</p>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-info" id="update_staff_login_details">Save</button>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                </div>
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>
    <!-- Other Receipts Modal Ends -->

