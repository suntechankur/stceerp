<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
    <div class="create_batch_form">
        <div id="box">
            <h2>Employee Search</h2></div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="text-align:center; color:#F00;"> </div>
                        <div class="panel-body">
                            <?php echo form_open(); ?>   

                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-4">
                                            <label>First Name:</label>
                                            <input class="form-control" type="text" value="<?php if(isset($filter_data['FIRSTNAME'])){ echo $filter_data['FIRSTNAME']; }?>" maxlength="50" name="FIRSTNAME" id="FIRSTNAME">
                                        </div>
                                        <div class="col-lg-4">
                                            <label>Last Name:</label>
                                            <input class="form-control" type="text" value="<?php if(isset($filter_data['LASTNAME'])){ echo $filter_data['LASTNAME']; }?>" maxlength="50" name="LASTNAME" id="LASTNAME">
                                        </div>
                                        <div class="col-lg-4">
                                            <label>Centre:</label>
                                            <select name="CENTRE_ID" class="form-control">
                                                <option value="">Select Centre</option>
                                                <?php foreach($centres as $centre){ ?>
                                                <option value="<?php echo $centre['CENTRE_ID']; ?>" <?php if(isset($enquiry_data['CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $enquiry_data['CENTRE_ID']) ? ' selected="selected"' : '';}?>><?php echo $centre['CENTRE_NAME']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-4">
                                            <label>Date Of Join From.:</label>
                                            <input name="JOINING_FROM" id="JOINING_FROM" value="<?php if(isset($filter_data['JOINING_FROM'])){ echo $filter_data['JOINING_FROM']; }?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">  </div>
                                            <div class="col-lg-4">
                                                <label>Date Of Join To.:</label>
                                                <input name="JOINING_TO" id="JOINING_TO" value="<?php if(isset($filter_data['JOINING_TO'])){ echo $filter_data['JOINING_TO']; }?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">  </div>
                                                <div class="col-lg-4">
                                                    <label>Active:</label>
                                                    <select name="ACTIVE" class="form-control">
                                                        <option value="">All</option>
                                                        <option value="1">Active</option>
                                                        <option value="0">Inactive</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row" style="margin-left:-30px;">
                                            <div class="col-lg-12" style="margin-left:0px;">
                                                <div class="col-lg-4">
                                                    <label>Date Of Leave From.:</label>
                                                    <input name="LEAVING_FROM" id="LEAVING_FROM" value="<?php if(isset($filter_data['LEAVING_FROM'])){ echo $filter_data['LEAVING_FROM']; }?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">  </div>
                                                    <div class="col-lg-4">
                                                        <label>Date Of Leave To.:</label>
                                                        <input name="LEAVING_TO" id="LEAVING_TO" value="<?php if(isset($filter_data['LEAVING_TO'])){ echo $filter_data['LEAVING_TO']; }?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY" type="text">  </div>
                                                        <div class="col-lg-4">
                                                            <label>Department:</label>
                                                           <select name="DEPARTMENT" class="form-control">
                                                                 <option value="">Select Department</option>
                                                                     <?php foreach($department as $dpt){ ?>
                                                                     <option value="<?php echo $dpt['DEPARTMENT_ID']; ?>" <?php if(isset($filter_data['DEPARTMENT'])){ echo ($dpt['DEPARTMENT_ID'] == $filter_data['DEPARTMENT']) ? ' selected="selected"' : '';}?>><?php echo $dpt['DEPARTMENT_NAME']; ?></option>
                                                                     <?php } ?>
                                                            </select> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row" style="margin-left:-30px;">
                                                    <div class="col-lg-12" style="margin-left:0px;">
                                                        <div class="col-lg-4">
                                                            <label>Designation:</label>
                                                            <select name="DESIGNATION" class="form-control">
                                                                <option value="">Select Designation</option>
                                                                <?php foreach($designation as $dsg){ ?>
                                                                <option value="<?php echo $dsg['CONTROLFILE_ID']; ?>" <?php if(isset($filter_data['DESIGNATION'])){ echo ($dsg['CONTROLFILE_ID'] == $filter_data['DESIGNATION']) ? ' selected="selected"' : '';}?>><?php echo $dsg['CONTROLFILE_VALUE']; ?></option>

                                                                <?php } ?>
                                                            </select>
<!--   <?php
// echo "<pre>";
// print_r($filter_data);
// echo "</pre>";

    ?> -->
</div>
<div class="col-lg-4">
    <br>
    <div class="col-md-6">
        <button type="submit" class="btn btn-primary">Search</button>
    </div>
    <div class="col-md-6">
        <button type="submit" name="reset" value="reset" id="reset" class="btn btn-primary" style="float:left">Reset</button>
    </div>
    
</div>
<?php if (!empty($filter_data)) { ?>
    <div class="col-lg-4">
        <br>
        <a href="<?php echo base_url('hra/export-employee-list');?>" class="btn" style="float:left" title="Export to excel"><i class="fa fa-file-excel-o fa-2x text-success"></i></a>
    </div>
<?php } ?>
</div>
</div>
</div>



<?php echo form_close(); ?>
</div>
</div>
</div>
</div>
</div>
<?php 
$attr = array('id'=>'empId');
echo form_open("hra/empIds",$attr); ?>
<div id="box"> <span class=""><?php echo $pagination; ?></span>
    <h2 class="text-center"> Employee List
    <span type="submit" style="margin-right: 9px;" class="btn btn-primary pull-right genId">Generate Selected Employees Id Card</span>
    <!-- <button type="submit">Generate</button> -->
    </h2>
    <!--        <div class="row"><div class="col-md-4 pull-right"></div></div>--></div>
    <div class="panel panel-default">
        <!--<div class="panel-heading">Form Elements</div>-->
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-condensed table-stripped">
                <thead>
                    <tr>
                        <th><input type="checkbox" class="sel_all"></th>
                        <th>Action</th>
                        <th>Sr No.</th>
                        <th>Employee ID</th>
                        <th>Biometric ID</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Centre</th>
                        <th>Timings</th>
                        <th>Date of Join</th>
                        <th>Date of Birth</th>
                        <th>Designation</th>
                        <th>Qualification</th>
                        <th>CTC</th>
                        <th>Basic</th>
                        <th>HRA</th>
                        <th>Address</th>
                        <th>Contact No</th>
                        <th>Official Email</th>
                        <th>Personal Email</th>
                        <th>PF Number</th>
                        <th>PAN Card No</th>
                        <th>Is Active</th>
                        <th>Bank Account No.</th>
                        <th>Branch Name</th>
                        <th>UAN</th>

                    </tr>
                </thead>
                <tbody>
                    <!--                    <tr><td colspan="18" class="text-center">Sorry Record Not Found</td></tr>							-->
                    <?php 
                    $count = $this->uri->segment(2) + 1;
                    foreach($employees as $employee){ 

                        $employee_id = $this->encrypt->encode($employee['EMPLOYEE_ID']);
                        ?>
                        <tr>
                            <td><input class="sel_data" name="empId[]" type="checkbox" value="<?php echo $employee_id; ?>"></td>
                            <td><a href="<?php echo base_url('hra/update-employee/').$employee_id; ?>"><span class="glyphicon glyphicon-edit"></span></a></td>
                            <td><?php echo $count; ?></td>
                            <td>
                                <?php echo $employee['EMPLOYEE_ID']; ?>
                            </td>
                            <td>
                                <?php echo $employee['BIOMETRIC_DEVICE_ID']; ?> 
                            </td>
                            <td>
                                <?php echo $employee['NAME']; ?>
                            </td>
                            <td>
                                <?php echo $employee['GENDER']; ?>
                            </td>
                            <td>
                                <?php echo $employee['CENTRE_NAME']; ?>
                            </td>
                            <td>
                                <?php //echo  date("d/m/Y", strtotime($employee['DATEOFJOIN'])); ?>
                                <?php 
                                    for ($i=0; $i <count($empShiftDays[$employee['EMPLOYEE_ID']]) ; $i++) {
                                        $empShift =  $empShiftDays[$employee['EMPLOYEE_ID']][$i];
                                        echo '<span class="label label-default">'.$empShift['CENTRE_NAME'].': <br>'.$empShift['DAYS'].'<br>'.$empShift['FROM_TIMING'].' to '.$empShift['TO_TIMING'].'</span><br>';
                                    }
                                ?>
                            </td>
                            <td>
                                <?php echo  date("d/m/Y", strtotime($employee['DATEOFJOIN'])); ?>
                            </td>
                            <td>
                                <?php echo  date("d/m/Y", strtotime($employee['DATEOFBIRTH'])); ?>
                            </td>
                            <td>
                                <?php echo $employee['DESIGNATION']; ?>
                            </td>
                            <td>
                                <?php echo $employee['QUALIFICATION']; ?>
                            </td>
                            <td>
                                <?php echo $employee['CTC']; ?>
                            </td>
                            <td>
                                <?php echo $employee['BASIC']; ?>
                            </td>
                            <td>
                                <?php echo $employee['HRA']; ?>
                            </td>
                            <td>
                                <?php echo $employee['ADDRESS1']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMP_TELEPHONE']; ?> <br />
                                <?php echo $employee['EMP_MOBILENO']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMP_OFFICIAL_EMAIL']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMP_EMAIL']; ?>
                            </td>
                            <td>
                                <?php echo $employee['EMPLOYEEPFNO']; ?>
                            </td>
                            <td>
                                <?php echo $employee['PAN_CARD_NO']; ?>
                            </td>
                            <td>
                                <input name="" type="checkbox" value="" <?php if($employee['ISACTIVE'] == '1'){ echo 'checked';} ?> />
                            </td>
                            <td>
                                <?php echo $employee['BANKACCOUNTNO']; ?>
                            </td>
                            <td>
                                <?php echo $employee['BANK_BRANCH']; ?>
                            </td>
                            <td>
                                <?php echo $employee['UA_NUMBER']; ?>
                            </td>


                        </tr>
                        <?php $count++; } ?>
                    </tbody>
                </table>
                <br> </div>
                <div class="row">
                    <div class="col-md-4 pull-right">
                        <?php echo $pagination; ?>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>