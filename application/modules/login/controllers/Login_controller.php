<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_controller extends MX_Controller {

     public function __construct()
        {
                $this->load->model('login_model');
                parent::__construct();
        }

    public function admin_credentials(){
        if($this->session->userdata('admin_data')){
            redirect(base_url('view-tele-enquiry'));
        }
        $this->form_validation->set_rules('LOGINNAME', 'Login Id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('PASSWORD', 'Password', 'trim|required|xss_clean');
        $this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'required');

        $username = $this->input->post('LOGINNAME');
        $password = $this->input->post('PASSWORD');

        $data = '';
        if($this->form_validation->run() == TRUE)
        {
            $credentials = $this->login_model->admin_credentials($username,$password);

            if(count($credentials) > 0){
                $this->session->set_userdata('admin_data',$credentials);
                // changes done by ankur on 23/10/2017
                if($this->session->userdata('admin_data')[0]['ROLE_ID'] == "23"){
                    redirect(base_url('view-tele-enquiry'),'refresh');
                }
                else{
                    redirect(base_url('dashboard'),'refresh');
                }
            }
            else{
                $data['msg'] = 'Invalid username or password';
            }
        }

        $this->load->view('login_view',$data);
    }

    public function logout(){
        $this->session->unset_userdata('admin_data');
        $this->session->sess_destroy();
        redirect(base_url());
    }
}
