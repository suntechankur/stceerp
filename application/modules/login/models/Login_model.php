<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {
    public function admin_credentials($username,$password){
        $query = $this->db->select('ul.USER_ID,ul.EMPLOYEE_ID,ul.LOGINNAME,ul.ROLE_ID,em.CENTRE_ID,em.EMP_FNAME,em.EMP_LASTNAME,em.EMP_OFFICIAL_EMAIL as EMAIL,em.ISACTIVE')
                 ->from('user_login ul')
                 ->join('employee_master em','ul.EMPLOYEE_ID=em.EMPLOYEE_ID','left')
                 ->where('ul.ISACTIVE','1')
                 ->where('LOGINNAME',$username)
                 ->where('PASSWORD',$password)
                 ->limit('1')
                 ->get();
        return $query->result_array();
    }

    public function getAdminData($empId){
    	$query = $this->db->select('EM.EMPLOYEE_ID,EM.EMP_FNAME,EM.EMP_LASTNAME,EM.DEPARTMENT_ID,CENTRE_ID')
    					  ->from('employee_master EM')
    					  ->where('EMPLOYEE_ID',$empId)
    					  ->get();
    	return $query->result_array();
    }
}
