<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asset_controller extends MX_Controller {

     public function __construct()
        {
			      $this->load->model('asset/asset_model');
			      $this->load->model('tele_enquiry/tele_enquiry_model');
			      parent::__construct();
        }

    public function addAsset(){
      $data['centres'] = $this->tele_enquiry_model->suggested_centre();
      $data['categories'] = $this->asset_model->get_assets_category();
      $data['asset_names'] = $this->asset_model->get_assets_names();
      $data['assets'] = $this->asset_model->asset_details();
      if(isset($_POST['add_asset']))
    	{
    		$this->form_validation->set_rules('company_name','Company name','required|trim|xss_clean');
    		$this->form_validation->set_rules('centre_id','Centre name','required|trim|xss_clean');
    		// $this->form_validation->set_rules('category_id','Category name','required|trim|xss_clean');
    		// $this->form_validation->set_rules('sub_category_id','Sub category name','required|trim|xss_clean');
    		$this->form_validation->set_rules('asset_control_id','Asset name','required|trim|xss_clean');
    		$this->form_validation->set_rules('quantity','Quantity','required|trim|xss_clean');
    		if($this->form_validation->run() == TRUE){
          unset($_POST['add_asset']);
          // $asset_id = $this->asset_model->get_latest_asset($this->input->post('company_name'),$this->input->post('centre_id'),$this->input->post('category_id'),$this->input->post('sub_category_id'),$this->input->post('asset_control_id'));
          for($i=0;$i<$this->input->post('quantity');$i++){
            $asset_id = $this->asset_model->get_latest_asset($this->input->post('company_name'),$this->input->post('centre_id'),$this->input->post('asset_control_id'));
            $asset_label = $this->input->post('company_name')."_".$this->input->post('centre_id')."_".$this->input->post('asset_name')."_".($asset_id + 1);
            $asset_data = array(
                'company_name' => $this->input->post('company_name'),
                'centre_id' => $this->input->post('centre_id'),
                'asset_control_id' => $this->input->post('asset_control_id'),
                'category_id' => $this->input->post('category_id'),
                'sub_category_id' => $this->input->post('sub_category_id'),
                'asset_serial_number' => ($asset_id + 1),
                'asset_label' => $asset_label,
                'quantity' => "1",
                'is_active' => "1",
                'asset_remark' => $this->input->post('asset_remark'),
                'created_by' => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
                'created_date' => date("Y-m-d"),
                'modified_by' => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
                'modified_date' => date("Y-m-d")
            );
            $add_asset = $this->asset_model->insert_assets($asset_data);
          }
          if($add_asset == "true"){
            unset($_POST);
            echo "<script>alert('Asset added successfully.');</script>";
          }
          else{
            unset($_POST);
            echo "<script>alert('Kindly check asset details.');</script>";
          }
        }
        else{
          unset($_POST);
          echo "<script>alert('Please fill all required details.');</script>";
        }
      }
      $this->load->admin_view('asset/add_asset',$data);
    }

    public function get_assets_sub_categories_details(){
      $data['sub_categories'] = $this->asset_model->get_assets_sub_category($_POST['category_id']);
      echo json_encode($data['sub_categories']);
    }


    public function updateItem($id){
		$itemId = $this->encrypt->decode($id);

         $vendorItrsd = '';

        if(isset($_POST['VENDORS_SELECT'])){
            $vendorItrsd = implode(",", $_POST['VENDORS_SELECT']);
            $vendorItrsdArr = $_POST['VENDORS_SELECT'];
        }
        $this->form_validation->set_rules('ITEMNAME', 'ITEM NAME', 'required');
        $this->form_validation->set_rules('ITEMPRICE', 'ITEM PRICE', 'required');
        $this->form_validation->set_rules('MINSTOCK', 'MIN STOCK ENTRY', 'required');
        $this->form_validation->set_rules('MAXREQ', 'MAXREQ', 'required');

        if ($this->form_validation->run() != FALSE){
            $updateItem = array(
                'ITEM_NAME' => $_POST['ITEMNAME'],
                'ITEM_DESCRIPTION' => $_POST['ITEMDESC'],
                'ITEM_PRICE' => $_POST['ITEMPRICE'],
                'ITEM_TYPE' => $_POST['ITEMTYPE'],
                'UNIT_ID' => $_POST['ITEMUNIT'],
                'OPENING_QTY' => $_POST['OPENQUAN'],
                'MIN_STOCK_LVL' => $_POST['MINSTOCK'],
                'MAX_QTY_FOR_REQUISITION' => $_POST['MAXREQ'],
                'CURRENT_STOCK_LVL' => $_POST['CURRSTOCK'],
                'EXCISE_DUTY' => $_POST['EXCDUTY'],
                'IS_TAX_APPLICABLE' => $this->input->post('ISTAXAPPLICABLE'),
                'IS_ACTIVE' => $this->input->post('ISACTIVE'),
                'VENDORS' =>  $vendorItrsd,
                'IsRequisition' => '1'
            );

            date_default_timezone_set("Asia/Kolkata");
            $updateItem['MODIFIED_DATE'] = date("Y-m-d");
         // $this->form_validation->set_rules('TAX_NAME', 'Tax Name', 'required');
        // $this->form_validation->set_rules('TAX_PERCENT', 'Tax Percent', 'required');
        // if ($this->form_validation->run() != FALSE){
        //     $updateItem = array(
        //         'TAX_NAME' => $_POST['TAX_NAME'],
        //         'TAX_PERCENT' => $_POST['TAX_PERCENT'],
        //         'TAX_TYPE' => $_POST['TAX_TYPE'],
        //         'TAX_DESC' => $_POST['TAX_DESC'],
        //     );
            $this->item_model->updateItem($itemId,$updateItem);
			redirect(base_url('item/add-item'));
        }
         $data['units'] = $this->item_model->getUnits();
         $data['vendor'] = $this->item_model->getVendors('','','');
		 $data['items'] = $this->item_model->getItemList($itemId,'','');

        $this->load->admin_view('item/edit_item',$data);
    }

    public function deleteItem($id){
        $itemId = $this->encrypt->decode($id);
        $this->item_model->deleteItem($itemId);
		redirect(base_url('item/view-item'));
    }

}
