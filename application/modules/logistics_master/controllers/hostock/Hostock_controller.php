<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Hostock_controller extends MX_Controller
{
	public function __construct()
	{
		$this->load->model('hostock/hostock_model');
		$this->load->model('item/item_model');
		parent::__construct();
	}
// echo base_url();
	public function hostock_add($offset='0')
	{

	$data['items'] = $this->hostock_model->getItem();
	$data['vendors'] = $this->item_model->getVendors('','','');
	
	$data['filter_HOStock'] = array('item_name' =>'','from_date' =>'','to_date' =>'');	        
	
	if(!empty($_POST['search']))
	{
        $this->session->set_userdata('filter_HOStock',$_POST);
        $data['filter_HOStock'] = $this->session->userdata('filter_HOStock');   
    }

    if(isset($_POST['reset']))
    {
        $this->session->unset_userdata('filter_HOStock');
        redirect(base_url('hostock/hostock-add'));
    }

	if(!empty($_POST['submit']))
	{
		$this->form_validation->set_rules('ITEM_ID', 'ITEM NAME', 'required');
		$this->form_validation->set_rules('TRANSACTION_DATE','TRANSACTION DATE','required');
		$this->form_validation->set_rules('hostockQTY','hostock QTY','required');
        if ($this->form_validation->run() == TRUE)
        {
			$TRANSACTION_DATE = str_replace('/', '-',$this->input->post('TRANSACTION_DATE'));
			$TRANSACTION_DATE = date('Y-m-d',strtotime($TRANSACTION_DATE));

			$data['getForUpdateHOstock'] = $this->hostock_model->getForUpdateHOstock($this->input->post('ITEM_ID'));

			$BALANCE = $data['getForUpdateHOstock']['0']['BALANCE'];
			$BALANCE = $BALANCE + $this->input->post('QTY_RECEIVED') - $this->input->post('QTY_ISSUED');

			$insert = array(
				"ITEM_ID" => $this->input->post('ITEM_ID'),
				"TRANSACTION_DATE" => $TRANSACTION_DATE,
				"RECEIVED_FROM" => $this->input->post('RECEIVED_FROM'),
				"ISSUED_TO" => $this->input->post('ISSUED_TO'),
				"CHALLAN_NO" => $this->input->post('CHALLAN_NO'),
				"QTY_RECEIVED" => $this->input->post('QTY_RECEIVED'),
				"QTY_ISSUED" => $this->input->post('QTY_ISSUED'),
				"BALANCE" => $BALANCE
				);
			$update = array("CURRENT_STOCK_LVL" => $BALANCE);

			$this->hostock_model->hostock_insert($insert);
			$this->hostock_model->updateItem($this->input->post('ITEM_ID'),$update);


			$this->session->set_flashdata('success','Save Successfully');
        	redirect(base_url('hostock/hostock-add'));
		}
		else
		{
			$this->session->set_flashdata('error','Please Fill Mandatory Fields');
		}
    }

       /* Pagination starts */
        $config['base_url'] = base_url('hostock/hostock-add/');
        $config['total_rows'] = $this->hostock_model->getHOStock_count($data['filter_HOStock']);
        $config['per_page'] = '10';
        
        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/
        
        $this->pagination->initialize($config);
        /* Pagination Ends */
                

        $data['hostocks'] = $this->hostock_model->getHOStock($offset,$config['per_page'],$data['filter_HOStock']);

        $data['pagination'] = $this->pagination->create_links();

	   $this->load->admin_view('hostock/hostock_add',$data);
	}
	
} 
?>