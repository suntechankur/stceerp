<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_controller extends MX_Controller {
    
     public function __construct()
        {
			$this->load->model('unit_master/unit_model');
			parent::__construct();
        }
     public function getUnitList(){
		
        $data['units'] = $this->unit_model->getUnitList();
        $this->load->admin_view('unit_master/unit_master',$data);
     }
    
    public function addUnit(){

        $this->form_validation->set_rules('UNIT_NAME', 'Unit Name', 'required');
        if ($this->form_validation->run() != FALSE){
            $addUnit = array(
                'UNIT_NAME' => $_POST['UNIT_NAME'],
                'UNIT_DESC' => $_POST['UNIT_DESC']
            );
            $this->unit_model->addUnit($addUnit);
			redirect(base_url('unit-master/unit-master'));
        }
		$this->getUnitList();
    }
    
    public function updateUnit($id){
		$unitId = $this->encrypt->decode($id);
        $this->form_validation->set_rules('UNIT_NAME', 'Unit Name', 'required');
        if ($this->form_validation->run() != FALSE){
            $updateUnit = array(
                'UNIT_NAME' => $_POST['UNIT_NAME'],
                'UNIT_DESC' => $_POST['UNIT_DESC'],
            );
            $this->unit_model->updateUnit($unitId,$updateUnit);
			redirect(base_url('unit-master/unit-master'));
        }
		$data['units'] = $this->unit_model->getUnitList($unitId);
        $this->load->admin_view('unit_master/edit_unit',$data);
    }
    
    public function deleteUnit($id){
        $unitId = $this->encrypt->decode($id);
        $this->unit_model->deleteUnit($unitId);
		redirect(base_url('unit-master/unit-master'));
    }
}
