<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asset_model extends CI_Model {

    public function get_assets_category(){
      $query = $this->db->select('*')
                        ->from('control_file')
                        ->where('CONTROLFILE_KEY','ASSET_CATEGORY')
                        ->get();
      return $query->result_array();
    }

    public function get_assets_sub_category($category_id){
      $category_name = "Assets sub category ".$category_id;
      $query = $this->db->select('*')
                        ->from('control_file')
                        ->where('CONTROLFILE_DESCRIPTION',$category_name)
                        ->get();
      return $query->result_array();
    }

    public function get_assets_names(){
      // $category_name = "Assets sub category ".$category_id;
      $query = $this->db->select('*')
                        ->from('control_file')
                        ->where('CONTROLFILE_KEY',"ASSET_NAME")
                        ->get();
      return $query->result_array();
    }

    public function asset_details(){
      // $category_name = "Assets sub category ".$category_id;
      $query = $this->db->select('am.company_name,cm.centre_name,cf.controlfile_value as category_name,cef.controlfile_value as sub_category_name,cof.controlfile_value as asset_name,count(am.quantity) as quantity,am.vendor_name,am.asset_remark')
                        ->from('asset_master am')
                        ->join('centre_master cm','cm.centre_id=am.centre_id','left')
                        ->join('control_file cf','cf.controlfile_id=am.category_id','left')
                        ->join('control_file cef','cef.controlfile_id=am.sub_category_id','left')
                        ->join('control_file cof','cof.controlfile_id=am.asset_control_id','left')
                        ->group_by('am.asset_control_id,am.company_name,am.centre_id')
                        ->get();
      return $query->result_array();
    }

    // public function get_latest_asset($company_name,$centre_id,$category_id,$sub_category_id,$asset_control_id){
    public function get_latest_asset($company_name,$centre_id,$asset_control_id){
      $query = $this->db->select('if(max(asset_serial_number) IS NULL,1,max(asset_serial_number)) as serial_number')
                        ->from('asset_master')
                        ->where('company_name',$company_name)
                        ->where('centre_id',$centre_id)
                        // ->where('category_id',$category_id)
                        // ->where('sub_category_id',$sub_category_id)
                        ->where('asset_control_id',$asset_control_id)
                        ->get();
      return $query->row()->serial_number;
    }

    public function insert_assets($asset_data){
      $this->db->insert('asset_master',$asset_data);
      if ($this->db->affected_rows() > 0) {
        return "true";
      }
      else {
        return "false";
      }
    }

  public function getVendors($id=0,$offset,$per_page){
        $query = $this->db->select('VM.ID,VM.VENDOR_NAME')
                          ->from('vendor_master VM')
                          ->where('IS_ACTIVE','1')
                          ->order_by('VM.VENDOR_NAME','ASC')
                          ->get();
        return $query->result_array();
    }

	public function countItem()
    {
		$query = $this->db->select('ITEM_ID')
                          ->from('item_master')
                          ->get();
        return $query->num_rows();
    }

	public function getItemLastId(){
		$query = $this->db->select_max('ITEM_ID')
						  ->get('item_master');
        return $query->result_array();
    }

    public function addItem($addItem){
    	$this->db->insert('item_master',$addItem);
    }

    public function updateItem($id,$updateItem){
    	$this->db->set($updateItem)
    			 ->where('ITEM_ID',$id)
    			 ->update('item_master');
    }

	public function deleteItem($itemId){
        $this->db->where_in('ITEM_ID',$itemId)
                 ->delete('item_master');
    }

    // public function getItemList(){
    //    $query = $this->db->get('item_master');

    //         return $query->result_array();
    // }

    public function getUnits(){
       $query = $this->db->select('UM.UNIT_ID,UM.UNIT_NAME')
                          ->from('unit_master UM')
                          ->where('IS_ACTIVE','1')
                          ->get();
        return $query->result_array();
      }

      public function vendorNames($vIds)
      {
        $query = $this->db->select('ID,VENDOR_NAME')
                          ->from('vendor_master')
                          ->where_in('ID',$vIds)
                          ->get();
        return $query->result_array();

      }

}
