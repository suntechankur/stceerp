<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hostock_model extends CI_Model
{
  public function getItem(){
    $query = $this->db->select('IM.ITEM_ID,IM.ITEM_NAME')
                      ->from('item_master IM')
                      ->where('ITEM_TYPE','18')
                      ->group_by('IM.ITEM_NAME')
                      ->order_by('IM.ITEM_NAME','ASC')
                      ->get();   
      return $query->result_array();
  }
  public function getForUpdateHOstock($ITEM_ID){
    $query = $this->db->select('HM.BALANCE')
                      ->from('hostock_master HM')
                      ->where('ITEM_ID',$ITEM_ID)
                      ->order_by('HOSTOCK_ID','DESC')
                      ->limit('1')
                      ->get();   
      return $query->result_array();
  }
  public function getHOStock($offset,$perpage,$filter_HOStock='')
  {
    $query = $this->db->select('IM.ITEM_ID,HM.ITEM_ID,IM.ITEM_NAME,HM.*')
                      ->from('hostock_master HM')
                      ->join('item_master IM','IM.ITEM_ID = HM.ITEM_ID','LEFT');

 
    if(isset($filter_HOStock))
    {                      
      if($filter_HOStock['item_name']!='')
      {
        $query = $this->db->where('HM.ITEM_ID',$filter_HOStock['item_name']);
      }
    }
    if(isset($filter_HOStock))
      {
      if($filter_HOStock['from_date']!='')
      {
        $FROM_DATE = str_replace('/', '-', $filter_HOStock['from_date']);
        $FROM_DATE = date('Y-m-d',strtotime($FROM_DATE));

        $query = $this->db->where('TRANSACTION_DATE >=',$FROM_DATE);
      }
    }
    if(isset($filter_HOStock))
    {
    if($filter_HOStock['from_date']=='' && $filter_HOStock['to_date']!='')
    {
      $TO_DATE = str_replace('/', '-', $filter_HOStock['to_date']);
      $TO_DATE = date('Y-m-d',strtotime($TO_DATE));

      $query = $this->db->where('TRANSACTION_DATE <=',$TO_DATE);
    }
    }
    if(isset($filter_HOStock))
    {
    if($filter_HOStock['from_date']!='' && $filter_HOStock['to_date']!='')
    {
      $FROM_DATE = str_replace('/', '-', $filter_HOStock['from_date']);
      $FROM_DATE = date('Y-m-d',strtotime($FROM_DATE));

      $TO_DATE = str_replace('/', '-', $filter_HOStock['to_date']);
      $TO_DATE = date('Y-m-d',strtotime($TO_DATE));

      $query = $this->db->where('TRANSACTION_DATE >=',$FROM_DATE);
      $query = $this->db->where('TRANSACTION_DATE <=',$TO_DATE);
    }
    }
    $query = $this->db->order_by('HOSTOCK_ID','DESC');
    $query = $this->db->limit($perpage,$offset);   
    $query = $this->db->get();
     //echo $this->db->last_query();
    return $query->result_array();
  }
  public function getHOStock_count($filter_HOStock)
  {
    $query = $this->db->select('HM.HOSTOCK_ID')
                      ->from('hostock_master HM')
                      ->order_by('HOSTOCK_ID','DESC'); 

    if(isset($filter_HOStock))
    {                      
      if($filter_HOStock['item_name']!='')
      {
        $query = $this->db->like('HM.ITEM_ID',$filter_HOStock['item_name']);
      }
    }
    if(isset($filter_HOStock))
      {
      if($filter_HOStock['from_date']!='')
      {
        $FROM_DATE = str_replace('/', '-', $filter_HOStock['from_date']);
        $FROM_DATE = date('Y-m-d',strtotime($FROM_DATE));

        $query = $this->db->where('TRANSACTION_DATE >=',$FROM_DATE);
      }
    }
    if(isset($filter_HOStock))
    {
    if($filter_HOStock['from_date']=='' && $filter_HOStock['to_date']!='')
    {
      $TO_DATE = str_replace('/', '-', $filter_HOStock['to_date']);
      $TO_DATE = date('Y-m-d',strtotime($TO_DATE));

      $query = $this->db->where('TRANSACTION_DATE <=',$TO_DATE);
    }
    }
    if(isset($filter_HOStock))
    {
    if($filter_HOStock['from_date']!='' && $filter_HOStock['to_date']!='')
    {
      $FROM_DATE = str_replace('/', '-', $filter_HOStock['from_date']);
      $FROM_DATE = date('Y-m-d',strtotime($FROM_DATE));

      $TO_DATE = str_replace('/', '-', $filter_HOStock['to_date']);
      $TO_DATE = date('Y-m-d',strtotime($TO_DATE));

      $query = $this->db->where('TRANSACTION_DATE >=',$FROM_DATE);
      $query = $this->db->where('TRANSACTION_DATE <=',$TO_DATE);
    }
    }

    $query = $this->db->get();
   
    return $query->num_rows();
  }
  public function hostock_insert($insert)
  {

    $this->db->insert('hostock_master',$insert);
  }
  public function updateItem($id,$updateItem){
  	$this->db->set($updateItem)
  			 ->where('ITEM_ID',$id)
  			 ->update('item_master');
  }

  public function deleteItem($itemId){
      $this->db->where_in('id',$itemId)
               ->delete('item_master');
  }
}
