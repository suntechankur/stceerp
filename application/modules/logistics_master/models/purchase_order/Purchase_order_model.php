<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_order_model extends CI_Model {

         public function purchaseOrder($itemId){
         	$query = $this->db->select('IM.ITEM_ID,IM.ITEM_NAME,IM.ITEM_PRICE,UM.UNIT_NAME')
         					  ->from('item_master IM')
         					  ->join('unit_master UM','UM.UNIT_ID = IM.UNIT_ID')
         					  ->where('ITEM_ID',$itemId)
         					  ->get();
         	return $query->result_array();
         }

        public function purchaseData($offset,$perpage,$purchase_filter_data){
         	$query = $this->db->select('POMT.PURCHASE_ORDER_MAIN_TRANSACTION_ID,POMT.PURCHASE_ORDER_DATE,VM.VENDOR_NAME,POMT.GRAND_TOTAL')
         					  ->from('purchase_order_main_transaction POMT')
         					  ->join('vendor_master VM','VM.ID = POMT.VENDOR_ID','left')
         					  ->order_by('PURCHASE_ORDER_MAIN_TRANSACTION_ID','DESC')
         					  ->limit($perpage,$offset)
         					  ->get();
         	return $query->result_array();
         }

		public function purchaseData_count($purchase_filter_data)
				{
				$query = $this->db->select("POMT.PURCHASE_ORDER_MAIN_TRANSACTION_ID")->from('purchase_order_main_transaction POMT')
				->order_by('PURCHASE_ORDER_MAIN_TRANSACTION_ID','DESC');
				$query = $this->db->get();
				return $query->num_rows();
				}

		public function purchaseOrderItem($vendId){
			$query = $this->db->select('ITEM_ID,ITEM_NAME,ITEM_PRICE')
         					  ->from('item_master')
         					  ->like('VENDORS',$vendId,'both')
         					  ->get();
         	return $query->result_array();
		}

      public function addPurchOrder($addPurchOrder){
         $this->db->insert('purchase_order_main_transaction',$addPurchOrder);
         return $this->db->insert_id();
      }

      public function addPurchOrderTransDet($addPurchOrderTransDet){
         $this->db->insert('purchase_order_transaction_details',$addPurchOrderTransDet);
      }

      public function printPurcOrdMainTransDet($poId){
         $query = $this->db->select('IM.ITEM_NAME,POTD.ITEM_USED_AT,IM.ITEM_DESCRIPTION,POTD.ITEM_ORDER_QUANTITY,POTD.ITEM_UNIT,POTD.ITEM_UNIT_RATE')
                           ->from('purchase_order_transaction_details POTD')
                           ->join('item_master IM','IM.ITEM_ID = POTD.ITEM_ID')
                           ->where('POTD.PURCHASE_ORDER_MAIN_TRANSACTION_ID',$poId)
                           ->get();
         return $query->result_array();
      }

      public function printPurchOrdMainTrans($poId){
         $query = $this->db->select('POMT.PURCHASE_ORDER_MAIN_TRANSACTION_ID,POMT.PURCHASE_ORDER_DATE,VM.VENDOR_NAME,VM.ADDRESS,POMT.PURCHASE_ORDER_DELIVERY_DETAILS,VM.MOBILE,POMT.ITEM_CATEGORY,POMT.VAT_PER,POMT.ST_PER,POMT.TOTAL_AMOUNT_EXCLUSIVE_TAX,POMT.TAX_AMOUNT,POMT.GRAND_TOTAL,POMT.DELIVERY_DATE,CM.CENTRE_NAME,POMT.BILL_NUMBER')
                           ->from('purchase_order_main_transaction POMT')
                           ->join('vendor_master VM','VM.ID = POMT.VENDOR_ID')
                           ->join('centre_master CM','CM.CENTRE_ID = POMT.PLACE_OF_DELIVERY')
                           ->where('POMT.PURCHASE_ORDER_MAIN_TRANSACTION_ID',$poId)
                           ->get();
         return $query->result_array();
      }

	}
