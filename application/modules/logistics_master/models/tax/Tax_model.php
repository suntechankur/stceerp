<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tax_model extends CI_Model {
    public function getTaxList($id=0,$offset,$perpage){
		if($id == 0){
            $query = $this->db->limit($perpage,$offset);   
        }
        else{
            $query = $this->db->where('id',$id);
        }
		$query = $this->db->get('tax_master');
        return $query->result_array();
    }
	
	public function countTax()
    {
		$query = $this->db->select('ID')
                          ->from('tax_master')
                          ->get();
        return $query->num_rows();
    }
	
	public function getTaxLastId(){
		$query = $this->db->select_max('id')
						  ->get('tax_master');
        return $query->result_array();
    }
	
    public function addTax($addTax){
    	$this->db->insert('tax_master',$addTax);
    }
	
    public function updateTax($id,$updateTax){
    	$this->db->set($updateTax)
    			 ->where('id',$id)
    			 ->update('tax_master');
    }
	
	public function deleteTax($taxId){
        $this->db->where_in('id',$taxId)
                 ->delete('tax_master');
    }
}
