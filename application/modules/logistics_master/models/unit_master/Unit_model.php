<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_model extends CI_Model {
    public function getUnitList($unitId=''){
        if($unitId!='')
        {
        $query = $this->db->select('UNIT_NAME,UNIT_DESC')
                          ->from('unit_master')        
                          ->where('UNIT_ID',$unitId)
                          ->get();
        }
        else
        {
		$query = $this->db->get('unit_master');
        }
        return $query->result_array();
    }
	
			
    public function addUnit($addUnit){
    	$this->db->insert('unit_master',$addUnit);
    }
	
    public function updateUnit($id,$updateUnit){
    	$this->db->set($updateUnit)
    			 ->where('UNIT_ID',$id)
    			 ->update('unit_master');
    }
	
	public function deleteUnit($UnitId){
        $this->db->where_in('UNIT_ID',$UnitId)
                 ->delete('unit_master');
    }
}
