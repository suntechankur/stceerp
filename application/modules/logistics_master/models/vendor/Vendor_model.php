<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor_model extends CI_Model {
	public function getVendorList($id=0,$offset,$per_page){
        $query = $this->db->select('VM.ID as vendor_masterid,VM.VENDORID,VM.VENDOR_NAME,VM.COMPANY_NAME,VM.ADDRESS,VM.TEL,VM.MOBILE,VM.FAX_NO,VM.EMAIL1,VM.EMAIL2,VM.VENDOR_TYPE,VM.INCOME_TAX_NUMBER,VM.CST_NUMBER,VM.SERVICE_TAX_NUMBER,VM.ECC_NUMBER,VM.MODIFIED_BY,VM.MODIFIED_DATE,VM.VAT_TIN_NUMBER,VM.TAX_ID,VM.IS_ACTIVE,TM.TAX_ID as tax_masterid,TM.TAX_NAME')
                          ->from('vendor_master VM')
                          ->join('tax_master TM','TM.TAX_ID = VM.TAX_ID','left'); 
        if($id == 0){
           $query = $this->db->limit($per_page,$offset);
        }
        else{
            $query = $this->db->where('VM.id',$id);
        }
        
		$query = $this->db->get();
        return $query->result_array();
    }
	
	public function countVendor()
    {
		$query = $this->db->select('ID')
                          ->from('vendor_master')
                          ->get();
        return $query->num_rows();
    }
	
	public function getVendorLastId(){
		$query = $this->db->select_max('id')
						  ->get('vendor_master');
        return $query->result_array();
    }
	
  public function addVendor($addVendor){
    	$this->db->insert('vendor_master',$addVendor);
    }
	
  public function updateVendor($id,$updateVendor){
    	$this->db->set($updateVendor)
    			 ->where('id',$id)
    			 ->update('vendor_master');
    }
	
	public function deleteVendor($vendorId){
        $this->db->where_in('id',$vendorId)
                 ->delete('vendor_master');
    }

  public function getTaxList(){
     $query =  $this->db->select('TAX_ID,TAX_NAME')
                 ->from('tax_master')
                 ->get();
        return $query->result_array();
   } 
}
