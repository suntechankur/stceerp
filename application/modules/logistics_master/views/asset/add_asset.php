<div class="gapping"></div>
<div class="create_batch_form">
    <div id="box">
        <h2>Add Assets</h2>
	  </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                	<!-- start message area -->
                  <?php if($this->session->flashdata('danger')) { ?>
                  <div class="alert alert-danger">
                  <strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
                  </div>
                  <?php } ?>

                  <?php if($this->session->flashdata('success')) { ?>
                  <div class="alert alert-success">
                  <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                  </div>
                  <?php } ?>


                  <?php if($this->session->flashdata('failed')) { ?>
                  <div class="alert alert-info">
                  <strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
                  </div>
                  <?php } ?>

                  <?php if($this->session->flashdata('info')) { ?>
                  <div class="alert alert-info">
                  <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
                  </div>
                  <?php } ?>

                  <?php if($this->session->flashdata('info1')) { ?>
                  <div class="alert alert-info">
                  <strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
                  </div>
                  <?php } ?>

                  <?php if($this->session->flashdata('warning')) { ?>
                  <div class="alert alert-warning">
                  <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
                  </div>
                  <?php } ?>

                  <!-- End message area -->
                </div>
                <div class="panel-body">
                   <?php
                    $attr = array('role'=>'form');
                    echo form_open('',$attr); ?>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                									<div class="col-lg-4">
                										<label>Company Name : <span class="text-danger">*</span></label>
                										<select class="form-control" name="company_name">
                                      <option disabled selected>Select company</option>
                                      <option value="SACL">St. Angelos Computer Education</option>
                                      <option value="SAVV">St. Angelos VNCT Vnetures</option>
                                    </select>
                									</div>
                									<div class="col-lg-4">
                										<label>Centre Name : <span class="text-danger">*</span></label>
                                    <select class="form-control" name="centre_id">
                                      <option disabled selected>Select centre</option>
                                      <?php foreach ($centres as $centre) {?>
            														<option value="<?php echo $centre['CENTRE_ID']; ?>"><?php echo $centre['CENTRE_NAME']; ?> </option>
            													<?php } ?>
                                      <option value="000">Ganesh Manish</option>
                                      <option value="181">Goregaon (w)</option>
                                    </select>
                									</div>
                									<div class="col-lg-4">
                										<label>Asset Category : <span class="text-danger">*</span></label>
                                    <select class="form-control" id="asset_main_category" name="category_id" disabled="disabled">
                                      <option disabled selected>Select category</option>
                                      <?php foreach ($categories as $category) {?>
            														<option value="<?php echo $category['CONTROLFILE_ID']; ?>"><?php echo $category['CONTROLFILE_VALUE']; ?> </option>
            													<?php } ?>
                                    </select>
                									</div>
                									<div class="col-lg-4">
                                    <label>Asset Sub Category : <span class="text-danger">*</span></label>
                                    <select class="form-control" id="asset_sub_category" name="sub_category_id" disabled="disabled">
                                    </select>
                									</div>
                									<div class="col-lg-4">
                										<label>Asset Name: <span class="text-danger">*</span></label>
                										<select class="form-control" name="asset_control_id" id="asset_selection">
                                      <option disabled selected>Select asset name</option>
                                      <?php foreach ($asset_names as $assetname) {?>
            														<option value="<?php echo $assetname['CONTROLFILE_ID']; ?>"><?php echo $assetname['CONTROLFILE_VALUE']; ?> </option>
            													<?php } ?>
	                                  </select>
                                    <input class="form-control" name="asset_name" type="hidden" id="asset_master_name">
                									</div>
                									<div class="col-lg-4">
                										<label>Vendor name / Manufracturer :</label>
                										<input class="form-control" name="vendor_name" type="text" value="" placeholder="Vendor name">
                									</div>
                									<div class="col-lg-4">
                										<label>Quantity : <span class="text-danger">*</span></label>
                										<input class="form-control" name="quantity" id="asset_quantity" type="number" value="1" min="1" max="1000">
                									</div>
                									<div class="col-lg-4">
                										<label>Asset Remark :</label>
                										<input class="form-control" name="asset_remark" type="text" value="" placeholder="Asset remarks">
                									</div>
                									<div class="col-lg-4">
                                    <br/>
                										<button type='submit' name="add_asset" class='btn btn-primary'>Save</button>
                									</div>
			                         </div>
                            </div>
                          </div>
                    </div>
                <?php echo form_close(); ?>
                </div>
            </div>
        </div>
</div>
<br/>
<div class="create_batch_form">
<div id="box">
      <h2 class="text-center">Asset List</h2>
	</div>
    <div class="panel panel-default">
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-condensed table-stripped">
				<thead>
					<tr>
						<th>Company Name</th>
						<th>Centre Name</th>
						<th>Category Name</th>
						<th>Sub category Price</th>
						<th>Asset Name</th>
						<th>Quantity</th>
						<th>Vandor / Manufracturer</th>
						<th>Remark</th>
					</tr>
				</thead>
				<tbody>
          <?php foreach ($assets as $asset) {?>
            <tr>
              <th><?php echo $asset['company_name'];?></th>
              <th><?php echo $asset['centre_name'];?></th>
              <th><?php echo $asset['category_name'];?></th>
              <th><?php echo $asset['sub_category_name'];?></th>
              <th><?php echo $asset['asset_name'];?></th>
              <th><?php echo $asset['quantity'];?></th>
              <th><?php echo $asset['vendor_name'];?></th>
              <th><?php echo $asset['asset_remark'];?></th>
            </tr>
          <?php } ?>
				</tbody>
			</table>
		<br>
		</div>
    </div>
</div>
</div>
</div>
