<div class="gapping"></div>
<div class="create_batch_form">
<div id="box">
<h2>HO Stock Add</h2>

</div>
<div class="row">
<div class="col-sm-12">
<div class="panel panel-default">
    <div class="panel-heading" style="text-align:center;">
    	<?php
if($this->session->flashdata('error'))
{
	echo $this->session->flashdata('error');
}
if($this->session->flashdata('success'))
{
	echo $this->session->flashdata('success');
}
?>
    </div>
    <div class="panel-body">
       <?php 
        
        echo form_open(); ?>
            <div class="col-md-12">
                <div class="form-group">
                <div class="row">
						<div class="col-lg-3">
							<label>Item Name: <span class="text-danger">*</span></label>
							<select class="form-control" name="ITEM_ID" id="ITEM_ID" >
							<option value="">Please select</option>
							<?php foreach($items as $item) { ?>
                             <option value="<?php echo $item['ITEM_ID']; ?>"<?php echo ($this->input->post('ITEM_ID')==$item['ITEM_ID']) ? "selected" : ''; ?>><?php echo $item['ITEM_NAME']; ?></option>
                            <?php } ?>
							</select> 
							<span class="text-danger"><?php echo form_error('ITEM_ID'); ?></span>
						</div>
						<div class="col-lg-3">
							<label>Date(dd/mm/yyyy): <span class="text-danger">*</span></label>
							<input class="form-control enquiry_date" name="TRANSACTION_DATE" value="<?php echo $this->input->post('TRANSACTION_DATE') ? $this->input->post('TRANSACTION_DATE') : '';?>" id="TRANSACTION_DATE" type="text"> 
							<span class="text-danger"><?php echo form_error('TRANSACTION_DATE'); ?></span>
						</div>
						<div class="col-lg-3">
							<label>Challan No.: <span class="text-danger"></span></label>
							<input class="form-control" name="CHALLAN_NO" value="<?php echo $this->input->post('CHALLAN_NO') ? $this->input->post('CHALLAN_NO') : '';?>" id="CHALLAN_NO" type="text"> 
							<span class="text-danger"><?php echo form_error('CHALLAN_NO'); ?></span>
						</div>
						<div class="col-lg-3">
<br>
	<input type="radio" name="hostockQTY" value="Received"> <label> Received: <span class="text-danger"></span></label>&nbsp;
	<input type="radio" name="hostockQTY" value="Issued"> <label> Issued: <span class="text-danger"></span></label>

	<span class="text-danger"><?php echo form_error('hostockQTY'); ?></span>
</div>
						
				</div>
                </div>
<div class="form-group">
<div class="row">			
	<div id="Received" style="display: none">
		<div class="col-lg-3">
			<label>Received From: <span class="text-danger"></span></label>
			<select class="form-control vendNames" name="RECEIVED_FROM" id="RECEIVED_FROM" >
				<option value="">Select Vendor</option>
					<?php foreach($vendors as $vendor) { ?>
	             <option value="<?php echo $this->encrypt->encode($vendor['ID']); ?>"  ><?php echo $vendor['VENDOR_NAME']; ?>
	             	
	             </option>
	                <?php } ?>
	         </select>
			<span class="text-danger"><?php echo form_error('RECEIVED_FROM'); ?></span>
		</div>

		<div class="col-lg-3">
			<label>Qty Received: <span class="text-danger"></span></label>
			<input class="form-control" name="QTY_RECEIVED" id="QTY_RECEIVED" value="<?php echo $this->input->post('QTY_RECEIVED') ? $this->input->post('QTY_RECEIVED') : '';?>" type="text"> 
			<span class="text-danger"><?php echo form_error('QTY_RECEIVED'); ?></span> 
		</div>
	</div>

	<div id="Issued" style="display: none">
		<div class="col-lg-3">
			<label>Issued To: <span class="text-danger"></span></label>
			<input class="form-control" name="ISSUED_TO" id="ISSUED_TO" value="<?php echo $this->input->post('ISSUED_TO') ? $this->input->post('ISSUED_TO') : '';?>" type="text"> 
			<span class="text-danger"><?php echo form_error('ISSUED_TO'); ?></span>
		</div>
		<div class="col-lg-3">
			<label>Qty Issued: <span class="text-danger"></span></label>
			<input class="form-control" name="QTY_ISSUED" id="QTY_ISSUED" value="<?php echo $this->input->post('QTY_ISSUED') ? $this->input->post('QTY_ISSUED') : '';?>" type="text"> 
			<span class="text-danger"><?php echo form_error('QTY_ISSUED'); ?></span>
		</div>
	</div>

	<div class="col-lg-6">
	<div class="row">
	<br>
	<div class="col-lg-12">
		<div class="col-md-4">
		<button type='submit' name="submit" value="submit" class='btn btn-primary'>Save</button>
		</div>
		<div class="col-md-8">
		<button type='submit' name="reset" value="reset" style="float: none;">Reset</button>
		</div> 
	</div>
	</div> 
	</div>

</div>
</div>		

					
					
                </div>

                
            </div>
   <?php echo form_close(); ?>
    </div>

</div>
</div>
</div>
<br>
<?php echo $pagination; ?>

<div class="create_batch_form">
<div id="box">

<h2 class="text-center"> Stock List</h2>
</div>

<div class="row">
<div class="col-sm-12">
<div class="panel panel-default">
    <div class="panel-heading" style="text-align:center; color:#F00;"></div>
    <div class="panel-body">
<div class="col-md-12">
<div class="form-group">
<div class="row">

<div class="panel-body table-responsive">
<table class="table table-bordered table-condensed table-stripped">

	<thead>
<tr>
<td colspan="9">
<?php echo form_open();?>
	<table align="left">
		<tr>
				<th style="width: 230px;">Item Name:

<select class="form-control" name="item_name" id="item_name" >
							<option value="">Please select</option>
							<?php foreach($items as $item) { ?>
                             <option value="<?php echo $item['ITEM_ID']; ?>"<?php echo ($this->input->post('item_name')==$item['ITEM_ID']) ? "selected" : ''; ?>><?php echo $item['ITEM_NAME']; ?></option>
                            <?php } ?>
							</select> 
							<span class="text-danger"><?php echo form_error('item_name'); ?></span>

				</th>
				<th>From Date: <input type="text" class="form-control enquiry_date" name="from_date"></th>
				<th>To Date: <input type="text" class="form-control enquiry_date" name="to_date"></th>
				<th>&nbsp;<input type="submit" value="Search" class="btn btn-primary" name="search"></th>
				
		</tr>
	</table>
	<?php echo form_close();?>
</td>
</tr>
		<tr>
			
			<th>Sr. No.</th>
			<th>Item Name</th>
			<th>Date</th>
			<th>Challan No.</th>
			<th>Received From</th>
			<th>Qty Received</th>

			<th>Issued To</th>
			<th>Qty Issued</th>
			<th>Balance</th>
			
			
		</tr>
	</thead>
	<tbody>
	
		<?php if(empty($hostocks))
		{ ?>
		<tr><td colspan="10" class="text-center">Sorry Record Not Found</td></tr>
		<?php } else{?>
			<?php 
				$count = $this->uri->segment(3) + 1;
				
				foreach($hostocks as $hostock){
				$HOSTOCK_ID = $this->encrypt->encode($hostock['ITEM_ID']);
				?>
			<tr>
				
				<td><?php echo $count; ?></td>
				
				<td><?php echo $hostock['ITEM_NAME']; ?></td>
				<td><?php echo date('d/m/Y',strtotime($hostock['TRANSACTION_DATE'])); ?></td>
				<td><?php echo $hostock['CHALLAN_NO']; ?></td>
				<td><?php echo $hostock['RECEIVED_FROM']; ?></td>
				<td><?php echo $hostock['QTY_RECEIVED']; ?></td>

				<td><?php echo $hostock['ISSUED_TO']; ?></td>
				<td><?php echo $hostock['QTY_ISSUED']; ?></td>
				<td><?php echo $hostock['BALANCE']; ?></td>
			</tr>
			<?php $count++;
				} }?>
	</tbody>
</table>
</div>
</div>

</div>
</div>
</div>
</div>

