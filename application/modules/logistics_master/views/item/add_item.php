<div class="gapping"></div>
<div class="create_batch_form">
    <div id="box">
        <h2>Add Item</h2>
        		
	</div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                	
                	<!-- start message area -->

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>


<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info1')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
</div>
<?php } ?>

<?php if($this->session->flashdata('warning')) { ?>
<div class="alert alert-warning">
<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php } ?>

<!-- End message area --> 
                </div>
                <div class="panel-body">
                   <?php 
                    $attr = array('role'=>'form');
                    echo form_open('',$attr); ?>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
									<div class="col-lg-4">
										<label>Item Name: <span class="text-danger">*</span></label>
										<input class="form-control" name="ITEMNAME" value="<?php echo $this->input->post('ITEMNAME')?>" id="ITEMNAME" type="text"> 
										<span class="text-danger"><?php echo form_error('ITEMNAME'); ?></span>
									</div>
									<div class="col-lg-8" style="float:right">
										<label>Item Description: <span class="text-danger"></span></label>
										<input class="form-control" name="ITEMDESC" value="<?php echo $this->input->post('ITEMDESC')?>" id="ITEMDESC" type="text"> 
										<span class="text-danger"><?php echo form_error('ITEMDESC'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Item Price: <span class="text-danger">*</span></label>
										<input class="form-control" name="ITEMPRICE" id="ITEMPRICE" value="<?php echo $this->input->post('ITEMPRICE')?>" type="text"> 
										<span class="text-danger"><?php echo form_error('ITEMPRICE'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Item Type: <span class="text-danger"></span></label>
										<select class="form-control" style="width:100% ;" name="ITEMTYPE" id="ITEMTYPE">
                                                            <option value="15">Course Materials</option>
                                                            <option value="16"<?php echo ($this->input->post('ITEMTYPE')=="16") ? 'selected' : '';?>>Pads/Forms</option>
                                                            <option value="17"<?php echo ($this->input->post('ITEMTYPE')=="17") ? 'selected' : '';?>>Promotional Materials</option>
                                                            <option value="18"<?php echo ($this->input->post('ITEMTYPE')=="18") ? 'selected' : '';?>>Stationery</option>
                                                            <option value="0"<?php echo ($this->input->post('ITEMTYPE')=="0") ? 'selected' : '';?>>others</option>
                                                        </select>
										<span class="text-danger"><?php echo form_error('ITEMTYPE'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Item Unit: <span class="text-danger"></span></label>
										<select class="form-control" name="ITEMUNIT" id="ITEMUNIT">
												<?php
		                                     foreach($units as $unit) { ?>
		                                     <option value="<?php echo $unit['UNIT_ID']; ?>"<?php echo ($this->input->post('ITEMUNIT')==$unit['UNIT_ID']) ? 'selected' : '';?> ><?php echo $unit['UNIT_NAME']; ?></option>
		                                        <?php } ?>
		                                 </select>       
										<span class="text-danger"><?php echo form_error('ITEMUNIT'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Opening Quantity: <span class="text-danger"></span></label>
										<input class="form-control" name="OPENQUAN" id="OPENQUAN" type="text" value="<?php echo $this->input->post('OPENQUAN');?>"> 
										<span class="text-danger"><?php echo form_error('OPENQUAN'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Min Stock Level: <span class="text-danger">*</span></label>
										<input class="form-control" name="MINSTOCK" id="MINSTOCK" type="text" value="<?php echo $this->input->post('MINSTOCK');?>"> 
										<span class="text-danger"><?php echo form_error('MINSTOCK'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Max Quantity for Requisition: <span class="text-danger">*</span></label>
										<input class="form-control" name="MAXREQ" id="MAXREQ" type="text" value="<?php echo $this->input->post('MAXREQ')?>"> 
										<span class="text-danger"><?php echo form_error('MAXREQ'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Current Stock Level: <span class="text-danger"></span></label>
										<input class="form-control" name="CURRSTOCK" id="CURRSTOCK" type="text" value="<?php echo $this->input->post('CURRSTOCK');?>"> 
										<span class="text-danger"><?php echo form_error('CURRSTOCK'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Excise Duty: <span class="text-danger"></span></label>
										<input class="form-control" name="EXCDUTY" id="EXCDUTY" type="text" value="<?php echo $this->input->post('EXCDUTY');?>"> 
										<span class="text-danger"><?php echo form_error('EXCDUTY'); ?></span>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
                                        <br>
                                        <label class="form-check-inline">
                                        
                                        <input class="form-check-input" name="ISTAXAPPLICABLE" value="1" <?php if($this->input->post('ISTAXAPPLICABLE')==true) { echo 'checked';}?> id="ISTAXAPPLICABLE" type="checkbox"> Is Tax Applicable &nbsp;&nbsp; </label>
                                        </div>
                                        <div class="form-group">
                                        <label class="form-check-inline">
                                        <input class="form-check-input" name="ISACTIVE" value="1"  id="ISACTIVE" type="checkbox" checked="checked"> Is Active &nbsp;&nbsp; </label>
                                        </div>
                                    </div>
                                    
                                  
                                    <div class="col-md-4">
                                       
                                     <label>Vendors <span class="text-danger">*</span></label>
                                     <?php 
		                                    if($this->input->post('VENDORS_SELECT[]'))
		                                {
		                                	
                                        $selected_vendor = explode(",",$this->input->post('
                                        	VENDORS_SELECT'));
                                        
                                    	}
                                    	?>
		                                <select class="form-control course_interested" style="width:100%;height:158px" name="VENDORS_SELECT[]" id="VENDORS_SELECT" multiple="">
		                                     
                                    	<?php
		                                     $selected_vendor = $this->input->post('VENDORS_SELECT');
		                                      foreach($vendor as $vendors) {
                                              $isSelected = in_array($vendors['ID'],$selected_vendor) ? "selected='selected'" : "";  
                                              ?> 
		                                     <option value="<?php echo $vendors['ID']; ?>" <?php echo $isSelected; ?>><?php echo $vendors['VENDOR_NAME']; ?></option>
		                                    
                                              <?php } ?>
                                               <option value=''>Other</option>
		                                </select>
		                                <span class="text-danger"><?php echo form_error('VENDORS_SELECT[]'); ?></span>
                                    </div>


									</div>
                                </div>
                            </div>
							<br />
                            <button type='submit' class='btn btn-primary'>Save</button>&nbsp;
                        </div>
               <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="box">
		<?php
			// echo "<pre>";
			// print_r($vendords);
			// echo "</pre>";
            echo $pagination;
		?>
        <h2 class="text-center"> Item List</h2>
	</div>
    <div class="panel panel-default">
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-condensed table-stripped">
				<thead>
					<tr>
						<th>Action</th>
						<th>Sr. no.</th>
						
						<th>Item Name</th>
						<th>Item Price</th>
						<th>Item Type</th>
						<th>Unit</th>
						<th>Current Stock</th>
						
						<th>Excise Duty</th>
						
						<th>Is Tax Applicable</th>
						
						<th>Vendors</th>
						
						
					</tr>
				</thead>
				<tbody>
					<!-- <tr><td colspan="18" class="text-center">Sorry Record Not Found</td></tr> -->
						<?php 
							$count = $this->uri->segment(3) + 1;
							
							foreach($items as $item){
							$item_id = $this->encrypt->encode($item['ITEM_ID']);
							?>
						<tr>
							<td>
								<a href="<?php echo base_url('item/edit-item/'.$item_id); ?>"><span class="glyphicon glyphicon-edit"></span></a>
								
							</td>
							<td><?php echo $count; ?></td>
							
							<td><?php echo $item['ITEM_NAME']; ?>
								


							</td>
							<td><?php echo $item['ITEM_PRICE']; ?></td>
							<td><?php 
								// echo $vendor['VENDOR_TYPE']; 
								switch ($item['ITEM_TYPE']) {
									case 15:
										echo "Course Materials";
										break;
									case 16:
										echo "Pads/Forms";
										break;
									case 17:
										echo "Promotional Materials";
										break;		
									case 18:
										echo "Stationery";
										break;
                                    default:
										echo "others";
										break;
								}
							?>
							</td>
							<td><?php echo $item['UNIT']; ?></td>
							<td><?php echo $item['CURRENT_STOCK_LVL']; ?></td>
							
							<td><?php echo $item['EXCISE_DUTY']; ?></td>
							
							<td><?php echo $item['IS_TAX_APPLICABLE']; ?></td>
										
							<td>
								
								<?php
									
									$venNames = '';
        			 				for ($i=0; $i <count($item['VENDORS']) ; $i++) { 
        			 					$venNames .= $item['VENDORS'][$i]['VENDOR_NAME'].', ';
        			 				}
        			 				$venNamesTrim = rtrim($venNames,", ");
        			 				echo $venNamesTrim;
        			 			?>
							</td>
							
							<?php $count++;
							} ?>
						</tr>
						
				</tbody>
			</table>
		<br>
		</div>
        <div class="row">
            <div class="col-md-4 pull-right">
                
            </div>
        </div>
    </div>
</div>
