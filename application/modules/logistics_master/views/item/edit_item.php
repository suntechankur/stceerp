<div class="gapping"></div>
<div class="create_batch_form">
    <div id="box">
        <h2>Update Item</h2>
        <?php
			if($this->session->flashdata('msg'))
			{
				$msg = $this->session->flashdata('msg');
				echo $msg['msg'];
			}
		?>
	</div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align:center; color:#F00;"></div>
                <div class="panel-body">
                   <?php 
                    $attr = array('role'=>'form');
                    echo form_open('',$attr); ?>
                    <?php foreach($items as $items) { ?>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
									<div class="col-lg-4">
										<label>Item Name: <span class="text-danger">*</span></label>
										<input class="form-control" name="ITEMNAME" value="<?php echo $items['ITEM_NAME']; ?>" id="ITEMNAME" type="text"> 
										<span class="text-danger"><?php echo form_error('ITEMNAME'); ?></span>
									</div>
									<div class="col-lg-8" style="float:right">
										<label>Item Description: <span class="text-danger">*</span></label>
										<input class="form-control" name="ITEMDESC" value="<?php echo $items['ITEM_DESCRIPTION']; ?>" id="ITEMDESC" type="text"> 
										<span class="text-danger"><?php echo form_error('ITEMDESC'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Item Price: <span class="text-danger">*</span></label>
										<input class="form-control" name="ITEMPRICE" id="ITEMPRICE" value="<?php echo $items['ITEM_PRICE']; ?>" type="text"> 
										<span class="text-danger"><?php echo form_error('ITEMPRICE'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Item Type: <span class="text-danger">*</span></label>
										<select class="form-control" style="width:100% ;" name="ITEMTYPE" id="ITEMTYPE">
                                                            <option value="15" <?php if($items['ITEM_TYPE'] == '15'){echo 'selected';} ?> >Course Materials</option>
                                                            <option value="16" <?php if($items['ITEM_TYPE'] == '16'){echo 'selected';} ?> >Pads/Forms</option>
                                                            <option value="17" <?php if($items['ITEM_TYPE'] == '17'){echo 'selected';} ?> >Promotional Materials</option>
                                                            <option value="18" <?php if($items['ITEM_TYPE'] == '18'){echo 'selected';} ?> >Stationery</option>
                                                            <option value="0"  <?php if($items['ITEM_TYPE'] == '0'){echo 'selected';} ?>>others</option>
                                                        </select>
										<span class="text-danger"><?php echo form_error('ITEMTYPE'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Item Unit: <span class="text-danger">*</span></label>
										<select class="form-control" name="ITEMUNIT" id="ITEMUNIT">
												<?php

		                                     foreach($units as $unit) { ?>
		                                     <option value="<?php echo $unit['UNIT_ID'];?>" <?php echo ($items['UNIT'] == $unit['UNIT_NAME']) ? ' selected="selected"' : '';?>><?php echo $unit['UNIT_NAME']; ?></option>
		                                        <?php } ?>
		                                 </select>       
										<span class="text-danger"><?php echo form_error('ITEMUNIT'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Opening Quantity: <span class="text-danger">*</span></label>
										<input class="form-control" name="OPENQUAN" value="<?php echo $items['OPENING_QTY']; ?>" id="OPENQUAN" type="text"> 
										<span class="text-danger"><?php echo form_error('OPENQUAN'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Min Stock Level: <span class="text-danger">*</span></label>
										<input class="form-control" name="MINSTOCK" value="<?php echo $items['MIN_STOCK_LVL']; ?>" id="MINSTOCK" type="text"> 
										<span class="text-danger"><?php echo form_error('MINSTOCK'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Max Quantity for Requisition: <span class="text-danger">*</span></label>
										<input class="form-control" name="MAXREQ" value="<?php echo $items['MAX_QTY_FOR_REQUISITION']; ?>" id="MAXREQ" type="text"> 
										<span class="text-danger"><?php echo form_error('MAXREQ'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Current Stock Level: <span class="text-danger">*</span></label>
										<input class="form-control" name="CURRSTOCK" value="<?php echo $items['CURRENT_STOCK_LVL']; ?>" id="CURRSTOCK" type="text"> 
										<span class="text-danger"><?php echo form_error('CURRSTOCK'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Excise Duty: <span class="text-danger">*</span></label>
										<input class="form-control" name="EXCDUTY" value="<?php echo $items['EXCISE_DUTY']; ?>" id="EXCDUTY" type="text"> 
										<span class="text-danger"><?php echo form_error('EXCDUTY'); ?></span>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
                                        <br>
                                        <label class="form-check-inline">
                                        <input class="form-check-input" name="ISTAXAPPLICABLE" value="1" id="ISTAXAPPLICABLE" type="checkbox" <?php if($items['IS_TAX_APPLICABLE'] == '1'){echo 'checked';} ?> > Is Tax Applicable &nbsp;&nbsp; </label>
                                        </div>
                                        <div class="form-group">
                                        <label class="form-check-inline">
                                        <input class="form-check-input" name="ISACTIVE" value="1" id="ISACTIVE" type="checkbox" checked="checked" <?php if($items['IS_ACTIVE'] == '1'){echo 'checked';} ?>> Is Active &nbsp;&nbsp; </label>
                                        </div>
                                    </div>
                                    
                                  
                                    <div class="col-md-4">
                                       
                                     <label>Vendors <span class="text-danger">*</span></label>
		                                <select class="form-control course_interested" style="width:100%;height:158px" name="VENDORS_SELECT[]" id="VENDORS_SELECT" multiple="">
		                                     <?php
		                                     $selected_vendor = explode(",",$items['VENDORS']);
		                                      foreach($vendor as $vendors) {
                                              $isSelected = in_array($vendors['ID'],$selected_vendor) ? "selected='selected'" : "";  
                                              ?>  

		                                     <option value="<?php echo $vendors['ID']; ?>" <?php echo $isSelected; ?> ><?php echo $vendors['VENDOR_NAME']; ?></option>
		                                       <?php } ?>
                                               <option value=''>Other</option>
		                                </select>
		                                <span class="text-danger"><?php echo form_error('VENDORS_SELECT'); ?></span>
                                    </div>


									</div>
                                </div>
                            </div>
							<br />
                            <button type='submit' class='btn btn-primary'>Update</button>&nbsp;
                        </div>
                        <?php } ?>
               <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
