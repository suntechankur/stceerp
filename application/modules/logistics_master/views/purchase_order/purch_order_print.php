<?php 

tcpdf();
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('St. Angelos');
$pdf->SetTitle('Purchase order print');
$pdf->SetSubject('Print Purchase Order');
$pdf->SetKeywords('St.angelos purchase order print.');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// ---------------------------------------------------------

// add a page
$pdf->AddPage();

$purchOrdDate = date("d/m/Y", strtotime($vendorDets[0]['PURCHASE_ORDER_DATE']));
$delvDate = date("d/m/Y", strtotime($vendorDets[0]['DELIVERY_DATE']));

// create some HTML content

$html = '
        <div>
            <table width="640px" style="font-size: small; height: 1400px; padding:15px">
                <tr>
                    <td colspan="6" align="center">
                        <img src="'.base_url('resources/images/banner.png').'"><br />
                        <span class="style6">St. Angelo&#39;s Corporate Head Office, 6th and 7th Floor, Jyoti Plaza, Diagonally opp. Fire Brigade, S.V. Road, Kandivali (W), Mumbai-400 067. Maharashtra. India</span><br
                            class="style6" />
                        <span class="style6">Telephone No.: 022-6789 9000 Fax No.: 022-2861 7529 Email: enquiry@saintangelos.com
                        Website: www.saintangelos.com</span><strong> </strong>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="6">&nbsp; <span class="style1" style="text-decoration:underline"><strong>Purchase Order</strong></span>
                    </td>
                </tr>
                
                <tr>
                    <td align="left" colspan="2">
                        <strong>Vendor Name : </strong>
                        <span id="lblVendorName" style="display:inline-block;width:300px;">'.$vendorDets[0]['VENDOR_NAME'].'</span>
                    </td>
                     <td align="left" colspan="2">
                        <strong>Order Date: </strong>
                        <span id="lblOrderDate" style="display:inline-block;width:300px;">'.$purchOrdDate.'</span>
                    </td>
                    
                   <td align="left" colspan="2">
                        <strong>Order No: </strong>
                        <span id="lblOrderNo" style="display:inline-block;width:300px;">'.$vendorDets[0]['PURCHASE_ORDER_MAIN_TRANSACTION_ID'].'</span>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="4">
                        <strong>Address :</strong>
                        <span id="lblVendorAddress" style="display:inline-block;width:400px;">'.$vendorDets[0]['ADDRESS'].'</span>
                    </td>
                    <td align="left" colspan="2">
                        <strong>Delivery Details:</strong>
                        <span id="lblDeliveryDetails" style="display:inline-block;width:400px;">'.$vendorDets[0]['PURCHASE_ORDER_DELIVERY_DETAILS'].'</span>
                    </td>
                    
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <strong>Contact :</strong>
                        <span id="lblVendorContact" style="display:inline-block;width:300px;">'.$vendorDets[0]['MOBILE'].'</span>
                    </td>
                    <td align="left" >
                        &nbsp;</td>
                    <td align="left" >&nbsp;</td>
                    <td align="left" >&nbsp;
                    </td>
                    <td style="width: 140px" align="left">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <strong>Item:</strong>
                    <span id="lblItemCategory" style="display:inline-block;width:300px;">'.$vendorDets[0]['ITEM_CATEGORY'].'</span>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="style4" colspan="6">&nbsp;<span class="style14">Please supply the following items as per the specifications, terms and conditions mentioned below.&nbsp;:</span>
                    </td>
                </tr>
                <tr>
                    <td>    
                                <table border="1" width="850%" >
                                    <tr>
                                        <th style="width:5%;text-align:centre"><strong>Sr No.</strong>
                                        </th>
                                        <th style="width:20%;text-align:centre"><strong>Particulars</strong>
                                        </th>
                                        <th style="width:40%;text-align:centre"><strong>Description</strong>
                                        </th>
                                        <th style="width:10%;text-align:centre"><strong>Quantity</strong>
                                        </th>
                                        <th style="width:7%;text-align:centre"><strong>Unit</strong>
                                        </th>
                                        <th style="width:10%;text-align:centre"><strong>Rate/Unit</strong>
                                        </th>
                                        <th style="width:10%;text-align:centre"><strong>Total Amount</strong>
                                        </th>
                                    </tr>';
                            $i=1;
                            // $totAmtExcTax = 0;
                            foreach ($products as $product) {
                                $totAmt = $product['ITEM_UNIT_RATE'] * $product['ITEM_ORDER_QUANTITY'];
                                
                                // $totAmtExcTax += $totAmt;

                                $html .='<tr>
                                    <td>'.$i.'</td>
                                    <td>'.$product['ITEM_NAME'].' ('.$product['ITEM_USED_AT'].')</td>
                                    <td>
                                        <span id="Repeater1_ctl01_lblITEM_DESCRIPTION">'.$product['ITEM_DESCRIPTION'].'</span>
                                    </td>
                                    <td>
                                        <span id="Repeater1_ctl01_lblITEM_ORDER_QUANTITY">'.$product['ITEM_ORDER_QUANTITY'].'</span>
                                    </td>
                                    <td>
                                        <span id="Repeater1_ctl01_lblITEM_UNIT">'.$product['ITEM_UNIT'].'</span>
                                    </td>
                                    <td>
                                        <span id="Repeater1_ctl01_lblITEM_UNIT_RATE">'.$product['ITEM_UNIT_RATE'].'</span>
                                    </td>
                                    <td>
                                        <span id="Repeater1_ctl01_lblTOTAL_PRICE">'.$totAmt.'</span>
                                    </td>
                                </tr>';
                            $i++;}
                            
                            $html .= '</table>
                    </td>            
                </tr>            
                
                <table align:centre style="width:95%">    
                <tr >
                    <td colspan="5" align="right">Total Amount Exclusive Tax</td>
                    <td colspan="1" align="right">
                    <span id="lblTotalAmountExclTax" style="display:inline-block;font-weight: 700">'.$vendorDets[0]['TOTAL_AMOUNT_EXCLUSIVE_TAX'].'</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" align="right">
                        <table>';
                             if ($vendorDets[0]['VAT_PER'] != null || $vendorDets[0]['VAT_PER'] != '' || $vendorDets[0]['ST_PER'] != null || $vendorDets[0]['ST_PER'] != '') 
                             {
                                $html .= '<tr><td>';
                                if($vendorDets[0]['VAT_PER'] != null || $vendorDets[0]['VAT_PER'] != '')
                                {
                                    $html .=  'VAT @ <span id="lblVAT_PER">'.$vendorDets[0]['VAT_PER'].'%</span><br>';  
                                }

                                if ($vendorDets[0]['ST_PER'] != null || $vendorDets[0]['ST_PER'] != '') {
                                    $html .= 'Service Tax @ <span id="lblST_PER">'.$vendorDets[0]['ST_PER'].'%</span>';
                                }
                                $html .= '</td></tr>';
                            }                       
                       $html .= '</table> 
                    </td>
                    <td colspan="1" align="right">  
                        <table>    
                            <tr>
                               <td rowspan="2">
                                    <span id="lblTAX_AMOUNT" style="display:inline-block;width:100px;font-weight: 700">'.$vendorDets[0]['TAX_AMOUNT'].'</span>
                                </td>
                            </tr>
						</table>
                    </td>
                </tr>
                 <tr>
                    <td colspan="6">
                        <br />
                    </td>
                </tr>               
                <tr>
                    <td colspan="2" align="centre">Amount In Words:</td>
                    <td colspan="2" align="left">
                    <span id="lblGrandTotalInWords" style="font-weight: 700"><strong>'.numToStr(round($vendorDets[0]['GRAND_TOTAL'])).' Only</span></strong>
                    </td>
                    <td colspan="1" align="right">
                    Grand Total:</td>
                    <td colspan="1" align="right">
                    <span id="lblGRAND_TOTAL" style="display:inline-block;width:100px;font-weight: 700">'.round($vendorDets[0]['GRAND_TOTAL']).'</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <br />
                    </td>
                </tr>
                </table>
                <tr>
                    <td colspan="2" align="left" >
                        <span style="display:inline-block;width:200px;">Date Of Delivery:</span></td>
                    <td colspan="2" align="left">
                        <span id="lblDateOfDelivery" style="display:inline-block;width:200px;">'.$delvDate.'</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left" >
                        <span id="Label2" style="display:inline-block;width:105px;">Place Of Delivery:</span></td>
                    <td colspan="2" align="left">
                        <span id="lblPlaceOfDelivery" style="display:inline-block;width:200px;">'.$vendorDets[0]['CENTRE_NAME'].'</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left">
                        <span style="display:inline-block;width:105px;">Terms Of Payment:</span></td>
                    <td colspan="2" align="left">
                        <span id="lblTermaOfPayment" style="display:inline-block;width:200px;">As Per The Discussion</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                       
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left">
                        <span style="display:inline-block;width:105px;">Bill No.:</span></td>
                    <td colspan="2" align="left">
                        <span id="lblBillNo" style="display:inline-block;width:200px;">'.$vendorDets[0]['BILL_NUMBER'].'</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left">
                        <span style="display:inline-block;width:105px;">Challan No.:</span>
                        </td>
                    <td colspan="2" align="left">
                        <span id="lblChallanNo" style="display:inline-block;width:200px;"></span>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="6" >
                        <hr />
                        <strong>Note:</strong>
                    <br><br>
                    <span>* Delivery should be done free at respective site.</span><br />&nbsp;
                    <span>* Bill must be submitted in duplicates & the purchase order reference no. must be quoted on bill & challan.</span><br />&nbsp;
                    <span>* All bill should be drawn in the favour of St. Angelo&#39;s Computers Ltd.
                        </span><br />
                        <hr />
                    </td>


                </tr>
               
                 
                <tr>
                    <td colspan="2">
                    Service Tax No.:
                    <span id="lblSERVICE_TAX_NUMBER">AAECS9697JST001</span>
                    </td>
                    <td colspan="2" align="center">
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <strong>______________________</strong><br />
                        <strong>&nbsp;Received By</strong>
                    </td>
                    <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>For St. Angelo&#39;s Computers Ltd.</strong><br />
                        <br />
                        <br />
                        <br />
                        <strong>______________________</strong><br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <strong>Authorized Signatory</strong>
                    </td>
                </tr>
            </table>
        </div>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();




// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_006.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
?>