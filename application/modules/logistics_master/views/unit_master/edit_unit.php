<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
    <div class="create_batch_form">
        <div id="box">
            <h2>Update Unit</h2>
		</div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align:center; color:#F00;"> </div>
                    <div class="panel-body">
                        <?php 
							$attr = array('role'=>'form');
							echo form_open('',$attr); ?>
							<?php foreach($units as $unit) {?>
								<div class="row">
									<div class="col-lg-12">
										<div class="col-lg-4 form-group">
											<label>Unit Name: <span class="text-danger">*</span></label>
											<input class="form-control" name="UNIT_NAME" value="<?php echo $unit['UNIT_NAME']; ?>" id="UNIT_NAME" type="text"> 
											<span class="text-danger"><?php echo form_error('UNIT_NAME'); ?></span>
										</div>
																				
										<div class="col-lg-4 form-group">
											<label>Unit Description: <span class="text-danger">*</span></label>
											<textarea class="form-control" name="UNIT_DESC" id="UNIT_DESC" ><?php echo $unit['UNIT_DESC']; ?></textarea>
											<span class="text-danger"><?php echo form_error('UNIT_DESC'); ?></span>
										</div>
										<div class="col-lg-12 form-group"><br>
											<button type='submit' class='btn btn-primary'>Update</button>
										</div>
									</div>
								</div>
							<?php } ?>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>