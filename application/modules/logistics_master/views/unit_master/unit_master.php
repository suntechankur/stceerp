<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
    <div class="create_batch_form">
        <div id="box">
            <h2>Unit Master</h2>
		</div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align:center; color:#F00;"> </div>
                    <div class="panel-body">
                        <?php 
							$attr = array('role'=>'form');
							echo form_open('unit-master/unit-master',$attr); ?>
								<div class="row">
									<div class="col-lg-12">
										<div class="col-lg-4 form-group">
											<label>Unit Name: <span class="text-danger">*</span></label>
											<input class="form-control" name="UNIT_NAME" value="" id="UNIT_NAME" type="text"> 
											<span class="text-danger"><?php echo form_error('UNIT_NAME'); ?></span>
										</div>
										
										
										<div class="col-lg-4 form-group">
											<label>Unit Description: <span class="text-danger">*</span></label>
											<textarea class="form-control" name="UNIT_DESC" id="UNIT_DESC" style=""></textarea>
											<span class="text-danger"><?php echo form_error('UNIT_DESC'); ?></span>
										</div>
										<div class="col-lg-12 form-group"><br>
											<button type='submit' name="submit" value="save" class='btn btn-primary'>Save</button>
										</div>
									</div>
								</div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<br>
    <div id="box">
        <h2 class="text-center"> Unit List</h2>
	</div>
    <div class="panel panel-default">
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-condensed table-stripped">
				<thead>
					<tr>
						<th>Action</th>
						<th>Sr. No.</th>
						<th>Unit Name</th>
						<th>Unit Description</th>
					</tr>
				</thead>
				<tbody>
					
						<?php 
							$count = $this->uri->segment(2) + 1;
							foreach($units as $unit){ 
							
							$UNIT_ID = $this->encrypt->encode($unit['UNIT_ID']);
							?>
						<tr>
							<td>
								<a href="<?php echo base_url('unit-master/edit-unit/'.$UNIT_ID); ?>"><span class="glyphicon glyphicon-edit"></span></a>
								<a href="<?php echo base_url('unit-master/del-unit/'.$UNIT_ID) ?>" onclick="return confirm('Are you sure to Delete this ?')"><span class="glyphicon glyphicon-trash"></span></a>
							</td>
							<td><?php echo $count; ?></td>
							<td><?php echo $unit['UNIT_NAME']; ?></td>
							<td><?php echo empty($unit['UNIT_DESC']) ? "N/a" : $unit['UNIT_DESC']; ?></td>
							
							<?php $count++;
							} ?>
						</tr>
						
				</tbody>
			</table>
		<br>
		</div>
        
    </div>
</div>