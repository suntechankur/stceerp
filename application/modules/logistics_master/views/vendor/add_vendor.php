<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
    <div id="box">
        <h2>Add Vendor</h2>
        <?php
			if($this->session->flashdata('msg'))
			{
				$msg = $this->session->flashdata('msg');
				echo $msg['msg'];
			}
		?>
	</div>
    <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align:center; color:#F00;"> </div>
                    <div class="panel-body">
                        <?php 
							$attr = array('role'=>'form');
							echo form_open('',$attr); ?>
								<div class="row">
									<div class="col-lg-8">
										<div class="col-lg-6 form-group">
											<label>Vendor Name: <span class="text-danger">*</span></label>
											<input class="form-control" name="VENDOR_NAME" value="" id="VENDOR_NAME" type="text" required /> 
											<span class="text-danger"><?php echo form_error('VENDOR_NAME'); ?></span>
										</div>
										<div class="col-lg-6 form-group">
											<label>Company Name: <span class="text-danger">*</span></label>
											<input class="form-control" name="COMPANY_NAME" value="" id="COMPANY_NAME" type="text" required /> 
											<span class="text-danger"><?php echo form_error('COMPANY_NAME'); ?></span>
										</div>
										<div class="col-lg-6 form-group">
											<label>Contact No 1: <span class="text-danger">*</span></label>
											<input class="form-control" value="" onkeypress="return isNumber(event)" name="TEL" id="TEL" style="width:100%" maxlength="10" size="10" type="text" required> 
											<span class="text-danger"><?php echo form_error('TEL'); ?></span>
										</div>
										<div class="col-lg-6 form-group">
											<label>Contact No 2: </label>
											<input class="form-control" value="" onkeypress="return isNumber(event)" name="MOBILE" id="MOBILE" style="width:100%" maxlength="10" size="10" type="text"> 
											<span class="text-danger"><?php echo form_error('MOBILE'); ?></span>
										</div>
									</div>
									<div class="col-lg-4 form-group">
										<label>Address: <span class="text-danger">*</span></label>
										<textarea class="form-control" name="ADDRESS" id="ADDRESS" style="height:108px" required></textarea>
										<span class="text-danger"><?php echo form_error('ADDRESS'); ?></span>
									</div>
								</div>
								<div class="col-lg-4 form-group">
									<label>Fax No.: </label>
									<input class="form-control" value="" onkeypress="return isNumber(event)" name="FAX_NO" id="FAX_NO" style="width:100%" maxlength="10" size="10" type="text"> 
									<span class="text-danger"><?php echo form_error('FAX_NO'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>E-mail 1: <span class="text-danger">*</span></label>
									<input class="form-control" name="EMAIL1" id="EMAIL1" value="" type="text"> 
									<span class="text-danger"><?php echo form_error('EMAIL1'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>E-mail 2: </label>
									<input class="form-control" name="EMAIL2" id="EMAIL2" value="" type="text"> 
									<span class="text-danger"><?php echo form_error('EMAIL2'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>Vendor Type: </label>
									<select class="form-control" name="VENDOR_TYPE" id="VENDOR_TYPE">
										<option value="1">Wholesaler</option>
										<option value="2">Broker</option>
										<option value="3">Sub-broker</option>
										<option value="4">Manufacturer</option>
										<option value="5">Retailer</option>
										<option value="6">Other</option>
										
									</select>
									<span class="text-danger"><?php echo form_error('VENDOR_TYPE'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>Income Tax Number: </label>
									<input class="form-control" name="INCOME_TAX_NUMBER" id="INCOME_TAX_NUMBER" type="text"> 
									<span class="text-danger"><?php echo form_error('INCOME_TAX_NUMBER'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>CST / TIN Number: </label>
									<input class="form-control" name="CST_NUMBER" id="CST_NUMBER" type="text"> 
									<span class="text-danger"><?php echo form_error('CST_NUMBER'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>Service Tax Number: </label>
									<input class="form-control" name="SERVICE_TAX_NUMBER" id="SERVICE_TAX_NUMBER" type="text"> 
									<span class="text-danger"><?php echo form_error('SERVICE_TAX_NUMBER'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>ECC Number: </label>
									<input class="form-control" name="ECC_NUMBER" id="ECC_NUMBER" type="text"> 
									<span class="text-danger"><?php echo form_error('ECC_NUMBER'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>VAT / TIN Number: </label>
									<input class="form-control" name="VAT_TIN_NUMBER" id="VAT_TIN_NUMBER" type="text"> 
									<span class="text-danger"><?php echo form_error('VAT_TIN_NUMBER'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>Applicable Tax: </label>
									<select class="form-control" name="TAX_ID" id="TAX_ID">
										<option value="">Select Applicable Tax</option>
										<?php foreach($applicable_tax as $applicable_taxs) { ?>
										<option value="<?php echo $applicable_taxs['TAX_ID']; ?>"><?php echo $applicable_taxs['TAX_NAME']; ?></option>
										<?php } ?>
									</select>
									<span class="text-danger"><?php echo form_error('TAX_ID'); ?></span>
								</div>
							<br />
							
							<button type='submit' class='btn btn-primary'>Save</button>&nbsp;
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
</div>
<br>

<span class=""></span>
<div id="box">
		
        <h2 class="text-center"> Vendor List</h2>
	</div>
    <div class="panel panel-default">
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-condensed table-stripped">
				<thead>
					<tr>
						<th>Action</th>
						<th>Sr. no.</th>
						
						<th>Vendor Name</th>
						<th>Company Name</th>
						<th>Address</th>
						<th>Telephone</th>
						<th>Mobile</th>
						
						<th>Email-ID</th>
						
						<th>Vendor Type</th>
						
						<th>CST Number</th>
						<th>Service Tax Number</th>
						<th>ECC Number</th>
						
						
						<th>VAT/TIN Number</th>
						<th>Applicable Tax</th>
						
					</tr>
				</thead>
				<tbody>
					<!-- <tr><td colspan="18" class="text-center">Sorry Record Not Found</td></tr> -->
						<?php 
							$count = $this->uri->segment(2) + 1;
							foreach($vendors as $vendor){ 
							//unset($vendor['vendor_masterid']);
							$vendor_id = $this->encrypt->encode($vendor['vendor_masterid']);
							?>
						<tr>
							<td>
								<a href="<?php echo base_url('vendor/edit-vendor/'.$vendor_id); ?>"><span class="glyphicon glyphicon-edit"></span></a>
								<a href="<?php echo base_url('vendor/del-vendor/'.$vendor_id) ?>"><span class="glyphicon glyphicon-trash"></span></a>
							</td>
							<td><?php echo $count; ?></td>
							
							<td><?php echo $vendor['VENDOR_NAME']; ?></td>
							<td><?php echo $vendor['COMPANY_NAME']; ?></td>
							<td><?php echo $vendor['ADDRESS']; ?></td>
							<td><?php echo empty($vendor['TEL']) ? "N/a":$vendor['TEL']; ?></td>
							<td><?php echo $vendor['MOBILE']; ?></td>
							
							<td><?php echo $vendor['EMAIL1']; ?></td>
							
							<td><?php 
								// echo $vendor['VENDOR_TYPE']; 
								switch ($vendor['VENDOR_TYPE']) {
									case 1:
										echo "Wholesaler";
										break;
									case 2:
										echo "Broker";
										break;
									case 3:
										echo "Sub-broker";
										break;		
									case 4:
										echo "Manufacturer";
										break;
                                    case 5:
										echo "Retailer";
										break;
									case 6:
										echo "Other";
										break;	

									default:
										echo "";
										break;
								}
							?>
							</td>
							
							<td><?php echo $vendor['CST_NUMBER']; ?></td>
							<td><?php echo $vendor['SERVICE_TAX_NUMBER']; ?></td>
							<td><?php echo $vendor['ECC_NUMBER']; ?></td>
							
							<td><?php echo $vendor['VAT_TIN_NUMBER']; ?></td>
							<td><?php echo $vendor['TAX_NAME']; ?></td>
							<td><?php echo ($vendor['IS_ACTIVE'] == '1') ? '<p style="color:green">Active</p>':'<p style="color:red">Inactive</p>'; ?></td>  
							<?php $count++;
							} ?>
						</tr>
						
				</tbody>
			</table>
		<br>
		</div>
        <div class="row">
            <div class="col-md-4 pull-right">
                
            </div>
        </div>



    </div>
</div>
