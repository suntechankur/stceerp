
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Change_password_controller extends MX_Controller {
	public function __construct()
        {
                $this->load->model('change_password_model');
                parent::__construct();
        }

	public function change_password(){

		$this->form_validation->set_rules('OLD_PASSWORD', 'old_Password', 'required');
		$this->form_validation->set_rules('NEW_PASSWORD', 'new_Password', 'required');
		$this->form_validation->set_rules('CONFIRM_PASSWORD', 'confirm_Password', 'required');

		$old_password = $this->input->post('OLD_PASSWORD');
		$new_password = $this->input->post('NEW_PASSWORD');
		$confirm_password = $this->input->post('CONFIRM_PASSWORD');
		//$passwd = $this->change_password_model->update_password($new_password,$old_password);
		//echo($passwd);
  		if($this->form_validation->run() == TRUE){
          	if($new_password == $confirm_password){
         	 	if($this->change_password_model->update_password($new_password,$old_password)){
               		$this->session->set_flashdata('success','Password Updated Succesfully.');
               		$this->session->unset_userdata('admin_data');
       				$this->session->sess_destroy();
        			redirect(base_url());
         	 	}
         		else{
          			$this->session->set_flashdata('danger', 'Sorry! Current Password is not matching.');
		  					redirect(base_url('change-password'));
          		}
          	}
          	else{
          		$this->session->set_flashdata('failed','New password & confirm_password is not matching.');
			 				redirect(base_url('change-password'));
          	}
		}

		$this->load->admin_view('change_password_view');
	}
}
?>
