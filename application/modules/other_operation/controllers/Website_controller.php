
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website_controller extends MX_Controller {
	public function __construct()
        {
                $this->load->model('website_model');
                parent::__construct();
        }

	public function manage_testimonial(){
		$data['testimonial_video'] = $this->website_model->get_website_testimonial_video_details();

		$this->form_validation->set_rules('video_link', 'Video Link', 'required');
  		if($this->form_validation->run() == TRUE){
				$insertdata = array(
						'youtube_link' => "https://www.youtube.com/embed/".$_POST['video_link'],
						'added_datetime' => date("Y-m-d H:i", strtotime(date('Y-m-d'))),
						'is_active' => '1');
					$result = $this->website_model->add_testimonial_video($insertdata);
					if($result){
						redirect(base_url('manage-website-testimonial'));
					}
			}

		$this->load->admin_view('manage_testimonial_video',$data);
	}

	public function delete_testimonial_video($video_id){
		$result = $this->website_model->delete_testimonial_video($video_id);
		if($result){
			redirect(base_url('manage-website-testimonial'));
		}
	}
}
?>
