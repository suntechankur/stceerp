<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website_model extends CI_Model {
    public function __construct() {
        $this->website_db = $this->load->database('website_db',TRUE);
        parent::__construct();
    }
     public function get_website_testimonial_video_details(){
         $query = $this->website_db->select('*')
                           ->from('testimonial_videos TV')
                           ->get();
         // echo $this->db->last_query();
         return $query->result_array();
      }
     public function add_testimonial_video($video_link){
       $this->website_db->insert('testimonial_videos',$video_link);
       if($this->website_db->affected_rows() > 0){
         return true;
       }
       else{
         return false;
       }
    }
     public function delete_testimonial_video($video_id){
       $this->website_db->where('test_video_id',$video_id)
                ->set('is_active',0)
                ->update('testimonial_videos');    // for mysql
       return 1;
    }
}
