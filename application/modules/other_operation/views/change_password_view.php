<br>
<div class="col-sm-12"><div id="box">
 <h2>Change Password</h2> 
 	</div>
		<!-- massage area start-->
<div class="panel-heading">

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

</div>
		<!-- massage area end -->
<div class="panel panel-default">
<div class="panel-body">
<div class="col-sm-12">
	<?php echo form_open(); ?>
	<div class="col-md-4">
		<div class="form-group">
			<label>Old Password:</label>
			<input type="Password" name="OLD_PASSWORD" class="form-control" value="" placeholder="Old Password">
			<!-- <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span> -->
			<span style="color:red;"><?php echo form_error('OLD_PASSWORD'); ?></span>
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>New Password:</label>
			<input type="Password" name="NEW_PASSWORD" class="form-control" value="" placeholder="New Password">
			<!-- <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span> -->
			 <span style="color:red;"><?php echo form_error('NEW_PASSWORD'); ?></span>						
		</div>
	</div>
	<div class="col-md-4">
		<div class="form-group">
			<label>Confirm New Password:</label>
			<input type="Password" name="CONFIRM_PASSWORD" class="form-control" value="" placeholder="Password Confirmation">
			<!-- <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span> -->
			<span style="color:red;"><?php echo form_error('CONFIRM_PASSWORD'); ?></span>	
		</div>
	</div>
</div><br><br>
	
	<button type="submit" id="submit" name="submit" class="btn btn-primary center-block" style="width:200px;">Change Password</button>
	

	<?php echo form_close(); ?>	

</div>
</div>
</div>


