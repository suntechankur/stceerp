<div class="col-md-12 col-sm-12">
<div class="create_batch_form">
<div id="box">
<h2>Manage Testimonial Video</h2>
</div>
				<div class="row">
						<div class="col-sm-12">
								<div class="panel panel-default">
                    <div class="panel-heading">
											<!-- start message area -->

										<?php if($this->session->flashdata('danger')) { ?>
										<div class="alert alert-danger">
										<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
										</div>
										<?php } ?>

										<?php if($this->session->flashdata('msg')) { ?>
										<div class="alert alert-success">
										<strong>Success!</strong> <?php $msg = $this->session->flashdata('msg'); echo $msg['msg']; ?>
										</div>
										<?php } ?>

										<?php if($this->session->flashdata('success')) { ?>
										<div class="alert alert-success">
										<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
										</div>
										<?php } ?>


										<?php if($this->session->flashdata('failed')) { ?>
										<div class="alert alert-info">
										<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
										</div>
										<?php } ?>

										<?php if($this->session->flashdata('info')) { ?>
										<div class="alert alert-info">
										<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
										</div>
										<?php } ?>

										<?php if($this->session->flashdata('info1')) { ?>
										<div class="alert alert-info">
										<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
										</div>
										<?php } ?>

										<?php if($this->session->flashdata('warning')) { ?>
										<div class="alert alert-warning">
										<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
										</div>
										<?php } ?>

										<!-- End message area -->
                  </div>
									<div class="panel-body">
                    <?php echo form_open(); ?>
											<div class="col-md-12">
												<div class="form-group">
													<div class="row" style="margin-left:-30px;">
														<div class="col-lg-12" style="margin-left:0px;">
															<div class="col-lg-12">
																<label>Video Link:</label>
																<input class="form-control" value="" name="video_link" id="video_link" type="text" placeholder="https://www.youtube.com/watch?v= 'sqZhlK3MvFU' <- Copy this value from youtube and paste here">
															</div>
														</div>
													</div>
												</div>
                      </div>

	                      <div class="form-group">
	                          <div class="row" style="margin-left:-30px;">
	                              <div class="col-lg-12" style="margin-left:0px;">
																		<div class="col-lg-3">
	                                      <button type="submit" value="submit" id="submit" class="btn btn-primary" style="float:right">Add</button>
	                                  </div>
	                                  <div class="col-lg-3">
	                                      <button type="reset" name="reset" id="reset" class="btn btn-primary" style="float:left">Reset</button>
	                                  </div>
																		<div class="col-lg-6">
																			<a href="https://suntechedu.com/testimonials" target="_blank"><button type="button" name="testimonial_redirect" id="testimonial_redirect" class="btn btn-warning" style="float:left;width:300px;">Click here to see where it will shown</button></a>
																	</div>
	                              </div>
	                          </div>
	                      </div>
											</div>
                      <?php echo form_close(); ?>
									</div>
								</div>
							</div>
							</div>
							</div>

							<div id="box"> <p>&nbsp;</p>
				        <h2 class="text-center"> Video List</h2>
				        <!--        <div class="row"><div class="col-md-4 pull-right"></div></div>--></div>
						    <div class="panel panel-default">
						        <!--<div class="panel-heading">Form Elements</div>-->
						        <div class="panel-body table-responsive">
						            <table class="table table-bordered table-condensed table-stripped" id="export_to_execl_view_enquiry">
						                <thead>
						                    <tr>
						                        <th>Action</th>
						                        <th>Video Link</th>
						                        <th>Status</th>
						                    </tr>
						                </thead>
						                <tbody>
						                     <?php
																$count = $this->uri->segment(2) + 1;
																foreach($testimonial_video as $video){
						                    	// $enquiry_id = $this->encrypt->encode($enquiry['ENQUIRY_ID']);
						                    	$video_id = $video['test_video_id'];
						                    	?>
						                        <tr>
						                            <td>
						                              <!-- <span data-url="<?php // echo base_url('enquiry/del-enquiry/'.$enquiry_id) ?>" data-method="delete" data-popup_header="Delete Enquiry" data-message="Are you sure you want to delete this entry?" class="glyphicon glyphicon-trash action pointer"></span> -->
						                            <!-- </td> -->
						                            <!-- <td> -->
						                              <!-- <a href="<?php //echo base_url('enquiry/edit-enquiry/'.$enquiry_id); ?>"><span class="glyphicon glyphicon-edit"></span></a> -->
						                              <a href="<?php echo base_url('del-testimonial-video/'.$video_id) ?>" onclick="return confirm('Are you sure to delete this?')"><span class="glyphicon glyphicon-trash"></span></a>
						                            </td>
						                            <td><?php echo $video['youtube_link']; ?></td>
						                            <td><?php echo ($video['is_active'] == "1")?'<span style="color:green;font-weight:bold;">Active</span>':'<span style="color:red;font-weight:bold;">Inactive</span>'; ?></td>
						                        </tr>
						                        <?php $count++; } ?>
						                </tbody>
						            </table>
						            <br> </div>
						    </div>
							</div>
