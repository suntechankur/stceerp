<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Placement_controller extends MX_Controller {

    public function __construct(){
            $this->load->model('admission/admission_model');
            $this->load->model('tele_enquiry/tele_enquiry_model');
            $this->load->model('placement_model');
            parent::__construct();
    }

    public function add_employer_details(){
      $data['placement_details'] = $this->placement_model->get_employer_details();
      $data['job_designation'] = $this->placement_model->get_company_job_designation();
      if(isset($_POST['add_job_details'])){
        $this->form_validation->set_rules('NAME_OF_COMPANY', 'Name of Company', 'required');
        $this->form_validation->set_rules('EMAIL_ID', 'Email Id', 'required|valid_email');
        $this->form_validation->set_rules('WEBSITE', 'Website', 'required');
        $this->form_validation->set_rules('JOB_DESIGNATION', 'Job Designation', 'required');
        $this->form_validation->set_rules('NO_OF_PEOPLE_REQ', 'No of People required', 'required');
        $this->form_validation->set_rules('GENDER', 'Gender', 'required');
        $this->form_validation->set_rules('JOB_PROFILE', 'Job Profile', 'required');
        $this->form_validation->set_rules('COMPANY_ADDRESS', 'Company Address', 'required');
        if($this->form_validation->run() != FALSE){
          unset($_POST['add_job_details']);
          $_POST['MODIFIEDBY'] = $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'];
          $_POST['MODIFIEDDATE'] = date("Y-m-d H:i:s");
          $_POST['IS_POSTED_IN_STUDENTDESK'] = "YES";
          $_POST['ISACTIVE'] = "0";
          $company_add = $this->placement_model->add_employer_details($_POST);
          if($company_add){
            $this->session->set_flashdata('success',"Company Details Added Successfully.");
            redirect(base_url('placement/add-employer-details'));
          }
          else{
            $this->session->set_flashdata('danger',"Kindly Check the details are feeded properly or not.");
            redirect(base_url('placement/add-employer-details'));
          }
        }
        else{
          $this->session->set_flashdata('danger',"Kindly Fill All Required Details Properly.");
          redirect(base_url('placement/add-employer-details'));
        }
      }
      $this->load->admin_view("add_employer_details",$data);
    }

    public function update_company_job_status(){
      $status = $this->input->post('status');
      $company_id = $this->input->post('company_id');
      $status = $this->placement_model->activate_or_deactivate_job($company_id,$status);
      echo json_encode($status);
    }

    public function delete_company_details(){
      $company_id = $this->input->post('company_id');
      $status = $this->placement_model->delete_company_job($company_id);
      echo json_encode($status);
    }

    public function student_search(){
      $data['courses'] = $this->tele_enquiry_model->all_courses();
      $data['modules'] = $this->tele_enquiry_model->all_modules();
      $data['student_details'] = array();
      if(isset($_POST['search_student_details'])){
        if(($_POST['admission_id'] != "") || ($_POST['first_name'] != "") || ($_POST['last_name'] != "")){
          unset($_POST['search_student_details']);
          $data['student_details'] = $this->placement_model->get_admission_details($_POST);
          unset($_POST);
          for($i=0;$i<count($data['student_details']);$i++){
              $data['student_details'][$i]['FEES_PAID'] = $this->admission_model->getFeesPaidByByAdmissionId($data['student_details'][$i]['ADMISSION_ID']);
              if($data['student_details'][$i]['FEES_PAID'] == ""){
                $data['student_details'][$i]['FEES_PAID']  = 0;
              }
          }
        }
      }
      $this->load->admin_view("student_search",$data);
    }

    public function download_resume(){
      $data['resumes'] = $this->placement_model->get_students_resume();
      $data['courses'] = $this->tele_enquiry_model->all_courses();
      $data['modules'] = $this->tele_enquiry_model->all_modules();
      $data['job_designation'] = $this->placement_model->get_company_job_designation();
      $data['centres'] = $this->tele_enquiry_model->suggested_centre();
      if(isset($_POST['search_student_resume'])){
          unset($_POST['search_student_resume']);
          $data['resumes'] = $this->placement_model->get_students_resume($_POST);
      }

      for($i=0;$i<count($data['resumes']);$i++){
        $remarks = $this->placement_model->get_students_placement_remarks($data['resumes'][$i]['ADMISSION_ID'],"normal");
        if(!empty($remarks)){
          $data['resumes'][$i]['PLACEMENT_STATUS'] = $remarks[0]['STATUS'];
          $data['resumes'][$i]['PLACEMENT_REMARKS'] = $remarks[0]['REMARKS'];
        }
        else{
          $data['resumes'][$i]['PLACEMENT_STATUS'] = "No Status";
          $data['resumes'][$i]['PLACEMENT_REMARKS'] = "Need to Followup.";
        }
      }
      $this->load->admin_view("download_student_resume",$data);
    }

    public function placement_remarks($admission_id){
      $admission_id = $this->encrypt->decode($admission_id);
      $data['courses'] = $this->tele_enquiry_model->all_courses();
      $data['modules'] = $this->tele_enquiry_model->all_modules();
      $data['enq_handled_by'] = $this->tele_enquiry_model->enquiry_handled_by();

      $student['admission_id'] = $admission_id;
      $data['student_details'] = $this->placement_model->get_admission_details($student);
      $data['placement_remarks'] = $this->placement_model->get_students_placement_remarks($admission_id,"remarks");

      // for blocking interview option internal process dropdown
      $data['process'] = array();
      foreach($data['placement_remarks'] as $remarks){
        if(($remarks['INTERNAL_PROCESS'] == 'Interview 1') || ($remarks['INTERNAL_PROCESS'] == 'Interview 2') || ($remarks['INTERNAL_PROCESS'] == 'Interview 3')){
          array_push($data['process'],$remarks['INTERNAL_PROCESS']);
        }
      }
      // end of array making for blocking interview option internal process dropdown
      
      if(isset($_POST['add_placement_remark'])){
        unset($_POST['add_placement_remark']);

        $remDateRep = str_replace('/', '-', $_POST['REMARKS_DATE']);
        $_POST['REMARKS_DATE'] = date("Y-m-d", strtotime($remDateRep));
        $added_remark = $this->placement_model->add_student_remarks($_POST);
        if($added_remark){
          $this->session->set_flashdata('success','Record Added Succesfully');
          redirect(base_url('placement/download-resume'));
        }
        else{
          $this->session->set_flashdata('success','Kindly Fill all the Details');
          redirect(base_url('placement/download-resume'));
        }
      }
      $this->load->admin_view("placement_remarks",$data);
    }

    public function search_employer_details(){
      $data['placement_details'] = $this->placement_model->get_employer_details();
      $data['job_designation'] = $this->placement_model->get_company_job_designation();
      if(isset($_POST['get_job_details'])){
          unset($_POST['get_job_details']);
          $data['placement_details'] = $this->placement_model->get_employer_details($_POST);
      }
      $this->load->admin_view("search_employers",$data);
    }

    public function update_employer_details(){
      $company_id = $_POST['COMPANY_ID'];
      unset($_POST['COMPANY_ID']);
      unset($_POST['update_company_info']);
      $_POST['MODIFIEDBY'] = $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'];
      $_POST['MODIFIEDDATE'] = date("Y-m-d H:i:s");

      $update_details = $this->placement_model->update_employer_details($_POST,$company_id);
      if($update_details){
        $this->session->set_flashdata('success','Company Details Updated Succesfully');
        redirect(base_url('placement/employer-details'));
      }
      else{
        $this->session->set_flashdata('danger','Kindly check all details are proper or not.');
        redirect(base_url('placement/employer-details'));
      }
    }
}
