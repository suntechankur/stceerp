<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Placement_model extends CI_Model {

  public $sql;
  public function __construct() {
      parent::__construct();
  }

  public function add_employer_details($data){
    $this->db->insert('company_info',$data);   // for mysql
    if($this->db->affected_rows() > 0){
      return $this->db->insert_id();
    }
    else{
      return false;
    }
  }

  public function get_employer_details($data = array()){
    $query = $this->db->select('*')
                      ->from('company_info');
                      if(!empty($data)){
                        if(isset($data['NAME_OF_COMPANY'])){
                          if ($data['NAME_OF_COMPANY'] != '') {
                            $query = $this->db->like('NAME_OF_COMPANY',$data['NAME_OF_COMPANY'],'after');
                          }
                        }
                        if(isset($data['NAME_OF_HR'])){
                          if ($data['NAME_OF_HR'] != '') {
                            $query = $this->db->like('NAME_OF_HR',$data['NAME_OF_HR'],'after');
                          }
                        }
                        if(isset($data['EMAIL_ID'])){
                          if ($data['EMAIL_ID'] != '') {
                            $query = $this->db->like('EMAIL_ID',$data['EMAIL_ID'],'after');
                          }
                        }
                        if(isset($data['CONTACT_NO_LANDLINE'])){
                          if ($data['CONTACT_NO_LANDLINE'] != '') {
                            $query = $this->db->like('CONTACT_NO_LANDLINE',$data['CONTACT_NO_LANDLINE'],'after');
                          }
                        }
                        if(isset($data['CONTACT_NO_MOBILE'])){
                          if ($data['CONTACT_NO_MOBILE'] != '') {
                            $query = $this->db->like('CONTACT_NO_MOBILE',$data['CONTACT_NO_MOBILE'],'after');
                          }
                        }
                        if(isset($data['WEBSITE'])){
                          if ($data['WEBSITE'] != '') {
                            $query = $this->db->like('WEBSITE',$data['WEBSITE'],'after');
                          }
                        }
                        if(isset($data['JOB_DESIGNATION'])){
                          if ($data['JOB_DESIGNATION'] != '') {
                            $query = $this->db->where('JOB_DESIGNATION',$data['JOB_DESIGNATION']);
                          }
                        }
                        if(isset($data['STREAM'])){
                          if ($data['STREAM'] != '') {
                            $query = $this->db->where('STREAM',$data['STREAM']);
                          }
                        }
                        if(isset($data['JOB_PROFILE'])){
                          if ($data['JOB_PROFILE'] != '') {
                            $query = $this->db->like('JOB_PROFILE',$data['JOB_PROFILE'],'both');
                          }
                        }
                        if(isset($data['COMPANY_ADDRESS'])){
                          if ($data['COMPANY_ADDRESS'] != '') {
                            $query = $this->db->like('COMPANY_ADDRESS',$data['COMPANY_ADDRESS'],'both');
                          }
                        }
                      }
    $query = $this->db->order_by('COMPANY_ID','DESC')
                      ->get();

    return $query->result_array();
  }

  public function get_company_job_designation(){
    $query = $this->db->select('*')
                      ->from('control_file')
                      ->where('CONTROLFILE_KEY','STUDENT_DESIGNATION')
                      ->order_by('CONTROLFILE_VALUE')
                      ->get();

    return $query->result_array();
  }

  public function activate_or_deactivate_job($company_id,$status){
    $this->db->set('ISACTIVE',$status)
             ->where('COMPANY_ID',$company_id)
             ->update('company_info');

    return $this->db->affected_rows();
  }

  public function delete_company_job($company_id){
    $this->db->where('COMPANY_ID',$company_id)
             ->delete('company_info');
    // echo $this->db->last_query();
    return $this->db->affected_rows();
  }

  public function get_admission_details($data){
    $query = $this->db->select('AM.ADMISSION_ID,concat(EM.ENQUIRY_FIRSTNAME," ",EM.ENQUIRY_LASTNAME) as Student_name,AM.ADMISSION_DATE,CM.CENTRE_NAME,AM.COURSE_TAKEN,AM.MODULE_TAKEN,AM.TOTALFEES,concat(if(EM.ENQUIRY_TELEPHONE IS NULL,EM.ENQUIRY_PARENT_NO,EM.ENQUIRY_TELEPHONE)," ", EM.ENQUIRY_MOBILE_NO) as enquiry_contact,EM.ENQUIRY_EMAIL,AM.STATUS,AM.STREAM')
                      ->from('admission_master AM')
                      ->join('centre_master CM','CM.CENTRE_ID = AM.CENTRE_ID')
                      ->join('enquiry_master EM','EM.ENQUIRY_ID = AM.ENQUIRY_ID')
                      ->join('employee_master EPM','EPM.EMPLOYEE_ID = AM.HANDLED_BY');
                      if(!empty($data)){
                        if(isset($data['admission_id'])){
                          if ($data['admission_id'] != '') {
                            $query = $this->db->where('AM.ADMISSION_ID',$data['admission_id']);
                          }
                        }

                        if(isset($data['first_name'])){
                          if ($data['first_name'] != '') {
                            $query = $this->db->like('EM.ENQUIRY_FIRSTNAME',$data['first_name'],'after');
                          }
                        }

                        if(isset($data['last_name'])){
                          if ($data['last_name'] != '') {
                            $query = $this->db->like('EM.ENQUIRY_LASTNAME',$data['last_name'],'after');
                          }
                        }
                      }
    $query = $this->db->where('AM.ISACTIVE','1')
                      ->get();

    return $query->result_array();
  }

  public function get_students_resume($data = array()){
    $where = "SR.APPLIED_ON_DATE between '2018-07-25' and '".date('Y-m-d')."'";
    $query = $this->db->select('AM.ADMISSION_ID,concat(EM.ENQUIRY_FIRSTNAME," ",EM.ENQUIRY_LASTNAME) as Student_name,AM.ADMISSION_DATE,CM.CENTRE_NAME,AM.COURSE_TAKEN,AM.MODULE_TAKEN,AM.TOTALFEES,concat(if(EM.ENQUIRY_TELEPHONE IS NULL,EM.ENQUIRY_PARENT_NO,EM.ENQUIRY_TELEPHONE)," ", EM.ENQUIRY_MOBILE_NO) as enquiry_contact,EM.ENQUIRY_EMAIL,AM.STATUS,AM.STREAM,SR.JOBAPPLIEDFOR,SR.RESUMEFILEPATH,SR.APPLIED_ON_DATE')
                      ->from('student_resume SR')
                      ->join('admission_master AM','AM.ADMISSION_ID = SR.STUDENTADMISSIONID')
                      ->join('centre_master CM','CM.CENTRE_ID = AM.CENTRE_ID')
                      ->join('enquiry_master EM','EM.ENQUIRY_ID = AM.ENQUIRY_ID')
                      ->join('employee_master EPM','EPM.EMPLOYEE_ID = AM.HANDLED_BY');
                      if(!empty($data)){
                        if(isset($data['admission_id'])){
                          if ($data['admission_id'] != '') {
                            $query = $this->db->where('SR.STUDENTADMISSIONID',$data['admission_id']);
                          }
                        }

                        if(isset($data['first_name'])){
                          if ($data['first_name'] != '') {
                            $query = $this->db->like('EM.ENQUIRY_FIRSTNAME',$data['first_name'],'after');
                          }
                        }

                        if(isset($data['last_name'])){
                          if ($data['last_name'] != '') {
                            $query = $this->db->like('EM.ENQUIRY_LASTNAME',$data['last_name'],'after');
                          }
                        }

                        if(isset($data['designation'])){
                          if ($data['designation'] != '') {
                            $query = $this->db->where('SR.JOBAPPLIEDFOR',$data['designation']);
                          }
                        }

                        if(isset($data['centre_id'])){
                          if ($data['centre_id'] != '') {
                            $query = $this->db->where('CM.CENTRE_ID',$data['centre_id']);
                          }
                        }
                      }
    $query = $this->db->where($where)
                      ->order_by('SR.JOBAPPLICATIONID','DESC')
                      ->group_by('SR.JOBAPPLICATIONID')
                      ->get();

    return $query->result_array();
  }

  public function get_students_placement_remarks($admission_id,$type){
    $query = $this->db->select('CM.CENTRE_NAME,concat(EPM.EMP_FNAME," ",EPM.EMP_MIDDLENAME," ",EPM.EMP_LASTNAME) as employee_name,PR.*')
                      ->from('placement_remarks PR')
                      ->join('admission_master AM','AM.ADMISSION_ID = PR.ADMISSION_ID')
                      ->join('centre_master CM','CM.CENTRE_ID = AM.CENTRE_ID')
                      ->join('employee_master EPM','EPM.EMPLOYEE_ID = PR.REMARKS_BY')
                      ->where('PR.ADMISSION_ID',$admission_id);
                      if($type == "remarks"){
                        $query = $this->db->order_by('PR.REMARKS_DATE','ASC');
                      }
                      else{
                        $query = $this->db->order_by('PR.REMARKS_DATE','DESC')
                                          ->limit(1);
                      }
                      $query = $this->db->get();

    return $query->result_array();
  }

  public function add_student_remarks($insert_remark){
      $this->db->insert('placement_remarks',$insert_remark);
      if($this->db->affected_rows() > 0){
        return true;
      }
      else{
        return false;
      }
  }

  public function update_employer_details($data,$company_id){
    $this->db->set($data)
             ->where('COMPANY_ID',$company_id)
             ->update('company_info');

    return $this->db->affected_rows();
  }

}
