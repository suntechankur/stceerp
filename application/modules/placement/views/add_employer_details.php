<div class="col-md-12 col-sm-12">
	<div class="create_batch_form" id="studentDetails">
		<div id="box">
			<h2>Add Employer Details</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?php if($this->session->flashdata('danger')) { ?>
						<div class="alert alert-danger">
						<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
						</div>
						<?php } ?>

						<?php if($this->session->flashdata('msg')) { ?>
						<div class="alert alert-success">
						<strong>Success!</strong> <?php $msg = $this->session->flashdata('msg'); echo $msg['msg']; ?>
						</div>
						<?php } ?>

						<?php if($this->session->flashdata('success')) { ?>
						<div class="alert alert-success">
						<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
						</div>
						<?php } ?>
					</div>
					<div class="panel-body">
						<?php echo form_open(); ?>
							<div class="col-md-12">

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Name of Company: <span style="color:red">*</span></label>
												<input type="text" class="form-control" name="NAME_OF_COMPANY" required>
											</div>
											<div class="col-lg-4">
												<label>Name of HR:</label>
												<input type="text" class="form-control" name="NAME_OF_HR">
											</div>
											<div class="col-lg-4">
												<label>Job Location:</label>
												<input type="text" class="form-control" name="JOB_LOCATION">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Conatct No. (Landline):</label>
												<input type="text" class="form-control" name="CONTACT_NO_LANDLINE">
											</div>
											<div class="col-lg-4">
												<label>Conatct No. (Mobile):</label>
												<input type="text" class="form-control" name="CONTACT_NO_MOBILE">
											</div>
											<div class="col-lg-4">
												<label>Email Id: <span style="color:red">*</span></label>
												<input type="email" class="form-control" name="EMAIL_ID" required>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Contact Website: <span style="color:red">*</span></label>
												<input type="text" class="form-control" name="WEBSITE" required>
											</div>
											<div class="col-lg-4">
												<label>Job Designation: <span style="color:red">*</span></label>
												<select class="form-control" name="JOB_DESIGNATION" required>
													<option disabled selected>Select Job Designation</option>
													<?php foreach($job_designation as $designation){?>
														<option value="<?php echo $designation['CONTROLFILE_VALUE'];?>"><?php echo $designation['CONTROLFILE_VALUE'];?></option>
													<?php }?>
												</select>
												<!-- <input type="text" class="form-control" name="JOB_DESIGNATION" required> -->
											</div>
											<div class="col-lg-4">
												<label>Number of people required: <span style="color:red">*</span></label>
												<input type="number" class="form-control" name="NO_OF_PEOPLE_REQ" required>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Gender: <span style="color:red">*</span></label>
												<select class="form-control" name="GENDER" required>
													<option value="Any">Any</option>
													<option value="For Male">For Male</option>
													<option value="For Female">For Female</option>
												</select>
											</div>
											<div class="col-lg-4">
												<label>Minimum Salary Offered:</label>
												<input type="text" class="form-control" name="SALARY_MIN">
											</div>
											<div class="col-lg-4">
												<label>Maximum Salary Offered:</label>
												<input type="text" class="form-control" name="SALARY_MAX">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Stream:</label>
												<select class="form-control" name="STREAM">
													<option selected disabled>Select Stream</option>
													<option value="Basic">Basic</option>
													<option value="Programming">Programming</option>
													<option value="Graphics And Animation">Graphics And Animation</option>
													<option value="Hardware And Networking">Hardware And Networking</option>
													<option value="Other">Other</option>
												</select>
											</div>
											<div class="col-lg-4">
												<label>Incentive Details:</label>
												<input type="text" class="form-control" name="INCENTIVE">
											</div>
											<div class="col-lg-4">
												<label>Job Timing (e.g. 10.30AM - 6.30PM):</label>
												<input type="text" class="form-control" name="JOB_TIMING">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Job Profile: <span style="color:red">*</span></label>
												<textarea class="form-control" name="JOB_PROFILE" rows="6" required></textarea>
											</div>
											<div class="col-lg-4">
												<label>Company Address: <span style="color:red">*</span></label>
												<textarea class="form-control" name="COMPANY_ADDRESS" rows="6" required></textarea>
											</div>
											<div class="col-lg-4">
												<label>How they came to know about us:</label>
												<textarea class="form-control" name="REFERRED_BY" rows="6"></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
											</div>
											<div class="col-lg-2">
												<button type="submit" name="add_job_details" class="btn btn-primary">Add</button>
											</div>
											<div class="col-lg-2">
												<button type="reset" class="btn btn-primary">Reset</button>
											</div>
											<div class="col-lg-4">
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php echo form_close();?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="subjectDetails">
			<div id="box">
				<p>&nbsp;</p>
      	<h2 class="text-center">Job Details</h2>
      </div>
	    <div class="panel panel-default">
	    	<div class="panel-body table-responsive">
	    		<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Actions</th>
						<th>Job Status</th>
						<th>Sr No.</th>
						<th>Company Name</th>
						<th>Company Address</th>
						<th>Name Of HR</th>
						<th>Contact Number</th>
						<th>Mobile Number</th>
						<th>Email Id</th>
						<th>Company Website</th>
						<th>Job Designation</th>
						<th>Total Requirement</th>
						<th>Gender</th>
						<th>Min Salry Offered</th>
						<th>Max Salry Offered</th>
						<th>Stream</th>
						<th>Incentive</th>
						<th>Job Timing</th>
						<th>Job Profile</th>
						<th>Job Location</th>
						<th>Job Reffered By</th>
						<th>Is Posted On Student Desk</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 1;
					foreach($placement_details as $placement){?>
						<tr style="text-align:center;background-color:<?php if($placement['ISACTIVE'] == '1'){echo 'rgba(0,255,0,0.1)';}else{echo 'rgba(255,0,0,0.1)';}?>">
							<td>
								<?php if($placement['ISACTIVE'] == 1){echo '<span class="fa fa-2x fa-toggle-on activate_or_deactivate_job" title="Click to De Activate Job" style="color:green;-webkit-transition: .4s;transition: .4s;cursor: pointer;" data-isactive="0" data-company_id="'.$placement['COMPANY_ID'].'"></span>';}else{echo '<span class="fa fa-2x fa-toggle-off activate_or_deactivate_job"  title="Click to Activate Job" style="color:red;-webkit-transition: .4s;transition: .4s;cursor: pointer;" data-isactive="1" data-company_id="'.$placement['COMPANY_ID'].'"></span>';}?>
								<br/>
								<label style="font-size:20px;">---</label>
								<br/>
								<span class="fa fa-2x fa-trash delete_company_job" title="Delete Job" data-company_id="<?php echo $placement['COMPANY_ID'];?>" style="color:red;cursor: pointer;"></span>
							</td>
							<td>
								<?php if($placement['ISACTIVE'] == 1){echo '<input class="form-control" type="checkbox" checked="true" disabled="disabled">';}else{echo '<input class="form-control" type="checkbox" disabled="disabled">';}?>
							</td>
							<td><?php echo $i++;?></td>
							<td><?php echo $placement['NAME_OF_COMPANY'];?></td>
							<td><?php echo $placement['COMPANY_ADDRESS'];?></td>
							<td><?php echo $placement['NAME_OF_HR'];?></td>
							<td><?php echo $placement['CONTACT_NO_LANDLINE'];?></td>
							<td><?php echo $placement['CONTACT_NO_MOBILE'];?></td>
							<td><?php echo $placement['EMAIL_ID'];?></td>
							<td><?php echo $placement['WEBSITE'];?></td>
							<td><?php echo $placement['JOB_DESIGNATION'];?></td>
							<td><?php echo $placement['NO_OF_PEOPLE_REQ'];?></td>
							<td><?php echo $placement['GENDER'];?></td>
							<td><?php echo $placement['SALARY_MIN'];?></td>
							<td><?php echo $placement['SALARY_MAX'];?></td>
							<td><?php echo $placement['STREAM'];?></td>
							<td><?php echo $placement['INCENTIVE'];?></td>
							<td><?php echo $placement['JOB_TIMING'];?></td>
							<td><?php echo $placement['JOB_PROFILE'];?></td>
							<td><?php echo $placement['JOB_LOCATION'];?></td>
							<td><?php echo $placement['REFERRED_BY'];?></td>
							<td><?php echo $placement['IS_POSTED_IN_STUDENTDESK'];?></td>
						</tr>
					<?php }?>
				</tbody>
	    		</table>
	    	</div>
	    </div>
    </div>
	</div>
</div>
