<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Download Students Resume</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
					<!-- start message area -->

					<?php if($this->session->flashdata('success')) { ?>
					<div class="alert alert-success">
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
					</div>
					<?php } ?>

					<!-- End message area -->
						<div class="col-md-12">
							<div class="form-group">
								<?php echo form_open(); ?>
								<div class="col-md-12">
									<div class="form-group">
										<div class="row" style="margin-left:-30px;">
											<div class="col-lg-12" style="margin-left:0px;">
												<div class="col-lg-4">
													<label>Admission Id:</label>
													<input type="text" class="form-control" name="admission_id">
												</div>
												<div class="col-lg-4">
													<label>First Name:</label>
													<input type="text" class="form-control" name="first_name">
												</div>
												<div class="col-lg-4">
													<label>Last Name:</label>
													<input type="text" class="form-control" name="last_name">
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row" style="margin-left:-30px;">
											<div class="col-lg-12" style="margin-left:0px;">
												<div class="col-lg-4">
													<label>Job Designation:</label>
													<select class="form-control" name="designation">
														<option disabled selected>Select Job Designation</option>
														<?php foreach($job_designation as $designation){?>
															<option value="<?php echo $designation['CONTROLFILE_VALUE'];?>"><?php echo $designation['CONTROLFILE_VALUE'];?></option>
														<?php }?>
													</select>
												</div>
												<div class="col-lg-4">
													<label>Centre Name:</label>
													<select class="form-control" name="centre_id">
														<option disabled selected>Please Select Centre</option>
														<?php foreach ($centres as $centre) {?>
															<option value="<?php echo $centre['CENTRE_ID']; ?>"><?php echo $centre['CENTRE_NAME']; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="col-lg-4">
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row" style="margin-left:-30px;">
											<div class="col-lg-12" style="margin-left:0px;">
												<div class="col-lg-4">
												</div>
												<div class="col-lg-2">
													<button type="submit" name="search_student_resume" class="btn btn-primary">Search</button>
												</div>
												<div class="col-lg-2">
												</div>
												<div class="col-lg-4">
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close();?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="subjectDetails">
		<div id="box">
			<p>&nbsp;</p>
			<h2 class="text-center">Students Details</h2>
		</div>
		<div class="panel panel-default">
			<div class="panel-body table-responsive">
				<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>Sr No.</th>
					<th>Admission Id</th>
					<th>Student Name</th>
					<th>Admission Date</th>
					<th>Centre Name</th>
					<th>Course Name</th>
					<th>Status</th>
					<th>Job Designation Applied For</th>
					<th>STREAM</th>
					<th>Resume Uploaded On</th>
					<th>Download Resume</th>
					<th>Status</th>
					<th style="padding-left:50px;padding-right:50px;width:100px">Remarks</th>
					<th>Add / View Followups</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$i = 1;
				foreach($resumes as $student){?>
					<tr>
						<td><?php echo $i++;?></td>
						<td><?php echo $student['ADMISSION_ID'];?></td>
						<td><?php echo $student['Student_name'];?></td>
						<td>
							<?php $admDateRep = str_replace('-', '/', $student['ADMISSION_DATE']);
									echo date("d/m/Y", strtotime($admDateRep) );?>
						</td>
						<td><?php echo $student['CENTRE_NAME'];?></td>
						<td>
							<?php
                  $courses_arr = explode(",",$student['COURSE_TAKEN']);
                       $courseNames = '';
                       //echo '<ul class="list-group">';
                        for($j=0;$j<count($courses);$j++){
                            if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
                                  $courseNames .= ','.$courses[$j]['COURSE_NAME'];
                                $student['COURSE_TAKEN'] = trim($courseNames,",");
                              }

                        }

                  $modules_arr = explode(",",$student['MODULE_TAKEN']);
                       $moduleNames = '';
                       //echo '<ul class="list-group">';
                        for($j=0;$j<count($modules);$j++){
                            if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
                                  $moduleNames .= ','.$modules[$j]['MODULE_NAME'];
                                $student['MODULE_TAKEN'] = trim($moduleNames,",");
                              }

                        }
                       //echo '</ul>';
									 echo trim($student['MODULE_TAKEN'].",".$student['COURSE_TAKEN'],",");
									 unset($student['MODULE_TAKEN']);
                ?>
						</td>
						<td><?php echo $student['STATUS'];?></td>
						<td><?php echo $student['JOBAPPLIEDFOR'];?></td>
						<td><?php echo $student['STREAM'];?></td>
						<td><?php
									$admDateRep = str_replace('-', '/', $student['APPLIED_ON_DATE']);
									echo date("d/m/Y", strtotime($admDateRep) );
								?>
						</td>
						<td align="center"><a href="http://saintangelos.com:8282/student_desk/<?php echo $student['RESUMEFILEPATH'];?>" target="_blank" title="Download Resume" style="color:green"><span class="fa fa-2x fa-download"></span></a></td>
						<td><?php echo $student['PLACEMENT_STATUS'];?></td>
						<td><?php echo $student['PLACEMENT_REMARKS'];?></td>
						<td align="center"><a href="<?php echo base_url("placement/placement-remarks/".$this->encrypt->encode($student['ADMISSION_ID']));?>" target="_blank" title="Add FollowUp" style="color:red"><span class="fa fa-2x fa-phone"></span></a></td>
					</tr>
				<?php }?>
			</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
