<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Upload Practical Marks</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
					<!-- start message area -->

					<?php if($this->session->flashdata('success')) { ?>
					<div class="alert alert-success">
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
					</div>
					<?php } ?>

					<!-- End message area -->
						<div class="col-md-12">
							<div class="form-group">
								<?php echo form_open_multipart('student_marks_upload') ?>
								<div class="row" style="margin-left:-30px;">
									<div class="col-lg-12" style="margin-left:0px;">
										<br/>
										<div class="col-md-3">
											<label>Select File To Upload : </label>
										</div>
										<div class="col-md-7">
											<input type="file" name="userfile" placeholder="Search File" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" >
										</div>
										<div class="col-md-2">
											<button type="submit" class="btn btn-primary">Upload Marks</button>
										</div>
									</div>
								</div>
								<?php echo form_close();?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
