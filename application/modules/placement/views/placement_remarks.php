<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Search Students</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
					<!-- start message area -->

					<?php if($this->session->flashdata('success')) { ?>
					<div class="alert alert-success">
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
					</div>
					<?php } ?>

					<!-- End message area -->
						<div class="col-md-12">
							<div class="form-group">
								<div class="col-md-12">
									<?php foreach($student_details as $student){?>
									<div class="form-group">
										<div class="row" style="margin-left:-30px;">
											<div class="col-lg-12" style="margin-left:0px;">
												<div class="col-lg-4">
													<label>Admission Id:</label> <label><?php echo $student['ADMISSION_ID']?></label>
												</div>
												<div class="col-lg-4">
													<label>Name:</label> <label><?php echo $student['Student_name']?></label>
												</div>
												<div class="col-lg-4">
													<label>Email:</label> <label><?php echo $student['ENQUIRY_EMAIL']?></label>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row" style="margin-left:-30px;">
											<div class="col-lg-12" style="margin-left:0px;">
												<div class="col-lg-4">
													<label>Admission Date:</label> <label><?php $admDateRep = str_replace('-', '/', $student['ADMISSION_DATE']);	echo date("d/m/Y", strtotime($admDateRep) );?></label>
												</div>
												<div class="col-lg-4">
													<label>Centre Name:</label> <label><?php echo $student['CENTRE_NAME'];?></label>
												</div>
												<div class="col-lg-4">
													<label>Contact:</label> <label><?php echo $student['enquiry_contact'];?></label>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row" style="margin-left:-30px;">
											<div class="col-lg-12" style="margin-left:0px;">
												<div class="col-lg-12">
													<label>Course Name:</label>
													<label>
														<?php
							                  $courses_arr = explode(",",$student['COURSE_TAKEN']);
							                       $courseNames = '';
							                       //echo '<ul class="list-group">';
							                        for($j=0;$j<count($courses);$j++){
							                            if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
							                                  $courseNames .= ','.$courses[$j]['COURSE_NAME'];
							                                $student['COURSE_TAKEN'] = trim($courseNames,",");
							                              }

							                        }

							                  $modules_arr = explode(",",$student['MODULE_TAKEN']);
							                       $moduleNames = '';
							                       //echo '<ul class="list-group">';
							                        for($j=0;$j<count($modules);$j++){
							                            if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
							                                  $moduleNames .= ','.$modules[$j]['MODULE_NAME'];
							                                $student['MODULE_TAKEN'] = trim($moduleNames,",");
							                              }

							                        }
							                       //echo '</ul>';
																 echo trim($student['MODULE_TAKEN'].",".$student['COURSE_TAKEN'],",");
																 unset($student['MODULE_TAKEN']);
							                ?>
													</label>
												</div>
											</div>
										</div>
									</div>
								<?php }?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="subjectDetails">
		<div id="box">
			<p>&nbsp;</p>
			<h2 class="text-center">Placement Remark</h2>
		</div>
		<div class="panel panel-default">
			<div class="panel-body table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>Add Remark</th>
							<th>Sr No.</th>
							<th>Centre Name</th>
							<th>Remark Given By</th>
							<th>Remark</th>
							<th>Remark Date</th>
							<th>Remark By</th>
							<th>Internal Process</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 1;
						foreach($placement_remarks as $remark){?>
							<tr>
								<td></td>
								<td><?php echo $i++;?></td>
								<td><?php echo $remark['CENTRE_NAME'];?></td>
								<td><?php echo $remark['REMARKS_SOURCE'];?></td>
								<td><?php echo $remark['REMARKS'];?></td>
								<td><?php $remDateRep = str_replace('-', '/', $remark['REMARKS_DATE']);	echo date("d/m/Y", strtotime($remDateRep));?></td>
								<td><?php echo $remark['employee_name'];?></td>
								<td><?php echo $remark['INTERNAL_PROCESS'];?></td>
								<td><?php echo $remark['STATUS'];?></td>
							</tr>
						<?php }?>
						<?php echo form_open(); ?>
						<tr>
							<td>
								<input type="hidden" class="form-control" name="ADMISSION_ID" value="<?php echo  $student['ADMISSION_ID'];?>">
								<button type="submit" class="btn btn-success" title="Add Remark" style="padding:3px;width:50px;" name="add_placement_remark"><i class="fa fa-2x fa-plus"></i></button>
							</td>
							<td></td>
							<td><?php echo $student['CENTRE_NAME'];?></td>
							<td>
								<select class="form-control" name="REMARKS_SOURCE" required>
									<option selected disabled>Select Department</option>
									<option value="Placement">Placement</option>
									<option value="Academic Head">Academic Head</option>
									<option value="Training Head">Training Head</option>
									<option value="Faculty">Faculty</option>
									<option value="HR">HR</option>
								</select>
							</td>
							<td><textarea class="form-control" name="REMARKS" rows="3" required></textarea></td>
							<td><input type="text" class="form-control enquiry_date" name="REMARKS_DATE" required></td>
							<td>
								<select class="form-control" name="REMARKS_BY" required>
									<option selected disabled>Select Employee</option>
									<?php foreach($enq_handled_by as $enq_handler){ ?>
					            <option value="<?php echo $enq_handler['EMPLOYEE_ID']; ?>"><?php echo $enq_handler['EMP_NAME'];?></option>
					        <?php } ?>
								</select>
							</td>
							<td>
								<select class="form-control" name="INTERNAL_PROCESS" required>
									<option selected disabled>Select Process</option>
									<option value="Interview 1" <?php if (in_array('Interview 1', $process)){echo 'disabled="true" style="background-color:grey;"';}?>>Interview 1</option>
									<option value="Interview 2" <?php if (in_array('Interview 2', $process)){echo 'disabled="true" style="background-color:grey;"';}?>>Interview 2</option>
									<option value="Interview 3" <?php if (in_array('Interview 3', $process)){echo 'disabled="true" style="background-color:grey;"';}?>>Interview 3</option>
									<option value="Aptitude 1">Aptitude 1</option>
									<option value="Aptitude 2">Aptitude 2</option>
									<option value="Aptitude 3">Aptitude 3</option>
									<option value="Viva 1">Viva 1</option>
									<option value="Viva 2">Viva 2</option>
									<option value="Finished">Finished</option>
									<option value="HR">HR</option>
								</select>
							</td>
							<td>
								<select class="form-control" name="STATUS" required>
									<option selected disabled>Select Status</option>
									<option value="Eligible">Eligible</option>
									<option value="Not Eligible">Not Eligible</option>
									<option value="Not Interested">Not Interested</option>
									<option value="Not Selected">Not Selected</option>
									<option value="On Hold">On Hold</option>
									<option value="Interviewing">Interviewing</option>
									<option value="Waiting for Apportunities">Waiting for Apportunities</option>
									<option value="Second Interview">Second Interview</option>
									<option value="Selected but Not Joined">Selected but Not Joined</option>
									<option value="Missed Interview">Missed Interview</option>
									<option value="Got Job">Got Job</option>
								</select>
							</td>
						</tr>
						<?php echo form_close();?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
