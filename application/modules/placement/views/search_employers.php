<div class="col-md-12 col-sm-12">
	<div class="create_batch_form" id="studentDetails">
		<div id="box">
			<h2>Search Employer Details</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?php if($this->session->flashdata('danger')) { ?>
						<div class="alert alert-danger">
						<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
						</div>
						<?php } ?>

						<?php if($this->session->flashdata('msg')) { ?>
						<div class="alert alert-success">
						<strong>Success!</strong> <?php $msg = $this->session->flashdata('msg'); echo $msg['msg']; ?>
						</div>
						<?php } ?>

						<?php if($this->session->flashdata('success')) { ?>
						<div class="alert alert-success">
						<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
						</div>
						<?php } ?>
					</div>
					<div class="panel-body">
						<?php echo form_open(); ?>
							<div class="col-md-12">

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Name of Company:</label>
												<input type="text" class="form-control" name="NAME_OF_COMPANY">
											</div>
											<div class="col-lg-4">
												<label>Name of HR:</label>
												<input type="text" class="form-control" name="NAME_OF_HR">
											</div>
											<div class="col-lg-4">
												<label>Email Id:</label>
												<input type="email" class="form-control" name="EMAIL_ID">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Conatct No. (Landline):</label>
												<input type="text" class="form-control" name="CONTACT_NO_LANDLINE">
											</div>
											<div class="col-lg-4">
												<label>Conatct No. (Mobile):</label>
												<input type="text" class="form-control" name="CONTACT_NO_MOBILE">
											</div>
											<div class="col-lg-4">
												<label>Contact Website:</label>
												<input type="text" class="form-control" name="WEBSITE">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-8">
												<label>Job Designation:</label>
												<select class="form-control" name="JOB_DESIGNATION">
													<option disabled selected>Select Job Designation</option>
													<?php foreach($job_designation as $designation){?>
														<option value="<?php echo $designation['CONTROLFILE_VALUE'];?>"><?php echo $designation['CONTROLFILE_VALUE'];?></option>
													<?php }?>
												</select>
												<!-- <input type="text" class="form-control" name="JOB_DESIGNATION" required> -->
											</div>
											<div class="col-lg-4">
												<label>Stream:</label>
												<select class="form-control" name="STREAM">
													<option selected disabled>Select Stream</option>
													<option value="Basic">Basic</option>
													<option value="Programming">Programming</option>
													<option value="Graphics And Animation">Graphics And Animation</option>
													<option value="Hardware And Networking">Hardware And Networking</option>
													<option value="Other">Other</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Job Profile:</label>
												<input type="text" class="form-control" name="JOB_PROFILE">
											</div>
											<div class="col-lg-4">
												<label>Company Address:</label>
												<input type="text" class="form-control" name="COMPANY_ADDRESS">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
											</div>
											<div class="col-lg-2">
												<button type="submit" name="get_job_details" class="btn btn-primary" style="width:100%">Search Employer</button>
											</div>
											<div class="col-lg-2">
												<button type="reset" class="btn btn-primary">Reset</button>
											</div>
											<div class="col-lg-4">
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php echo form_close();?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="subjectDetails">
			<div id="box">
				<p>&nbsp;</p>
      	<h2 class="text-center">Employer Details</h2>
      </div>
	    <div class="panel panel-default">
	    	<div class="panel-body table-responsive">
	    		<table class="table table-striped">
						<thead>
							<tr>
								<th>Actions</th>
								<th>Job Status</th>
								<th>Sr No.</th>
								<th>Company Name</th>
								<th>Company Address</th>
								<th>Name Of HR</th>
								<th>Contact Number</th>
								<th>Mobile Number</th>
								<th>Email Id</th>
								<th>Company Website</th>
								<th>Job Designation</th>
								<th>Total Requirement</th>
								<th>Gender</th>
								<th>Min Salry Offered</th>
								<th>Max Salry Offered</th>
								<th>Stream</th>
								<th>Incentive</th>
								<th>Job Timing</th>
								<th>Job Profile</th>
								<th>Job Location</th>
								<th>Job Reffered By</th>
								<th>Is Posted On Student Desk</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i = 1;
							foreach($placement_details as $placement){?>
								<tr style="text-align:center;background-color:<?php if($placement['ISACTIVE'] == '1'){echo 'rgba(0,255,0,0.1)';}else{echo 'rgba(255,0,0,0.1)';}?>">
									<td>
										<?php if($placement['ISACTIVE'] == 1){echo '<span class="fa fa-2x fa-toggle-on activate_or_deactivate_job" title="Click or De Activate Job" style="color:green;-webkit-transition: .4s;transition: .4s;cursor: pointer;" data-isactive="0" data-company_id="'.$placement['COMPANY_ID'].'"></span>';}else{echo '<span class="fa fa-2x fa-toggle-off activate_or_deactivate_job"  title="Click to Activate Job" style="color:red;-webkit-transition: .4s;transition: .4s;cursor: pointer;" data-isactive="1" data-company_id="'.$placement['COMPANY_ID'].'"></span>';}?>
										<br/>
										<label style="font-size:20px;">---</label>
										<br/>
										<span class="fa fa-2x fa-pencil edit_company_job" title="Update Job" data-company_id="<?php echo $placement['COMPANY_ID'];?>" style="color:black;cursor: pointer;"></span>
										<br/>
										<label style="font-size:20px;">---</label>
										<br/>
										<span class="fa fa-2x fa-trash delete_company_job" title="Delete Job" data-company_id="<?php echo $placement['COMPANY_ID'];?>" style="color:red;cursor: pointer;"></span>
									</td>
									<td>
										<?php if($placement['ISACTIVE'] == 1){echo '<input class="form-control" type="checkbox" checked="true" disabled="disabled">';}else{echo '<input class="form-control" type="checkbox" disabled="disabled">';}?>
									</td>
									<td><?php echo $i++;?></td>
									<td><?php echo $placement['NAME_OF_COMPANY'];?></td>
									<td><?php echo $placement['COMPANY_ADDRESS'];?></td>
									<td><?php echo $placement['NAME_OF_HR'];?></td>
									<td><?php echo $placement['CONTACT_NO_LANDLINE'];?></td>
									<td><?php echo $placement['CONTACT_NO_MOBILE'];?></td>
									<td><?php echo $placement['EMAIL_ID'];?></td>
									<td><?php echo $placement['WEBSITE'];?></td>
									<td><?php echo $placement['JOB_DESIGNATION'];?></td>
									<td><?php echo $placement['NO_OF_PEOPLE_REQ'];?></td>
									<td><?php echo $placement['GENDER'];?></td>
									<td><?php echo $placement['SALARY_MIN'];?></td>
									<td><?php echo $placement['SALARY_MAX'];?></td>
									<td><?php echo $placement['STREAM'];?></td>
									<td><?php echo $placement['INCENTIVE'];?></td>
									<td><?php echo $placement['JOB_TIMING'];?></td>
									<td><?php echo $placement['JOB_PROFILE'];?></td>
									<td><?php echo $placement['JOB_LOCATION'];?></td>
									<td><?php echo $placement['REFERRED_BY'];?></td>
									<td><?php echo $placement['IS_POSTED_IN_STUDENTDESK'];?></td>
								</tr>
								<?php echo form_open('placement/update-employer-details');?>
								<tr style="text-align:center;display:none;" id="update_company_<?php echo $placement['COMPANY_ID'];?>" >
									<td>
										<button type="submit" class="btn btn-primary" title="Update Company Details" style="padding:1px;width:31px;height:32px;border-radius:100%;" name="update_company_info"><i class="fa fa-2x fa-plus-circle"></i></button>
										<input type="hidden" value="<?php echo $placement['COMPANY_ID'];?>" name="COMPANY_ID">
									</td>
									<td>
										<?php if($placement['ISACTIVE'] == 1){echo '<input class="form-control" type="checkbox" checked="true" disabled="disabled">';}else{echo '<input class="form-control" type="checkbox" disabled="disabled">';}?>
									</td>
									<td></td>
									<td><textarea class="form-control" name="NAME_OF_COMPANY" style="width:200px;"><?php echo $placement['NAME_OF_COMPANY'];?></textarea></td>
									<td><textarea class="form-control" rows="6" name="COMPANY_ADDRESS" style="width:500px;"><?php echo $placement['COMPANY_ADDRESS'];?></textarea></td>
									<td><textarea class="form-control" name="NAME_OF_HR" style="width:200px;"><?php echo $placement['NAME_OF_HR'];?></textarea></td>
									<td><input type="text" class="form-control" value="<?php echo $placement['CONTACT_NO_LANDLINE'];?>" name="CONTACT_NO_LANDLINE" style="width:200px;"></td>
									<td><input type="number" class="form-control" value="<?php echo $placement['CONTACT_NO_MOBILE'];?>" name="CONTACT_NO_MOBILE" style="width:200px;"></td>
									<td><input type="text" class="form-control" value="<?php echo $placement['EMAIL_ID'];?>" name="EMAIL_ID" style="width:200px;"></td>
									<td><input type="text" class="form-control" value="<?php echo $placement['WEBSITE'];?>" name="WEBSITE" style="width:200px;"></td>
									<td>
										<select class="form-control" name="JOB_DESIGNATION" style="width:250px;">
											<option disabled selected>Select Job Designation</option>
											<?php foreach($job_designation as $designation){?>
												<option value="<?php echo $designation['CONTROLFILE_VALUE'];?>"><?php echo $designation['CONTROLFILE_VALUE'];?></option>
											<?php }?>
										</select>
									</td>
									<td><input type="text" class="form-control" value="<?php echo $placement['NO_OF_PEOPLE_REQ'];?>" name="NO_OF_PEOPLE_REQ" style="width:200px;"></td>
									<td>
										<select class="form-control" name="GENDER" style="width:100px;">
											<option value="Any">Any</option>
											<option value="For Male">For Male</option>
											<option value="For Female">For Female</option>
										</select>
									</td>
									<td><input type="number" class="form-control" value="<?php echo $placement['SALARY_MIN'];?>" name="SALARY_MIN" style="width:200px;"></td>
									<td><input type="number" class="form-control" value="<?php echo $placement['SALARY_MAX'];?>" name="SALARY_MAX" style="width:200px;"></td>
									<td>
										<select class="form-control" name="STREAM" style="width:150px;">
											<option selected disabled>Select Stream</option>
											<option value="Basic">Basic</option>
											<option value="Programming">Programming</option>
											<option value="Graphics And Animation">Graphics And Animation</option>
											<option value="Harware And Networking">Harware And Networking</option>
											<option value="Other">Other</option>
										</select>
									</td>
									<td><input type="text" class="form-control" value="<?php echo $placement['INCENTIVE'];?>" name="INCENTIVE" style="width:200px;"></td>
									<td><input type="text" class="form-control" value="<?php echo $placement['JOB_TIMING'];?>" name="JOB_TIMING" style="width:200px;"></td>
									<td><textarea class="form-control" rows="6" name="JOB_PROFILE" style="width:500px;"><?php echo $placement['JOB_PROFILE'];?></textarea></td>
									<td><input type="text" class="form-control" value="<?php echo $placement['JOB_LOCATION'];?>" name="JOB_LOCATION" style="width:200px;"></td>
									<td><input type="text" class="form-control" value="<?php echo $placement['REFERRED_BY'];?>" name="REFERRED_BY" style="width:200px;"></td>
									<td></td>
								</tr>
								<?php echo form_close();?>
							<?php }?>
						</tbody>
	    		</table>
	    	</div>
	    </div>
    </div>
	</div>
</div>
