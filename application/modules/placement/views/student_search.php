<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Search Students</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
					<!-- start message area -->

					<?php if($this->session->flashdata('success')) { ?>
					<div class="alert alert-success">
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
					</div>
					<?php } ?>

					<!-- End message area -->
						<div class="col-md-12">
							<div class="form-group">
								<?php echo form_open(); ?>
								<div class="col-md-12">
									<div class="form-group">
										<div class="row" style="margin-left:-30px;">
											<div class="col-lg-12" style="margin-left:0px;">
												<div class="col-lg-4">
													<label>Admission Id:</label>
													<input type="text" class="form-control" name="admission_id">
												</div>
												<div class="col-lg-4">
													<label>First Name:</label>
													<input type="text" class="form-control" name="first_name">
												</div>
												<div class="col-lg-4">
													<label>Last Name:</label>
													<input type="text" class="form-control" name="last_name">
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row" style="margin-left:-30px;">
											<div class="col-lg-12" style="margin-left:0px;">
												<div class="col-lg-4">
												</div>
												<div class="col-lg-2">
													<button type="submit" name="search_student_details" class="btn btn-primary">Search</button>
												</div>
												<div class="col-lg-2">
												</div>
												<div class="col-lg-4">
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close();?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="subjectDetails">
		<div id="box">
			<p>&nbsp;</p>
			<h2 class="text-center">Students Details</h2>
		</div>
		<div class="panel panel-default">
			<div class="panel-body table-responsive">
				<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>Admission Id</th>
					<th>Student Name</th>
					<th>Admission Date</th>
					<th>Centre Name</th>
					<th>Course Taken</th>
					<th>Status</th>
					<th>Fees</th>
					<th>Fee paid</th>
					<th>Balance</th>
					<th>Contact</th>
					<th>Email Id</th>
					<th>Add / View Followup</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$i = 1;
				foreach($student_details as $student){?>
					<tr>
						<td><?php echo $student['ADMISSION_ID'];?></td>
						<td><?php echo $student['Student_name'];?></td>
						<td>
							<?php $admDateRep = str_replace('-', '/', $student['ADMISSION_DATE']);
									echo date("d/m/Y", strtotime($admDateRep) );?>
						</td>
						<td><?php echo $student['CENTRE_NAME'];?></td>
						<td>
							<?php
                  $courses_arr = explode(",",$student['COURSE_TAKEN']);
                       $courseNames = '';
                       //echo '<ul class="list-group">';
                        for($j=0;$j<count($courses);$j++){
                            if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
                                  $courseNames .= ','.$courses[$j]['COURSE_NAME'];
                                $student['COURSE_TAKEN'] = trim($courseNames,",");
                              }

                        }

                  $modules_arr = explode(",",$student['MODULE_TAKEN']);
                       $moduleNames = '';
                       //echo '<ul class="list-group">';
                        for($j=0;$j<count($modules);$j++){
                            if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
                                  $moduleNames .= ','.$modules[$j]['MODULE_NAME'];
                                $student['MODULE_TAKEN'] = trim($moduleNames,",");
                              }

                        }
                       //echo '</ul>';
									 echo trim($student['MODULE_TAKEN'].",".$student['COURSE_TAKEN'],",");
									 unset($student['MODULE_TAKEN']);
                ?>
						</td>
						<td><?php echo $student['STATUS'];?></td>
						<td><?php echo $student['TOTALFEES'];?></td>
						<td><?php echo $student['FEES_PAID'];?></td>
						<td><?php echo $student['TOTALFEES'] - $student['FEES_PAID'];?></td>
						<td><?php echo $student['enquiry_contact'];?></td>
						<td><?php echo $student['ENQUIRY_EMAIL'];?></td>
						<td align="center"><a href="<?php echo base_url("placement/placement-remarks/".$this->encrypt->encode($student['ADMISSION_ID']));?>" target="_blank" title="Add FollowUp" style="color:red"><span class="fa fa-2x fa-phone"></span></a></td>
					</tr>
				<?php }?>
			</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
