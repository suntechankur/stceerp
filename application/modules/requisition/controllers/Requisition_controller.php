<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Requisition_controller extends MX_Controller {

     public function __construct()
        {
        	$this->load->model('requisition_model');
        	$this->load->model('employee/employee_model');
        	$this->load->model('tele_enquiry/tele_enquiry_model');
			parent::__construct();
        }

    public function challan_print($encReqId)
    {
        $reqId = $this->encrypt->decode($encReqId);

        $this->load->helper('currency');
        $this->load->helper('pdf_helper');
        $challanData['challanDet'] = $this->requisition_model->getChallan($reqId);
        $challanData['challanItems'] = $this->requisition_model->getChallanItems($reqId);
        $this->load->view('requisition/challan_print',$challanData);
    }


    public function updateStatusRequisition($REQUSITION_ID='')
    {
    	$REQUSITION_ID = $this->encrypt->decode($REQUSITION_ID);
      $data['add_bel_cust_js'] = base_url('resources/js/requisition.js');
    	$data['centres'] = $this->tele_enquiry_model->suggested_centre();
    	$data['empNames'] = $this->employee_model->getEmployeeNames();
    	$data['req'] = $this->requisition_model->getReqUpdateData($REQUSITION_ID);
    	foreach ($data['req'] as $key => $value) {

    		$data['getItems'] = $this->requisition_model->getItem($value['REQUSITION_ID']);
    	}


    	if(!empty($_POST))
    	{
    	$this->form_validation->set_rules('CLOSED_DATE', 'Closed date', 'trim|required|xss_clean');
    	if($this->input->post('APPROVED_BY') == ''){
    		$this->form_validation->set_rules('APPROVED_BY','Approved by','trim|required|xss_clean');
    	}
    	$this->form_validation->set_rules('DEL_THROUGH','Delivery through','trim|required|xss_clean');
    	$this->form_validation->set_rules('REMARKS','Remarks','trim|required|xss_clean');

    	if($this->input->post('QTY_ISSUED') == ''){
    		$this->form_validation->set_rules('QTY_ISSUED[]','Quantity issued','trim|required|xss_clean');
    	}

    		if($this->form_validation->run() == false)
    		{
    			$msg['errors'][] = $this->form_validation->error_array();
    		}
    		else
    		{
    			$MODIFIED_BY = $this->session->userdata('admin_data')[0]['USER_ID'];
    			$MODIFIED_DATE=date("Y-m-d H:i:s");

    			$CLOSED_DATE = str_replace('/','-',$this->input->post('CLOSED_DATE'));
    			$CLOSED_DATE = date("Y-m-d",strtotime($CLOSED_DATE));

    			foreach ($this->input->post('QTY_ISSUED') as $k => $v)
    			{
    			$ary = array(
    				"REQUISTION_TRANSACTION_ID" => $this->input->post('REQUISTION_TRANSACTION_ID')[$k],
    				"REQUSITION_ID" => $this->input->post('REQUSITION_ID'),
    				"CLOSED_DATE" => $CLOSED_DATE,
    				"APPROVED_BY" => $this->input->post('APPROVED_BY'),
    				"DEL_THROUGH" => $this->input->post('DEL_THROUGH'),
    				"REMARKS" => $this->input->post('REMARKS'),
    				"MODIFIED_BY" => $MODIFIED_BY,
    				"MODIFIED_DATE" => $MODIFIED_DATE,
    				"QTY_GIVEN" => $v
    				);
    			$this->requisition_model->updateRequisition($ary);
    			}
    			redirect(base_url('requisition/viewRequisition'));
    		}
    	}
    	$this->load->admin_view('updateStatusRequisition',$data);
    }

    public function stationery(){
     	$statItems['statItems'] = $this->requisition_model->getStationeryItems();
     	$statItems['add_bel_cust_js'] = base_url('resources/js/requisition.js');

		  $this->load->admin_view('stationery',$statItems);
     }

     public function addReq(){
     	$userId = $this->session->userdata('admin_data')[0]['USER_ID'];
     	$userCen = $this->employee_model->getEmpCentre($userId);

     	$qtyRequested = $this->input->post('reqQty');
     	$encItemId = $this->input->post('item');
     	$curStock = $this->input->post('curStockVal');

     	$_POST['validate'] = '1';

     	$this->form_validation->set_rules('validate', 'Validate', 'required');

     	for ($i=0; $i <count($encItemId) ; $i++) {

     		if ($encItemId[$i] == '') {
     			$this->form_validation->set_rules('item', 'Stationery', 'required');
     		}

     		if ($qtyRequested[$i] == '') {
     			$this->form_validation->set_rules('reqQty', 'Required Quantity', 'required');
     		}

     		if ($curStock[$i] == '') {
     			$this->form_validation->set_rules('curStock', 'Current Stock', 'required');
     		}
     	}

     	if ($this->form_validation->run() != FALSE)
        {
        	$addReqMast = array(
        		'CENTRE_ID' => $userCen[0]['CENTRE_ID'],
        		'REQUESTED_BY' => $userId,
        		'REQUISITION_DATE' => date('Y-m-d'),
        		'REQUISITION_PRIORITY' => 'normal',
        		'ISCLOSED' => '0',
  	     		'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['USER_ID'],
  	     		'MODIFIED_DATE' => date('Y-m-d')
        		);

        	$reqId = $this->requisition_model->addReqMast($addReqMast);
        	for ($j=0; $j <count($encItemId) ; $j++) {
        		$itemId = $this->encrypt->decode($encItemId[$j]);

        		$addReqTrans = array(
  	     		'REQUSITION_ID' => $reqId,
  	     		'ITEM_ID' => $itemId,
  	     		'QTY_REQUESTED' => $qtyRequested[$j],
  	     		'CURRENT_STOCK' => $curStock[$j],
  	     		'MODIFIED_BY' => $this->session->userdata('admin_data')[0]['USER_ID'],
  	     		'MODIFIED_DATE' => date('Y-m-d')
  	     		);
                $data['getItemsName'] = $this->requisition_model->getItemName($itemId);

                $ITEM_NAME = $data['getItemsName']['0']['ITEM_NAME'];
                $ForMail[] = array(
                'ITEM_ID' => $ITEM_NAME,
                'QTY_REQUESTED' => $qtyRequested[$j],
                'CURRENT_STOCK' => $curStock[$j]
                );

	     		$this->requisition_model->addReqTrans($addReqTrans);
        	}


	     	$msg = 'Successfully Inserted';
            // mailer($from,$to,$cc,$bcc,$attachments,$subject,$body);
            $subject = $userCen[0]['CENTRE_NAME']." - Stationary Requisition";

            $bodymsg = "
       <p>Respected Sir/Madam,</p>

<p>Following are requisition for Stationary</p>
<table border='1' style='font-family: Arial, Helvetica, Verdana;
font-size: 14px;
line-height: 22px;
color: #666;
-webkit-text-size-adjust: none;'>
<tr>
<th>Sr. No.</th>
<th>Stationary</th>
<th>Quantity</th>
<th>Current Stock</th>
</tr>";
$Sr = 1;
for ($k=0; $k <count($ForMail) ; $k++)
{
    $ITEM_ID =$ForMail[$k]['ITEM_ID'];
    $QTY_REQUESTED =$ForMail[$k]['QTY_REQUESTED'];
    $CURRENT_STOCK =$ForMail[$k]['CURRENT_STOCK'];

    $bodymsg .= "<tr>
    <td>".$Sr."</td>
    <td>".$ITEM_ID."</td>
    <td>".$QTY_REQUESTED."</td>
    <td>".$CURRENT_STOCK."</td>
    </tr>";
    $Sr = $Sr+1;
}

$bodymsg .="</table>";

        $from = "emis@saintangelos.com";
        $to = $this->session->userdata('admin_data')[0]['EMAIL'];
        $cc = array('ankur.prajapati@saintangelos.com','kavita.kadam@saintangelos.com','maheshk@saintangelos.com');

        $this->load->mailer($from,$to,$cc,"","",$subject,$bodymsg,"enquiry");
        //$this->load->mailer('',$to,$cc,'','',$subject,$bodymsg);
     	}
     	else{
            $msg['errors'][] = $this->form_validation->error_array();
     	}
     	echo json_encode($msg);
     }

     public function stationeryRequisition(){
     	$encItemId = $this->input->post('itemId');
     	$itemId = $this->encrypt->decode($encItemId);
     	$statMaxQty = $this->requisition_model->statMaxQty($itemId);
     	echo json_encode($statMaxQty);
     }

// code for report genration ///////////////////////////////

public function RequisitionReport()
{
    $data['req_filter'] = $this->session->userdata('req_filter');
    $requisition = $this->requisition_model->getRequisitionReport($data['req_filter']);

         $this->load->library('excel');
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0);

        $col = 'A';
        for($i=0;$i<count($requisition['fields']);$i++){
            $objPHPExcel->getActiveSheet()->SetCellValue($col.'1', $requisition['fields'][$i]);
            $objPHPExcel->getActiveSheet()->getStyle("A1:X1")->getFont()->setBold(true);
            $col++;
        }


        $j = 2;
        $alphebetCol = 'A';
        foreach ($requisition['details'] as $key => $value) {

            foreach ($requisition['details'][$key] as $index => $data) {
                $objPHPExcel->getActiveSheet()->SetCellValue($alphebetCol.$j,$requisition['details'][$key][$index]);

                $alphebetCol++;
                if($alphebetCol == "M"){
                    $alphebetCol = 'A';
                }
            }

            $j++;
        }

        $filename = "Requisition_Details-" . date("h.i.s.d-m-Y") . ".xls";
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=" . $filename);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
}
     public function viewRequisition($offset = 0){

     	if(!empty($_POST)){
            $this->session->set_userdata('req_filter',$_POST);
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('req_filter');
        }

     	$data['req_filter'] = $this->session->userdata('req_filter');
     	/* Pagination starts */
        $config['base_url'] = base_url('requisition/viewRequisition/');
        $config['total_rows'] = $this->requisition_model->getReqDataCount();
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */

        $data['pagination'] = $this->pagination->create_links();
     	$data['req_list'] = $this->requisition_model->getReqData($offset, $config['per_page'], $data['req_filter']);

     	for ($i=0; $i <count($data['req_list']) ; $i++) {
     		$reqId = $data['req_list'][$i]['REQUSITION_ID'];
     		$getItem[$reqId] = $this->requisition_model->getItem($reqId);

     	}
     	$data['getItems'] = $getItem;
     	$data['centres'] = $this->tele_enquiry_model->suggested_centre();
     	$this->load->admin_view('viewReq',$data);
     }

     public function requisitionStatus($offset = 0){

     	if(!empty($_POST)){
            $this->session->set_userdata('req_filter',$_POST);
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('req_filter');
        }

     	$data['req_filter'] = $this->session->userdata('req_filter');
     	/* Pagination starts */
        $config['base_url'] = base_url('requisition/viewRequisition/');
        $config['total_rows'] = $this->requisition_model->getReqDataCount();
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */

        $data['pagination'] = $this->pagination->create_links();
     	$data['req_list'] = $this->requisition_model->getReqData($offset, $config['per_page'], $data['req_filter']);

     	for ($i=0; $i <count($data['req_list']) ; $i++) {
     		$reqId = $data['req_list'][$i]['REQUSITION_ID'];
     		$getItem[$reqId] = $this->requisition_model->getItem($reqId);

     	}
     	$data['getItems'] = $getItem;
     	$data['centres'] = $this->tele_enquiry_model->suggested_centre();
     	$this->load->admin_view('requisitionStatus',$data);
     }
}
