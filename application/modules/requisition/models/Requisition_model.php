<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Requisition_model extends CI_Model {

	  public $sql;
	  public function __construct() {
	      parent::__construct();
	  }

	public function updateRequisition($update)
	{
		$maxChallanNo = $this->maxChallanNo();

		$updateRM = array(
			"ISCLOSED" => '1',
			"CLOSED_DATE" => $update['CLOSED_DATE'],
			"DEL_THROUGH" => $update['DEL_THROUGH'],
			"REMARKS" => $update['REMARKS'],
			"APPROVED_BY" => $update['APPROVED_BY'],
			"MODIFIED_BY" => $update['MODIFIED_BY'],
			"MODIFIED_DATE" => $update['MODIFIED_DATE'],
			"CHALLAN_NO"	=> $maxChallanNo[0]['CHALLAN_NO']+1
			);

		$updateRT = array(
			"QTY_GIVEN" => $update['QTY_GIVEN'],
			"REMARKS" => $update['REMARKS'],
			"MODIFIED_BY" => $update['MODIFIED_BY'],
			"MODIFIED_DATE" => $update['MODIFIED_DATE']
			);

		$this->db->where('REQUSITION_ID',$update['REQUSITION_ID'])
						 ->update('requisition_master',$updateRM);								//for mysql

		$this->db->where('REQUISTION_TRANSACTION_ID',$update['REQUISTION_TRANSACTION_ID'])
				     ->update('requstion_transaction',$updateRT);						 //for mysql


		return;
	}
	public function getStationeryItems(){
		$query = $this->db->select('ITEM_ID,ITEM_NAME')
						  ->from('item_master')
						  ->where('ITEM_TYPE','18')
						  ->order_by('ITEM_NAME')
						  ->get();
		return $query->result_array();
	}

	public function statMaxQty($itemId){
		$query = $this->db->select('MAX_QTY_FOR_REQUISITION')
						  ->from('item_master')
						  ->where('ITEM_ID',$itemId)
						  ->get();
		return $query->result_array();
	}

	public function addReqMast($addReqMast)
	{
		$this->db->insert('requisition_master',$addReqMast);
		if ($this->db->affected_rows() > 0) {
			return $this->db->insert_id();
		}
	}

	public function addReqTrans($addReqTrans){
		$this->db->insert('requstion_transaction',$addReqTrans);			//for mysql
	}
  public function getItemName($itemId)
  {

    $query = $this->db->select('IM.ITEM_ID,IM.ITEM_NAME')
              ->from('item_master IM')
              ->where('IM.ITEM_ID',$itemId)
              ->get();
    return $query->result_array();
  }
	public function getItem($itemId)
	{

		$query = $this->db->select('RT.REQUISTION_TRANSACTION_ID,IM.ITEM_NAME,RT.QTY_REQUESTED,RT.CURRENT_STOCK')
						  ->from('requstion_transaction RT')
						  ->join('item_master IM','IM.ITEM_ID = RT.ITEM_ID')
						  ->where('RT.REQUSITION_ID',$itemId)
						  ->get();
		return $query->result_array();
	}

	public function getReqUpdateData($REQUSITION_ID)
	{
		$query = $this->db->select('RM.REQUSITION_ID,RM.REQUISITION_DATE,RM.CENTRE_ID,CM.CENTRE_NAME,RM.APPROVED_BY,RM.REQUESTED_BY,RM.CLOSED_DATE,RM.DEL_THROUGH,RM.REMARKS')
							->from('requisition_master RM')
							->join('requstion_transaction RT','RT.REQUSITION_ID = RM.REQUSITION_ID')
							->join('centre_master CM','CM.CENTRE_ID = RM.CENTRE_ID','left')

        					->where('RM.REQUSITION_ID',$REQUSITION_ID)
        					->limit('1')
        					->get();
        					return $query->result_array();

	}

	public function getReqData($offset, $perpage, $reqFilter){
      $query = $this->db->select('RM.REQUSITION_ID,RM.REQUISITION_DATE,RM.CENTRE_ID,CM.CENTRE_NAME,RT.ITEM_ID,IM.ITEM_NAME,RM.REQUISITION_PRIORITY,RM.REMARKS,concat(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as APPROVED_BY,RM.ISCLOSED,RM.DIRECTLY_ISSUED,RM.CHALLAN_NO')
                        ->from('requisition_master RM')
                        ->join('requstion_transaction RT','RT.REQUSITION_ID = RM.REQUSITION_ID')
                        ->join('centre_master CM','CM.CENTRE_ID = RM.CENTRE_ID','left')
                        ->join('employee_master EM','EM.EMPLOYEE_ID = RM.APPROVED_BY','left')
                        ->join('item_master IM','IM.ITEM_ID = RT.ITEM_ID','left');

												$role_ids = array('35','36','37','38','39','40','41','42');

								        if (in_array($this->session->userdata('admin_data')[0]['ROLE_ID'], $role_ids)){
								          $query = $this->db->where('RM.CENTRE_ID',$this->session->userdata('admin_data')[0]['CENTRE_ID']);
								        }
								        else{
													if (isset($reqFilter['CENTRE']))
										      {
										      	if ($reqFilter['CENTRE'] != '')
										      	{
										      		$query = $this->db->where('RM.CENTRE_ID',$reqFilter['CENTRE']);
										      	}
										      }
								        }

									      if(isset($reqFilter['from_date'])){

									      	 if ($reqFilter['from_date'] != '') {
									      	 	$from_date_rep = str_replace('/', '-', $reqFilter['from_date']);
									      	 	$from_date =  date("Y-m-d H:i:s", strtotime($from_date_rep));

									      	 	$query = $this->db->where('RM.REQUISITION_DATE >=',$from_date);
									                           	  // ->where('RM.REQUISITION_DATE < ',$to_date);
									      	 }
									      }

											 	if (isset($reqFilter['item']))
											  	{
											  		if ($reqFilter['item'] != '')
											  		{

											  			$query = $this->db->where('IM.ITEM_NAME',$reqFilter['item']);
											  		}
											 	}

									      if (isset($reqFilter['status']))
									      {
									      	if ($reqFilter['status'] != '')
									  		{
									      		$query = $this->db->where('RM.ISCLOSED',$reqFilter['status']);
									        }
									      }

			      $query = $this->db->limit($perpage, $offset)
			      					->group_by('RM.REQUSITION_ID')
			        				->order_by('RM.REQUSITION_ID','desc')
			        				->get();
       	// echo $this->db->last_query();
      return $query->result_array();
    }

    public function getRequisitionReport($reqFilter){
      $query = $this->db->select('RM.REQUSITION_ID,RM.REQUISITION_DATE,RM.CENTRE_ID,CM.CENTRE_NAME,RT.ITEM_ID,IM.ITEM_NAME,RM.REQUISITION_PRIORITY,RM.REMARKS,RM.APPROVED_BY,RM.ISCLOSED,RM.DIRECTLY_ISSUED,RM.CHALLAN_NO')
                        ->from('requisition_master RM')
                        ->join('requstion_transaction RT','RT.REQUSITION_ID = RM.REQUSITION_ID')
                        ->join('centre_master CM','CM.CENTRE_ID = RM.CENTRE_ID','left')
                        ->join('item_master IM','IM.ITEM_ID = RT.ITEM_ID','left');

      if (isset($reqFilter['CENTRE']))
      {
        if ($reqFilter['CENTRE'] != '')
        {
          $query = $this->db->where('RM.CENTRE_ID',$reqFilter['CENTRE']);
        }
      }

      if(isset($reqFilter['from_date'])){

         if ($reqFilter['from_date'] != '') {
          $from_date_rep = str_replace('/', '-', $reqFilter['from_date']);
          $from_date =  date("Y-m-d H:i:s", strtotime($from_date_rep));

          $query = $this->db->where('RM.REQUISITION_DATE >=',$from_date);
         }
      }

  if (isset($reqFilter['item']))
    {
      if ($reqFilter['item'] != '')
      {

        $query = $this->db->where('IM.ITEM_NAME',$reqFilter['item']);
      }
  }

      if (isset($reqFilter['status']))
      {
        if ($reqFilter['status'] != '')
      {
          $query = $this->db->where('RM.ISCLOSED',$reqFilter['status']);
        }
      }

      $query = $this->db->limit('10000')
                ->group_by('RM.REQUSITION_ID')
                ->order_by('RM.REQUSITION_ID','desc')
                ->get();

      $requisition['fields'] = $query->list_fields();
      $requisition['details'] = $query->result_array();
      return $requisition;
    }

    public function getReqDataCount($reqFilter = array()){
    	$query = $this->db->select('REQUSITION_ID')
    					  ->from('requisition_master');

      if (isset($reqFilter['CENTRE'])) {
      	$query = $this->db->where('RM.CENTRE_ID',$reqFilter['CENTRE']);
      }

      if (isset($reqFilter['REQTYPE'])) {
      	$query = $this->db->where('RM.REQUISITION_PRIORITY',$reqFilter['REQTYPE']);
      }

      if (isset($reqFilter['REQTYPE'])) {
      	$query = $this->db->where('RM.REQUISITION_PRIORITY',$reqFilter['REQTYPE']);
      }

      if (isset($reqFilter['status'])) {
      	$query = $this->db->where('RM.ISCLOSED',$reqFilter['status']);
      }

    	$query = $this->db->get();
    	return $query->num_rows();
    }

    public function maxChallanNo(){
    	$query = $this->db->select_max('CHALLAN_NO')
    					  ->from('requisition_master')
    					  ->get();
    	return $query->result_array();
    }

    public function getChallan($reqId){
    	$query = $this->db->select('CM.CENTRE_NAME,RM.DEL_THROUGH')
    					  ->from('requisition_master RM')
    					  ->join('centre_master CM','CM.CENTRE_ID = RM.CENTRE_ID')
    					  ->where('RM.REQUSITION_ID',$reqId)
    					  ->get();

    	$maxChallanNo = $this->maxChallanNo();

    	$result = $query->result_array();
    	$result[0]['CHALLAN_NO'] = $maxChallanNo[0]['CHALLAN_NO']+1;
    	return $result;
    }

    public function getChallanItems($reqId){
    	$query = $this->db->select('IM.ITEM_NAME,IM.ITEM_DESCRIPTION,RT.QTY_GIVEN')
    					  ->from('requstion_transaction RT')
    					  ->join('item_master IM','IM.ITEM_ID = RT.ITEM_ID')
    					  ->where('RT.REQUSITION_ID',$reqId)
    					  ->get();
    	return $query->result_array();
    }
}
