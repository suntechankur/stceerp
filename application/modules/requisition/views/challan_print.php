<?php

tcpdf();
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Ankur Prajapati');
$pdf->SetTitle('Delivery Chalan Print');
$pdf->SetSubject('Delivery Chalan Print');
$pdf->SetKeywords('St.angelos Delivery Chalan Print.');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// ---------------------------------------------------------

// add a page
$pdf->AddPage();

// create some HTML content

$html =

			'<TABLE width="98%">
				<TR>
					<TD align="left" colSpan="5">
            <p align="center"><img src="'.base_url('resources/images/banner.png').'"></p>
						<P align="center"><strong>St.Angelo&#39;s Computer Education</strong><br />
						Head Office: 6th
						Floor, Jyoti Plaza, Above TATA Motors, S.V.Road,<BR>
						Kandivali[w],Mumbai-400 067 . Tel:-022-28617575-78/28657525-27<BR>
						Website: www.saintangelos.com. E-mail: info@saintangelos.com
						</P>
						<P class="Header1" align="center"><strong><U>DELIVERY CHALLAN</U></strong></P>
					</TD>
				</TR>
			    <TABLE width="98%" colspan="9">
				<TR>
					<TD align="left" colspan="3"><STRONG>&nbsp;&nbsp;To: </STRONG><span>'.$challanDet[0]['CENTRE_NAME'].'</span></TD>
					<TD align="left" colspan="3"></TD>
					<TD align="left" colspan="3"><STRONG>Challan No: </STRONG><span>'.$challanDet[0]['CHALLAN_NO'].'</span></TD>

				</TR>
				<TR>
				    <TD align="left" colspan="3"><STRONG>Through: </STRONG><span>'.$challanDet[0]['DEL_THROUGH'].'</span></TD>
					<TD align="left" colspan="3"></TD>
					<TD align="left" colspan="3"><STRONG>Date: </STRONG><span>'.date("d/m/Y").'</span></TD>

				</TR>
				<TR>
					<TD class="Header2" align="left" colSpan="9">
						<HR width="90%" color="black">
					</TD>
				</TR>
				</TABLE>
				<TR>
					<TD align="center" colSpan="8">
					    <table colSpan="8" align="Center" rules="all" border="1" id="dgReqChallan" style="width:98%;border-collapse:collapse;">
						<tr class="Header1" style="border-color:Black;border-width:1px;border-style:Solid;">
							<th style="width:25%;text-align:centre"><strong>Sr. No.</strong></th>
							<th style="width:50%;text-align:centre"><strong>Description</strong></th>
							<th style="width:25%;text-align:centre"><strong>Quantity</strong></th>
						</tr>';
				$i=1;
				foreach($challanItems as $item){
				$html .= '<tr class="GridItem">
							<td>'.$i.'</td>
							<td>'.$item['ITEM_NAME'].'</td>
							<td>'.$item['QTY_GIVEN'].'</td>
						</tr>';
					$i++;
					}

				$html .= '</table></TD>
				</TR>
				<TR>
					<TD></TD>

				</TR>
				<TR>
					<TD></TD>

				</TR>
				<TABLE width="110%" colspan="9">
                <br />
                <br />
                <br />
                <br />
				<TR>
					<TD align="right" colspan="3"><STRONG>__________________</STRONG></TD>
					<TD align="left" colspan="3"></TD>
					<TD align="left" colspan="3"><STRONG>__________________</STRONG></TD>

				</TR>
				<TR>
				    <TD align="right" colspan="3"><STRONG>Receiver&#39;s Signature</STRONG></TD>
					<TD align="left" colspan="3"></TD>
					<TD align="left" colspan="3"><STRONG>Authorised Signature</STRONG></TD>

				</TR>
				<TR>
					<TD class="Header2" align="left" colSpan="9">
						<HR width="95%" color="black">
					</TD>
				</TR>
				</TABLE>

			</TABLE>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();




// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_006.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
?>
