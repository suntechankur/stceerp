<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>View Requisition</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<?php echo form_open(); ?>
				<div class="panel panel-default">
					<div class="panel-body">
							<div class="col-md-12">
								<div class="form-group">
								 <div class="row">
								 	<div class="col-md-4">
										<label for="centre">Centre</label>
										<select name="CENTRE" id="centre" class="form-control">
											<option value="">Select Centre</option>
											<?php foreach($centres as $centre){
															$role_ids = array('35','36','37','38','39','40','41','42');

															if (in_array($this->session->userdata('admin_data')[0]['ROLE_ID'], $role_ids)){
																if($centre['CENTRE_ID'] == $this->session->userdata('admin_data')[0]['CENTRE_ID']){?>
																	<option value="<?php echo $centre['CENTRE_ID'] ?>" selected="selected"><?php echo $centre['CENTRE_NAME'] ?></option>
													<?php } }
																else{?>
																	<option value="<?php echo $centre['CENTRE_ID'] ?>"><?php echo $centre['CENTRE_NAME'] ?></option>
													<?php }
															}?>
										</select>
									</div>

									<div class="col-md-4">
										<label for="item">Item</label>
										<input type="text" name="item" id="item" class="form-control">
									</div>
									<div class="col-md-4">
										<label for="status">Status</label>
										<select class="form-control" name="status" id="status">
											<option value="">All</option>
											<option value="0">Open</option>
											<option value="1">Closed</option>
										</select>
									</div>
								 </div>
								 <div class="row">
								 	<div class="col-md-4">
								 		<label for="from_date">From Date</label>
								 		<input type="text" class="enquiry_date form-control" name="from_date">
								 	</div>
								 	<div class="col-md-4">
								 		<label for="to_date">To Date</label>
								 		<input type="text" class="enquiry_date form-control" name="to_date">
								 	</div>


									<div class="col-md-4">

									</div>
								 </div>
								</div>
							</div>

					</div>
						<br>
						<div class="col-lg-12" style="margin-left:0px;">
	                        <div class="col-lg-4">
	                            <button type="submit" value="submit" id="submit" class="btn btn-primary" style="float:right">Search</button>
	                        </div>
	                        <div class="col-lg-4">
	                            <button type="submit" name="reset" value="reset" id="reset" class="btn btn-primary" style="float:left">Reset</button>
	                        </div>
	                        <?php if (!empty($req_filter)) { ?>
							<div class="col-lg-4">
							<a href="<?php echo base_url('requisition/RequisitionReport');?>" class="btn" style="float:left" title="Export to excel"><i class="fa fa-file-excel-o fa-2x text-success"></i></a>
							</div>
							<?php } ?>
	                    </div>
					</div>
					<?php echo form_close(); ?>

				</div>
			</div>
		</div>
		<div id="box"> <span class="">
			<?php echo $pagination ?>
		</span>
        	<h2 class="text-center"> Requisition List</h2>
        	<div class="panel panel-default">
        		<div class="panel-body table-responsive">
        			<table class="table table-hover table-condensed table-stripped">
        			 <thead>
        			 	<tr>
        					<th>Requisition Id</th>
        					<th>Requisition Date</th>
        					<th>Centre</th>
        					<th>Requisition Item</th>
        					<th>Remark</th>
        					<th>Approved By</th>
        					<th>Status</th>
        					<th>Issue</th>
        					<th>Challan No</th>
        				</tr>
        			 </thead>
        			 <tbody>
        			 	<?php foreach($req_list as $req){ ?>
        			 	<tr>
        			 		<td><?php echo $req['REQUSITION_ID']; ?></td>
        			 		<td>
        			 			<?php
        			 			$reqDate = date("d/m/Y", strtotime($req['REQUISITION_DATE']));
        			 			echo $reqDate; ?>
        			 		</td>
        			 		<td><?php echo $req['CENTRE_NAME']; ?></td>
        			 		<td>
        			 			<?php
        			 				foreach($getItems[$req['REQUSITION_ID']] as $getItem){
        			 						echo '<span class="label label-default">'.$getItem['ITEM_NAME'].' ('.$getItem['QTY_REQUESTED'].')</span> ';
        			 				}
        			 			?>
        			 		</td>
        			 		<td><?php echo $req['REMARKS']; ?></td>
        			 		<td><?php echo $req['APPROVED_BY']; ?></td>
        			 		<td>
        			 			<?php if($req['ISCLOSED'] == '1'){echo 'Closed';}else{echo "Open";} ?>
        			 		</td>
        			 		<td>
        			 			<?php if($req['ISCLOSED'] == '1'){ ?>
											<span class="text-default">Issued</span>
        			 			<?php } else{ ?>
									<span class="text-default"></span>
        			 			<?php } ?>
        			 		</td>
        			 		<td><?php echo $req['CHALLAN_NO']; ?></td>
        			 	</tr>
        			 	<?php } ?>
        			 </tbody>
        			</table>
        		</div>
        		 <div class="row">
		            <div class="col-md-4 pull-right">
		                <?php echo $pagination; ?>
		            </div>
		        </div>
        	</div>
        	</div>
	</div>
