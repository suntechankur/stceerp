<div id="wait" style="display: none;position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: url(../resources/images/loading.gif) center no-repeat #fff;"></div>

<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Stationery Requisition</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<?php //echo form_open(); ?>
						  <div class="form-group col-md-2">
							<label for="item">Stationery:</label>
							  <select class="form-control" id="item" class="item">
							    <option>Select Stationery</option>
							    <?php foreach($statItems as $stat){ ?>
							     <option value="<?php echo $this->encrypt->encode($stat['ITEM_ID']); ?>"><?php echo $stat['ITEM_NAME'] ?></option>
							    <?php } ?>
							  </select>
							  <span class="text-danger itemMsg"></span>
							</div>
							<div class="statForm"></div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="box"> <span class=""><p>&nbsp;</p></span>
        	<h2 class="text-center"> Requisition List</h2>
        	<div class="panel panel-default">
        		<div class="panel-body table-responsive">
        			<table class="table table-bordered table-condensed table-stripped">
        			 <thead>
        			 	<tr>
        			 		<th>Sr.No</th>
        					<th>Course Ware</th>
        					<th>Required Quantity</th>
        					<th>Max.Qty for Requsition</th>
        					<th>Current Stock</th>
        				</tr>
        			 </thead>
        			 <tbody class="statReqTbl"></tbody>
        			</table>
        		</div>
        		<div class="row">
        			<!-- <div class="col-md-4 pull-right">
        				<br>
        				<button class="btn btn-primary">Save Requisition</button>
        			</div> -->
        			<div class="col-md-4 pull-right">
        			<br>
        			<form class="saveRecDiv">

        			</form>
        			</div>
        		</div>
        	</div>
        </div>
</div>
