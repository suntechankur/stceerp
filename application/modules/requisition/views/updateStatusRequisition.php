
<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Requisition Update Status</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">

				<?php echo form_open(); ?>
				<div class="panel panel-default">
					<div class="panel-body">
							<div class="col-md-12">
								<div class="form-group">
								<?php foreach ($req as $key => $data) {
									?>
								 <div class="row">
								 	<div class="col-md-4">
										<label for="centre">Centre</label>
										<select disabled="" name="CENTRE_ID" id="CENTRE_ID" class="form-control" >
											<option value="">Select Centre</option>
											<?php foreach($centres as $centre){ ?>
												<option value="<?php echo $centre['CENTRE_ID'] ?>"<?php echo $centre['CENTRE_ID']==$data['CENTRE_ID'] ? "selected" : '';?>><?php echo $centre['CENTRE_NAME'] ?></option>
											<?php } ?>
										</select>
									</div>

									<div class="col-md-4">
										<label for="item">Requisition ID</label>
										<input type="text" name="REQUSITION_ID" value="<?php echo $data['REQUSITION_ID'] ? $data['REQUSITION_ID'] : '';?>" id="REQUSITION_ID" class="form-control">
									</div>
									<div class="col-md-4">Requested Date</label>
								 		<input type="text" class="enquiry_date form-control" name="REQUISITION_DATE" value="<?php echo $data['REQUISITION_DATE'] ? date("d/m/Y",strtotime($data['REQUISITION_DATE'])) : '';?>">
										</select>
									</div>
								 </div>
								 <div class="row">
								 	<div class="col-md-4">
								 		<label for="from_date">Requested By</label>
								 		<input type="text" class="form-control" name="REQUESTED_BY"  value="<?php echo $data['REQUESTED_BY'] ? $data['REQUESTED_BY'] : '';?>">
								 	</div>
								 	<div class="col-md-4">
								 		<label for="to_date">Issued Date <span class="text-danger">*</span></label>
								 		<input type="text" required="" class="enquiry_date form-control" name="CLOSED_DATE" value="<?php echo $data['CLOSED_DATE'] ? $data['CLOSED_DATE'] : '';?>" >
								 	</div>


									<div class="col-md-4">
										<label for="APPROVED_BY">Approved By <span class="text-danger">*</span></label>
										<?php
										// echo "<pre>";
										// print_r($empNames);
										// echo "</pre>";
										?>
									<select name="APPROVED_BY" id="APPROVED_BY" class="form-control" required="">
										<option value="">Select Name</option>
										<?php foreach($empNames as $emp){
														$employee_ids = array('3154','3170','2148');

														if (in_array($emp['EMPLOYEE_ID'], $employee_ids)){
															echo "<option value=".$emp['EMPLOYEE_ID'].">".$emp['EMP_FNAME']." ".$emp['EMP_LASTNAME']."</option>";
														}
													}?>
													<option value="2148">MAHESH KAMBLE</option>
									</select>
									<span class="text-danger"><?php echo form_error('APPROVED_BY'); ?></span>
									</div>
								 </div>
								 <div class="row">
								 	<div class="col-md-4">
								 		<label for="from_date">Delivery Through <span class="text-danger">*</span></label>
								 		<input type="text" required="" class="form-control" name="DEL_THROUGH" value="<?php echo $data['DEL_THROUGH'] ? $data['DEL_THROUGH'] : '';?>">
								 	</div>
								 	<div class="col-md-8">
								 		<label for="to_date">Remarks <span class="text-danger">*</span></label>
								 		<textarea class="enquiry_date form-control" name="REMARKS" required=""><?php echo $data['REMARKS'] ? $data['REMARKS'] : '';?></textarea>
								 	</div>

								 </div>
							<?php }?>
								 <div class="row">
								 	<div class="col-md-12">


								 		<table class="table table-hover table-condensed table-stripped">
        			 <thead>
        			 	<tr>

        					<th>Sr. No.</th>
        					<th>Item Name</th>
        					<th>Quantity Requested</th>
        					<th>Current Stock</th>
        					<th>Quantity Issued</th>

        				</tr>
        			 </thead>
        			 <tbody>
        			 	<?php
        			 	$count = $this->uri->segment(2) + 1;

        			 	foreach($getItems as $item){ ?>
        			 	<tr>
        			 		<td><?php echo $count; ?></td>
        			 		<td><?php echo $item['ITEM_NAME']; ?></td>
        			 		<td id="QTY_REQUESTED<?php echo $count;?>"><?php echo $item['QTY_REQUESTED']; ?></td>
        			 		<td><?php echo $item['CURRENT_STOCK'] ? $item['CURRENT_STOCK'] : '0';?></td>
        			 		<td><input type="text" class="QTY_ISSUED" id="<?php echo $count;?>" name="QTY_ISSUED[]" class="form-control" required=""></td>

        			 		<input type="hidden" name="REQUISTION_TRANSACTION_ID[]" value="<?php echo $item['REQUISTION_TRANSACTION_ID'] ? $item['REQUISTION_TRANSACTION_ID'] : '';?>" class="form-control">
        			 	</tr>
        			 	<?php $count++;
        			 	} ?>
        			 </tbody>
        			</table>
								 	</div>

								 </div>


								</div>
							</div>

					</div>
						<br>
						<div class="col-lg-12" style="margin-left:0px;">
	                        <div class="col-lg-6">
	                            <button type="submit" value="submit" id="submit" class="btn btn-primary" style="float:right">Update</button>
	                        </div>
	                        <div class="col-lg-6">
	                            <button type="reset" name="reset" value="reset" id="reset" class="btn btn-primary" style="float:left">Reset</button>
	                        </div>
	                    </div>
					</div>
					<?php echo form_close();
					?>

				</div>
			</div>
		</div>

	</div>
