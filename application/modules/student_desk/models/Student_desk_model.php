<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_desk_model extends CI_Model {
    public $sql;
  public function __construct() {
      parent::__construct();
  }
       public function update_password($new_password,$old_password){

        $employee_id = $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'];
        $role_id = $this->session->userdata('admin_data')[0]['ROLE_ID'];

      $this->db->set('PASSWORD',$new_password)
                ->where('PASSWORD',$old_password)
                 ->where('EMPLOYEE_ID',$employee_id)
                 ->where('ROLE_ID',$role_id)
                 ->where('ISACTIVE','1')
                 ->update('user_login');

          if ($this->db->affected_rows() > 0){
            return TRUE;
          }
          else{
            return FALSE;
          }
    }
}
