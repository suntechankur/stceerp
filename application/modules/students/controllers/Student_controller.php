<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_controller extends MX_Controller {

     public function __construct()
        {
                $this->load->model('admission/admission_model');
                $this->load->model('tele_enquiry/tele_enquiry_model');
                $this->load->model('student_model');
                parent::__construct();
        }


    public function student_details($num){
        $enqId = $this->encrypt->decode($num);
        $data['studData'] = $this->student_model->getStudentData($enqId);
        $data['centerDetails'] = $this->student_model->getCenterDetails();
        $data['universityDetails'] = $this->student_model->getUniversityDetails();
        $data['courseDetails'] = $this->student_model->getCourseDetails($enqId);

        $data['installmentPlan'] = $this->admission_model->getAdmissionInstallmentTransaction($enqId);
        $data['receiptDetails'] = $this->admission_model->getAdmissionReceipt($enqId);
        $data['courses'] = $this->tele_enquiry_model->all_courses();
        $data['modules'] = $this->tele_enquiry_model->all_modules();
        $data['leaveDetails'] = $this->student_model->getLeavingDetails($enqId);
        $data['allCourseDetails'] = $this->student_model->getAllCourseDetails();
        $data['allSavedCertificateDetails'] = $this->student_model->getAllCertificateDetails($enqId);
        $data['allPlacementDetails'] = $this->student_model->getAllPlacementDetails($enqId);

        $total_fees = 0;
        foreach($data['receiptDetails'] as $receipts){
          if($receipts['AMOUNT_DEPOSITED'] != NULL){
            $total_fees += $receipts['AMOUNT_PAID_FEES'];
          }
        }
        $data['studData'][0]['FEE_PAID'] = $total_fees;

        // echo "<pre>";
        // print_r($data['studData']);
        // echo "</pre>";

        if(count($data['receiptDetails'])){
            $totalFees = $data['receiptDetails'][0]['TOTALFEES'];
            $deductAmount = $data['receiptDetails'][0]['AMOUNT_PAID'] - $data['receiptDetails'][0]['AMOUNT_PAID_SERVICETAX'];

            for($i=0;$i<count($data['receiptDetails']);$i++){
                $j = $totalFees - $deductAmount;

                $data['receiptDetails'][$i]['BALANCES'] = $j;       //adding this value in receiptDetails array
                $totalFees = $j;

                if($i != (count($data['receiptDetails']) - 1)){
                    $deductAmount = $data['receiptDetails'][$i + 1]['AMOUNT_PAID'] - $data['receiptDetails'][$i + 1]['AMOUNT_PAID_SERVICETAX'];
                }

                $j = "";
            }
        }

        $data['otherReceipts'] = $this->admission_model->getOtherReceiptsData($enqId);
        $data['courseStatus'] = $this->student_model->getStudentCourseStatus($enqId);
        for($i=0;$i<count($data['courseStatus']);$i++){
            $lectureCount= $this->student_model->getStudentLectureDetails($data['courseStatus'][$i]['BATCHID']);
            $attendanceCount= $this->student_model->getStudentAttendanceDetails($data['courseStatus'][$i]['BATCHID'],$enqId);

            $data['courseStatus'][$i]['PRESENT'] = $attendanceCount;
            $data['courseStatus'][$i]['TOTALNOBATCH'] = $lectureCount;
        }

        // for getting the pending subject details from course details and course status array doing arraydiff for getting the resultant pending subject details

        $fullCourseSubject['COURSE_NAME'] = array();
        $courseStatus['COURSE_NAME'] = array();

        for($i=0;$i<count($data['courseDetails']);$i++){
            $fullCourseSubject['COURSE_NAME'][] = $data['courseDetails'][$i]['CERTIFICATE_TITLE'];
        }
        for($j=0;$j<count($data['courseStatus']);$j++){
            $courseStatus['COURSE_NAME'][] = $data['courseStatus'][$j]['CERTIFICATE_TITLE'];
        }

        $data['pendingSubject'][] = array_diff($fullCourseSubject['COURSE_NAME'], $courseStatus['COURSE_NAME']);

        $data['examStatus'] = $this->student_model->getStudentExamStatus($enqId);

        if(isset($_POST['update_student_data'])){
            $this->session->set_userdata('student_details',$_POST);
            $student_details = $this->student_model->update_student_details($this->session->userdata('student_details'),$enqId);
            $this->session->set_flashdata('flashMsgRemarks', $student_details);
            redirect(base_url('students/student-details/'.$this->encrypt->encode($enqId)));
        }

        $this->load->admin_view("student_details",$data);
    }

    public  function ExcelDataAdd(){
        $data = array();
      //Path of files were you want to upload on localhost (C:/xampp/htdocs/ProjectName/uploads/excel/)
      if(isset($_FILES['userfile']))
      {
          $file_name = $_FILES['userfile']['tmp_name'];

          $this->load->library('excel');
          //$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003
          $objReader= PHPExcel_IOFactory::createReader('Excel2007'); // For excel 2007
          //Set to read only
          $objReader->setReadDataOnly(true);
          //Load excel file
          $objPHPExcel=$objReader->load($file_name);
          $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel
          $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
      //     //loop from first data untill last data
          $MODIFIED_BY = $this->session->userdata('admin_data')[0]['USER_ID'];
              for($i=2;$i<=$totalrows;$i++)
              {
                  $EXAM_MASTER_NEW_ID= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
                  $JOURNAL_MARKS= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue(); //Excel Column 1
                  $JOURNAL_MARKS_OUT_OF= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue(); //Excel Column 2
                  $PROJECT_MARKS=$objWorksheet->getCellByColumnAndRow(3,$i)->getValue(); //Excel Column 3
                  $PROJECT_MARKS_OUT_OF=$objWorksheet->getCellByColumnAndRow(4,$i)->getValue(); //Excel Column 4

                  $data=array(
                  'JOURNAL_MARKS'=>$JOURNAL_MARKS,
                  'JOURNAL_MARKS_OUT_OF'=>$JOURNAL_MARKS_OUT_OF,
                  'PROJECT_MARKS'=>$PROJECT_MARKS,
                  'PROJECT_MARKS_OUT_OF'=>$PROJECT_MARKS_OUT_OF,
                  'IS_EXAM_GIVEN'=>"1",
                  'MODIFIED_BY'=>$MODIFIED_BY,
                  'MODIFIED_DATE'=>date('Y-m-d')
                  );
                 $this->student_model->upload_marks($EXAM_MASTER_NEW_ID,$data);
              }
          //unlink('uploads/excel/'.$file_name); //File Deleted After uploading in databas
          $this->session->set_flashdata('success','Upload successfully.');
          redirect(base_url("student_marks_upload"));
      }
      $this->load->admin_view('marks_upload',$data);
    }

    public function upload_student_attendance(){

      if(isset($_FILES['attendancefile'])) {
        if($_FILES['attendancefile']['size'] != 0) {
          $file_name = $_FILES['attendancefile']['tmp_name'];

          $this->load->library('excel');
          //$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003
          $objReader= PHPExcel_IOFactory::createReader('Excel2007'); // For excel 2007
          //Set to read only
          $objReader->setReadDataOnly(true);
          //Load excel file
          $objPHPExcel=$objReader->load($file_name);
          $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel
          $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);
      //     //loop from first data untill last data
          $added_by = $this->session->userdata('admin_data')[0]['USER_ID'];
              for($i=2;$i<=$totalrows;$i++)
              {
                  $deviceLogId= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
                  $userId= $objWorksheet->getCellByColumnAndRow(3,$i)->getValue(); //Excel Column 1
                  // $JOURNAL_MARKS_OUT_OF= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue(); //Excel Column 2
                  // $PROJECT_MARKS=$objWorksheet->getCellByColumnAndRow(3,$i)->getValue(); //Excel Column 3
                  $logDate=$objWorksheet->getCellByColumnAndRow(4,$i)->getFormattedValue(); //Excel Column 4
                  $newLogdate = PHPExcel_Shared_Date::ExcelToPHP($logDate);
                  $updatedLogDate= gmdate("Y-m-d H:i:s", $newLogdate);
                  // echo $logDate."<br>".$field1."<br/>";
                  $data=array(
                  'device_log_id'=>$deviceLogId,
                  'user_id'=>$userId,
                  'log_date'=>$updatedLogDate,
                  'is_active'=>1,
                  'added_by'=>$added_by,
                  'added_on'=>date('Y-m-d')
                  );
                 $this->student_model->upload_attendance_log($data);
              }
          //unlink('uploads/excel/'.$file_name); //File Deleted After uploading in databas
          $this->session->set_flashdata('success','Upload successfully.');
          redirect(base_url("students/student_attendance_upload"));
          die();
        }
      }

      $this->load->admin_view('student_attendance_upload');
    }

    public function student_attendance(){
        if(isset($_POST['generate_attendance'])){
            $this->session->set_userdata('attendance_data',$_POST);
        }
        if(isset($_POST['reset_attendance'])){
            $this->session->unset_userdata('attendance_data');
            redirect('hra/attendance');
        }

        $data['student_attendance_search'] = $this->session->userdata('attendance_data');

        $data['centres'] = $this->tele_enquiry_model->get_centres();
        $data['student_details'] = $this->student_model->get_student_details($data['student_attendance_search']);

        $from_date = date('d-m-Y', strtotime(str_replace('/', '-', $data['student_attendance_search']['from_date'])));
        $to_date = date('d-m-Y', strtotime(str_replace('/', '-', $data['student_attendance_search']['to_date'])));
        $start = strtotime($from_date);
        $end = strtotime($to_date);
        $day = (24*60*60);

        // for getting working hours reports using
        if((isset($data['student_attendance_search']['from_date'])) && (isset($data['student_attendance_search']['to_date']))){
          $from_date_month_name = date("F",strtotime(str_replace('/', '-', $data['student_attendance_search']['from_date'])));
          $from_date_year = date("Y",strtotime(str_replace('/', '-', $data['student_attendance_search']['from_date'])));
          $to_date_month_name = date("F",strtotime(str_replace('/', '-', $data['student_attendance_search']['to_date'])));
          $to_date_year = date("Y",strtotime(str_replace('/', '-', $data['student_attendance_search']['to_date'])));
          $datediff = $end - $start;
          $days_count = round($datediff / (60 * 60 * 24));
          if($from_date_year == $to_date_year){
            if($from_date_month_name == $from_date_month_name){
              $report_of = $from_date_month_name." ".$from_date_year;
            }
          }
          else{
            $report_of = $from_date_month_name." ".$from_date_year." - ".$to_date_month_name." ".$to_date_year;
          }
        }

        $dateArray = array();
        for($i=$start; $i<= $end; $i+=86400){
          $date_with_days = array('Date'=>date('d-m-Y', $i),'Day'=>date('l', $i));
          array_push($dateArray,$date_with_days);
        }

          // $holidays_list = $this->HRA_model->get_holidays_list();
          // $holidays_data = array();
          // foreach($holidays_list as $holidays){
          //   $timestamp = strtotime($holidays['DATE']);
          //   $date = date('d-m-Y',$timestamp);
          //   array_push($holidays_data,$date);
          // }
          //
          // $holidays_modifid = array();
          // foreach($holidays_list as $holiday){
          //   $timestamp = strtotime($holiday['DATE']);
          //   $date = date('d-m-Y',$timestamp);
          //   array_push($holidays_modifid,$date);
          // }

          if(!empty($data['student_attendance_search'])){
            if(($data['student_attendance_search']['from_date'] != "") && ($data['student_attendance_search']['to_date'] != "") && ($data['student_attendance_search']['centre_name'] != "")){
              $employee_details_with_attendance = array(); // blank array for final attandance data with employee_details

              foreach($data['student_details'] as $employee){
                // $data['employee_timings'] = $this->HRA_model->get_employee_timing($employee['EMPLOYEE_ID']);   // for getting employee timing details from employee timing table
                // $employee_timing = array();
                // $employee_timing_with_days = array();
                // foreach($data['employee_timings'] as $employe_timing){
                //   array_push($employee_timing,$employe_timing['Timing']);
                //   array_push($employee_timing_with_days,array('Days'=>$employe_timing['DAYS'],'Time'=>$employe_timing['TIMING_FROM']));
                // }
                // $employee_timing = implode(", ", $employee_timing);

                // $shift_details = array();
                // foreach($employee_timing_with_days as $employee_time_and_day){
                //   $day_initial = explode("/",$employee_time_and_day['Days']);
                //   foreach($day_initial as $days){
                //     switch ($days) {
                //       case 'M':
                //           array_push($shift_details,array('Day_name'=>'Monday','Time'=>$employee_time_and_day['Time']));
                //           break;
                //       case 'T':
                //           array_push($shift_details,array('Day_name'=>'Tuesday','Time'=>$employee_time_and_day['Time']));
                //           break;
                //       case 'W':
                //           array_push($shift_details,array('Day_name'=>'Wednesday','Time'=>$employee_time_and_day['Time']));
                //           break;
                //       case 'Th':
                //           array_push($shift_details,array('Day_name'=>'Thursday','Time'=>$employee_time_and_day['Time']));
                //           break;
                //       case 'F':
                //           array_push($shift_details,array('Day_name'=>'Friday','Time'=>$employee_time_and_day['Time']));
                //           break;
                //       case 'S':
                //           array_push($shift_details,array('Day_name'=>'Saturday','Time'=>$employee_time_and_day['Time']));
                //           break;
                //     }
                //   }
                // }

                $data['attendance_report'] = $this->student_model->get_attendance_report($employee['BIOMETRIC_DEVICE_ID'],$data['student_attendance_search']['from_date'],$data['student_attendance_search']['to_date']);
                // echo "<pre>";print_r($data['attendance_report']);echo "</pre>";
                // for getting employee attendance details from biometric device id from exportnewlog  table

                // $data['leave_approval_details'] = $this->HRA_model->get_leave_approval_report($employee['EMPLOYEE_ID'],$employee['BIOMETRIC_DEVICE_ID'],$data['student_attendance_search']['from_date'],$data['student_attendance_search']['to_date']);


                $attendancedate = array();  // blank array for getting all attendance datetime between from and to date
                $off_punch_count = 0;
                for($index=0;$index<count($dateArray);$index++){
                  $attendance_dates = array();  // blank array for attendance datetime one by one between from and to date

                  foreach($data['attendance_report'] as $attendance){
                    $timestamp = strtotime($attendance['log_date']);
                    $date = date('d-m-Y',$timestamp);

                    if($dateArray[$index]['Date'] == $date){
                      array_push($attendance_dates,$attendance['log_date']);
                    }
                  }

                  $count_timing = count($attendance_dates) - 1;
                  $in_time = "";
                  $out_time = "";
                  $difference = "";
                  $color = "";
                  $text_color = "";
                  $hrs_color = "";
                  $shift_time_difference = "";

                  if(count($attendance_dates) == 0){
                    // if(in_array($dateArray[$index]['Date'], $holidays_data)){
                    //   $in_time = "H";
                    //   $out_time = "H";
                    //   $difference = "H";
                    // }
                    // else
                    if($dateArray[$index]['Day'] == "Sunday"){
                      $in_time = "O";
                      $out_time = "O";
                      $difference = "O";
                    }
                    else{
                      $in_time = "A";
                      $out_time = "A";
                      $difference = "A";
                      $text_color = "red";
                      // foreach($data['leave_approval_details'] as $leave_approvals){
                      //   $modified_date_format = date('Y-m-d 00:00:00',strtotime($dateArray[$index]['Date']));
                      //   if(in_array($modified_date_format, $leave_approvals)){
                      //     if($leave_approvals['approval_status'] == "approved"){
                      //       $in_time = "AL";
                      //       $out_time = "AL";
                      //       $difference = "AL";
                      //       $text_color = "green";
                      //     }
                      //     elseif($leave_approvals['approval_status'] == "disapproved"){
                      //       $in_time = "NAL";
                      //       $out_time = "NAL";
                      //       $difference = "NAL";
                      //       $text_color = "blue";
                      //     }
                      //   }
                      // }
                    }
                  }

                  for($j=0;$j<count($attendance_dates);$j++){
                    $in_time = date('h:i A',strtotime($attendance_dates['0']));
                    $out_time = date('h:i A',strtotime($attendance_dates[$count_timing]));
                    // foreach($shift_details as $timing_details){
                    //   if($timing_details['Day_name'] == $dateArray[$index]['Day']){
                    //     $shift_time = date('h:i A',strtotime($timing_details['Time']));
                    //
                    //     if(strtotime($in_time) > strtotime($timing_details['Time'])){
                    //       $shift_time_difference = $this->dateDiff($shift_time, $in_time);
                    //     }
                    //     else{
                    //       $shift_time_difference = '0';
                    //     }
                    //   }
                    // }

                    // hours difference updated on 11/10/2018 by ankur
                    $difference = $this->dateDiff($out_time, $in_time);
                    if($difference == ""){
                      $difference = "0";
                      $hrs_color = "";
                    }
                    // if($difference < 8.30){
                    //   if($difference == 'A'){
                    //     $hrs_color = "";
                    //   }
                    //   elseif ($difference == 'O') {
                    //     $hrs_color = "";
                    //   }
                    //   else{
                    //     $hrs_color = "red";
                    //   }
                    // }
                    // end of difference implemented

                  }

                  // color bifercation for holidays
                  // if(in_array($dateArray[$index]['Date'], $holidays_data)){
                  //   $color = "rgba(220, 220, 220, 0.9)";
                  //   $off_punch_count+= 1;
                  // }
                  if($dateArray[$index]['Day'] == "Sunday"){
                    $color = "rgba(255, 255, 0, 0.4)";
                    $off_punch_count+= 1;
                  }

                  if($in_time != "O"){
                    // if(in_array($dateArray[$index]['Date'], $holidays_data)){
                    //   $color = "rgba(0, 125, 0, 0.4)";
                    // }
                    if($dateArray[$index]['Day'] == "Sunday"){
                      $color = "rgba(0, 125, 0, 0.4)";
                    }
                  }

                  $attendance_details = array(
                                            'Admission_Id'=>$employee['ADMISSION_ID'],
                                            'Biometric_id'=>$employee['BIOMETRIC_DEVICE_ID'],
                                            'Centre_id'=>$employee['CENTRE_ID'],
                                            'Student_Name'=>$employee['Student_Name'],
                                            'Date'=>$dateArray[$index]['Date'],
                                            'Date_no'=>date('d',strtotime($dateArray[$index]['Date'])),
                                            'Day'=>$dateArray[$index]['Day'],
                                            'In'=>$in_time,
                                            'Out'=>$out_time,
                                            'Hrs'=>$difference,
                                            'Hrs_Color'=>$hrs_color,
                                            'Time_diff'=>$shift_time_difference,
                                            'Color'=>$color,
                                            'Textcolor'=>$text_color);

                  array_push($attendancedate,$attendance_details);
                }

                array_push($employee_details_with_attendance,array('Admission_Id'=>$employee['ADMISSION_ID'],'Student_Name'=>$employee['Student_Name'],'BIOMETRIC_ID'=>$employee['BIOMETRIC_DEVICE_ID'],'CENTRE_ID'=>$employee['CENTRE_ID'],'Attendance_Data'=>$attendancedate,"Actual_off_days"=>$off_punch_count));

              }

              $data['student_details_with_attendance'] = $employee_details_with_attendance;
            }
          }
          // echo "<pre>";
          // print_r($data);
          // echo "</pre>";
        $this->load->admin_view('student_attendance_details',$data);
    }

    // Time format is UNIX timestamp or
    // PHP strtotime compatible strings
    public function dateDiff($time1, $time2, $precision = 6) {
      date_default_timezone_set("UTC");
      // If not numeric then convert texts to unix timestamps
      if (!is_int($time1)) {
        $time1 = strtotime($time1);
      }
      if (!is_int($time2)) {
        $time2 = strtotime($time2);
      }

      // If time1 is bigger than time2
      // Then swap time1 and time2
      if ($time1 > $time2) {
        $ttime = $time1;
        $time1 = $time2;
        $time2 = $ttime;
      }

      // Set up intervals and diffs arrays
      $intervals = array('year','month','day','hour','minute','second');
      $diffs = array();

      // Loop thru all intervals
      foreach ($intervals as $interval) {
        // Create temp time from time1 and interval
        $ttime = strtotime('+1 ' . $interval, $time1);
        // Set initial values
        $add = 1;
        $looped = 0;
        // Loop until temp time is smaller than time2
        while ($time2 >= $ttime) {
          // Create new temp time from time1 and interval
          $add++;
          $ttime = strtotime("+" . $add . " " . $interval, $time1);
          $looped++;
        }

        $time1 = strtotime("+" . $looped . " " . $interval, $time1);
        $diffs[$interval] = $looped;
      }

      $count = 0;
      $times = array();
      // Loop thru all diffs
      foreach ($diffs as $interval => $value) {
        // Break if we have needed precission
        if ($count >= $precision) {
          break;
        }
        // Add value and interval
        // if value is bigger than 0
        if ($value > 0) {
          // Add s if value is not 1
          if ($value != 1) {
            $interval .= "s";
          }
          // Add value and interval to times array
          $times[] = $value . " " . $interval;
          $count++;
        }
      }

      // Return string with times

      $actual_time =  "";
      $hr = "";
      $min = "";
      foreach($times as $time){
        if (strpos($time, 'hour') !== false) {
          $hr = preg_replace('/\D/', '', $time);
        }
        if (strpos($time, 'minute') !== false) {
          $min = preg_replace('/\D/', '', $time);
        }
      }

      $actual_time = $hr.".".$min;
      if($min == ""){
        $actual_time = $hr;
      }
      elseif(($hr == "") && ($min != "")){
        $actual_time = "0.".$min;
      }
      // return implode(".", $times);
      return $actual_time;
    }


}
