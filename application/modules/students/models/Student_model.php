<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_model extends CI_Model {

  public $sql;
  public function __construct() {
      parent::__construct();
  }

    public function getStudentData($enqId){
        $where = "AM.ADMISSION_ID = '$enqId' AND AR.ISACTIVE='1' AND AR.AMOUNT_PAID IS NOT NULL";
        $query = $this->db->select('AM.ADMISSION_ID,AM.ENQUIRY_ID,EM.ENQUIRY_FIRSTNAME,EM.ENQUIRY_MIDDLENAME,EM.ENQUIRY_LASTNAME,CM.CENTRE_NAME,AM.ADMISSION_DATE,AM.STREAM,AM.COURSE_TAKEN,AM.MODULE_TAKEN,AM.STATUS,AM.REMARKS,AM.ADMISSION_TYPE,AM.TOTALFEES,SUM(AR.AMOUNT_PAID_FEES) as FEE_PAID,EM.ENQUIRY_ADDRESS1,EM.ENQUIRY_ADDRESS2,EM.ENQUIRY_CITY,EM.ENQUIRY_ZIP,EM.ENQUIRY_STATE,EM.ENQUIRY_COUNTRY,EM.ENQUIRY_MOBILE_NO,EM.ENQUIRY_PARENT_NO,EM.ENQUIRY_EMAIL,EM.ENQUIRY_DATEOFBIRTH,EpM.EMP_FNAME,AM.IN_TAKE,AM.YEAR,AM.ADMISSION_TYPE,AM.CENTRE_ID,CM.CENTRE_NAME,AM.UNIVERSITY_NAME,AM.UNIVERSITY_REGISTRATION_NO,AM.UNIVERSITY_REGISTRATION_NO_2')
                          ->from('admission_master AM')
                          ->join('enquiry_master EM', 'AM.ENQUIRY_ID=EM.ENQUIRY_ID', 'left')
                          ->join('centre_master CM', 'AM.CENTRE_ID=CM.CENTRE_ID', 'left')
                          ->join('admission_receipts AR', 'AM.ADMISSION_ID=AR.ADMISSION_ID', 'left')
                          ->join('employee_master EpM', 'AM.HANDLED_BY=EpM.EMPLOYEE_ID', 'left')
                          ->where($where)
                          ->get();

        return $query->result_array();
    }
    public function getCenterDetails(){
        $query = $this->db->select('*')
                          ->from('centre_master')
                          ->group_by('CENTRE_NAME')
                          ->order_by('ISACTIVE','DESC')
                          ->get();

        return $query->result_array();
    }
    public function getUniversityDetails(){
        $query = $this->db->select('UNIVERSITY_NAME')
                          ->from('admission_master')
                          ->group_by('UNIVERSITY_NAME')
                          ->get();

        return $query->result_array();
    }
    public function getCourseDetails($enqId){
        $query = $this->db->select('MM.MODULE_NAME,CM.COURSE_NAME,ACT.COURSE_ID,CM.CERTIFICATE_TITLE')
                          ->from('admission_course_transaction ACT')
                          ->join('module_master MM', 'MM.MODULE_ID=ACT.MODULE_ID', 'left')
                          ->join('course_master CM', 'CM.COURSE_ID=ACT.COURSE_ID', 'left')
                          ->where('ACT.ADMISSION_ID',$enqId)
                          ->get();

        return $query->result_array();
    }
    public function getStudentCourseStatus($enqId){
        $query = $this->db->select('BST.BATCHID,CM.COURSE_NAME,CM.CERTIFICATE_TITLE,CeM.CENTRE_NAME,BM.FACULTY_NAME,EM.EMP_FNAME,EM.EMP_MIDDLENAME,EM.EMP_LASTNAME,BM.DAYS,BM.STARTTIME,BM.ENDTIME,BM.STARTDATE,BM.ACTUALENDDATE')
                          ->from('batch_master_std_trans BST')
                          ->join('batch_master BM', 'BM.BATCHID=BST.BATCHID', 'left')
                          ->join('course_master CM', 'BM.SUBJECT=CM.COURSE_ID', 'left')
                          ->join('centre_master CeM', 'BM.CENTRE_ID=CeM.CENTRE_ID', 'left')
                          ->join('employee_master EM', 'BM.FACULTY=EM.EMPLOYEE_ID', 'left')
                          ->where('BST.ADMISSION_ID',$enqId)
                          ->get();

        return $query->result_array();
    }
    public function getStudentLectureDetails($batchId){
        $query = $this->db->select('BATCH_ID')
                          ->from('batch_lecture_details')
                          ->where('BATCH_ID',$batchId)
                          ->get();

        return $query->num_rows();
    }
    public function getStudentAttendanceDetails($batchId,$enqId){
      $where = "BLD.BATCH_ID ='$batchId' AND BAD.ADMISSION_ID ='$enqId' AND PRESENT = '1'";
        $query = $this->db->select('PRESENT')
                          ->from('batch_lecture_details BLD')
                          ->join('batch_attendance_details BAD', 'BLD.BATCH_LECTURE_ID=BAD.BATCH_LECTURE_ID', 'left')
                          ->where($where)
                          ->get();

        return $query->num_rows();
    }
    public function getStudentExamStatus($enqId){
        $query = $this->db->select('EMN.EXAM_MASTER_NEW_ID as EXAM_ID,EMN.EXAM_DATE,EMN.EXAM_TIME,EMN.ONLINE_MARKS as EXTERNAL_MARKS,EMN.ONLINE_MARKS_OUT_OF as EXTERNAL_OUT_OF,EMN.PROJECT_MARKS as INTERNAL_MARKS,EMN.PROJECT_MARKS_OUT_OF as INTERNAL_OUT_OF,EMN.PERCENTAGE,EMN.RESULT,CM.COURSE_NAME')
                          ->from('exam_master_new EMN')
                          ->join('course_master CM', 'CM.COURSE_ID=EMN.COURSE_ID', 'left')
                          ->where('EMN.ADMISSION_ID',$enqId)
                          ->get();

        return $query->result_array();
    }
    public function upload_marks($EXAM_MASTER_NEW_ID,$data){
      $this->db->set($data)
             ->where('EXAM_MASTER_NEW_ID',$EXAM_MASTER_NEW_ID)
             ->update('exam_master_new');       // for mysql

    }

    public function upload_attendance_log($data){
      $this->db->insert('student_attendance_log',$data);
      // $this->db->set($data)
      //        ->where('EXAM_MASTER_NEW_ID',$EXAM_MASTER_NEW_ID)
      //        ->update('exam_master_new');       // for mysql
    }

  public function update_student_details($student_data,$admission_id){
    unset($student_data['update_student_data']);
    $this->db->set('STATUS',$student_data['status'])
           ->where('admission_id',$admission_id)
           ->update('admission_master');       // for mysql

    unset($student_data['status']);
    if(isset($student_data['enquiry_id'])){
      $this->db->set($student_data)
             ->where('enquiry_id',$student_data['enquiry_id'])
             ->update('enquiry_master');       // for mysql
    }


           if($this->db->affected_rows() > 0){
             return $this->db->insert_id();
           }
  }

  public function getExamBookingDetails($admission_id){
      $query = $this->db->select('EXAM_MASTER_NEW_ID,EXAM_DATE,EXAM_TIME,ONLINE_MARKS,ONLINE_MARKS_OUT_OF,JOURNAL_MARKS,JOURNAL_MARKS_OUT_OF,PROJECT_MARKS,PROJECT_MARKS_OUT_OF,PERCENTAGE,EXAM_FEES_RECEIPT_NO,EXAM_TYPE,RESULT,em.COURSE_ID as COURSE_ID,COURSE_NAME,IS_EXAM_GIVEN')
                        ->from('exam_master_new em')
                        ->join('course_master cm','em.COURSE_ID=cm.COURSE_ID','left')
                        ->where('ADMISSION_ID',$admission_id)
                        ->get();

      return $query->result_array();
  }

  public function getExam_BookingDate($stream,$exam_type){
      $query = $this->db->select('EXAM_DATE')
                        ->from('exam_schedule_master')
                        ->where('TRUE','1');
                        if($exam_type == "practical"){
                          $query = $this->db->where('STREAM',$stream);
                        }
                        $query = $this->db->where('EXAM_TYPE',$exam_type)
                        ->get();

      return $query->result_array();
  }

  public function bookExam($data,$exam_type){
    unset($data['book_exam']);
    if($exam_type == "theory"){
      $start_date_format_change = str_replace("/","-",$data['START_DATE']);
      $data['START_DATE'] = date('Y-m-d', strtotime($start_date_format_change));
      $end_date_format_change = str_replace("/","-",$data['END_DATE']);
      $data['END_DATE'] = date('Y-m-d', strtotime($end_date_format_change));
    }

    $data['MODIFIED_BY'] = $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'];
    $data['MODIFIED_DATE'] = date("Y-m-d");
    if($exam_type == "practical"){
      $data['EXAM_TYPE'] = "PRACTICAL";
      $this->db->insert('exam_master_new',$data);   // for mysql
      if($this->db->affected_rows() > 0){
        return $this->db->insert_id();
      }
      else{
        return false;
      }
    }
    else{
      $data['EXAM_TYPE'] = "THEORY";
      $query = $this->db->select('count(QNo) as question_count')
                        ->from('question_paper_master')
                        ->where('CourseID',$data['COURSE_ID'])
                        ->group_by('QNo')
                        ->get();
      $question_count = $query->row()->question_count;

      $query1 = $this->db->select('count(Course_ID) as qpcount')
                        ->from('qpattern')
                        ->where('Course_ID',$data['COURSE_ID'])
                        ->group_by('Course_ID')
                        ->get();
      $qpcount = $query1->row()->qpcount;

      if($question_count && $qpcount){
        $this->db->insert('exam_master_new',$data);// for mysql
        if($this->db->affected_rows() > 0){
          return $this->db->insert_id();
        }
        else{
          return "false";
        }
      }
      else{
        return "notfound";
      }
    }

  }

  public function getExamBookingTimingSlots($centreid,$examdate,$exam_type){
    $query = $this->db->select('EXAM_TIME,count(EXAM_TIME) as Booking_Count')
                      ->from('exam_master_new')
                      ->where('CENTRE_ID',$centreid)
                      ->where('EXAM_DATE',$examdate)
                      ->where('EXAM_TYPE',$exam_type)
                      ->order_by('EXAM_TIME')
                      ->group_by('EXAM_TIME')
                      ->get();

    return $query->result_array();
  }

  public function getExamHallticketDetails($admId,$examid,$exam_type){
    $query = $this->db->select('CONCAT(EM.ENQUIRY_FIRSTNAME," ",EM.ENQUIRY_LASTNAME) as Name,CM.COURSE_NAME,EN.ADMISSION_ID,EN.EXAM_MASTER_NEW_ID as EXAM_ID,EN.EXAM_DATE,EN.EXAM_TIME,CeM.CENTRE_NAME as Exam_Center,CE.CENTRE_NAME as CENTRE,CE.ADDRESSS1 as ADDRESS,EN.EXAM_TYPE')
                      ->from('exam_master_new EN')
                      ->join('admission_master AM','EN.ADMISSION_ID=AM.ADMISSION_ID','left')
                      ->join('enquiry_master EM','EM.ENQUIRY_ID=AM.ENQUIRY_ID','left')
                      ->join('course_master CM','CM.COURSE_ID=EN.COURSE_ID','left')
                      ->join('centre_master CeM','CeM.CENTRE_ID=AM.CENTRE_ID','left')
                      ->join('centre_master CE','CE.CENTRE_ID=EN.CENTRE_ID','left')
                      ->where('EN.ADMISSION_ID',$admId)
                      ->where('EN.EXAM_MASTER_NEW_ID',$examid)
                      ->where('EN.EXAM_TYPE',$exam_type)
                      ->get();

    return $query->result_array();
  }

  public function getAllCourseDetails(){
        // $this->db->select("COURSE_ID,COURSE_NAME,MAX_FEES,MIN_FEES,BEST_FEES,ISACTIVE,CERTIFICATE_TITLE");
        // $this->db->from("course_master");
        // $query1 = $this->db->get_compiled_select(); // It resets the query just like a get()

        $this->db->select("MODULE_ID as COURSE_ID,MODULE_NAME as COURSE_NAME,MAX_FEES,MIN_FEES,BEST_FEES,ISACTIVE,CERTIFICATE_TITLE");
        $this->db->from("module_master");
        $query = $this->db->get();

        // $query = $this->db->query($query1." UNION ".$query2." order by course_name");


    return $query->result_array();
  }

  public function getMarksAndUpdateMarks($examId){
        $this->db->select("EXAM_MASTER_NEW_ID as EXAM_ID,COURSE_ID,PROJECT_MARKS,PROJECT_MARKS_OUT_OF,EXAM_TYPE");
        $this->db->from("exam_master_new");
        $this->db->where("EXAM_MASTER_NEW_ID",$examId);
        $query = $this->db->get(); // It resets the query just like a get()

    return $query->result_array();
  }

  public function updateMarksInExamTable($examId,$examType,$examMarks){
    $this->db->set($examMarks)
           ->where('EXAM_MASTER_NEW_ID',$examId)
           ->where('EXAM_TYPE',$examType)
           ->update('exam_master_new');       // for mysql

           if($this->db->affected_rows() > 0){
             return $this->db->insert_id();
           }
  }

  public function save_certificate_data($certificate_data){
    unset($certificate_data['save_certificate_data']);
    $exam_ids = explode(",",$certificate_data['EXAM_MASTER_NEW_ID']);
    unset($certificate_data['EXAM_MASTER_NEW_ID']);
    $this->db->insert('certificate_issue_details_new ',$certificate_data);
    if($this->db->affected_rows() > 0){
      $subject_data['CERTIFICATE_ISSUE_DETAILS_NEW_ID'] = $this->db->insert_id();
      $subject_data['MODIFIED_BY'] = $certificate_data['MODIFIED_BY'];
      $subject_data['MODIFIED_DATE'] = $certificate_data['MODIFIED_DATE'];
      foreach($exam_ids as $exam_id){
        $subject_data['EXAM_MASTER_NEW_ID'] = $exam_id;
        $this->db->insert('CERTIFICATE_SUBJECT_DETAILS',$subject_data);
      }
      return $this->db->insert_id();
    }
    else{
      return "false";
    }
  }

  public function getAllCertificateDetails($admission_id){
    $query = $this->db->select('CERTIFICATE_ISSUE_DETAILS_NEW_ID as CERTIFICATE_ID,CONCAT(EM.ENQUIRY_FIRSTNAME," ",EM.ENQUIRY_LASTNAME) as Name,CONCAT(EeM.EMP_FNAME," ",EeM.EMP_LASTNAME) as Employee_Name,CE.CENTRE_NAME as CENTRE,CD.MODULE_ID,CD.ISSUE_DATE,EXAM_MASTER_NEW_ID as EXAM_ID')
                      ->from('certificate_issue_details_new  CD')
                      ->join('admission_master AM','CD.ADMISSION_ID=AM.ADMISSION_ID','left')
                      ->join('enquiry_master EM','EM.ENQUIRY_ID=AM.ENQUIRY_ID','left')
                      ->join('centre_master CE','CE.CENTRE_ID=AM.CENTRE_ID','left')
                      ->join('employee_master EeM','EeM.EMPLOYEE_ID=CD.ISSUED_BY','left')
                      ->where('CD.ADMISSION_ID',$admission_id)
                      ->get();

    return $query->result_array();
  }

  public function getCertificateAndMarksDetails($certificate_id){
    $query = $this->db->select('CERTIFICATE_ISSUE_DETAILS_NEW_ID as Certificate_id,CONCAT(EM.ENQUIRY_FIRSTNAME,"   ",EM.ENQUIRY_MIDDLENAME,"   ",EM.ENQUIRY_LASTNAME) as Name,CE.CENTRE_NAME as CENTRE,CD.MODULE_ID,CD.ISSUE_DATE,MM.CERTIFICATE_TITLE as COURSE_NAME')
                      ->from('CERTIFICATE_ISSUE_DETAILS_NEW CD')
                      ->join('admission_master AM','CD.ADMISSION_ID=AM.ADMISSION_ID','left')
                      ->join('enquiry_master EM','EM.ENQUIRY_ID=AM.ENQUIRY_ID','left')
                      ->join('centre_master CE','CE.CENTRE_ID=AM.CENTRE_ID','left')
                      ->join('module_master MM','CD.MODULE_ID=MM.MODULE_ID','left')
                      ->where('CD.CERTIFICATE_ISSUE_DETAILS_NEW_ID',$certificate_id)
                      ->get();

    $certificate = $query->result_array();

    $query1 = $this->db->select('EN.EXAM_MASTER_NEW_ID as EXAM_ID,CM.COURSE_NAME,CM.CERTIFICATE_TITLE,EN.START_DATE,EN.END_DATE,EN.ONLINE_MARKS,EN.ONLINE_MARKS_OUT_OF,EN.JOURNAL_MARKS,EN.JOURNAL_MARKS_OUT_OF,EN.PROJECT_MARKS,EN.PROJECT_MARKS_OUT_OF,EN.PERCENTAGE,EN.RESULT')
                      ->from('CERTIFICATE_SUBJECT_DETAILS CS')
                      ->join('exam_master_new EN','CS.EXAM_MASTER_NEW_ID=EN.EXAM_MASTER_NEW_ID','left')
                      ->join('course_master CM','EN.COURSE_ID=CM.COURSE_ID','left')
                      ->where('CS.CERTIFICATE_ISSUE_DETAILS_NEW_ID',$certificate_id)
                      ->get();
    $exam_marks = $query1->result_array();

    array_push($certificate[0],$exam_marks);
    return $certificate;
  }

  public function getLeavingDetails($admission_id){
    $query = $this->db->select('CONCAT(EM.ENQUIRY_FIRSTNAME," ",EM.ENQUIRY_LASTNAME) as Name,SL.APPLICATION_DATE,SL.LEAVE_FROM_DATE,SL.LEAVE_TO_DATE,SL.LEAVE_REASON,SL.REMARKS')
                      ->from('student_leave_apllication_details SL')
                      ->join('admission_master AM','SL.ADMISSION_ID=AM.ADMISSION_ID','left')
                      ->join('enquiry_master EM','EM.ENQUIRY_ID=AM.ENQUIRY_ID','left')
                      ->where('SL.ADMISSION_ID',$admission_id)
                      ->get();

    return $query->result_array();
  }

  public function getAllPlacementDetails($admission_id){
    $query = $this->db->select('CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as REMARKS_BY,PR.ADMISSION_ID,CM.CENTRE_NAME,PR.REMARKS_SOURCE,PR.REMARKS,PR.REMARKS_DATE,PR.INTERNAL_PROCESS,PR.STATUS')
                      ->from('placement_remarks PR')
                      ->join('admission_master AM','PR.ADMISSION_ID=AM.ADMISSION_ID','left')
                      ->join('centre_master CM','CM.CENTRE_ID=AM.CENTRE_ID','right')
                      ->join('employee_master EM','EM.EMPLOYEE_ID=PR.REMARKS_BY','right')
                      ->where('PR.ADMISSION_ID',$admission_id)
                      ->get();

    return $query->result_array();
  }

  public function get_student_details($attendance_data){
    $query = $this->db->select('AM.ADMISSION_ID,AM.CENTRE_ID,CONCAT(EM.ENQUIRY_FIRSTNAME," ",EM.ENQUIRY_MIDDLENAME," ",EM.ENQUIRY_LASTNAME) as Student_Name,AM.BIOMETRIC_DEVICE_ID')
                      ->from('admission_master AM')
                      ->join('enquiry_master EM','AM.ENQUIRY_ID=EM.ENQUIRY_ID','inner');

    if(!empty($attendance_data)) {
      if(isset($attendance_data['first_name'])){
        if ($attendance_data['first_name'] != '') {
          $query = $this->db->like('EM.ENQUIRY_FIRSTNAME',$attendance_data['first_name'],'after');
        }
      }
      if(isset($attendance_data['last_name'])){
        if ($attendance_data['last_name'] != '') {
          $query = $this->db->like('EM.ENQUIRY_LASTNAME',$attendance_data['last_name'],'after');
        }
      }
      if(isset($attendance_data['active_status'])){
        if ($attendance_data['active_status'] != '') {
          $query = $this->db->where('EM.IS_ACTIVE',$attendance_data['active_status']);
        }
      }
      if(isset($attendance_data['centre_name'])){
        if ($attendance_data['centre_name'] != '') {
          $query = $this->db->where('AM.CENTRE_ID',$attendance_data['centre_name']);
        }
      }
      if(isset($attendance_data['admission_id'])){
        if ($attendance_data['admission_id'] != '') {
          $query = $this->db->where('AM.ADMISSION_ID',$attendance_data['admission_id']);
        }
      }
    }

    $query = $this->db->get();

    return $query->result_array();
  }

  public function get_attendance_report($biometric_id,$from_date,$to_date){
    $date_format_change = str_replace("/","-",$from_date);
    $from_date = date('Y-m-d', strtotime($date_format_change));

    $to_date_format_change = str_replace("/","-",$to_date);
    $to_date = date('Y-m-d', strtotime($to_date_format_change. ' + 1 day'));

    $where = " log_date BETWEEN '$from_date' and '$to_date'";

    $query = $this->db->select('*')
                              ->from('student_attendance_log')
                              ->where('user_id',$biometric_id)
                              ->where($where)
                              ->order_by('log_date','ASC');

    $query = $this->db->get();
    // echo $this->db->last_query();
    // exit();
    return $query->result_array();
  }

}
