
<style>
.rotatecell{
  -webkit-transform: rotate(+90deg);
  -moz-transform: rotate(+90deg);
}
</style>
<div class="gapping"></div>
<div class="create_batch_form">
  <div id="box">
  <h2>Monthly Attendance Report</h2>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
        <!-- start message area -->

        <?php if($this->session->flashdata('1')){ ?>
        <div class="alert alert-danger">
        <?php echo "Please select From Date and To Date"; ?>
        </div>
        <?php } ?>

        <!-- End message area -->

        </div>
        <div class="panel-body">
          <?php echo form_open();?>
          <div class="col-md-12">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-4">
                  <label>First Name: <span class="text-danger">*</span></label>
                  <?php if(IsEmployeeAttendancePermittedByRoleIds){?>
                    <input type='textbox' class='form-control' name='first_name' value="<?php if(isset($student_attendance_search['first_name'])){ echo $student_attendance_search['first_name']; }?>">
                  <?php }else{?>
                    <input type='hidden' class='form-control' name='first_name' value="<?php echo $this->session->userdata('admin_data')[0]['EMP_FNAME'];?>"> <?php echo $this->session->userdata('admin_data')[0]['EMP_FNAME'];?>
                  <?php }?>
                </div>
                <div class="col-lg-4">
                  <label>Last Name: <span class="text-danger">*</span></label>
                  <?php if(IsEmployeeAttendancePermittedByRoleIds){?>
                    <input type='textbox' class='form-control' name='last_name' value="<?php if(isset($student_attendance_search['last_name'])){ echo $student_attendance_search['last_name']; }?>">
                  <?php }else{?>
                    <input type='hidden' class='form-control' name='last_name' value="<?php echo $this->session->userdata('admin_data')[0]['EMP_LASTNAME'];?>"> <?php echo $this->session->userdata('admin_data')[0]['EMP_LASTNAME'];?>
                  <?php }?>
                </div>
                <div class="col-lg-4">
                  <label>Is Active: <span class="text-danger">*</span></label>
                  <?php if(IsEmployeeAttendancePermittedByRoleIds){?>
                  <select name="active_status" class="form-control">
                    <option value="1" <?php if(isset($student_attendance_search['active_status'])){ if($student_attendance_search['active_status'] == "1"){ echo "selected='selected'";}}?>>Active</option>
                    <option value="0" <?php if(isset($student_attendance_search['active_status'])){ if($student_attendance_search['active_status'] == "0"){ echo "selected='selected'";}}?>>Is Active</option>
                  </select>
                  <?php }else{
                        if($this->session->userdata('admin_data')[0]['ISACTIVE'] == "1"){?>
                          <input type='hidden' class='form-control' name='active_status' value="1"> Active
                      <?php }if($this->session->userdata('admin_data')[0]['ISACTIVE'] == "0"){?>
                          <input type='hidden' class='form-control' name='active_status' value="0"> In Active
                    <?php }?>
                  <?php }?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-4">
                  <label>From Date: <span class="text-danger">*</span></label>
                  <input type='textbox' class='form-control enquiry_date' name='from_date' value="<?php if(isset($student_attendance_search['from_date'])){ echo $student_attendance_search['from_date']; }?>" required>
                </div>
                <div class="col-lg-4">
                  <label>To Date: <span class="text-danger">*</span></label>
                  <input type='textbox' class='form-control enquiry_date' name='to_date' value="<?php if(isset($student_attendance_search['to_date'])){ echo $student_attendance_search['to_date']; }?>" required>
                </div>
                <div class="col-lg-4">
                  <label>Centre Name: <span class="text-danger">*</span></label>
                  <?php if(IsEmployeeAttendancePermittedByRoleIds){?>
                    <select name="centre_name" class="form-control" required>
                      <option value="" seleceted="seleceted">Select Centre Name</option>
                      <?php
                        foreach($centres as $centre){
                          $selected = "";
                          if(isset($student_attendance_search['centre_name'])){
                            if($student_attendance_search['centre_name'] == $centre['CENTRE_ID']){
                              $selected = "selected='selected'";
                            }
                          }
                          echo "<option value='".$centre['CENTRE_ID']."' ".$selected.">".$centre['CENTRE_NAME']."</option>";
                        }
                      ?>
                    </select>
                  <?php }else{
                      foreach($centres as $centre){
                          if($this->session->userdata('admin_data')[0]['CENTRE_ID'] == $centre['CENTRE_ID']){
                            echo "<input type='hidden' class='form-control' name='centre_name' value='".$centre['CENTRE_ID']."'> ".$centre['CENTRE_NAME'];
                          }

                      }
                    }?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <div class="row">
                <div class="col-lg-4">
                </div>
                <div class="col-lg-4">
                  <button type="submit" class="btn btn-primary" style="width:143px;" name="generate_attendance">Generate Report</button> &nbsp;
                  <button type="submit" class="btn btn-primary" name="reset_attendance">Reset</button> &nbsp;
                </div>
                <div class="col-lg-4">
                  <?php if (!empty($this->session->userdata('attendance_data'))) { ?>
                  <a onClick ="$('#export_to_excel_attendance_details').tableExport({type:'excel',escape:'false'});" class="btn" style="float:left" title="Export to excel"><i class="fa fa-file-excel-o fa-2x text-success"></i></a>

                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
          <?php echo form_close();?>
        </div>
      </div>
    </div>
  </div>
</div>
<br/>

  <?php if(!empty($student_attendance_search)){?>
    <div class="panel panel-default">
      <div class="panel-body table-responsive">
        <table class="table table-bordered" id="export_to_excel_attendance_details">
          <?php if(!empty($student_details_with_attendance)){
          for($i=0;$i<count($student_details_with_attendance);$i++){ ?>
          <thead class="thead-light">
            <tr>
              <th>Admission Id / Biometric Id</th>
              <th>Name</th>
              <th>P</th>
              <th>A</th>
              <th>WO</th>
              <th>Days</th>
              <th>Direction</th>
              <?php
                for($j=0;$j<count($student_details_with_attendance[0]['Attendance_Data']);$j++){
                  $cell_title = "";
                  if($student_details_with_attendance[0]['Attendance_Data'][$j]['Color'] == 'rgba(255, 255, 0, 0.4)'){
                    $cell_title = "title='Sunday'";
                  }
                  else if($student_details_with_attendance[0]['Attendance_Data'][$j]['Color'] == 'rgba(220, 220, 220, 0.9)'){
                    $cell_title = "title='Bank Holiday'";
                  }
                  else if($student_details_with_attendance[0]['Attendance_Data'][$j]['Color'] == 'rgba(0, 125, 0, 0.4)'){
                    $cell_title = "title='Worked on Holiday'";
                  }
                 echo "<th style='background-color:".$student_details_with_attendance[0]['Attendance_Data'][$j]['Color']."' ".$cell_title.">".$student_details_with_attendance[0]['Attendance_Data'][$j]['Date_no']."</th>";
                }?>
            </tr>
          </thead>
          <tbody>
              <tr>
                <th><?php echo $student_details_with_attendance[$i]['Admission_Id']." / ".$student_details_with_attendance[$i]['BIOMETRIC_ID']?></th>
                <th><?php echo $student_details_with_attendance[$i]['Student_Name']?></th>
                <th></th>
                <td></td>
                <td></td>
                <td></td>
                <td>IN</td>
                <?php for($j=0;$j<count($student_details_with_attendance[$i]['Attendance_Data']);$j++){
                    if($student_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'A' || $student_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'NAL' || $student_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'AL' || $student_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'O'){
                      echo "<td style='background-color:".$student_details_with_attendance[$i]['Attendance_Data'][$j]['Color'].";font-weight:bold;color:".$student_details_with_attendance[$i]['Attendance_Data'][$j]['Textcolor']."'>".$student_details_with_attendance[$i]['Attendance_Data'][$j]['In']."</td>";
                    }
                    else{
                      echo "<td style='writing-mode: tb-rl;background-color:".$student_details_with_attendance[$i]['Attendance_Data'][$j]['Color']."'>".$student_details_with_attendance[$i]['Attendance_Data'][$j]['In']."</td>";
                    }
                 } ?>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>OUT</td>
                <?php for($j=0;$j<count($student_details_with_attendance[$i]['Attendance_Data']);$j++){
                    if($student_details_with_attendance[$i]['Attendance_Data'][$j]['Out'] == 'A' || $student_details_with_attendance[$i]['Attendance_Data'][$j]['Out'] == 'NAL' || $student_details_with_attendance[$i]['Attendance_Data'][$j]['Out'] == 'AL' || $student_details_with_attendance[$i]['Attendance_Data'][$j]['Out'] == 'O'){
                      echo "<td  style='background-color:".$student_details_with_attendance[$i]['Attendance_Data'][$j]['Color'].";font-weight:bold;color:".$student_details_with_attendance[$i]['Attendance_Data'][$j]['Textcolor']."'>".$student_details_with_attendance[$i]['Attendance_Data'][$j]['Out']."</td>";
                    }
                    else{
                      echo "<td class='' style='writing-mode: tb-rl;background-color:".$student_details_with_attendance[$i]['Attendance_Data'][$j]['Color']."'>".$student_details_with_attendance[$i]['Attendance_Data'][$j]['Out']."</td>";
                    }
                 } ?>
              </tr>
              <tr>
                <th>Process</th>
                <th></th>
                <?php
                  $days = count($student_details_with_attendance[$i]['Attendance_Data']);
                  $paid_off_count = "";
                  $off_count = 0;
                  $off_punch_count = 0;
                  $absent_count = 0;
                  $approved_leaves_count = 0;
                  $not_approved_leaves_count = 0;
                  $late_count = 0;
                  $present_count = 0;
                  $present_count_wo_off = 0;
                  $actual_off_count = 0;
                  $half_day_count = 0;

                  for($j=0;$j<count($student_details_with_attendance[$i]['Attendance_Data']);$j++){
                    if(($student_details_with_attendance[$i]['Attendance_Data'][$j]['Color'] == 'rgba(255, 255, 0, 0.4)') || ($student_details_with_attendance[$i]['Attendance_Data'][$j]['Color'] == 'rgba(220, 220, 220, 0.9)')){
                      $off_count+= 1;
                      if($student_details_with_attendance[$i]['Attendance_Data'][$j]['In'] != 'O'){
                        $off_punch_count+= 1;
                      }
                    }

                    if(($student_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'A') || ($student_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'AL') || ($student_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'NAL')){
                      $absent_count+= 1;
                      if($student_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'AL'){
                        $approved_leaves_count+= 1;
                      }
                      elseif ($student_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'NAL') {
                        $not_approved_leaves_count+= 1;
                      }
                    }

                    if($student_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs_Color'] == 'red'){
                      $half_day_count+= 1;
                    }

                    if($student_details_with_attendance[$i]['Attendance_Data'][$j]['In'] == 'O'){
                      $actual_off_count+= 1;
                    }

                    $numpart = explode(".", $student_details_with_attendance[$i]['Attendance_Data'][$j]['Time_diff']);
                    if(count($numpart) > 1){
                      if(($numpart[1] > 5 )){
                        $late_count+= 1;
                      }
                      elseif(($numpart[0] > 0 )){
                        $late_count+= 1;
                      }
                    }
                  }
                  $paid_off_count = $off_count - $off_punch_count;
                  $present_count = $days - $actual_off_count - $absent_count;
                  $present_count_wo_off = $days - $off_count;
                ?>
                <th><?php echo $present_count;?></th>
                <th><?php echo $absent_count;?></th>
                <th><?php echo $paid_off_count;?></th>
                <th><?php echo $days;?></th>
                <td>Hrs</td>
                <?php
                $worked_hours_normal = 0;
                $off_days_working_hours = 0;
                for($j=0;$j<count($student_details_with_attendance[$i]['Attendance_Data']);$j++){
                  if(!($student_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs'] == 'A') || !($student_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs'] == 'AL') || !($student_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs'] == 'NAL') || !($student_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs'] == 'O')){
                    $worked_hours_normal += $student_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs'];
                  }
                  if(($student_details_with_attendance[$i]['Attendance_Data'][$j]['Color'] == 'rgba(220, 220, 220, 0.9)') || ($student_details_with_attendance[$i]['Attendance_Data'][$j]['Color'] == 'rgba(255, 255, 0, 0.4)')){
                    $off_days_working_hours += $student_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs'];
                  }
                    echo "<td style='background-color:".$student_details_with_attendance[$i]['Attendance_Data'][$j]['Color'].";color:".$student_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs_Color']."'>".$student_details_with_attendance[$i]['Attendance_Data'][$j]['Hrs']."</td>";
                 }
                  $actual_worked_hours = $worked_hours_normal - $off_days_working_hours ?>
              </tr>
              <th colspan="100"></th>
          </tbody>
        <?php }
        }?>
        </table>
      </div>
    </div>
  <?php }?>
