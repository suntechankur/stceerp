<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Upload student Attendance</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<!-- start message area -->

						<?php if($this->session->flashdata('success')) { ?>
						<div class="alert alert-success">
						<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
						</div>
						<?php } ?>

						<!-- End message area -->
						<div class="col-md-12">
							<div class="form-group">
								<?php echo form_open_multipart('upload_student_attendance') ?>
								<div class="row" style="margin-left:-30px;">
									<div class="col-lg-12" style="margin-left:0px;">
										<br/>
										<div class="col-md-2">
											<label>Excel File : </label>
										</div>
										<div class="col-md-10">
											<input type="file" name="attendancefile" placeholder="Search File">
										</div>
									</div>
									<div class="col-lg-12" style="margin-left:0px;">
										<br/>
										<div class="col-md-6">
											<button type="submit" class="btn btn-primary">Export Data</button>
										</div>
										<?php if($this->session->flashdata('flashMsg')){
												 if($this->session->flashdata('flashMsg') == "2"){
												 	echo "<label id='flashmsg' style='color:red'>* Please fill all the details.</label>";
												 }
												 else if($this->session->flashdata('flashMsg') == "1"){
												 	echo "<label id='flashmsg' style='color:red'>* End date should greater than start date.</label>";
												 }
												 else{}?>
												 <script>
												 	setTimeout(function(){ $("#flashmsg").hide(); }, 3000);
												 </script>
										<?php } ?>
									</div>
								</div>
								<?php echo form_close();?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($this->session->flashdata('exportData')) { ?>
	<div id="box">
		<p>&nbsp;</p>
		<h2></h2>
    </div>
    <div class="panel panel-default">
    	<div class="panel-body table-responsive">
    		<table class="table table-striped table-hover">
				<thead>
					<tr>
						<?php
				        foreach($tableheading as $key => $value){?>
				        	<th><?php echo $tableheading[$key];?></th>
				        <?php }	?>
					</tr>
				</thead>
				<tbody>
				<?php
			        foreach($tabledata as $key => $value){
			        	echo "<tr>";
			        	foreach ($tabledata[$key] as $tableKey => $tableValue) { ?>

			        		<td><?php echo $tabledata[$key][$tableKey];?></td>
			        <?php	}
			        echo "</tr>";
			        }	?>
				</tbody>
				</table>
				<label><i>* <?php echo $affectrows;?> rows affected.</i></label>
    		</table>
    	</div>
    </div>
    <?php } ?>
</div>
