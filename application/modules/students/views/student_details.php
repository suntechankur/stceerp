<div class="col-md-12 col-sm-12">
	<div class="create_batch_form">
		<div id="box">
			<h2>Direct links</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-body">
							<div class="col-md-12">
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-md-3">
												<a href="#studentDetails"><button type="submit" class="btn btn-primary" style="width:100%;">Student Details</button></a>
											</div>
											<div class="col-md-3">
												<a href="#subjectDetails"><button type="submit" class="btn btn-primary" style="width:100%;">Subject Details</button></a>
											</div>
											<div class="col-md-3">
												<a href="#installmentPlan"><button type="submit" class="btn btn-primary" style="width:100%;">Installment Plan</button></a>
											</div>
											<div class="col-md-3">
												<a href="#receiptDetails"><button type="submit" class="btn btn-primary" style="width:100%;">Receipts Details</button></a>
											</div>
										</div>
										<br/><br/>
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-md-3">
												<a href="#otherReceiptDetails"><button type="submit" class="btn btn-primary" style="width:100%;">Other Receipts Details</button></a>
											</div>
											<div class="col-md-3">
												<a href="#courseStatus"><button type="submit" class="btn btn-primary" style="width:100%;">Course Status</button></a>
											</div>
											<div class="col-md-3">
												<a href="#pendingSubject"><button type="submit" class="btn btn-primary" style="width:100%;">Pending Subjects</button></a>
											</div>
											<div class="col-md-3">
												<a href="#examStatus"><button type="submit" class="btn btn-primary" style="width:100%;">Exam Status</button></a>
											</div>
										</div>
										<br/><br/>
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-md-3">
												<a href="#placementHistory"><button type="submit" class="btn btn-primary" style="width:100%;">Placement History</button></a>
											</div>
											<div class="col-md-3">
												<a href="#otherDetails"><button type="submit" class="btn btn-primary" style="width:100%;">Other Details</button></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br/>
	<div class="create_batch_form" id="studentDetails">
		<div id="box">
			<h2>Student Details</h2>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<?php
                    /* This Form is for convert Enquiry */
                    foreach($studData as $sdata){
                    ?>
					<div class="panel-body">
						<?php echo form_open(); ?>
							<div class="col-md-12">

								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Addmission Id: <?php echo $sdata['ADMISSION_ID']?></label>
											</div>
											<div class="col-lg-4">
												<label>Full Name: <?php echo $sdata['ENQUIRY_FIRSTNAME']." ".$sdata['ENQUIRY_MIDDLENAME']." ".$sdata['ENQUIRY_LASTNAME']?></label>
											</div>
											<div class="col-lg-4">
												<label>Centre Name: <?php echo $sdata['CENTRE_NAME']?></label>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Admission Date: <?php $date=date_create($sdata['ADMISSION_DATE']); echo date_format($date,"d/m/Y");?></label>
											</div>
											<div class="col-lg-4">
												<?php
                          $courses_arr = explode(",",$sdata['COURSE_TAKEN']);
                               $courseNames = '';
                               //echo '<ul class="list-group">';
                                for($j=0;$j<count($courses);$j++){
                                    if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
                                          $courseNames .= ','.$courses[$j]['COURSE_NAME'];
                                        $sdata['COURSE_TAKEN'] = trim($courseNames,",");
                                      }

                                }
                               //echo '</ul>';
                          ?>
                          <?php
                          $modules_arr = explode(",",$sdata['MODULE_TAKEN']);
                               $moduleNames = '';
                               //echo '<ul class="list-group">';
                                for($j=0;$j<count($modules);$j++){
                                    if(in_array($modules[$j]['MODULE_ID'],$modules_arr)){
                                          $moduleNames .= ','.$modules[$j]['MODULE_NAME'];
                                        $sdata['MODULE_TAKEN'] = trim($moduleNames,",");
                                      }

                                }
                               //echo '</ul>';
															 $sdata['COURSE_TAKEN'] = trim($sdata['MODULE_TAKEN'].",".$sdata['COURSE_TAKEN'],",");
															 unset($sdata['MODULE_TAKEN']);
                          // echo "<pre>";
                          // print_r($modules);
                          // echo "</pre>";
                          ?>
												<label>Course Taken : <?php echo $sdata['COURSE_TAKEN']?></label>
											</div>
											<div class="col-lg-4">
												<label>Admission Type : <?php echo $sdata['ADMISSION_TYPE']?></label>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-lg-4">
												<label>Fees Charged : <?php echo $sdata['TOTALFEES']?></label>
											</div>
											<div class="col-lg-4">
                          <label>Fees Paid : <?php echo $sdata['FEE_PAID']?></label>
                      </div>
                      <div class="col-lg-4">
                      	<label for="">Balance Fees : <?php echo $sdata['TOTALFEES']-$sdata['FEE_PAID']?></label>
                      </div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-md-4">
                                            <label>Address : <?php echo $sdata['ENQUIRY_ADDRESS1'].$sdata['ENQUIRY_ADDRESS2']?></label>
                                            <label>City : <?php echo $sdata['ENQUIRY_CITY']?></label><br/>
                                            <label>Zip : <?php echo $sdata['ENQUIRY_ZIP']?></label><br/>
                                            <label>State : <?php echo $sdata['ENQUIRY_STATE']?></label><br/>
                                            <label>Country : <?php echo $sdata['ENQUIRY_COUNTRY']?></label>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Contact Details: </label><br/><br/>
																						<?php if(!(ConvertToEnquiryConversionAccessByRoleId)){?>
																							<label>Mobile : <?php echo $sdata['ENQUIRY_MOBILE_NO']?></label><br/>
																							<label>Parent Mobile : <?php echo $sdata['ENQUIRY_PARENT_NO']?></label><br/>
																							<label>Email : <?php echo $sdata['ENQUIRY_EMAIL']?></label>
																						<?php }else{?>
																						<input class="form-control" type="hidden" value="<?php echo $sdata['ENQUIRY_ID']?>" name="enquiry_id">
                                            <label>Mobile :</label><input class="form-control" type="text" pattern="[1-9]{1}[0-9]{9}" maxlength="10" value="<?php echo $sdata['ENQUIRY_MOBILE_NO']?>" name="ENQUIRY_MOBILE_NO"><br/>
                                            <label>Parent Mobile :</label><input class="form-control" type="text" pattern="[1-9]{1}[0-9]{9}" maxlength="10" value="<?php echo $sdata['ENQUIRY_PARENT_NO']?>" name="ENQUIRY_PARENT_NO"><br/>
                                            <label>Email :</label><input class="form-control" type="email" value="<?php echo $sdata['ENQUIRY_EMAIL']?>" name="ENQUIRY_EMAIL">
																					<?php }?>
																				</div>
                                        <div class="col-md-4">
											<label>Date Of Birth : <?php $date=date_create($sdata['ENQUIRY_DATEOFBIRTH']); echo date_format($date,"d/m/Y");?></label>
											<label>Admission Handled By : <?php echo $sdata['EMP_FNAME']?></label>
											<label>BLPC Update Date :</label>
											<input type="text" class="form-control enquiry_date" value="<?php echo date("d/m/Y");?>">
											<label>BLPC Status :</label>
											<select class="form-control" name="status">
												<option value="">Select Status</option>
												<option value="persuing" <?php if($sdata['STATUS'] == 'persuing'){ echo "selected='selected'";}?>>Persuing</option>
												<option value="completed" <?php if($sdata['STATUS'] == 'completed'){ echo "selected='selected'";}?>>Completed</option>
												<option value="break" <?php if($sdata['STATUS'] == 'break'){ echo "selected='selected'";}?>>Break</option>
												<option value="terminated" <?php if($sdata['STATUS'] == 'terminated'){ echo "selected='selected'";}?>>Terminated</option>
												<option value="hold" <?php if($sdata['STATUS'] == 'hold'){ echo "selected='selected'";}?>>Hold</option>
												<option value="left" <?php if($sdata['STATUS'] == 'left'){ echo "selected='selected'";}?>>Left</option>
											</select>
										</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-md-6">
												<label>Remarks :</label>
												<input type="text" class="form-control" name="remarks" value="<?php echo $sdata['REMARKS'];?>">
											</div>
											<div class="col-md-6">
												<label>Student Incentive Remarks :</label>
												<input type="text" class="form-control">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-md-4">
												<label>Intake : <?php echo $sdata['IN_TAKE']?></label>
											</div>
											<div class="col-md-4">
												<label>Academic Year : <?php echo $sdata['YEAR']?></label>
											</div>
											<div class="col-md-4">
												<label>Admission Type[MODULAR] : <?php echo $sdata['ADMISSION_TYPE']?></label>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-md-4">
											</div>
											<div class="col-md-4">
												<button type="submit" name="update_student_data" class="btn btn-primary" style="width:100%;">Update</button>
											</div>
											<div class="col-md-4">
											</div>
										</div>
									</div>
								</div>
								 <?php echo form_close(); ?>
								  <?php echo form_open(); ?>
								<div class="form-group">
									<div class="row" style="margin-left:-30px;">
										<div class="col-lg-12" style="margin-left:0px;">
											<div class="col-md-6">
												<label>Study Center :</label><br/>
												<div class="col-md-7">
												<select>
												<?php
							                    /* This Form is for convert Enquiry */
							                    foreach($centerDetails as $cdetails){
							                    	if($cdetails['ISACTIVE'] == "1"){
							                    		?>
							                    			<option value="<?php echo $cdetails['CENTRE_NAME']?>" <?php echo ($cdetails['CENTRE_ID'] == $sdata['CENTRE_ID']) ? ' selected="selected"' : '';?>><?php echo $cdetails['CENTRE_NAME']?></option>
							                    		<?php
							                    	}
							                    	else{
							                    		?>
							                    			<option value=<?php echo $cdetails['CENTRE_NAME']?> disabled style="color:grey;background:#ccc;"><?php echo $cdetails['CENTRE_NAME']?></option>
							                    		<?php
							                    	}
							                    }?>
												</select>
												</div>
												<div class="col-md-5">
													<button type="submit" class="btn btn-primary" style="width:100%;">Update Student Center</button>
												</div>

											</div>
											<div class="col-md-6">
												<label>University Name :</label><br/>
												<div class="col-md-7">
												<select>
												<?php
							                    /* This Form is for convert Enquiry */
							                    foreach($universityDetails as $udetails){ ?>
				                    					<option value="<?php echo $udetails['UNIVERSITY_NAME']?>" <?php echo ($udetails['UNIVERSITY_NAME'] == $sdata['UNIVERSITY_NAME']) ? ' selected="selected"' : '';?>><?php echo $udetails['UNIVERSITY_NAME']?></option>
							                    <?php   }?>
												</select>
												</div>
												<div class="col-md-5">
													<button type="submit" class="btn btn-primary" style="width:100%;">Update University Name</button>
												</div>
											</div>
											<div class="col-md-6">
												<label>University Registration No :</label><br/>
												<div class="col-md-6">
													<input type="text" class="form-control" value="<?php echo $sdata['UNIVERSITY_REGISTRATION_NO']?>">
												</div>
												<div class="col-md-6">
													<button type="submit" class="btn btn-primary" style="width:100%;">Update University Reg. No</button>
												</div>
											</div>
											<div class="col-md-6">
												<label>University Registration No(2nd) :</label><br/>
												<div class="col-md-6">
													<input type="text" class="form-control" value="<?php echo $sdata['UNIVERSITY_REGISTRATION_NO_2']?>">
												</div>
												<div class="col-md-6">
													<button type="submit" class="btn btn-primary" style="width:100%;">Update University Reg. No 2</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						 <?php echo form_close(); ?>
						</div>
						<?php }?>
					</div>
				</div>
			</div>
		</div>
		<div id="subjectDetails">
			<div id="box">
				<p>&nbsp;</p>
	        	<h2 class="text-center">Subject Details</h2>
	        </div>
	        <div class="panel panel-default">
	        	<div class="panel-body table-responsive">
	        		<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Module Name</th>
								<th>Course Name</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($courseDetails as $courseData){?>
							<tr><td><?php echo $courseData['MODULE_NAME']?></td><td><?php echo $courseData['COURSE_NAME']?></td></tr>
						<?php } ?>
						</tbody>
	        		</table>
	        	</div>
	        </div>
        </div>
        <div id="installmentPlan">
			<div id="box">
				<p>&nbsp;</p>
	        	<h2 class="text-center">Installment Plan</h2>
	        </div>
	        <div class="panel panel-default">
	        	<div class="panel-body table-responsive">
	        		<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Sr No</th>
								<th>ID</th>
								<th>DP/Installment Date</th>
								<th>DP/Installment Amount</th>
							</tr>
						</thead>
						<tbody>
						<?php $i=1;
						// echo "<pre>";
						// print_r($installmentPlan);
						// echo "</pre>";
						foreach($installmentPlan as $installPlan){

							$duedate = explode(",",$installPlan['DUEDATE']);
							$dueamount = explode(",",$installPlan['DUE_AMOUNT']);

							for($j=0;$j<count($dueamount);$j++){?>
								<tr>
									<td><?php echo $i++;?></td>
									<td><?php echo $installPlan['ADMISSION_INSTALLEMENT_ID'];?></td>

									<?php if(isset($duedate[$j])){
										$date=date_format(date_create($duedate[$j]),"d/m/Y");
										echo "<td>$date</td>";
									}
									else{
									echo "<td></td>";
									}

									if(isset($dueamount[$j])){
										echo "<td>$dueamount[$j]</td>";
									}
									else{
										echo "<td></td>";
									}
									?>
								</tr>
							<?php }
							} ?>
						</tbody>
	        		</table>
	        	</div>
	        </div>
        </div>
        <div id="receiptDetails">
			<div id="box">
				<p>&nbsp;</p>
	        	<h2 class="text-center">Receipts Details</h2>
	        </div>
	        <div class="panel panel-default">
	        	<div class="panel-body table-responsive">
	        		<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Receipt No</th>
								<th>Token No</th>
								<th>Date</th>
								<th>Amount Paid WST</th>
								<th>Amount Paid W/o ST</th>
								<th>Payment Type</th>
								<th>Balance</th>
								<th>Receipt</th>
								<th>Summary</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($receiptDetails as $receiptData){?>
							<tr>
								<td><?php echo $receiptData['ADMISSION_RECEIPT_NO']?></td>
								<td></td>
								<td><?php $date=date_create($receiptData['PAYMENT_DATE']); echo date_format($date,"d/m/Y")?></td>
								<td><?php echo $receiptData['AMOUNT_PAID']?></td>
								<td><?php echo $receiptData['AMOUNT_PAID']-$receiptData['AMOUNT_PAID_SERVICETAX']?></td>
								<td>
									<?php if($receiptData['PAYMENT_TYPE'] == "CASH"){
											echo $receiptData['PAYMENT_TYPE'];
										}
										else{
											$date=date_create($receiptData['CHEQUE_DATE']);

											echo $receiptData['PAYMENT_TYPE']."(".$receiptData['CHEQUE_NO'].", ".date_format($date,"d/m/Y").", ".$receiptData['CHEQUE_BANK_NAME'].")";
										}
									?>
								</td>
								<td><?php echo $receiptData['BALANCES']?></td>
								<td><a href="">Print</a></td>
								<td><a href="">Print</a></td>
							</tr>
						<?php } ?>
						</tbody>
	        		</table>
	        	</div>
	        </div>
        </div>
        <div id="otherReceiptDetails">
			<div id="box">
				<p>&nbsp;</p>
	        	<h2 class="text-center">Other Receipts Details</h2>
	        </div>
	        <div class="panel panel-default">
	        	<div class="panel-body table-responsive">
	        		<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Receipt No</th>
								<th>Date</th>
								<th>Particular</th>
								<th>Amount Paid WST</th>
								<th>Amount Paid W/o ST</th>
								<th>Payment Type</th>
								<th>Print</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($otherReceipts as $otherReceipt){?>
							<tr>
								<td><?php echo $otherReceipt['PENALTY_RECEIPT_NO']?></td>
								<td><?php $date=date_create($otherReceipt['PAYMENT_DATE']); echo date_format($date,"d/m/Y")?></td>
								<td><?php echo $otherReceipt['PARTICULAR']?></td>
								<td><?php echo $otherReceipt['AMOUNT']?></td>
								<td><?php echo $otherReceipt['AMOUNT_WITHOUT_ST']?></td>
								<td><?php echo $otherReceipt['PAYMENT_TYPE'];?></td>
								<td><a href="">Print</a></td>
							</tr>
						<?php } ?>
						</tbody>
	        		</table>
	        	</div>
	        </div>
        </div>
        <div id="courseStatus">
			<div id="box">
				<p>&nbsp;</p>
	        	<h2 class="text-center">Course Status</h2>
	        </div>
	        <div class="panel panel-default">
	        	<div class="panel-body table-responsive">
	        		<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Batch Id</th>
								<th>Subject</th>
								<th>Centre</th>
								<th>Faculty</th>
								<th>Timing</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Attendance</th>
								<th>Exam Date</th>
								<th>Assignments/Journals Marks</th>
								<th>Online Exam Marks</th>
								<th>Total Marks</th>
								<th>%</th>
								<th>Result</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($courseStatus as $cStatus){?>
							<tr>
								<td><?php echo $cStatus['BATCHID']?></td>
								<td><?php echo $cStatus['COURSE_NAME']?></td>
								<td><?php echo $cStatus['CENTRE_NAME']?></td>
								<td>
								<?php if(($cStatus['FACULTY_NAME'] == "") || ($cStatus['FACULTY_NAME'] == null)){
										echo $cStatus['EMP_FNAME']." ".$cStatus['EMP_MIDDLENAME']." ".$cStatus['EMP_LASTNAME'];
									}
									else{
										echo $cStatus['FACULTY_NAME'];
									}?>
								</td>
								<td><?php echo $cStatus['DAYS'].", ".$cStatus['STARTTIME']." to ".$cStatus['ENDTIME']?></td>
								<td><?php $date=date_create($cStatus['STARTDATE']); echo date_format($date,"d/m/Y")?></td>
								<td><?php $date1=date_create($cStatus['ACTUALENDDATE']); echo date_format($date1,"d/m/Y")?></td>
								<td><?php echo $cStatus['PRESENT']."/ ".$cStatus['TOTALNOBATCH']?></td>
								<td><a href="">Print</a></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						<?php } ?>
						</tbody>
	        		</table>
	        	</div>
	        </div>
        </div>
        <div id="pendingSubject">
			<div id="box">
				<p>&nbsp;</p>
	        	<h2 class="text-center">Pending Subjects</h2>
	        </div>
	        <div class="panel panel-default">
	        	<div class="panel-body table-responsive">
	        		<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Module Name</th>
								<th>Course Name</th>
							</tr>
						</thead>
						<tbody>
						<?php
							for ($m=0; $m <count($pendingSubject) ; $m++) {

								if(is_array($pendingSubject[$m])){

					               foreach ($pendingSubject[$m] as $key => $value) {
					               		echo "<tr><td>".$courseDetails[0]['MODULE_NAME']."</td><td>".$pendingSubject[$m][$key]."</td></tr>";
					               }
					            }
							 }
					 	?>
						</tbody>
	        		</table>
	        	</div>
	        </div>
        </div>
        <div id="examStatus">
			<div id="box">
				<p>&nbsp;</p>
	        	<h2 class="text-center">Exam Status</h2>
	        </div>
	        <div class="panel panel-default">
	        	<div class="panel-body table-responsive">
	        		<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Exam Id</th>
								<th>Exam Date</th>
								<th>Time</th>
								<th>Subject</th>
								<th>Internal Exam / Project Marks</th>
								<th>External Exam Marks</th>
								<th>Total Marks</th>
								<th>Percentage</th>
								<th>Result</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($examStatus as $eStatus){?>
							<tr>
								<td><?php echo $eStatus['EXAM_ID']?></td>
								<td><?php $date=date_create($eStatus['EXAM_DATE']); echo date_format($date,"d/m/Y")?></td>
								<td><?php echo $eStatus['EXAM_TIME']?></td>
								<td><?php echo $eStatus['COURSE_NAME']?></td>
								<td>
								<?php
									if($eStatus['INTERNAL_MARKS'] && $eStatus['INTERNAL_OUT_OF']){
										echo $eStatus['INTERNAL_MARKS']." / ".$eStatus['INTERNAL_OUT_OF'];
									}
									else{
										echo "-";
									}
								?>
								</td>
								<td>
									<?php
										if($eStatus['EXTERNAL_MARKS'] && $eStatus['EXTERNAL_OUT_OF']){
											echo $eStatus['EXTERNAL_MARKS']." / ".$eStatus['EXTERNAL_OUT_OF'];
										}
										else{
											echo "-";
										}
									?>
								</td>
								<td></td>
								<td>
								<?php if($eStatus['PERCENTAGE']){
										echo $eStatus['PERCENTAGE']."%";
									}
								?>
								</td>
								<td><?php echo $eStatus['RESULT']?></td>
							</tr>
						<?php } ?>
						</tbody>
	        		</table>
	        	</div>
	        </div>
        </div>
        <div id="placementHistory">
			<div id="box">
				<p>&nbsp;</p>
	        	<h2 class="text-center">Placement History</h2>
	        </div>
	        <div class="panel panel-default">
	        	<div class="panel-body table-responsive">
	        		<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>Sr No.</th>
										<th>Event Date</th>
										<th>Department</th>
										<th>Remarks_By</th>
										<th>Status</th>
										<th>Remarks</th>
										<th>Remarks Date</th>
									</tr>
								</thead>
								<tbody>
									<?php $i = 1; foreach($allPlacementDetails as $placement){?>
									<tr>
										<td><?php echo $i++;?></td>
										<td><?php
										$r_Date = str_replace('-', '/',$placement['REMARKS_DATE']);
										$remarkDate = date('d/m/Y',strtotime($r_Date));
										echo $remarkDate;?></td>
										<td><?php echo $placement['REMARKS_SOURCE'];?></td>
										<td><?php echo $placement['REMARKS_BY'];?></td>
										<td><?php echo $placement['STATUS'];?></td>
										<td><?php echo $placement['REMARKS'];?></td>
										<td><?php echo $remarkDate;?></td>
									</tr>
								<?php }?>
								</tbody>
	        		</table>
	        	</div>
	        </div>
        </div>
        <div id="otherDetails">
			<div id="box">
			<p>&nbsp;</p>
			<h2></h2>
	        </div>
	        <div class="row">
				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-body">
								<div class="col-md-12">
									<div class="form-group">
										<div class="row" style="margin-left:-30px;">
											<div class="col-lg-12" style="margin-left:0px;">
													<label>Certificate Issued Details :</label>
													<table class="table table-striped table-hover">
														<thead>
															<tr>
																<th>Course</th>
																<th>Issue Date</th>
																<th>Issue By</th>
															</tr>
														</thead>
														<tbody>
															<?php foreach($allSavedCertificateDetails as $certificate_data){?>
																<tr>
																	<td>
																		<?php foreach($allCourseDetails as $course){
																						if($course['COURSE_ID'] == $certificate_data['MODULE_ID']){
																							echo $course['COURSE_NAME'];
																						}
																		}?>
																	</td>
																	<td><?php
																	$i_Date = str_replace('-', '/',$certificate_data['ISSUE_DATE']);
																	$issueDate = date('d/m/Y',strtotime($i_Date));
																	echo $issueDate;?></td>
																	<td><?php echo $certificate_data['Employee_Name'];?></td>
																</tr>
															<?php } ?>
														</tbody>
													</table>
											</div>
										</div>
									</div>
      					</div>
								<div class="col-md-12">
									<div class="form-group">
										<div class="row" style="margin-left:-30px;">
											<div class="col-lg-12" style="margin-left:0px;">
													<label>Leave Application :</label>
													<table class="table table-striped table-hover">
														<thead>
															<tr>
																<th>Sr No.</th>
																<th>Application Date</th>
																<th>Leave From Date</th>
																<th>Leave To Date</th>
																<th>Leaving Reason</th>
																<th>Leaving Remark</th>
															</tr>
														</thead>
														<tbody>
															<?php $i = 1;
															foreach($leaveDetails as $leave){?>
																<tr>
																	<td><?php echo $i++;?></td>
																	<td><?php
																	$a_Date = str_replace('-', '/',$leave['APPLICATION_DATE']);
																	$applicationDate = date('d/m/Y',strtotime($a_Date));
																	echo $applicationDate;?></td>
																	<td><?php
																	$l_f_Date = str_replace('-', '/',$leave['LEAVE_FROM_DATE']);
																	$fromDate = date('d/m/Y',strtotime($l_f_Date));
																	echo $fromDate;?></td>
																	<td><?php
																	$l_t_Date = str_replace('-', '/',$leave['LEAVE_TO_DATE']);
																	$toDate = date('d/m/Y',strtotime($l_t_Date));
																	echo $toDate;?></td>
																	<td><?php echo $leave['LEAVE_REASON'];?></td>
																	<td><?php echo $leave['REMARKS'];?></td>
																</tr>
															<?php }?>
														</tbody>
							        		</table>
											</div>
										</div>
									</div>
	        			</div>
      			</div>
      		</div>
      	</div>
      </div>
    </div>
	</div>
</div>
