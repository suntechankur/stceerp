<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tele_enquiry_controller extends MX_Controller {

     public function __construct()
        {
                $this->load->model('tele_enquiry_model');
                $this->load->model('enquiry/enquiry_model');
                parent::__construct();
        }

    public function view_tele_enquiry($offset = '0'){
        if(!empty($_POST)){
            $this->session->set_userdata('filter_data',$_POST);
        }
        if(isset($_POST['reset'])){
            $this->session->unset_userdata('filter_data');
        }
        $tele_enq_list['filter_data'] = $this->session->userdata('filter_data');
        /* Pagination starts */
        $config['base_url'] = base_url('view-tele-enquiry/');
        $config['total_rows'] = $this->tele_enquiry_model->get_tele_enquiry_count($tele_enq_list['filter_data']);

        $config['per_page'] = '50';

        $tele_enq_list['tele_enquiries'] = $this->tele_enquiry_model->get_tele_enquiry_list($offset,$config['per_page'],$tele_enq_list['filter_data']);

        $tele_enq_list['total_rows'] = $this->tele_enquiry_model->get_tele_enquiry_list($offset,$config['per_page'],$tele_enq_list['filter_data'],"1");


        foreach($tele_enq_list['tele_enquiries'] as $key => $value){
            $get_followup_details = $this->tele_enquiry_model->get_followup_details($tele_enq_list['tele_enquiries'][$key]['TELE_ENQUIRY_ID']);

            unset($tele_enq_list['tele_enquiries'][$key]['EMP_NAME']);
            unset($tele_enq_list['tele_enquiries'][$key]['FOLLOWUP_DETAILS']);

            if(!empty($get_followup_details[0])){

                $tele_enq_list['tele_enquiries'][$key]['PREFERRED_TIME'] = $get_followup_details[0]['PREFERRED_TIME'];
                $tele_enq_list['tele_enquiries'][$key]['FOLLOWUP_DATE'] = $get_followup_details[0]['FOLLOWUP_DATE'];

                if($get_followup_details[0]['FOLLOWUP_DETAILS'] != ""){
                    $tele_enq_list['tele_enquiries'][$key]['FOLLOWUP_DETAILS'] = $get_followup_details[0]['FOLLOWUP_DETAILS'];
                }
                else{
                    $tele_enq_list['tele_enquiries'][$key]['FOLLOWUP_DETAILS'] = "To be followup";
                }

                if(is_numeric($get_followup_details[0]['FOLLOWUP_BY'])){
                    $follow_up =  $this->tele_enquiry_model->getEmployeeNameFromEmployeeId($get_followup_details[0]['FOLLOWUP_BY']);
                    $tele_enq_list['tele_enquiries'][$key]['EMP_NAME'] = $follow_up;
                }
                else{
                    $tele_enq_list['tele_enquiries'][$key]['EMP_NAME'] = $get_followup_details[0]['FOLLOWUP_BY'];
                }

                $tele_enq_list['tele_enquiries'][$key]['FOLLOWUP_DETAILS'] = $get_followup_details[0]['FOLLOWUP_DETAILS'];
                $tele_enq_list['tele_enquiries'][$key]['NEXT_FOLLOWUP_DATE'] = $get_followup_details[0]['NEXT_FOLLOWUP_DATE'];

                $tele_enq_list['tele_enquiries'][$key]['Status'] = $get_followup_details[0]['STATUS'];

            }
            else{
                $tele_enq_list['tele_enquiries'][$key]['PREFERRED_TIME'] = '';
                $tele_enq_list['tele_enquiries'][$key]['FOLLOWUP_DATE'] = '';

                $tele_enq_list['tele_enquiries'][$key]['FOLLOWUP_DETAILS'] = "To be followup";

                $tele_enq_list['tele_enquiries'][$key]['EMP_NAME'] = '';

                $tele_enq_list['tele_enquiries'][$key]['FOLLOWUP_DETAILS'] = '';
                $tele_enq_list['tele_enquiries'][$key]['NEXT_FOLLOWUP_DATE'] = '';

                $tele_enq_list['tele_enquiries'][$key]['Status'] = '';
            }

            if(is_numeric($tele_enq_list['tele_enquiries'][$key]['Status'])){
                if($tele_enq_list['tele_enquiries'][$key]['Status'] != ""){
                    $status =  $this->tele_enquiry_model->get_followup_status_details($tele_enq_list['tele_enquiries'][$key]['Status']);
                    unset($tele_enq_list['tele_enquiries'][$key]['Status']);
                    $tele_enq_list['tele_enquiries'][$key]['Recent_Status'] = $status;
                }
            }
            else{
                $tele_enq_list['tele_enquiries'][$key]['Recent_Status'] = $tele_enq_list['tele_enquiries'][$key]['Status'];
                unset($tele_enq_list['tele_enquiries'][$key]['Status']);
            }

        }

        $tele_enq_list['pagination'] = $this->pagination->create_links();
        $tele_enq_list['enq_handled_by'] = $this->tele_enquiry_model->enquiry_handled_by();
        $tele_enq_list['sources'] = $this->tele_enquiry_model->get_tele_sources();
        $tele_enq_list['centres'] = $this->tele_enquiry_model->suggested_centre();
        $tele_enq_list['courses'] = $this->tele_enquiry_model->course_interested();
        $this->load->admin_view('view_tele_enquiry',$tele_enq_list);

    }

	public function add_tele_enquiry()
	{
        $data['centres'] = $this->tele_enquiry_model->get_centres();
        $data['sources'] = $this->tele_enquiry_model->get_tele_sources();
        // $data['courses'] = $this->tele_enquiry_model->course_interested();
        $data['courses'] = $this->tele_enquiry_model->get_course_details_with_fees();

        $data['enq_handled_by'] = $this->tele_enquiry_model->enquiry_handled_by();
        $this->form_validation->set_rules('FIRSTNAME', 'First Name', 'required');
        $this->form_validation->set_rules('LASTNAME', 'Last Name', 'required');
        // $this->form_validation->set_rules('EMAIL_ID', 'Email', 'trim|required|valid_email|xss_clean');
        $this->form_validation->set_rules('QUALIFICATION', 'Qualification', 'required');
        // $this->form_validation->set_rules('MOBILE', 'Mobile', 'required|min_length[10]|max_length[10]|numeric');
        $this->form_validation->set_rules('MOBILE', 'Mobile', 'required|min_length[10]|max_length[10]|numeric|is_unique[tele_enquiries.MOBILE]',array('is_unique' => 'Mobile No. already exists'));
        $this->form_validation->set_rules('PARENT_NUMBER', 'Mobile', 'min_length[10]|max_length[10]|numeric');
        $this->form_validation->set_rules('ENQUIRY_DATE', 'Enquiry date', 'required');

        if(empty($_POST['SUGGESTED_CENTRE']) || $_POST['SUGGESTED_CENTRE'] == ''){
            $this->form_validation->set_rules('SUGGESTED_CENTRE', 'Suggested Centre', 'required');
        }

        if(empty($_POST['ENQUIRY_HANDELED_BY']) || $_POST['ENQUIRY_HANDELED_BY'] == ''){
            $this->form_validation->set_rules('ENQUIRY_HANDELED_BY', 'Enquiry Handled By', 'required');
        }

        if(empty($_POST['SOURCE']) || $_POST['SOURCE'] == ''){
            $this->form_validation->set_rules('SOURCE', 'Source', 'required');
        }

        // added by ankur on 03/11/2017

        if(empty($_POST['LOCATION']) || $_POST['LOCATION'] == ''){
            $this->form_validation->set_rules('LOCATION', 'LOCATION', 'required');
        }

        if(empty($_POST['COURSE_INTERESTED']) || $_POST['COURSE_INTERESTED'] == ''){
            $this->form_validation->set_rules('COURSE_INTERESTED', 'Course Interested', 'required');
        }


        if ($this->form_validation->run() != FALSE)
        {
            // for checking preffered filled or not
            if(($_POST['from_time'] != "") && ($_POST['to_time'] != "")){
                $PREF_TIME = $_POST['from_time'].' - '.$_POST['to_time'];
            }
            else{
                $PREF_TIME = "";
            }

            $date_rep = str_replace('/', '-', $_POST['ENQUIRY_DATE']);
            $enq_date =  date("Y-m-d H:i", strtotime($date_rep));
            $modified_date =  date("Y-m-d H:i", strtotime(date('Y-m-d')));

            $insert_enquiry = array(
                'FIRSTNAME' => $this->input->post('FIRSTNAME'),
                'MIDDLENAME' => $this->input->post('MIDDLENAME'),
                'LASTNAME'  => $this->input->post('LASTNAME'),
                'EMAIL_ID'  => $this->input->post('EMAIL_ID'),
                'QUALIFICATION'  => $this->input->post('QUALIFICATION'),
                'SUGGESTED_CENTRE_ID'  => $this->input->post('SUGGESTED_CENTRE'),
                'ENQUIRY_HANDELED_BY'  => $this->input->post('ENQUIRY_HANDELED_BY'),
                'SOURCE'  => $this->input->post('SOURCE'),
                'SUB_SOURCE'  => $this->input->post('SUBSOURCE'),
                'ENQUIRY_DATE'  => $enq_date,
                'MOBILE'  => $this->input->post('MOBILE'),
                'TELEPHONE'  => $this->input->post('MOBILE'),
                'PARENT_NUMBER'  => $this->input->post('PARENT_NUMBER'),
                'STREAM'  => $this->input->post('STREAM'),
                'REMARKS'  => $this->input->post('REMARKS'),
                'WALKIN' => '0',
                'LOCATION'  => $this->input->post('LOCATION'),
                'MODIFIED_DATE'  => $modified_date,
                'CREATED_ON'  => $modified_date,
                'CREATED_BY'  => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
                'MODIFIED_BY'  => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
                'PREFERRED_TIME' => $PREF_TIME
            );

            if(!empty($_POST['COURSE_INTERESTED'])){
                $insert_enquiry['COURSE_INTERESTED'] = implode(",",$_POST['COURSE_INTERESTED']);
            }

            if(isset($_POST['ISOUTOFSTATION'])){
                $insert_enquiry['ISOUTOFSTATION'] = $_POST['ISOUTOFSTATION'];
            }

            $info = getdate();
            $date = $info['mday'];
            $month = $info['mon'];
            $year = $info['year'];
            $hour = $info['hours'];
            $min = $info['minutes'];
            $sec = $info['seconds'];
            $current_date = "$year-$month-$date $hour:$min:$sec";

            $insert_enquiry['CREATED_DATE'] = $current_date;
            $addEnq = $this->tele_enquiry_model->add_tele_enquiry($insert_enquiry);

            if($addEnq == true){

              if(($_POST['SUGGESTED_CENTRE'] == "200") || ($_POST['SUGGESTED_CENTRE'] == "143")){
                $message = "St. Angelos New Enquiry \n".($_POST['FIRSTNAME']." ".$_POST['LASTNAME']).", M. No:".($_POST['MOBILE'].", ".$_POST['PARENT_NUMBER']).", Course:".implode(",",$_POST['COURSE_INTERESTED'])."";

                $cc = "";
                if($_POST['SUGGESTED_CENTRE'] == "143"){
                  $recipientsList = "9324488310,8291993316,9320248222";
                }
                elseif($_POST['SUGGESTED_CENTRE'] == "200") {
                  $recipientsList = "9324488336,9324488350";
                }
                $mobileNumbers = explode(",",$recipientsList);

                $confirmationSms = "";

                // for sms send to enquiry
                foreach ($mobileNumbers as $numbers) {
                  $confirmationSms = $this->load->sendSms($numbers,$message,$cc,"TE");
                }
                //end of sms code
                if($confirmationSms){
                  $this->session->set_flashdata('success','Record Added Succesfully');
                  redirect(base_url('add-tele-enquiry'));
                }
              }
              $this->session->set_flashdata('success','Record Added Succesfully');
              redirect(base_url('add-tele-enquiry'));
            }
          }
        $this->load->admin_view('add_tele_enquiry',$data);
    }

    public function edit_tele_enquiry($encrypt_id){
        $id = $this->encrypt->decode($encrypt_id);
        $data['tele_enquiries'] = $this->tele_enquiry_model->edit_tele_enquiry($id);
        $data['centres'] = $this->tele_enquiry_model->get_centres();
        $data['sources'] = $this->tele_enquiry_model->get_tele_sources();
        // $data['courses'] = $this->tele_enquiry_model->course_interested();
        $data['courses'] = $this->tele_enquiry_model->get_course_details_with_fees();
        $data['enq_handled_by'] = $this->tele_enquiry_model->enquiry_handled_by();

        $this->form_validation->set_rules('FIRSTNAME', 'First Name', 'required');
        $this->form_validation->set_rules('LASTNAME', 'Last Name', 'required');
        $this->form_validation->set_rules('EMAIL_ID', 'Email', 'trim|required|valid_email|xss_clean');
        $this->form_validation->set_rules('QUALIFICATION', 'Qualification', 'required');
        $this->form_validation->set_rules('MOBILE', 'Mobile', 'required|min_length[10]|max_length[10]|numeric|is_unique[tele_enquiries.MOBILE]',array('is_unique' => 'Mobile No. already exists'));
        $this->form_validation->set_rules('PARENT_NUMBER', 'Mobile', 'min_length[10]|max_length[10]|numeric');
        $this->form_validation->set_rules('MOBILE', 'Mobile', 'required|min_length[10]|max_length[10]|numeric');
        $this->form_validation->set_rules('ENQUIRY_DATE', 'Enquiry date', 'required');

        if(empty($_POST['SUGGESTED_CENTRE']) || $_POST['SUGGESTED_CENTRE'] == ''){
            $this->form_validation->set_rules('SUGGESTED_CENTRE', 'Suggested Centre', 'required');
        }

        if(empty($_POST['ENQUIRY_HANDELED_BY']) || $_POST['ENQUIRY_HANDELED_BY'] == ''){
            $this->form_validation->set_rules('ENQUIRY_HANDELED_BY', 'Enquiry Handled By', 'required');
        }

        if(empty($_POST['SOURCE']) || $_POST['SOURCE'] == ''){
            $this->form_validation->set_rules('SOURCE', 'Source', 'required');
        }

        if(empty($_POST['COURSE_INTERESTED'])){
            $this->form_validation->set_rules('COURSE_INTERESTED', 'Course Interested', 'required');
        }


        $modified_date =  date("Y-m-d H:i", strtotime(date('Y-m-d')));

        if ($this->form_validation->run() != FALSE)
        {
            $date_rep = str_replace('/', '-', $_POST['ENQUIRY_DATE']);
            $enq_date =  date("Y-m-d H:i", strtotime($date_rep));

            $update_enquiry = array(
                'FIRSTNAME' => $this->input->post('FIRSTNAME'),
                'MIDDLENAME' => $this->input->post('MIDDLENAME'),
                'LASTNAME'  => $this->input->post('LASTNAME'),
                'EMAIL_ID'  => $this->input->post('EMAIL_ID'),
                'QUALIFICATION'  => $this->input->post('QUALIFICATION'),
                'SUGGESTED_CENTRE_ID'  => $this->input->post('SUGGESTED_CENTRE'),
                'ENQUIRY_HANDELED_BY'  => $this->input->post('ENQUIRY_HANDELED_BY'),
                'SOURCE'  => $this->input->post('SOURCE'),
                'SUB_SOURCE'  => $this->input->post('SUBSOURCE'),
                'ENQUIRY_DATE'  => $enq_date,
                'MOBILE'  => $this->input->post('MOBILE'),
                'PARENT_NUMBER'  => $this->input->post('PARENT_NUMBER'),
                'STREAM'  => $this->input->post('STREAM'),
                'REMARKS'  => $this->input->post('REMARKS'),
                'PREFERRED_TIME' => $PREF_TIME,
                'MODIFIED_DATE'  => $modified_date,
                'MODIFIED_BY'  => $this->session->userdata('admin_data')[0]['EMPLOYEE_ID']
            );

            if(!empty($_POST['COURSE_INTERESTED'])){
                $update_enquiry['COURSE_INTERESTED'] = implode(",",$_POST['COURSE_INTERESTED']);
            }

            if(isset($_POST['ISOUTOFSTATION'])){
                $update_enquiry['ISOUTOFSTATION'] = $_POST['ISOUTOFSTATION'];
            }


            $info = getdate();
            $date = $info['mday'];
            $month = $info['mon'];
            $year = $info['year'];
            $hour = $info['hours'];
            $min = $info['minutes'];
            $sec = $info['seconds'];
            $current_date = "$year-$month-$date $hour:$min:$sec";

//            $update_enquiry['CREATED_ON'] = $current_date;
            $this->tele_enquiry_model->update_tele_enquiry($id,$update_enquiry);
            $this->session->set_flashdata('success','Record Updated Succesfully');
            redirect(base_url('view-tele-enquiry'));
        }
		$this->load->admin_view('edit_tele_enquiry',$data);
    }

    public function del_tele_enquiry($id){
        $telEnqId = $this->encrypt->decode($id);
        $this->tele_enquiry_model->del_tele_enquiry($telEnqId);
        $this->session->set_flashdata('danger','1 Record Deleted Succesfully');
        redirect(base_url('view-tele-enquiry'));
    }

    public function enquiry_handler(){
        $centreId = $this->input->post('scId');
        $enquiry_handlers = $this->tele_enquiry_model->get_enquiry_handlers($centreId);
        echo json_encode($enquiry_handlers);
    }

    public function sub_sources(){
        $psId = $this->input->post('psId');
        $sub_sources = $this->tele_enquiry_model->get_sub_sources($psId);
        echo json_encode($sub_sources);
    }

    public function getTodayFollowUps(){
        $followups = $this->tele_enquiry_model->get_today_followups();

        //print_r($followups);

        for($i=0;$i<count($followups);$i++){
            $courseInterest = $followups[$i]['COURSE_INTERESTED'];
            $courseId = explode(",",$courseInterest);
            $followups[$i]['courses'] = $this->tele_enquiry_model->course_names_for_followups($courseId);
            $followups[$i]['encodeId'] = $this->encrypt->encode($followups[$i]['TELE_ENQUIRY_ID']);

        }
        echo json_encode($followups);
    }

    public function showTodayFollowUps(){
        $data['followups'] = $this->tele_enquiry_model->get_today_followups();
        for($i=0;$i<count($data['followups']);$i++){
            $courseInterest = $data['followups'][$i]['COURSE_INTERESTED'];
            $courseId = explode(",",$courseInterest);
            $data['followups'][$i]['courses'] = $this->tele_enquiry_model->course_names_for_followups($courseId);

        }
        $this->load->admin_view('followup_enquiry_details',$data);
    }

    public function telEnqExp(){
        $this->load->library('excel');
        $filterData = $this->session->userdata('filter_data');
        $telEnqList = $this->tele_enquiry_model->get_tele_enquiry_list_excel($filterData);

        $i=1;
        $col = 'A';
        foreach ($telEnqList['telEnqFields'] as $key => $value) {
            $this->excel->getActiveSheet()->setCellValue($col.'1',$telEnqList['telEnqFields'][$key]);
            $rowNum = $i+1;

            foreach ($telEnqList['telEnqList'] as $telEnqKey => $telEnqValue) {
                $ENQUIRY_DATE = date("d/m/Y", strtotime($telEnqValue['ENQUIRY_DATE']));
                if($telEnqValue['FOLLOWUP_DATE'] == '0000-00-00 00:00:00' || $telEnqValue['FOLLOWUP_DATE'] == ''){
                $FOLLOWUP_DATE = 'N/a';
                }
                else{
                $FOLLOWUP_DATE = date("d/m/Y", strtotime($telEnqValue['FOLLOWUP_DATE']));
                }
                if($telEnqValue['NEXT_FOLLOWUP_DATE'] == '0000-00-00 00:00:00' || $telEnqValue['NEXT_FOLLOWUP_DATE'] == ''){
                $NEXT_FOLLOWUP_DATE = 'N/a';
                }
                else{
                $NEXT_FOLLOWUP_DATE = date("d/m/Y", strtotime($telEnqValue['NEXT_FOLLOWUP_DATE']));
                }
                if($telEnqValue['ISENROLLED'] == '0'){
                    $telEnqValue['ISENROLLED'] = 'No';
                }
                else if($telEnqValue['ISENROLLED'] == '1'){
                    $telEnqValue['ISENROLLED'] = 'Yes';
                }
                if($telEnqValue['WALKIN'] == '0'){
                    $telEnqValue['WALKIN'] = 'No';
                }
                else if($telEnqValue['WALKIN'] == '1'){
                    $telEnqValue['WALKIN'] = 'Yes';
                }
                if($telEnqValue['ISOUTOFSTATION'] == '0'){
                    $telEnqValue['ISOUTOFSTATION'] = 'No';
                }
                else if($telEnqValue['ISOUTOFSTATION'] == '1'){
                    $telEnqValue['ISOUTOFSTATION'] = 'Yes';
                }
                if($telEnqValue['ISWORKING'] == '0'){
                    $telEnqValue['ISWORKING'] = 'No';
                }
                else if($telEnqValue['ISWORKING'] == '1'){
                    $telEnqValue['ISWORKING'] = 'Yes';
                }

                $telEnqValue['ENQUIRY_DATE'] = $ENQUIRY_DATE;
                $telEnqValue['FOLLOWUP_DATE'] = $FOLLOWUP_DATE;
                $telEnqValue['NEXT_FOLLOWUP_DATE'] = $NEXT_FOLLOWUP_DATE;
                // echo "<pre>";
                // print_r($telEnqValue);
                // echo "</pre>";
                $this->excel->getActiveSheet()->setCellValue($col.$rowNum,$telEnqValue[$telEnqList['telEnqFields'][$key]]);
                $rowNum++;
            }

            $col++;
        }
        $filename='tele_enquiry_lists.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); /* tell browser what's the file name */
        header('Cache-Control: max-age=0'); //no cache

        /* save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type) */

        /* if you want to save it as .XLSX Excel 2007 format */
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        /* force user to download the Excel file without writing it to server's HD */
        $objWriter->save('php://output');
    }


    public function send_sms_to_all_type_of_receipients(){
        $message = $this->input->post('message');
        $cc = $this->input->post('cc');
        $recipientsList = $this->input->post('mobileNumbers');
        $mobileNumbers = explode(",",$recipientsList);

        $numberCount = count($mobileNumbers);
        $ccNumber = "";
        if($cc != ""){
            if(($numberCount == 1) && ($mobileNumbers['0'] == "")){
                $mobileNumbers['0'] = $cc;
                $ccNumber  = $mobileNumbers['0'];
            }
            else{
                $mobileNumbers[$numberCount] = $cc;
                $ccNumber  = $mobileNumbers[$numberCount];
            }
        }

        $confirmation['response'] = "";
        foreach($mobileNumbers as $number){
            $confirmation['response'] = $this->load->sendSms($number,$message,$ccNumber,"C");
        }

        echo json_encode($confirmation);
    }


    // This function is to convert a tele enquiry into walkin, the parameter  passed is tele_enquiry_id.
    public function update_convert_to_walk($id){
        $id= $this->encrypt->decode($id);
        $walkin_Status  =  array(
              'WALKIN' => '1',
              'MODIFIED_BY'=> $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
              'MODIFIED_DATE'=> date("Y-m-d H:i:s")
              );
        $tele_enquiry =  $this->tele_enquiry_model->get_tele_enquiry($id);

                  $insertenquirymaster= array(
                    'CENTRE_ID'=> $tele_enquiry[0]['SUGGESTED_CENTRE_ID'],
                    'ENQUIRY_DATE'=> $tele_enquiry[0]['ENQUIRY_DATE'],
                    'ENQUIRY_FIRSTNAME'=> $tele_enquiry[0]['FIRSTNAME'],
                    'ENQUIRY_MIDDLENAME'=> $tele_enquiry[0]['MIDDLENAME'],
                    'ENQUIRY_LASTNAME'=> $tele_enquiry[0]['LASTNAME'],
                    'ENQUIRY_EMAIL'=> $tele_enquiry[0]['EMAIL_ID'],
                    'ENQUIRY_HANDELED_BY'=> $tele_enquiry[0]['ENQUIRY_HANDELED_BY'],
                    'ISENROLLED'=> $tele_enquiry[0]['ISENROLLED'],
                    'REMARKS'=> $tele_enquiry[0]['REMARKS'],
                    'MODIFIED_BY'=> $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
                    'MODIFIED_DATE'=> date("Y-m-d H:i:s"),
                    'ENQUIRY_TELEPHONE'=> $tele_enquiry[0]['TELEPHONE'],
                    'ENQUIRY_MOBILE_NO'=> $tele_enquiry[0]['MOBILE'],
                    'COURSE_INTERESTED'=> $tele_enquiry[0]['COURSE_INTERESTED'],
                    'TELE_ENQUIRY_ID'=> $tele_enquiry[0]['TELE_ENQUIRY_ID'],
                    'ENQUIRY_PARENT_NO'=> $tele_enquiry[0]['PARENT_NUMBER'],
                    'ADMISSION_ID'=> '0',
                    'CREATED_BY'=> $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'],
                    'CREATED_DATE'=> date("Y-m-d H:i:s")
                      );


            $this->tele_enquiry_model->insert_enquiry_master($insertenquirymaster);
            $this->tele_enquiry_model->update_con_to_walkin($id,$walkin_Status);
            $this->session->set_flashdata('success','Convert to Walkin Successfully');
            redirect(base_url('view-tele-enquiry'));
    }

}
