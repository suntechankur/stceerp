<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tele_enquiry_model extends CI_Model {

  public $sql;
  public function __construct() {
      parent::__construct();
  }

	public function get_centres()
	{
		$query = $this->db->select('CENTRE_ID,CENTRE_NAME')
                          ->from('centre_master')
                          ->where('ISACTIVE','1')
                          ->get();
        return $query->result_array();
	}

    public function get_qualification(){
        $query = $this->db->get('qualification_master');
        return $query->result_array();
    }

    public function get_enquiry_handlers($centreId){
        $query = $this->db->select('ENQUIRY_FIRSTNAME,ENQUIRY_LASTNAME,ENQUIRY_ID')
                          ->from('enquiry_master')
                          ->where('CENTRE_ID',$centreId)
                          ->get();

        return $query->result_array();
    }

    public function get_sources($sid = 0){
        $query = $this->db->select('SOURCE_ID,SOURCE_SUBSOURCE')
                          ->from('source_master');
        if($sid == 0){
          $query = $this->db->where('PARENT_SOURCE_ID', '0');
        }
        else{
          $query = $this->db->where('SOURCE_ID',$sid);
        }
        $query = $this->db->order_by('SOURCE_SUBSOURCE','ASC')
                          ->get();
        return $query->result_array();
    }

    public function get_tele_sources($sid = 0){
        $query = $this->db->select('controlfile_id,controlfile_value')
                          ->from('control_file')
                          ->where('controlfile_key="HEARD_ABOUT_SACL" and ISACTIVE=1');
                          if($sid != 0){
                            $query = $this->db->where('controlfile_id'.$sid);
                          }
                          $query = $this->db->order_by('controlfile_value','ASC')
                          ->get();
        return $query->result_array();
    }

    public function get_modules(){
      $query = $this->db->select('MODULE_ID,MODULE_NAME')
                        ->from('module_master')
                        ->where('ISACTIVE','1')
                        ->get();
      return $query->result_array();
    }

    public function get_sub_sources($psId){
        $query = $this->db->select('SOURCE_ID,SOURCE_SUBSOURCE')
                          ->from('source_master')
                          ->where('PARENT_SOURCE_ID !=', '0')
                          ->where('PARENT_SOURCE_ID',$psId)
                          ->order_by('SOURCE_SUBSOURCE','ASC')
                          ->get();
        return $query->result_array();
    }

    public function get_tele_enquiry_list($offset,$perpage,$filter_data,$getcount = 0){
                $query = $this->db->select('TE.TELE_ENQUIRY_ID,TE.ENQUIRY_DATE,CONCAT(TE.FIRSTNAME," ",TE.LASTNAME) as NAME,SM.controlfile_value as SOURCE,TE.MOBILE,TE.PARENT_NUMBER,TE.EMAIL_ID,TE.COURSE_INTERESTED,TE.STREAM,CM.CENTRE_NAME,SC.CENTRE_NAME as SCENTRE_NAME,TE.LOCATION,CONCAT(EMH.EMP_FNAME," ",EMH.EMP_LASTNAME) as EMPH_NAME,TE.REMARKS,TE.WALKIN,TE.ISOUTOFSTATION,TE.ENQUIRY_ID,TE.ISENROLLED,TE.ADMISSION_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME,TE.ISWORKING,TE.FOLLOWED_UP')
                 ->from('tele_enquiries TE')
                 ->join('followup_details FD','FD.SOURSE_ID = TE.TELE_ENQUIRY_ID','left')
                 ->join('employee_master EM','EM.EMPLOYEE_ID = FD.FOLLOWUP_BY','left')
                 ->join('centre_master CM','CM.CENTRE_ID = TE.CENTRE_ID','left')
                 ->join('centre_master SC','SC.CENTRE_ID = TE.SUGGESTED_CENTRE_ID','left')
                 ->join('employee_master EMH','EMH.EMPLOYEE_ID = TE.ENQUIRY_HANDELED_BY','left')
                 ->join('control_file SM','SM.controlfile_id = TE.SOURCE','left outer')
                 ->group_by('TE.TELE_ENQUIRY_ID')
                 ->order_by('TE.TELE_ENQUIRY_ID','desc');

        $today_date_format_change = str_replace("/","-",date("Y/m/d"));
        $date = date('Y-m-d', strtotime($today_date_format_change));

        // $today_date_format_change = str_replace("/","-",date("2017/03/10"));
        // $date = date('Y-m-d', strtotime($today_date_format_change));

        if(!empty($filter_data)){
            if($filter_data['STARTDATE'] != '' && $filter_data['TODATE'] != ''){
                $date_format_change = str_replace("/","-",$filter_data['STARTDATE']);
                $start_date = date('Y-m-d', strtotime($date_format_change));

                $to_date_format_change = str_replace("/","-",$filter_data['TODATE']);
                $to_date = date('Y-m-d', strtotime($to_date_format_change));
                 $where = "TE.ENQUIRY_DATE BETWEEN '$start_date' AND '$to_date'";

                $query = $this->db->where($where);
            }

            if($filter_data['ISOUTOFSTATION']!=""){
              $query = $this->db->where('TE.LOCATION',$filter_data['ISOUTOFSTATION']);
            }

            // filter by laxmi
            if($filter_data['FIRSTNAME']!=""){
              $query = $this->db->where('TE.FIRSTNAME',$filter_data['FIRSTNAME']);
            }

            if($filter_data['LASTNAME']!=""){
              $query = $this->db->where('TE.LASTNAME',$filter_data['LASTNAME']);
            }

            if($filter_data['SUGGESTED_CENTRE_ID']!=""){
              $query = $this->db->where('TE.SUGGESTED_CENTRE_ID',$filter_data['SUGGESTED_CENTRE_ID']);
            }

            if($filter_data['SOURCE']!=""){
              $query = $this->db->where('TE.SOURCE',$filter_data['SOURCE']);
            }

             if($filter_data['ENQUIRY_HANDELED_BY']!=""){
              $query = $this->db->where('TE.ENQUIRY_HANDELED_BY',$filter_data['ENQUIRY_HANDELED_BY']);
            }

            if($filter_data['WALKIN'] != ""){
              if($filter_data['WALKIN']){
                $query = $this->db->where('TE.WALKIN = 1');
              }
              else{
                $where = "(TE.WALKIN = 0 OR TE.WALKIN IS NULL)";
                $query = $this->db->where($where);
              }
            }

            if($filter_data['STREAM']!=""){
              $query = $this->db->where('TE.STREAM',$filter_data['STREAM']);
            }

            if($filter_data['MOBILE']!=""){
              $query = $this->db->where('TE.MOBILE',$filter_data['MOBILE']);
            }

            if($filter_data['PARENT_NUMBER']!=""){
              $query = $this->db->where('TE.PARENT_NUMBER',$filter_data['PARENT_NUMBER']);
            }


            if($filter_data['ISWORKING']!="")
            {
              $query = $this->db->where('TE.ISWORKING',$filter_data['ISWORKING']);
            }

            if($filter_data['ISENROLLED'] != ""){
              if($filter_data['ISENROLLED']){
                $query = $this->db->where('TE.ISENROLLED = 1');
              }
              else{
                $where = "(TE.ISENROLLED = 0 OR TE.ISENROLLED IS NULL)";
                $query = $this->db->where($where);
              }
            }

            if($filter_data['followUp'] != ""){
              if($filter_data['followUp']){
                $query = $this->db->where('TE.FOLLOWED_UP = 1');
              }
              else{
                $query = $this->db->where('TE.FOLLOWED_UP',NULL);
              }
            }

            if($filter_data['ISHO'] != ""){
              if($filter_data['ISHO']){
                $query = $this->db->where('TE.SUGGESTED_CENTRE_ID != 155');
              }
              else{
                $query = $this->db->where('TE.SUGGESTED_CENTRE_ID = 155');
              }
            }

            unset($filter_data['STARTDATE'],$filter_data['TODATE'],$filter_data['ISHO'],$filter_data['ISOUTOFSTATION'],$filter_data['ISWORKING'],$filter_data['followUp']);

            // print_r($filter_data);
            // exit();
            foreach($filter_data as $filterkey => $filter){
                if($filter_data[$filterkey] != ''){

                $query = $this->db->like('TE.'.$filterkey,$filter,'both');
                }
            }
        }
        else{
           $query = $this->db->where('TE.ENQUIRY_DATE',$date);
        }

            if($getcount){
              $query = $this->db->get();

              return $query->num_rows();
              exit();
            }
              //$query = $this->db->limit($perpage,$offset);
              $query = $this->db->get();

              $tele_enquiry_data = $query->result_array();

              for ($i=0; $i <count($tele_enquiry_data) ; $i++) {
                $tele_enquiry_id = $tele_enquiry_data[$i]['TELE_ENQUIRY_ID'];

                $followup_details_query = $this->db->select('FOLLOWUP_DETAILS')
                                                   ->from('followup_details')
                                                   ->where('SOURSE_ID',$tele_enquiry_id)
                                                   ->order_by('NEXT_FOLLOWUP_DATE','desc')
                                                   ->limit(1)
                                                   ->get()
                                                   ->result_array();

                if(empty($followup_details_query)){
                  $followup_details_query['FOLLOWUP_DETAILS'] = '';
                }
                else{
                  $followup_details_query['FOLLOWUP_DETAILS'] = $followup_details_query[0]['FOLLOWUP_DETAILS'];
                  unset($followup_details_query[0]);
                }

                $tele_enquiry_data[$i]['FOLLOWUP_DETAILS'] = $followup_details_query['FOLLOWUP_DETAILS'];
              }
        return $tele_enquiry_data;
    }
    public function get_tele_enquiry_list_excel($filter_data){
        $query = $this->db->select('TE.ENQUIRY_DATE,CONCAT(TE.FIRSTNAME," ",TE.LASTNAME) as NAME,SM.SOURCE_SUBSOURCE,TE.MOBILE,TE.PARENT_NUMBER,TE.EMAIL_ID,TE.COURSE_INTERESTED,TE.STREAM,CM.CENTRE_NAME,SC.CENTRE_NAME as SUGGESTED_CENTRE_NAME,CONCAT(EMH.EMP_FNAME," ",EMH.EMP_LASTNAME) as EMPH_NAME,TE.REMARKS,TE.WALKIN,TE.ISOUTOFSTATION,TE.ENQUIRY_ID,TE.ISENROLLED,FD.PREFERRED_TIME,max(FD.FOLLOWUP_DATE) as FOLLOWUP_DATE,FD.FOLLOWUP_DETAILS,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME,FD.NEXT_FOLLOWUP_DATE,TE.ISWORKING')
                 ->from('tele_enquiries TE')
                 ->join('followup_details FD','FD.SOURSE_ID = TE.TELE_ENQUIRY_ID','left')
                 ->join('employee_master EM','EM.EMPLOYEE_ID = FD.FOLLOWUP_BY','left')
                 ->join('centre_master CM','CM.CENTRE_ID = TE.CENTRE_ID','left')
                 ->join('centre_master SC','SC.CENTRE_ID = TE.SUGGESTED_CENTRE_ID','left')
                 ->join('employee_master EMH','EMH.EMPLOYEE_ID = TE.ENQUIRY_HANDELED_BY','left')
                 ->join('source_master SM','SM.SOURCE_ID = TE.SOURCE','left outer')
                 ->group_by('TE.TELE_ENQUIRY_ID')
                 ->order_by('TE.TELE_ENQUIRY_ID','desc');
        if(!empty($filter_data)){
            if($filter_data['STARTDATE'] != '' && $filter_data['TODATE'] != ''){
                $date_format_change = str_replace("/","-",$filter_data['STARTDATE']);
                $start_date = date('Y-m-d', strtotime($date_format_change));

                $to_date_format_change = str_replace("/","-",$filter_data['TODATE']);
                $to_date = date('Y-m-d', strtotime($to_date_format_change));

                $query = $this->db->where('TE.ENQUIRY_DATE > ',$start_date)
                                  ->where('TE.ENQUIRY_DATE <',$to_date);
            }

            if($filter_data['ISOUTOFSTATION']!=""){
              $query = $this->db->where('ISOUTOFSTATION',$filter_data['ISOUTOFSTATION']);
            }

            if($filter_data['ISWORKING']!="")
            {
            $query = $this->db->where('TE.ISWORKING',$filter_data['ISWORKING']);
          }

            unset($filter_data['STARTDATE'],$filter_data['TODATE'],$filter_data['ISHO'],$filter_data['ISOUTOFSTATION'],$filter_data['ISWORKING'],$filter_data['followUp']);
            foreach($filter_data as $filterkey => $filter){

                if($filter_data[$filterkey] != ''){

                $query = $this->db->like('TE.'.$filterkey,$filter,'both');
                }
            }
        }
              $query = $this->db->get();

        $return['telEnqFields'] = $query->list_fields();
        $return['telEnqList'] = $query->result_array();
        return $return;
    }

    public function get_followup_details($tele_id){
        $query = $this->db->select('*')
                          ->from('followup_details')
                          ->where('SOURSE_ID',$tele_id)
                          ->where('SOURCE','J')
                          ->order_by('FOLLOWUP_DETAILS_ID','desc')
                          ->limit('1')
                          ->get();
        return $query->result_array();
    }

    public function get_followup_status_details($status){
        $query = $this->db->select('FollowUp_Status as status')
                          ->from('followup_status')
                          ->where('FollowUp_Status_ID ',$status)
                          ->get();
        return $query->row()->status;
    }

    public function enquiry_handled_by(){
        if(($this->session->userdata('admin_data')[0]['ROLE_ID'] == "2") || ($this->session->userdata('admin_data')[0]['ROLE_ID'] == "3") || ($this->session->userdata('admin_data')[0]['ROLE_ID'] == "23")){
          $query = $this->db->select('EM.EMPLOYEE_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME')
                            ->from('tele_enquiries TE')
                            ->join('followup_details FD','FD.SOURSE_ID = TE.TELE_ENQUIRY_ID')
                            ->join('employee_master EM','EM.EMPLOYEE_ID = TE.ENQUIRY_HANDELED_BY')
                            ->group_by('EM.EMP_FNAME,EM.EMP_LASTNAME')
                            ->where('FD.SOURCE','j')
                            ->get();
        }
        else{
          $query = $this->db->select('EM.EMPLOYEE_ID,CONCAT(EM.EMP_FNAME," ",EM.EMP_LASTNAME) as EMP_NAME')
                            ->from('employee_master EM')
                            ->where('EM.EMPLOYEE_ID',$this->session->userdata('admin_data')[0]['EMPLOYEE_ID'])
                            ->get();
        }

        return $query->result_array();
    }

    public function getEmployeeNameFromEmployeeId($employeeId){
        $query = $this->db->select('CONCAT(EMP_FNAME," ",EMP_MIDDLENAME," ",EMP_LASTNAME) as EMP_NAME')
                          ->from('employee_master')
                          ->where('EMPLOYEE_ID',$employeeId)
                          ->get();
        return $query->row()->EMP_NAME;
    }

    public function suggested_centre(){
        // $centerRolesIds = array(35,36,37,38,39,40,41,42);
        $query = $this->db->select('CENTRE_ID,CENTRE_NAME')
                          ->from('centre_master')
                          ->where('ISACTIVE','1')
                        //   if(in_array($this->session->userdata('admin_data')[0]['ROLE_ID'], $centerRolesIds)){
                        //     $query = $this->db->where('CENTRE_ID',$this->session->userdata('admin_data')[0]['CENTRE_ID']);
                        //   }
                          ->get();
        return $query->result_array();
    }

    public function course_interested($course_ids = array()){
        $employeeIds = array(3170,2818,3230);
        $query = $this->db->select('COURSE_ID,COURSE_NAME,MIN_FEES,MAX_FEES')
                          ->from('course_master')
                          ->group_by('COURSE_NAME');

        if(!(in_array($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'], $employeeIds))){
          $query = $this->db->where('ISACTIVE','1');
        }

        if($this->session->userdata('admin_data')[0]['CENTRE_ID'] == '205'){
            $query = $this->db->like("course_name","virar","both");
        }
        else{
            $query = $this->db->where("course_name not like '%virar%'");
        }

        if(!empty($course_ids) && is_array($course_ids)){
//            print_r($course_ids);
            $query = $this->db->where_in('COURSE_ID',$course_ids);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function all_courses(){
      $query = $this->db->select('COURSE_ID,COURSE_NAME,MIN_FEES,MAX_FEES')
                        ->from('course_master')
                        ->group_by('COURSE_NAME');

      $query = $this->db->get();
      return $query->result_array();
    }

    public function all_modules(){
      $query = $this->db->select('MODULE_ID,MODULE_NAME')
                        ->from('module_master')
                        ->get();
      return $query->result_array();
    }

    // added by ankur on 16/11/2016
    public function get_course_details_with_fees(){
      $sql ='';
      $query ='';
      if($this->session->userdata('admin_data')[0]['CENTRE_ID'] == '205'){
          $this->db->select("COURSE_ID,COURSE_NAME,MAX_FEES,MIN_FEES,BEST_FEES,ISACTIVE");
          $this->db->from("course_master");
          $this->db->like("course_name","virar","both");
          $this->db->where("isactive","1");
          $query1 = $this->db->get_compiled_select(); // It resets the query just like a get()

          $this->db->select("MODULE_ID,CONCAT(MODULE_NAME,'~'),MAX_FEES,MIN_FEES,BEST_FEES,ISACTIVE");
          $this->db->from("module_master");
          $this->db->like("module_name","virar","both");
          $this->db->where("isactive","1");
          $query2 = $this->db->get_compiled_select();

          $query = $this->db->query($query1." UNION ".$query2." order by course_name");
      }
      else{
          $this->db->select("COURSE_ID,COURSE_NAME,MAX_FEES,MIN_FEES,BEST_FEES,ISACTIVE");
          $this->db->from("course_master");
          $this->db->where("course_name not like '%virar%'");
          $this->db->where("isactive","1");
          $query1 = $this->db->get_compiled_select(); // It resets the query just like a get()

          $this->db->select("MODULE_ID,CONCAT(MODULE_NAME,'~'),MAX_FEES,MIN_FEES,BEST_FEES,ISACTIVE");
          $this->db->from("module_master");
          $this->db->where("module_name not like '%virar%'");
          $this->db->where("isactive","1");
          $query2 = $this->db->get_compiled_select();

          $query = $this->db->query($query1." UNION ".$query2." order by course_name");
      }
        return $query->result_array();
    }

    public function module_interested($module_ids = array()){
        $employeeIds = array(3170,2818,3230);
        $query = $this->db->select('MODULE_ID,MODULE_NAME,MIN_FEES,MAX_FEES')
                          ->from('module_master')
                          ->group_by('MODULE_NAME');

        if(!(in_array($this->session->userdata('admin_data')[0]['EMPLOYEE_ID'], $employeeIds))){
          $query = $this->db->where('ISACTIVE','1');
        }

        if($this->session->userdata('admin_data')[0]['CENTRE_ID'] == '205'){
            $query = $this->db->like("module_name","virar","both");
        }
        else{
            $query = $this->db->where("module_name not like '%virar%'");
        }

        if(!empty($module_ids) && is_array($module_ids)){
            $query = $this->db->where_in('MODULE_ID',$module_ids);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_tele_enquiry_count($filter_data){
        $query = $this->db->select('TE.TELE_ENQUIRY_ID')
                 ->from('tele_enquiries TE')
                 ->join('followup_details FD','FD.SOURSE_ID = TE.TELE_ENQUIRY_ID','left')
                 ->join('employee_master EM','EM.EMPLOYEE_ID = FD.FOLLOWUP_BY','left')
                 ->join('employee_master EMH','EMH.EMPLOYEE_ID = TE.ENQUIRY_HANDELED_BY','left')
                 ->group_by('TE.TELE_ENQUIRY_ID')
                 ->where('FD.SOURCE','j');
        if(!empty($filter_data)){

            if($filter_data['STARTDATE'] != '' && $filter_data['TODATE'] != ''){
                $date_format_change = str_replace("/","-",$filter_data['STARTDATE']);
                $start_date = date('Y-m-d', strtotime($date_format_change));

                $to_date_format_change = str_replace("/","-",$filter_data['TODATE']);
                $to_date = date('Y-m-d', strtotime($to_date_format_change));

                $query = $this->db->where('TE.ENQUIRY_DATE > ',$start_date)
                                  ->where('TE.ENQUIRY_DATE <',$to_date);
            }
            $query = $this->db->where('ISOUTOFSTATION',$filter_data['ISOUTOFSTATION']);
            unset($filter_data['STARTDATE'],$filter_data['TODATE'],$filter_data['ISHO'],$filter_data['followUp']);
            foreach($filter_data as $filterkey => $filter){

                $query = $this->db->like('TE.'.$filterkey,$filter,'both');
            }
        }
            $query = $this->db->get();
        return $query->num_rows();
    }

    public function add_tele_enquiry($insert_enquiry){
        $insert_enquiry['CENTRE_ID'] = '155';
        $this->db->insert('tele_enquiries',$insert_enquiry);
        $insert_enquiry['TELE_ENQUIRY_ID'] = $this->db->insert_id();
        if($this->db->affected_rows() > 0){
          return true;
        }
        else{
          return false;
        }
    }

    public function edit_tele_enquiry($id){
        $query = $this->db->select('TE.FIRSTNAME,TE.MIDDLENAME,TE.LASTNAME,TE.EMAIL_ID,TE.QUALIFICATION,CM.CENTRE_NAME,SM.SOURCE_ID,SM.SOURCE_SUBSOURCE,TE.SOURCE,TE.SUB_SOURCE,TE.ENQUIRY_DATE,TE.MOBILE,TE.COURSE_INTERESTED,TE.PARENT_NUMBER,TE.LOCATION,TE.STREAM,TE.ISOUTOFSTATION,TE.ISWORKING,TE.REMARKS,TE.SUGGESTED_CENTRE_ID,TE.ENQUIRY_HANDELED_BY')
                 ->from('tele_enquiries TE')
                 ->join('qualification_master QM','QM.QUALIFICATION_ID = TE.QUALIFICATION','left')
                 ->join('centre_master CM','CM.CENTRE_ID = TE.CENTRE_ID','left')
                 ->join('source_master SM','SM.SOURCE_ID = TE.SUB_SOURCE','left')
                 ->where('TE.TELE_ENQUIRY_ID',$id)
                 ->get();
        return $query->result_array();
    }

    public function update_tele_enquiry($id,$update_enquiry){
        $this->db->set($update_enquiry)
                 ->where('TELE_ENQUIRY_ID',$id)
                 ->update('tele_enquiries');
    }

    public function del_tele_enquiry($telEnqId){
        $this->db->where('TELE_ENQUIRY_ID',$telEnqId)
                 ->delete('tele_enquiries');
    }

    public function checkSubsource($sid){
      $query = $this->db->select('PARENT_SOURCE_ID')
                        ->from('source_master')
                        ->where('PARENT_SOURCE_ID',$sid)
                        ->get();
      if($query->num_rows() > 0){
        return true;
      }
      else{
        return false;
      }
    }

    public function get_today_followups(){
      $datetime = date('Y-m-d 00:00:00');
     // echo $datetime;
      $where = "FD.NEXT_FOLLOWUP_DATE = '$datetime' AND FD.SOURCE = 'J'";
      $query = $this->db->select('FD.SOURSE_ID,TE.FIRSTNAME,TE.MIDDLENAME,TE.LASTNAME,TE.MOBILE,TE.PARENT_NUMBER,FD.PREFERRED_TIME,TE.COURSE_INTERESTED,TE.TELE_ENQUIRY_ID,CM.CENTRE_NAME')
                        ->from('followup_details FD')
                        ->join('tele_enquiries TE','FD.SOURSE_ID = TE.TELE_ENQUIRY_ID','right')
                        ->join('centre_master CM','CM.CENTRE_ID = TE.SUGGESTED_CENTRE_ID','right')
                        ->where('FD.IS_FOLLOWUPED','0')
                        ->where($where);
                        if(($this->session->userdata('admin_data')[0]['ROLE_ID'] == "35") || ($this->session->userdata('admin_data')[0]['ROLE_ID'] == "36") || ($this->session->userdata('admin_data')[0]['ROLE_ID'] == "37") || ($this->session->userdata('admin_data')[0]['ROLE_ID'] == "38") || ($this->session->userdata('admin_data')[0]['ROLE_ID'] == "39") || ($this->session->userdata('admin_data')[0]['ROLE_ID'] == "40") || ($this->session->userdata('admin_data')[0]['ROLE_ID'] == "41")){
                          $query = $this->db->where('TE.SUGGESTED_CENTRE_ID',$this->session->userdata('admin_data')[0]['CENTRE_ID']);
                        }
                        $query = $this->db->where('CM.ISACTIVE',1)
                        ->group_by('TE.TELE_ENQUIRY_ID')
                        ->order_by('TE.ENQUIRY_DATE','DESC')
                        ->get();

        return $query->result_array();
    }

    public function course_names_for_followups($courseIdArray){
      $query = $this->db->select('COURSE_NAME')
                        ->from('course_master')
                        ->where_in('COURSE_ID',$courseIdArray)
                        ->get();

        return $query->result_array();
    }

    public function insert_into_sms_log($mobileNumber,$message,$cc,$toType){
      $data['CONTACT_NUM'] = $mobileNumber;
      $data['MSG'] = $message;
      $data['CC'] = $cc;
      $data['TO_TYPE'] = $toType;
      $data['SMS_DATE_TIME'] = date("Y-m-d H:i:s.u");
      $data['SEND_BY_USER_ID'] = $this->session->userdata('admin_data')[0]['EMPLOYEE_ID'];
      $query = $this->db->insert('sms_log',$data);
    }

    public function update_con_to_walkin($id,$walkin_Status){
        $this->db->set($walkin_Status)
                 ->where('TELE_ENQUIRY_ID',$id)
                 ->update('tele_enquiries');


    }


    public function get_tele_enquiry($id){
        $tele_enquiry = $this->db->select('SUGGESTED_CENTRE_ID,ENQUIRY_DATE,FIRSTNAME,MIDDLENAME,LASTNAME,EMAIL_ID,ENQUIRY_HANDELED_BY,ISENROLLED,REMARKS,MODIFIED_BY,MODIFIED_DATE,TELEPHONE,MOBILE,COURSE_INTERESTED,TELE_ENQUIRY_ID,PARENT_NUMBER,PREFERRED_TIME,ADMISSION_ID')
                        ->from('tele_enquiries')
                        ->where('TELE_ENQUIRY_ID',$id)
                        ->get();
        return $tele_enquiry->result_array();
    }

    public function insert_enquiry_master($insertenquirymaster){
        $this->db->insert('enquiry_master',$insertenquirymaster);
        unset($insertenquirymaster['ENQUIRY_PARENT_NO']);
        unset($insertenquirymaster['ADMISSION_ID']);
        unset($insertenquirymaster['CREATED_BY']);
        unset($insertenquirymaster['CREATED_DATE']);
         // echo $rowsAffected;
         // print_r($insertenquirymaster);

    }


}