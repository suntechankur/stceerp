<div class="gapping"></div>
<div class="create_batch_form">
<div id="box">
<h2>Tele Enquiries</h2>

</div>
<div class="row">
<div class="col-sm-12">
<div class="panel panel-default">
<div class="panel-heading">

    <!-- start message area -->

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('msg')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php $msg = $this->session->flashdata('msg'); echo $msg['msg']; ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>


<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info1')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
</div>
<?php } ?>

<?php if($this->session->flashdata('warning')) { ?>
<div class="alert alert-warning">
<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php } ?>

<!-- End message area -->
</div>
<div class="panel-body">
<?php
$attr = array('role'=>'form');
echo form_open('',$attr); ?>
<!--                    <form role="form" data-toggle="validator" method="post" action="lib/save.php" novalidate="true">-->
<div class="col-md-6">
<div class="form-group">
<label>First Name: <span class="text-danger">*</span></label>
<input class="form-control" name="FIRSTNAME" value="<?php echo $this->input->post('FIRSTNAME');?>" id="FIRSTNAME" type="text">
<span class="text-danger"><?php echo form_error('FIRSTNAME'); ?></span>
</div>
<div class="form-group">
<label>Middle Name:</label>
<input class="form-control" name="MIDDLENAME" value="<?php echo $this->input->post('MIDDLENAME');?>" id="MIDDLENAME" type="text"> </div>
<div class="form-group">
<label>Last Name: <span class="text-danger">*</span></label>
<input class="form-control" name="LASTNAME" value="<?php echo $this->input->post('LASTNAME');?>" id="LASTNAME" type="text">
<span class="text-danger"><?php echo form_error('LASTNAME'); ?></span>
</div>
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-6">
<label>E-mail: <span class="text-danger"></span></label>
<input class="form-control" name="EMAIL_ID" id="EMAIL_ID" value="<?php echo $this->input->post('EMAIL_ID');?>" type="text">
<span class="text-danger"><?php echo form_error('EMAIL_ID'); ?></span>
</div>
<div class="col-lg-6" style="float:right">
<label>Current Qualification: <span class="text-danger">*</span></label>
<select name="QUALIFICATION" class="form-control">
    <option value="">Please Select</option>
    <option value="1" <?php echo ($this->input->post('QUALIFICATION')=="1") ? "selected" : '';?>>Xth Pass </option>
    <option value="2" <?php echo ($this->input->post('QUALIFICATION')=="2") ? "selected" : '';?>>XIIth Pass </option>
    <option value="3" <?php echo ($this->input->post('QUALIFICATION')=="3") ? "selected" : '';?>>Under Graduate </option>
    <option value="4" <?php echo ($this->input->post('QUALIFICATION')=="4") ? "selected" : '';?>>Graduate </option>
    <option value="5" <?php echo ($this->input->post('QUALIFICATION')=="5") ? "selected" : '';?>>Post Graduate </option>
    <option value="6" <?php echo ($this->input->post('QUALIFICATION')=="6") ? "selected" : '';?>>Other </option>
</select>
<span class="text-danger"><?php echo form_error('QUALIFICATION'); ?></span>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="form-group">
<label>Select Multiple Course: <span class="text-danger">*</span></label><br />
<?php $COURSE_INTERESTED = $this->input->post('COURSE_INTERESTED');?>
<select class="form-control course_interested" name="COURSE_INTERESTED[]" id="COURSE_INTERESTED" multiple>
<?php

foreach($courses as $course){

 $isSelected = '';
        if ($COURSE_INTERESTED) {

          $isSelected = in_array($course['COURSE_NAME'],$COURSE_INTERESTED) ? "selected='selected'" : "";
         }

 ?>
<option value="<?php echo $course['COURSE_NAME']; ?>" <?php echo $isSelected; ?>><?php echo $course['COURSE_NAME']; ?></option>
<?php } ?>
</select>
<span class="text-danger"><?php echo form_error('COURSE_INTERESTED'); ?></span>
</div>
<div class="row">
<div class="col-md-12">
<ul class="list-group courses_seld"></ul>
</div>
</div>
</div>
<div class="col-md-6">
<label>Location : <span class="text-danger"></span></label>
<select class="form-control" name="LOCATION">
    <option value="">Please Select</option>
    <option value="0" <?php if(isset($filter_data['LOCATION']) && $filter_data['LOCATION'] == '0'){echo 'selected="selected"';} ?> >From Mumbai</option>
    <option value="1" <?php if(isset($filter_data['LOCATION']) && $filter_data['LOCATION'] == '1'){echo 'selected="selected"';} ?>>Out Station</option>
</select>
<span class="text-danger"><?php echo form_error('LOCATION'); ?></span>
</div>
<div class="col-md-6">
<label class="form-check-inline">
<input class="form-check-input" name="ISWORKING" value="1"<?php echo ($this->input->post('ISWORKING')=="1") ? "checked" : '';?> id="ISWORKING" type="checkbox"> Is Working &nbsp;&nbsp; </label>

</div>
</div>
</div>
<div class="col-md-6">
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-6">
<label>Suggested Center: <span class="text-danger">*</span></label>
<select name="SUGGESTED_CENTRE" class="form-control suggested_centre">
     <option value="">Please Select Centre</option>
     <?php foreach($centres as $centre){?>
    <option value="<?php echo $centre['CENTRE_ID']; ?>"<?php echo ($this->session->userdata('admin_data')[0]['CENTRE_ID']==$centre['CENTRE_ID']) ? "selected" : '';?>><?php echo $centre['CENTRE_NAME']; ?></option>
    <?php } ?>
</select>
<span class="text-danger"><?php echo form_error('SUGGESTED_CENTRE'); ?></span>
</div>
<div class="col-lg-6" style="float:right">
<label>Enquiry Handled By:</label>
<select name="ENQUIRY_HANDELED_BY" class="form-control enquiry_handler" readonly="true">
<?php foreach($enq_handled_by as $enq_handler){ ?>
            <option value="<?php echo $enq_handler['EMPLOYEE_ID']; ?>"<?php echo ($this->session->userdata('admin_data')[0]['EMPLOYEE_ID']==$enq_handler['EMPLOYEE_ID']) ? "selected" : '';?>><?php echo $enq_handler['EMP_NAME']; //$filter_data['ENQUIRY_HANDELED_BY'] ?></option>
        <?php } ?>
</select>
<span class="text-danger"><?php echo form_error('ENQUIRY_HANDELED_BY'); ?></span>
</div>
</div>
</div>
</div>
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-6">
<label>Source: <span class="text-danger">*</span></label>
<select name="SOURCE" class="form-control source_master">
    <option value="">Please Select Source</option>
    <?php foreach($sources as $source){ ?>
    <option value="<?php echo $source['controlfile_id'] ?>"<?php echo ($this->input->post('SOURCE')==$source['controlfile_id']) ? "selected" : '';?>><?php echo $source['controlfile_value'] ?></option>
    <?php } ?>
</select>
<span class="text-danger"><?php echo form_error('SOURCE'); ?></span>
</div>
<div class="col-lg-6" style="float:right">
<label>Sub Source:</label>
<div id="SubsourceHint">
    <script type="text/javascript" src="js/reloaddiv.js"></script>
    <select name="SUBSOURCE" class="form-control sub_source">
<!--                                                    <option value="">Other</option>-->
    </select>
</div>

</div>
</div>
</div>
</div>
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-6">
<label>Enquiry Date: <span class="text-danger">*</span></label>
<input class="form-control enquiry_date add_enquiry_date" maxlength="10" name="ENQUIRY_DATE" id="enquiry_date" type="text" value="<?php echo $this->input->post('ENQUIRY_DATE');?>">
<span class="text-danger"><?php echo form_error('ENQUIRY_DATE'); ?></span>
  </div>
<div class="col-lg-6" style="float:right">
<label>Mobile No.: <span class="text-danger">*</span></label>
<input class="form-control" value="<?php echo $this->input->post('MOBILE');?>" onkeypress="return isNumber(event)" name="MOBILE" id="MOBILE" style="width:100%" maxlength="10" size="10" type="text">
<span class="text-danger"><?php echo form_error('MOBILE'); ?></span>
</div>
</div>
</div>
</div>
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-md-6">
<label>Parent's No.:</label>
<input class="form-control" name="PARENT_NUMBER" value="<?php echo $this->input->post('PARENT_NUMBER');?>" id="PARENT_NUMBER" style="width:100%" maxlength="10" size="10" type="text">
<span class="text-danger"><?php echo form_error('PARENT_NUMBER'); ?></span>
</div>
<div class="col-md-6">
<div class="form-group">
            <label>Stream:</label>
            <select class="form-control" style="width:100% ;" name="STREAM" id="STREAM">
                <option value="basics"<?php echo ($this->input->post('STREAM')=="basics") ? "selected" : '';?>>Basics</option>
                <option value="Programming"<?php echo ($this->input->post('STREAM')=="Programming") ? "selected" : '';?>>Programming</option>
                <option value="Graphics_animation"<?php echo ($this->input->post('STREAM')=="Graphics_animation") ? "selected" : '';?>>Graphics &amp; animation</option>
                <option value="Hardware_Networking"<?php echo ($this->input->post('STREAM')=="Hardware_Networking") ? "selected" : '';?>>Hardware &amp; Networking</option>
                <option value="others"<?php echo ($this->input->post('STREAM')=="others") ? "selected" : '';?>>others</option>
            </select>
</div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="form-group">
<label>Remarks:</label>
<input class="form-control" name="REMARKS" value="<?php echo $this->input->post('REMARKS');?>" type="text">
</div>
</div>
<div class="col-md-6">
<div class="form-group">
<br>
<label class="form-check-inline">
<input class="form-check-input" name="ISOUTOFSTATION" value="1"<?php echo ($this->input->post('ISOUTOFSTATION')=="1") ? "checked" : '';?> id="ISOUTOFSTATION" type="checkbox"> Is Out Of Station &nbsp;&nbsp; </label>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<label><input class="form-check-input select_prefered_time" name="PREFFERED_TIME_SELECTION" type="checkbox">
<span id="preffered_time_selection_span">Show Preffered Time &nbsp;&nbsp; </span></label>
<label for="" id="preffered_time_selection_label" style="display:none;">Preferred Time</label>
</div>
<div id="preffered_timing_selection_div" style="display:none;">
    <div class="col-md-6">
    <!-- <label for="">Preferred Timing</label> -->
    <label for="from_time">From</label>
    <input class="pref_time form-control" id="from_time" data-format="h:mm a" data-template="hh : mm a" name="from_time" value="" type="text">
    </div>
    <div class="col-md-6">
    <label for="to_time">To &nbsp;&nbsp;&nbsp;</label>
    <input class="pref_time" id="to_time" data-format="h:mm a" data-template="hh : mm a" name="to_time" value="" type="text">
    </div>
</div>
</div>
<br>
<button type="submit" class="btn btn-primary">Create</button> &nbsp;
<button type="reset" style="float:none">Reset</button>
</div>
<!--                    </form>-->
<?php echo form_close(); ?>
</div>
</div>
</div>
</div>
</div>
