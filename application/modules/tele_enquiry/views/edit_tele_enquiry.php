<div class="gapping"></div>
<div class="create_batch_form">
    <div id="box">
        <h2>Update Tele Enquiries</h2>
        <?php if($this->session->flashdata('msg')) {
            $msg = $this->session->flashdata('msg');
            echo $msg['msg'];
        } ?>
        </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align:center; color:#F00;"></div>
                <div class="panel-body">
                   <?php
                    $attr = array('role'=>'form');
                    echo form_open('',$attr);
                    foreach($tele_enquiries as $enquiry){
                    ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name:</label>
                                <input class="form-control" name="FIRSTNAME" value="<?php echo $enquiry['FIRSTNAME'] ?>" id="FIRSTNAME" type="text">
                                <span class="text-danger"><?php echo form_error('FIRSTNAME'); ?></span>
                                </div>
                            <div class="form-group">
                                <label>Middle Name:</label>
                                <input class="form-control" name="MIDDLENAME" value="<?php echo $enquiry['MIDDLENAME'] ?>" id="MIDDLENAME" type="text"> </div>
                            <div class="form-group">
                                <label>Last Name:</label>
                                <input class="form-control" name="LASTNAME" value="<?php echo $enquiry['LASTNAME'] ?>" id="LASTNAME" type="text">
                                <span class="text-danger"><?php echo form_error('LASTNAME'); ?></span>
                                </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>E-mail:</label>
                                            <input class="form-control" name="EMAIL_ID" id="EMAIL_ID" value="<?php echo $enquiry['EMAIL_ID'] ?>" type="text">
                                            <span class="text-danger"><?php echo form_error('EMAIL_ID'); ?></span>
                                            </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Current Qualification:</label>
                                            <select name="QUALIFICATION" class="form-control">
                                                <option value="">Please Select</option>
                                                <option value="1" <?php if($enquiry['QUALIFICATION'] == '1'){ echo 'selected'; }?>>Xth Pass </option>
                                                <option value="2" <?php if($enquiry['QUALIFICATION'] == '2'){ echo 'selected'; }?>>XIIth Pass </option>
                                                <option value="3" <?php if($enquiry['QUALIFICATION'] == '3'){ echo 'selected'; }?>>Under Graduate </option>
                                                <option value="4" <?php if($enquiry['QUALIFICATION'] == '4'){ echo 'selected'; }?>>Graduate </option>
                                                <option value="5" <?php if($enquiry['QUALIFICATION'] == '5'){ echo 'selected'; }?>>Post Graduate </option>
                                                <option value="6" <?php if($enquiry['QUALIFICATION'] == '6'){ echo 'selected'; }?>>Other </option>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('QUALIFICATION'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                               <div class="col-md-6">
                                   <label>Select Multiple Course:</label><br />
                                <select class="form-control course_interested" name="COURSE_INTERESTED[]" id="COURSE_INTERESTED" multiple>
                                <?php
//                                    $test = array('3767','3708');
                                    $test = explode(",",$enquiry['COURSE_INTERESTED']);
                                    foreach($courses as $course){
                                        $isSelected = in_array($course['COURSE_NAME'],$test) ? "selected='selected'" : "";
                                    ?>
                                    <option value="<?php echo $course['COURSE_NAME']; ?>" <?php echo $isSelected; ?> ><?php echo $course['COURSE_NAME']; ?></option>
                                    <?php } ?>
                                </select>
                                <span class="text-danger"><?php echo form_error('COURSE_INTERESTED'); ?></span>
                               </div>
                               <div class="row">
                                <div class="col-md-12">
                                <ul class="list-group courses_seld">
                                  <?php
                                       $selected_course = explode(",",$enquiry['COURSE_INTERESTED']);
                                       foreach($courses as $course){
                                       if(in_array($course['COURSE_NAME'],$selected_course)){ ?>
                                         <li class="list-group-item">
                                             <?php echo $course['COURSE_NAME'];; ?>
                                         </li>
                                   <?php } } ?>
                                </ul>
                                </div>
                                </div>
                               <div class="col-md-6">
                                            <label class="form-check-inline">
                                                <input class="form-check-input" name="ISWORKING" value="1" id="ISWORKING" type="checkbox" <?php if($enquiry['ISOUTOFSTATION'] == '0'){echo 'checked';} ?>> Is Working &nbsp;&nbsp; </label>
                               </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Suggested Center:</label>
                                            <select name="SUGGESTED_CENTRE" class="form-control suggested_centre">
                                                 <option value="">Please Select Centre</option>
                                                 <?php foreach($centres as $centre){?>
                                                <option value="<?php echo $centre['CENTRE_ID']; ?>" <?php echo ($centre['CENTRE_ID'] == $enquiry['SUGGESTED_CENTRE_ID']) ? ' selected="selected"' : '';?>><?php echo $centre['CENTRE_NAME']; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('SUGGESTED_CENTRE'); ?></span>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Enquiry Handled By:</label>
                                            <select name="ENQUIRY_HANDELED_BY" class="form-control enquiry_handler"> 
                                            <?php foreach($enq_handled_by as $enq_handler){ ?>
                                                        <option value="<?php echo $enq_handler['EMPLOYEE_ID']; ?>" <?php echo ($enq_handler['EMPLOYEE_ID'] == $enquiry['ENQUIRY_HANDELED_BY']) ? ' selected="selected"' : '';?>><?php echo $enq_handler['EMP_NAME']; //$filter_data['ENQUIRY_HANDELED_BY'] ?></option>
                                                    <?php } ?>
                                            </select>
                                            <span class="text-danger"><?php echo form_error('ENQUIRY_HANDELED_BY'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $enquiry['SOURCE'];?>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Source:</label>
                                            <select name="SOURCE" class="form-control source_master" <?php if($enquiry['SOURCE'] != ''){ if($enquiry['SUB_SOURCE'] != '0'){echo 'data-subsource="'.$enquiry['SUB_SOURCE'].'"';}else{echo 'data-subsource="err"';}} ?>>
                                                <option value="">Please Select Source</option>
                                                <?php foreach($sources as $source){ ?>
                                                <option value="<?php echo $source['controlfile_id']; ?>"<?php echo ($source['controlfile_id'] == $enquiry['SOURCE']) ? ' selected="selected"' : '';?>><?php echo $source['controlfile_value'] ?></option>
                                                <?php } ?>
                                                <span class="text-danger"><?php echo form_error('SOURCE'); ?></span>
                                            </select>
                                        </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Sub Source:</label>
                                            <div id="SubsourceHint">
                                                <script type="text/javascript" src="js/reloaddiv.js"></script>
                                                <select name="SUBSOURCE" class="form-control sub_source">
<!--                                                    <option value="">Other</option>-->
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-lg-6">
                                            <label>Enquiry Date:</label>
                                            <input value="<?php $ENQUIRY_DATE = date("d/m/Y", strtotime($enquiry['ENQUIRY_DATE'])); echo $ENQUIRY_DATE; ?>" class="form-control enquiry_date" maxlength="10" name="ENQUIRY_DATE" id="enquiry_date" type="text"> </div>
                                        <div class="col-lg-6" style="float:right">
                                            <label>Mobile No.:</label>
                                            <input class="form-control" value="<?php echo $enquiry['MOBILE'] ?>" onkeypress="return isNumber(event)" name="MOBILE" id="MOBILE" style="width:100%" type="text">
                                            <span class="text-danger"><?php echo form_error('MOBILE'); ?></span>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row" style="margin-left:-30px;">
                                    <div class="col-lg-12" style="margin-left:0px;">
                                        <div class="col-md-6">
                                            <label>Parent's No.:</label>
                                            <input class="form-control" name="PARENT_NUMBER" value="<?php echo $enquiry['PARENT_NUMBER']; ?>" id="PARENT_NUMBER" style="width:100%" maxlength="10" size="10" type="text">
                                            <span class="text-danger"><?php echo form_error('PARENT_NUMBER'); ?></span>
                                        </div>
                                        <div class="col-md-6">
                                                <label>Stream:</label>
                                            <select class="form-control" style="width:100% ;" name="STREAM" id="STREAM">
                                                <option value="basics" <?php if($enquiry['STREAM'] == 'basics'){ echo 'selected'; }?>>Basics</option>
                                                <option value="Programming" <?php if($enquiry['STREAM'] == 'Programming'){ echo 'selected'; }?>>Programming</option>
                                                <option value="Graphics_animation" <?php if($enquiry['STREAM'] == 'Graphics_animation'){ echo 'selected'; }?>>Graphics &amp; animation</option>
                                                <option value="Hardware_Networking" <?php if($enquiry['STREAM'] == 'Hardware_Networking'){ echo 'selected'; }?>>Hardware &amp; Networking</option>
                                                <option value="others" <?php if($enquiry['STREAM'] == 'others'){ echo 'selected'; }?>>others</option>
                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label>Remarks:</label>
                                    <input class="form-control" name="REMARKS" value="<?php echo $enquiry['REMARKS']; ?>" type="text">
                                 </div>
                                </div>
                                <div class="col-md-6">
                                   <br>
                                    <label class="form-check-inline">
                                                <input class="form-check-input" name="ISOUTOFSTATION" value="1" id="ISOUTOFSTATION" type="checkbox" <?php if($enquiry['ISOUTOFSTATION'] == '1'){echo 'checked';} ?>> Is Out Of Station &nbsp;&nbsp; </label>
                                </div>
                            </div>
                            <br>
<!--                            <button type="submit" class="btn btn-primary">Update</button> &nbsp;-->
                            <button type="submit" class="btn btn-primary">Update</button> &nbsp;
                            <button type="reset">Reset</button>
                        </div>
<!--                    </form>-->
               <?php echo form_close(); ?>
               <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
