<br/>
<div class="col-md-12 col-sm-12">
	<div id="box">
		<h2>Today FollowUps</h2>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-body">
						<div class="col-md-12">
							<div class="form-group">
								<div class="row" style="margin-left:-30px;">
									<div class="col-lg-12" style="margin-left:0px;">
									<?php
									foreach($followups as $fdetails){
									?>
									<div class="col-md-6">
										<label>Student Name : </label> <?php echo $fdetails['FIRSTNAME']." ".$fdetails['MIDDLENAME']." ".$fdetails['LASTNAME']?>
									</div>
									<div class="col-md-6">
										<label>Preferred Time : </label> <?php echo $fdetails['PREFERRED_TIME']?>
									</div>
									<div class="col-md-6">
										<label>Mobile No. : </label> <?php echo $fdetails['MOBILE']?>
										<br/>
										<label>Parent Mobile No. : </label> <?php echo $fdetails['PARENT_NUMBER']?>
										<br/>
										<label>Suggested Centre : </label> <?php echo $fdetails['CENTRE_NAME']?>
									</div>
									<div class="col-md-6">
										<label>Course Interested : </label>
										<?php
										$courses = "";
										foreach($fdetails['courses'] as $value) {
											
											$courses .= $value['COURSE_NAME'].", ";
											
										 } 
										 echo rtrim($courses,", ");
										 ?>
									</div>
									<div class="col-md-6">
										<a href="<?php echo base_url('tele-enquiry-follow-up/details/'.$this->encrypt->encode($fdetails['TELE_ENQUIRY_ID']).'/teleEnquiry');?>"><span class="glyphicon glyphicon-earphone" style="font-size:20px;color:red;"></span>Add folllowup</a>
									</div>
									<br/>
									<hr>
									<br/>
									<?php } ?>

									</div>

								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>