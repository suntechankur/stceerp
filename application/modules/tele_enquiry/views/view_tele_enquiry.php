<br/>
<div class="col-md-12 col-sm-12">
<div class="create_batch_form">
<div id="box">
<h2>Tele Enquiry Search</h2></div>
<div class="row">
<div class="col-sm-12">
<div class="panel panel-default">
<div class="panel-heading">

    <!-- start message area -->

<?php if($this->session->flashdata('danger')) { ?>
<div class="alert alert-danger">
<strong>Danger!</strong> <?php echo $this->session->flashdata('danger'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('msg')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php $msg = $this->session->flashdata('msg'); echo $msg['msg']; ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('success')) { ?>
<div class="alert alert-success">
<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
</div>
<?php } ?>


<?php if($this->session->flashdata('failed')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('failed'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
</div>
<?php } ?>

<?php if($this->session->flashdata('info1')) { ?>
<div class="alert alert-info">
<strong>Info!</strong> <span style="color:red"><?php echo $this->session->flashdata('info1'); ?></span>
</div>
<?php } ?>

<?php if($this->session->flashdata('warning')) { ?>
<div class="alert alert-warning">
<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
</div>
<?php } ?>

<!-- End message area -->
 </div>
<div class="panel-body">
<?php echo form_open(); ?>
<div class="col-md-6">
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-4">
<label>Enquiry Handled By:</label>
<select name="ENQUIRY_HANDELED_BY" class="form-control">
<option value="">Select Enquiry Handler</option>
<?php foreach($enq_handled_by as $enq_handler){ ?>
<option value="<?php echo $enq_handler['EMPLOYEE_ID']; ?>" <?php if(isset($filter_data['ENQUIRY_HANDELED_BY'])){ echo ($enq_handler['EMPLOYEE_ID'] == $filter_data['ENQUIRY_HANDELED_BY']) ? ' selected="selected"' : '';}?>>
<?php echo $enq_handler['EMP_NAME']; //$filter_data['ENQUIRY_HANDELED_BY'] ?>
</option>
<?php } ?>
</select>
</div>
<div class="col-lg-4">
<label>From Date:</label>
<input type="text" name="STARTDATE" id="STARTDATE" value="<?php if(isset($filter_data['STARTDATE'])){ echo $filter_data['STARTDATE']; } else {echo date('d/m/Y');}?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY"> </div>
<div class="col-lg-4">
<label>To Date:</label>
<input type="text" name="TODATE" id="TODATE" value="<?php if(isset($filter_data['TODATE'])){ echo $filter_data['TODATE']; } else {echo date('d/m/Y');}?>" class="form-control enquiry_date" maxlength="10" placeholder="DD/MM/YYYY"> </div>
</div>
</div>
</div>
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-4">
<label>Mobile No.:</label>
<input type="Text" name="MOBILE" value="<?php if(isset($filter_data['MOBILE'])){ echo $filter_data['MOBILE'];} ?>" maxlength="10" id="MOBILE" class="form-control"> </div>
<div class="col-lg-4">
<label>Parent's No.:</label>
<input type="Text" name="PARENT_NUMBER" value="<?php if(isset($filter_data['PARENT_NUMBER'])){ echo $filter_data['PARENT_NUMBER'];} ?>" maxlength="10" id="PARENT_NUMBER" class="form-control"> </div>
<div class="col-lg-4">
<label>Source:</label>
<select name="SOURCE" class="form-control source_master">
<option value="">Select Source</option>
<?php foreach($sources as $source){ ?>
<option value="<?php echo $source['controlfile_id']; ?>" <?php if(isset($filter_data['SOURCE'])){ echo ($source['controlfile_id'] == $filter_data['SOURCE']) ? ' selected="selected"' : ''; }?>><?php echo $source['controlfile_value'] ?></option>
<?php } ?>
</select>
</div>
</div>
</div>
</div>
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-4">
<label>Is Enrolled:</label>
<select name="ISENROLLED" class="form-control">
<option value="">Please Select</option>
<option value="1" <?php if(isset($filter_data['ISENROLLED']) && $filter_data['ISENROLLED'] == '1'){echo 'selected="selected"';} ?> >Enrolled</option>
<option value="0" <?php if(isset($filter_data['ISENROLLED']) && $filter_data['ISENROLLED'] == '0'){echo 'selected="selected"';} ?>>Not Enrolled</option>
</select>
</div>

<div class="col-lg-4">
<label>Is Walkin:</label>
<select name="WALKIN" class="form-control">
<option value="">Please Select</option>
<option value="1" <?php if(isset($filter_data['WALKIN']) && $filter_data['WALKIN'] == '1'){echo 'selected="selected"';} ?>>Walkin</option>
<option value="0" <?php if(isset($filter_data['WALKIN']) && $filter_data['WALKIN'] == '0'){echo 'selected="selected"';} ?>>Not Walkin</option>
</select>
</div>
<div class="col-lg-4">
<label>Is HO:</label>
<select name="ISHO" class="form-control">
<option value="">All</option>
<option value="1" <?php if(isset($filter_data['ISHO']) && $filter_data['ISHO'] == '1'){echo 'selected="selected"';} ?>>Forwarded</option>
<option value="0" <?php if(isset($filter_data['ISHO']) && $filter_data['ISHO'] == '0'){echo 'selected="selected"';} ?>>Not Forwarded</option>
</select>
</div>
</div>
</div>
</div>
<div class="form-group">
	<div class="row" style="margin-left:-30px;">
		<div class="col-lg-12" style="margin-left:0px;">
			<div class="col-md-4">
				<label for="isFollowed">Follow Up</label>
				<select name="followUp" id="followUp" class="form-control">
					<option value="">All</option>
					<option value="1" <?php if(isset($filter_data['followUp']) && $filter_data['followUp'] == '1'){echo 'selected="selected"';} ?>>Followed</option>
					<option value="0" <?php if(isset($filter_data['followUp']) && $filter_data['followUp'] == '0'){echo 'selected="selected"';} ?>>Unfollowed</option>
				</select>
			</div>
		</div>
	</div>
</div>
</div>
<div class="col-md-6">
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-4">
<label>First Name:</label>
<input class="form-control" type="text" value="<?php if(isset($filter_data['FIRSTNAME'])){ echo $filter_data['FIRSTNAME']; }?>" maxlength="50" name="FIRSTNAME" id="FIRSTNAME"> </div>
<div class="col-lg-4">
<label>Last Name:</label>
<input class="form-control" type="text" value="<?php if(isset($filter_data['LASTNAME'])){ echo $filter_data['LASTNAME']; }?>" name="LASTNAME" id="LASTNAME" maxlength="50"> </div>
<div class="col-lg-4">
<label>Qualification:</label>
<select name="QUALIFICATION" class="form-control">
<option value="">Please Select</option>
<option value="1" <?php if(isset($filter_data['QUALIFICATION']) && $filter_data['QUALIFICATION'] == '1'){echo 'selected="selected"';} ?>>Xth Pass </option>
<option value="2" <?php if(isset($filter_data['QUALIFICATION']) && $filter_data['QUALIFICATION'] == '2'){echo 'selected="selected"';} ?>>XIIth Pass </option>
<option value="3" <?php if(isset($filter_data['QUALIFICATION']) && $filter_data['QUALIFICATION'] == '3'){echo 'selected="selected"';} ?>>Under Graduate </option>
<option value="4" <?php if(isset($filter_data['QUALIFICATION']) && $filter_data['QUALIFICATION'] == '4'){echo 'selected="selected"';} ?>>Graduate </option>
<option value="5" <?php if(isset($filter_data['QUALIFICATION']) && $filter_data['QUALIFICATION'] == '5'){echo 'selected="selected"';} ?>>Post Graduate </option>
<option value="6" <?php if(isset($filter_data['QUALIFICATION']) && $filter_data['QUALIFICATION'] == '6'){echo 'selected="selected"';} ?>>Other </option>
</select>
</div>
</div>
</div>
</div>
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-4">
<label>Sub Source:</label>
<select name="SUB_SOURCE" class="form-control sub_source">
<option value="">Select Source First</option>
</select>
</div>
<div class="col-lg-4">
<label>Suggested Centre:</label>
<select name="SUGGESTED_CENTRE_ID" class="form-control">
<option value="">Select Centre</option>
<?php foreach($centres as $centre){ ?>
<option value="<?php echo $centre['CENTRE_ID']; ?>" <?php if(isset($filter_data['SUGGESTED_CENTRE_ID'])){ echo ($centre['CENTRE_ID'] == $filter_data['SUGGESTED_CENTRE_ID']) ? ' selected="selected"' : '';}?>><?php echo $centre['CENTRE_NAME']; ?></option>
<?php } ?>
</select>
</div>
<div class="col-lg-4">
<label>Course Interested:</label>
<select class="form-control" name="COURSE_INTERESTED" id="COURSE_INTERESTED" value="">
<option value="">Select Course</option>
<?php foreach($courses as $course){ ?>
<option value="<?php echo $course['COURSE_NAME']; ?>" <?php if(isset($filter_data['COURSE_INTERESTED'])){ echo ($course['COURSE_NAME'] == $filter_data['COURSE_INTERESTED']) ? ' selected="selected"' : ''; }?>><?php echo $course['COURSE_NAME']; ?></option>
<?php } ?>
</select>
</div>
</div>
</div>
</div>
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-4">
<label>Is Out Station:</label>
<select name="ISOUTOFSTATION" class="form-control">
<option value=''>Please Select</option>
<option value="1" <?php if(isset($filter_data['ISOUTOFSTATION']) && $filter_data['ISOUTOFSTATION'] == '1'){echo 'selected="selected"';} ?>>Out Station</option>
<option value="0" <?php if(isset($filter_data['ISOUTOFSTATION']) && $filter_data['ISOUTOFSTATION'] == '0'){echo 'selected="selected"';} ?>>From Mumbai</option>
</select>
</div>
<div class="col-lg-4">
<label>Is Working:</label>
<select name="ISWORKING" class="form-control">
<option value=''>Please Select</option>
<option value="1" <?php if(isset($filter_data['ISWORKING']) && $filter_data['ISWORKING'] == '1'){echo 'selected="selected"';} ?>>Working</option>
<option value="0" <?php if(isset($filter_data['ISWORKING']) && $filter_data['ISWORKING'] == '0'){echo 'selected="selected"';} ?>>Not Working</option>
</select>
</div>
<div class="col-lg-4">
<label>Stream:</label>
<select name="STREAM" class="form-control">
<option value=''>Please Select</option>
<option value="Basics" <?php if(isset($filter_data['STREAM']) && $filter_data['STREAM'] == 'Basics'){echo 'selected="selected"';} ?>>Basics</option>
<option value="Programming" <?php if(isset($filter_data['STREAM']) && $filter_data['STREAM'] == 'Programming'){echo 'selected="selected"';} ?>>Programming</option>
<option value="Graphics & Animation" <?php if(isset($filter_data['STREAM']) && $filter_data['STREAM'] == 'Graphics & Animation'){echo 'selected="selected"';} ?>>Graphics & Animation</option>
<option value="Hardware & Networking" <?php if(isset($filter_data['STREAM']) && $filter_data['STREAM'] == 'Hardware & Networking'){echo 'selected="selected"';} ?>>Hardware & Networking</option>
</select>
</div>
</div>
</div>
</div>
<div class="form-group">
<div class="row" style="margin-left:-30px;">
<div class="col-lg-12" style="margin-left:0px;">
<div class="col-lg-6">
<button type="submit" value="submit" id="submit" class="btn btn-primary" style="float:right">Search</button>
</div>
<div class="col-lg-6">
<button type="submit" name="reset" value="reset" id="reset" class="btn btn-primary" style="float:left">Reset</button>
</div>
</div>
</div>
</div>
</div>
<?php echo form_close(); ?>
</div>
</div>
</div>
</div>
</div>

<!-- section for send sms functionality -->
<br/>
<div class="create_batch_form">
<div id="box">
<h2>Send Sms</h2></div>
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="col-md-5">
					<label>Message : </label>
					<textarea rows="4" cols="50" class="form-control" name="message_body" id="message_body_of_tele_enquiry_receipient_sms"></textarea>
				</div>
				<div class="col-md-5">
					<label>CC : </label>
					<br/><br/>
					<input type="text" class="form-control" name="cc_number" id="cc_number_for_tele_sms">
					<br/>
					<label id="sms_enquiry_receipient_count" style="color:red;"></label>
				</div>
				<div class="col-md-2">
					<br/>
					<input type="hidden" data-enquries_mobile_numbers="" id="enquries_mobile_numbers">
					<button type="submit" name="submit" value="submit" id="send_sms_to_selected_tele_enquries" class="btn btn-primary" style="float:left">Send</button>
					<br/><br/>
					<button type="reset" name="reset" value="reset" id="reset_sms_details" class="btn btn-primary" style="float:left">Reset</button>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- sms functionality end section -->

<br>
<?php if($this->session->userdata('filter_data')){?>
<div id="box"> <span class=""><?php //echo $pagination; ?></span>
<h2 class="text-center">
<label style="color:red;font-size:15px;">Search returned <?php echo $total_rows;?> records</label>
<?php if (!empty($filter_data)) { ?>
	<span>
		<a title="Export to Excel" target="_blank"  onClick ="$('#getEnquiryDetails').tableExport({type:'excel',escape:'false'});" class="btn" >
			<i class="fa fa-file-excel-o fa-2x text-success"></i>
    </a>
	</span>
<?php } ?>
</h2>
<!--        <div class="row"><div class="col-md-4 pull-right"></div></div>--></div>
<div class="panel panel-default">
<!--<div class="panel-heading">Form Elements</div>-->
<div class="panel-body table-responsive">
<table class="table table-bordered table-condensed table-stripped" id="getEnquiryDetails">
<thead>
<tr>
<th><label>Actions</label><input type="checkbox" class="form-control send_sms_to_all_enquiry" title="Select for send sms to all"></th>
<?php if(IsConvertToEnquiryAndWalkinPermitted){?><th>Convert to Enquiry</th><?php } ?>
<?php if(ConvertToEnquiryConversionAccessByRoleId){?><th>Convert to Enquiry</th><?php } ?>
<th>Add Follow Up</th>
<th>Enquiry Date</th>
<th>Name</th>
<th>Source</th>
<th>Mobile No.</th>
<th>Parent's No.</th>
<th>EMail ID.</th>
<th>Course Interested</th>
<th>Stream</th>
<th>Centre</th>
<th>Suggested Centre</th>
<th>Location</th>
<th>Handled By</th>
<th>Remark</th>
<th>Is Walkin</th>
<th>Is out of station</th>
<th>Enquiry ID</th>
<th>Is Enrolled</th>
<th>Admission ID</th>
<th>Preferred Timing</th>
<th>Last Tele FollowUp Date</th>
<th>Last Tele FollowUp Details</th>
<th>FollowUp By</th>
<th>Next FollowUp Date</th>
<th>Status</th>
<?php if(IsConvertToEnquiryAndWalkinPermitted){?><th>Convert to walkin</th><?php } ?>
<th>Add Followup</th>
</tr>
</thead>
<tbody>
                    <!-- <tr><td colspan="27" class="text-center" >Sorry Record Not Found</td></tr>	 -->

<?php
$count = $this->uri->segment(2) + 1;

// echo "<pre>";
 //print_r($tele_enquiries);
// echo "</pre>";
// exit();

foreach($tele_enquiries as $enquiry){
unset($enquiry['FOLLOWUP_DETAILS_ID']);

$followUpNotificationClass = '';
$nextFollowupDate = date("Y-m-d", strtotime($enquiry['NEXT_FOLLOWUP_DATE']));
if ($nextFollowupDate == date('Y-m-d')) {
	$followUpNotificationClass = 'warning';
}
else if($nextFollowupDate < date('Y-m-d')){
	$followUpNotificationClass = 'danger';
}

$enquiry_id = $this->encrypt->encode($enquiry['TELE_ENQUIRY_ID']);

?>
<tr class="<?php echo $followUpNotificationClass; ?>">
<td>
	<ul style="display:flex;list-style:none;">
		<li style="display:inline;"><input type="checkbox" class="form-control send_sms_to_enquiry" title="Select for send sms" data-mobile_no="<?php echo $enquiry['MOBILE'];?>" data-enquiry_id="<?php echo $enquiry_id;?>"></li>
		<li style="display:inline;"><a href="<?php echo base_url('edit-tele-enquiry/'.$enquiry_id); ?>"><span class="glyphicon glyphicon-edit" title="Update Tele Enquiries"></span></a></li>

	</ul>
	<!-- <a href="<?php// echo base_url('del-tele-enquiry/'.$enquiry_id) ?>" onclick="return confirm('Are you sure to delete this ?')"><span class="glyphicon glyphicon-trash"></span></a> -->
</td>
<?php if(IsConvertToEnquiryAndWalkinPermitted){?>
<td>
<?php if($enquiry['ENQUIRY_ID'] == null){ ?><a href="<?php echo base_url('tele-enquiry/convert-enquiry/'.$enquiry_id); ?>" title="Convert to walkin"><span class="glyphicon glyphicon-question-sign"></span></a><?php }else{echo 'Enquiry already Exists';}?>
</td>
<?php }?>
<?php if(ConvertToEnquiryConversionAccessByRoleId){?>
<td>
<?php if($enquiry['ENQUIRY_ID'] == null){ ?><a href="<?php echo base_url('tele-enquiry/convert-enquiry/'.$enquiry_id); ?>" title="Convert to walkin"><span class="glyphicon glyphicon-question-sign"></span></a><?php }else{echo 'Enquiry already Exists';}?>
</td>
<?php }?>
<td>
<a href="<?php $k="teleEnquiry"; echo base_url('tele-enquiry-follow-up/details/'.$enquiry_id."/".$k) ?>" target="_blank"><span class="glyphicon glyphicon-earphone" title="Follow up the Candidate"></span></a>
</td>
<?php
if($enquiry['FOLLOWUP_DATE'] == '0000-00-00 00:00:00' || $enquiry['FOLLOWUP_DATE'] == ''){
$FOLLOWUP_DATE = 'N/a';
}
else{
$FOLLOWUP_DATE = date("d/m/Y", strtotime($enquiry['FOLLOWUP_DATE']));
}

if($enquiry['NEXT_FOLLOWUP_DATE'] == '0000-00-00 00:00:00' || $enquiry['NEXT_FOLLOWUP_DATE'] == ''){
$NEXT_FOLLOWUP_DATE = 'N/a';
}
else{
$NEXT_FOLLOWUP_DATE = date("d/m/Y", strtotime($enquiry['NEXT_FOLLOWUP_DATE']));
}
if ($enquiry['FOLLOWED_UP'] == 0) {
	$enquiry['FOLLOWUP_DETAILS'] = 'To be followed up';
}
unset($enquiry['TELE_ENQUIRY_ID'],$enquiry['FOLLOWED_UP'], $enquiry['ISWORKING']);
foreach($enquiry as $key => $enq){

$ENQUIRY_DATE = date("d/m/Y", strtotime($enquiry['ENQUIRY_DATE']));
$enquiry['ENQUIRY_DATE'] = $ENQUIRY_DATE;

$enquiry['FOLLOWUP_DATE'] = $FOLLOWUP_DATE;
$enquiry['NEXT_FOLLOWUP_DATE'] = $NEXT_FOLLOWUP_DATE;

if(is_numeric($enquiry['LOCATION'])){
  if($enquiry['LOCATION'] == '1'){
    $enquiry['LOCATION'] = 'Out Station';
  }
  else{
    $enquiry['LOCATION'] = 'From Mumbai';
  }
}
?>
<td>
<?php echo $enquiry[$key];
if($enquiry['ISENROLLED'] == '0'){
	$enquiry['ISENROLLED'] = 'No';
}
else if($enquiry['ISENROLLED'] == '1'){
	$enquiry['ISENROLLED'] = 'Yes';
}

if($enquiry['WALKIN'] == '0' || $enquiry['WALKIN'] == NULL){
	$enquiry['WALKIN'] = 'No';
}
else if($enquiry['WALKIN'] == '1'){
	$enquiry['WALKIN'] = 'Yes';
}

if($enquiry['ISOUTOFSTATION'] == '0'){
	$enquiry['ISOUTOFSTATION'] = 'No';
}
else if($enquiry['ISOUTOFSTATION'] == '1'){
	$enquiry['ISOUTOFSTATION'] = 'Yes';
}

$courses_arr = explode(",",$enquiry['COURSE_INTERESTED']);
$courseNames = '';
echo '<ul class="list-group">';
for($j=0;$j<count($courses);$j++){
if(in_array($courses[$j]['COURSE_ID'],$courses_arr)){
$courseNames .= '<li class="list-group-item">'.$courses[$j]['COURSE_NAME'].'</li>';
$enquiry['COURSE_INTERESTED'] = $courseNames;
}

}
echo '</ul>';
?>
</td>
<?php  } ?>


<?php if(IsConvertToEnquiryAndWalkinPermitted){?>
<td>

<?php if($enquiry['WALKIN'] == 'No' || $enquiry['WALKIN'] == NULL){ ?><a href="<?php echo base_url('tele-enquiry/convert-to-walkin/').$enquiry_id; ?>">convert to walkin</a><?php }else{echo 'Already Walkin done.';}?></td>
<?php } ?>
<td>
<a href="<?php $k="teleEnquiry"; echo base_url('tele-enquiry-follow-up/details/'.$enquiry_id."/".$k) ?>" target="_blank"><span class="glyphicon glyphicon-earphone" title="Follow up the Candidate"></span></a>
</td>
</tr>
<?php $count++; } ?>
</tbody>
</table>
<br> </div>
<div class="row">
<div class="col-md-4 pull-right">
<?php echo $pagination; ?>
</div>
</div>
</div>
<?php } ?>
</div>
