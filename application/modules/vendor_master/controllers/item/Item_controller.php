<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item_controller extends MX_Controller {
    
     public function __construct()
        {
			$this->load->model('item/item_model');
            $this->load->model('vendor/vendor_model');
			parent::__construct();

        }
     public function getItemList($offset = 0){
		/* Pagination Starts */
        $config['base_url'] = base_url('add_item');
        $config['total_rows'] = $this->item_model->countItem();
        $config['per_page'] = 10;
        
        /*Pagination Styling Starts*/
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Styling Ends*/
        
        $this->pagination->initialize($config);
        /* Pagination Ends */
		 
       
		//print_r($data['items']);
		$data['pagination'] = $this->pagination->create_links();
        //$this->load->admin_view('item/add_item',$data);
     }
    
    public function addItem($offset = 0){

        $config['base_url'] = base_url('item/add-item');
        $config['total_rows'] = $this->item_model->getItemListCount();
        $config['per_page'] = 20;
        
          /*Pagination Styling Starts*/
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Styling Ends*/

        $this->pagination->initialize($config);
         $vendorItrsd = '';
           
        if(isset($_POST['VENDORS_SELECT'])){
            $vendorItrsd = implode(",", $_POST['VENDORS_SELECT']);
            $vendorItrsdArr = $_POST['VENDORS_SELECT'];
        }
        $this->form_validation->set_rules('ITEMNAME', 'ITEM NAME', 'required');
        $this->form_validation->set_rules('ITEMPRICE', 'ITEM PRICE', 'required');
        $this->form_validation->set_rules('MINSTOCK', 'MIN STOCK ENTRY', 'required');
        $this->form_validation->set_rules('MAXREQ', 'MAXREQ', 'required');
        if ($this->form_validation->run() != FALSE){
            $addItem = array(
                'ITEM_NAME' => $_POST['ITEMNAME'],
                'ITEM_DESCRIPTION' => $_POST['ITEMDESC'],
                'ITEM_PRICE' => $_POST['ITEMPRICE'],
                'ITEM_TYPE' => $_POST['ITEMTYPE'],
                'UNIT_ID' => $_POST['ITEMUNIT'],
                'OPENING_QTY' => $_POST['OPENQUAN'],
                'MIN_STOCK_LVL' => $_POST['MINSTOCK'],
                'MAX_QTY_FOR_REQUISITION' => $_POST['MAXREQ'],
                'CURRENT_STOCK_LVL' => $_POST['CURRSTOCK'],
                'EXCISE_DUTY' => $_POST['EXCDUTY'],
                'IS_TAX_APPLICABLE' => $this->input->post('ISTAXAPPLICABLE'),
                'IS_ACTIVE' => $this->input->post('ISACTIVE'),
                'VENDORS' =>  $vendorItrsd, 
                'IsRequisition' => '1'      
            );

            date_default_timezone_set("Asia/Kolkata");
            $addItem['MODIFIED_DATE'] = date("Y-m-d");

         
            $this->item_model->addItem($addItem);
			redirect(base_url('item/add-item'));
        }
		//$this->Item_model->getItemList();
        $data['vendor'] = $this->item_model->getVendors('','','');
        //$this->load->admin_view('item/add_item',$data);
        echo validation_errors();
        //$data['items'] = $this->item_model->getItemList();
        $data['units'] = $this->item_model->getUnits();
         $data['items'] = $this->item_model->getItemList('',$offset, $config['per_page']);

         // $vNames = '';
         // for($i = 0;$i < count($data['items']);$i++){
         //    $vIds =  explode(",",$data['items'][$i]['VENDORS']);
         //    $vNames[] = $this->item_model->vendorNames($vIds);
         // }

          // $data['vendords'] = $venArrays;

          $data['pagination'] = $this->pagination->create_links();

         $this->load->admin_view('item/add_item',$data);
    }
    

    public function updateItem($id){
		$itemId = $this->encrypt->decode($id);

         $vendorItrsd = '';

        if(isset($_POST['VENDORS_SELECT'])){
            $vendorItrsd = implode(",", $_POST['VENDORS_SELECT']);
            $vendorItrsdArr = $_POST['VENDORS_SELECT'];
        }
        $this->form_validation->set_rules('ITEMNAME', 'ITEM NAME', 'required');
        $this->form_validation->set_rules('ITEMPRICE', 'ITEM PRICE', 'required');
        $this->form_validation->set_rules('MINSTOCK', 'MIN STOCK ENTRY', 'required');
        $this->form_validation->set_rules('MAXREQ', 'MAXREQ', 'required');

        if ($this->form_validation->run() != FALSE){
            $updateItem = array(
                'ITEM_NAME' => $_POST['ITEMNAME'],
                'ITEM_DESCRIPTION' => $_POST['ITEMDESC'],
                'ITEM_PRICE' => $_POST['ITEMPRICE'],
                'ITEM_TYPE' => $_POST['ITEMTYPE'],
                'UNIT_ID' => $_POST['ITEMUNIT'],
                'OPENING_QTY' => $_POST['OPENQUAN'],
                'MIN_STOCK_LVL' => $_POST['MINSTOCK'],
                'MAX_QTY_FOR_REQUISITION' => $_POST['MAXREQ'],
                'CURRENT_STOCK_LVL' => $_POST['CURRSTOCK'],
                'EXCISE_DUTY' => $_POST['EXCDUTY'],
                'IS_TAX_APPLICABLE' => $this->input->post('ISTAXAPPLICABLE'),
                'IS_ACTIVE' => $this->input->post('ISACTIVE'),
                'VENDORS' =>  $vendorItrsd, 
                'IsRequisition' => '1'      
            );

            date_default_timezone_set("Asia/Kolkata");
            $updateItem['MODIFIED_DATE'] = date("Y-m-d");
         // $this->form_validation->set_rules('TAX_NAME', 'Tax Name', 'required');
        // $this->form_validation->set_rules('TAX_PERCENT', 'Tax Percent', 'required');
        // if ($this->form_validation->run() != FALSE){
        //     $updateItem = array(
        //         'TAX_NAME' => $_POST['TAX_NAME'],
        //         'TAX_PERCENT' => $_POST['TAX_PERCENT'],
        //         'TAX_TYPE' => $_POST['TAX_TYPE'],
        //         'TAX_DESC' => $_POST['TAX_DESC'],
        //     );
            $this->item_model->updateItem($itemId,$updateItem);
			redirect(base_url('item/add-item'));
        }
         $data['units'] = $this->item_model->getUnits();
         $data['vendor'] = $this->item_model->getVendors('','','');
		 $data['items'] = $this->item_model->getItemList($itemId,'','');

        $this->load->admin_view('item/edit_item',$data);
    }
    
    public function deleteItem($id){
        $itemId = $this->encrypt->decode($id);
        $this->item_model->deleteItem($itemId);
		redirect(base_url('item/view-item'));
    }   
    
}
