<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_order_controller extends MX_Controller {

     public function __construct()
        {
			$this->load->model('item/item_model');
            $this->load->model('vendor/vendor_model');
            $this->load->model('tele_enquiry/Tele_enquiry_model');
            $this->load->model('purchase_order/purchase_order_model');

            // $this->load->model('purchase_order/purchase_order_model');
			parent::__construct();
        }
    public function addPurchaseOrder($offset=0)
        {
            $data['vendors'] = $this->item_model->getVendors('','','');
            $data['centres'] = $this->Tele_enquiry_model->get_centres();
            $data['add_bel_cust_js'] = base_url('resources/js/purchase_order_cust.js');
            // print_r($data['vendors']);

        if(!empty($_POST))
        {
        $this->session->set_userdata('purchase_filter_data',$_POST);
        }
        else
        {

        $this->session->unset_userdata('purchase_filter_data');
        }
        if(isset($_POST['reset'])){
        $this->session->unset_userdata('purchase_filter_data');
        }

         $data['purchase_filter_data'] = $this->session->userdata('purchase_filter_data');
        /* Pagination starts */
        $config['base_url'] = base_url('purchase-order/add-purchase-order/');
        $config['total_rows'] = $this->purchase_order_model->purchaseData_count($data['purchase_filter_data']);
        $config['per_page'] = '10';

        /*Pagination Templating Starts*/
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Templating Ends*/

        $this->pagination->initialize($config);
        /* Pagination Ends */
        $data['purchaseData'] = $this->purchase_order_model->purchaseData($offset,$config['per_page'],$data['purchase_filter_data']);

        $data['pagination'] = $this->pagination->create_links();

        $purcOrdDateRep = str_replace('/', '-', $this->input->post('PO_DATE'));
        $purcOrdDate =  date("Y-m-d H:i", strtotime($purcOrdDateRep) );

        $delvDateRep = str_replace('/', '-', $this->input->post('DELIVERYDATE'));
        $delvDate =  date("Y-m-d H:i", strtotime($delvDateRep) );

        // echo "<pre>";
        // print_r($_POST);
        // echo "</pre>";
        $this->form_validation->set_rules('PO_DATE', 'PO Date', 'required');
        if($this->input->post('VENDORSELECT') == ''){
            $this->form_validation->set_rules('VENDORSELECT', 'Vendor', 'required');
        }
        if($this->input->post('VENDORSELECT') != '' && $this->input->post('ITEMLIST') == ''){
            $this->form_validation->set_rules('ITEMLIST', 'Item List', 'required',array('required' => 'Please Select Item'));
        }

        if($this->input->post('ITEMLIST') != '' && $this->input->post('used_at')){
            $this->form_validation->set_rules('USED_AT', 'Used At', 'required');
        }
        if($this->input->post('ITEMLIST') != '' && $this->input->post('order_qty')){
            $this->form_validation->set_rules('ORDER_QTY', 'Order Qyantity', 'required|numeric|greater_than[0]');
        }
        if($this->input->post('ITEMLIST') != '' && $this->input->post('item_rate')){
            $this->form_validation->set_rules('ITEM_RATE', 'Item Rate', 'required');
        }

        $this->form_validation->set_rules('PODETAILS', 'Purchase Delivery Details', 'required');
        $this->form_validation->set_rules('CATEGORY', 'Category', 'required');

        if ($this->form_validation->run() != FALSE)
        {
            $addPurchOrder = array(
                'PURCHASE_ORDER_DATE' => $purcOrdDate,
                'VENDOR_ID' => $this->encrypt->decode($this->input->post('VENDORSELECT')),
                'PURCHASE_ORDER_DELIVERY_DETAILS' => $this->input->post('PODETAILS'),
                'ITEM_CATEGORY' => $this->input->post('CATEGORY'),
                'TAX_AMOUNT' => $this->input->post('TAXPERCENT'),
                'FREIGHT_AMOUNT' => $this->input->post('FREIGHTAMOUNT'),
                'DELIVERY_DATE' => $delvDate,
                'PLACE_OF_DELIVERY' => $this->input->post('DELIVERYPLACE'),
                'PAYMENT_MODE' => 'As Per the discussion',
                'BILL_NUMBER' => $this->input->post('BILLNUMBER'),
                'CHALLAN_NUMBER' => $this->input->post('CHALANNO')
                );

            // if($this->input->post('TAX') == '1'){
            //     $addPurchOrder['ST_PER'] = $this->input->post('TAXPERCENT');
            // }
            // if ($this->input->post('TAX') == '2') {
            //     $addPurchOrder['VAT_PER'] = $this->input->post('TAXPERCENT');
            // }


            $tot_amt = 0;
            $indTaxAmtTot = 0;
            for ($i=0; $i <count($this->input->post('ITEM_ID')) ; $i++) {
                $tot_price = 0;
                $order_qty = $this->input->post('ORDER_QTY')[$i];
                $item_rate = $this->input->post('ITEM_RATE')[$i];
                $individual_gst = $this->input->post('SERV_TAX_PERC')[$i];

                //echo $tot_price.' '.$item_rate.' '.$individual_gst.'<br>';
                $tot_price = $order_qty * $item_rate;
                $tot_amt += $tot_price;
                $indTaxAmt = ($individual_gst/100) * $tot_price;
                $indTaxPerc = round($indTaxAmt,2);

                $indTaxAmtTot += $indTaxPerc;

                $addPurchOrderTransDet[] = array(
                    'ITEM_ID' => $this->input->post('ITEM_ID')[$i],
                    'ITEM_DESCRIPTION' => $this->input->post('ITEM_DESC')[$i],
                    'ITEM_ORDER_QUANTITY' => $this->input->post('ORDER_QTY')[$i],
                    'ITEM_UNIT_RATE' => $this->input->post('ITEM_RATE')[$i],
                    'ST_PER' => $this->input->post('SERV_TAX_PERC')[$i],
                    'ITEM_USED_AT' => $this->input->post('USED_AT')[$i],
                    'TOTAL_PRICE' => $tot_price
                    );


            }

            $addPurchOrder['TOTAL_AMOUNT_EXCLUSIVE_TAX'] = $tot_amt;
            $addPurchOrder['GRAND_TOTAL'] = $tot_amt + $indTaxAmtTot;
            // echo "<pre>";
            // print_r($addPurchOrder);
            // echo "</pre>";
            $purchaseOrderMainTransId = $this->purchase_order_model->addPurchOrder($addPurchOrder);



            for ($j=0; $j <count($addPurchOrderTransDet) ; $j++) {
                $addPurchOrderTransDet[$j]['PURCHASE_ORDER_MAIN_TRANSACTION_ID'] = $purchaseOrderMainTransId;
                $this->purchase_order_model->addPurchOrderTransDet($addPurchOrderTransDet[$j]);
            }
            redirect(base_url('purchase-order/add-purchase-order'));
        }

        $this->load->admin_view('purchase_order/purchase_order',$data);

        }

    public function purchaseOrder(){
        $itemId = $this->input->post('itemId');
        $products = $this->purchase_order_model->purchaseOrder($itemId);
        echo json_encode($products);
    }

    public function purchaseOrderItem(){
        $encvendId = $this->input->post('vendId');
        $vendId = $this->encrypt->decode($encvendId);
        $products = $this->purchase_order_model->purchaseOrderItem($vendId);
        echo json_encode($products);
    }

    public function printPurchOrder($encPoId){
        $poId = $this->encrypt->decode($encPoId);
        $data['vendorDets'] = $this->purchase_order_model->printPurchOrdMainTrans($poId);
        $data['products'] = $this->purchase_order_model->printPurcOrdMainTransDet($poId);
        // echo "<pre>";
        // print_r($data['vendorDets']);
        // echo "</pre>";
        // exit();
        if(!empty($data['vendorDets'])){
            $this->load->helper('currency');
            $this->load->helper('pdf_helper');
            $this->load->view('purchase_order/purch_order_print',$data);
        }
        else{
            echo 'This vendor does not exist';
        }
    }

   }
?>
