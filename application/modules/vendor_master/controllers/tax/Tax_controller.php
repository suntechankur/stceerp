<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tax_controller extends MX_Controller {
    
     public function __construct()
        {
			$this->load->model('tax/tax_model');
			parent::__construct();
        }
     public function getTaxList($offset = 0){
		/* Pagination Starts */
        $config['base_url'] = base_url('view_tax');
        $config['total_rows'] = $this->tax_model->countTax();
        $config['per_page'] = 10;
        
        /*Pagination Styling Starts*/
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Styling Ends*/
        
        $this->pagination->initialize($config);
        /* Pagination Ends */
		 
        $data['taxs'] = $this->tax_model->getTaxList('',$offset,$config['per_page']);
		//print_r($data['taxs']);
		$data['pagination'] = $this->pagination->create_links();
        $this->load->admin_view('tax/view_tax',$data);
     }
    
    public function addTax(){
        $this->form_validation->set_rules('TAX_NAME', 'Tax Name', 'required');
        $this->form_validation->set_rules('TAX_PERCENT', 'Tax Percent', 'required');
        //$this->form_validation->set_rules('TAX_TYPE', 'Tax Type', 'required');
        //$this->form_validation->set_rules('TAX_DESC', 'Tax Description', 'required');
        if ($this->form_validation->run() != FALSE){
            $addTax = array(
                'TAX_NAME' => $_POST['TAX_NAME'],
                'TAX_PERCENT' => $_POST['TAX_PERCENT'],
                'TAX_TYPE' => $_POST['TAX_TYPE'],
                'TAX_DESC' => $_POST['TAX_DESC'],
            );
            $this->tax_model->addTax($addTax);
			redirect(base_url('tax/view-tax'));
        }
		$this->getTaxList($offset = 0);
    }
    
    public function updateTax($id){
		$taxId = $this->encrypt->decode($id);
        $this->form_validation->set_rules('TAX_NAME', 'Tax Name', 'required');
        $this->form_validation->set_rules('TAX_PERCENT', 'Tax Percent', 'required');
        //$this->form_validation->set_rules('TAX_TYPE', 'Tax Type', 'required');
        //$this->form_validation->set_rules('TAX_DESC', 'Tax Description', 'required');
        if ($this->form_validation->run() != FALSE){
            $updateTax = array(
                'TAX_NAME' => $_POST['TAX_NAME'],
                'TAX_PERCENT' => $_POST['TAX_PERCENT'],
                'TAX_TYPE' => $_POST['TAX_TYPE'],
                'TAX_DESC' => $_POST['TAX_DESC'],
            );
            $this->tax_model->updateTax($taxId,$updateTax);
			redirect(base_url('tax/view-tax'));
        }
		$data['taxs'] = $this->tax_model->getTaxList($taxId,'','');
        $this->load->admin_view('tax/edit_tax',$data);
    }
    
    public function deleteTax($id){
        $taxId = $this->encrypt->decode($id);
        $this->tax_model->deleteTax($taxId);
		redirect(base_url('tax/view-tax'));
    }
}
