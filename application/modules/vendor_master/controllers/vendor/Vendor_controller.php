<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor_controller extends MX_Controller {
    
     public function __construct()
        {
                $this->load->model('vendor/vendor_model');
                parent::__construct();
        }
  //    public function getVendorList($offset = 0){
		// /* Pagination Starts */
  //       $config['base_url'] = base_url('view_vendor');
  //       $config['total_rows'] = $this->vendor_model->countVendor();
  //       $config['per_page'] = 10;
        
  //       /*Pagination Styling Starts*/
  //       $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
  //       $config['full_tag_close'] = '</ul>';
  //       $config['first_link'] = false;
  //       $config['last_link'] = false;
  //       $config['first_tag_open'] = '<li>';
  //       $config['first_tag_close'] = '</li>';
  //       $config['prev_link'] = '&laquo';
  //       $config['prev_tag_open'] = '<li class="prev">';
  //       $config['prev_tag_close'] = '</li>';
  //       $config['next_link'] = '&raquo';
  //       $config['next_tag_open'] = '<li>';
  //       $config['next_tag_close'] = '</li>';
  //       $config['last_tag_open'] = '<li>';
  //       $config['last_tag_close'] = '</li>';
  //       $config['cur_tag_open'] = '<li class="active"><a href="#">';
  //       $config['cur_tag_close'] = '</a></li>';
  //       $config['num_tag_open'] = '<li>';
  //       $config['num_tag_close'] = '</li>';
  //       /*Pagination Styling Ends*/
        
  //       $this->pagination->initialize($config);
  //       /* Pagination Ends */
		 
  //       $data['vendors'] = $this->vendor_model->getVendorList('',$offset,$config['per_page']);
		// $this->load->model('tax/tax_model');
		// $data['applicable_tax'] = $this->vendor_model->getTaxList();
		// //print_r($data['vendors']);
		// $data['pagination'] = $this->pagination->create_links();
  //       $this->load->admin_view('vendor/view_vendor',$data);
  //    }
    
    public function addVendor($offset = 0){
        $this->form_validation->set_rules('VENDOR_NAME', 'Vendor Name', 'required');
        $this->form_validation->set_rules('COMPANY_NAME', 'Company Name', 'required');
        $this->form_validation->set_rules('TEL', 'Telephone', 'required');
        $this->form_validation->set_rules('MOBILE', 'Mobile No.', 'min_length[10]|max_length[10]|numeric');
		$this->form_validation->set_rules('ADDRESS', 'Address', 'required');
        //$this->form_validation->set_rules('FAX_NO', 'Fax No', 'required');
        $this->form_validation->set_rules('EMAIL1', 'Email ID', 'valid_email');
        $this->form_validation->set_rules('EMAIL2', 'Email ID', 'valid_email');
        //$this->form_validation->set_rules('VENDOR_TYPE', 'Vendor Type', 'required');
        //$this->form_validation->set_rules('INCOME_TAX_NUMBER', 'Income Tax Number', 'required');
        //$this->form_validation->set_rules('CST_NUMBER', 'CST Number', 'required');
        //$this->form_validation->set_rules('SERVICE_TAX_NUMBER', 'Service Tax Number', 'required');
        //$this->form_validation->set_rules('ECC_NUMBER', 'ECC Number', 'required');
        //$this->form_validation->set_rules('VAT_TIN_NUMBER', 'VAT/TIN Number', 'required');
        //$this->form_validation->set_rules('TAX_ID', 'TAX ID', 'required');
        if ($this->form_validation->run() != FALSE){
			$lastid = $this->vendor_model->getVendorLastId();
			$lastid=$lastid[0]['id']+1;
			$lastid = 'STVEND'.$lastid;
            $addVendor = array(
				'VENDORID' => $lastid,
                'VENDOR_NAME' => $_POST['VENDOR_NAME'],
                'COMPANY_NAME' => $_POST['COMPANY_NAME'],
                'ADDRESS' => $_POST['ADDRESS'],
                'TEL' => $_POST['TEL'],
                'MOBILE' => $_POST['MOBILE'],
                'FAX_NO' => $_POST['FAX_NO'],
				'EMAIL1' => $_POST['EMAIL1'],
				'EMAIL2' => $_POST['EMAIL2'],
				'VENDOR_TYPE' => $_POST['VENDOR_TYPE'],
				'INCOME_TAX_NUMBER' => $_POST['INCOME_TAX_NUMBER'],
				'CST_NUMBER' => $_POST['CST_NUMBER'],
				'SERVICE_TAX_NUMBER' => $_POST['SERVICE_TAX_NUMBER'],
				'ECC_NUMBER' => $_POST['ECC_NUMBER'],
				'VAT_TIN_NUMBER' => $_POST['VAT_TIN_NUMBER'],
				'TAX_ID' => $_POST['TAX_ID'],
                'IS_ACTIVE' => '1'
            );
			$addVendor['MODIFIED_DATE'] = date("Y-m-d");
			
            $this->vendor_model->addVendor($addVendor);
			redirect(base_url('vendor/add-vendor'));
        }
		// $this->getVendorList($offset = 0);
        /* Pagination Starts */
        $config['base_url'] = base_url('vendor/add-vendor');
        $config['total_rows'] = $this->vendor_model->countVendor();
        $config['per_page'] = 10;
        
        /*Pagination Styling Starts*/
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        /*Pagination Styling Ends*/
        
        $this->pagination->initialize($config);
        /* Pagination Ends */

        $data['applicable_tax'] = $this->vendor_model->getTaxList();
        $data['vendors'] = $this->vendor_model->getVendorList('',$offset,$config['per_page']);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->admin_view('vendor/add_vendor',$data);
    }
    
    public function updateVendor($id){
		$vendorId = $this->encrypt->decode($id);
		//echo $vendorId;
        $this->form_validation->set_rules('VENDOR_NAME', 'Vendor Name', 'required');
        $this->form_validation->set_rules('COMPANY_NAME', 'Company Name', 'required');
        $this->form_validation->set_rules('TEL', 'Telephone', 'required');
        //$this->form_validation->set_rules('MOBILE', 'Mobile No.', 'required|min_length[10]|max_length[10]|numeric');
		    $this->form_validation->set_rules('ADDRESS', 'Address', 'required');
        //$this->form_validation->set_rules('FAX_NO', 'Fax No', 'required');
        //$this->form_validation->set_rules('EMAIL1', 'Email ID', 'required|valid_email');
        //$this->form_validation->set_rules('EMAIL2', 'Email ID', 'required|valid_email');
        //$this->form_validation->set_rules('VENDOR_TYPE', 'Vendor Type', 'required');
        //$this->form_validation->set_rules('INCOME_TAX_NUMBER', 'Income Tax Number', 'required');
        //$this->form_validation->set_rules('CST_NUMBER', 'CST Number', 'required');
        //$this->form_validation->set_rules('SERVICE_TAX_NUMBER', 'Service Tax Number', 'required');
        //$this->form_validation->set_rules('ECC_NUMBER', 'ECC Number', 'required');
        //$this->form_validation->set_rules('VAT_TIN_NUMBER', 'VAT/TIN Number', 'required');
        //$this->form_validation->set_rules('TAX_ID', 'TAX ID', 'required');
        if ($this->form_validation->run() != FALSE){
            $updateVendor = array(
                'VENDOR_NAME' => $_POST['VENDOR_NAME'],
                'COMPANY_NAME' => $_POST['COMPANY_NAME'],
                'ADDRESS' => $_POST['ADDRESS'],
                'TEL' => $_POST['TEL'],
                'MOBILE' => $_POST['MOBILE'],
                'FAX_NO' => $_POST['FAX_NO'],
				'EMAIL1' => $_POST['EMAIL1'],
				'EMAIL2' => $_POST['EMAIL2'],
				'VENDOR_TYPE' => $_POST['VENDOR_TYPE'],
				'INCOME_TAX_NUMBER' => $_POST['INCOME_TAX_NUMBER'],
				'CST_NUMBER' => $_POST['CST_NUMBER'],
				'SERVICE_TAX_NUMBER' => $_POST['SERVICE_TAX_NUMBER'],
				'ECC_NUMBER' => $_POST['ECC_NUMBER'],
				'VAT_TIN_NUMBER' => $_POST['VAT_TIN_NUMBER'],
				'TAX_ID' => $_POST['TAX_ID'],
                
            );
			date_default_timezone_set("Asia/Kolkata");
			$updateVendor['MODIFIED_DATE'] = date("Y-m-d");
			//$updateVendor['MODIFIED_BY'] = $this->session->userdata('admin_data');
			if(isset($_POST['IS_ACTIVE'])){
				$updateVendor['IS_ACTIVE'] = '1';
			}
			//print_r($updateVendor);
			//exit;
            $this->vendor_model->updateVendor($vendorId,$updateVendor);
			redirect(base_url('vendor/add-vendor'));
        }
		$data['vendors'] = $this->vendor_model->getVendorList($vendorId,'','');
        $this->load->admin_view('vendor/edit_vendor',$data);
    }
    
    public function deleteVendor($id){
        $vendorId = $this->encrypt->decode($id);
        $this->vendor_model->deleteVendor($vendorId);
		redirect(base_url('vendor/add-vendor'));
    }


}
