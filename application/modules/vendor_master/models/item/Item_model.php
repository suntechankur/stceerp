<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item_model extends CI_Model {
    public function getItemList($id,$offset,$per_page){
      $query = $this->db->select('IM.ITEM_ID,IM.ITEM_DESCRIPTION,IM.ITEM_NAME,IM.ITEM_PRICE,IM.ITEM_TYPE,UM.UNIT_NAME AS UNIT,IM.OPENING_QTY,IM.MIN_STOCK_LVL,IM.MAX_QTY_FOR_REQUISITION,IM.CURRENT_STOCK_LVL,IM.EXCISE_DUTY,IM.IS_TAX_APPLICABLE,IM.IS_ACTIVE,IM.VENDORS')
                        ->from('item_master IM')
                        ->join('unit_master UM','IM.UNIT_ID = UM.UNIT_ID','left');
                      
     if($id != 0){    
            $query = $this->db->where('ITEM_ID',$id);   
       }
      else{
            $query = $this->db->limit($per_page,$offset)
                              ->order_by('IM.ITEM_ID','desc');
      
        }

        $query = $this->db->get();   
        return $query->result_array();
    } 

    public function getItemListCount(){
      $query = $this->db->select('IM.ITEM_ID')
                        ->from('item_master IM');
                        // ->join('unit_master UM','IM.UNIT_ID = UM.UNIT_ID','left');
		  $query = $this->db->get();
        return $query->num_rows();
    }
	
  public function getVendors($id=0,$offset,$per_page){
        $query = $this->db->select('VM.ID,VM.VENDOR_NAME')
                          ->from('vendor_master VM')
                          ->where('IS_ACTIVE','1')
                          ->order_by('VM.VENDOR_NAME','ASC')
                          ->get();
        return $query->result_array();
    }

	public function countItem()
    {
		$query = $this->db->select('ID')
                          ->from('item_master')
                          ->get();
        return $query->num_rows();
    }
	
	public function getItemLastId(){
		$query = $this->db->select_max('id')
						  ->get('item_master');
        return $query->result_array();
    }
	
    public function addItem($addItem){
    	$this->db->insert('item_master',$addItem);
    }
	
    public function updateItem($id,$updateItem){
    	$this->db->set($updateItem)
    			 ->where('item_id',$id)
    			 ->update('item_master');
    }
	
	public function deleteItem($itemId){
        $this->db->where_in('id',$itemId)
                 ->delete('item_master');
    }

    // public function getItemList(){
    //    $query = $this->db->get('item_master');
                         
    //         return $query->result_array();       
    // }

    public function getUnits(){
       $query = $this->db->select('UM.UNIT_ID,UM.UNIT_NAME')
                          ->from('unit_master UM')
                          ->where('IS_ACTIVE','1')
                          ->get();
        return $query->result_array();
      }

      public function vendorNames($vIds)
      {
        $query = $this->db->select('ID,VENDOR_NAME')
                          ->from('vendor_master')
                          ->where_in('ID',$vIds)
                          ->get();
        return $query->result_array();

      }

}
