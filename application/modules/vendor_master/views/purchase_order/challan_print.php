<?php 

tcpdf();
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 006');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);

// ---------------------------------------------------------

// add a page
$pdf->AddPage();

// create some HTML content

$html =
'<form name="Form1" method="post" action="Delivary_Challan.aspx?reqid=8549" id="Form1">

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="64B5CDE5" />
</div>
			<TABLE class="normal" id="Table4" cellSpacing="0" cellPadding="3" width="700" align="left"
				border="0" style="BORDER-RIGHT: black 1px solid; BORDER-TOP: black 1px solid; BORDER-LEFT: black 1px solid; BORDER-BOTTOM: black 1px solid">
				<TR>
					<TD align="left" colSpan="8">
						<P class="Header1" align="center">St.AngelosComputer Education</P>
					</TD>
				</TR>
				<TR>
					<TD style="bottom-botton: solid 1px black" align="center" colSpan="8">H.O:-6th 
						Floor, Jyoti Plaza, Above TATA Motors, S.V.Road,<BR>
						Kandivli[w],Mumbai-400 067 . Tel:-022-28617575-78/28657525-27<BR>
						Website :-www.saintangelos.com E-mail:-info@saintangelos.com</TD>
				</TR>
				<TR>
					<TD align="left" colSpan="8">
						<P class="Header1" align="center"><U>Delivery Challan</U></P>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 14px" align="right"><STRONG></STRONG></TD>
					<TD align="left"><STRONG>To:</STRONG><span id="lblCentre">GOREGAON(E)</span></TD>
					<TD style="WIDTH: 139px" align="right" colSpan="5"><STRONG></STRONG></TD>
					<TD align="left"><STRONG>Challan No:</STRONG><span id="lblChallan" style="font-weight:bold;">14158</span></TD>
				</TR>
				<TR>
					<TD class="]" style="WIDTH: 14px" align="right"><STRONG></STRONG></TD>
					<TD align="left"><STRONG>Through:</STRONG><span id="lblThrough">Delivered</span></TD>
					<TD style="WIDTH: 139px" align="right" colSpan="5">&nbsp;</TD>
					<TD align="left"><STRONG>Date:</STRONG><span id="lblIssuedDt">03/02/2017</span></TD>
				</TR>
				<TR>
					<TD class="Header2" align="left" colSpan="8">
						<HR width="100%" color="black">
					</TD>
				</TR>
				<TR>
					<TD align="center" colSpan="8"><table cellspacing="0" cellpadding="3" align="Center" rules="all" border="1" id="dgReqChallan" style="width:464px;border-collapse:collapse;">
	<tr class="Header1" style="border-color:Black;border-width:1px;border-style:Solid;">
		<td>Sr. No.</td><td>Description</td><td>Quantity </td>
	</tr><tr class="GridItem">
		<td>1</td><td>100 PAGES REGISTER</td><td>1</td>
	</tr><tr class="GridItem">
		<td>2</td><td>200 PAGES REGISTER</td><td>2</td>
	</tr><tr class="GridItem">
		<td>3</td><td>A4 SIZE PLAIN PAPER</td><td>25</td>
	</tr><tr class="GridItem">
		<td>4</td><td>ANSWER SHEET</td><td>20</td>
	</tr><tr class="GridItem">
		<td>5</td><td>ASSIGNMENT FOLDER</td><td>1</td>
	</tr><tr class="GridItem">
		<td>6</td><td>ATTEND SHEET/BATCH CONF.REPORT</td><td>1</td>
	</tr><tr class="GridItem">
		<td>7</td><td>BOX FILE SMALL</td><td>2</td>
	</tr><tr class="GridItem">
		<td>8</td><td>CASH PAYMENT VOUCHER PAD</td><td>1</td>
	</tr><tr class="GridItem">
		<td>9</td><td>CELLO TAPE</td><td>1</td>
	</tr><tr class="GridItem">
		<td>10</td><td>ENQUIRY FORM PAD</td><td>1</td>
	</tr><tr class="GridItem">
		<td>11</td><td>FEVI STICK</td><td>1</td>
	</tr><tr class="GridItem">
		<td>12</td><td>Jacket Prospectus</td><td>20</td>
	</tr><tr class="GridItem">
		<td>13</td><td>MARKER</td><td>5</td>
	</tr><tr class="GridItem">
		<td>14</td><td>MOUSE PAD</td><td>15</td>
	</tr><tr class="GridItem">
		<td>15</td><td>PENCIL</td><td>2</td>
	</tr><tr class="GridItem">
		<td>16</td><td>RJ 45</td><td>5</td>
	</tr><tr class="GridItem">
		<td>17</td><td>SCALE PLASTIC</td><td>1</td>
	</tr><tr class="GridItem">
		<td>18</td><td>SCISSOR</td><td>1</td>
	</tr><tr class="GridItem">
		<td>19</td><td>SHARPENER</td><td>1</td>
	</tr><tr class="GridItem">
		<td>20</td><td>SPRING FILE</td><td>5</td>
	</tr><tr class="GridItem">
		<td>21</td><td>STAMP PAD</td><td>1</td>
	</tr><tr class="GridItem">
		<td>22</td><td>STAMP PAD INK</td><td>1</td>
	</tr><tr class="GridItem">
		<td>23</td><td>STAPLER PINS (SMALL) BOX</td><td>1</td>
	</tr><tr class="GridItem">
		<td>24</td><td>WHITE BOARD DUSTER</td><td>1</td>
	</tr><tr class="GridItem">
		<td>25</td><td>WHITE ENVELOPS </td><td>20</td>
	</tr><tr class="GridItem">
		<td>26</td><td>WHITE INK</td><td>1</td>
	</tr>
</table></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 14px; HEIGHT: 19px"></TD>
					<TD style="HEIGHT: 19px" vAlign="middle" align="left" height="19"></TD>
					<TD style="WIDTH: 139px; HEIGHT: 19px" vAlign="middle" align="left" colSpan="5" height="19">&nbsp;</TD>
					<TD style="HEIGHT: 19px" vAlign="middle" align="left" height="20"></TD>
				</TR>
				<TR>
					<td style="WIDTH: 14px; HEIGHT: 19px"></td>
					<TD style="HEIGHT: 19px" vAlign="middle" align="left" height="19">__________________</TD>
					<TD style="WIDTH: 139px; HEIGHT: 19px" vAlign="middle" align="left" colSpan="5" height="19"></TD>
					<TD style="HEIGHT: 19px" vAlign="middle" align="left" height="20">___________________</TD>
				</TR>
				<TR>
					<td style="WIDTH: 14px"></td>
					<TD vAlign="middle" align="left" height="20"><STRONG>Receivers Signature</STRONG></TD>
					<TD vAlign="middle" align="center" colSpan="5" height="20" style="WIDTH: 139px"><STRONG>(LOG-F-02)</STRONG></TD>
					<TD vAlign="middle" align="left" height="20"><STRONG>Authorised Signature</STRONG></TD>
				</TR>
			</TABLE>'
		

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();




// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_006.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
?>