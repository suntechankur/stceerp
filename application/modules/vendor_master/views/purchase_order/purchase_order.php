<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script> -->

<script type="text/javascript">
	$(document).ready(function(){
        $('')
});
</script>

<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
<div class="create_batch_form">
    <div id="box">
        <h2>Purchase Order</h2>
        <?php
			if($this->session->flashdata('msg'))
			{
				$msg = $this->session->flashdata('msg');
				echo $msg['msg'];
			}
		?>
	</div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align:center; color:#F00;"></div>
                <div class="panel-body">
                   <?php
                    $attr = array('role'=>'form');
                    echo form_open('',$attr); ?>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                  <div class="col-lg-12">
									<div class="col-lg-4">
										<label>PO Date: <span class="text-danger">*</span></label>
										 <input type="text" id="PO_DATE" class="form-control enquiry_date" name="PO_DATE" value="">
										<span class="text-danger"><?php echo form_error('PO_DATE'); ?></span>
									</div>
									<div class="col-lg-8 style="padding-left:70px;padding-bottom:10px">
										<label><p>&nbsp;</p></label><br />
										&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="TAX" value="1" checked class="servTax"><strong>GST</strong> Choose GST as per product applicable
									    <span class="text-danger"><?php echo form_error('TAX'); ?></span>
									</div>
								  </div>
								</div>
								<div class="row">
								  <div class="col-lg-12">
								  	<div class="col-lg-12">
										<label>Purchase Delivery Details: <span class="text-danger">*</span></label>
										<textarea class="form-control" name="PODETAILS" id="PODETAILS"></textarea>
										<span class="text-danger"><?php echo form_error('PODETAILS'); ?></span>
									</div>
								  </div>
								</div>
								<div class="row">
								  <div class="col-lg-12">
									<div class="col-lg-4">
										<label>Enter Category: <span class="text-danger">*</span></label>
										<input class="form-control" name="CATEGORY" id="CATEGORY" value="" type="text">
										<span class="text-danger"><?php echo form_error('CATEGORY'); ?></span>
									</div>
									<div class="col-lg-4" style="float:right">
										<label>Vendor: <span class="text-danger">*</span></label>
										<select class="form-control vendNames" name="VENDORSELECT" id="VENDORSELECT" >
											<option value="">Select Vendor</option>
												<?php foreach($vendors as $vendor) { ?>
		                                     <option value="<?php echo $this->encrypt->encode($vendor['ID']); ?>"  ><?php echo $vendor['VENDOR_NAME']; ?>

		                                     </option>
		                                        <?php } ?>
		                                 </select>
										<span class="text-danger"><?php echo form_error('VENDORSELECT'); ?></span>
										<span class="text-danger"><?php echo form_error('ITEMLIST'); ?></span>
										<span class="text-danger"><?php echo form_error('used_at'); ?></span>
										<span class="text-danger"><?php echo form_error('order_qty'); ?></span>
										<span class="text-danger"><?php echo form_error('item_rate'); ?></span>
									</div>
									<div class="col-lg-4">
									    <label>TAX AMOUNT : </label>
									    <input class="form-control" name="TAXPERCENT" id="TAXPERCENT" value="0.00" type="text">
									</div>
								  </div>
								</div>
								<div class="row">
								  <div class="col-lg-12">
									<div class="col-lg-4">
										<label>Other Tax Amount:</label>
										<input class="form-control" name="OTHERTAXAMOUNT" id="OTHERTAXAMOUNT" value="" type="text">
										<span class="text-danger"><?php echo form_error('OTHERTAXAMOUNT'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Freight Amount: </label>
										<input class="form-control" name="FREIGHTAMOUNT" id="FREIGHTAMOUNT" type="text" readonly="readonly" value="0.00">
										<span class="text-danger"><?php echo form_error('FREIGHTAMOUNT'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Delivery Date: </label>
										 <input type="text" id="DELIVERYDATE" class="form-control enquiry_date" name="DELIVERYDATE" value="">
										<span class="text-danger"><?php echo form_error('DELIVERYDATE'); ?></span>
									</div>
								  </div>
								</div>
								<div class="row">
								  <div class="col-lg-12">
									<div class="col-lg-4">
										<label>Place of Delivery: </label>
										<select name="DELIVERYPLACE" class="form-control">
                                                    <option value=''>Please Select</option>
                                                    <?php foreach($centres as $centre){ ?>
                                                    <option value="<?php echo $centre['CENTRE_ID'] ?>" ><?php echo $centre['CENTRE_NAME'] ?></option>
                                                    <?php } ?>
                                                </select>
										<span class="text-danger"><?php echo form_error('DELIVERYPLACE'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>Bill Number: </label>
										<input class="form-control" name="BILLNUMBER" id="BILLNUMBER" type="text">
										<span class="text-danger"><?php echo form_error('BILLNUMBER'); ?></span>
									</div>
									<div class="col-lg-4">
										<label>CHALAN NUMBER:</label>
										<input class="form-control" name="CHALANNO" id="CHALANNO" type="text">
									</div>
								  </div>
								</div>

									</div>
                                </div>
                                <div class="purchOrderData"></div>
                            </div>
							<br />
                            <button type='submit' class='btn btn-primary' style="margin-left:500px">Save</button>&nbsp;
                        </div>

                  <?php echo form_close(); ?>
                </div>
            </div>
        </div>

        <div id="box"> <span class=""><?php echo $pagination; ?></span>
        	<h2 class="text-center"> Purchase Order List</h2>
        	<div class="panel panel-default">
        		<div class="panel-body table-responsive">
        			<table class="table table-bordered table-condensed table-stripped">
        			 <thead>
        			 	<tr>
        			 		<th>Action</th>
        					<th>PO No.</th>
        					<th>PO Date</th>
        					<th>Vendor Name</th>
        					<th>PO Amount</th>
        				</tr>
        			 </thead>
        			 <tbody>
        			 <?php
        			 foreach ($purchaseData as $key => $value) {

        			 ?>
        			 	<tr>
        			 		<td>

	        			 		<a target="_blank" href="<?php echo base_url('purchase-order/printPurchOrder/'.$this->encrypt->encode($value['PURCHASE_ORDER_MAIN_TRANSACTION_ID'])); ?>"><span class="glyphicon glyphicon-print"></span></a>
        			 		</td>
        			 		<td><?php echo $value['PURCHASE_ORDER_MAIN_TRANSACTION_ID']?></td>
        			 		<td><?php echo date("d/m/Y",strtotime($value['PURCHASE_ORDER_DATE']));?></td>
        			 		<td><?php echo $value['VENDOR_NAME']?></td>
        			 		<td><?php echo $value['GRAND_TOTAL']?></td>

        			 	</tr>
        			 	<?php } ?>
        			 </tbody>
        			</table>
        		</div>
        	</div>
        	<?php echo $pagination; ?>
        <!--        <div class="row"><div class="col-md-4 pull-right"></div></div>--></div>
    </div>
</div>


<!-- Receipts Modal Starts -->
<div id="poModal" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-lg">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <h4 class="modal-title">Purchase Order Details</h4>
		      </div>
		      <div class="modal-body vendor_items">
		       <select name="ITEMLIST" id="ITEMLIST" class="item_list form-control"></select>
		      	<table class="table table-striped">
		      		<thead>
		      			<tr>
		      				<td>Item name</td>
		      				<td>Item used at</td>
		      				<td>Item description</td>
		      				<td>Order Quantity</td>
		      				<td>Item unit</td>
		      				<td>Item rate</td>
		      				<td>Total cost</td>
		      				<td>Service tax percent</td>
		      				<td>VAT percent</td>
		      				<td>Individual tax amount</td>
		      			</tr>
		      		</thead>
		      		<tbody class="vendItemTable"></tbody>
		      	</table>
		      	<div class="row">
		      		<div class="col-md-12">
		      			<span class="pull-right">Total Amount Total: <span class="totalAmount">0.00</span></span><br>
		      			<span class="pull-right">Individual Tax Amount Total: <span class="indTaxAmount">0.00</span></span><br>
		      			<span class="pull-right">Grand Total: <span class="grandTot">0.00</span></span>
		      		</div>
		      	</div>


		      </div>
		      <div class="modal-footer">
		      	<div class="col-md-2">
		      		<button class="btn btn-primary savePurchOrder" type="button" data-dismiss="modal">Save</button>
		      	</div>
		        <div class="col-md-2">
					<button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
		        </div>
              </div>
		    </div>
		  </div>
</div>
