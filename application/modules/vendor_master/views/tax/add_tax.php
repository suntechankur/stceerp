<div class="gapping"></div>
<div class="create_batch_form">
    <div id="box">
        <h2>Add Vendor</h2>
        <?php
			if($this->session->flashdata('msg'))
			{
				$msg = $this->session->flashdata('msg');
				echo $msg['msg'];
			}
		?>
	</div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="text-align:center; color:#F00;"></div>
                <div class="panel-body">
                   <?php 
                    $attr = array('role'=>'form');
                    echo form_open('',$attr); ?>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
									<div class="col-lg-6">
										<label>Full Name: <span class="text-danger">*</span></label>
										<input class="form-control" name="FULLNAME" value="" id="FULLNAME" type="text"> 
										<span class="text-danger"><?php echo form_error('FULLNAME'); ?></span>
									</div>
									<div class="col-lg-6" style="float:right">
										<label>Mobile No.: <span class="text-danger">*</span></label>
										<input class="form-control" value="" onkeypress="return isNumber(event)" name="MOBILE" id="MOBILE" style="width:100%" maxlength="10" size="10" type="text"> 
										<span class="text-danger"><?php echo form_error('MOBILE'); ?></span>
									</div>
									<div class="col-lg-6">
										<label>E-mail: <span class="text-danger">*</span></label>
										<input class="form-control" name="EMAIL_ID" id="EMAIL_ID" value="" type="text"> 
										<span class="text-danger"><?php echo form_error('EMAIL_ID'); ?></span>
									</div>
									<div class="col-lg-6">
										<label>Shop Name: <span class="text-danger">*</span></label>
										<input class="form-control" name="SHOP_NAME" value="" id="SHOP_NAME" type="text"> 
										<span class="text-danger"><?php echo form_error('SHOP_NAME'); ?></span>
									</div>
									<div class="col-lg-6">
										<label>Address: <span class="text-danger">*</span></label>
										<textarea class="form-control" name="ADDRESS" id="ADDRESS"></textarea>
										<span class="text-danger"><?php echo form_error('ADDRESS'); ?></span>
									</div>
									<div class="col-lg-6" style="float:right">
										<label>TIN No.: <span class="text-danger">*</span></label>
										<input class="form-control" name="TIN_NO" id="TIN_NO" style="width:100%" type="text"> 
										<span class="text-danger"><?php echo form_error('TIN_NO'); ?></span>
									</div>
                                </div>
                            </div>
							<br />
                            <button type='submit' class='btn btn-primary'>Save</button>&nbsp;
                        </div>
               <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>