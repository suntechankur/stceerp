<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
    <div class="create_batch_form">
        <div id="box">
            <h2>Vendor Master</h2>
		</div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align:center; color:#F00;"> </div>
                    <div class="panel-body">
                        <?php 
							$attr = array('role'=>'form');
							echo form_open('',$attr); ?>
							<?php foreach($taxs as $tax) {?>
								<div class="row">
									<div class="col-lg-12">
										<div class="col-lg-4 form-group">
											<label>Tax Name: <span class="text-danger">*</span></label>
											<input class="form-control" name="TAX_NAME" value="<?php echo $tax['TAX_NAME']; ?>" id="TAX_NAME" type="text"> 
											<span class="text-danger"><?php echo form_error('TAX_NAME'); ?></span>
										</div>
										<div class="col-lg-4 form-group">
											<label>Tax Percent.: <span class="text-danger">*</span></label>
											<div class="input-group">
												<input class="form-control" value="<?php echo $tax['TAX_PERCENT']; ?>" name="TAX_PERCENT" id="TAX_PERCENT" type="text">
												<span class="input-group-addon" id="basic-addon1">%</span>
											</div>
											<span class="text-danger"><?php echo form_error('TAX_PERCENT'); ?></span>
										</div>
										<div class="col-lg-4 form-group">
											<label>Tax Type: <span class="text-danger">*</span></label>
											<select class="form-control" name="TAX_TYPE" id="TAX_TYPE">
												<option value="">Select Tax Type</option>
												<option value="Local" <?php echo $tax['TAX_TYPE']=='Local' ? ' selected="selected"' : ''; ?>>Local</option>
												<option value="State" <?php echo $tax['TAX_TYPE']=='State' ? ' selected="selected"' : ''; ?>>State</option>
												<option value="National" <?php echo $tax['TAX_TYPE']=='National' ? ' selected="selected"' : ''; ?>>National</option>
												<option value="Inter-National" <?php echo $tax['TAX_TYPE']=='Inter-National' ? ' selected="selected"' : ''; ?>>Inter-National</option>
											</select>
											<span class="text-danger"><?php echo form_error('TAX_TYPE'); ?></span>
										</div>
										<div class="col-lg-4 form-group">
											<label>Tax Description: <span class="text-danger">*</span></label>
											<textarea class="form-control" name="TAX_DESC" id="TAX_DESC" ><?php echo $tax['TAX_DESC']; ?></textarea>
											<span class="text-danger"><?php echo form_error('TAX_DESC'); ?></span>
										</div>
										<div class="col-lg-12 form-group"><br>
											<button type='submit' class='btn btn-primary'>Update</button>
										</div>
									</div>
								</div>
							<?php } ?>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>