<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
    <div class="create_batch_form">
        <div id="box">
            <h2>Tax Master</h2>
		</div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align:center; color:#F00;"> </div>
                    <div class="panel-body">
                        <?php 
							$attr = array('role'=>'form');
							echo form_open('tax/add-tax',$attr); ?>
								<div class="row">
									<div class="col-lg-12">
										<div class="col-lg-4 form-group">
											<label>Tax Name: <span class="text-danger">*</span></label>
											<input class="form-control" name="TAX_NAME" value="" id="TAX_NAME" type="text"> 
											<span class="text-danger"><?php echo form_error('TAX_NAME'); ?></span>
										</div>
										<div class="col-lg-4 form-group">
											<label>Tax Percent.: <span class="text-danger">*</span></label>
											<div class="input-group">
												<input class="form-control" value="" name="TAX_PERCENT" id="TAX_PERCENT" type="text">
												<span class="input-group-addon" id="basic-addon1">%</span>
											</div>
											<span class="text-danger"><?php echo form_error('TAX_PERCENT'); ?></span>
										</div>
										<div class="col-lg-4 form-group">
											<label>Tax Type: <span class="text-danger">*</span></label>
											<select class="form-control" name="TAX_TYPE" id="TAX_TYPE">
												<option value="">Select Tax Type</option>
												<option value="Local">Local</option>
												<option value="State">State</option>
												<option value="National">National</option>
												<option value="Inter-National">Inter-National</option>
											</select>
											<span class="text-danger"><?php echo form_error('TAX_TYPE'); ?></span>
										</div>
										<div class="col-lg-4 form-group">
											<label>Tax Description: <span class="text-danger">*</span></label>
											<textarea class="form-control" name="TAX_DESC" id="TAX_DESC" style=""></textarea>
											<span class="text-danger"><?php echo form_error('TAX_DESC'); ?></span>
										</div>
										<div class="col-lg-12 form-group"><br>
											<button type='submit' class='btn btn-primary'>Save</button>
										</div>
									</div>
								</div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<br>
    <div id="box">
		<span class=""><?php echo $pagination; ?></span>
        <h2 class="text-center"> Tax List</h2>
	</div>
    <div class="panel panel-default">
        <div class="panel-body table-responsive">
            <table class="table table-bordered table-condensed table-stripped">
				<thead>
					<tr>
						<th>Action</th>
						<th>Sr. No.</th>
						<th>Tax Name</th>
						<th>Tax Description</th>
						<th>Tax Percent</th>
						<th>Tax Type</th>
					</tr>
				</thead>
				<tbody>
					<!-- <tr><td colspan="18" class="text-center">Sorry Record Not Found</td></tr> -->
						<?php 
							$count = $this->uri->segment(2) + 1;
							foreach($taxs as $tax){ 
							//unset($tax['ID']);
							$tax_id = $this->encrypt->encode($tax['TAX_ID']);
							?>
						<tr>
							<td>
								<a href="<?php echo base_url('tax/edit-tax/'.$tax_id); ?>"><span class="glyphicon glyphicon-edit"></span></a>
								<a href="<?php echo base_url('tax/del-tax/'.$tax_id) ?>"><span class="glyphicon glyphicon-trash"></span></a>
							</td>
							<td><?php echo $count; ?></td>
							<td><?php echo $tax['TAX_NAME']; ?></td>
							<td><?php echo empty($tax['TAX_DESC']) ? "N/a" : $tax['TAX_DESC']; ?></td>
							<td><?php echo rtrim($tax['TAX_PERCENT'],'%'); ?> %</td>
							<td><?php echo empty($tax['TAX_TYPE']) ? "-" : $tax['TAX_TYPE']; ?></td>
							<?php $count++;
							} ?>
						</tr>
						
				</tbody>
			</table>
		<br>
		</div>
        <div class="row">
            <div class="col-md-4 pull-right">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
</div>