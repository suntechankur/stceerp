<div class="gapping"></div>
<div class="col-md-12 col-sm-12">
    <div class="create_batch_form">
        <div id="box">
            <h2>Vendor Master</h2>
		</div>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align:center; color:#F00;"> </div>
                    <div class="panel-body">
                        <?php 
							$attr = array('role'=>'form');
							echo form_open('',$attr); ?>
							<?php foreach($vendors as $vendor) {?>
								<div class="row">
									<div class="col-lg-8">
										<div class="col-lg-6 form-group">
											<label >Vendor Name: <span class="text-danger">*</span></label>
											<input class="form-control" name="VENDOR_NAME" value="<?php $vendor['VENDOR_NAME']; ?>" id="VENDOR_NAME" type="text"> 
											<span class="text-danger"><?php echo form_error('VENDOR_NAME'); ?></span>
										</div>
										<div class="col-lg-6 form-group">
											<label>Company Name: <span class="text-danger">*</span></label>
											<input class="form-control" name="COMPANY_NAME" value="<?php echo $vendor['COMPANY_NAME']; ?>" id="COMPANY_NAME" type="text"> 
											<span class="text-danger"><?php echo form_error('COMPANY_NAME'); ?></span>
										</div>
										<div class="col-lg-6 form-group">
											<label>Contact NO 1: <span class="text-danger">*</span></label>
											<input class="form-control" value="<?php echo $vendor['TEL'];?>" name="TEL" id="TEL" maxlength="10" size="10" type="text"> 
											<span class="text-danger"><?php echo form_error('TEL'); ?></span>
										</div>
										<div class="col-lg-6 form-group">
											<label>Contact NO 2:</label>
											<input class="form-control" value="<?php echo $vendor['MOBILE'];?>" name="MOBILE" id="MOBILE" maxlength="10" size="10" type="text"> 
											<span class="text-danger"><?php echo form_error('MOBILE'); ?></span>
										</div>
									</div>
									<div class="col-lg-4 form-group">
										<label>Address: <span class="text-danger">*</span></label>
										<textarea class="form-control" name="ADDRESS" id="ADDRESS" style="height:108px"><?php echo $vendor['ADDRESS'];?></textarea>
										<span class="text-danger"><?php echo form_error('ADDRESS'); ?></span>
									</div>
								</div>
								<div class="col-lg-4 form-group">
									<label>Fax No.: </label>
									<input class="form-control" value="<?php echo $vendor['FAX_NO'];?>" name="FAX_NO" id="FAX_NO" type="text"> 
									<span class="text-danger"><?php echo form_error('FAX_NO'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>E-mail 1:</label>
									<input class="form-control" name="EMAIL1" id="EMAIL1" value="<?php echo $vendor['EMAIL1'];?>" type="text"> 
									<span class="text-danger"><?php echo form_error('EMAIL1'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>E-mail 2:</label>
									<input class="form-control" name="EMAIL2" id="EMAIL2" value="<?php echo $vendor['EMAIL2'];?>" type="text"> 
									<span class="text-danger"><?php echo form_error('EMAIL2'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>Vendor Type: </label>
									<select class="form-control" name="VENDOR_TYPE" id="VENDOR_TYPE">
										<option></option>
										<option></option>
										<option></option>
									</select>
									<span class="text-danger"><?php echo form_error('VENDOR_TYPE'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>Income Tax Number: </label>
									<input class="form-control" name="INCOME_TAX_NUMBER" value="<?php echo $vendor['INCOME_TAX_NUMBER'];?>" id="INCOME_TAX_NUMBER" type="text"> 
									<span class="text-danger"><?php echo form_error('INCOME_TAX_NUMBER'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>CST / TIN Number: </label>
									<input class="form-control" name="CST_NUMBER" value="<?php echo $vendor['CST_NUMBER'];?>" id="CST_NUMBER" type="text"> 
									<span class="text-danger"><?php echo form_error('CST_NUMBER'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>Service Tax Number: </label>
									<input class="form-control" name="SERVICE_TAX_NUMBER" value="<?php echo $vendor['SERVICE_TAX_NUMBER'];?>" id="SERVICE_TAX_NUMBER" type="text"> 
									<span class="text-danger"><?php echo form_error('SERVICE_TAX_NUMBER'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>ECC Number: </label>
									<input class="form-control" name="ECC_NUMBER" value="<?php echo $vendor['ECC_NUMBER'];?>" id="ECC_NUMBER" type="text"> 
									<span class="text-danger"><?php echo form_error('ECC_NUMBER'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>VAT / TIN Number: </label>
									<input class="form-control" name="VAT_TIN_NUMBER" value="<?php echo $vendor['VAT_TIN_NUMBER'];?>" id="VAT_TIN_NUMBER" type="text"> 
									<span class="text-danger"><?php echo form_error('VAT_TIN_NUMBER'); ?></span>
								</div>
								<div class="col-lg-4 form-group">
									<label>Applicable Tax: </label>
									<select class="form-control" name="TAX_ID" id="TAX_ID">
										<option></option>
										<option></option>
										<option></option>
									</select>
									<span class="text-danger"><?php echo form_error('TAX_ID'); ?></span>
								</div>
								<div class="col-lg-4" style="float:right">
									<br>
									<label class="form-check-inline">
										<input class="form-check-input" name="IS_ACTIVE" value="1" id="IS_ACTIVE" type="checkbox" <?php echo ($vendor['IS_ACTIVE'] == '1') ? "checked='checked'":''; ?>> Is Active &nbsp;&nbsp;
									</label>
								</div>
							<br />
							<button type='submit' class='btn btn-primary' >Update</button>&nbsp;
							<?php } ?>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>