$('.discount').click(function(event){
    if (this.checked){
       $('.discount_style').removeAttr('style');
        $.ajax({
        dataType: 'json',
        data: {angelos_csrf_token: csrfTkn()},
        url: base_url('discounts'),
        type: 'post',
        success: function(result){
            $('.discount_cover').css('display','none');
            var html = '<div class="row" style="margin-left:-30px;"><div class="col-md-12" style="margin-left:0px;"><div class="col-md-6"><label>Discount Scheme</label><select class="form-control discount_menu" style="width:100% ;" name="ADMISSION_DISCOUNT" id="DISC_SCHEME"><option value="">Please Select</option></select></div></div></div>';
            $('.discount_style').append(html).removeAttr('style');
            $.each(result, function(index, item){
                var dropdown = '<option value="'+item.DISCOUNT_ID+'">'+item.DISCOUNT_SCHEME+'</option>';
                $('.discount_menu').append(dropdown);
            });

         }
        });
    }
    else{
        $('.discount_style').empty().css('display','none');
        $('.discount_cover').removeAttr('style');
    }
});

$('.course_enrolled').multiselect({
    numberDisplayed: 0,
    maxHeight: 200,
    buttonWidth: 150,
    nSelectedText: 'Course selected',
    nonSelectedText: 'Select Course',
    enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
    filterPlaceholder: 'Search course ...',
    onChange: function(element, checked) {
        $('.courses_seld').empty();
        var modules = $('.module_enrolled option:selected');
        var modules_val = [];
        var courses = $('.course_enrolled option:selected');
        var courses_text = [];
        var courses_val = [];
        var min_fees = '';
        $(courses).each(function(index, courses){
            courses_text.push([$(this).text()]);
            courses_val.push($(this).val());
        });
        var join_courses = courses_val.join(",");

        $(modules).each(function(index, courses){
            modules_val.push($(this).val());
        });
        var join_modules = modules_val.join(",");

        var courses_seld = courses_val.length;
        for(var i=0;i<courses_seld; i++ ){
            var html = '<li class="list-group-item">'+courses_text[i]+'</li>'
            $('.courses_seld').append(html);
        }

         $.ajax({
            dataType: 'json',
            url: base_url('course-fees'),
            type: 'post',
            data: {
                cids:join_courses,
                mids:join_modules,
                angelos_csrf_token: csrfTkn()   //added by ankur for checking course fees on add admission page
            },
            success: function(result){
                var module_min_fees = 0;
                var module_max_fees = 0;

                var course_min_fees = 0;
                var course_max_fees = 0;

                var courseFeesLength = result.courseFees;
                var moduleFeesLength = result.moduleFees;

                // alert(courseFeesLength.length);
                if(moduleFeesLength.length > 0){
                    $.each(result.moduleFees, function(index, item){
                        var module_min_fees_calc = parseInt(item.MIN_FEES);
                        var module_max_fees_calc = parseInt(item.MAX_FEES);
                        module_min_fees += parseInt(module_min_fees_calc);
                        module_max_fees += parseInt(module_max_fees_calc);
                    });
                }

                if (courseFeesLength.length > 0) {
                    $.each(result.courseFees, function(index, item){
    //                    alert(result);
                        var course_min_fees_calc = parseInt(item.MIN_FEES);
                        var course_max_fees_calc = parseInt(item.MAX_FEES);
                        course_min_fees += parseInt(course_min_fees_calc);
                        course_max_fees += parseInt(course_max_fees_calc);
                    });
                }

                var totMinFees = module_min_fees + course_min_fees;
                var totMaxFees = module_max_fees + course_max_fees;
                // alert(totMinFees);

                $('.min_fee').val(totMinFees);
                $('.max_fee').val(totMaxFees);
                totalFeesPayable();   // function for checking minimum fees and maximum fees validation
            }
         });
    }
});
$('.module_enrolled').multiselect({
    numberDisplayed: 0,
    maxHeight: 200,
    buttonWidth: 150,
    nSelectedText: 'Module selected',
    nonSelectedText: 'Select Module',
    enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
    filterPlaceholder: 'Search module ...',
    onChange: function(element, checked) {
        $('.modules_seld').empty();
        var courses = $('.course_enrolled option:selected');
        var courses_val = [];
        var modules = $('.module_enrolled option:selected');
        var modules_text = [];
        var modules_val = [];
        var min_fees = '';
        $(modules).each(function(index, courses){
            modules_text.push([$(this).text()]);
            modules_val.push($(this).val());
        });
        var join_modules = modules_val.join(",");

        $(courses).each(function(index, courses){
            courses_val.push($(this).val());
        });
        var join_courses = courses_val.join(",");

        var modules_seld = modules.length;
        for(var i=0;i<modules_seld; i++ ){
            var html = '<li class="list-group-item">'+modules_text[i]+'</li>'
            $('.modules_seld').append(html);
        }

         $.ajax({
            dataType: 'json',
            url: base_url('course-fees'),
            type: 'post',
            data: {
                mids:join_modules,
                cids:join_courses,
                angelos_csrf_token: csrfTkn()
            },
            success: function(result){
                var module_min_fees = 0;
                var module_max_fees = 0;

                var course_min_fees = 0;
                var course_max_fees = 0;

                var courseFeesLength = result.courseFees;
                var moduleFeesLength = result.moduleFees;

                // alert(courseFeesLength.length);
                if(moduleFeesLength.length > 0){
                    $.each(result.moduleFees, function(index, item){
                        var module_min_fees_calc = parseInt(item.MIN_FEES);
                        var module_max_fees_calc = parseInt(item.MAX_FEES);
                        module_min_fees += parseInt(module_min_fees_calc);
                        module_max_fees += parseInt(module_max_fees_calc);
                    });
                }

                if (courseFeesLength.length > 0) {
                    $.each(result.courseFees, function(index, item){
    //                    alert(result);
                        var course_min_fees_calc = parseInt(item.MIN_FEES);
                        var course_max_fees_calc = parseInt(item.MAX_FEES);
                        course_min_fees += parseInt(course_min_fees_calc);
                        course_max_fees += parseInt(course_max_fees_calc);
                    });
                }

                var totMinFees = module_min_fees + course_min_fees;
                var totMaxFees = module_max_fees + course_max_fees;
                // alert(totMinFees);

                $('.min_fee').val(totMinFees);
                $('.max_fee').val(totMaxFees);
                totalFeesPayable();   // function for checking minimum fees and maximum fees validation
            }
         });
    }
});

$('.receipt').click(function(){
    if($(this).attr('data-other_receipt_modal_permission') == "false"){
        $('#receiptModal').modal('show');
        $('.chequeNo, .chequeDate, .bankName, .bankBranch').attr('disabled',true);
        $('.paymentType').val("1").prop("selected");
        $('.courseModule').empty();
         var admId = $(this).closest('tr').find('#ADMISSION_ID').text(),
             encAdmId = $(this).closest('tr').data('admid'),
             enqName = $(this).closest('tr').find('#ENQUIRY_NAME').text(),
             admDate = $(this).closest('tr').find('#ADMISSION_DATE').text(),
             cenName = $(this).closest('tr').find('#CENTRE_NAME').text(),
             module = $(this).closest('tr').find('#MODULE_TAKEN').html(),
             course = $(this).closest('tr').find('#COURSE_TAKEN').html(),
             courseFees = $(this).closest('tr').find('#TOTALFEES').text();
             // feespaid checking logic implemented on 20/09/2018 by Ankur to avoiding making of fees when balance is nil
             feesPaid = $(this).closest('tr').find('#0').text();
             balanceFees = courseFees - feesPaid;
             // end of taking fees details in calculation for receipt making

         $('.admId').html(admId);
         $('#saveAdmissionReceipt').attr('data-admid',encAdmId);
         $('.enqName').html(enqName);
         $('.admDate').html(admDate);
         $('.cenName option:contains("'+cenName+'")').attr('selected', 'selected');
         if (module != '') {
            $('.courseModule').append('<li class="list-group-item list-group-item-success moduleTaken"><strong>Modules</strong></li>');
            $('.moduleTaken').append(module);
         }
         if (course != '') {
            $('.courseModule').append('<li class="list-group-item list-group-item-info courseTaken"><strong>Courses</strong></li>');
            $('.courseTaken').append(course);
            $('.courseFees').html(courseFees);
            // condition for handeling fees details
            if(balanceFees >= 0){
              $('.balancefees').val(balanceFees);
            }
            else{
              $('.balancefees').val('0');
            }
            // end of condition
         }

        get_main_fee_receipts_details_on_model(encAdmId);
    }
    else{
        alert("Please first make "+$(this).attr('data-fees_applicable'));
        $('#otherReceiptsModal').modal('show');

        $('.chequeNo, .chequeDate, .bankName, .bankBranch').attr('disabled',true);
        $('.paymentType').val("1").prop("selected");
        $('.courseModule').empty();
        var admId = $(this).closest('tr').find('#ADMISSION_ID').text(),
             encAdmId = $(this).closest('tr').data('admid'),
             enqName = $(this).closest('tr').find('#ENQUIRY_NAME').text(),
             admDate = $(this).closest('tr').find('#ADMISSION_DATE').text(),
             cenName = $(this).closest('tr').find('#CENTRE_NAME').text(),
             module = $(this).closest('tr').find('#MODULE_TAKEN').html(),
             course = $(this).closest('tr').find('#COURSE_TAKEN').html();

        $('.admId').html(admId);
         $('#saveOtherReceipt').attr('data-admid',encAdmId);
         $('.enqName').html(enqName);
         $('.admDate').html(admDate);
         $('.cenName option:contains("'+cenName+'")').attr('selected', 'selected');
         if (module != '') {
            $('.courseModule').append('<li class="list-group-item list-group-item-success moduleTaken"><strong>Modules</strong></li>');
            $('.moduleTaken').append(module);
         }
         if (course != '') {
            $('.courseModule').append('<li class="list-group-item list-group-item-info courseTaken"><strong>Courses</strong></li>');
            $('.courseTaken').append(course);
         }

        get_other_receipt_details_on_model(encAdmId,0,0,'view_admission');
    }
});

$('.officialReceipt').click(function(){
    if($(this).attr('data-other_receipt_modal_permission') == "false"){
        if($(this).closest('tr').find('#ADMISSION_DATE').text() < "01/10/2017"){
          $('#officialReceiptsModal').modal('show');
          $('.chequeNo, .chequeDate, .bankName, .bankBranch').attr('disabled',true);
          $('.paymentType').val("1").prop("selected");
          $('.courseModule').empty();
           var admId = $(this).closest('tr').find('#ADMISSION_ID').text(),
               encAdmId = $(this).closest('tr').data('admid'),
               enqName = $(this).closest('tr').find('#ENQUIRY_NAME').text(),
               admDate = $(this).closest('tr').find('#ADMISSION_DATE').text(),
               cenName = $(this).closest('tr').find('#CENTRE_NAME').text(),
               module = $(this).closest('tr').find('#MODULE_TAKEN').html(),
               course = $(this).closest('tr').find('#COURSE_TAKEN').html(),
               courseFees = $(this).closest('tr').find('#TOTALFEES').text();

           $('.admId').html(admId);
           $('#saveAdmissionReceipt').attr('data-admid',encAdmId);
           $('.enqName').html(enqName);
           $('.admDate').html(admDate);
           $('.cenName option:contains("'+cenName+'")').attr('selected', 'selected');
           if (module != '') {
              $('.courseModule').append('<li class="list-group-item list-group-item-success moduleTaken"><strong>Modules</strong></li>');
              $('.moduleTaken').append(module);
           }
           if (course != '') {
              $('.courseModule').append('<li class="list-group-item list-group-item-info courseTaken"><strong>Courses</strong></li>');
              $('.courseTaken').append(course);
              $('.courseFees').html(courseFees);
           }

          get_main_fee_receipts_details_on_model(encAdmId);
        }
        else{
          alert("ankur");
        }
    }
    else{
        alert("Please first make "+$(this).attr('data-fees_applicable'));
        $('#otherReceiptsModal').modal('show');

        $('.chequeNo, .chequeDate, .bankName, .bankBranch').attr('disabled',true);
        $('.paymentType').val("1").prop("selected");
        $('.courseModule').empty();
        var admId = $(this).closest('tr').find('#ADMISSION_ID').text(),
             encAdmId = $(this).closest('tr').data('admid'),
             enqName = $(this).closest('tr').find('#ENQUIRY_NAME').text(),
             admDate = $(this).closest('tr').find('#ADMISSION_DATE').text(),
             cenName = $(this).closest('tr').find('#CENTRE_NAME').text(),
             module = $(this).closest('tr').find('#MODULE_TAKEN').html(),
             course = $(this).closest('tr').find('#COURSE_TAKEN').html();

        $('.admId').html(admId);
         $('#saveOtherReceipt').attr('data-admid',encAdmId);
         $('.enqName').html(enqName);
         $('.admDate').html(admDate);
         $('.cenName option:contains("'+cenName+'")').attr('selected', 'selected');
         if (module != '') {
            $('.courseModule').append('<li class="list-group-item list-group-item-success moduleTaken"><strong>Modules</strong></li>');
            $('.moduleTaken').append(module);
         }
         if (course != '') {
            $('.courseModule').append('<li class="list-group-item list-group-item-info courseTaken"><strong>Courses</strong></li>');
            $('.courseTaken').append(course);
         }

        get_other_receipt_details_on_model(encAdmId,0,0,'view_admission');
    }
});

function get_main_fee_receipts_details_on_model(encAdmId){
    $.ajax({
        dataType: 'json',
        url: base_url('admission/getAdmissionReceipt'),
        type: 'post',
        data: {
            angelos_csrf_token: csrfTkn(),
            admId:encAdmId
        },
        success: function(result){
            var paymentType = 0;
            $('.receiptTblData').empty();
            $('.instData').empty();
            var html = '';

            if(result.admReceipt.length > 0){
                var balance = 0;
                var totalAmount = result.admReceipt['0']['TOTALFEES'];
                $.each(result.admReceipt, function(index, item){
                   // console.log("encrypted id : "+item.ADMISSION_RECEIPTS_ID_ENC);

                    balance = totalAmount - item.AMOUNT_PAID_FEES;

                    totalAmount = balance;


                    switch(item.PAYMENT_TYPE){
                        case '1':
                            paymentType = 'CASH';
                        break;
                        case '2':
                            paymentType = 'UPI';
                        break;
                        case '3':
                            paymentType = 'CHEQUE';
                        break;
                        default:
                            paymentType = item.PAYMENT_TYPE;
                    }

                    var recDate = new Date(item.PAYMENT_DATE);
                    var recDateFormat = recDate.getDate() + '/' + (recDate.getMonth() + 1) + '/' + recDate.getFullYear();

                    //var recUrl = 'admission/admissionReciptPrint/'+item.ADMISSION_RECEIPTS_ID_ENC;

                    var printReceiptUrl = 'accounts/print-receipts/'+item.ADMISSION_RECEIPTS_ID_ENC+'/search';
                    var printSummaryUrl = 'accounts/print-summary/'+item.ADMISSION_RECEIPTS_ID_ENC+'/0';

                    html = '<tr><td>'+item.ADMISSION_RECEIPT_NO+'</td><td>'+recDateFormat+'</td><td>'+item.AMOUNT_PAID+'</td><td>'+item.AMOUNT_PAID_FEES+'</td><td>'+paymentType+'</td><td class="balanceRemains">'+balance+'</td><td><a href="'+base_url(printReceiptUrl)+'" target="_blank">Print</a></td><td><a href="'+base_url(printSummaryUrl)+'" target="_blank">Print</a></td></tr>';

                    $('.receiptTblData').append(html);
                });
            }
            else{
                html = '<tr><td colspan = "8"><center><span class="text-danger noRec">No Receipts Found</span></center></td></tr>';
                $('.receiptTblData').append(html);
            }

            $i=1;
            var length = (result.insPlan).length;
            if(length){
                $.each(result.insPlan,function(index, item){
                    var insDate = new Date(item.DUEDATE);
                    var insDateFormat = insDate.getDate() + '/' + (insDate.getMonth() + 1) + '/' + insDate.getFullYear();

                    var instData = '<tr><td>'+$i+'</td><td>'+insDateFormat+'</td><td>'+item.DUE_AMOUNT+'</td></tr>';
                    $('.instData').append(instData);
                $i++;
                });
            }
            else{
                var instData = '<tr><td colspan="3">No installment found.</td></tr>';
                    $('.instData').append(instData);
            }

            var lastBalance = $('#mainReceiptsTable tr:last').find('.balanceRemains').text();

            // if(lastBalance == 0){
            //     $("#receiptdetails").empty();
            //     $("#receiptdetails").append('<h4 class="modal-title">Admission Receipt&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label style="color:red;">Fees Balance is Nill</label></h4>');
            //     $('.submit').prop('disabled',true);
            // }
            // else{
            //     if(result.penalty>0)
            //     {
            //         $("#receiptdetails").empty();
            //         $("#receiptdetails").append('<h4 class="modal-title">Penalty : <span align="centre" style="color: red;">'+result.penalty+'</span></h4><h5>Admission Receipt</h5>');

            //             $('.submit').prop('disabled',true);
            //     }
            // }

        }
    });
}

$('.otherReceipt').click(function(){
    $('.chequeNo, .chequeDate, .bankName, .bankBranch').attr('disabled',true);
    $('.paymentType').val("1").prop("selected");
    $('.courseModule').empty();
    var admId = $(this).closest('tr').find('#ADMISSION_ID').text(),
         encAdmId = $(this).closest('tr').data('admid'),
         enqName = $(this).closest('tr').find('#ENQUIRY_NAME').text(),
         admDate = $(this).closest('tr').find('#ADMISSION_DATE').text(),
         cenName = $(this).closest('tr').find('#CENTRE_NAME').text(),
         module = $(this).closest('tr').find('#MODULE_TAKEN').html(),
         course = $(this).closest('tr').find('#COURSE_TAKEN').html();

    $('.admId').html(admId);
     $('#saveOtherReceipt').attr('data-admid',encAdmId);
     $('.enqName').html(enqName);
     $('.admDate').html(admDate);
     $('.cenName option:contains("'+cenName+'")').attr('selected', 'selected');
     if (module != '') {
        $('.courseModule').append('<li class="list-group-item list-group-item-success moduleTaken"><strong>Modules</strong></li>');
        $('.moduleTaken').append(module);
     }
     if (course != '') {
        $('.courseModule').append('<li class="list-group-item list-group-item-info courseTaken"><strong>Courses</strong></li>');
        $('.courseTaken').append(course);
     }

    get_other_receipt_details_on_model(encAdmId,0,0,'view_admission');
});

$('.make_payment_other_receipt').click(function(){
    if($(this).attr('data-permission') == "true"){
        $('#otherReceiptsModal').modal('show');
        get_other_receipt_details_on_model($(this).attr('data-admission_id'),$(this).attr('data-processing_fees'),$(this).attr('data-exam_fees'),'admission_installment');
    }
    else{
        $('#receiptModal').modal('show');
        get_main_fee_receipts_details_on_model($(this).attr('data-admission_id'));
    }
});

function get_other_receipt_details_on_model(encAdmId,process_fees,exam_fees,process_type){
    $.ajax({
            dataType: 'json',
            url: base_url('admission/otherRecData'),
            type: 'post',
            data: {
                admId: encAdmId,
                angelos_csrf_token: csrfTkn()
            },
            success: function(result){
                var html = '';
                $('.payFor').empty();
                $('.exmCrsMod').empty();
                $('.otherReceiptTblData').empty();
                $.each(result.payFor, function(index, item){
                    var otherFeesCategory = "";
                    if(process_type == 'admission_installment'){
                        if(process_fees != 0){
                            if(item.CONTROLFILE_VALUE == "PROCESSING FEE"){
                                otherFeesCategory = '<option value="'+item.CONTROLFILE_ID+'" selected="selected">'+item.CONTROLFILE_VALUE+'</option>';
                            }
                            //$('.amtSt').val(process_fees);
                        }
                        else{
                            if(item.CONTROLFILE_VALUE == "EXAM FEE"){
                                otherFeesCategory = '<option value="'+item.CONTROLFILE_ID+'" selected="selected">'+item.CONTROLFILE_VALUE+'</option>';
                            }
                           //$('.amtSt').val(exam_fees);
                        }
                        //$('.amtSt').keyup();
                    }
                    else{
                        if(item.CONTROLFILE_VALUE == "EXAM FEE"){
                            otherFeesCategory = '<option value="'+item.CONTROLFILE_ID+'" selected="selected">'+item.CONTROLFILE_VALUE+'</option>';
                        }
                        else{
                            otherFeesCategory = '<option value="'+item.CONTROLFILE_ID+'">'+item.CONTROLFILE_VALUE+'</option>';
                        }
                    }
                    $('.payFor').append(otherFeesCategory);
                });

                $.each(result.exmCourse,function(index, item){
                    var exmCourse = '<option value="'+item.COURSE_ID+'-1">'+item.COURSE_NAME+'</option>';
                    $('.exmCrsMod').append(exmCourse);
                });

                $.each(result.exmModule,function(index, item){
                    var exmModule = '<option value="'+item.MODULE_ID+'-2">'+item.MODULE_NAME+'</option>';
                    $('.exmCrsMod').append(exmModule);
                });

                if (result.othRecData.length > 0) {
                $.each(result.othRecData, function(index, item){
                        // alert(item.ADMISSION_ID);
                        var balance = item.TOTALFEES - item.AMOUNT_PAID_FEES;
                        switch(item.PAYMENT_TYPE){
                            case '1':
                                paymentType = 'CASH';
                            break;
                            case '2':
                                paymentType = 'UPI';
                            break;
                            case '3':
                                paymentType = 'CHEQUE';
                            break;
                            default:
                                paymentType = item.PAYMENT_TYPE;
                        }

                        var recDate = new Date(item.PAYMENT_DATE);
                        var recDateFormat = recDate.getDate() + '/' + (recDate.getMonth() + 1) + '/' + recDate.getFullYear();
                        // alert(recDateFormat);
                        var othRecUrl = 'accounts/print-other-receipts/'+item.PENALTY_RECEIPT_ID+'/search';
                            html = '<tr><td>'+item.PENALTY_RECEIPT_NO+'</td><td>'+recDateFormat+'</td><td>'+item.PARTICULAR+'</td><td>'+item.AMOUNT+'</td><td>'+item.AMOUNT_WITHOUT_ST+'</td><td>'+paymentType+'</td><td><a href="'+base_url(othRecUrl)+'" target="_blank">Print</a></td></tr>';

                        $('.otherReceiptTblData').append(html);
                    });
                }
                else{
                    html = '<tr><td colspan = "8"><center><span class="text-danger noRec">No Receipts Found</span></center></td></tr>';
                    $('.otherReceiptTblData').append(html);
                }
            }
        });
}

$('.amtSt').keyup(function() {
    var amtNoSt = Math.round($(this).val()); //var amtNoSt = Math.round($(this).val()/1.18);
    var servTax = $(this).val() - amtNoSt;
    $('.amtNoSt').html(amtNoSt);
    $('.servTax').html(servTax);
});

$('.paymentType').change(function(){

    switch($(this).val()){
        case '1':
            $("input").removeAttr('style');
            $('.chequeNo, .chequeDate, .bankName, .bankBranch').attr('disabled',true);
            break;
        case '2':
            $('.chequeNo, .chequeDate, .bankName, .bankBranch').removeAttr('disabled');
            break;
        case '3':
            $('.chequeNo, .chequeDate, .bankName, .bankBranch').removeAttr('disabled');
            break;
        default:
          $('.chequeNo, .chequeDate, .bankName, .bankBranch').attr('disabled',true);
    }
});

$('.chequeNo, .chequeDate, .bankName, .bankBranch').change(function(){
    if($('.paymentType').val() == '2' && $(this).val() != '' || $('.paymentType').val() == '3' && $(this).val() != ''){
        $(this).removeAttr('style');
    }
});

$('#saveAdmissionReceipt').click(function(){
    var admId = $(this).data('admid');
    // var type = "";
    // if($(this).attr('page_type') == "admission_details"){
    //     type = "admission_details";
    // }
    var amtSt = $('.amtSt').val();
    var paymentType = $('.paymentType').val();
    var payDate = $('.payDate').val();
    var recBy = $('.recBy').val();
    var cqDate = $('.chequeDate').val();
    var cqNo = $('.chequeNo').val();
    var bankName = $('.bankName').val();
    var bankBranch = $('.bankBranch').val();
    // var receiptFor = $('.receipt_for:checked').val();
    // var receiptSpecificFor = $('#receipt_specific_for_option').val();


    $.ajax({
            dataType: 'json',
            url: base_url('admission/saveAdmissionReceipt'),
            type: 'post',
            data: {
                angelos_csrf_token: csrfTkn(),
                admId:admId,
                TOTALFEES: amtSt,
                payType: paymentType,
                payDate: payDate,
                RECEIVED_BY: recBy,
                CHEQUE_DATE: cqDate,
                CHEQUE_NO: cqNo,
                BANK_NAME: bankName,
                BANK_BRANCH: bankBranch
            },
            beforeSend: function(){
              $("#wait").show();
            },
            complete: function(){
              $("#wait").hide();
            },
            success: function(result){
                if (result.errors) {
                    if(result.errors.length > 0){
                            $.each(result.errors, function(index, item){
                                $.each(item, function(key, value){
                                    $( "input[name='"+key+"']" ).css('border','2px solid red');
                                });
                            });
                        }
                }
                else{
                    $("input").removeAttr('style');
                }

                var printReceiptUrl = '';
                var printSummaryUrl = '';
                if (result.admRecData) {
                    $('.noRec').remove();
                    $.each(result.admRecData, function(index, item){
                        // var balance = item.TOTALFEES - item.AMOUNT_PAID_FEES;
                        switch(item.PAYMENT_TYPE){
                            case '1':
                                paymentType = 'Cash';
                            break;
                            case '2':
                                paymentType = 'UPI';
                            break;
                            case '3':
                                paymentType = 'Cheque';
                            break;
                            default:
                                paymentType = 'Unknown';
                        }

                        var recDate = new Date(item.PAYMENT_DATE);
                        var recDateFormat = recDate.getDate() + '/' + (recDate.getMonth() + 1) + '/' + recDate.getFullYear();

                        printReceiptUrl = 'accounts/print-receipts/'+item.ADMISSION_RECEIPTS_ID_ENC+'/search';
                        printSummaryUrl = 'accounts/print-summary/'+item.ADMISSION_RECEIPTS_ID_ENC+'/0';

                        html = '<tr><td>'+item.ADMISSION_RECEIPT_NO+'</td><td>'+recDateFormat+'</td><td>'+item.AMOUNT_PAID+'</td><td>'+item.AMOUNT_PAID_FEES+'</td><td>'+paymentType+'</td><td>'+item.BALANCE+'</td><td><a href="'+base_url(printReceiptUrl)+'" target="_blank">Print</a></td><td><a href="'+base_url(printSummaryUrl)+'" target="_blank">Print</a></td></tr>';

                        $('.receiptTblData').append(html);
                    });
                }
                window.open(base_url(printReceiptUrl), '_blank');
                window.open(base_url(printSummaryUrl), '_blank');
                location.reload();
            }
        });
});

$('#saveOtherReceipt').click(function(){
    var admId = $(this).data('admid');
    var amtSt = $('#otherReceiptsModal .amtSt').val();
    var paymentType = $('#otherReceiptsModal .paymentType').val();
    var payDate = $('#otherReceiptsModal .payDate').val();
    var recBy = $('#otherReceiptsModal .recBy').val();
    var cqDate = $('#otherReceiptsModal .chequeDate').val();
    var cqNo = $('#otherReceiptsModal .chequeNo').val();
    var bankName = $('#otherReceiptsModal .bankName').val();
    var bankBranch = $('#otherReceiptsModal .bankBranch').val();
    var payFor = $('#otherReceiptsModal .payFor').val();
    var exmCourse = $('.exmCrsMod').val();
    $.ajax({
            dataType: 'json',
            url: base_url('admission/addOtherReceipts'),
            type: 'post',
            data: {
                angelos_csrf_token: csrfTkn(),
                admId:admId,
                TOTALFEES: amtSt,
                payType: paymentType,
                payDate: payDate,
                payFor: payFor,
                RECEIVED_BY: recBy,
                CHEQUE_DATE: cqDate,
                CHEQUE_NO: cqNo,
                BANK_NAME: bankName,
                BANK_BRANCH: bankBranch,
                COURSES: exmCourse
            },
            beforeSend: function(){
              $("#wait").show();
            },
            complete: function(){
              $("#wait").hide();
            },
            success: function(result){
                if (result.errors) {
                    if(result.errors.length > 0){
                            $.each(result.errors, function(index, item){
                                $.each(item, function(key, value){
                                    $( "input[name='"+key+"']" ).css('border','2px solid red');
                                });
                            });
                        }
                }
                else{
                    $("input").removeAttr('style');
                }

                var othRecUrl = "";
                if (result.admRecData) {
                    $('.noRec').remove();
                    $.each(result.admRecData, function(index, item){
                        // alert(item.ADMISSION_ID);
                        var balance = item.TOTALFEES - item.AMOUNT_PAID_FEES;
                        switch(item.PAYMENT_TYPE){
                            case '1':
                                paymentType = 'Cash';
                            break;
                            case '2':
                                paymentType = 'UPI';
                            break;
                            case '3':
                                paymentType = 'Cheque';
                            break;
                            default:
                                paymentType = 'Unknown';
                        }

                        var recDate = new Date(item.PAYMENT_DATE);
                        var recDateFormat = recDate.getDate() + '/' + (recDate.getMonth() + 1) + '/' + recDate.getFullYear();
                        // alert(recDateFormat);
                        othRecUrl = 'accounts/print-other-receipts/'+item.admRecId+'/search';
                        var html = '<tr><td>'+item.PENALTY_RECEIPT_NO+'</td><td>'+recDateFormat+'</td><td>'+item.PARTICULAR+'</td><td>'+item.AMOUNT+'</td><td>'+item.AMOUNT_WITHOUT_ST+'</td><td>'+paymentType+'</td><td><a href="'+base_url(othRecUrl)+'" target="_blank">Print</a></td></tr>';

                        $('.otherReceiptTblData').append(html);
                    });
                }
                window.open(base_url(othRecUrl), '_blank');
                location.reload();
            }
        });
});

Date.isLeapYear = function (year) {
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
};

Date.getDaysInMonth = function (year, month) {
    return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

Date.prototype.isLeapYear = function () {
    return Date.isLeapYear(this.getFullYear());
};

Date.prototype.getDaysInMonth = function () {
    return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
};

Date.prototype.addMonths = function (value) {
    var n = this.getDate();
    this.setDate(1);
    this.setMonth(this.getMonth() + value);
    this.setDate(Math.min(n, this.getDaysInMonth()));
    return this;
};

$('.installmentDet').on('click', '.addInstallment', function(){

    // if ($('.delInstallment').length < 1) {
    //     var delBtnHtml = '<div class="row"><div class="col-md-4"><button class="btn btn-danger delInstallment" type="button"><i class="fa fa-trash"></i> Delete</button></div></div>';
    //     $('.installmentTable tr td:last-child').html(delBtnHtml);
    // }

    var installmentDateFetch = $(this).closest('tr').find('.enquiry_date').val();
    var splitInstallmentDate = installmentDateFetch.split("/");
    var createInstallmentDate = moment(splitInstallmentDate[2]+'-'+splitInstallmentDate[1]+'-'+splitInstallmentDate[0]);

    var installmentDateSet = createInstallmentDate.add(1, 'months');
    var installmentDate = new Date(installmentDateSet);

    var lastInstallmentDate = installmentDate.getDate();
    var lastInstallmentMonth = installmentDate.getMonth()+1;
    var lastInstallmentYear = installmentDate.getFullYear();

    var completeLastInstallmentDate = lastInstallmentDate + '/' + lastInstallmentMonth + '/' + lastInstallmentYear;

    $(this).remove();

    var html = '<tr><td><input type="text" class="form-control enquiry_date" value="'+completeLastInstallmentDate+'" name="due_date[]"></td><td><input type="text" class="form-control insAmt" value="" name="due_amount" class="due_amount"></td><td class="feePending">0</td><td><div class="row"><div class="col-md-4"><button class="btn btn-info saveInstallment" type="button"><i class="fa fa-floppy-o"></i> Save</button></div><div class="col-md-1"><p>&nbsp;</p></div><div class="col-md-4"><button class="btn btn-primary addInstallment" type="button">Add Details</button></div></div></td></tr>';
    $('.installmentDet').append(html);
    datePicker();
});

$('.installmentDet').on('click', '.saveInstallment',function(){
    var dueData = $(this).closest('tr').find('.enquiry_date').val();
    var dueAmount = $(this).closest('tr').find('.due_amount').val();
    var admId = $(this).data('adm_id');
    var admInstId = $(this).data('adm_inst_id');
    var fetchPrevRowFeePending = parseInt($(this).closest('tr').prev().find('.feePending').text());
    var currRowFeePending = parseInt($(this).closest('tr').find('.insAmt').val());
    if (fetchPrevRowFeePending == currRowFeePending) {
        alert('Please Change the Installment fees paid value')
        return false;
    }

    $.ajax({
        dataType: 'json',
        data: {
            angelos_csrf_token: csrfTkn(),
            admInstId: admInstId,
            admId: admId,
            due_date: dueData,
            due_amount: dueAmount
        },
        url: base_url('admission/addUpdateInstallment'),
        type: 'post',
        success: function(result){
           alert('Installment Saved Successfully');
           window.location.reload();
        }
    });
});

$('.installmentDet').on('click','.delInstallment',function(){
    var r = confirm("Are you sure you want to delete this installment.");
    var admInstId = $(this).data('adm_inst_id');
    if (r == true) {
        $.ajax({
            dataType: 'json',
            data: {
                angelos_csrf_token: csrfTkn(),
                admInstId: admInstId,
            },
            url: base_url('admission/delInstallment'),
            type: 'post',
            success: function(result){
               // alert('Installment Saved Successfully');
               $(this).closest('tr').remove();
                var installments = $('.installmentTable tr').length;
                var addInstallmentBtn = $('.addInstallment').length;
                var html = '<button class="btn btn-primary addInstallment" type="button">Add Details</button>';
                if(addInstallmentBtn == 0){
                    // alert('2');
                    $('.installmentTable tr:last-child td:last-child .col-md-4:last-child').append(html);
                }
                if (installments < 2) {
                    $('.installmentTable tr td:last-child').empty();
                    $('.installmentTable tr td:last-child').html(html);
                }
               window.location.reload();
            }
        });

    } else {
        return false;
    }
});

// for tatalfees validation using javascript
$("input[name='TOTALFEES']").change(function (e) {
  totalFeesPayable();
});

// totalfees calculation validation on course and module change and on minimum and maximum fees
function totalFeesPayable(){
  var min_fees = parseInt($('.min_fee').val());
  var max_fees = parseInt($('.max_fee').val());
  var fee_error = '';

  var fees = parseInt($("#total_payable_fees_amount").val());
  var a = 0;
  var b = 0;
  if(fees >= min_fees){
    a = 1;
  }
  if(fees <= max_fees){
    b = 1;
  }

  var c = a * b;
  if(c == 0){
    fee_error = 'Total Fees should be in the range of minimum fees and maximum fees';
    $('#add_admission_validation').removeAttr("disabled");
    $('#add_admission_validation').removeAttr("title");
    $('#add_admission_validation').attr({"disabled":"disabled","title":"Total fees is not between min and max fees."});
  }
  else{
    fee_error = '';
    $('#add_admission_validation').removeAttr("disabled");
    $('#add_admission_validation').removeAttr("title");
    $('#add_admission_validation').attr({"title":"Add Admission"});
  }

  $('.fee-error').html(fee_error);
}
