// var full = 'loc: '+location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');


function base_url(page = ''){
// 	if (window.location.origin == 'http://localhost' || window.location.origin == 'http://localhost:'+location.port) {
// 		return window.location.origin + '/stceerp/' + page;
// 	}
// 	else{
		return window.location.origin + '/' + page;
	//}
}
$(function() {
    $('input[name="hostockQTY"]').on('click', function() {
        if ($(this).val() == 'Received') {
            $('#Received').show();
        }
        else {
            $('#Received').hide();
        }

        if ($(this).val() == 'Issued') {
            $('#Issued').show();
        }
        else {
            $('#Issued').hide();
        }

    });
});

////////////////// start add employee shift timing
var counter = 2;

    $("#addButton").click(function () {

    if(counter>3){
            alert("Only 3 references allow");
            return false;
    }

    var newTextBoxDiv = $(document.createElement('div'))
         .attr("id", 'TextBoxDiv' + counter);


    newTextBoxDiv.after().html("<div class='form-group'><div class='row'><div class='col-md-12'><div class='col-md-4'> <label>Name : </label><input type='textbox' class='form-control' name='ref_name[]' id='textbox1' > </div>"
     +
          "<div class='col-md-4'><label>Relation : </label><input type='textbox' class='form-control' name='ref_relation[]' id='textbox2' ></div>"
           +
          "<div class='col-md-4'><label>Contact No.: </label><input type='textbox' class='form-control' name='ref_contact[]' id='textbox3' ></div>"

          +
          "</div></div></div>");

    newTextBoxDiv.appendTo("#TextBoxesGroup");


    counter++;
     });

     $("#removeButton").click(function () {
    if(counter==1){
          alert("No more references to remove");
          return false;
       }

    counter--;

        $("#TextBoxDiv" + counter).remove();

     });

    ///// code for EMP Shift Timing///////////////////////////

    var count = 2;

    $("#addShift").click(function () {

    if(count>10){
            alert("Only 10 Shifting Time Allow");
            return false;
    }

    var newShiftDiv = $(document.createElement('div'))
         .attr("id", 'ShiftDiv' + count).addClass('shiftDiv');

var centres = $('.centre').html();

newShiftDiv.after().html('<div class="form-group"><div class="row"><div class="col-md-6"><label>Center:</label><select name="CENTRE_ID[]" class="form-control centre">'+centres+'</select></div><div class="col-md-6"><input id="dynamic_timing" value="1" name="dynamic_timing[]" type="checkbox"><label>dynamic Timing (Timing is Frequently Changed)</label><input type="hidden" value="0" name="dynamic_timing[]"></div></div></div><div class="form-group"><div class="row"><div class="col-md-12"></div><div class="col-md-6"><label for="from_time">From</label> <input class="pref_time form-control" id="from_time" data-format="h:mm a" data-template="hh : mm a" name="from_time[]" value="10:00 am" type="text"></div><div class="col-md-6">   <label for="to_time">To &nbsp;&nbsp;&nbsp;</label>    <input class="pref_time" id="to_time" data-format="h:mm a" data-template="hh : mm a" name="to_time[]" value="06:30 pm" type="text"></div></div></div><div class="form-group"><div class="row"><div class="col-md-12 days_cont"><label>Days: </label> &nbsp;<input type="checkbox" data-day="Mon" value="Mon" name="days'+count+'[]" class="days"> Mon &nbsp;&nbsp;<input type="checkbox" data-day="Tue" value="Tue" name="days'+count+'[]" class="days"> Tue &nbsp;&nbsp;<input type="checkbox" data-day="Wed" value="Wed" name="days'+count+'[]" class="days"> Wed &nbsp;&nbsp;<input type="checkbox" data-day="Thu" value="Thu" name="days'+count+'[]" class="days"> Thu &nbsp;&nbsp;<input type="checkbox" data-day="Fri" value="Fri" name="days'+count+'[]" class="days"> Fri &nbsp;&nbsp;<input type="checkbox" data-day="Sat" value="Sat" name="days'+count+'[]" class="days"> Sat &nbsp;&nbsp;<input type="checkbox" data-day="Sun" value="Sun" name="days'+count+'[]" class="days"> Sun &nbsp;&nbsp;</div></div></div><hr>');
    newShiftDiv.appendTo("#ShiftGroup");
    prefTime();

    count++;
     });

     $("#removeShift").click(function () {
    if(count==1){
          alert("No more Shifting to remove");
          return false;
       }

    count--;

        $("#ShiftDiv" + count).remove();

     });

     $('#ShiftGroup').on('change','.centre',function(){
        var centre = $(this).val();
       if(!$('#ShiftGroup').hasClass('day_set')){
           $(this).closest('.shiftDiv').find('.days').each(function(){
                    var setDays = $(this).data('day');
                    $(this).val(setDays);
           });
           $(this).closest('.shiftDiv').addClass('day_set');
       }

     });


////////////////// end  add employee shift timing



$('.update_staff_login_details').click(function(){
  $("#current_employee_id").attr("data-id",$(this).attr("data-id"));
  $("#current_employee_id").attr("data-name",$(this).attr("data-name"));
  $(".username_detail").val($("#current_employee_id").attr("data-name"));
});

$('#update_staff_login_details').click(function(){

var user_id = $('#current_employee_id').attr("data-id");
var user_name = $('.username_detail').val();
var user_password = $('.user_password').val();

  $.ajax({
    dataType: "json",
    url: base_url('update_staff_login_details'),
    type: "post",
    data: { user_id: user_id,
            loginname: user_name,
            user_password: user_password,
            angelos_csrf_token: csrfTkn()},
    success: function(result){
      if(result){
                location.reload();
              }
    }
  });

});
////////////////// Start code for calculat age by DOB //////////////
$('#ENQUIRY_DATEOFBIRTH').change(function(){
var age =$(this).val();
var age = new Date().getFullYear() - new Date(age).getFullYear();
document.getElementById("age").innerHTML = age;

});
////////////////// end  code of calculat age by DOB //////////

$('.addTiming').click(function(){
        // alert($('.centre option:selected').text());
        var SelDays = [];
        $('.days').each(function(){
            if($(this).is(':checked')){
                SelDays.push($(this).val());
            }
        });

         $.ajax({
            url: base_url('hra/addTiming'),
            dataType: 'Json',
            data: {
                dynamic_timing: $('#dynamic_timing').val(),
                from_time: $('#from_time').val(),
                to_time: $('#to_time').val(),
                days: SelDays,
                centre: $('.centre').val(),
                centre_name: $('.centre option:selected').text(),
                angelos_csrf_token: csrfTkn()},
            method: 'post',
            success: function(result)
            {
                $('.info').empty();
                var html = '<div class="alert alert-info"><strong>'+result+'</strong></div>';
                $('.info').append(html);

            }
           });
     });

function csrfTkn(){
	var admToken = $('.admToken').val();
	return admToken;
}
$('.sel_data').on('change',function(){
   if(this.checked){
       $(this).addClass('seld_data');
   }
   else{
       $(this).removeClass('seld_data');
   }
});

$('.sel_all').click(function(event) {  //on click
    if (this.checked) { // check select status
        $('.sel_data').each(function() { //loop through each checkbox
            this.checked = true;  //select all checkboxes with class "checkbox1"
            $(this).addClass('seld_data');
        });
    } else {
        $('.sel_data').each(function() { //loop through each checkbox
            this.checked = false; //deselect all checkboxes with class "checkbox1"
            $(this).removeClass('seld_data');
        });
    }
});

$(".del_selected").on('click', function(e) {
    e.preventDefault();
    var checkValues = $('.sel_data:checked').map(function()
    {
        return $(this).val();
    }).get();

    // alert(checkValues);
    $.ajax({
        url: base_url('batchMaster/delSelectedStudents'),
        type: 'post',
        data: {
          data_id: checkValues,
          angelos_csrf_token: csrfTkn()
        },
        success: function(result){

            $('.seld_data').each(function(){
               $(this).closest('tr').remove();
               if(result){
                location.reload();
              }
            });
        }
    });
});

$('.genId').click(function(){
  $('form#empId').submit();
});

$(".addSelectedStudentsToBatch").on('click', function(e) {

    e.preventDefault();

    var checkValues = $('.sel_data:checked').map(function()
    {

        return $(this).val();
    }).get();

    $.ajax({
        url: base_url('batchMaster/addSelectedStudentsToBatch'),
        type: 'post',
        data: {
          Admission_ID: checkValues,
          batch_id: $('#batchId').text(),
        angelos_csrf_token: csrfTkn()
      },

        success: function(result){

            $('.seld_data').each(function(){
               $(this).closest('tr').remove();
               if(result){
                location.reload();
              }
            });
        }
    });
});


$(function()
{
	$('nav#menu').mmenu(
	{
		extensions	:	[ 'effect-slide-menu', 'pageshadow' ],
		searchfield	:	true,
		counters	:	true,
		navbar 		:  	{	title : 'Main menu'	},
		navbars		:  	[
							{
								position	: 'top',
								content		: [ 'searchfield' ]
							},
							{
								position	: 'top',
								content		: [ 'prev', 'title', 'close']
							},
							{
								position	: 'bottom',
								content 	: [ '' ]
							}
						]
	});

	$(".dash-button,.newclass").mouseover(function()
	{
		$(".newclass").stop().slideDown();
	});
	$(".dash-button,.newclass").mouseleave(function()
	{
		$(".newclass").stop().slideUp();
	});

	$("#show_hide_table").click(function()
	{
		$("#st_batch").stop().slideToggle();
		var str = document.getElementById("show_hide_table").innerHTML;
		if(str == "Show Batch table")
		{
			document.getElementById("show_hide_table").innerHTML = "Hide Batch table";
		}
		if(str == "Hide Batch table")
		{
			document.getElementById("show_hide_table").innerHTML = "Show Batch table";
		}
	});
});

/////// start get duration ajax /////////////
$('.getduration').on('change',function(){

    $.ajax({
            url: base_url('get_duration'),
            dataType: 'Json',
            data: {sId:$(this).val(),angelos_csrf_token: csrfTkn()},
            method: 'post',
            success: function(result)
            {
            	$.each(result, function(index,item){
            		var html = "<input type='text' class='form-control' readonly='' name='DURATION' id='DURATION' value='"+item.DURATION+"'>";
                	$('.duration').html(html);
            	});

            }
           });
});
/////// End duration ajax ///////////

$('.CENTRE_NAME').on('change',function(){
    var subSource = $('.CENTRE_NAME').attr('data-subsource');
    $.ajax({
            url: base_url('get_empname'),
            dataType: 'Json',
            data: {psId:$(this).val(),angelos_csrf_token: csrfTkn()},
            method: 'post',
            success: function(result)
            {
                $('.EMPLOYEE_ID').empty();
                $('.EMPLOYEE_ID').append('<option value="" selected="selected">Please Select</option><option value="other">Other</option>');
                $.each(result, function(index,item){
                    var html = '<option value='+item.EMPLOYEE_ID+'>'+item.EMP_NAME+'</option>';
                    $('.EMPLOYEE_ID').append(html);
                });
                if($('.CENTRE_NAME').attr('data-subsource') != 'err'){
                    $('.EMPLOYEE_ID').val(subSource);
                }

                if($('.CENTRE_NAME').data('faculty')){
                  $('.EMPLOYEE_ID').val($('.CENTRE_NAME').data('faculty'));
                }
            }
           });
});

$('.CENTRE_NAME').trigger('change');

$('.source_master').on('change',function(){
    var subSource = $('.source_master').attr('data-subsource');
    $.ajax({
            url: base_url('sub_sources'),
            dataType: 'Json',
            data: {psId:$(this).val(),angelos_csrf_token: csrfTkn()},
            method: 'post',
            success: function(result)
            {
                $('.sub_source').empty();
                $('.sub_source').append('<option value="">Please Select Subsource</option>');
                $.each(result, function(index,item){
                    var html = '<option value='+item.SOURCE_ID+'>'+item.SOURCE_SUBSOURCE+'</option>';
                    $('.sub_source').append(html);
                });
                if($('.source_master').attr('data-subsource') != 'err'){
                    $('.sub_source').val(subSource);
                }
            }
           });
});

if($('.source_master').attr('data-subsource')){
 $('.source_master').change();
}

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
var currentDate = dd+'/'+mm+'/'+yyyy;

$('.add_enquiry_date').datepicker(
    {
        dateFormat: 'dd/mm/yy'
    }
).val(currentDate);

datePicker();

function datePicker(){
  $('.enquiry_date').datepicker(
      {
          dateFormat: 'dd/mm/yy'
      }
  ).val();
}

$('.course_interested').multiselect({
	numberDisplayed: 0,
  maxHeight: 200,
  buttonWidth: 150,
  nSelectedText: 'course selected',
  nonSelectedText: 'Select course',
  enableFiltering: true,
  enableCaseInsensitiveFiltering: true,
  filterPlaceholder: 'Search course ...',
   onChange: function(element, checked) {
       $('.courses_seld').empty();
       var brands = $('.course_interested option:selected');
       var selected = [];
       $(brands).each(function(index, brand){
           selected.push([$(this).text()]);
       });

       var courses_seld = selected.length;
       for(var i=0;i<courses_seld; i++ ){
           var html = '<li class="list-group-item">'+selected[i]+'</li>';
           $('.courses_seld').append(html);
       }
   }
});

$('.module_interested').multiselect({
	numberDisplayed: 0,
  maxHeight: 200,
  buttonWidth: 150,
  nSelectedText: 'module selected',
  nonSelectedText: 'Select module',
  filterPlaceholder: 'Search module ...',
   onChange: function(element, checked) {
       $('.modules_seld').empty();
       var brands = $('.module_interested option:selected');
       var selected = [];
       $(brands).each(function(index, brand){
           selected.push([$(this).text()]);
       });

       var courses_seld = selected.length;
       for(var i=0;i<courses_seld; i++ ){
           var html = '<li class="list-group-item">'+selected[i]+'</li>';
           $('.modules_seld').append(html);
       }
   }
});

prefTime();
/* Preferance time script starts*/
function prefTime() {
  $('.pref_time').combodate();
}
/* Preferance time script ends*/

// for screen notifications of todays followups on time
function teleEnquiry(){
  $.ajax({
    dataType: "json",
    url: base_url('getCurrentEquiries'),
    type: "post",
    data: {angelos_csrf_token: csrfTkn()},
    success: function(result){
      $.each(result,function(index,item){
        // alert(item.encodeId);
        var preftime = item.PREFERRED_TIME;
        var notifytime = preftime.split(" - ");
        var newnotify = "";
        var addedtime = "";
        var todaydate = new Date();
        var converteddate = todaydate.toString();
        var datesplited = converteddate.split(/\s+/);
        if((notifytime[0].indexOf('pm') > -1) || (notifytime[0].indexOf('PM') > -1)){
          newnotify = notifytime[0].split(/\s+/);
          newnotify = newnotify[0].split(":");
          if(newnotify[0] < 12){
            addedtime = parseInt(newnotify[0]) + 12;
          }
          else{
            addedtime = newnotify[0];
          }

          newnotify = addedtime+":"+newnotify[1]+":00";
        }
        else {
          newnotify = notifytime[0].split(/\s+/);
          newnotify = newnotify[0]+":00";
        }
        var dbdate = datesplited[0]+" "+datesplited[1]+" "+datesplited[2]+" "+datesplited[3]+" "+newnotify+" "+datesplited[5]+" "+datesplited[6]+" "+datesplited[7]+" "+datesplited[8];
        var currenttime = new Date().getTime();
        var dbtime = new Date(dbdate).getTime();

        var timeoutseconds = dbtime - currenttime;
        var name = item.FIRSTNAME+" "+item.MIDDLENAME+" "+item.LASTNAME;
        var smobile = item.MOBILE;
        var pmobile = item.PARENT_NUMBER;
        var teleenquiryid = item.encodeId;
        var course_interested =[];
        $.each(item.courses,function(key,val){
          course_interested.push(val.COURSE_NAME);
        });

        if(timeoutseconds > 0){
          setTimeout(function(){pushNotifications(name,smobile,pmobile,teleenquiryid);},timeoutseconds);
        }
      });
    }
  });
}

function pushNotifications(name,mobileno,pmobileno,teleenquiryid){
  Push.create("Name : "+name,{
    body : "Mobile No. : "+mobileno+"\nParent Mob No. : \n"+pmobileno,
    icon : "http://www.ambicamachinetools.com/js/inquiry.jpg",
    onClick: function () {
        window.open(base_url('tele-enquiry-follow-up/details/'+teleenquiryid+'/teleEnquiry'),'_blank');
        this.close();
    }
  });
}

setTimeout(function(){
  teleEnquiry();
},5000);
// push notifications function ends

// for getting todayfollowup count in fraction of seconds
function followUpCount(){
  $.ajax({
    dataType: "json",
    url: base_url('getCurrentEquiries'),
    type: "post",
    data: {angelos_csrf_token: csrfTkn()},
    success: function(result){
      $("#followupcount").empty();
      $("#followupcount").text(result.length);
    }
  });
}

setTimeout(function(){
  followUpCount();
},1000);
// end of getting count

// for hinding and showing div with scrolling in hr setting of reinstate employee
$("#openReinstate").click(function(){
  $("#reinstateEmployee").toggle();
  var mainheight = $(document).height() - 800;
  $("html, body").animate({ scrollTop: mainheight }, 500);
});
// end of hinding and showing div with scrolling in hr setting of reinstate employee

// search ex employee details by combination of all searches and data automatically appended to div
$("#reinstateemployeesearch").click(function(){
  var fname = $("#firstname").val();
  var lname = $("#lastname").val();
  var designation = $("#designation").val();

  if(!(fname || lname || designation)) {
    alert("Please select atleast one option");
  }
  else {
    $.ajax({
      dataType: "json",
      url: base_url('hr_settings_reinstate_exemployeesearch'),
      type: "post",
      data: { firstname: fname,
              lastname: lname,
              designation: designation,
              angelos_csrf_token: csrfTkn() },
      success: function(result) {
        $("#reinstateemployeedata").show();
        $("#employeedetails").empty();
        if(result.length == 0) {
          $("#employeedetails").append('<tr><td colspan="7">No record found</td></tr>');
        }
        else {
          $.each(result,function(index,item) {
            if((item.EMP_FNAME == null)){
              item.EMP_FNAME = "";
            }
            if((item.EMP_LASTNAME == null)){
              item.EMP_LASTNAME = "";
            }
            if((item.CURRENT_DESIGNATION == null)){
              item.CURRENT_DESIGNATION = "";
            }
            if((item.EMP_TELEPHONE == null)){
              item.EMP_TELEPHONE = "";
            }
            if((item.EMP_MOBILENO == null)){
              item.EMP_MOBILENO = "";
            }
            if((item.DATEOFLEAVING == null)){
              item.DATEOFLEAVING = "";
            }

            $("#employeedetails").append('<tr><td>'+(index + 1)+'</td><td>'+item.EMPLOYEE_ID+'</td><td>'+item.EMP_FNAME+" "+item.EMP_LASTNAME+'</td><td>'+item.designation+'</td><td>'+item.EMP_TELEPHONE+'</td><td>'+item.EMP_MOBILENO+'</td><td>'+item.DATEOFLEAVING+'</td><td><button class="btn btn-primary reinstateemployeedata" data-empid="'+item.EMPLOYEE_ID+'"">Reinstate</button></td></tr>');
          });
        }
      }
    });
  }
});
// end of search ex employee details by combination of all searches and data automatically appended to div

// re insate on clicking of reinsate button  of all datas for inserting new data in table
$("#employeedetails").on('click','.reinstateemployeedata',function(){
  var empId = $(this).attr("data-empid");
  $.ajax({
    dataType: "json",
    url: base_url('new_reinstate_exemployee'),
    type: "post",
    data: { empid: empId,
            angelos_csrf_token: csrfTkn()},
    success: function(result){
      alert("Employee re-instate successfully !!!\nYour Employee Id = "+result);
    }
  });
});
// end of re insate on clicking of reinsate button  of all datas for inserting new data in table

// amount paid with gst change calculate amount without gst and gst paid in admission receipts page
$(".amount_with_gst").on('keyup',function(){
  var amount = parseInt($(this).val());
  if($.isNumeric(amount)){
    gstCalculation(amount);
  }
  else{
		gstCalculation(0);
  }
});

// function added by ankur on 20 April 2018 due to amount gst calculation
function gstCalculation(amount){
		$(".amount_without_gst").val(Math.round(amount / 1.1800));
		$(".gst_paid").val(amount - Math.round(amount / 1.1800));
}
// end amount paid with gst change calculate amount without gst and gst paid in admission receipts page

// for save changes of individual receipt on cash cheque deposition page
// for fees receipt and other receipt cash cheque depostion
$(".save_cash_cheque_deposition").click(function(){
  var response = $("#response").val();
  var receiptId = $(this).attr('data-receiptid');
  var amountDept = $(this).closest('tr').find('.amount_dept').val();
  var depositDate = $(this).closest('tr').find('.deposit_date').val();
  var depositSlipNo = $(this).closest('tr').find('.deposit_slip_no').val();
  var remarks = $(this).closest('tr').find('.remarks').val();

  if((amountDept != '') && (depositDate != '') && (depositSlipNo != '')){
    $.ajax({
      dataType: "json",
      url: base_url('save_cash_cheque_data/'+response),
      type: "post",
      data: { receipt_id: receiptId,
              amount_dept: amountDept,
              deposit_date: depositDate,
              deposit_slip_no: depositSlipNo,
              remarks: remarks,
              angelos_csrf_token: csrfTkn() },
      success: function(result) {
              if(result){
                location.reload();
              }
      }
    });
  }
  else{
    alert("Please fill changes details of receipts");
  }

});
// ends of save changes of individual receipt on cash cheque deposition page

// for enabling disabled text field on check box on cash cheque deposition page
// for fees receipt and other receipt cash cheque depostion
$(".cash_cheque_enable").click(function(){
  if($(this). prop("checked") == true){
    $(this).closest('tr').find('.receipt_input').attr('disabled',false);
  }
  else{
    $(this).closest('tr').find('.receipt_input').attr('disabled',true);
  }
});
// ends of enabling disabled text field on check box on cash cheque deposition page

// js for transfer receipt page of admission submenu
// function for fetching admission and receipts details on click of check button by feeding admission_id in from and to section
// transfer and fees receipts from and to panel fees selection
$(".check_transfer_receipt").click(function(){
  var response = $("#response").val();
  var action = $(this).attr("data-action");
  var admission_id = "";
  if(action == "to"){
    admission_id = $("#to_admission_id").val();
  }
  else if(action == "from"){
    admission_id = $("#from_admission_id").val();
  }

  if(parseInt(admission_id) > 0){
    $.ajax({
      dataType: "json",
      url: base_url('get_transfer_receipt_detail/'+response),
      type: "post",
      data: { admission_id:admission_id,
              angelos_csrf_token: csrfTkn() },
      success: function(result) {
        // for rendering student details
          $("#"+action+"_name").val(result['studData'][0]['ENQUIRY_FIRSTNAME']+" "+result['studData'][0]['ENQUIRY_MIDDLENAME']+" "+result['studData'][0]['ENQUIRY_LASTNAME']);
          $("#"+action+"_centre").val(result['studData'][0]['CENTRE_NAME']);
          $("#"+action+"_admission_date").val(result['studData'][0]['ADMISSION_DATE']);
          $("#"+action+"_course_taken").val(result['studData'][0]['COURSE_TAKEN']);
          $("#"+action+"_total_fees").val(result['studData'][0]['TOTALFEES']);

          $("#"+action+"_receipt_table").show();
          $("#"+action+"_receipt_details").empty();
          $.each(result['receiptDetails'],function(index,item) {
            var payment_date = item.PAYMENT_DATE;
            var payment_type = "";
            if(item.PAYMENT_TYPE != "CASH"){
              payment_type = item.PAYMENT_TYPE+"("+item.CHEQUE_NO+","+item.CHEQUE_DATE+","+item.CHEQUE_BANK_NAME+")";
            }
            else{
              payment_type = item.PAYMENT_TYPE;
            }

            // rendering receipts details in table and action checkbox divided as per from and to condition
            if(result['response'] == "other"){
              if(action == "from"){
                $("#"+action+"_receipt_details").append('<tr><td><input type="checkbox" class="form-control selected_transfer" data-receiptId='+item.PENALTY_RECEIPT_ID+'></td><td>'+item.PENALTY_RECEIPT_NO+'</td><td></td><td>'+payment_date+'</td><td>'+item.AMOUNT+'</td><td>'+item.AMOUNT_WITHOUT_ST+'</td><td>'+payment_type+'</td></tr>');
              }
              else{
                $("#"+action+"_receipt_details").append('<tr><td>'+item.PENALTY_RECEIPT_ID+'</td><td>'+item.PENALTY_RECEIPT_NO+'</td><td>'+item.PAYMENT_DATE+'</td><td>'+item.AMOUNT+'</td><td>'+item.AMOUNT_WITHOUT_ST+'</td><td>'+payment_type+'</td></tr>');
              }
            }
            else{
              if(action == "from"){
                $("#"+action+"_receipt_details").append('<tr><td><input type="checkbox" class="form-control selected_transfer" data-receiptId='+item.ADMISSION_RECEIPTS_ID+'></td><td>'+item.ADMISSION_RECEIPT_NO+'</td><td></td><td>'+payment_date+'</td><td>'+item.AMOUNT_PAID+'</td><td>'+item.AMOUNT_PAID_FEES+'</td><td>'+payment_type+'</td><td>'+item.BALANCES+'</td></tr>');
              }
              else{
                $("#"+action+"_receipt_details").append('<tr><td>'+item.ADMISSION_RECEIPT_NO+'</td><td></td><td>'+item.PAYMENT_DATE+'</td><td>'+item.AMOUNT_PAID+'</td><td>'+item.AMOUNT_PAID_FEES+'</td><td>'+payment_type+'</td><td>'+item.BALANCES+'</td></tr>');
              }
            }

          });
      }
    });
  }
  else{
    alert("Please Enter Admission Id");
  }
});

// function for validtaion and transfer selected button click
// for other receipts and fees receipt trasfer receipts
$("#transfer_receipts").click(function(){
  var response = $("#response").val();
  if(parseInt($("#from_admission_id").val()) && parseInt($("#to_admission_id").val())){
    if($(".selected_transfer:checked").length == 0){
      alert("Select receipts for transfer");
    }
    else if($("#from_remarks").val().length == 0){
      alert("Enter proper reason in remarks field");
    }
    else{
      var receipts = new Array();
      $(".selected_transfer:checked").each(function(){
          receipts.push($(this).attr('data-receiptId'));
      });
        $.ajax({
        dataType: "json",
        url: base_url('transfer_selected_receipts/'+response),
        type: "post",
        data: { receipts_data: receipts,
                from_admission_id: $("#from_admission_id").val(),
                to_admission_id: $("#to_admission_id").val(),
                remarks: $("#from_remarks").val(),
                angelos_csrf_token: csrfTkn() },
        success: function(result) {
                if(result == 1){
                  alert("Receipts are transfered successfully");
                  setTimeout(function(){location.reload();},1000);
                }
        }
      });
    }
  }
  else{
    alert("Please put admission id in transfer from and transfer to.");
  }
});
// ends of transfer receipts functionality

// function for updating control file data on hra/update-controls page
$(".edit_control_file_data").click(function(){
  $("#control_id").val($(this).attr('data-control-id'));
  $("#control_key").val($(this).attr('data-control-key'));
  $("#control_value").val($(this).attr('data-control-value'));
  $("#description").val($(this).attr('data-control-description'));
  $(".modal_control_key").val($(this).attr('data-control-key'));
  $(".modal_control_value").val($(this).attr('data-control-value'));
  $(".modal_control_description").val($(this).attr('data-control-description'));
});

$("#save_control_file_data").click(function(){
    var control_id = $("#control_id").val();
    var control_key = $("#modal_control_key").val();
    var control_value = $("#modal_control_value").val();
    var description = $("#modal_control_description").val();
      $.ajax({
        dataType: "json",
        url: base_url('update_control_data'),
        type: "post",
        data: { control_id: control_id,
                control_key: control_key,
                control_value: control_value,
                description: description,
                angelos_csrf_token: csrfTkn() },
        success: function(result) {
                if(result){
                  alert("Receipts are transfered successfully");
                  setTimeout(function(){location.reload();},1000);
                }
                else{
                  alert("Please check data.");
                }
        }
      });
});
// end of updating control file data

// function for updating bank name
$(".edit_bank_name").click(function(){
  $("#bank_id").val($(this).attr('data-bank-id'));
  $("#bank_name").val($(this).attr('data-bank-name'));
  $(".modal_bank_name").val($(this).attr('data-bank-name'));
});

$("#save_bank_name").click(function(){
    var bank_id = $("#bank_id").val();
    var bank_name = $("#modal_bank_name").val();
    if(bank_name != ''){
      $.ajax({
        dataType: "json",
        url: base_url('update_bank_name'),
        type: "post",
        data: { bank_id: bank_id,
                bank_name: bank_name,
                angelos_csrf_token: csrfTkn() },
        success: function(result) {
                if(result){
                  alert("Bank name updated.");
                  setTimeout(function(){location.reload();},1000);
                }
        }
      });
    }
    else{
      alert("Please fill bank name.");
    }

});
// end of function

// function for updating bank branch name
$(".edit_branch_name").click(function(){
  $("#branch_detail_id").val($(this).attr('data-branch-id'));
  $("#branch_detail_name").val($(this).attr('data-branch-name'));
  $("#branch_ifsc_code").val($(this).attr('data-ifsc-code'));
  $(".modal_branch_name").val($(this).attr('data-branch-name'));
  $(".modal_ifsc_code").val($(this).attr('data-ifsc-code'));
});

$("#save_branch_name").click(function(){
    var bank_id = $("#bank_detail_id").val();
    var branch_id = $("#branch_detail_id").val();
    var branch_name = $("#modal_branch_name").val();
    var ifsc_code = $("#modal_ifsc_code").val();
    if((branch_name != '') && (ifsc_code != '')){
      $.ajax({
        dataType: "json",
        url: base_url('update_bank_branch_name'),
        type: "post",
        data: { branch_id: branch_id,
                bank_id: bank_id,
                branch_name: branch_name,
                ifsc_code: ifsc_code,
                angelos_csrf_token: csrfTkn() },
        success: function(result) {
                if(result){
                  alert("Branch name updated.");
                  setTimeout(function(){location.reload();},1000);
                }
        }
      });
    }
    else{
      alert("Please fill branch name.");
    }

});

$(".delete_branch").click(function(){
  var branch_id = $(this).attr('data-branch-id');

  $.ajax({
    dataType: "json",
    url: base_url('delete_bank_branch_name'),
    type: "post",
    data: { branch_id: branch_id,
            angelos_csrf_token: csrfTkn() },
    success: function(result) {
            if(result){
              alert("Branch deleted.");
              setTimeout(function(){location.reload();},1000);
            }
    }
  });
});
// end of function

/* hide and show preffered time selection on checkbox selection in add tele enquiry page */
$(".select_prefered_time").click(function(){
  if($(this).is(':checked')){
    $("#preffered_time_selection_span").hide();
    $("#preffered_time_selection_label").show();
    $("#preffered_timing_selection_div").show();
  }
  else{
    $("#preffered_time_selection_span").show();
    $("#preffered_time_selection_label").hide();
    $("#preffered_timing_selection_div").hide();
  }
});
/* end of function above written*/

/* Dynamic Action based popup - by amar */
$('.action').click(function(){
  $('#myModal').remove();
  var url = $(this).data('url');
  var method = $(this).data('method');
  var message = $(this).data('message');
  var value = $(this).data('value');
  var selector = $(this).data('selector');
  var popup_header = $(this).data('popup_header');

  var create_modal = '<div id="myModal" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">'+popup_header+'</h4></div><div class="modal-body"><p>'+message+'</p></div><div class="modal-footer action-buttons-cont"></div></div></div></div>';
  if (method == 'delete') {
    $('body').append(create_modal);
    $('#myModal').modal("show");
    var action_buttons = '<button type="button" class="btn btn-danger pull-left modal-del" data-url="'+url+'" data-value="'+value+'" data-dismiss="modal">Yes</button><button type="button" class="btn btn-default" data-dismiss="modal">No</button>';
    $('.action-buttons-cont').append(action_buttons);
  }
});

$(document).on('click','.modal-del',function(){
  var url = $(this).data('url');
  var value = $(this).data('value');

  var data = {
      angelos_csrf_token: csrfTkn()
  };

  $.ajax({
    dataType: "json",
    url: url,
    type: "post",
    data: data,
    beforeSend: function(){
      $('body').html('Deleting...');
    },
    success: function(result) {
      if(result.status == true){
        location.reload();
      }
    }
  });
});

// end of dynamic action popup  - by amar
// end of dynamic action popup  - by amar=======
// end of dynamic action popup  - by amar

// send sms function integration for tele_enquiries on view-tele-enquiry page
$(".send_sms_to_all_enquiry").click(function(){
  if($(this).is(':checked')){
    $('.send_sms_to_enquiry').prop('checked', true);
    var list = $(".send_sms_to_enquiry").map(function(){
        return $(this).attr("data-mobile_no");
    }).get();

    $("#enquries_mobile_numbers").attr("data-enquries_mobile_numbers",list);
    $("#sms_enquiry_receipient_count").text(list.length+" Recepients selected.");
  }
  else{
    $('.send_sms_to_enquiry').prop('checked', false);
    $("#enquries_mobile_numbers").attr("data-enquries_mobile_numbers","");
    $("#sms_enquiry_receipient_count").text("No recepients selected.");
  }
});

$(".send_sms_to_enquiry").click(function(){
  if($(this).is(':checked')){
    var list = $(".send_sms_to_enquiry:checked").map(function(){
        return $(this).attr("data-mobile_no");
    }).get();
    $("#enquries_mobile_numbers").attr("data-enquries_mobile_numbers",list);
    $("#sms_enquiry_receipient_count").text(list.length+" Recepients selected.");
  }
  else{
    var list = $(".send_sms_to_enquiry:checked").map(function(){
        return $(this).attr("data-mobile_no");
    }).get();
    $("#enquries_mobile_numbers").attr("data-enquries_mobile_numbers",list);
    $("#sms_enquiry_receipient_count").text(list.length+" Recepients selected.");
  }
});

$("#send_sms_to_selected_tele_enquries").click(function(){
  var messageBody = $("#message_body_of_tele_enquiry_receipient_sms").val();
  var ccNumber = $("#cc_number_for_tele_sms").val();
  var recipientsList = $("#enquries_mobile_numbers").attr("data-enquries_mobile_numbers");

  $.ajax({
    dataType: "json",
    url: base_url('send_sms_to_receipts_from_emis'),
    type: "post",
    data: { message  : messageBody,
            cc  : ccNumber,
            mobileNumbers  : recipientsList,
            angelos_csrf_token: csrfTkn() },
    success: function(result) {
      if((result.response).indexOf("OK") >= 0){
        alert("Sms send successfully.");
      }
      else{
        alert("Some error occurred, please try after some time.");
      }
    }
  });
});
// ending of send sms function integration for tele_enquiries on view-tele-enquiry page

//exam booking page center selection
$(".select_exam_centre_name").change(function(){
	var centreid = $(this).val();
	var exam_date = $("#exam_scheduled_date").val();
	$.ajax({
		dataType: "json",
		url: base_url('admission/get_exam_slots'),
		type: "post",
		data: { centreid  : centreid,
						examdate  : exam_date,
						angelos_csrf_token: csrfTkn() },
		success: function(result) {
			$('#exam_time_select_table').show();
			var bookednine=0;
			var bookedninefifty=0;
			var bookedtenforty=0;
			var bookedeleventhirty=0;
			var bookedtwelvetwenty=0;
			var bookedoneten=0;
			var bookedtwo=0;
			var bookedtwofifty=0;
			var bookedthreeforty=0;
			var bookedfourthirty=0;
			var bookedfivetwenty=0;
			var notbookednine=0,notbookedninefifty=0,notbookedtenforty=0,notbookedeleventhirty=0,notbookedtwelvetwenty=0,notbookedoneten=0,notbookedtwo=0,notbookedtwofifty=0,notbookedthreeforty=0,notbookedfourthirty=0,notbookedfivetwenty=0;
		  $.each(result, function(index,item) {
				if((item.EXAM_TIME == "09:00") || (item.EXAM_TIME == "09:00 AM")){
					bookednine += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "09:50") || (item.EXAM_TIME == "09:50 AM")) {
					bookedninefifty += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "10:40") || (item.EXAM_TIME == "10:40 AM")) {
					bookedtenforty += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "11:30") || (item.EXAM_TIME == "11:30 AM")) {
					bookedeleventhirty += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "12:20") || (item.EXAM_TIME == "12:20 PM")) {
					bookedtwelvetwenty += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "01:10") || (item.EXAM_TIME == "01:10 PM")) {
					bookedoneten += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "02:00") || (item.EXAM_TIME == "02:00 PM")) {
					bookedtwo += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "02:50") || (item.EXAM_TIME == "02:50 PM")) {
					bookedtwofifty += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "03:40") || (item.EXAM_TIME == "03:40 PM")) {
					bookedthreeforty += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "04:30") || (item.EXAM_TIME == "04:30 PM")) {
					bookedfourthirty += parseFloat(item.Booking_Count);
				}
				else if ((item.EXAM_TIME == "05:20") || (item.EXAM_TIME == "05:20 PM")) {
					bookedfivetwenty += parseInt(item.Booking_Count);
				}
				notbookednine= parseInt(20 - bookednine);
				notbookedninefifty= parseInt(20 - bookedninefifty);
				notbookedtenforty= parseInt(20 - bookedtenforty);
				notbookedeleventhirty= parseInt(20 - bookedeleventhirty);
				notbookedtwelvetwenty= parseInt(20 - bookedtwelvetwenty);
				notbookedoneten= parseInt(20 - bookedoneten);
				notbookedtwo= parseInt(20 - bookedtwo);
				notbookedtwofifty= parseInt(20 - bookedtwofifty);
				notbookedthreeforty= parseInt(20 - bookedthreeforty);
				notbookedfourthirty= parseInt(20 - bookedfourthirty);
				notbookedfivetwenty= parseInt(20 - bookedfivetwenty);
		  });
			$("#bookedfor900am").text(bookednine);
			$("#bookedfor950am").text(bookedninefifty);
			$("#bookedfor1040am").text(bookedtenforty);
			$("#bookedfor1130am").text(bookedeleventhirty);
			$("#bookedfor1220pm").text(bookedtwelvetwenty);
			$("#bookedfor110pm").text(bookedoneten);
			$("#bookedfor200pm").text(bookedtwo);
			$("#bookedfor250pm").text(bookedtwofifty);
			$("#bookedfor340pm").text(bookedthreeforty);
			$("#bookedfor430pm").text(bookedfourthirty);
			$("#bookedfor520pm" ).text(bookedfivetwenty);

			$("#notbookedfor900am").text(notbookednine);
			$("#notbookedfor950am").text(notbookedninefifty);
			$("#notbookedfor1040am").text(notbookedtenforty);
			$("#notbookedfor1130am").text(notbookedeleventhirty);
			$("#notbookedfor1220pm").text(notbookedtwelvetwenty);
			$("#notbookedfor110pm").text(notbookedoneten);
			$("#notbookedfor200pm").text(notbookedtwo);
			$("#notbookedfor250pm").text(notbookedtwofifty);
			$("#notbookedfor340pm").text(notbookedthreeforty);
			$("#notbookedfor430pm").text(notbookedfourthirty);
			$("#notbookedfor520pm" ).text(notbookedfivetwenty);
		}
	});
});

$(".selectexamtime").click(function(){
	if($(this).closest('tr').find(".availableseat").text() <= 20){
		$("#exam_scheduled_time").val($(this).data("time"));
	}
	else{
		alert("Seats already full of this time slot.");
	}
});
//end of exam booking page center selection

//merge exam data on issue certificate page
$(".select_exam_id").click(function(){
  if($(this).is(':checked')){
    var list = $(".select_exam_id:checked").map(function(){
        return $(this).attr("data-examId");
    }).get();
    $(".examids").val(list);
    $(".exam_id_for_certificate").val(list);
  }
  else{
    var list = $(".select_exam_id:checked").map(function(){
        return $(this).attr("data-examId");
    }).get();
    $(".examids").val(list);
    $(".exam_id_for_certificate").val(list);
  }
});

$(".merge_exam_data").click(function(){
	var exam_id_list = $(".examids").val();
	var exam_id_array = exam_id_list.split(',');
	if(exam_id_array.length == "2"){
		$.ajax({
	    dataType: "json",
	    url: base_url('merge_exam_marks'),
	    type: "post",
	    data: { exam_id  : exam_id_list,
	            angelos_csrf_token: csrfTkn() },
	    success: function(result) {
				if(result){
					alert("Merge exam details successfully.");
				}
				else{
					alert("Kindly update data manually or check EXAM_TYPE data.");
				}
	    }
	  });
	}
	else{
		alert("Kindly select two exam data for merge marks.");
	}
});
//end of merge exam data on issue certificate page

// add asset form jquery function starts
$("#asset_main_category").change(function(){
	var categoryId = $(this).val();
	$.ajax({
		dataType: "json",
		url: base_url('get_assets_sub_category'),
		type: "post",
		data: { category_id  : categoryId,
						angelos_csrf_token: csrfTkn() },
		success: function(result) {
			if(result){
				$("#asset_sub_category").empty();
				$("#asset_sub_category").append("<option selected disabled>Select sub category</option>");
				$.each(result, function(index,item) {
					$("#asset_sub_category").append("<option value='"+item.CONTROLFILE_ID+"'>"+item.CONTROLFILE_VALUE+"</option>");
				});
			}
			else{
				alert("Kindly refresh the page and then try to select category.");
			}
		}
	});
});

$("#asset_selection").change(function(){
	var asset_name = $("#asset_selection option:selected").text();
	var asset_name_without_space = (asset_name).replace(/\s/g, "");
	var asset_name_without_space_with_uppercase = (asset_name_without_space).toUpperCase();
	$("#asset_master_name").val(asset_name_without_space_with_uppercase);
});

$("#asset_quantity").change(function(){
	if(($(this).val() == 0) || ($(this).val() == "") || ($(this).val() == null) || ($(this).val() < "1")){
		$(this).val("1");
	}
});
// end of add asset functions

// starting function for receipt changes in admission receipt modal
$(".receipt_for").click(function(){
	if($(this).attr("data-specific") == "university_receipt_for"){
		$("#receipt_specific_for").show(100);
		$("#further_receipt_actions").hide();
	}
	else{
		$("#receipt_specific_for").hide(50);
		$("#further_receipt_actions").show();
	}
});

$("#receipt_specific_for_option").click(function(){
	if(($("#receipt_specific_for_option option:selected").val()) != ""){
		$("#further_receipt_actions").show(100);
	}
});
// end of function for receipt changes in admission receipt modal

// placement add employeer details function starts
$(document).on('click','.activate_or_deactivate_job',function(){
	var status = $(this).attr("data-isactive");
	var company_id = $(this).attr("data-company_id");
		$.ajax({
	    dataType: "json",
	    url: base_url('update_company_job_status'),
	    type: "post",
	    data: { status  : status,
							company_id  : company_id,
	            angelos_csrf_token: csrfTkn() },
	    success: function(result) {
				if(result != 0){
					location.reload();
				}
				else{
					alert("Kindly try to update details after some time.");
				}
	    }
	  });
});

$(".delete_company_job").click(function(){
	var company_id = $(this).attr("data-company_id");
	if (confirm('Are you sure you want to delete this Job?')) {
		$.ajax({
	    dataType: "json",
	    url: base_url('delete_company_details'),
	    type: "post",
	    data: { company_id  : company_id,
	            angelos_csrf_token: csrfTkn() },
	    success: function(result) {
				if(result != 0){
					location.reload();
				}
				else{
					alert("Kindly try to delete job details after some time.");
				}
	    }
	  });
	}
});
// end of placement add employeer details function starts

// placement employer details update function
$('.edit_company_job').click(function(){
	var company_id = $(this).attr("data-company_id");
	$("#update_company_"+company_id).toggle();
});
// end of placement employer details update function

// toggling weekly test marks function
$('.hide_show_feeded_marks').click(function(){
	var className = $( "#marks_toggling_icon" ).attr('class');
	if(className == 'fa fa-2x fa-eye-slash'){
		$( "#marks_toggling_icon" ).removeClass('fa fa-2x fa-eye-slash');
		$( "#marks_toggling_icon" ).addClass('fa fa-2x fa-eye');
		$( "#marks_toggling_icon" ).css('color','green');
		$(".weekly_test_feeded").show();
	}
	else{
		$( "#marks_toggling_icon" ).removeClass('fa fa-2x fa-eye');
		$( "#marks_toggling_icon" ).addClass('fa fa-2x fa-eye-slash');
		$( "#marks_toggling_icon" ).css('color','red');
		$(".weekly_test_feeded").hide();
	}
});
// end of toggling function


// function for fetching weekly test marks data

// end of the function

// leave approval page function
$(".approval_actions").on('click','span',function(){
	var approvalId = $(this).attr("data-approval_id");
	var status = $(this).attr("data-type");
	var date = $(this).attr("data-date");
	var employeeId = $(this).attr("data-employee_id");
	var biometricId = $(this).attr("data-biometric_id");
	$.ajax({
		dataType: "json",
		url: base_url('approve_or_disapprove_leaves'),
		type: "post",
		data: { status  : status,
			      approval_id  : approvalId,
			      date  : date,
			      employee_id  : employeeId,
			      biometric_id  : biometricId,
						angelos_csrf_token: csrfTkn() },
		success: function(result) {
			if(result != 0){
				location.reload();
			}
			else{
				alert("Status Not updated.");
			}
		}
	});
});
// end of leave approval function

// disable right click and f12 and ctrl+shift+I
// $(document).bind("contextmenu",function(e){
//  return false;
// });
// $(document).keydown(function (event) {
//     if (event.keyCode == 123) { // Prevent F12
//         return false;
//     } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I
//         return false;
//     }
// });
// disable right click and f12 and ctrl+shift+I